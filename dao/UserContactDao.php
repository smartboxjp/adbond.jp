<?php
class UserContactDao extends CommonDao{

	//コンストラクタ
	function __construct(){
		parent::__construct();
	}

	//デストラクタ
	function __destruct(){
		parent::__destruct();
	}

	//全データの取得
	public function getAllMediaLoginUser(){
		is_null($this->mysqli) and $this->connect();
		$result = $this->mysqli->query(" SELECT * FROM media_login_users WHERE deleted_at is NULL ");

		$record_array = array();

		while($row = $result->fetch_array(MYSQLI_ASSOC)){
			$record = new LoginUser();
			$record->setId($row["id"]);
			$record->setUserName($row["user_name"]);
			$record->setLoginId($row["login_id"]);
			$record->setLoginPass($row["login_pass"]);
			$record->setLastLoginAt($row["last_login_at"]);
			$record->setCreatedAt($row["created_at"]);
			$record->setUpdatedAt($row["updated_at"]);
			$record_array[] = $record;
		}
		$result->close();
		return $record_array;
	}

	//指定されたデータの取得
	private function getMediaLoginUser($sql){
		is_null($this->mysqli) and $this->connect();
		$result = $this->mysqli->query($sql);

		$record = null;

		if($result->num_rows != 0){
			$row = $result->fetch_array(MYSQLI_ASSOC);
			$record = new MediaLoginUser();
			$record->setId($row["id"]);
			$record->setUserName($row["user_name"]);
			$record->setLoginId($row["login_id"]);
			$record->setLoginPass($row["login_pass"]);
			$record->setLastLoginAt($row["last_login_at"]);
			$record->setCreatedAt($row["created_at"]);
			$record->setUpdatedAt($row["updated_at"]);
		}
		$result->close();
		return $record;
	}

	//新規データの登録
	public function insertUserContact($record, &$result_message = ""){
//		$record = $this->escapeStringMediaLoginUser($record);
//		is_null($this->mysqli) and $this->connect();
		//idカラムに空を挿入することで，auto_incrementによるカウントアップ値
		//を代入できる
		$sql = " INSERT INTO user_contact values ('', "
			. " '" . $record->getUserName() . "', "
			. " '" . $record->getPref() . "', "
			. " '" . $record->getAddress1() . "', "
			. " '" . $record->getAddress2() . "', "
			. " '" . $record->getTel() . "', "
			. " '" . $record->getEmail() . "', "
			. " '" . $record->getTextarea() . "', "
			. " Now()) ";
		if(!$this->mysqli->query($sql)){
			$result_message = "登録に失敗しました" . $this->mysqli->error . "\n";
			return false;
		}
		$result_message = "一件追加しました";
		return true;
	}

// -----------------------------------------------------------------------------------
// 2010/12/16 追加
// お問い合わせデータの取得
// DBに接続し、user_contact情報を取得する

	public function getUserContactId($id){
		//$id = $this->mysqli->real_escape_string($id);
		$sql = " SELECT * FROM user_contact WHERE id = '$id' ";
		return $this->getUserContact($sql);
	}

	//指定されたデータの取得
	public function getUserContact($sql){
		is_null($this->mysqli) and $this->connect();
		$result = $this->mysqli->query($sql);

		$record = null;

		if($result->num_rows != 0){
			$row = $result->fetch_array(MYSQLI_ASSOC);
			$record = new UserContact();
			$record->setId($row["id"]);
			$record->setUserName($row["user_name"]);
			$record->setPref($row["pref"]);
			$record->setAddress1($row["address1"]);
			$record->setAddress2($row["address2"]);
			$record->setTel($row["tel"]);
			$record->setEmail($row["email"]);
			$record->setTextarea($row["textarea"]);
			$record->setCommitAt($row["commit_at"]);
		}
		$result->close();
		return $record;
	}
// -----------------------------------------------------------------------------------

	//データの削除
	public function deleteMediaLoginUser($id, &$result_message = ""){
		$id = $this->mysqli->real_escape_string($id);
		is_null($this->mysqli) and $this->connect();

		$sql = " UPDATE media_login_users SET deleted_at = Now() "
			. " WHERE id = '$id' and deleted_at is NULL ";
		if(!$this->mysqli->query($sql)){
			$result_message = "削除に失敗しました" . $this->mysqli->error . "\n";
			return false;
		}
		$result_message = "削除しました";
		return true;
	}

	//LoginUserクラスの各プロパティから特殊文字をエスケープ
	private function escapeStringMediaLoginUser($record){
		$record->setId($this->mysqli->real_escape_string($record->getId()));
		$record->setUserName($this->mysqli->real_escape_string($record->getUserName()));
		$record->setLoginId($this->mysqli->real_escape_string($record->getLoginId()));
		$record->setLoginPass($this->mysqli->real_escape_string($record->getLoginPass()));
		$record->setLastLoginAt($this->mysqli->real_escape_string($record->getLastLoginAt()));
		$record->setCreatedAt($this->mysqli->real_escape_string($record->getCreatedAt()));
		$record->setUpdatedAt($this->mysqli->real_escape_string($record->getUpdatedAt()));
		$record->setDeletedAt($this->mysqli->real_escape_string($record->getDeletedAt()));
		return $record;
	}
}
?>