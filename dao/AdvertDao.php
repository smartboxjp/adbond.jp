<?php
class AdvertDao extends CommonDao{

	//コンストラクタ
	function __construct(){
		parent::__construct();
	}

	//デストラクタ
	function __destruct(){
		parent::__destruct();
	}

	//全データの取得
	public function getAllAdvert(){
		is_null($this->mysqli) and $this->connect();
		$result = $this->mysqli->query(" SELECT * FROM advert WHERE deleted_at is NULL ");

		$record_array = array();

		while($row = $result->fetch_array(MYSQLI_ASSOC)){
			$record = new Advert();
			$record->setId($row["id"]);
			$record->setAdvertClientId($row["advert_client_id"]);
			$record->setAdvertCategoryId($row["advert_category_id"]);
			$record->setAdvertName($row["advert_name"]);
			$record->setContentType($row["content_type"]);
			$record->setSupportDocomo($row["support_docomo"]);
			$record->setSupportSoftbank($row["support_softbank"]);
			$record->setSupportAu($row["support_au"]);
			$record->setSupportPc($row["support_pc"]);
			$record->setSiteUrlDocomo($row["site_url_docomo"]);
			$record->setSiteUrlSoftbank($row["site_url_softbank"]);
			$record->setSiteUrlAu($row["site_url_au"]);
			$record->setSiteUrlPc($row["site_url_pc"]);
			$record->setSiteOutline($row["site_outline"]);
			$record->setClickPriceClient($row["click_price_client"]);
			$record->setClickPriceMedia($row["click_price_media"]);
			$record->setActionPriceClientDocomo1($row["action_price_client_docomo_1"]);
			$record->setActionPriceClientSoftbank1($row["action_price_client_softbank_1"]);
			$record->setActionPriceClientAu1($row["action_price_client_au_1"]);
			$record->setActionPriceClientPc1($row["action_price_client_pc_1"]);
			$record->setActionPriceClientDocomo2($row["action_price_client_docomo_2"]);
			$record->setActionPriceClientSoftbank2($row["action_price_client_softbank_2"]);
			$record->setActionPriceClientAu2($row["action_price_client_au_2"]);
			$record->setActionPriceClientPc2($row["action_price_client_pc_2"]);
			$record->setActionPriceClientDocomo3($row["action_price_client_docomo_3"]);
			$record->setActionPriceClientSoftbank3($row["action_price_client_softbank_3"]);
			$record->setActionPriceClientAu3($row["action_price_client_au_3"]);
			$record->setActionPriceClientPc3($row["action_price_client_pc_3"]);
			$record->setActionPriceClientDocomo4($row["action_price_client_docomo_4"]);
			$record->setActionPriceClientSoftbank4($row["action_price_client_softbank_4"]);
			$record->setActionPriceClientAu4($row["action_price_client_au_4"]);
			$record->setActionPriceClientPc4($row["action_price_client_pc_4"]);
			$record->setActionPriceClientDocomo5($row["action_price_client_docomo_5"]);
			$record->setActionPriceClientSoftbank5($row["action_price_client_softbank_5"]);
			$record->setActionPriceClientAu5($row["action_price_client_au_5"]);
			$record->setActionPriceClientPc5($row["action_price_client_pc_5"]);
			$record->setActionPriceMediaDocomo1($row["action_price_media_docomo_1"]);
			$record->setActionPriceMediaSoftbank1($row["action_price_media_softbank_1"]);
			$record->setActionPriceMediaAu1($row["action_price_media_au_1"]);
			$record->setActionPriceMediaPc1($row["action_price_media_pc_1"]);
			$record->setActionPriceMediaDocomo2($row["action_price_media_docomo_2"]);
			$record->setActionPriceMediaSoftbank2($row["action_price_media_softbank_2"]);
			$record->setActionPriceMediaAu2($row["action_price_media_au_2"]);
			$record->setActionPriceMediaPc2($row["action_price_media_pc_2"]);
			$record->setActionPriceMediaDocomo3($row["action_price_media_docomo_3"]);
			$record->setActionPriceMediaSoftbank3($row["action_price_media_softbank_3"]);
			$record->setActionPriceMediaAu3($row["action_price_media_au_3"]);
			$record->setActionPriceMediaPc3($row["action_price_media_pc_3"]);
			$record->setActionPriceMediaDocomo4($row["action_price_media_docomo_4"]);
			$record->setActionPriceMediaSoftbank4($row["action_price_media_softbank_4"]);
			$record->setActionPriceMediaAu4($row["action_price_media_au_4"]);
			$record->setActionPriceMediaPc4($row["action_price_media_pc_4"]);
			$record->setActionPriceMediaDocomo5($row["action_price_media_docomo_5"]);
			$record->setActionPriceMediaSoftbank5($row["action_price_media_softbank_5"]);
			$record->setActionPriceMediaAu5($row["action_price_media_au_5"]);
			$record->setActionPriceMediaPc5($row["action_price_media_pc_5"]);
			$record->setMsText1($row["ms_text_1"]);
			$record->setMsEmail1($row["ms_email_1"]);
			$record->setMsImageType1($row["ms_image_type_1"]);
			$record->setMsImageUrl1($row["ms_image_url_1"]);
			$record->setMsText2($row["ms_text_2"]);
			$record->setMsEmail2($row["ms_email_2"]);
			$record->setMsImageType2($row["ms_image_type_2"]);
			$record->setMsImageUrl2($row["ms_image_url_2"]);
			$record->setMsText3($row["ms_text_3"]);
			$record->setMsEmail3($row["ms_email_3"]);
			$record->setMsImageType3($row["ms_image_type_3"]);
			$record->setMsImageUrl3($row["ms_image_url_3"]);
			$record->setMsText4($row["ms_text_4"]);
			$record->setMsEmail4($row["ms_email_4"]);
			$record->setMsImageType4($row["ms_image_type_4"]);
			$record->setMsImageUrl4($row["ms_image_url_4"]);
			$record->setMsText5($row["ms_text_5"]);
			$record->setMsEmail5($row["ms_email_5"]);
			$record->setMsImageType5($row["ms_image_type_5"]);
			$record->setMsImageUrl5($row["ms_image_url_5"]);
			$record->setUniqueClickType($row["unique_click_type"]);
			$record->setPointBackFlag($row["point_back_flag"]);
			$record->setAdultFlag($row["adult_flag"]);
			$record->setDatingFlag($row["dating_flag"]);
			$record->setAdvertStartDate($row["advert_start_date"]);
			$record->setAdvertEndDate($row["advert_end_date"]);
			$record->setUnrestraintFlag($row["unrestraint_flag"]);
			$record->setTestFlag($row["test_flag"]);
			$record->setStatus($row["status"]);
			$record->setCreatedAt($row["created_at"]);
			$record->setUpdatedAt($row["updated_at"]);
			$record_array[] = $record;
		}
		$result->close();
		return $record_array;
	}

	//指定されたデータの取得
	private function getAdvert($sql){
		is_null($this->mysqli) and $this->connect();
		$result = $this->mysqli->query($sql);

		$record = null;

		if($result->num_rows != 0){
			$row = $result->fetch_array(MYSQLI_ASSOC);
			$record = new Advert();
			$record->setId($row["id"]);
			$record->setAdvertClientId($row["advert_client_id"]);
			$record->setAdvertCategoryId($row["advert_category_id"]);
			$record->setAdvertName($row["advert_name"]);
			$record->setContentType($row["content_type"]);
			$record->setSupportDocomo($row["support_docomo"]);
			$record->setSupportSoftbank($row["support_softbank"]);
			$record->setSupportAu($row["support_au"]);
			$record->setSupportPc($row["support_pc"]);
			$record->setSiteUrlDocomo($row["site_url_docomo"]);
			$record->setSiteUrlSoftbank($row["site_url_softbank"]);
			$record->setSiteUrlAu($row["site_url_au"]);
			$record->setSiteUrlPc($row["site_url_pc"]);
			$record->setSiteOutline($row["site_outline"]);
			$record->setClickPriceClient($row["click_price_client"]);
			$record->setClickPriceMedia($row["click_price_media"]);
			$record->setActionPriceClientDocomo1($row["action_price_client_docomo_1"]);
			$record->setActionPriceClientSoftbank1($row["action_price_client_softbank_1"]);
			$record->setActionPriceClientAu1($row["action_price_client_au_1"]);
			$record->setActionPriceClientPc1($row["action_price_client_pc_1"]);
			$record->setActionPriceClientDocomo2($row["action_price_client_docomo_2"]);
			$record->setActionPriceClientSoftbank2($row["action_price_client_softbank_2"]);
			$record->setActionPriceClientAu2($row["action_price_client_au_2"]);
			$record->setActionPriceClientPc2($row["action_price_client_pc_2"]);
			$record->setActionPriceClientDocomo3($row["action_price_client_docomo_3"]);
			$record->setActionPriceClientSoftbank3($row["action_price_client_softbank_3"]);
			$record->setActionPriceClientAu3($row["action_price_client_au_3"]);
			$record->setActionPriceClientPc3($row["action_price_client_pc_3"]);
			$record->setActionPriceClientDocomo4($row["action_price_client_docomo_4"]);
			$record->setActionPriceClientSoftbank4($row["action_price_client_softbank_4"]);
			$record->setActionPriceClientAu4($row["action_price_client_au_4"]);
			$record->setActionPriceClientPc4($row["action_price_client_pc_4"]);
			$record->setActionPriceClientDocomo5($row["action_price_client_docomo_5"]);
			$record->setActionPriceClientSoftbank5($row["action_price_client_softbank_5"]);
			$record->setActionPriceClientAu5($row["action_price_client_au_5"]);
			$record->setActionPriceClientPc5($row["action_price_client_pc_5"]);
			$record->setActionPriceMediaDocomo1($row["action_price_media_docomo_1"]);
			$record->setActionPriceMediaSoftbank1($row["action_price_media_softbank_1"]);
			$record->setActionPriceMediaAu1($row["action_price_media_au_1"]);
			$record->setActionPriceMediaPc1($row["action_price_media_pc_1"]);
			$record->setActionPriceMediaDocomo2($row["action_price_media_docomo_2"]);
			$record->setActionPriceMediaSoftbank2($row["action_price_media_softbank_2"]);
			$record->setActionPriceMediaAu2($row["action_price_media_au_2"]);
			$record->setActionPriceMediaPc2($row["action_price_media_pc_2"]);
			$record->setActionPriceMediaDocomo3($row["action_price_media_docomo_3"]);
			$record->setActionPriceMediaSoftbank3($row["action_price_media_softbank_3"]);
			$record->setActionPriceMediaAu3($row["action_price_media_au_3"]);
			$record->setActionPriceMediaPc3($row["action_price_media_pc_3"]);
			$record->setActionPriceMediaDocomo4($row["action_price_media_docomo_4"]);
			$record->setActionPriceMediaSoftbank4($row["action_price_media_softbank_4"]);
			$record->setActionPriceMediaAu4($row["action_price_media_au_4"]);
			$record->setActionPriceMediaPc4($row["action_price_media_pc_4"]);
			$record->setActionPriceMediaDocomo5($row["action_price_media_docomo_5"]);
			$record->setActionPriceMediaSoftbank5($row["action_price_media_softbank_5"]);
			$record->setActionPriceMediaAu5($row["action_price_media_au_5"]);
			$record->setActionPriceMediaPc5($row["action_price_media_pc_5"]);
			$record->setMsText1($row["ms_text_1"]);
			$record->setMsEmail1($row["ms_email_1"]);
			$record->setMsImageType1($row["ms_image_type_1"]);
			$record->setMsImageUrl1($row["ms_image_url_1"]);
			$record->setMsText2($row["ms_text_2"]);
			$record->setMsEmail2($row["ms_email_2"]);
			$record->setMsImageType2($row["ms_image_type_2"]);
			$record->setMsImageUrl2($row["ms_image_url_2"]);
			$record->setMsText3($row["ms_text_3"]);
			$record->setMsEmail3($row["ms_email_3"]);
			$record->setMsImageType3($row["ms_image_type_3"]);
			$record->setMsImageUrl3($row["ms_image_url_3"]);
			$record->setMsText4($row["ms_text_4"]);
			$record->setMsEmail4($row["ms_email_4"]);
			$record->setMsImageType4($row["ms_image_type_4"]);
			$record->setMsImageUrl4($row["ms_image_url_4"]);
			$record->setMsText5($row["ms_text_5"]);
			$record->setMsEmail5($row["ms_email_5"]);
			$record->setMsImageType5($row["ms_image_type_5"]);
			$record->setMsImageUrl5($row["ms_image_url_5"]);
			$record->setUniqueClickType($row["unique_click_type"]);
			$record->setPointBackFlag($row["point_back_flag"]);
			$record->setAdultFlag($row["adult_flag"]);
			$record->setDatingFlag($row["dating_flag"]);
			$record->setAdvertStartDate($row["advert_start_date"]);
			$record->setAdvertEndDate($row["advert_end_date"]);
			$record->setUnrestraintFlag($row["unrestraint_flag"]);
			$record->setTestFlag($row["test_flag"]);
			$record->setStatus($row["status"]);
			$record->setCreatedAt($row["created_at"]);
			$record->setUpdatedAt($row["updated_at"]);
		}
		$result->close();
		return $record;
	}

	//idでデータを取得 deleted_at is NULLを省く
	public function getAdvertByIdAll($id){
		$id = $this->mysqli->real_escape_string($id);
		$sql = " SELECT * FROM advert WHERE id = '$id' ";
		return $this->getAdvert($sql);
	}

	//idでデータを取得
	public function getAdvertById($id){
		$id = $this->mysqli->real_escape_string($id);
		$sql = " SELECT * FROM advert WHERE id = '$id' AND deleted_at is NULL ";
		return $this->getAdvert($sql);
	}

	//id statusでデータを取得
	public function getAdvertByIdStatus($id, $status){
		$id = $this->mysqli->real_escape_string($id);
		$status = $this->mysqli->real_escape_string($status);
		$sql = " SELECT * FROM advert WHERE id = '$id' AND status = '$status' AND deleted_at is NULL ";
		return $this->getAdvert($sql);
	}

	//advert_nameでデータを取得
	public function getAdvertByAdvertName($advert_name){
		$advert_name = $this->mysqli->real_escape_string($advert_name);
		$sql = " SELECT * FROM advert WHERE advert_name = '$advert_name' AND deleted_at is NULL ";
		return $this->getAdvert($sql);
	}

	//データの更新
	public function updateAdvert($record, &$result_message = ""){
		$record = $this->escapeStringAdvert($record);
		is_null($this->mysqli) and $this->connect();
		$sql = " UPDATE advert SET "
			. " advert_client_id = '" . $record->getAdvertClientId() . "', "
			. " advert_category_id = '" . $record->getAdvertCategoryId() . "', "
			. " advert_name = '" . $record->getAdvertName() . "', "
			. " content_type = '" . $record->getContentType() . "', "
			. " support_docomo = '" . $record->getSupportDocomo() . "', "
			. " support_softbank = '" . $record->getSupportSoftbank() . "', "
			. " support_au = '" . $record->getSupportAu() . "', "
			. " support_pc = '" . $record->getSupportPc() . "', "
			. " site_url_docomo = '" . $record->getSiteUrlDocomo() . "', "
			. " site_url_softbank = '" . $record->getSiteUrlSoftbank() . "', "
			. " site_url_au = '" . $record->getSiteUrlAu() . "', "
			. " site_url_pc = '" . $record->getSiteUrlPc() . "', "
			. " site_outline = '" . $record->getSiteOutline() . "', "
			. " click_price_client = '" . $record->getClickPriceClient() . "', "
			. " click_price_media = '" . $record->getClickPriceMedia() . "', "
			. " action_price_client_docomo_1 = '" . $record->getActionPriceClientDocomo1() . "', "
			. " action_price_client_softbank_1 = '" . $record->getActionPriceClientSoftbank1() . "', "
			. " action_price_client_au_1 = '" . $record->getActionPriceClientAu1() . "', "
			. " action_price_client_pc_1 = '" . $record->getActionPriceClientPc1() . "', "
			. " action_price_client_docomo_2 = '" . $record->getActionPriceClientDocomo2() . "', "
			. " action_price_client_softbank_2 = '" . $record->getActionPriceClientSoftbank2() . "', "
			. " action_price_client_au_2 = '" . $record->getActionPriceClientAu2() . "', "
			. " action_price_client_pc_2 = '" . $record->getActionPriceClientPc2() . "', "
			. " action_price_client_docomo_3 = '" . $record->getActionPriceClientDocomo3() . "', "
			. " action_price_client_softbank_3 = '" . $record->getActionPriceClientSoftbank3() . "', "
			. " action_price_client_au_3 = '" . $record->getActionPriceClientAu3() . "', "
			. " action_price_client_pc_3 = '" . $record->getActionPriceClientPc3() . "', "
			. " action_price_client_docomo_4 = '" . $record->getActionPriceClientDocomo4() . "', "
			. " action_price_client_softbank_4 = '" . $record->getActionPriceClientSoftbank4() . "', "
			. " action_price_client_au_4 = '" . $record->getActionPriceClientAu4() . "', "
			. " action_price_client_pc_4 = '" . $record->getActionPriceClientPc4() . "', "
			. " action_price_client_docomo_5 = '" . $record->getActionPriceClientDocomo5() . "', "
			. " action_price_client_softbank_5 = '" . $record->getActionPriceClientSoftbank5() . "', "
			. " action_price_client_au_5 = '" . $record->getActionPriceClientAu5() . "', "
			. " action_price_client_pc_5 = '" . $record->getActionPriceClientPc5() . "', "
			. " action_price_media_docomo_1 = '" . $record->getActionPriceMediaDocomo1() . "', "
			. " action_price_media_softbank_1 = '" . $record->getActionPriceMediaSoftbank1() . "', "
			. " action_price_media_au_1 = '" . $record->getActionPriceMediaAu1() . "', "
			. " action_price_media_pc_1 = '" . $record->getActionPriceMediaPc1() . "', "
			. " action_price_media_docomo_2 = '" . $record->getActionPriceMediaDocomo2() . "', "
			. " action_price_media_softbank_2 = '" . $record->getActionPriceMediaSoftbank2() . "', "
			. " action_price_media_au_2 = '" . $record->getActionPriceMediaAu2() . "', "
			. " action_price_media_pc_2 = '" . $record->getActionPriceMediaPc2() . "', "
			. " action_price_media_docomo_3 = '" . $record->getActionPriceMediaDocomo3() . "', "
			. " action_price_media_softbank_3 = '" . $record->getActionPriceMediaSoftbank3() . "', "
			. " action_price_media_au_3 = '" . $record->getActionPriceMediaAu3() . "', "
			. " action_price_media_pc_3 = '" . $record->getActionPriceMediaPc3() . "', "
			. " action_price_media_docomo_4 = '" . $record->getActionPriceMediaDocomo4() . "', "
			. " action_price_media_softbank_4 = '" . $record->getActionPriceMediaSoftbank4() . "', "
			. " action_price_media_au_4 = '" . $record->getActionPriceMediaAu4() . "', "
			. " action_price_media_pc_4 = '" . $record->getActionPriceMediaPc4() . "', "
			. " action_price_media_docomo_5 = '" . $record->getActionPriceMediaDocomo5() . "', "
			. " action_price_media_softbank_5 = '" . $record->getActionPriceMediaSoftbank5() . "', "
			. " action_price_media_au_5 = '" . $record->getActionPriceMediaAu5() . "', "
			. " action_price_media_pc_5 = '" . $record->getActionPriceMediaPc5() . "', "
			. " ms_text_1 = '" . $record->getMsText1() . "', "
			. " ms_email_1 = '" . $record->getMsEmail1() . "', "
			. " ms_image_type_1 = '" . $record->getMsImageType1() . "', "
			. " ms_image_url_1 = '" . $record->getMsImageUrl1() . "', "
			. " ms_text_2 = '" . $record->getMsText2() . "', "
			. " ms_email_2 = '" . $record->getMsEmail2() . "', "
			. " ms_image_type_2 = '" . $record->getMsImageType2() . "', "
			. " ms_image_url_2 = '" . $record->getMsImageUrl2() . "', "
			. " ms_text_3 = '" . $record->getMsText3() . "', "
			. " ms_email_3 = '" . $record->getMsEmail3() . "', "
			. " ms_image_type_3 = '" . $record->getMsImageType3() . "', "
			. " ms_image_url_3 = '" . $record->getMsImageUrl3() . "', "
			. " ms_text_4 = '" . $record->getMsText4() . "', "
			. " ms_email_4 = '" . $record->getMsEmail4() . "', "
			. " ms_image_type_4 = '" . $record->getMsImageType4() . "', "
			. " ms_image_url_4 = '" . $record->getMsImageUrl4() . "', "
			. " ms_text_5 = '" . $record->getMsText5() . "', "
			. " ms_email_5 = '" . $record->getMsEmail5() . "', "
			. " ms_image_type_5 = '" . $record->getMsImageType5() . "', "
			. " ms_image_url_5 = '" . $record->getMsImageUrl5() . "', "
			. " unique_click_type = '" . $record->getUniqueClickType() . "', "
			. " point_back_flag = '" . $record->getPointBackFlag() . "', "
			. " adult_flag = '" . $record->getAdultFlag() . "', "
			. " dating_flag = '" . $record->getDatingFlag() . "', "
			. " advert_start_date = '" . $record->getAdvertStartDate() . "', "
			. " advert_end_date = '" . $record->getAdvertEndDate() . "', "
			. " unrestraint_flag = '" . $record->getUnrestraintFlag() . "', "
			. " test_flag = '" . $record->getTestFlag() . "', "
			. " status = '" . $record->getStatus() . "' "
			. " WHERE id = '" . $record->getId() . "' AND deleted_at is NULL ";
		if(!$this->mysqli->query($sql)) {
			$result_message = "更新に失敗しました" . $this->mysqli->error . "\n";
			return false;
		}
		$result_message = "更新しました";
		return true;
	}

	//新規データの登録
	public function insertAdvert($record, &$result_message = ""){
		$record = $this->escapeStringAdvert($record);
		is_null($this->mysqli) and $this->connect();
		//idカラムに空を挿入することで，auto_incrementによるカウントアップ値
		//を代入できる
		$sql = " INSERT INTO advert values ('', "
			. " '" . $record->getAdvertClientId() . "', "
			. " '" . $record->getAdvertCategoryId() . "', "
			. " '" . $record->getAdvertName() . "', "
			. " '" . $record->getContentType() . "', "
			. " '" . $record->getSupportDocomo() . "', "
			. " '" . $record->getSupportSoftbank() . "', "
			. " '" . $record->getSupportAu() . "', "
			. " '" . $record->getSupportPc() . "', "
			. " '" . $record->getSiteUrlDocomo() . "', "
			. " '" . $record->getSiteUrlSoftbank() . "', "
			. " '" . $record->getSiteUrlAu() . "', "
			. " '" . $record->getSiteUrlPc() . "', "
			. " '" . $record->getSiteOutline() . "', "
			. " '" . $record->getClickPriceClient() . "', "
			. " '" . $record->getClickPriceMedia() . "', "
			. " '" . $record->getActionPriceClientDocomo1() . "', "
			. " '" . $record->getActionPriceClientSoftbank1() . "', "
			. " '" . $record->getActionPriceClientAu1() . "', "
			. " '" . $record->getActionPriceClientPc1() . "', "
			. " '" . $record->getActionPriceClientDocomo2() . "', "
			. " '" . $record->getActionPriceClientSoftbank2() . "', "
			. " '" . $record->getActionPriceClientAu2() . "', "
			. " '" . $record->getActionPriceClientPc2() . "', "
			. " '" . $record->getActionPriceClientDocomo3() . "', "
			. " '" . $record->getActionPriceClientSoftbank3() . "', "
			. " '" . $record->getActionPriceClientAu3() . "', "
			. " '" . $record->getActionPriceClientPc3() . "', "
			. " '" . $record->getActionPriceClientDocomo4() . "', "
			. " '" . $record->getActionPriceClientSoftbank4() . "', "
			. " '" . $record->getActionPriceClientAu4() . "', "
			. " '" . $record->getActionPriceClientPc4() . "', "
			. " '" . $record->getActionPriceClientDocomo5() . "', "
			. " '" . $record->getActionPriceClientSoftbank5() . "', "
			. " '" . $record->getActionPriceClientAu5() . "', "
			. " '" . $record->getActionPriceClientPc5() . "', "
			. " '" . $record->getActionPriceMediaDocomo1() . "', "
			. " '" . $record->getActionPriceMediaSoftbank1() . "', "
			. " '" . $record->getActionPriceMediaAu1() . "', "
			. " '" . $record->getActionPriceMediaPc1() . "', "
			. " '" . $record->getActionPriceMediaDocomo2() . "', "
			. " '" . $record->getActionPriceMediaSoftbank2() . "', "
			. " '" . $record->getActionPriceMediaAu2() . "', "
			. " '" . $record->getActionPriceMediaPc2() . "', "
			. " '" . $record->getActionPriceMediaDocomo3() . "', "
			. " '" . $record->getActionPriceMediaSoftbank3() . "', "
			. " '" . $record->getActionPriceMediaAu3() . "', "
			. " '" . $record->getActionPriceMediaPc3() . "', "
			. " '" . $record->getActionPriceMediaDocomo4() . "', "
			. " '" . $record->getActionPriceMediaSoftbank4() . "', "
			. " '" . $record->getActionPriceMediaAu4() . "', "
			. " '" . $record->getActionPriceMediaPc4() . "', "
			. " '" . $record->getActionPriceMediaDocomo5() . "', "
			. " '" . $record->getActionPriceMediaSoftbank5() . "', "
			. " '" . $record->getActionPriceMediaAu5() . "', "
			. " '" . $record->getActionPriceMediaPc5() . "', "
			. " '" . $record->getMsText1() . "', "
			. " '" . $record->getMsEmail1() . "', "
			. " '" . $record->getMsImageType1() . "', "
			. " '" . $record->getMsImageUrl1() . "', "
			. " '" . $record->getMsText2() . "', "
			. " '" . $record->getMsEmail2() . "', "
			. " '" . $record->getMsImageType2() . "', "
			. " '" . $record->getMsImageUrl2() . "', "
			. " '" . $record->getMsText3() . "', "
			. " '" . $record->getMsEmail3() . "', "
			. " '" . $record->getMsImageType3() . "', "
			. " '" . $record->getMsImageUrl3() . "', "
			. " '" . $record->getMsText4() . "', "
			. " '" . $record->getMsEmail4() . "', "
			. " '" . $record->getMsImageType4() . "', "
			. " '" . $record->getMsImageUrl4() . "', "
			. " '" . $record->getMsText5() . "', "
			. " '" . $record->getMsEmail5() . "', "
			. " '" . $record->getMsImageType5() . "', "
			. " '" . $record->getMsImageUrl5() . "', "
			. " '" . $record->getUniqueClickType() . "', "
			. " '" . $record->getPointBackFlag() . "', "
			. " '" . $record->getAdultFlag() . "', "
			. " '" . $record->getDatingFlag() . "', "
			. " '" . $record->getAdvertStartDate() . "', "
			. " '" . $record->getAdvertEndDate() . "', "
			. " '" . $record->getUnrestraintFlag() . "', "
			. " '" . $record->getTestFlag() . "', "
			. " '" . $record->getStatus() . "', "
			. " Now(), Now(), NULL) ";
		if(!$this->mysqli->query($sql)){
			$result_message = "登録に失敗しました" . $this->mysqli->error . "\n";
			return false;
		}
		$result_message = "一件追加しました";
		return true;
	}

	//データの削除
	public function deleteAdvert($id, &$result_message = ""){
		$id = $this->mysqli->real_escape_string($id);
		is_null($this->mysqli) and $this->connect();

		$sql = "UPDATE advert SET deleted_at = Now() "
			. " WHERE id = '$id' and deleted_at is NULL ";
		if(!$this->mysqli->query($sql)){
			$result_message = "削除に失敗しました" . $this->mysqli->error . "\n";
			return false;
		}
		$result_message = "削除しました";
		return true;
	}

	//LoginUserクラスの各プロパティから特殊文字をエスケープ
	private function escapeStringAdvert($record){
		$record->setId($this->mysqli->real_escape_string($record->getId()));
		$record->setAdvertClientId($this->mysqli->real_escape_string($record->getAdvertClientId()));
		$record->setAdvertCategoryId($this->mysqli->real_escape_string($record->getAdvertCategoryId()));
		$record->setAdvertName($this->mysqli->real_escape_string($record->getAdvertName()));
		$record->setContentType($this->mysqli->real_escape_string($record->getContentType()));
		$record->setSupportDocomo($this->mysqli->real_escape_string($record->getSupportDocomo()));
		$record->setSupportSoftbank($this->mysqli->real_escape_string($record->getSupportSoftbank()));
		$record->setSupportAu($this->mysqli->real_escape_string($record->getSupportAu()));
		$record->setSupportPc($this->mysqli->real_escape_string($record->getSupportPc()));
		$record->setSiteUrlDocomo($this->mysqli->real_escape_string($record->getSiteUrlDocomo()));
		$record->setSiteUrlSoftbank($this->mysqli->real_escape_string($record->getSiteUrlSoftbank()));
		$record->setSiteUrlAu($this->mysqli->real_escape_string($record->getSiteUrlAu()));
		$record->setSiteUrlPc($this->mysqli->real_escape_string($record->getSiteUrlPc()));
		$record->setSiteOutline($this->mysqli->real_escape_string($record->getSiteOutline()));
		$record->setClickPriceClient($this->mysqli->real_escape_string($record->getClickPriceClient()));
		$record->setClickPriceMedia($this->mysqli->real_escape_string($record->getClickPriceMedia()));
		$record->setActionPriceClientDocomo1($this->mysqli->real_escape_string($record->getActionPriceClientDocomo1()));
		$record->setActionPriceClientSoftbank1($this->mysqli->real_escape_string($record->getActionPriceClientSoftbank1()));
		$record->setActionPriceClientAu1($this->mysqli->real_escape_string($record->getActionPriceClientAu1()));
		$record->setActionPriceClientPc1($this->mysqli->real_escape_string($record->getActionPriceClientPc1()));
		$record->setActionPriceClientDocomo2($this->mysqli->real_escape_string($record->getActionPriceClientDocomo2()));
		$record->setActionPriceClientSoftbank2($this->mysqli->real_escape_string($record->getActionPriceClientSoftbank2()));
		$record->setActionPriceClientAu2($this->mysqli->real_escape_string($record->getActionPriceClientAu2()));
		$record->setActionPriceClientPc2($this->mysqli->real_escape_string($record->getActionPriceClientPc2()));
		$record->setActionPriceClientDocomo3($this->mysqli->real_escape_string($record->getActionPriceClientDocomo3()));
		$record->setActionPriceClientSoftbank3($this->mysqli->real_escape_string($record->getActionPriceClientSoftbank3()));
		$record->setActionPriceClientAu3($this->mysqli->real_escape_string($record->getActionPriceClientAu3()));
		$record->setActionPriceClientPc3($this->mysqli->real_escape_string($record->getActionPriceClientPc3()));
		$record->setActionPriceClientDocomo4($this->mysqli->real_escape_string($record->getActionPriceClientDocomo4()));
		$record->setActionPriceClientSoftbank4($this->mysqli->real_escape_string($record->getActionPriceClientSoftbank4()));
		$record->setActionPriceClientAu4($this->mysqli->real_escape_string($record->getActionPriceClientAu4()));
		$record->setActionPriceClientPc4($this->mysqli->real_escape_string($record->getActionPriceClientPc4()));
		$record->setActionPriceClientDocomo5($this->mysqli->real_escape_string($record->getActionPriceClientDocomo5()));
		$record->setActionPriceClientSoftbank5($this->mysqli->real_escape_string($record->getActionPriceClientSoftbank5()));
		$record->setActionPriceClientAu5($this->mysqli->real_escape_string($record->getActionPriceClientAu5()));
		$record->setActionPriceClientPc5($this->mysqli->real_escape_string($record->getActionPriceClientPc5()));
		$record->setActionPriceMediaDocomo1($this->mysqli->real_escape_string($record->getActionPriceMediaDocomo1()));
		$record->setActionPriceMediaSoftbank1($this->mysqli->real_escape_string($record->getActionPriceMediaSoftbank1()));
		$record->setActionPriceMediaAu1($this->mysqli->real_escape_string($record->getActionPriceMediaAu1()));
		$record->setActionPriceMediaPc1($this->mysqli->real_escape_string($record->getActionPriceMediaPc1()));
		$record->setActionPriceMediaDocomo2($this->mysqli->real_escape_string($record->getActionPriceMediaDocomo2()));
		$record->setActionPriceMediaSoftbank2($this->mysqli->real_escape_string($record->getActionPriceMediaSoftbank2()));
		$record->setActionPriceMediaAu2($this->mysqli->real_escape_string($record->getActionPriceMediaAu2()));
		$record->setActionPriceMediaPc2($this->mysqli->real_escape_string($record->getActionPriceMediaPc2()));
		$record->setActionPriceMediaDocomo3($this->mysqli->real_escape_string($record->getActionPriceMediaDocomo3()));
		$record->setActionPriceMediaSoftbank3($this->mysqli->real_escape_string($record->getActionPriceMediaSoftbank3()));
		$record->setActionPriceMediaAu3($this->mysqli->real_escape_string($record->getActionPriceMediaAu3()));
		$record->setActionPriceMediaPc3($this->mysqli->real_escape_string($record->getActionPriceMediaPc3()));
		$record->setActionPriceMediaDocomo4($this->mysqli->real_escape_string($record->getActionPriceMediaDocomo4()));
		$record->setActionPriceMediaSoftbank4($this->mysqli->real_escape_string($record->getActionPriceMediaSoftbank4()));
		$record->setActionPriceMediaAu4($this->mysqli->real_escape_string($record->getActionPriceMediaAu4()));
		$record->setActionPriceMediaPc4($this->mysqli->real_escape_string($record->getActionPriceMediaPc4()));
		$record->setActionPriceMediaDocomo5($this->mysqli->real_escape_string($record->getActionPriceMediaDocomo5()));
		$record->setActionPriceMediaSoftbank5($this->mysqli->real_escape_string($record->getActionPriceMediaSoftbank5()));
		$record->setActionPriceMediaAu5($this->mysqli->real_escape_string($record->getActionPriceMediaAu5()));
		$record->setActionPriceMediaPc5($this->mysqli->real_escape_string($record->getActionPriceMediaPc5()));
		$record->setMsText1($this->mysqli->real_escape_string($record->getMsText1()));
		$record->setMsEmail1($this->mysqli->real_escape_string($record->getMsEmail1()));
		$record->setMsImageType1($this->mysqli->real_escape_string($record->getMsImageType1()));
		$record->setMsImageUrl1($this->mysqli->real_escape_string($record->getMsImageUrl1()));
		$record->setMsText2($this->mysqli->real_escape_string($record->getMsText2()));
		$record->setMsEmail2($this->mysqli->real_escape_string($record->getMsEmail2()));
		$record->setMsImageType2($this->mysqli->real_escape_string($record->getMsImageType2()));
		$record->setMsImageUrl2($this->mysqli->real_escape_string($record->getMsImageUrl2()));
		$record->setMsText3($this->mysqli->real_escape_string($record->getMsText3()));
		$record->setMsEmail3($this->mysqli->real_escape_string($record->getMsEmail3()));
		$record->setMsImageType3($this->mysqli->real_escape_string($record->getMsImageType3()));
		$record->setMsImageUrl3($this->mysqli->real_escape_string($record->getMsImageUrl3()));
		$record->setMsText4($this->mysqli->real_escape_string($record->getMsText4()));
		$record->setMsEmail4($this->mysqli->real_escape_string($record->getMsEmail4()));
		$record->setMsImageType4($this->mysqli->real_escape_string($record->getMsImageType4()));
		$record->setMsImageUrl4($this->mysqli->real_escape_string($record->getMsImageUrl4()));
		$record->setMsText5($this->mysqli->real_escape_string($record->getMsText5()));
		$record->setMsEmail5($this->mysqli->real_escape_string($record->getMsEmail5()));
		$record->setMsImageType5($this->mysqli->real_escape_string($record->getMsImageType5()));
		$record->setMsImageUrl5($this->mysqli->real_escape_string($record->getMsImageUrl5()));
		$record->setUniqueClickType($this->mysqli->real_escape_string($record->getUniqueClickType()));
		$record->setPointBackFlag($this->mysqli->real_escape_string($record->getPointBackFlag()));
		$record->setAdultFlag($this->mysqli->real_escape_string($record->getAdultFlag()));
		$record->setDatingFlag($this->mysqli->real_escape_string($record->getDatingFlag()));
		$record->setAdvertStartDate($this->mysqli->real_escape_string($record->getAdvertStartDate()));
		$record->setAdvertEndDate($this->mysqli->real_escape_string($record->getAdvertEndDate()));
		$record->setUnrestraintFlag($this->mysqli->real_escape_string($record->getUnrestraintFlag()));
		$record->setTestFlag($this->mysqli->real_escape_string($record->getTestFlag()));
		$record->setStatus($this->mysqli->real_escape_string($record->getStatus()));
		$record->setStatus($this->mysqli->real_escape_string($record->getStatus()));
		$record->setCreatedAt($this->mysqli->real_escape_string($record->getCreatedAt()));
		$record->setUpdatedAt($this->mysqli->real_escape_string($record->getUpdatedAt()));
		$record->setDeletedAt($this->mysqli->real_escape_string($record->getDeletedAt()));
		return $record;
	}
}
?>