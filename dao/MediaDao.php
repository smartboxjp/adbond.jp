<?php
class MediaDao extends CommonDao{

	//コンストラクタ
	function __construct(){
		parent::__construct();
	}

	//デストラクタ
	function __destruct(){
		parent::__destruct();
	}

	//全データの取得
	public function getAllMedia(){
		is_null($this->mysqli) and $this->connect();
		$result = $this->mysqli->query(" SELECT * FROM media WHERE deleted_at is NULL ");

		$record_array = array();

		while($row = $result->fetch_array(MYSQLI_ASSOC)){
			$record = new Media();
			$record->setId($row["id"]);
			$record->setMediaPublisherId($row["media_publisher_id"]);
			$record->setMediaCategoryId($row["media_category_id"]);
			$record->setMediaName($row["media_name"]);
			$record->setSupportDocomo($row["support_docomo"]);
			$record->setSupportSoftbank($row["support_softbank"]);
			$record->setSupportAu($row["support_au"]);
			$record->setSupportPc($row["support_pc"]);
			$record->setSiteUrlDocomo($row["site_url_docomo"]);
			$record->setSiteUrlSoftbank($row["site_url_softbank"]);
			$record->setSiteUrlAu($row["site_url_au"]);
			$record->setSiteUrlPc($row["site_url_pc"]);
			$record->setMediaType($row["media_type"]);
			$record->setPageViewDay($row["page_view_day"]);
			$record->setSiteOutline($row["site_outline"]);
			$record->setResponseType($row["response_type"]);
			$record->setPointBackUrl($row["point_back_url"]);
			$record->setPointTestUrl($row["point_test_url"]);
			$record->setTestFlag($row["test_flag"]);
			$record->setStatus($row["status"]);
			$record->setCreatedAt($row["created_at"]);
			$record->setUpdatedAt($row["updated_at"]);
			$record_array[] = $record;
		}
		$result->close();
		return $record_array;
	}

	//指定された複数データの取得
	public function getMoreMedia($sql){
		is_null($this->mysqli) and $this->connect();
		$result = $this->mysqli->query($sql);

		$record_array = array();

		while($row = $result->fetch_array(MYSQLI_ASSOC)){
			$record = new Media();
			$record->setId($row["id"]);
			$record->setMediaPublisherId($row["media_publisher_id"]);
			$record->setMediaCategoryId($row["media_category_id"]);
			$record->setMediaName($row["media_name"]);
			$record->setSupportDocomo($row["support_docomo"]);
			$record->setSupportSoftbank($row["support_softbank"]);
			$record->setSupportAu($row["support_au"]);
			$record->setSupportPc($row["support_pc"]);
			$record->setSiteUrlDocomo($row["site_url_docomo"]);
			$record->setSiteUrlSoftbank($row["site_url_softbank"]);
			$record->setSiteUrlAu($row["site_url_au"]);
			$record->setSiteUrlPc($row["site_url_pc"]);
			$record->setMediaType($row["media_type"]);
			$record->setPageViewDay($row["page_view_day"]);
			$record->setSiteOutline($row["site_outline"]);
			$record->setResponseType($row["response_type"]);
			$record->setPointBackUrl($row["point_back_url"]);
			$record->setPointTestUrl($row["point_test_url"]);
			$record->setTestFlag($row["test_flag"]);
			$record->setStatus($row["status"]);
			$record->setCreatedAt($row["created_at"]);
			$record->setUpdatedAt($row["updated_at"]);
			$record_array[] = $record;
		}
		$result->close();
		return $record_array;
	}



	//指定されたデータを1件取得
	private function getMedia($sql){
		is_null($this->mysqli) and $this->connect();
		$result = $this->mysqli->query($sql);

		$record = null;

		if($result->num_rows != 0){
			$row = $result->fetch_array(MYSQLI_ASSOC);
			$record = new Media();
			$record->setId($row["id"]);
			$record->setMediaPublisherId($row["media_publisher_id"]);
			$record->setMediaCategoryId($row["media_category_id"]);
			$record->setMediaName($row["media_name"]);
			$record->setSupportDocomo($row["support_docomo"]);
			$record->setSupportSoftbank($row["support_softbank"]);
			$record->setSupportAu($row["support_au"]);
			$record->setSupportPc($row["support_pc"]);
			$record->setSiteUrlDocomo($row["site_url_docomo"]);
			$record->setSiteUrlSoftbank($row["site_url_softbank"]);
			$record->setSiteUrlAu($row["site_url_au"]);
			$record->setSiteUrlPc($row["site_url_pc"]);
			$record->setMediaType($row["media_type"]);
			$record->setPageViewDay($row["page_view_day"]);
			$record->setSiteOutline($row["site_outline"]);
			$record->setResponseType($row["response_type"]);
			$record->setPointBackUrl($row["point_back_url"]);
			$record->setPointTestUrl($row["point_test_url"]);
			$record->setTestFlag($row["test_flag"]);
			$record->setStatus($row["status"]);
			$record->setCreatedAt($row["created_at"]);
			$record->setUpdatedAt($row["updated_at"]);
		}
		$result->close();
		return $record;
	}

	//idでデータを取得
	public function getMediaById($id){
		$id = $this->mysqli->real_escape_string($id);
		$sql = " SELECT * FROM media WHERE id = '$id' AND deleted_at is NULL ";
		return $this->getMedia($sql);
	}

	//id statusでデータを取得
	public function getMediaByIdStatus($id, $status){
		$id = $this->mysqli->real_escape_string($id);
		$status = $this->mysqli->real_escape_string($status);
		$sql = " SELECT * FROM media WHERE id = '$id' AND status = '$status' AND deleted_at is NULL ";
		return $this->getMedia($sql);
	}

	//site_nameでデータを取得
	public function getMediaBySiteName($site_name){
		$site_name = $this->mysqli->real_escape_string($site_name);
		$sql = " SELECT * FROM media WHERE site_name = '$site_name' AND deleted_at is NULL ";
		return $this->getMediaPublisher($sql);
	}

	//PublisherId,CategoryIdでデータを取得
	public function getMediaByPublisherCategoryId($media_publisher_id, $media_category_id){
		$media_publisher_id = $this->mysqli->real_escape_string($media_publisher_id);
		$media_category_id = $this->mysqli->real_escape_string($media_category_id);
		$sql = " SELECT * FROM media WHERE media_publisher_id = '$media_publisher_id' AND media_category_id = '$media_category_id' AND deleted_at is NULL ";
		return $this->getMoreMedia($sql);
	}

	//PublisherIdでデータを取得
	public function getMediaByPublisherId($media_publisher_id){
		$media_publisher_id = $this->mysqli->real_escape_string($media_publisher_id);
		$sql = " SELECT * FROM media WHERE media_publisher_id = '$media_publisher_id' AND deleted_at is NULL ";
		return $this->getMoreMedia($sql);
	}



	//データの更新
	public function updateMedia($record, &$result_message = ""){
		$record = $this->escapeStringMedia($record);
		is_null($this->mysqli) and $this->connect();
		$sql = " UPDATE media SET "
			. " media_publisher_id = '" . $record->getMediaPublisherId() . "', "
			. " media_category_id = '" . $record->getMediaCategoryId() . "', "
			. " media_name = '" . $record->getMediaName() . "', "
			. " support_docomo = '" . $record->getSupportDocomo() . "', "
			. " support_softbank = '" . $record->getSupportSoftbank() . "', "
			. " support_au = '" . $record->getSupportAu() . "', "
			. " support_pc = '" . $record->getSupportPc() . "', "
			. " site_url_docomo = '" . $record->getSiteUrlDocomo() . "', "
			. " site_url_softbank = '" . $record->getSiteUrlSoftbank() . "', "
			. " site_url_au = '" . $record->getSiteUrlAu() . "', "
			. " site_url_pc = '" . $record->getSiteUrlPc() . "', "
			. " media_type = '" . $record->getMediaType() . "', "
			. " page_view_day = '" . $record->getPageViewDay() . "', "
			. " site_outline = '" . $record->getSiteOutline() . "', "
			. " response_type = '" . $record->getResponseType() . "', "
			. " point_back_url = '" . $record->getPointBackUrl() . "', "
			. " point_test_url = '" . $record->getPointTestUrl() . "', "
			. " test_flag = '" . $record->getTestFlag() . "', "
			. " status = '" . $record->getStatus() . "' "
			. " WHERE id = '" . $record->getId() . "' AND deleted_at is NULL ";
		if(!$this->mysqli->query($sql)) {
			$result_message = "更新に失敗しました" . $this->mysqli->error . "\n";
			return false;
		}
		$result_message = "更新しました";
		return true;
	}

	//新規データの登録
	public function insertMedia($record, &$result_message = ""){
		$record = $this->escapeStringMedia($record);
		is_null($this->mysqli) and $this->connect();
		//idカラムに空を挿入することで，auto_incrementによるカウントアップ値
		//を代入できる
		$sql = " INSERT INTO media values ('', "
			. " '" . $record->getMediaPublisherId() . "', "
			. " '" . $record->getMediaCategoryId() . "', "
			. " '" . $record->getMediaName() . "', "
			. " '" . $record->getSupportDocomo() . "', "
			. " '" . $record->getSupportSoftbank() . "', "
			. " '" . $record->getSupportAu() . "', "
			. " '" . $record->getSupportPc() . "', "
			. " '" . $record->getSiteUrlDocomo() . "', "
			. " '" . $record->getSiteUrlSoftbank() . "', "
			. " '" . $record->getSiteUrlAu() . "', "
			. " '" . $record->getSiteUrlPc() . "', "
			. " '" . $record->getMediaType() . "', "
			. " '" . $record->getPageViewDay() . "', "
			. " '" . $record->getSiteOutline() . "', "
			. " '" . $record->getResponseType() . "', "
			. " '" . $record->getPointBackUrl() . "', "
			. " '" . $record->getPointTestUrl() . "', "
			. " '" . $record->getTestFlag() . "', "
			. " '" . $record->getStatus() . "', "
			. " Now(), Now(), NULL) ";
		if(!$this->mysqli->query($sql)){
			$result_message = "登録に失敗しました" . $this->mysqli->error . "\n";
			return false;
		}
		$result_message = "一件追加しました";
		return true;
	}

	//データの削除
	public function deleteMedia($id, &$result_message = ""){
		$id = $this->mysqli->real_escape_string($id);
		is_null($this->mysqli) and $this->connect();

		$sql = "UPDATE media SET deleted_at = Now() "
			. " WHERE id = '$id' and deleted_at is NULL ";
		if(!$this->mysqli->query($sql)){
			$result_message = "削除に失敗しました" . $this->mysqli->error . "\n";
			return false;
		}
		$result_message = "削除しました";
		return true;
	}

	//データのステータス更新 正規/終了
	public function statusUpdateMedia($id, $status, $result_message = ""){
		$id = $this->mysqli->real_escape_string($id);
		$status = $this->mysqli->real_escape_string($status);
		is_null($this->mysqli) and $this->connect();

		$sql = "UPDATE media SET status = '$status' "
			. " WHERE id = '$id' and deleted_at is NULL ";
		if(!$this->mysqli->query($sql)){
			$result_message = "削除に失敗しました" . $this->mysqli->error . "\n";
			return false;
		}
		$result_message = "削除しました";
		return true;
	}


	//LoginUserクラスの各プロパティから特殊文字をエスケープ
	private function escapeStringMedia($record){
		$record->setId($this->mysqli->real_escape_string($record->getId()));
		$record->setMediaPublisherId($this->mysqli->real_escape_string($record->getMediaPublisherId()));
		$record->setMediaCategoryId($this->mysqli->real_escape_string($record->getMediaCategoryId()));
		$record->setMediaName($this->mysqli->real_escape_string($record->getMediaName()));
		$record->setSupportDocomo($this->mysqli->real_escape_string($record->getSupportDocomo()));
		$record->setSupportSoftbank($this->mysqli->real_escape_string($record->getSupportSoftbank()));
		$record->setSupportAu($this->mysqli->real_escape_string($record->getSupportAu()));
		$record->setSupportPc($this->mysqli->real_escape_string($record->getSupportPc()));
		$record->setSiteUrlDocomo($this->mysqli->real_escape_string($record->getSiteUrlDocomo()));
		$record->setSiteUrlSoftbank($this->mysqli->real_escape_string($record->getSiteUrlSoftbank()));
		$record->setSiteUrlAu($this->mysqli->real_escape_string($record->getSiteUrlAu()));
		$record->setSiteUrlPc($this->mysqli->real_escape_string($record->getSiteUrlPc()));
		$record->setMediaType($this->mysqli->real_escape_string($record->getMediaType()));
		$record->setPageViewDay($this->mysqli->real_escape_string($record->getPageViewDay()));
		$record->setSiteOutline($this->mysqli->real_escape_string($record->getSiteOutline()));
		$record->setResponseType($this->mysqli->real_escape_string($record->getResponseType()));
		$record->setPointBackUrl($this->mysqli->real_escape_string($record->getPointBackUrl()));
		$record->setPointTestUrl($this->mysqli->real_escape_string($record->getPointTestUrl()));
		$record->setTestFlag($this->mysqli->real_escape_string($record->getTestFlag()));
		$record->setStatus($this->mysqli->real_escape_string($record->getStatus()));
		$record->setCreatedAt($this->mysqli->real_escape_string($record->getCreatedAt()));
		$record->setUpdatedAt($this->mysqli->real_escape_string($record->getUpdatedAt()));
		$record->setDeletedAt($this->mysqli->real_escape_string($record->getDeletedAt()));
		return $record;
	}
}
?>