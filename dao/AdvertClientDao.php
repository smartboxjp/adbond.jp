<?php
class AdvertClientDao extends CommonDao{

	//コンストラクタ
	function __construct(){
		parent::__construct();
	}

	//デストラクタ
	function __destruct(){
		parent::__destruct();
	}

	//全データの取得
	public function getAllAdvertClient(){
		is_null($this->mysqli) and $this->connect();
		$result = $this->mysqli->query(" SELECT * FROM advert_clients WHERE deleted_at is NULL ");

		$record_array = array();

		while($row = $result->fetch_array(MYSQLI_ASSOC)){
			$record = new AdvertClient();
			$record->setId($row["id"]);
			$record->setLoginUserId($row["login_user_id"]);
			$record->setAdvertGroupId($row["advert_group_id"]);
			$record->setClientName($row["client_name"]);
			$record->setContactPerson($row["contact_person"]);
			$record->setTel($row["tel"]);
			$record->setFax($row["fax"]);
			$record->setEmail($row["email"]);
			$record->setZipcode1($row["zipcode1"]);
			$record->setZipcode2($row["zipcode2"]);
			$record->setPref($row["pref"]);
			$record->setAddress1($row["address1"]);
			$record->setAddress2($row["address2"]);
			$record->setStatus($row["status"]);
			$record->setCreatedAt($row["created_at"]);
			$record->setUpdatedAt($row["updated_at"]);
			$record_array[] = $record;
		}
		$result->close();
		return $record_array;
	}

	//指定されたデータの取得
	private function getAdvertClient($sql){
		is_null($this->mysqli) and $this->connect();
		$result = $this->mysqli->query($sql);

		$record = null;

		if($result->num_rows != 0){
			$row = $result->fetch_array(MYSQLI_ASSOC);
			$record = new AdvertClient();
			$record->setId($row["id"]);
			$record->setLoginUserId($row["login_user_id"]);
			$record->setAdvertGroupId($row["advert_group_id"]);
			$record->setClientName($row["client_name"]);
			$record->setContactPerson($row["contact_person"]);
			$record->setTel($row["tel"]);
			$record->setFax($row["fax"]);
			$record->setEmail($row["email"]);
			$record->setZipcode1($row["zipcode1"]);
			$record->setZipcode2($row["zipcode2"]);
			$record->setPref($row["pref"]);
			$record->setAddress1($row["address1"]);
			$record->setAddress2($row["address2"]);
			$record->setStatus($row["status"]);
			$record->setCreatedAt($row["created_at"]);
			$record->setUpdatedAt($row["updated_at"]);
		}
		$result->close();
		return $record;
	}

	//idでデータを取得
	public function getAdvertClientById($id){
		$id = $this->mysqli->real_escape_string($id);
		$sql = " SELECT * FROM advert_clients WHERE id = '$id' AND deleted_at is NULL ";
		return $this->getAdvertClient($sql);
	}

	//company_nameでデータを取得
	public function getAdvertClientByCompanyName($company_name){
		$company_name = $this->mysqli->real_escape_string($company_name);
		$sql = " SELECT * FROM advert_clients WHERE company_name = '$company_name' AND deleted_at is NULL ";
		return $this->getAdvertClient($sql);
	}

	//login_user_idでデータを取得
	public function getAdvertClientByLoginUserId($login_user_id){
		$login_user_id = $this->mysqli->real_escape_string($login_user_id);
		$sql = " SELECT * FROM advert_clients WHERE login_user_id = '$login_user_id' AND deleted_at is NULL ";
		return $this->getAdvertClient($sql);
	}

	//データの更新
	public function updateAdvertClient($record, &$result_message = ""){
		$record = $this->escapeStringAdvertClient($record);
		is_null($this->mysqli) and $this->connect();
		$sql = " UPDATE advert_clients SET "
			. " login_user_id = '" . $record->getLoginUserId() . "', "
			. " advert_group_id = '" . $record->getAdvertGroupId() . "', "
			. " client_name = '" . $record->getClientName() . "', "
			. " contact_person = '" . $record->getContactPerson() . "', "
			. " tel = '" . $record->getTel() . "', "
			. " fax = '" . $record->getFax() . "', "
			. " email = '" . $record->getEmail() . "', "
			. " zipcode1 = '" . $record->getZipcode1() . "', "
			. " zipcode2 = '" . $record->getZipcode2() . "', "
			. " pref = '" . $record->getPref() . "', "
			. " address1 = '" . $record->getAddress1() . "', "
			. " address2 = '" . $record->getAddress2() . "', "
			. " status = '" . $record->getStatus() . "' "
			. " WHERE id = '" . $record->getId() . "' AND deleted_at is NULL ";
		if(!$this->mysqli->query($sql)) {
			$result_message = "更新に失敗しました" . $this->mysqli->error . "\n";
			return false;
		}
		$result_message = "更新しました";
		return true;
	}

	//新規データの登録
	public function insertAdvertClient($record, &$result_message = ""){
		$record = $this->escapeStringAdvertClient($record);
		is_null($this->mysqli) and $this->connect();
		//idカラムに空を挿入することで，auto_incrementによるカウントアップ値
		//を代入できる
		$sql = " INSERT INTO advert_clients values ('', "
			. " '" . $record->getLoginUserId() . "', "
			. " '" . $record->getAdvertGroupId() . "', "
			. " '" . $record->getClientName() . "', "
			. " '" . $record->getContactPerson() . "', "
			. " '" . $record->getTel() . "', "
			. " '" . $record->getFax() . "', "
			. " '" . $record->getEmail() . "', "
			. " '" . $record->getZipcode1() . "', "
			. " '" . $record->getZipcode2() . "', "
			. " '" . $record->getPref() . "', "
			. " '" . $record->getAddress1() . "', "
			. " '" . $record->getAddress2() . "', "
			. " '" . $record->getStatus() . "', "
			. " Now(), Now(), NULL) ";
		if(!$this->mysqli->query($sql)){
			$result_message = "登録に失敗しました" . $this->mysqli->error . "\n";
			return false;
		}
		$result_message = "一件追加しました";
		return true;
	}

	//データの削除
	public function deleteAdvertClient($id, &$result_message = ""){
		$id = $this->mysqli->real_escape_string($id);
		is_null($this->mysqli) and $this->connect();

		$sql = "UPDATE advert_clients SET deleted_at = Now() "
			. " WHERE id = '$id' and deleted_at is NULL ";
		if(!$this->mysqli->query($sql)){
			$result_message = "削除に失敗しました" . $this->mysqli->error . "\n";
			return false;
		}
		$result_message = "削除しました";
		return true;
	}

	//LoginUserクラスの各プロパティから特殊文字をエスケープ
	private function escapeStringAdvertClient($record){
		$record->setId($this->mysqli->real_escape_string($record->getId()));
		$record->setLoginUserId($this->mysqli->real_escape_string($record->getLoginUserId()));
		$record->setAdvertGroupId($this->mysqli->real_escape_string($record->getAdvertGroupId()));
		$record->setClientName($this->mysqli->real_escape_string($record->getClientName()));
		$record->setContactPerson($this->mysqli->real_escape_string($record->getContactPerson()));
		$record->setTel($this->mysqli->real_escape_string($record->getTel()));
		$record->setFax($this->mysqli->real_escape_string($record->getFax()));
		$record->setEmail($this->mysqli->real_escape_string($record->getEmail()));
		$record->setZipcode1($this->mysqli->real_escape_string($record->getZipcode1()));
		$record->setZipcode2($this->mysqli->real_escape_string($record->getZipcode2()));
		$record->setPref($this->mysqli->real_escape_string($record->getPref()));
		$record->setAddress1($this->mysqli->real_escape_string($record->getAddress1()));
		$record->setAddress2($this->mysqli->real_escape_string($record->getAddress2()));
		$record->setStatus($this->mysqli->real_escape_string($record->getStatus()));
		$record->setCreatedAt($this->mysqli->real_escape_string($record->getCreatedAt()));
		$record->setUpdatedAt($this->mysqli->real_escape_string($record->getUpdatedAt()));
		$record->setDeletedAt($this->mysqli->real_escape_string($record->getDeletedAt()));
		return $record;
	}
}
?>