<?php
class ConnectLogDao extends CommonDao{

	//コンストラクタ
	function __construct(){
		parent::__construct();
	}

	//デストラクタ
	function __destruct(){
		parent::__destruct();
	}

	//全データの取得
	public function getAllConnectLog(){
		is_null($this->mysqli) and $this->connect();
		$result = $this->mysqli->query(" SELECT * FROM connect_logs WHERE deleted_at is NULL ");

		$record_array = array();

		while($row = $result->fetch_array(MYSQLI_ASSOC)){
			$record = new ConnectLog();
			$record->setId($row["id"]);
			$record->setMediaId($row["media_id"]);
			$record->setAdvertId($row["advert_id"]);
			$record->setStatus($row["status"]);
			$record->setCreatedAt($row["created_at"]);
			$record->setUpdatedAt($row["updated_at"]);
			$record_array[] = $record;
		}
		$result->close();
		return $record_array;
	}

	//指定されたデータの取得
	private function getConnectLog($sql){
		is_null($this->mysqli) and $this->connect();
		$result = $this->mysqli->query($sql);

		$record = null;

		if($result->num_rows != 0){
			$row = $result->fetch_array(MYSQLI_ASSOC);
			$record = new ConnectLog();
			$record->setId($row["id"]);
			$record->setMediaId($row["media_id"]);
			$record->setAdvertId($row["advert_id"]);
			$record->setStatus($row["status"]);
			$record->setCreatedAt($row["created_at"]);
			$record->setUpdatedAt($row["updated_at"]);
		}
		$result->close();
		return $record;
	}

	//idでデータを取得
	public function getConnectLogById($id){
		$id = $this->mysqli->real_escape_string($id);
		$sql = " SELECT * FROM connect_logs WHERE id = '$id' AND deleted_at is NULL ";
		return $this->getConnectLog($sql);
	}

	//session_idでデータを取得
	public function getConnectLogBySessionId($session_id){
		$session_id = $this->mysqli->real_escape_string($session_id);
		$sql = " SELECT * FROM connect_logs WHERE session_id = '$session_id' AND deleted_at is NULL ";
		return $this->getConnectLog($sql);
	}

	//media_id advert_idでデータを取得
	public function getConnectLogByMidAid($media_id, $advert_id){
		$media_id = $this->mysqli->real_escape_string($media_id);
		$advert_id = $this->mysqli->real_escape_string($advert_id);
		$sql = " SELECT * FROM connect_logs WHERE media_id = '$media_id' AND advert_id = '$advert_id' AND deleted_at is NULL ";
		return $this->getConnectLog($sql);
	}

	//media_id advert_id statusでデータを取得
	public function getConnectLogByMidAidStatus($media_id, $advert_id, $status){
		$media_id = $this->mysqli->real_escape_string($media_id);
		$advert_id = $this->mysqli->real_escape_string($advert_id);
		$status = $this->mysqli->real_escape_string($status);
		$sql = " SELECT * FROM connect_logs WHERE media_id = '$media_id' AND advert_id = '$advert_id' AND status = '$status' AND deleted_at is NULL ";
		return $this->getConnectLog($sql);
	}


	//データの更新
	public function updateConnectLog($record, &$result_message = ""){
		$record = $this->escapeStringConnectLog($record);
		is_null($this->mysqli) and $this->connect();
		$sql = " UPDATE connect_logs SET "
			. " media_id = '" . $record->getMediaId() . "', "
			. " advert_id = '" . $record->getAdvertId() . "', "
			. " status = '" . $record->getStatus() . "' "
			. " WHERE id = '" . $record->getId() . "' AND deleted_at is NULL ";
		if(!$this->mysqli->query($sql)) {
			$result_message = "更新に失敗しました" . $this->mysqli->error . "\n";
			return false;
		}
		$result_message = "更新しました";
		return true;
	}

	//新規データの登録
	public function insertConnectLog($record, &$result_message = ""){
		$record = $this->escapeStringConnectLog($record);
		is_null($this->mysqli) and $this->connect();
		//idカラムに空を挿入することで，auto_incrementによるカウントアップ値
		//を代入できる
		$sql = " INSERT INTO connect_logs values ('', "
			. " '" . $record->getMediaId() . "', "
			. " '" . $record->getAdvertId() . "', "
			. " '" . $record->getStatus() . "', "
			. " Now(), Now(), NULL) ";
		if(!$this->mysqli->query($sql)){
			$result_message = "登録に失敗しました" . $this->mysqli->error . "\n";
			return false;
		}
		$result_message = "一件追加しました";
		return true;
	}

	//データの削除
	public function deleteConnectLog($id, &$result_message = ""){
		$id = $this->mysqli->real_escape_string($id);
		is_null($this->mysqli) and $this->connect();

		$sql = "UPDATE connect_logs SET deleted_at = Now() "
			. " WHERE id = '$id' and deleted_at is NULL ";
		if(!$this->mysqli->query($sql)){
			$result_message = "削除に失敗しました" . $this->mysqli->error . "\n";
			return false;
		}
		$result_message = "削除しました";
		return true;
	}

	//LoginUserクラスの各プロパティから特殊文字をエスケープ
	private function escapeStringConnectLog($record){
		$record->setId($this->mysqli->real_escape_string($record->getId()));
		$record->setMediaId($this->mysqli->real_escape_string($record->getMediaId()));
		$record->setAdvertId($this->mysqli->real_escape_string($record->getAdvertId()));
		$record->setStatus($this->mysqli->real_escape_string($record->getStatus()));
		$record->setCreatedAt($this->mysqli->real_escape_string($record->getCreatedAt()));
		$record->setUpdatedAt($this->mysqli->real_escape_string($record->getUpdatedAt()));
		$record->setDeletedAt($this->mysqli->real_escape_string($record->getDeletedAt()));
		return $record;
	}
}
?>