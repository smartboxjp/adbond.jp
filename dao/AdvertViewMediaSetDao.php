<?php
class AdvertViewMediaSetDao extends CommonDao{

	//コンストラクタ
	function __construct(){
		parent::__construct();
	}

	//デストラクタ
	function __destruct(){
		parent::__destruct();
	}

	//全データの取得
	public function getAllAdvertViewMediaSet(){
		is_null($this->mysqli) and $this->connect();
		$result = $this->mysqli->query(" SELECT * FROM advert_view_media_sets WHERE deleted_at is NULL ");

		$record_array = array();

		while($row = $result->fetch_array(MYSQLI_ASSOC)){
			$record = new AdvertViewMediaSet();
			$record->setId($row["id"]);
			$record->setAdvertId($row["advert_id"]);
			$record->setMediaId($row["media_id"]);
			$record->setMediaPublisherId($row["media_publisher_id"]);
			$record->setMediaCategoryId($row["media_category_id"]);
			$record->setCreatedAt($row["created_at"]);
			$record->setUpdatedAt($row["updated_at"]);
			$record_array[] = $record;
		}
		$result->close();
		return $record_array;
	}

	//指定されたデータの取得
	private function getAdvertViewMediaSet($sql){
		is_null($this->mysqli) and $this->connect();
		$result = $this->mysqli->query($sql);

		$record = null;

		if($result->num_rows != 0){
			$row = $result->fetch_array(MYSQLI_ASSOC);
			$record = new AdvertViewMediaSet();
			$record->setId($row["id"]);
			$record->setAdvertId($row["advert_id"]);
			$record->setMediaId($row["media_id"]);
			$record->setMediaPublisherId($row["media_publisher_id"]);
			$record->setMediaCategoryId($row["media_category_id"]);
			$record->setCreatedAt($row["created_at"]);
			$record->setUpdatedAt($row["updated_at"]);
		}
		$result->close();
		return $record;
	}

	//idでデータを取得
	public function getAdvertViewMediaSetById($id){
		$id = $this->mysqli->real_escape_string($id);
		$sql = " SELECT * FROM advert_view_media_sets WHERE id = '$id' AND deleted_at is NULL ";
		return $this->getAdvertViewMediaSet($sql);
	}

	//session_idでデータを取得
	public function getAdvertViewMediaSetBySessionId($session_id){
		$session_id = $this->mysqli->real_escape_string($session_id);
		$sql = " SELECT * FROM advert_view_media_sets WHERE session_id = '$session_id' AND deleted_at is NULL ";
		return $this->getAdvertViewMediaSet($sql);
	}

	//advert_idでデータを取得
	public function getAdvertViewMediaSetByAdvertId($advert_id){
		$session_id = $this->mysqli->real_escape_string($advert_id);
		$sql = " SELECT * FROM advert_view_media_sets WHERE advert_id = '$advert_id' AND deleted_at is NULL ";
		return $this->getAdvertViewMediaSet($sql);
	}

	//advert_id, media_publisher_idでデータを取得
	public function getAdvertViewMediaSetByMidAid($advert_id, $media_id, $media_publisher_id, $media_category_id){
		$advert_id = $this->mysqli->real_escape_string($advert_id);
		$media_id = $this->mysqli->real_escape_string($media_id);
		$media_publisher_id = $this->mysqli->real_escape_string($media_publisher_id);
		$media_category_id = $this->mysqli->real_escape_string($media_category_id);
		$sql = " SELECT * FROM advert_view_media_sets WHERE advert_id = '$advert_id' AND media_id = '$media_id' AND media_publisher_id = '$media_publisher_id' AND media_category_id = '$media_category_id' AND deleted_at is NULL ";
		return $this->getAdvertViewMediaSet($sql);
	}

	//advert_id, media_publisher_idでデータを取得
	public function getAdvertViewMediaSetByMidAid2($advert_id, $media_id, $media_publisher_id, $media_category_id){
		$advert_id = $this->mysqli->real_escape_string($advert_id);
		$media_id = $this->mysqli->real_escape_string($media_id);
		$media_publisher_id = $this->mysqli->real_escape_string($media_publisher_id);
		$media_category_id = $this->mysqli->real_escape_string($media_category_id);
		$sql = " SELECT * FROM advert_view_media_sets WHERE advert_id = '$advert_id' AND (media_id = '$media_id' OR media_publisher_id = '$media_publisher_id' OR media_category_id = '$media_category_id') AND deleted_at is NULL ";
		return $this->getAdvertViewMediaSet($sql);
	}

	//データの更新
	public function updateAdvertViewMediaSet($record, &$result_message = ""){
		$record = $this->escapeStringAdvertViewMediaSet($record);
		is_null($this->mysqli) and $this->connect();
		$sql = " UPDATE advert_view_media_sets SET "
			. " advert_id = '" . $record->getAdvertId() . "', "
			. " media_id = '" . $record->getMediaId() . "', "
			. " media_publisher_id = '" . $record->getMediaPublisherId() . "', "
			. " media_category_id = '" . $record->getMediaCategoryId() . "' "
			. " WHERE id = '" . $record->getId() . "' AND deleted_at is NULL ";
		if(!$this->mysqli->query($sql)) {
			$result_message = "更新に失敗しました" . $this->mysqli->error . "\n";
			return false;
		}
		$result_message = "更新しました";
		return true;
	}

	//新規データの登録
	public function insertAdvertViewMediaSet($record, &$result_message = ""){
		$record = $this->escapeStringAdvertViewMediaSet($record);
		is_null($this->mysqli) and $this->connect();
		//idカラムに空を挿入することで，auto_incrementによるカウントアップ値
		//を代入できる
		$sql = " INSERT INTO advert_view_media_sets values ('', "
			. " '" . $record->getAdvertId() . "', "
			. " '" . $record->getMediaId() . "', "
			. " '" . $record->getMediaPublisherId() . "', "
			. " '" . $record->getMediaCategoryId() . "', "
			. " Now(), Now(), NULL) ";
		if(!$this->mysqli->query($sql)){
			$result_message = "登録に失敗しました" . $this->mysqli->error . "\n";
			return false;
		}
		$result_message = "一件追加しました";
		return true;
	}

	//データの削除
	public function deleteAdvertViewMediaSet($id, &$result_message = ""){
		$id = $this->mysqli->real_escape_string($id);
		is_null($this->mysqli) and $this->connect();

		$sql = "UPDATE advert_view_media_sets SET deleted_at = Now() "
			. " WHERE id = '$id' and deleted_at is NULL ";
		if(!$this->mysqli->query($sql)){
			$result_message = "削除に失敗しました" . $this->mysqli->error . "\n";
			return false;
		}
		$result_message = "削除しました";
		return true;
	}

	//LoginUserクラスの各プロパティから特殊文字をエスケープ
	private function escapeStringAdvertViewMediaSet($record){
		$record->setId($this->mysqli->real_escape_string($record->getId()));
		$record->setAdvertId($this->mysqli->real_escape_string($record->getAdvertId()));
		$record->setMediaId($this->mysqli->real_escape_string($record->getMediaId()));
		$record->setMediaPublisherId($this->mysqli->real_escape_string($record->getMediaPublisherId()));
		$record->setMediaCategoryId($this->mysqli->real_escape_string($record->getMediaCategoryId()));
		$record->setCreatedAt($this->mysqli->real_escape_string($record->getCreatedAt()));
		$record->setUpdatedAt($this->mysqli->real_escape_string($record->getUpdatedAt()));
		$record->setDeletedAt($this->mysqli->real_escape_string($record->getDeletedAt()));
		return $record;
	}
}
?>