<?php
class AdvertPriceMediaSetDao extends CommonDao{

	//コンストラクタ
	function __construct(){
		parent::__construct();
	}

	//デストラクタ
	function __destruct(){
		parent::__destruct();
	}

	//全データの取得
	public function getAllAdvertPriceMediaSet(){
		is_null($this->mysqli) and $this->connect();
		$result = $this->mysqli->query(" SELECT * FROM advert_price_media_sets WHERE deleted_at is NULL ");

		$record_array = array();

		while($row = $result->fetch_array(MYSQLI_ASSOC)){
			$record = new AdvertPriceMediaSet();
			$record->setId($row["id"]);
			$record->setAdvertId($row["advert_id"]);
			$record->setMediaId($row["media_id"]);
			$record->setMediaPublisherId($row["media_publisher_id"]);
			$record->setMediaCategoryId($row["media_category_id"]);
			$record->setClickPriceClient($row["click_price_client"]);
			$record->setClickPriceMedia($row["click_price_media"]);
			$record->setActionPriceClientDocomo1($row["action_price_client_docomo_1"]);
			$record->setActionPriceClientSoftbank1($row["action_price_client_softbank_1"]);
			$record->setActionPriceClientAu1($row["action_price_client_au_1"]);
			$record->setActionPriceClientPc1($row["action_price_client_pc_1"]);
			$record->setActionPriceClientDocomo2($row["action_price_client_docomo_2"]);
			$record->setActionPriceClientSoftbank2($row["action_price_client_softbank_2"]);
			$record->setActionPriceClientAu2($row["action_price_client_au_2"]);
			$record->setActionPriceClientPc2($row["action_price_client_pc_2"]);
			$record->setActionPriceClientDocomo3($row["action_price_client_docomo_3"]);
			$record->setActionPriceClientSoftbank3($row["action_price_client_softbank_3"]);
			$record->setActionPriceClientAu3($row["action_price_client_au_3"]);
			$record->setActionPriceClientPc3($row["action_price_client_pc_3"]);
			$record->setActionPriceClientDocomo4($row["action_price_client_docomo_4"]);
			$record->setActionPriceClientSoftbank4($row["action_price_client_softbank_4"]);
			$record->setActionPriceClientAu4($row["action_price_client_au_4"]);
			$record->setActionPriceClientPc4($row["action_price_client_pc_4"]);
			$record->setActionPriceClientDocomo5($row["action_price_client_docomo_5"]);
			$record->setActionPriceClientSoftbank5($row["action_price_client_softbank_5"]);
			$record->setActionPriceClientAu5($row["action_price_client_au_5"]);
			$record->setActionPriceClientPc5($row["action_price_client_pc_5"]);
			$record->setActionPriceMediaDocomo1($row["action_price_media_docomo_1"]);
			$record->setActionPriceMediaSoftbank1($row["action_price_media_softbank_1"]);
			$record->setActionPriceMediaAu1($row["action_price_media_au_1"]);
			$record->setActionPriceMediaPc1($row["action_price_media_pc_1"]);
			$record->setActionPriceMediaDocomo2($row["action_price_media_docomo_2"]);
			$record->setActionPriceMediaSoftbank2($row["action_price_media_softbank_2"]);
			$record->setActionPriceMediaAu2($row["action_price_media_au_2"]);
			$record->setActionPriceMediaPc2($row["action_price_media_pc_2"]);
			$record->setActionPriceMediaDocomo3($row["action_price_media_docomo_3"]);
			$record->setActionPriceMediaSoftbank3($row["action_price_media_softbank_3"]);
			$record->setActionPriceMediaAu3($row["action_price_media_au_3"]);
			$record->setActionPriceMediaPc3($row["action_price_media_pc_3"]);
			$record->setActionPriceMediaDocomo4($row["action_price_media_docomo_4"]);
			$record->setActionPriceMediaSoftbank4($row["action_price_media_softbank_4"]);
			$record->setActionPriceMediaAu4($row["action_price_media_au_4"]);
			$record->setActionPriceMediaPc4($row["action_price_media_pc_4"]);
			$record->setActionPriceMediaDocomo5($row["action_price_media_docomo_5"]);
			$record->setActionPriceMediaSoftbank5($row["action_price_media_softbank_5"]);
			$record->setActionPriceMediaAu5($row["action_price_media_au_5"]);
			$record->setActionPriceMediaPc5($row["action_price_media_pc_5"]);
			$record->setCreatedAt($row["created_at"]);
			$record->setUpdatedAt($row["updated_at"]);
			$record_array[] = $record;
		}
		$result->close();
		return $record_array;
	}

	//指定されたデータの取得
	private function getAdvertPriceMediaSet($sql){
		is_null($this->mysqli) and $this->connect();
		$result = $this->mysqli->query($sql);

		$record = null;

		if($result->num_rows != 0){
			$row = $result->fetch_array(MYSQLI_ASSOC);
			$record = new AdvertPriceMediaSet();
			$record->setId($row["id"]);
			$record->setAdvertId($row["advert_id"]);
			$record->setMediaId($row["media_id"]);
			$record->setMediaPublisherId($row["media_publisher_id"]);
			$record->setMediaCategoryId($row["media_category_id"]);
			$record->setClickPriceClient($row["click_price_client"]);
			$record->setClickPriceMedia($row["click_price_media"]);
			$record->setActionPriceClientDocomo1($row["action_price_client_docomo_1"]);
			$record->setActionPriceClientSoftbank1($row["action_price_client_softbank_1"]);
			$record->setActionPriceClientAu1($row["action_price_client_au_1"]);
			$record->setActionPriceClientPc1($row["action_price_client_pc_1"]);
			$record->setActionPriceClientDocomo2($row["action_price_client_docomo_2"]);
			$record->setActionPriceClientSoftbank2($row["action_price_client_softbank_2"]);
			$record->setActionPriceClientAu2($row["action_price_client_au_2"]);
			$record->setActionPriceClientPc2($row["action_price_client_pc_2"]);
			$record->setActionPriceClientDocomo3($row["action_price_client_docomo_3"]);
			$record->setActionPriceClientSoftbank3($row["action_price_client_softbank_3"]);
			$record->setActionPriceClientAu3($row["action_price_client_au_3"]);
			$record->setActionPriceClientPc3($row["action_price_client_pc_3"]);
			$record->setActionPriceClientDocomo4($row["action_price_client_docomo_4"]);
			$record->setActionPriceClientSoftbank4($row["action_price_client_softbank_4"]);
			$record->setActionPriceClientAu4($row["action_price_client_au_4"]);
			$record->setActionPriceClientPc4($row["action_price_client_pc_4"]);
			$record->setActionPriceClientDocomo5($row["action_price_client_docomo_5"]);
			$record->setActionPriceClientSoftbank5($row["action_price_client_softbank_5"]);
			$record->setActionPriceClientAu5($row["action_price_client_au_5"]);
			$record->setActionPriceClientPc5($row["action_price_client_pc_5"]);
			$record->setActionPriceMediaDocomo1($row["action_price_media_docomo_1"]);
			$record->setActionPriceMediaSoftbank1($row["action_price_media_softbank_1"]);
			$record->setActionPriceMediaAu1($row["action_price_media_au_1"]);
			$record->setActionPriceMediaPc1($row["action_price_media_pc_1"]);
			$record->setActionPriceMediaDocomo2($row["action_price_media_docomo_2"]);
			$record->setActionPriceMediaSoftbank2($row["action_price_media_softbank_2"]);
			$record->setActionPriceMediaAu2($row["action_price_media_au_2"]);
			$record->setActionPriceMediaPc2($row["action_price_media_pc_2"]);
			$record->setActionPriceMediaDocomo3($row["action_price_media_docomo_3"]);
			$record->setActionPriceMediaSoftbank3($row["action_price_media_softbank_3"]);
			$record->setActionPriceMediaAu3($row["action_price_media_au_3"]);
			$record->setActionPriceMediaPc3($row["action_price_media_pc_3"]);
			$record->setActionPriceMediaDocomo4($row["action_price_media_docomo_4"]);
			$record->setActionPriceMediaSoftbank4($row["action_price_media_softbank_4"]);
			$record->setActionPriceMediaAu4($row["action_price_media_au_4"]);
			$record->setActionPriceMediaPc4($row["action_price_media_pc_4"]);
			$record->setActionPriceMediaDocomo5($row["action_price_media_docomo_5"]);
			$record->setActionPriceMediaSoftbank5($row["action_price_media_softbank_5"]);
			$record->setActionPriceMediaAu5($row["action_price_media_au_5"]);
			$record->setActionPriceMediaPc5($row["action_price_media_pc_5"]);
			$record->setCreatedAt($row["created_at"]);
			$record->setUpdatedAt($row["updated_at"]);
		}
		$result->close();
		return $record;
	}

	//idでデータを取得
	public function getAdvertPriceMediaSetById($id){
		$id = $this->mysqli->real_escape_string($id);
		$sql = " SELECT * FROM advert_price_media_sets WHERE id = '$id' AND deleted_at is NULL ";
		return $this->getAdvertPriceMediaSet($sql);
	}

	//session_idでデータを取得
	public function getAdvertPriceMediaSetBySessionId($session_id){
		$session_id = $this->mysqli->real_escape_string($session_id);
		$sql = " SELECT * FROM advert_price_media_sets WHERE session_id = '$session_id' AND deleted_at is NULL ";
		return $this->getAdvertPriceMediaSet($sql);
	}

	//advert_id, media_idでデータを取得
	public function getAdvertPriceMediaSetByMidAid($advert_id, $media_id){
		$advert_id = $this->mysqli->real_escape_string($advert_id);
		$media_id = $this->mysqli->real_escape_string($media_id);
		$sql = " SELECT * FROM advert_price_media_sets WHERE advert_id = '$advert_id' AND media_id = '$media_id' AND deleted_at is NULL ";
		return $this->getAdvertPriceMediaSet($sql);
	}

	//advert_idでデータを取得
	public function getAdvertPriceMediaSetByAid($advert_id){
		$advert_id = $this->mysqli->real_escape_string($advert_id);
		$sql = " SELECT * FROM advert_price_media_sets WHERE advert_id = '$advert_id' AND deleted_at is NULL ";
		return $this->getAdvertPriceMediaSet($sql);
	}


	//IDを指定してデータの更新
	public function updateAdvertPriceMediaSet($record, &$result_message = ""){
		$record = $this->escapeStringAdvertPriceMediaSet($record);
		is_null($this->mysqli) and $this->connect();
		$sql = " UPDATE advert_price_media_sets SET "
			. " advert_id = '" . $record->getAdvertId() . "', "
			. " media_id = '" . $record->getMediaId() . "', "
			. " media_publisher_id = '" . $record->getMediaPublisherId() . "', "
			. " media_category_id = '" . $record->getMediaCategoryId() . "', "
			. " click_price_client = '" . $record->getClickPriceClient() . "', "
			. " click_price_media = '" . $record->getClickPriceMedia() . "', "
			. " action_price_client_docomo_1 = '" . $record->getActionPriceClientDocomo1() . "', "
			. " action_price_client_softbank_1 = '" . $record->getActionPriceClientSoftbank1() . "', "
			. " action_price_client_au_1 = '" . $record->getActionPriceClientAu1() . "', "
			. " action_price_client_pc_1 = '" . $record->getActionPriceClientPc1() . "', "
			. " action_price_client_docomo_2 = '" . $record->getActionPriceClientDocomo2() . "', "
			. " action_price_client_softbank_2 = '" . $record->getActionPriceClientSoftbank2() . "', "
			. " action_price_client_au_2 = '" . $record->getActionPriceClientAu2() . "', "
			. " action_price_client_pc_2 = '" . $record->getActionPriceClientPc2() . "', "
			. " action_price_client_docomo_3 = '" . $record->getActionPriceClientDocomo3() . "', "
			. " action_price_client_softbank_3 = '" . $record->getActionPriceClientSoftbank3() . "', "
			. " action_price_client_au_3 = '" . $record->getActionPriceClientAu3() . "', "
			. " action_price_client_pc_3 = '" . $record->getActionPriceClientPc3() . "', "
			. " action_price_client_docomo_4 = '" . $record->getActionPriceClientDocomo4() . "', "
			. " action_price_client_softbank_4 = '" . $record->getActionPriceClientSoftbank4() . "', "
			. " action_price_client_au_4 = '" . $record->getActionPriceClientAu4() . "', "
			. " action_price_client_pc_4 = '" . $record->getActionPriceClientPc4() . "', "
			. " action_price_client_docomo_5 = '" . $record->getActionPriceClientDocomo5() . "', "
			. " action_price_client_softbank_5 = '" . $record->getActionPriceClientSoftbank5() . "', "
			. " action_price_client_au_5 = '" . $record->getActionPriceClientAu5() . "', "
			. " action_price_client_pc_5 = '" . $record->getActionPriceClientPc5() . "', "
			. " action_price_media_docomo_1 = '" . $record->getActionPriceMediaDocomo1() . "', "
			. " action_price_media_softbank_1 = '" . $record->getActionPriceMediaSoftbank1() . "', "
			. " action_price_media_au_1 = '" . $record->getActionPriceMediaAu1() . "', "
			. " action_price_media_pc_1 = '" . $record->getActionPriceMediaPc1() . "', "
			. " action_price_media_docomo_2 = '" . $record->getActionPriceMediaDocomo2() . "', "
			. " action_price_media_softbank_2 = '" . $record->getActionPriceMediaSoftbank2() . "', "
			. " action_price_media_au_2 = '" . $record->getActionPriceMediaAu2() . "', "
			. " action_price_media_pc_2 = '" . $record->getActionPriceMediaPc2() . "', "
			. " action_price_media_docomo_3 = '" . $record->getActionPriceMediaDocomo3() . "', "
			. " action_price_media_softbank_3 = '" . $record->getActionPriceMediaSoftbank3() . "', "
			. " action_price_media_au_3 = '" . $record->getActionPriceMediaAu3() . "', "
			. " action_price_media_pc_3 = '" . $record->getActionPriceMediaPc3() . "', "
			. " action_price_media_docomo_4 = '" . $record->getActionPriceMediaDocomo4() . "', "
			. " action_price_media_softbank_4 = '" . $record->getActionPriceMediaSoftbank4() . "', "
			. " action_price_media_au_4 = '" . $record->getActionPriceMediaAu4() . "', "
			. " action_price_media_pc_4 = '" . $record->getActionPriceMediaPc4() . "', "
			. " action_price_media_docomo_5 = '" . $record->getActionPriceMediaDocomo5() . "', "
			. " action_price_media_softbank_5 = '" . $record->getActionPriceMediaSoftbank5() . "', "
			. " action_price_media_au_5 = '" . $record->getActionPriceMediaAu5() . "', "
			. " action_price_media_pc_5 = '" . $record->getActionPriceMediaPc5() . "' "
			. " WHERE id = '" . $record->getId() . "' AND deleted_at is NULL ";
		if(!$this->mysqli->query($sql)) {
			$result_message = "更新に失敗しました" . $this->mysqli->error . "\n";
			return false;
		}
		$result_message = "更新しました";
		return true;
	}

	//advert_id,media_idを指定してデータの更新
	public function updateAdvertPriceMediaAdvertMediaSet($record, $advert_id = "", $media_id = ""){
		$record = $this->escapeStringAdvertPriceMediaSet($record);

		is_null($this->mysqli) and $this->connect();
		$sql = " UPDATE advert_price_media_sets SET "
//			. " advert_id = '" . $record->getAdvertId() . "', "
//			. " media_id = '" . $record->getMediaId() . "', "
//			. " media_publisher_id = '" . $record->getMediaPublisherId() . "', "
//			. " media_category_id = '" . $record->getMediaCategoryId() . "', "
			. " click_price_client = '" . $record->getClickPriceClient() . "', "
			. " click_price_media = '" . $record->getClickPriceMedia() . "', "
			. " action_price_client_docomo_1 = '" . $record->getActionPriceClientDocomo1() . "', "
			. " action_price_client_softbank_1 = '" . $record->getActionPriceClientSoftbank1() . "', "
			. " action_price_client_au_1 = '" . $record->getActionPriceClientAu1() . "', "
			. " action_price_client_pc_1 = '" . $record->getActionPriceClientPc1() . "', "
			. " action_price_client_docomo_2 = '" . $record->getActionPriceClientDocomo2() . "', "
			. " action_price_client_softbank_2 = '" . $record->getActionPriceClientSoftbank2() . "', "
			. " action_price_client_au_2 = '" . $record->getActionPriceClientAu2() . "', "
			. " action_price_client_pc_2 = '" . $record->getActionPriceClientPc2() . "', "
			. " action_price_client_docomo_3 = '" . $record->getActionPriceClientDocomo3() . "', "
			. " action_price_client_softbank_3 = '" . $record->getActionPriceClientSoftbank3() . "', "
			. " action_price_client_au_3 = '" . $record->getActionPriceClientAu3() . "', "
			. " action_price_client_pc_3 = '" . $record->getActionPriceClientPc3() . "', "
			. " action_price_client_docomo_4 = '" . $record->getActionPriceClientDocomo4() . "', "
			. " action_price_client_softbank_4 = '" . $record->getActionPriceClientSoftbank4() . "', "
			. " action_price_client_au_4 = '" . $record->getActionPriceClientAu4() . "', "
			. " action_price_client_pc_4 = '" . $record->getActionPriceClientPc4() . "', "
			. " action_price_client_docomo_5 = '" . $record->getActionPriceClientDocomo5() . "', "
			. " action_price_client_softbank_5 = '" . $record->getActionPriceClientSoftbank5() . "', "
			. " action_price_client_au_5 = '" . $record->getActionPriceClientAu5() . "', "
			. " action_price_client_pc_5 = '" . $record->getActionPriceClientPc5() . "', "
			. " action_price_media_docomo_1 = '" . $record->getActionPriceMediaDocomo1() . "', "
			. " action_price_media_softbank_1 = '" . $record->getActionPriceMediaSoftbank1() . "', "
			. " action_price_media_au_1 = '" . $record->getActionPriceMediaAu1() . "', "
			. " action_price_media_pc_1 = '" . $record->getActionPriceMediaPc1() . "', "
			. " action_price_media_docomo_2 = '" . $record->getActionPriceMediaDocomo2() . "', "
			. " action_price_media_softbank_2 = '" . $record->getActionPriceMediaSoftbank2() . "', "
			. " action_price_media_au_2 = '" . $record->getActionPriceMediaAu2() . "', "
			. " action_price_media_pc_2 = '" . $record->getActionPriceMediaPc2() . "', "
			. " action_price_media_docomo_3 = '" . $record->getActionPriceMediaDocomo3() . "', "
			. " action_price_media_softbank_3 = '" . $record->getActionPriceMediaSoftbank3() . "', "
			. " action_price_media_au_3 = '" . $record->getActionPriceMediaAu3() . "', "
			. " action_price_media_pc_3 = '" . $record->getActionPriceMediaPc3() . "', "
			. " action_price_media_docomo_4 = '" . $record->getActionPriceMediaDocomo4() . "', "
			. " action_price_media_softbank_4 = '" . $record->getActionPriceMediaSoftbank4() . "', "
			. " action_price_media_au_4 = '" . $record->getActionPriceMediaAu4() . "', "
			. " action_price_media_pc_4 = '" . $record->getActionPriceMediaPc4() . "', "
			. " action_price_media_docomo_5 = '" . $record->getActionPriceMediaDocomo5() . "', "
			. " action_price_media_softbank_5 = '" . $record->getActionPriceMediaSoftbank5() . "', "
			. " action_price_media_au_5 = '" . $record->getActionPriceMediaAu5() . "', "
			. " action_price_media_pc_5 = '" . $record->getActionPriceMediaPc5() . "' "
			. " WHERE "
			. " advert_id = '" . $advert_id . "' "
			. " AND "
			. " media_id = '" . $media_id . "' "
			. " AND deleted_at is NULL ";

		if(!$this->mysqli->query($sql)) {
			$result_message = "更新に失敗しました" . $this->mysqli->error . "\n";
			return false;
		}
		$result_message = "更新しました";
		return true;
	}


	//新規データの登録
	public function insertAdvertPriceMediaSet($record, &$result_message = ""){
		$record = $this->escapeStringAdvertPriceMediaSet($record);
		is_null($this->mysqli) and $this->connect();
		//idカラムに空を挿入することで，auto_incrementによるカウントアップ値
		//を代入できる
		$sql = " INSERT INTO advert_price_media_sets values ('', "
			. " '" . $record->getAdvertId() . "', "
			. " '" . $record->getMediaId() . "', "
			. " '" . $record->getMediaPublisherId() . "', "
			. " '" . $record->getMediaCategoryId() . "', "
			. " '" . $record->getClickPriceClient() . "', "
			. " '" . $record->getClickPriceMedia() . "', "
			. " '" . $record->getActionPriceClientDocomo1() . "', "
			. " '" . $record->getActionPriceClientSoftbank1() . "', "
			. " '" . $record->getActionPriceClientAu1() . "', "
			. " '" . $record->getActionPriceClientPc1() . "', "
			. " '" . $record->getActionPriceClientDocomo2() . "', "
			. " '" . $record->getActionPriceClientSoftbank2() . "', "
			. " '" . $record->getActionPriceClientAu2() . "', "
			. " '" . $record->getActionPriceClientPc2() . "', "
			. " '" . $record->getActionPriceClientDocomo3() . "', "
			. " '" . $record->getActionPriceClientSoftbank3() . "', "
			. " '" . $record->getActionPriceClientAu3() . "', "
			. " '" . $record->getActionPriceClientPc3() . "', "
			. " '" . $record->getActionPriceClientDocomo4() . "', "
			. " '" . $record->getActionPriceClientSoftbank4() . "', "
			. " '" . $record->getActionPriceClientAu4() . "', "
			. " '" . $record->getActionPriceClientPc4() . "', "
			. " '" . $record->getActionPriceClientDocomo5() . "', "
			. " '" . $record->getActionPriceClientSoftbank5() . "', "
			. " '" . $record->getActionPriceClientAu5() . "', "
			. " '" . $record->getActionPriceClientPc5() . "', "
			. " '" . $record->getActionPriceMediaDocomo1() . "', "
			. " '" . $record->getActionPriceMediaSoftbank1() . "', "
			. " '" . $record->getActionPriceMediaAu1() . "', "
			. " '" . $record->getActionPriceMediaPc1() . "', "
			. " '" . $record->getActionPriceMediaDocomo2() . "', "
			. " '" . $record->getActionPriceMediaSoftbank2() . "', "
			. " '" . $record->getActionPriceMediaAu2() . "', "
			. " '" . $record->getActionPriceMediaPc2() . "', "
			. " '" . $record->getActionPriceMediaDocomo3() . "', "
			. " '" . $record->getActionPriceMediaSoftbank3() . "', "
			. " '" . $record->getActionPriceMediaAu3() . "', "
			. " '" . $record->getActionPriceMediaPc3() . "', "
			. " '" . $record->getActionPriceMediaDocomo4() . "', "
			. " '" . $record->getActionPriceMediaSoftbank4() . "', "
			. " '" . $record->getActionPriceMediaAu4() . "', "
			. " '" . $record->getActionPriceMediaPc4() . "', "
			. " '" . $record->getActionPriceMediaDocomo5() . "', "
			. " '" . $record->getActionPriceMediaSoftbank5() . "', "
			. " '" . $record->getActionPriceMediaAu5() . "', "
			. " '" . $record->getActionPriceMediaPc5() . "', "
			. " Now(), Now(), NULL) ";
		if(!$this->mysqli->query($sql)){
			$result_message = "登録に失敗しました" . $this->mysqli->error . "\n";
			return false;
		}
		$result_message = "一件追加しました";
		return true;
	}

	//データの削除
	public function deleteAdvertPriceMediaSet($id, &$result_message = ""){
		$id = $this->mysqli->real_escape_string($id);
		is_null($this->mysqli) and $this->connect();

		$sql = "UPDATE advert_price_media_sets SET deleted_at = Now() "
			. " WHERE id = '$id' and deleted_at is NULL ";
		if(!$this->mysqli->query($sql)){
			$result_message = "削除に失敗しました" . $this->mysqli->error . "\n";
			return false;
		}
		$result_message = "削除しました";
		return true;
	}

	//LoginUserクラスの各プロパティから特殊文字をエスケープ
	private function escapeStringAdvertPriceMediaSet($record){
		$record->setId($this->mysqli->real_escape_string($record->getId()));
		$record->setAdvertId($this->mysqli->real_escape_string($record->getAdvertId()));
		$record->setMediaId($this->mysqli->real_escape_string($record->getMediaId()));
		$record->setMediaPublisherId($this->mysqli->real_escape_string($record->getMediaPublisherId()));
		$record->setMediaCategoryId($this->mysqli->real_escape_string($record->getMediaCategoryId()));
		$record->setClickPriceClient($this->mysqli->real_escape_string($record->getClickPriceClient()));
		$record->setClickPriceMedia($this->mysqli->real_escape_string($record->getClickPriceMedia()));
		$record->setActionPriceClientDocomo1($this->mysqli->real_escape_string($record->getActionPriceClientDocomo1()));
		$record->setActionPriceClientSoftbank1($this->mysqli->real_escape_string($record->getActionPriceClientSoftbank1()));
		$record->setActionPriceClientAu1($this->mysqli->real_escape_string($record->getActionPriceClientAu1()));
		$record->setActionPriceClientPc1($this->mysqli->real_escape_string($record->getActionPriceClientPc1()));
		$record->setActionPriceClientDocomo2($this->mysqli->real_escape_string($record->getActionPriceClientDocomo2()));
		$record->setActionPriceClientSoftbank2($this->mysqli->real_escape_string($record->getActionPriceClientSoftbank2()));
		$record->setActionPriceClientAu2($this->mysqli->real_escape_string($record->getActionPriceClientAu2()));
		$record->setActionPriceClientPc2($this->mysqli->real_escape_string($record->getActionPriceClientPc2()));
		$record->setActionPriceClientDocomo3($this->mysqli->real_escape_string($record->getActionPriceClientDocomo3()));
		$record->setActionPriceClientSoftbank3($this->mysqli->real_escape_string($record->getActionPriceClientSoftbank3()));
		$record->setActionPriceClientAu3($this->mysqli->real_escape_string($record->getActionPriceClientAu3()));
		$record->setActionPriceClientPc3($this->mysqli->real_escape_string($record->getActionPriceClientPc3()));
		$record->setActionPriceClientDocomo4($this->mysqli->real_escape_string($record->getActionPriceClientDocomo4()));
		$record->setActionPriceClientSoftbank4($this->mysqli->real_escape_string($record->getActionPriceClientSoftbank4()));
		$record->setActionPriceClientAu4($this->mysqli->real_escape_string($record->getActionPriceClientAu4()));
		$record->setActionPriceClientPc4($this->mysqli->real_escape_string($record->getActionPriceClientPc4()));
		$record->setActionPriceClientDocomo5($this->mysqli->real_escape_string($record->getActionPriceClientDocomo5()));
		$record->setActionPriceClientSoftbank5($this->mysqli->real_escape_string($record->getActionPriceClientSoftbank5()));
		$record->setActionPriceClientAu5($this->mysqli->real_escape_string($record->getActionPriceClientAu5()));
		$record->setActionPriceClientPc5($this->mysqli->real_escape_string($record->getActionPriceClientPc5()));
		$record->setActionPriceMediaDocomo1($this->mysqli->real_escape_string($record->getActionPriceMediaDocomo1()));
		$record->setActionPriceMediaSoftbank1($this->mysqli->real_escape_string($record->getActionPriceMediaSoftbank1()));
		$record->setActionPriceMediaAu1($this->mysqli->real_escape_string($record->getActionPriceMediaAu1()));
		$record->setActionPriceMediaPc1($this->mysqli->real_escape_string($record->getActionPriceMediaPc1()));
		$record->setActionPriceMediaDocomo2($this->mysqli->real_escape_string($record->getActionPriceMediaDocomo2()));
		$record->setActionPriceMediaSoftbank2($this->mysqli->real_escape_string($record->getActionPriceMediaSoftbank2()));
		$record->setActionPriceMediaAu2($this->mysqli->real_escape_string($record->getActionPriceMediaAu2()));
		$record->setActionPriceMediaPc2($this->mysqli->real_escape_string($record->getActionPriceMediaPc2()));
		$record->setActionPriceMediaDocomo3($this->mysqli->real_escape_string($record->getActionPriceMediaDocomo3()));
		$record->setActionPriceMediaSoftbank3($this->mysqli->real_escape_string($record->getActionPriceMediaSoftbank3()));
		$record->setActionPriceMediaAu3($this->mysqli->real_escape_string($record->getActionPriceMediaAu3()));
		$record->setActionPriceMediaPc3($this->mysqli->real_escape_string($record->getActionPriceMediaPc3()));
		$record->setActionPriceMediaDocomo4($this->mysqli->real_escape_string($record->getActionPriceMediaDocomo4()));
		$record->setActionPriceMediaSoftbank4($this->mysqli->real_escape_string($record->getActionPriceMediaSoftbank4()));
		$record->setActionPriceMediaAu4($this->mysqli->real_escape_string($record->getActionPriceMediaAu4()));
		$record->setActionPriceMediaPc4($this->mysqli->real_escape_string($record->getActionPriceMediaPc4()));
		$record->setActionPriceMediaDocomo5($this->mysqli->real_escape_string($record->getActionPriceMediaDocomo5()));
		$record->setActionPriceMediaSoftbank5($this->mysqli->real_escape_string($record->getActionPriceMediaSoftbank5()));
		$record->setActionPriceMediaAu5($this->mysqli->real_escape_string($record->getActionPriceMediaAu5()));
		$record->setActionPriceMediaPc5($this->mysqli->real_escape_string($record->getActionPriceMediaPc5()));
		$record->setCreatedAt($this->mysqli->real_escape_string($record->getCreatedAt()));
		$record->setUpdatedAt($this->mysqli->real_escape_string($record->getUpdatedAt()));
		$record->setDeletedAt($this->mysqli->real_escape_string($record->getDeletedAt()));
		return $record;
	}
}
?>