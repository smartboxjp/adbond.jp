<?php
class AdvertAttentionDao extends CommonDao{

	// コンストラクタ
	function __construct(){
		parent::__construct();
	}

	// デストラクタ
	function __destruct(){
		parent::__destruct();
	}

	// *******************************************************************
	// 指定されたデータを1件取得
	// *******************************************************************
	private function getAdvertAttention($sql){
		is_null($this->mysqli) and $this->connect();
		$result = $this->mysqli->query($sql);

		$record = null;

		if($result->num_rows != 0){
			$row = $result->fetch_array(MYSQLI_ASSOC);
			$record = new AdvertAttention();
			$record->setId($row["id"]);
			$record->setAdvertId($row["advert_id"]);
			$record->setMsImageUrlNum($row["ms_image_url_num"]);
			$record->setMsTextNum($row["ms_text_num"]);
			$record->setCreatedAt($row["created_at"]);
			$record->setDeletedAt($row["deleted_at"]);
		}
		$result->close();
		return $record;
	}

	// *******************************************************************
	// idでデータを取得
	// *******************************************************************
	public function getAdvertAttentionById($id){
		$id = $this->mysqli->real_escape_string($id);
		$sql = " SELECT * FROM advert_attention WHERE id = '$id' AND deleted_at is NULL ";
		return $this->getAdvertAttention($sql);
	}

	// *******************************************************************
	// advert_idでデータを取得
	// *******************************************************************
	public function getAdvertAttentionByAdvertId($advert_id){
		$advert_id = $this->mysqli->real_escape_string($advert_id);
		$sql = " SELECT * FROM advert_attention WHERE advert_id = '$advert_id' AND deleted_at is NULL ";
		return $this->getAdvertAttention($sql);
	}

	// *******************************************************************
	//データの更新
	// *******************************************************************
	public function updateAdvertAttention($record, &$result_message = ""){
		$record = $this->escapeStringMedia($record);
		is_null($this->mysqli) and $this->connect();

		$sql = " UPDATE advert_attention SET "
			. " ms_image_url_num = '" . $record->getMsImageUrlNum() . "', "
			. " ms_text_num = '" . $record->getMsTextNum() . "' "
			. " WHERE id = '" . $record->getId() . "' AND deleted_at is NULL ";

		if(!$this->mysqli->query($sql)) {
			$result_message = "更新に失敗しました" . $this->mysqli->error . "\n";
			return false;
		}
		$result_message = "更新しました";
		return true;
	}

	// *******************************************************************
	//新規データの登録
	// *******************************************************************
	public function insertAdvertAttention($record, &$result_message = ""){
		$record = $this->escapeStringMedia($record);
		is_null($this->mysqli) and $this->connect();

		$sql = " INSERT INTO advert_attention values ('', "
			. " '" . $record->getAdvertId() . "', "
			. " '" . $record->getMsImageUrlNum() . "', "
			. " '" . $record->getMsTextNum() . "', "
			. " Now(), NULL) ";
		if(!$this->mysqli->query($sql)){
			$result_message = "登録に失敗しました" . $this->mysqli->error . "\n";
			return false;
		}
		$result_message = "一件追加しました";
		return true;
	}

	// *******************************************************************
	//データの削除
	// *******************************************************************
	public function deleteAdvertAttention($id, &$result_message = ""){
		$id = $this->mysqli->real_escape_string($id);
		is_null($this->mysqli) and $this->connect();

		$sql = "UPDATE advert_attention SET deleted_at = Now() "
			. " WHERE id = '$id' and deleted_at is NULL ";
		if(!$this->mysqli->query($sql)){
			$result_message = "削除に失敗しました" . $this->mysqli->error . "\n";
			return false;
		}
		$result_message = "削除しました";
		return true;
	}


	// *******************************************************************
	//LoginUserクラスの各プロパティから特殊文字をエスケープ
	// *******************************************************************
	private function escapeStringMedia($record){
		$record->setId($this->mysqli->real_escape_string($record->getId()));
		$record->setAdvertId($this->mysqli->real_escape_string($record->getAdvertId()));
		$record->setMsImageUrlNum($this->mysqli->real_escape_string($record->getMsImageUrlNum()));
		$record->setMsTextNum($this->mysqli->real_escape_string($record->getMsTextNum()));
		$record->setCreatedAt($this->mysqli->real_escape_string($record->getCreatedAt()));
		$record->setDeletedAt($this->mysqli->real_escape_string($record->getDeletedAt()));
		return $record;
	}

}

?>