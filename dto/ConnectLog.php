<?php
class ConnectLog {

	// プロパティ
	private $id = "";
	private $media_id = "";
	private $advert_id = "";
	private $status = "";
	private $created_at = "";
	private $updated_at = "";
	private $deleted_at = "";

	// _toString()
	public function _toString(){
		return (string)($this->id . ","
						. $this->media_id . ","
						. $this->advert_id . ","
						. $this->status . ","
						. $this->created_at . ","
						. $this->updated_at . ","
						. $this->deleted_at);
	}

	// id
	public function getId(){
		return $this->id;
	}

	public function setId($val){
		$this->id = $val;
	}

	// media_id
	public function getMediaId(){
		return $this->media_id;
	}

	public function setMediaId($val){
		$this->media_id = $val;
	}

	// advert_id
	public function getAdvertId(){
		return $this->advert_id;
	}

	public function setAdvertId($val){
		$this->advert_id = $val;
	}

	// status
	public function getStatus(){
		return $this->status;
	}

	public function setStatus($val){
		$this->status = $val;
	}

	// created_at
	public function getCreatedAt(){
		return $this->created_at;
	}

	public function setCreatedAt($val){
		$this->created_at = $val;
	}

	// updated_at
	public function getUpdatedAt(){
		return $this->updated_at;
	}

	public function setUpdatedAt($val){
		$this->updated_at = $val;
	}

	// deleted_at
	public function getDeletedAt(){
		return $this->deleted_at;
	}

	public function setDeletedAt($val){
		$this->deleted_at = $val;
	}

}