<?php
class PointBackLog {

	// プロパティ
	private $id = "";
	private $session_id = "";
	private $media_id = "";
	private $advert_id = "";
	private $point_back_parameter = "";
	private $point_back_url = "";
	private $status = "";
	private $created_at = "";
	private $updated_at = "";
	private $deleted_at = "";

	// _toString()
	public function _toString(){
		return (string)($this->id . ","
						. $this->session_id . ","
						. $this->media_id . ","
						. $this->advert_id . ","
						. $this->point_back_parameter . ","
						. $this->point_back_url . ","
						. $this->status . ","
						. $this->created_at . ","
						. $this->updated_at . ","
						. $this->deleted_at);
	}

	// id
	public function getId(){
		return $this->id;
	}

	public function setId($val){
		$this->id = $val;
	}

	// session_id
	public function getSessionId(){
		return $this->session_id;
	}

	public function setSessionId($val){
		$this->session_id = $val;
	}

	// media_id
	public function getMediaId(){
		return $this->media_id;
	}

	public function setMediaId($val){
		$this->media_id = $val;
	}

	// advert_id
	public function getAdvertId(){
		return $this->advert_id;
	}

	public function setAdvertId($val){
		$this->advert_id = $val;
	}

	// point_back_parameter
	public function getPointBackParameter(){
		return $this->point_back_parameter;
	}

	public function setPointBackParameter($val){
		$this->point_back_parameter = $val;
	}

	// point_back_url
	public function getPointBackUrl(){
		return $this->point_back_url;
	}

	public function setPointBackUrl($val){
		$this->point_back_url = $val;
	}

	// status
	public function getStatus(){
		return $this->status;
	}

	public function setStatus($val){
		$this->status = $val;
	}

	// created_at
	public function getCreatedAt(){
		return $this->created_at;
	}

	public function setCreatedAt($val){
		$this->created_at = $val;
	}

	// updated_at
	public function getUpdatedAt(){
		return $this->updated_at;
	}

	public function setUpdatedAt($val){
		$this->updated_at = $val;
	}

	// deleted_at
	public function getDeletedAt(){
		return $this->deleted_at;
	}

	public function setDeletedAt($val){
		$this->deleted_at = $val;
	}

}