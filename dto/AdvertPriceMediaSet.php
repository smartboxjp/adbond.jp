<?php
class AdvertPriceMediaSet {

	// プロパティ
	private $id = "";
	private $advert_id = "";
	private $media_id = "";
	private $media_publisher_id = "";
	private $media_category_id = "";
	private $click_price_client = "";
	private $click_price_media = "";
	private $action_price_client_docomo_1 = "";
	private $action_price_client_softbank_1 = "";
	private $action_price_client_au_1 = "";
	private $action_price_client_pc_1 = "";
	private $action_price_client_docomo_2 = "";
	private $action_price_client_softbank_2 = "";
	private $action_price_client_au_2 = "";
	private $action_price_client_pc_2 = "";
	private $action_price_client_docomo_3 = "";
	private $action_price_client_softbank_3 = "";
	private $action_price_client_au_3 = "";
	private $action_price_client_pc_3 = "";
	private $action_price_client_docomo_4 = "";
	private $action_price_client_softbank_4 = "";
	private $action_price_client_au_4 = "";
	private $action_price_client_pc_4 = "";
	private $action_price_client_docomo_5 = "";
	private $action_price_client_softbank_5 = "";
	private $action_price_client_au_5 = "";
	private $action_price_client_pc_5 = "";
	private $action_price_media_docomo_1 = "";
	private $action_price_media_softbank_1 = "";
	private $action_price_media_au_1 = "";
	private $action_price_media_pc_1 = "";
	private $action_price_media_docomo_2 = "";
	private $action_price_media_softbank_2 = "";
	private $action_price_media_au_2 = "";
	private $action_price_media_pc_2 = "";
	private $action_price_media_docomo_3 = "";
	private $action_price_media_softbank_3 = "";
	private $action_price_media_au_3 = "";
	private $action_price_media_pc_3 = "";
	private $action_price_media_docomo_4 = "";
	private $action_price_media_softbank_4 = "";
	private $action_price_media_au_4 = "";
	private $action_price_media_pc_4 = "";
	private $action_price_media_docomo_5 = "";
	private $action_price_media_softbank_5 = "";
	private $action_price_media_au_5 = "";
	private $action_price_media_pc_5 = "";
	private $created_at = "";
	private $updated_at = "";
	private $deleted_at = "";

	// _toString()
	public function _toString(){
		return (string)($this->id . ","
						. $this->advert_id . ","
						. $this->media_id . ","
						. $this->media_publisher_id . ","
						. $this->media_category_id . ","
						. $this->click_price_client . ","
						. $this->click_price_media . ","
						. $this->action_price_client_docomo_1 . ","
						. $this->action_price_client_softbank_1 . ","
						. $this->action_price_client_au_1 . ","
						. $this->action_price_client_pc_1 . ","
						. $this->action_price_client_docomo_2 . ","
						. $this->action_price_client_softbank_2 . ","
						. $this->action_price_client_au_2 . ","
						. $this->action_price_client_pc_2 . ","
						. $this->action_price_client_docomo_3 . ","
						. $this->action_price_client_softbank_3 . ","
						. $this->action_price_client_au_3 . ","
						. $this->action_price_client_pc_3 . ","
						. $this->action_price_client_docomo_4 . ","
						. $this->action_price_client_softbank_4 . ","
						. $this->action_price_client_au_4 . ","
						. $this->action_price_client_pc_4 . ","
						. $this->action_price_client_docomo_5 . ","
						. $this->action_price_client_softbank_5 . ","
						. $this->action_price_client_au_5 . ","
						. $this->action_price_client_pc_5 . ","
						. $this->action_price_media_docomo_1 . ","
						. $this->action_price_media_softbank_1 . ","
						. $this->action_price_media_au_1 . ","
						. $this->action_price_media_pc_1 . ","
						. $this->action_price_media_docomo_2 . ","
						. $this->action_price_media_softbank_2 . ","
						. $this->action_price_media_au_2 . ","
						. $this->action_price_media_pc_2 . ","
						. $this->action_price_media_docomo_3 . ","
						. $this->action_price_media_softbank_3 . ","
						. $this->action_price_media_au_3 . ","
						. $this->action_price_media_pc_3 . ","
						. $this->action_price_media_docomo_4 . ","
						. $this->action_price_media_softbank_4 . ","
						. $this->action_price_media_au_4 . ","
						. $this->action_price_media_pc_4 . ","
						. $this->action_price_media_docomo_5 . ","
						. $this->action_price_media_softbank_5 . ","
						. $this->action_price_media_au_5 . ","
						. $this->action_price_media_pc_5 . ","
						. $this->created_at . ","
						. $this->updated_at . ","
						. $this->deleted_at);
	}

	// id
	public function getId(){
		return $this->id;
	}

	public function setId($val){
		$this->id = $val;
	}

	// advert_id
	public function getAdvertId(){
		return $this->advert_id;
	}

	public function setAdvertId($val){
		$this->advert_id = $val;
	}

	// media_id
	public function getMediaId(){
		return $this->media_id;
	}

	public function setMediaId($val){
		$this->media_id = $val;
	}

	// media_publisher_id
	public function getMediaPublisherId(){
		return $this->media_publisher_id;
	}

	public function setMediaPublisherId($val){
		$this->media_publisher_id = $val;
	}

	// media_category_id
	public function getMediaCategoryId(){
		return $this->media_category_id;
	}

	public function setMediaCategoryId($val){
		$this->media_category_id = $val;
	}

	// click_price_client
	public function getClickPriceClient(){
		return $this->click_price_client;
	}

	public function setClickPriceClient($val){
		$this->click_price_client = $val;
	}

	// click_price_media
	public function getClickPriceMedia(){
		return $this->click_price_media;
	}

	public function setClickPriceMedia($val){
		$this->click_price_media = $val;
	}

	// action_price_client_docomo_1
	public function getActionPriceClientDocomo1(){
		return $this->action_price_client_docomo_1;
	}

	public function setActionPriceClientDocomo1($val){
		$this->action_price_client_docomo_1 = $val;
	}

	// action_price_client_softbank_1
	public function getActionPriceClientSoftbank1(){
		return $this->action_price_client_softbank_1;
	}

	public function setActionPriceClientSoftbank1($val){
		$this->action_price_client_softbank_1 = $val;
	}

	// action_price_client_au_1
	public function getActionPriceClientAu1(){
		return $this->action_price_client_au_1;
	}

	public function setActionPriceClientAu1($val){
		$this->action_price_client_au_1 = $val;
	}

	// action_price_client_pc_1
	public function getActionPriceClientPc1(){
		return $this->action_price_client_pc_1;
	}

	public function setActionPriceClientPc1($val){
		$this->action_price_client_pc_1 = $val;
	}

	// action_price_client_docomo_2
	public function getActionPriceClientDocomo2(){
		return $this->action_price_client_docomo_2;
	}

	public function setActionPriceClientDocomo2($val){
		$this->action_price_client_docomo_2 = $val;
	}

	// action_price_client_softbank_2
	public function getActionPriceClientSoftbank2(){
		return $this->action_price_client_softbank_2;
	}

	public function setActionPriceClientSoftbank2($val){
		$this->action_price_client_softbank_2 = $val;
	}

	// action_price_client_au_2
	public function getActionPriceClientAu2(){
		return $this->action_price_client_au_2;
	}

	public function setActionPriceClientAu2($val){
		$this->action_price_client_au_2 = $val;
	}

	// action_price_client_pc_2
	public function getActionPriceClientPc2(){
		return $this->action_price_client_pc_2;
	}

	public function setActionPriceClientPc2($val){
		$this->action_price_client_pc_2 = $val;
	}

	// action_price_client_docomo_3
	public function getActionPriceClientDocomo3(){
		return $this->action_price_client_docomo_3;
	}

	public function setActionPriceClientDocomo3($val){
		$this->action_price_client_docomo_3 = $val;
	}

	// action_price_client_softbank_3
	public function getActionPriceClientSoftbank3(){
		return $this->action_price_client_softbank_3;
	}

	public function setActionPriceClientSoftbank3($val){
		$this->action_price_client_softbank_3 = $val;
	}

	// action_price_client_au_3
	public function getActionPriceClientAu3(){
		return $this->action_price_client_au_3;
	}

	public function setActionPriceClientAu3($val){
		$this->action_price_client_au_3 = $val;
	}

	// action_price_client_pc_3
	public function getActionPriceClientPc3(){
		return $this->action_price_client_pc_3;
	}

	public function setActionPriceClientPc3($val){
		$this->action_price_client_pc_3 = $val;
	}

	// action_price_client_docomo_4
	public function getActionPriceClientDocomo4(){
		return $this->action_price_client_docomo_4;
	}

	public function setActionPriceClientDocomo4($val){
		$this->action_price_client_docomo_4 = $val;
	}

	// action_price_client_softbank_4
	public function getActionPriceClientSoftbank4(){
		return $this->action_price_client_softbank_4;
	}

	public function setActionPriceClientSoftbank4($val){
		$this->action_price_client_softbank_4 = $val;
	}

	// action_price_client_au_4
	public function getActionPriceClientAu4(){
		return $this->action_price_client_au_4;
	}

	public function setActionPriceClientAu4($val){
		$this->action_price_client_au_4 = $val;
	}

	// action_price_client_pc_4
	public function getActionPriceClientPc4(){
		return $this->action_price_client_pc_4;
	}

	public function setActionPriceClientPc4($val){
		$this->action_price_client_pc_4 = $val;
	}

	// action_price_client_docomo_5
	public function getActionPriceClientDocomo5(){
		return $this->action_price_client_docomo_5;
	}

	public function setActionPriceClientDocomo5($val){
		$this->action_price_client_docomo_5 = $val;
	}

	// action_price_client_softbank_5
	public function getActionPriceClientSoftbank5(){
		return $this->action_price_client_softbank_5;
	}

	public function setActionPriceClientSoftbank5($val){
		$this->action_price_client_softbank_5 = $val;
	}

	// action_price_client_au_5
	public function getActionPriceClientAu5(){
		return $this->action_price_client_au_5;
	}

	public function setActionPriceClientAu5($val){
		$this->action_price_client_au_5 = $val;
	}

	// action_price_client_pc_5
	public function getActionPriceClientPc5(){
		return $this->action_price_client_pc_5;
	}

	public function setActionPriceClientPc5($val){
		$this->action_price_client_pc_5 = $val;
	}

	// action_price_media_docomo_1
	public function getActionPriceMediaDocomo1(){
		return $this->action_price_media_docomo_1;
	}

	public function setActionPriceMediaDocomo1($val){
		$this->action_price_media_docomo_1 = $val;
	}

	// action_price_media_softbank_1
	public function getActionPriceMediaSoftbank1(){
		return $this->action_price_media_softbank_1;
	}

	public function setActionPriceMediaSoftbank1($val){
		$this->action_price_media_softbank_1 = $val;
	}

	// action_price_media_au_1
	public function getActionPriceMediaAu1(){
		return $this->action_price_media_au_1;
	}

	public function setActionPriceMediaAu1($val){
		$this->action_price_media_au_1 = $val;
	}

	// action_price_media_pc_1
	public function getActionPriceMediaPc1(){
		return $this->action_price_media_pc_1;
	}

	public function setActionPriceMediaPc1($val){
		$this->action_price_media_pc_1 = $val;
	}

	// action_price_media_docomo_2
	public function getActionPriceMediaDocomo2(){
		return $this->action_price_media_docomo_2;
	}

	public function setActionPriceMediaDocomo2($val){
		$this->action_price_media_docomo_2 = $val;
	}

	// action_price_media_softbank_2
	public function getActionPriceMediaSoftbank2(){
		return $this->action_price_media_softbank_2;
	}

	public function setActionPriceMediaSoftbank2($val){
		$this->action_price_media_softbank_2 = $val;
	}

	// action_price_media_au_2
	public function getActionPriceMediaAu2(){
		return $this->action_price_media_au_2;
	}

	public function setActionPriceMediaAu2($val){
		$this->action_price_media_au_2 = $val;
	}

	// action_price_media_pc_2
	public function getActionPriceMediaPc2(){
		return $this->action_price_media_pc_2;
	}

	public function setActionPriceMediaPc2($val){
		$this->action_price_media_pc_2 = $val;
	}

	// action_price_media_docomo_3
	public function getActionPriceMediaDocomo3(){
		return $this->action_price_media_docomo_3;
	}

	public function setActionPriceMediaDocomo3($val){
		$this->action_price_media_docomo_3 = $val;
	}

	// action_price_media_softbank_3
	public function getActionPriceMediaSoftbank3(){
		return $this->action_price_media_softbank_3;
	}

	public function setActionPriceMediaSoftbank3($val){
		$this->action_price_media_softbank_3 = $val;
	}

	// action_price_media_au_3
	public function getActionPriceMediaAu3(){
		return $this->action_price_media_au_3;
	}

	public function setActionPriceMediaAu3($val){
		$this->action_price_media_au_3 = $val;
	}

	// action_price_media_pc_3
	public function getActionPriceMediaPc3(){
		return $this->action_price_media_pc_3;
	}

	public function setActionPriceMediaPc3($val){
		$this->action_price_media_pc_3 = $val;
	}

	// action_price_media_docomo_4
	public function getActionPriceMediaDocomo4(){
		return $this->action_price_media_docomo_4;
	}

	public function setActionPriceMediaDocomo4($val){
		$this->action_price_media_docomo_4 = $val;
	}

	// action_price_media_softbank_4
	public function getActionPriceMediaSoftbank4(){
		return $this->action_price_media_softbank_4;
	}

	public function setActionPriceMediaSoftbank4($val){
		$this->action_price_media_softbank_4 = $val;
	}

	// action_price_media_au_4
	public function getActionPriceMediaAu4(){
		return $this->action_price_media_au_4;
	}

	public function setActionPriceMediaAu4($val){
		$this->action_price_media_au_4 = $val;
	}

	// action_price_media_pc_4
	public function getActionPriceMediaPc4(){
		return $this->action_price_media_pc_4;
	}

	public function setActionPriceMediaPc4($val){
		$this->action_price_media_pc_4 = $val;
	}

	// action_price_media_docomo_5
	public function getActionPriceMediaDocomo5(){
		return $this->action_price_media_docomo_5;
	}

	public function setActionPriceMediaDocomo5($val){
		$this->action_price_media_docomo_5 = $val;
	}

	// action_price_media_softbank_5
	public function getActionPriceMediaSoftbank5(){
		return $this->action_price_media_softbank_5;
	}

	public function setActionPriceMediaSoftbank5($val){
		$this->action_price_media_softbank_5 = $val;
	}

	// action_price_media_au_5
	public function getActionPriceMediaAu5(){
		return $this->action_price_media_au_5;
	}

	public function setActionPriceMediaAu5($val){
		$this->action_price_media_au_5 = $val;
	}

	// action_price_media_pc_5
	public function getActionPriceMediaPc5(){
		return $this->action_price_media_pc_5;
	}

	public function setActionPriceMediaPc5($val){
		$this->action_price_media_pc_5 = $val;
	}

	// created_at
	public function getCreatedAt(){
		return $this->created_at;
	}

	public function setCreatedAt($val){
		$this->created_at = $val;
	}

	// updated_at
	public function getUpdatedAt(){
		return $this->updated_at;
	}

	public function setUpdatedAt($val){
		$this->updated_at = $val;
	}

	// deleted_at
	public function getDeletedAt(){
		return $this->deleted_at;
	}

	public function setDeletedAt($val){
		$this->deleted_at = $val;
	}

}