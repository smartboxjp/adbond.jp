<?php
class Media {

	// プロパティ
	private $id = "";
	private $media_publisher_id = "";
	private $media_category_id = "";
	private $media_name = "";
	private $support_docomo = "";
	private $support_softbank = "";
	private $support_au = "";
	private $support_pc = "";
	private $site_url_docomo = "";
	private $site_url_softbank = "";
	private $site_url_au = "";
	private $site_url_pc = "";
	private $media_type = "";
	private $page_view_day = "";
	private $site_outline = "";
	private $response_type = "";
	private $point_back_url = "";
	private $point_test_url = "";
	private $test_flag = "";
	private $status = "";
	private $created_at = "";
	private $updated_at = "";
	private $deleted_at = "";

	// _toString()
	public function _toString(){
		return (string)($this->id . ","
						 . $this->media_publisher_id . ","
						 . $this->media_category_id . ","
						 . $this->media_name . ","
						 . $this->support_docomo . ","
						 . $this->support_softbank . ","
						 . $this->support_au . ","
						 . $this->support_pc . ","
						 . $this->site_url_docomo . ","
						 . $this->site_url_softbank . ","
						 . $this->site_url_au . ","
						 . $this->site_url_pc . ","
						 . $this->media_type . ","
						 . $this->page_view_day . ","
						 . $this->site_outline . ","
						 . $this->response_type . ","
						 . $this->point_back_url . ","
						 . $this->point_test_url . ","
						 . $this->test_flag . ","
						 . $this->status . ","
						 . $this->created_at . ","
						 . $this->updated_at . ","
						 . $this->deleted_at);
	}

	// id
	public function getId(){
		return $this->id;
	}

	public function setId($val){
		$this->id = $val;
	}

	// media_publisher_id
	public function getMediaPublisherId(){
		return $this->media_publisher_id;
	}

	public function setMediaPublisherId($val){
		$this->media_publisher_id = $val;
	}

	// media_category_id
	public function getMediaCategoryId(){
		return $this->media_category_id;
	}

	public function setMediaCategoryId($val){
		$this->media_category_id = $val;
	}

	// media_name
	public function getMediaName(){
		return $this->media_name;
	}

	public function setMediaName($val){
		$this->media_name = $val;
	}

	// support_docomo
	public function getSupportDocomo(){
		return $this->support_docomo;
	}

	public function setSupportDocomo($val){
		$this->support_docomo = $val;
	}

	// support_softbank
	public function getSupportSoftbank(){
		return $this->support_softbank;
	}

	public function setSupportSoftbank($val){
		$this->support_softbank = $val;
	}

	// support_au
	public function getSupportAu(){
		return $this->support_au;
	}

	public function setSupportAu($val){
		$this->support_au = $val;
	}

	// support_pc
	public function getSupportPc(){
		return $this->support_pc;
	}

	public function setSupportPc($val){
		$this->support_pc = $val;
	}

	// site_url_docomo
	public function getSiteUrlDocomo(){
		return $this->site_url_docomo;
	}

	public function setSiteUrlDocomo($val){
		$this->site_url_docomo = $val;
	}

	// site_url_softbank
	public function getSiteUrlSoftbank(){
		return $this->site_url_softbank;
	}

	public function setSiteUrlSoftbank($val){
		$this->site_url_softbank = $val;
	}

	// site_url_au
	public function getSiteUrlAu(){
		return $this->site_url_au;
	}

	public function setSiteUrlAu($val){
		$this->site_url_au = $val;
	}

	// site_url_pc
	public function getSiteUrlPc(){
		return $this->site_url_pc;
	}

	public function setSiteUrlPc($val){
		$this->site_url_pc = $val;
	}

	// media_type
	public function getMediaType(){
		return $this->media_type;
	}

	public function setMediaType($val){
		$this->media_type = $val;
	}

	// page_view_day
	public function getPageViewDay(){
		return $this->page_view_day;
	}

	public function setPageViewDay($val){
		$this->page_view_day = $val;
	}

	// site_outline
	public function getSiteOutline(){
		return $this->site_outline;
	}

	public function setSiteOutline($val){
		$this->site_outline = $val;
	}

	// response_type
	public function getResponseType(){
		return $this->response_type;
	}

	public function setResponseType($val){
		$this->response_type = $val;
	}

	// point_back_url
	public function getPointBackUrl(){
		return $this->point_back_url;
	}

	public function setPointBackUrl($val){
		$this->point_back_url = $val;
	}

	// point_test_url
	public function getPointTestUrl(){
		return $this->point_test_url;
	}

	public function setPointTestUrl($val){
		$this->point_test_url = $val;
	}

	// test_flag
	public function getTestFlag(){
		return $this->test_flag;
	}

	public function setTestFlag($val){
		$this->test_flag = $val;
	}

	// status
	public function getStatus(){
		return $this->status;
	}

	public function setStatus($val){
		$this->status = $val;
	}

	// created_at
	public function getCreatedAt(){
		return $this->created_at;
	}

	public function setCreatedAt($val){
		$this->created_at = $val;
	}

	// updated_at
	public function getUpdatedAt(){
		return $this->updated_at;
	}

	public function setUpdatedAt($val){
		$this->updated_at = $val;
	}

	// deleted_at
	public function getDeletedAt(){
		return $this->deleted_at;
	}

	public function setDeletedAt($val){
		$this->deleted_at = $val;
	}

}