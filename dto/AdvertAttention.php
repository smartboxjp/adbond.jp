<?php

class AdvertAttention {

	private $id = "";
	private $advert_id = "";
	private $ms_image_url_num = "";
	private $ms_text_num = "";
	private $created_at = "";
	private $deleted_at = "";


	public function _toString(){
		return (string)($this->id . ","
						. $this->advert_id . ","
						. $this->ms_image_url_num . ","
						. $this->ms_text_num . ","
						. $this->created_at . ","
						. $this->deleted_at);

	}

	// -- ------------------------------------------ -- //
	// id
	public function getId(){
		return $this->id;
	}
	public function setId($val){
		$this->id = $val;
	}
	// -- ------------------------------------------ -- //

	// -- ------------------------------------------ -- //
	// advert_id
	public function getAdvertId(){
		return $this->advert_id;
	}
	public function setAdvertId($val){
		$this->advert_id = $val;
	}
	// -- ------------------------------------------ -- //

	// -- ------------------------------------------ -- //
	// ms_image_url
	public function getMsImageUrlNum(){
		return $this->ms_image_url_num;
	}
	public function setMsImageUrlNum($val){
		$this->ms_image_url_num = $val;
	}
	// -- ------------------------------------------ -- //

	// -- ------------------------------------------ -- //
	// ms_text
	public function getMsTextNum(){
		return $this->ms_text_num;
	}
	public function setMsTextNum($val){
		$this->ms_text_num = $val;
	}
	// -- ------------------------------------------ -- //

	// -- ------------------------------------------ -- //
	// created_at
	public function getCreatedAt(){
		return $this->created_at;
	}
	public function setCreatedAt($val){
		$this->created_at = $val;
	}
	// -- ------------------------------------------ -- //

	// -- ------------------------------------------ -- //
	// deleted_at
	public function getDeletedAt(){
		return $this->deleted_at;
	}
	public function setDeletedAt($val){
		$this->deleted_at = $val;
	}
	// -- ------------------------------------------ -- //

}

?>