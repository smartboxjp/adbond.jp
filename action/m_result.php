<?php
// 共通設定
require_once( '../common/CommonAdminBase.php' );
require_once( '../common/CommonDao.php' );
require_once( '../common/CommonFunc.php' );
require_once( '../dao/ActionLogDao.php' );
require_once( '../dto/ActionLog.php' );
require_once( '../dao/PointBackLogDao.php' );
require_once( '../dto/PointBackLog.php' );
require_once( '../dao/MediaDao.php' );
require_once( '../dto/Media.php' );
require_once( '../dao/AdvertDao.php' );
require_once( '../dto/Advert.php' );

require_once( './referer_logs.php'  );

// GETパラメータbidがセットされているか
if(isset($_GET['bid']) && $_GET['bid'] != ''){

	// オブジェクト生成
	// DB接続クラス生成
	$common_dao = new CommonDao();
	// action_log_daoクラス生成
	$action_log_dao = new ActionLogDao();
	// action_logクラス生成
	$advert_dao = new AdvertDao();
	// media_daoクラス生成
	$media_dao = new MediaDao();

	// ログを取るタイミング

	// GET送信で受け取ったパラメータを取得
	$bid = $_GET['bid'];

	//------------------------------------------------------
	//7/21 追加 auidをGETで受け取る
	// ライブレボリューション専用ユニークID
	$auid = $_GET['auid'];
	//------------------------------------------------------

	// エラーフラグ
	$error_flag = 0;
	// 結果メッセージ
	$result_msg = "";

	//受け取ったセッションIDからレコードを取得
	// action_logクラス生成
	$action_log = new ActionLog();
	// bidを条件にaction_logからレコードを取得
	$action_log = $action_log_dao->getActionLogBySessionId($bid);
	if(!is_null($action_log)) {	//登録されているレコードか確認
		// 該当レコードが存在する

		// bidを変数へ代入
		$session_id = $bid;
		// 広告IDを取得
		$advert_id = $action_log->getAdvertId();
		// 媒体IDを取得
		$media_id = $action_log->getMediaId();
		// キャリアIDを取得
		$carrier_id = $action_log->getCarrierId();
		// 個体識別を取得
		$uid = $action_log->getUid();
		// ポイントバックパラメータ(キックバックパラメータ)を取得
		$point_back_parameter = $action_log->getPointBackParameter();
		// ポイントバックURL(キックバックURL)を取得
		$point_back_url = $action_log->getPointBackUrl();
		// 登録日付を取得
		$created_at = $action_log->getCreatedAt();

		$error_flag = 0;

			$ac = date("YmdHis");

			// 英文形式の日付をタイムスタンプに変換し取得
			$c_date = strtotime($action_log->getCreatedAt());
			// 登録日付の年を取得
			$c_year = date("Y", $c_date);
			// 登録日付の月を取得
			$c_month = date("m", $c_date);
			// 登録日付の日を取得
			$c_day = date("d", $c_date);
			// 登録日付の時を取得
			$c_hour = date("H", $c_date);
			// 登録日付の分を取得
			$c_minute = date("i", $c_date);
			// 登録日付の秒を取得
			$c_second = date("s", $c_date);


			// 英文形式の日付をタイムスタンプに変換し取得
			$a_date = strtotime($ac);
			// ac日付の年を取得
			$a_year = date("Y", $a_date);
			// ac日付の月を取得
			$a_month = date("m", $a_date);
			// ac日付の日を取得
			$a_day = date("d", $a_date);
			// ac日付の時を取得
			$a_hour = date("H", $a_date);
			// ac日付の分を取得
			$a_minute = date("i", $a_date);
			// ac日付の秒を取得
			$a_second = date("s", $a_date);

			// 登録日付とac日付を秒の長整数に変換し比較する
			// ac日付が登録日付以上であるか
			if(mktime($c_hour, $c_minute, $c_second, $c_month, $c_day, $c_year) <= mktime($a_hour, $a_minute, $a_second, $a_month, $a_day, $a_year)) {
				// ac日付が登録日付以上である
				// acを変数へ格納
			$action_complete_date = $ac;

			} else {
				// ac日付が登録日付以上でない
				// エラーフラグに1を代入
				$error_flag = 1;

			}

	} else {

		$error_flag = 1;

	}

	// エラーフラグが0か
	if($error_flag == 0) {
		// action_log_daoクラス トランザクション スタート
		$action_log_dao->transaction_start();
		// action_logクラスのget/setメソッドに値をセット
		// アクション日付をセット
		$action_log->setActionCompleteDate($action_complete_date);
		// ステータス2をセット
		$action_log->setStatus(3);
	//-------------------------------------------
	// 7/22 追加 auidをセット
	// ライブレボリューション専用ユニークID
		$action_log->setAuid($auid);
	//-------------------------------------------

		//Updateを実行
		$db_result = $action_log_dao->UpdateActionLog($action_log, $result_message);
		// DB結果
		if($db_result) {
			// 更新成功
			// action_log_daoクラス トランザクション エンド
			$action_log_dao->transaction_end();

			// ポイントバックURL(キックバックURL)が空でないか
			if($point_back_url != "") {

				//ポイントバック成果通知処理
				//$point_back_url = $action_log->getPointBackUrl();

				// 成果返却タイプ
				if($response_type == 1) {
					// 成果 セッションID
					$res_id = $point_back_parameter;
					// ##ID##とポイントバックパラメータ(キックバックパラメータ)を置き換え
					$point_back_url = ereg_replace("##ID##", $res_id, $point_back_url);

				} elseif($response_type == 0) {
					// 成果 個体識別
					$res_id = urlencode($uid);
					// ##ID##と個体識別を置き換え
					$point_back_url = ereg_replace("##ID##", $res_id, $point_back_url);

				} elseif($response_type == 2) {
					// 成果 セッションIDと個体識別
					// ##ID##とポイントバックパラメータ(キックバックパラメータ)を置き換え
					$point_back_url = ereg_replace("##ID##", $point_back_parameter, $point_back_url);
					// ##UID##と個体識別を置き換え
					$point_back_url = ereg_replace("##UID##", urlencode($uid), $point_back_url);

				}

//				$point_back_url = ereg_replace("##ID##", $res_id, $point_back_url);
				// ##CID##と広告IDを置き換え
				$point_back_url = ereg_replace("##CID##", $action_log->getAdvertId(), $point_back_url);
				// ##CLICK_DATE##と登録日付を置き換え
				$point_back_url = ereg_replace("##CLICK_DATE##", date("YmdHis", $action_log->getCreatedAt()), $point_back_url);
				// ##ACTION_DATE##とアクション完了日付を置き換え
				$point_back_url = ereg_replace("##ACTION_DATE##", date("YmdHis", $action_log->getActionCompleteDate()), $point_back_url);
				// ##AUID##とライブレボリューション専用ユニークIDを置き換え
				$point_back_url = ereg_replace("##AUID##", $action_log->getAuid(), $point_back_url);

				// ポイントバックURL(キックバックURL)を配列に分割
				$url_array = parse_url($point_back_url);

				// parse_url queryがセットされているか
				if(isset($url_array['query'])){
					// parse_url queryの文字列に?を付与し変数へ格納
					$query = "?" . $url_array['query'];
				}

				// parse_url hostを変数へ格納
				$host = $url_array['host'];
				//parse_url pathに変数$queryを付与し変数へ格納
				$path = $url_array['path'] . $query;

				// ------------------------------------------------------------ 2011/06/01 追加
				// ライブレボリューション auid付与
				if(isset($_GET['auid']) && $_GET['auid'] != ''){
					$path = $url_array['path'] . $query . "&auid=" . $auid;
				}
				// ----------------------------------------------------------------------------

//				// ------------------------------------------------------------ 2011/06/01 追加
//				// ユニティ・モバアフィ[au/softbank] 個体識別付与
//				if($media_id == '249'){
//					$path = $url_array['path'] . $query . "&OrderID=" . urlencode($uid);
//				}
//				// ----------------------------------------------------------------------------

				// String型でかつpathの値が""か
				if($path === ""){
					// "/"を変数に代入
					$path = "/";
				}

				$port = 80;             // HTTP なので80
				$timeout = 30;             // 接続に失敗した場合の待ち時間

				// ポイントバックログのステータスに1を代入
				$pb_status = 1;
				$sock = fsockopen($host, $port, $errno, $errstr, $timeout);  // サーバに接続する
				if($sock === FALSE){    // 接続に失敗したらメッセージを表示し、終了させる
					// エラーメッセージを発行
					echo "SOCK OPEN ERROR<br>";
					// ポイントバックログのステータスに2を代入
					$pb_status = 2;

				} else {
					// HTTP ヘッダ部分の送信になる。
					fwrite($sock, "GET http://" . $host . $path . " HTTP/1.0\r\n");
					// ヘッダの終了を通知
					fwrite($sock, "\r\n\r\n");

				}
				// ソケットクローズ
				fclose($sock);

			}

		} else {
			// レコード更新失敗
			// $action_log_daoクラス トランザクション ロールバック
			$action_log_dao->transaction_rollback();
			exit();

		}

	}else{

	}

}else{
	exit();
}

function compareDate($year1, $month1, $day1, $year2, $month2, $day2) {
    $dt1 = mktime(0, 0, 0, $month1, $day1, $year1);
    $dt2 = mktime(0, 0, 0, $month2, $day2, $year2);
    $diff = $dt1 - $dt2;
    $diffDay = $diff / 86400;//1日は86400秒
    return $diffDay;
}
?>