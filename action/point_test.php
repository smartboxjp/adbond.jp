<?php
// 共通設定
require_once( '../common/CommonAdminBase.php' );
require_once( '../common/CommonFunc.php' );
require_once( '../common/CommonDao.php' );
require_once( '../dao/ActionLogDao.php' );
require_once( '../dto/ActionLog.php' );
require_once( '../dao/PointBackLogDao.php' );
require_once( '../dto/PointBackLog.php' );
require_once( '../dao/MediaDao.php' );
require_once( '../dto/Media.php' );
require_once( '../dao/AdvertDao.php' );
require_once( '../dto/Advert.php' );
require_once( '../dao/AdvertPriceMediaSetDao.php' );
require_once( '../dto/AdvertPriceMediaSet.php' );

if(isset($_GET['m']) && $_GET['m'] != '' && isset($_GET['a']) && $_GET['a'] != ''){

	$common_dao = new CommonDao();
	$action_log_dao = new ActionLogDao();

	$session_id = md5(uniqid(rand(), true));
	$carrier_id = "";
	$user_agent = $_SERVER['HTTP_USER_AGENT'];
	//$uid = get_subno();
	$uid = "";
	$ip_address = $_SERVER['REMOTE_ADDR'];
	$host_name = gethostbyaddr($_SERVER["REMOTE_ADDR"]);
	$media_id = $_GET['m'];
	$media_publisher_id = "";
	$advert_id = $_GET['a'];
	$advert_client_id = "";
	$click_price = 0;
	$action_price = 0;
//	$point_back_parameter = $_GET['pb'];
	$status = 3;

	$error_flag = 0;

	//受け取った媒体IDからレコードを取得
	$media_dao = new MediaDao();
	$media = new Media();
	$media = $media_dao->getMediaById($media_id);
	if(!is_null($media)) {	//登録されている媒体か確認
		$media_publisher_id = $media->getMediaPublisherId();
		$media_category_id = $media->getMediaCategoryId();
		$point_back_url = $media->getPointBackUrl();
	} else {
		$error_flag = 1;
	}

//	//受け取った広告IDからレコードを取得
//	$advert_dao = new AdvertDao();
//	$advert = new Advert();
//	$advert = $advert_dao->getAdvertById($advert_id);
//	if(!is_null($advert)) {	//登録されている広告か確認
//		$advert_client_id = $advert->getAdvertClientId();
//		$advert_category_id = $advert->getAdvertCategoryId();
//
//		$advert_price_media_set_dao = new AdvertPriceMediaSetDao();
//		$advert_price_madia_set = new AdvertPriceMediaSet();
//		$advert_price_madia_set = $advert_price_media_set_dao->getAdvertPriceMediaSetByMidAid($advert_id, $media_id);
//
//		if(!is_null($advert_price_madia_set)) {
//			$click_price_client = $advert_price_madia_set->getClickPriceClient();
//			$click_price_media = $advert_price_madia_set->getClickPriceMedia();
//
//			$action_price_client_docomo_1 = $advert_price_madia_set->getActionPriceClientDocomo1();
//			$action_price_client_softbank_1 = $advert_price_madia_set->getActionPriceClientSoftbank1();
//			$action_price_client_au_1 = $advert_price_madia_set->getActionPriceClientAu1();
//			$action_price_client_pc_1 = $advert_price_madia_set->getActionPriceClientPc1();
//
//			$action_price_media_docomo_1 = $advert_price_madia_set->getActionPriceMediaDocomo1();
//			$action_price_media_softbank_1 = $advert_price_madia_set->getActionPriceMediaSoftbank1();
//			$action_price_media_au_1 = $advert_price_madia_set->getActionPriceMediaAu1();
//			$action_price_media_pc_1 = $advert_price_madia_set->getActionPriceMediaPc1();
//		} else {
//			$click_price_client = $advert->getClickPriceClient();
//			$click_price_media = $advert->getClickPriceMedia();
//
//			$action_price_client_docomo_1 = $advert->getActionPriceClientDocomo1();
//			$action_price_client_softbank_1 = $advert->getActionPriceClientSoftbank1();
//			$action_price_client_au_1 = $advert->getActionPriceClientAu1();
//			$action_price_client_pc_1 = $advert->getActionPriceClientPc1();
//
//			$action_price_media_docomo_1 = $advert->getActionPriceMediaDocomo1();
//			$action_price_media_softbank_1 = $advert->getActionPriceMediaSoftbank1();
//			$action_price_media_au_1 = $advert->getActionPriceMediaAu1();
//			$action_price_media_pc_1 = $advert->getActionPriceMediaPc1();
//		}
//
//		$support_docomo = $advert->getSupportDocomo();
//		$support_softbank = $advert->getSupportSoftbank();
//		$support_au = $advert->getSupportAu();
//		$support_pc = $advert->getSupportPc();
//	} else {
//		$error_flag = 1;
//	}

	// キャリアチェック
	if(ereg("^DoCoMo", $user_agent)) {
		$carrier_id = 1;

		// 個体識別番号チェック
		if(isset($_SERVER['HTTP_X_DCMGUID'])) {
			$uid = $_SERVER['HTTP_X_DCMGUID'];
		} else {
			preg_match("/^.+ser([0-9a-zA-Z]+).*$/", $user_agent, $match);
			$uid = $match[1];
		}

	} else if(ereg("^J-PHONE|^Vodafone|^SoftBank", $user_agent)) {
		$carrier_id = 2;

		// 個体識別番号チェック
		if(isset($_SERVER['HTTP_X_JPHONE_UID'])) {
			$uid = $_SERVER['HTTP_X_JPHONE_UID'];
		}

	} else if(ereg("^UP.Browser|^KDDI", $user_agent)) {
		$carrier_id = 3;

		// 個体識別番号チェック
		if(isset($_SERVER['HTTP_X_UP_SUBNO'])) {
			$uid = $_SERVER['HTTP_X_UP_SUBNO'];
		}

	} else {
		$carrier_id = 4;

	}

	if($error_flag == 0){

		if($point_back_url != "") {
			//ポイントバック成果通知処理
			$point_back_parameter = $uid;
			if($uid == ''){
				$point_back_parameter = 123;
			}


			$point_back_url = ereg_replace("##ID##", urlencode($point_back_parameter), $point_back_url);
			$point_back_url = ereg_replace("##CID##", $advert_id, $point_back_url);
			$point_back_url = ereg_replace("##CLICK_DATE##", date("YmdHis"), $point_back_url);
			$point_back_url = ereg_replace("##ACTION_DATE##", date("YmdHis"), $point_back_url);

			$url_array = parse_url($point_back_url);

			if(isset($url_array['query'])){
				$query = "?" . $url_array['query'];
			}

			$host = $url_array['host'];
			$path = $url_array['path'] . $query;

			if($path === ""){
				$path = "/";
			}

			$port = 80;             // HTTP なので80
			$timeout = 30;             // 接続に失敗した場合の待ち時間

			$pb_status = 1;
			$sock = fsockopen($host, $port, $errno, $errstr, $timeout);  // サーバに接続する
			if($sock === FALSE){    // 接続に失敗したらメッセージを表示し、終了させる
				echo "SOCK OPEN ERROR<br>";
				$pb_status = 2;
			} else {
				// HTTP ヘッダ部分の送信になる。
				fwrite($sock, "GET http://" . $host . $path . " HTTP/1.0\r\n");
				// ヘッダの終了を通知
				fwrite($sock, "\r\n\r\n");
			}
			fclose($sock);

			//ポイントバック通知結果をデータベースに登録
			$point_back_log_dao = new PointBackLogDao();
			$point_back_log = new PointBackLog();

			$point_back_log_dao->transaction_start();

			$point_back_log->setSessionId($session_id);
			$point_back_log->setMediaId($media_id);
			$point_back_log->setAdvertId($advert_id);

			$point_back_log->setPointBackParameter($point_back_parameter);
			$point_back_log->setPointBackUrl($point_back_url);
			$point_back_log->setStatus($pb_status);

			//INSERTを実行
			$db_result = $point_back_log_dao->insertPointBackLog($point_back_log, $result_message);
			if($db_result) {
				$point_back_log_dao->transaction_end();

				exit();
			} else {
				$point_back_log_dao->transaction_rollback();

				exit();
			}
		}
		//header('Location: '.$advert_url);

		exit();
	} else {
		echo "未登録です";
		exit();
	}
}else{
	exit();
}
?>