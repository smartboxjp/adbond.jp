<?php
// 共通設定
require_once( '../common/CommonAdminBase.php' );
require_once( '../common/CommonFunc.php' );
require_once( '../common/CommonDao.php' );
require_once( '../dao/ActionLogDao.php' );
require_once( '../dto/ActionLog.php' );
require_once( '../dao/MediaDao.php' );
require_once( '../dto/Media.php' );
require_once( '../dao/AdvertDao.php' );
require_once( '../dto/Advert.php' );
require_once( '../dao/ConnectLogDao.php' );
require_once( '../dto/ConnectLog.php' );
require_once( '../dao/AdvertPriceMediaSetDao.php' );
require_once( '../dto/AdvertPriceMediaSet.php' );

require_once( './referer_logs.php'  );

// GETパラメータm、aがセットされているか
if(isset($_GET['m']) && $_GET['m'] != '' && isset($_GET['a']) && $_GET['a'] != ''){

	// GETパラメータsidがセットされているか
	if(isset($_GET['sid'])) {
		// GET送信で受け取ったパラメータを取得
		$sid = $_GET['sid'];
	}

	// GETパラメータafidがセットされているか
	if(isset($_GET['afid'])) {
		// GET送信で受け取ったパラメータを取得
		$sid = $_GET['afid'];
	}


	// オブジェクトの生成
	// DB接続クラスの生成
	$common_dao = new CommonDao();
	// action_log_daoクラスの生成
	$action_log_dao = new ActionLogDao();

	// セッションIDを生成
	// ※randにてランダムに英数字を取得
	// ※uniqidにて23文字の一意の文字列を取得
	// ※md5にて32桁のハッシュ値を取得
	$session_id = md5(uniqid(rand(), true));
	// キャリアID
	$carrier_id = "";
	// ユーザーエージェントを取得
	$user_agent = $_SERVER['HTTP_USER_AGENT'];
	// 個体識別
	$uid = "";
	// IPアドレスの取得
	$ip_address = $_SERVER['REMOTE_ADDR'];
	// ホスト名を取得
	$host_name = gethostbyaddr($_SERVER["REMOTE_ADDR"]);
	// GET送信で取得したパラメータ 媒体IDを取得
	$media_id = $_GET['m'];
	// 媒体発行者ID
	$media_publisher_id = "";
	// GET送信で取得したパラメータ 広告IDを取得
	$advert_id = $_GET['a'];
	// 広告主
	$advert_client_id = "";
	// クリック単価の初期化
	$click_price = 0;
	// アクション単価の初期化
	$action_price = 0;
	// ポイントバックパラメータ(キックバックパラメータ)
	$point_back_parameter = "";
	// ステータスの初期化
	$status = 1;
	//-------------------------------------------
	// 7/21 追加
	// ライブレボリューション用ユニークID
	$auid = "NULL";
	//-------------------------------------------
	// エラーフラグの初期化
	$error_flag = 0;

	// オブジェクトの生成
	// connect_log_daoクラスの生成
	$connect_log_dao = new ConnectLogDao();
	// connect_logクラスの生成
	$connect_log = new ConnectLog();

//	下記のように修正が必要ではないか コード↓
	// 媒体ID、広告ID、ステータスを条件にレコードを取得
	$connect_log = $connect_log_dao->getConnectLogByMidAidStatus($media_id, $advert_id, 2);
//	$connect_log = $connect_log_dao->getConnectLogByMidAid($media_id, $advert_id);

	// レコードがNULLか
	if(is_null($connect_log)) {
		// エラーメッセージの発行
		echo "error:(001)未提携です。";
		exit();
	}

	//受け取った媒体IDからレコードを取得
	// オブジェクトの生成
	// media_daoクラスの生成
	$media_dao = new MediaDao();
	// mediaクラスの生成
	$media = new Media();

//	下記のように修正が必要ではないか コード↓
	// 媒体ID、ステータスを条件にレコードを取得
	$media = $media_dao->getMediaByIdStatus($media_id, 2);
//	$media = $media_dao->getMediaById($media_id);

	// レコードがNULLでないか
	if(!is_null($media)) {	//登録されている媒体か確認
		// 媒体発行者IDを取得
		$media_publisher_id = $media->getMediaPublisherId();
		// 媒体カテゴリーIDを取得
		$media_category_id = $media->getMediaCategoryId();
		// レスポンスタイプを取得
		$response_type = $media->getResponseType();
		// ポイントバックURL(キックバックURL)を取得
		$point_back_url = $media->getPointBackUrl();

		// レスポンスタイプが1または2か
		if($response_type == 1 || $response_type == 2) {
			// ポイントバックパラメータ(キックバックパラメータ)を取得
			$point_back_parameter = $sid;
		}

	// レコードがNULL
	} else {
		// エラーメッセージを発行
		echo "error:(002)正規の媒体ではありません。";
		exit();
	}

	//受け取った広告IDからレコードを取得
	// オブジェクトの生成
	// advert_daoクラスの生成
	$advert_dao = new AdvertDao();
	// advertクラスの生成
	$advert = new Advert();
	// 広告ID、ステータスを条件にレコードを取得
	$advert = $advert_dao->getAdvertByIdStatus($advert_id, 2);
	// レコードがNULLでないか
	if(!is_null($advert)) {	//登録されている広告か確認
		// 広告主IDを取得
		$advert_client_id = $advert->getAdvertClientId();

		// オブジェクトの生成
		// advert_price_media_set_daoクラスの生成
		$advert_price_media_set_dao = new AdvertPriceMediaSetDao();
		// advert_price_media_setクラスの生成
		$advert_price_madia_set = new AdvertPriceMediaSet();
		// 広告ID、媒体IDを条件にレコードを取得
		$advert_price_madia_set = $advert_price_media_set_dao->getAdvertPriceMediaSetByMidAid($advert_id, $media_id);

		// レコードがNULLでないか
		if(!is_null($advert_price_madia_set)) {
			// 媒体別単価設定に設定されている(レコードが存在する)

			// advert_price_media_set_dao
			// クリック単価(グロス)の取得
			$click_price_client = $advert_price_madia_set->getClickPriceClient();
			// クリック単価(グロス)の値が0と等しいか
			if($click_price_client == "0"){
				// advert
				// クリック単価(グロス)の取得
				$click_price_client = $advert->getClickPriceClient();
			}

			// advert_price_media_set_dao
			// クリック単価(ネット)の取得
			$click_price_media = $advert_price_madia_set->getClickPriceMedia();
			// クリック単価(ネット)の値が0と等しいか
			if($click_price_media == "0"){
				// advert
				// クリック単価(ネット)の取得
				$click_price_media = $advert->getClickPriceMedia();
			}

			// グロス --------------------------------------------------------------------------
			// advert_price_media_set_dao
			// docomoのアクション単価(グロス)を取得
			$action_price_client_docomo_1 = $advert_price_madia_set->getActionPriceClientDocomo1();
			// docomosアクション単価(グロス)が0と等しいか
			if($action_price_client_docomo_1 == "0"){
				// advert
				// docomoのアクション単価(グロス)を取得
				$action_price_client_docomo_1 = $advert->getActionPriceClientDocomo1();
			}

			// advert_price_media_set_dao
			// softbankのアクション単価(グロス)を取得
			$action_price_client_softbank_1 = $advert_price_madia_set->getActionPriceClientSoftbank1();
			// softbankのアクション単価(グロス)が0と等しいか
			if($action_price_client_softbank_1 == "0"){
				// advert
				// softbankのアクション単価(グロス)を取得
				$action_price_client_softbank_1 = $advert->getActionPriceClientSoftbank1();
			}

			// advert_price_media_set_dao
			// auのアクション単価(グロス)を取得
			$action_price_client_au_1 = $advert_price_madia_set->getActionPriceClientAu1();
			// auのアクション単価(グロス)が0と等しいか
			if($action_price_client_au_1 == "0"){
				// advert
				// auのアクション単価(グロス)を取得
				$action_price_client_au_1 = $advert->getActionPriceClientAu1();
			}

			// advert_price_media_set_dao
			// pcのアクション単価(グロス)を取得
			$action_price_client_pc_1 = $advert_price_madia_set->getActionPriceClientPc1();
			// pcのアクション単価(グロス)が0と等しいか
			if($action_price_client_pc_1 == "0"){
				// advert
				// pcのアクション単価(グロス)を取得
				$action_price_client_pc_1 = $advert->getActionPriceClientPc1();
			}

			// ネット --------------------------------------------------------------------------
			// advert_price_media_set_dao
			// docomoのアクション単価(ネット)を取得
			$action_price_media_docomo_1 = $advert_price_madia_set->getActionPriceMediaDocomo1();
			// docomosアクション単価(ネット)が0と等しいか
			if($action_price_media_docomo_1 == "0"){
				// advert
				// docomoのアクション単価(ネット)を取得
				$action_price_media_docomo_1 = $advert->getActionPriceMediaDocomo1();
			}

			// advert_price_media_set_dao
			// softbankのアクション単価(ネット)を取得
			$action_price_media_softbank_1 = $advert_price_madia_set->getActionPriceMediaSoftbank1();
			// softbankアクション単価(ネット)が0と等しいか
			if($action_price_media_softbank_1 == "0"){
				// advert
				// softbankのアクション単価(ネット)を取得
				$action_price_media_softbank_1 = $advert->getActionPriceMediaSoftbank1();
			}

			// advert_price_media_set_dao
			// auのアクション単価(ネット)を取得
			$action_price_media_au_1 = $advert_price_madia_set->getActionPriceMediaAu1();
			// auアクション単価(ネット)が0と等しいか
			if($action_price_media_au_1 == "0"){
				// advert
				// auのアクション単価(ネット)を取得
				$action_price_media_au_1 = $advert->getActionPriceMediaAu1();
			}

			// advert_price_media_set_dao
			// pcのアクション単価(ネット)を取得
			$action_price_media_pc_1 = $advert_price_madia_set->getActionPriceMediaPc1();
			// pcアクション単価(ネット)が0と等しいか
			if($action_price_media_pc_1 == "0"){
				// advert
				// pcのアクション単価(ネット)を取得
				$action_price_media_pc_1 = $advert->getActionPriceMediaPc1();
			}

		} else {
			// 媒体別単価設定に設定されていない(レコードが存在しない)

			// クリック単価(グロス)を取得
			$click_price_client = $advert->getClickPriceClient();
			// クリック単価(ネット)を取得
			$click_price_media = $advert->getClickPriceMedia();

			// docomoアクション単価(グロス)を取得
			$action_price_client_docomo_1 = $advert->getActionPriceClientDocomo1();
			// softbankアクション単価(グロス)を取得
			$action_price_client_softbank_1 = $advert->getActionPriceClientSoftbank1();
			// auアクション単価(グロス)を取得
			$action_price_client_au_1 = $advert->getActionPriceClientAu1();
			// pcアクション単価(グロス)を取得
			$action_price_client_pc_1 = $advert->getActionPriceClientPc1();

			// docomoアクション単価(ネット)を取得
			$action_price_media_docomo_1 = $advert->getActionPriceMediaDocomo1();
			// softbankアクション単価(ネット)を取得
			$action_price_media_softbank_1 = $advert->getActionPriceMediaSoftbank1();
			// auアクション単価(ネット)を取得
			$action_price_media_au_1 = $advert->getActionPriceMediaAu1();
			// pcアクション単価(ネット)を取得
			$action_price_media_pc_1 = $advert->getActionPriceMediaPc1();
		}

		// 対応キャリア
		// docomo
		$support_docomo = $advert->getSupportDocomo();
		// softbank
		$support_softbank = $advert->getSupportSoftbank();
		// au
		$support_au = $advert->getSupportAu();
		// pc
		$support_pc = $advert->getSupportPc();

	} else {
		// エラーメッセージを発行
		echo "error:(003)正規の広告ではありません。";
		exit();
	}

	// キャリアチェック
	// docomo
	// ユーザーエージェントにDoCoMoの文字列が含まれているか
	if(ereg("^DoCoMo", $user_agent)) {
		// キャリアIDに1を代入
		$carrier_id = 1;

		// docomo 個体識別番号チェック
		// SERVER変数に個体識別(iモードID)がセットされているか
		if(isset($_SERVER['HTTP_X_DCMGUID'])) {
			// SERVER変数に個体識別(iモードID)がセットされている場合
			// 個体識別(iモードID)を取得
			$uid = $_SERVER['HTTP_X_DCMGUID'];

		} else {
			// SERVER変数に個体識別(iモードID)がセットされていない場合
			// ユーザーエージェントよりマッチする英数字を取得
			preg_match("/^.+ser([0-9a-zA-Z]+).*$/", $user_agent, $match);
			// 配列(要素1)の値を変数へ代入
			$uid = $match[1];
		}

		// docomoアクション単価(グロス)を取得
		$action_price_client = $action_price_client_docomo_1;
		// docomoアクション単価(ネット)を取得
		$action_price_media = $action_price_media_docomo_1;

		// docomoが対応キャリアか
		if($support_docomo == 1) {
			// 広告遷移先URLを取得
			$advert_url = $advert->getSiteUrlDocomo();
		} else {
			// エラーフラグに1を代入
			$error_flag = 1;
		}

	// softbank
	// ユーザーエージェントにJ-PHONE,Vodafone,SoftBankの文字列が含まれているか
	} else if(ereg("^J-PHONE|^Vodafone|^SoftBank", $user_agent)) {
		// キャリアIDに2を代入
		$carrier_id = 2;

		// softbank 個体識別番号チェック
		// SERVER変数に個体識別がセットされているか
		if(isset($_SERVER['HTTP_X_JPHONE_UID'])) {
			// SERVER変数に個体識別がセットされている場合
			// 個体識別を取得
			$uid = $_SERVER['HTTP_X_JPHONE_UID'];
		}

		// softbankアクション単価(グロス)を取得
		$action_price_client = $action_price_client_softbank_1;
		// softbankアクション単価(ネット)を取得
		$action_price_media = $action_price_media_softbank_1;

		// sftbankが対応キャリアか
		if($support_softbank == 1) {
			// 広告遷移先URLを取得
			$advert_url = $advert->getSiteUrlSoftbank();

		} else {
			// エラーフラグに1を代入
			$error_flag = 1;
		}

	// au
	// ユーザーエージェントにUP.Browser,KDDIが含まれているか
	} else if(ereg("^UP.Browser|^KDDI", $user_agent)) {
		// キャリアIDに3を代入
		$carrier_id = 3;

		// au 個体識別番号チェック
		// SERVER変数に個体識別がセットされているか
		if(isset($_SERVER['HTTP_X_UP_SUBNO'])) {
			// SERVER変数に個体識別がセットされている場合
			// 個体識別を取得
			$uid = $_SERVER['HTTP_X_UP_SUBNO'];
		}

		// auアクション単価(グロス)を取得
		$action_price_client = $action_price_client_au_1;
		// auアクション単価(ネット)を取得
		$action_price_media = $action_price_media_au_1;

		// 対応キャリアか
		if($support_au == 1) {
			// 広告遷移先URLを取得
			$advert_url = $advert->getSiteUrlAu();

		} else {
			// エラーフラグに1を代入
			$error_flag = 1;
		}

	// pc,その他
	} else {
		// キャリアIDに4を代入
		$carrier_id = 4;

		// pcアクション単価(グロス)を取得
		$action_price_client = $action_price_client_pc_1;
		// pcアクション単価(ネット)を取得
		$action_price_media = $action_price_media_pc_1;

		// 対応キャリアか
		if($support_pc == 1) {
			// 広告遷移先URLを取得
			$advert_url = $advert->getSiteUrlPc();

		} else {
			// エラーフラグに1を代入
			$error_flag = 1;
		}
	}

//----------------------------------------------------------------------------------
	// リファラ
	if( $_SERVER['HTTP_REFERER'] != NULL ){
		$ref = $_SERVER['HTTP_REFERER'];
	}
	else{
		$ref = "NO REFERER";
	}

	$c_referer_logs = new C_refererLogs();
	$c_referer_logs->M_getReferer("1", $advert_id, $media_id, $carrier_id, $ref);
//----------------------------------------------------------------------------------



	// エラーフラグが0と等しいか
	if($error_flag == 0){
		// 広告遷移先URLに##ID##が含まれているか
		if(stripos($advert_url, "##ID##")) {
			// ##ID##部分をsessionIDに置換
			$advert_url = ereg_replace("##ID##", $session_id, $advert_url);

		// 広告遷移先URLに?が含まれているか
		} elseif(!stripos($advert_url, "?")) {
			// 広告遷移先URLに?が含まれていない
			// 広告遷移先URLに?bid=session_idを付加する
			$advert_url .= "?bid=$session_id";

		} else {
			// 広告遷移先URLに?が含まれている
			// 広告遷移先URLに&bid=session_idを付加する
			$advert_url .= "&bid=$session_id";
		}

//----------------------------------------------------------------------------------
		// 8/16 追加 プロへディア用 ポイントバックパラメータ取得
		if(isset($_GET['uid1'])){
			$uid1= $_GET['uid1'];
			$point_back_parameter = $uid1;
		}
//----------------------------------------------------------------------------------
		// 12/15追加 株式会社ラフアンドピース専用
		// $advert_urlにパラメータを付与し送信
		// m=1：adpice側のadbondメディアID
		// a=$advert_id：メディアから受信したadvert_id
		// $session_id：セッションを付与 adpice側でポイントバックパラメータとする

		if($advert_client_id == '24'){
			$advert_url .= "&m=1";
			$advert_url .= "&a=".$advert_id;
		}
//----------------------------------------------------------------------------------

//----------------------------------------------------------------------------------
		// 12/15追加 株式会社リンクエッジ専用
		// $advert_urlにパラメータを付与し送信
		// m=1：adpice側のadbondメディアID
		// a=$advert_id：メディアから受信したadvert_id
		// $session_id：セッションを付与 adpice側でポイントバックパラメータとする

		if($advert_client_id == '35'){
			$advert_url .= "&m=1";
			$advert_url .= "&a=".$advert_id;
		}
//----------------------------------------------------------------------------------

		// action_log_daoのトランザクションをスタート
		$action_log_dao->transaction_start();

		// action_logクラスの生成
		$action_log = new ActionLog();
		// action_logのget/setメソッドにセット
		// セッションIDをセット
		$action_log->setSessionId($session_id);
		// キャリアIDをセット
		$action_log->setCarrierId($carrier_id);
		// ユーザーエージェントをセット
		$action_log->setUserAgent($user_agent);
		// 個体識別をセット
		$action_log->setUid($uid);
		// IPアドレスをセット
		$action_log->setIpAddress($ip_address);
		// ホスト名をセット
		$action_log->setHostName($host_name);
		// 媒体IDをセット
		$action_log->setMediaId($media_id);
		// 媒体発行者IDをセット
		$action_log->setMediaPublisherId($media_publisher_id);
		// 広告IDをセット
		$action_log->setAdvertId($advert_id);
		// 広告主IDをセット
		$action_log->setAdvertClientId($advert_client_id);
		// クリック単価(グロス)をセット
		$action_log->setClickPriceClient($click_price_client);
		// クリック単価(ネット)をセット
		$action_log->setClickPriceMedia($click_price_media);
		// アクション単価(グロス)をセット
		$action_log->setActionPriceClient($action_price_client);
		// アクション単価(ネット)をセット
		$action_log->setActionPriceMedia($action_price_media);
		// 広告遷移先URLをセット
		$action_log->setLinkUrl($advert_url);
		// ポイントバックパラメータ(キックバックパラメータ)をセット
		$action_log->setPointBackParameter($point_back_parameter);
		// ポイントバックURL(キックバックURL)をセット
		$action_log->setPointBackUrl($point_back_url);
		// ステータスをセット
		$action_log->setStatus($status);

		//-------------------------------------------
		// 7/21 追加
		// ライブレボリューション専用ユニークID
		$action_log->setAuid($auid);
		//-------------------------------------------

		//INSERTを実行
		$db_result = $action_log_dao->insertActionLog($action_log, $result_message);
		// DB結果
		if($db_result) {
			// insert成功
			// トランザクション エンド
			$action_log_dao->transaction_end();
			// 広告遷移先へ遷移
			header('Location: '.$advert_url);
			exit();

		} else {
			// insert失敗
			// トランザクション ロールバック
			$action_log_dao->transaction_rollback();
			// メッセージ表示
			echo $resut_message;
			exit();

		}
	} else {
		// エラーフラグが1の場合
		// エラーメッセージを発行
		echo "error:(004)このキャリアに対応した広告ではありません。";
		exit();

	}

}else{
	// GETパラメータm,aがセットされてなかった場合
	exit();

}
?>