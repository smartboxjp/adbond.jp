<?php
// 共通設定
require_once( '../common/CommonAdminBase.php' );
require_once( '../common/CommonDao.php' );
require_once( '../common/CommonFunc.php' );
require_once( '../dao/ActionLogDao.php' );
require_once( '../dto/ActionLog.php' );
require_once( '../dao/PointBackLogDao.php' );
require_once( '../dto/PointBackLog.php' );
require_once( '../dao/MediaDao.php' );
require_once( '../dto/Media.php' );
require_once( '../dao/AdvertDao.php' );
require_once( '../dto/Advert.php' );

require_once( './referer_logs.php'  );

// 退会処理
require_once( '../dao/WithdrawalDao.php' );
require_once( '../dto/Withdrawal.php' );

// GETパラメータbidがセットされているか
if(isset($_GET['bid']) && $_GET['bid'] != ''){

	// オブジェクト生成
	// DB接続クラス生成
	$common_dao = new CommonDao();
	// action_log_daoクラス生成
	$action_log_dao = new ActionLogDao();
	// action_logクラス生成
	$advert_dao = new AdvertDao();
	// media_daoクラス生成
	$media_dao = new MediaDao();
	// withdrawal_daoクラス生成
	$withdrawal_dao = new WithdrawalDao();


	// GET送信で受け取ったパラメータを取得
	$bid = $_GET['bid'];

	// エラーフラグ
	$error_flag = 0;
	// 結果メッセージ
	$result_message = "";

	// 退会ログを取得
	$db_result = $withdrawal_dao->getActionLogBySessionId($bid);
	if($db_result){
		header("Content-Type: text/html; charset=UTF-8");
		// 退会ログが既に存在してた場合
		echo "error001<br />"
		. "セッションID:" . $bid . "のデータは既に退会しています。";
		exit();
	}


	//受け取ったセッションIDからレコードを取得
	// action_logクラス生成
	$action_log = new ActionLog();
	// bidを条件にaction_logからレコードを取得
	$action_log = $action_log_dao->getActionLogBySessionId($bid);
	if(!is_null($action_log)) {	//登録されているレコードか確認
		// 該当レコードが存在する

		// アクションが上がっていない場合
		if($action_log->getStatus() == '1') {
			header("Content-Type: text/html; charset=UTF-8");
			echo "error002<br />"
			. "セッションID:" . $bid . "のデータは成果になっておりません。";
			exit();
		}

		// 後でクラスを使用する
		$get_select_sql = " SELECT "
		. " advert_client_id "
		. " FROM "
		. " advert_withdrawal_view_user "
		. " WHERE "
		. " advert_client_id = '" . $action_log->getAdvertClientId() . "' "
		. " AND "
		. " status = '2' "
		. " AND "
		. " deleted_at IS NULL ";


		$db_result = $common_dao->db_query($get_select_sql);

		if(!$db_result) {
			header("Content-Type: text/html; charset=UTF-8");
			echo "error003<br />"
			. "退会レポートを取得する許可がありません。";
			exit();
		}


		// bidを変数へ代入
		$session_id = $bid;
		// 広告IDを取得
		$advert_id = $action_log->getAdvertId();
		// 媒体IDを取得
		$media_id = $action_log->getMediaId();
		// キャリアIDを取得
		$carrier_id = $action_log->getCarrierId();
		// 個体識別を取得
		$uid = $action_log->getUid();
		// ポイントバックパラメータ(キックバックパラメータ)を取得
		$point_back_parameter = $action_log->getPointBackParameter();
		// ポイントバックURL(キックバックURL)を取得
		$point_back_url = $action_log->getPointBackUrl();
		// アクション日付を取得
		$action_complete_date = $action_log->getActionCompleteDate();

		// 本日の日付を取得
		// 本日の年月日時分秒を取得
		$ac = date("YmdHis");

		// 英文形式の日付をタイムスタンプに変換し取得
		$c_date = strtotime($action_complete_date);
		// 登録日付の年を取得
		$c_year = date("Y", $c_date);
		// 登録日付の月を取得
		$c_month = date("m", $c_date);
		// 登録日付の日を取得
		$c_day = date("d", $c_date);
		// 登録日付の時を取得
		$c_hour = date("H", $c_date);
		// 登録日付の分を取得
		$c_minute = date("i", $c_date);
		// 登録日付の秒を取得
		$c_second = date("s", $c_date);


		// 英文形式の日付をタイムスタンプに変換し取得
		$a_date = strtotime($ac);
		// ac日付の年を取得
		$a_year = date("Y", $a_date);
		// ac日付の月を取得
		$a_month = date("m", $a_date);
		// ac日付の日を取得
		$a_day = date("d", $a_date);
		// ac日付の時を取得
		$a_hour = date("H", $a_date);
		// ac日付の分を取得
		$a_minute = date("i", $a_date);
		// ac日付の秒を取得
		$a_second = date("s", $a_date);

		// アクション年月と本日の年月が等しいか
		if($c_year . $c_month == $a_year . $a_month) {
			// 退会表示
			$status = 2;

		} else {
			// 退会非表示
			$status = 1;

		}

		// acを変数へ格納
		$created_at = $ac;

	}


	// エラーフラグが0か
	if($error_flag == 0) {
		// トランザクション スタート
		$withdrawal_dao->transaction_start();

		// action_logクラスのget/setメソッドに値をセット
		// 登録日付をセット
		$action_log->setCreatedAt($created_at);
		// ステータス2をセット
		$action_log->setStatus($status);

		//Updateを実行
//		$db_result = $withdrawal_dao->UpdateActionLog($action_log, $result_message);
		//Insertを実行
		$db_result = $withdrawal_dao->insertActionLog($action_log, $result_message);

		// DB結果
		if($db_result) {
			// トランザクションエンド
			$withdrawal_dao->transaction_end();

		} else {
			// トランザクションロールバック
			$withdrawal_dao->transaction_rollback();
			exit();
		}
	}

}else{
	exit();
}

function compareDate($year1, $month1, $day1, $year2, $month2, $day2) {
    $dt1 = mktime(0, 0, 0, $month1, $day1, $year1);
    $dt2 = mktime(0, 0, 0, $month2, $day2, $year2);
    $diff = $dt1 - $dt2;
    $diffDay = $diff / 86400;//1日は86400秒
    return $diffDay;
}
?>