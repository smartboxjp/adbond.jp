<?php
// *****************************************************************
// リファラを取得するクラス
// *****************************************************************
// DB接続ファイル
require_once( '../common/CommonDao.php'  );

class C_refererLogs {

	// -------------------------------------------------------------
	// リファラを取得
	// $status 1：クリック 2：アクション
	// $advert_id 広告ID
	// $carrier_id キャリアID
	// $referer リファラー
	// -------------------------------------------------------------
	public function M_getReferer($status, $advert_id, $media_id, $carrier_id, $referer){

		// DB接続クラス生成
		$common_dao = new CommonDao();

		// プライマリキー
		$sql = " INSERT INTO referer_logs values ('', "
		// advert_id
		. " ' ". $advert_id ." ', "
		// media_id
		. " ' ". $media_id ." ', "
		// carrier_id
		. " ' ". $carrier_id ." ', "
		// referer
		. " ' ". $referer ." ', "
		// status
		. " ' ". $status ." ', "
		// created_at delete_at
		. " Now(), NULL) ";

		// insert実行
		$db_result = $common_dao->db_update($sql);

		// テスト出力
//		echo $sql;
//		echo "<br />";

		return $db_result;
	}
}

?>