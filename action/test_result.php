<?php
// 共通設定
require_once( '../common/CommonAdminBase.php' );
require_once( '../common/CommonDao.php' );
require_once( '../common/CommonFunc.php' );
require_once( '../dao/ActionLogDao.php' );
require_once( '../dto/ActionLog.php' );
require_once( '../dao/PointBackLogDao.php' );
require_once( '../dto/PointBackLog.php' );
require_once( '../dao/MediaDao.php' );
require_once( '../dto/Media.php' );
require_once( '../dao/AdvertDao.php' );
require_once( '../dto/Advert.php' );



if(isset($_GET['bid']) && $_GET['bid'] != ''){
	$common_dao = new CommonDao();
	$action_log_dao = new ActionLogDao();
	$advert_dao = new AdvertDao();
	$media_dao = new MediaDao();




	$bid = $_GET['bid'];

	//------------------------------------------------------
	//7/21 追加 auidをGETで受け取る
	$auid = $_GET['auid'];
	//------------------------------------------------------

	$error_flag = 0;
	$result_msg = "";

	//受け取ったセッションIDからレコードを取得
	$action_log = new ActionLog();
	$action_log = $action_log_dao->getActionLogBySessionId($bid);
	if(!is_null($action_log)) {	//登録されているレコードか確認
		$session_id = $bid;
		$advert_id = $action_log->getAdvertId();
		$media_id = $action_log->getMediaId();
		$uid = $action_log->getUid();
		$point_back_parameter = $action_log->getPointBackParameter();
		$point_back_url = $action_log->getPointBackUrl();
		$created_at = $action_log->getCreatedAt();

		$advert = new Advert();

						if($advert_id == '221'){
						$res_id = $bid;

					}

		if(!is_null($advert)) {
			$unique_click_type = $advert->getUniqueClickType();
			$test_flag = $advert->getTestFlag();

			if($test_flag != 1) {
				if($unique_click_type == 1) {
					$monthly = date("Ym");
					$sql = " SELECT * FROM action_logs "
							. " WHERE deleted_at is NULL "
							. " AND status = 2 "
							. " AND advert_id = '$advert_id' "
							. " AND media_id = '$media_id' "
							. " AND uid = '$uid' "
							. " AND DATE_FORMAT(action_complete_date,'%Y%m') = '$monthly' ";

					$db_result = $common_dao->db_query($sql);
					if($db_result) {
						$error_flag = 1;
					} else {
					}
				} elseif($unique_click_type == 2) {

					$sql = " SELECT * FROM action_logs "
							. " WHERE deleted_at is NULL "
							. " AND status = 2 "
							. " AND advert_id = '$advert_id' "
							. " AND media_id = '$media_id' "
							. " AND uid = '$uid' "
							. " AND action_complete_date > DATE_SUB(NOW(), INTERVAL 7 DAY) ";

					$db_result = $common_dao->db_query($sql);
					if($db_result) {
						$error_flag = 1;
					} else {
					}
				} elseif($unique_click_type == 3) {
					$daily = date("Ymd");
					$sql = " SELECT * FROM action_logs "
							. " WHERE deleted_at is NULL "
							. " AND status = 2 "
							. " AND advert_id = '$advert_id' "
							. " AND media_id = '$media_id' "
							. " AND uid = '$uid' "
							. " AND DATE_FORMAT(action_complete_date,'%Y%m%d') = '$daily' ";

					$db_result = $common_dao->db_query($sql);
					if($db_result) {
						$error_flag = 1;
					} else {
					}
				} elseif($unique_click_type == 4) {
					$daily = date("Ymd");
					$sql = " SELECT * FROM action_logs "
							. " WHERE deleted_at is NULL "
							. " AND status = 2 "
							. " AND advert_id = '$advert_id' "
							. " AND media_id = '$media_id' "
							. " AND uid = '$uid' ";

					$db_result = $common_dao->db_query($sql);
					if($db_result) {
						$error_flag = 1;
					} else {
					}
				}
			}
		} else {
			$error_flag = 1;
		}

		$media = new Media();
		$media = $media_dao->getMediaById($media_id);

		if(!is_null($media)) {
			$response_type = $media->getResponseType();
		} else {
			$error_flag = 1;
		}

		if(isset($_GET['ac']) && $_GET['ac'] != "") {
			$ac = $_GET['ac'];

			if(strlen($ac) == 14) {
				$c_date = strtotime($action_log->getCreatedAt());
				$c_year = date("Y", $c_date);
				$c_month = date("m", $c_date);
				$c_day = date("d", $c_date);
				$c_hour = date("H", $c_date);
				$c_minute = date("i", $c_date);
				$c_second = date("s", $c_date);

				$a_date = strtotime($ac);
				$a_year = date("Y", $a_date);
				$a_month = date("m", $a_date);
				$a_day = date("d", $a_date);
				$a_hour = date("H", $a_date);
				$a_minute = date("i", $a_date);
				$a_second = date("s", $a_date);

				if(mktime($c_hour, $c_minute, $c_second, $c_month, $c_day, $c_year) <= mktime($a_hour, $a_minute, $a_second, $a_month, $a_day, $a_year)) {
					$action_complete_date = $ac;
				} else {
					$error_flag = 1;
				}
			} else {
				$error_flag = 1;
			}
		} else {
		//acが無い場合
			$ac = date("YmdHis");

			$c_date = strtotime($action_log->getCreatedAt());
			$c_year = date("Y", $c_date);
			$c_month = date("m", $c_date);
			$c_day = date("d", $c_date);
			$c_hour = date("H", $c_date);
			$c_minute = date("i", $c_date);
			$c_second = date("s", $c_date);

			$a_date = strtotime($ac);
			$a_year = date("Y", $a_date);
			$a_month = date("m", $a_date);
			$a_day = date("d", $a_date);
			$a_hour = date("H", $a_date);
			$a_minute = date("i", $a_date);
			$a_second = date("s", $a_date);

			if(mktime($c_hour, $c_minute, $c_second, $c_month, $c_day, $c_year) <= mktime($a_hour, $a_minute, $a_second, $a_month, $a_day, $a_year)) {
				$action_complete_date = $ac;
			} else {
				$error_flag = 1;
			}
		}
	} else {
		$error_flag = 1;
	}



	//-------------------------------------------
	// 7/21 追加 auidが重複しているか
	if(isset($_GET['auid']) && $_GET['auid'] != ''){
		$sql = " SELECT * FROM action_logs "
				. " WHERE auid = '$auid'";

		$db_result = $action_log_dao->getAuidLog($sql);
		if($db_result){
			$error_flag = 1;
			$result_msg = "Duplicate the AUID<br />";
		}else{

		}
	}else{

	}
	//-------------------------------------------


	if($error_flag == 0) {



//		$session_id = $action_log->getSessionId();
		$carrier_id = $action_log->getCarrierId();
		$user_agent = $action_log->getUserAgent();
//		$uid = $action_log->getUid();
		$ip_address = $action_log->getIpAddress();
		$host_name = $action_log->getHostName();
//		$media_id = $action_log->getMediaId();
		$media_publisher_id = $action_log->getMediaPublisherId();
//		$advert_id = $action_log->getAdvertId();
		$advert_client_id = $action_log->getAdvertClientId();
		$click_price_client = $action_log->getClickPriceClient();
		$click_price_media = $action_log->getClickPriceMedia();
		$action_price_client = $action_log->getActionPriceClient();
		$action_price_media = $action_log->getActionPriceMedia();
		$advert_url = $action_log->getLinkUrl();
//		$point_back_parameter = $action_log->getPointBackParameter();
		$point_back_url = $action_log->getPointBackUrl();


		$action_log_dao->transaction_start();
		//--------------------------------------------
		$action_log = new ActionLog();
		$action_log->setSessionId($session_id);
		$action_log->setCarrierId($carrier_id);
		$action_log->setUserAgent($user_agent);
		$action_log->setUid($uid);
		$action_log->setIpAddress($ip_address);
		$action_log->setHostName($host_name);
		$action_log->setMediaId($media_id);
		$action_log->setMediaPublisherId($media_publisher_id);
		$action_log->setAdvertId($advert_id);
		$action_log->setAdvertClientId($advert_client_id);
		$action_log->setClickPriceClient($click_price_client);
		$action_log->setClickPriceMedia($click_price_media);
		$action_log->setActionPriceClient($action_price_client);
		$action_log->setActionPriceMedia($action_price_media);
		$action_log->setLinkUrl($advert_url);
		$action_log->setPointBackParameter($point_back_parameter);
		$action_log->setPointBackUrl($point_back_url);
		//-------------------------------------------
		// 7/21 追加
		$action_log->setAuid($auid);
		//-------------------------------------------

		$action_log->setActionCompleteDate($action_complete_date);
		$action_log->setStatus("2");
		//-------------------------------------------

		echo $action_log->getSessionId();
		echo "<br />";
		echo $action_log->getCarrierId();
		echo "<br />";
		echo $action_log->getUserAgent();
		echo "<br />";
		echo $action_log->getUid();
		echo "<br />";
		echo $action_log->getIpAddress();
		echo "<br />";
		echo $action_log->getHostName();
		echo "<br />";
		echo $action_log->getMediaId();
		echo "<br />";
		echo $action_log->getMediaPublisherId();
		echo "<br />";
		echo $action_log->getAdvertId();
		echo "<br />";
		echo $action_log->getAdvertClientId();
		echo "<br />";
		echo $action_log->getClickPriceClient();
		echo "<br />";
		echo $action_log->getClickPriceMedia();
		echo "<br />";
		echo $action_log->getActionPriceClient();
		echo "<br />";
		echo $action_log->getActionPriceMedia();
		echo "<br />";
		echo $action_log->getLinkUrl();
		echo "<br />";
		echo $action_log->getPointBackParameter();
		echo "<br />";
		echo $action_log->getPointBackUrl();
		echo "<br />";
		echo $action_log->getActionCompleteDate();
		echo "<br />";
		echo $action_log->getStatus();


		//Updateを実行
//		$db_result = $action_log_dao->UpdateActionLog($action_log, $result_message);
//
//		if($db_result) {
//
//			$action_log_dao->transaction_end();
//
//			if($point_back_url != "") {
//
//				//ポイントバック成果通知処理
//				//$point_back_url = $action_log->getPointBackUrl();
//
//				if($response_type == 1) {
//					$res_id = $point_back_parameter;
//
////					//きせかえリッチ (どうじんらんど2、フラゲー気分)
////					if($media_id == '26' or $media_id == '13'){
////						$res_id = $bid;
////
////					}
//				} else {
//
//					$res_id = urlencode($uid);
//				}
//
//				$point_back_url = ereg_replace("##ID##", $res_id, $point_back_url);
//				$point_back_url = ereg_replace("##CID##", $action_log->getAdvertId(), $point_back_url);
//				$point_back_url = ereg_replace("##CLICK_DATE##", date("YmdHis", $action_log->getCreatedAt()), $point_back_url);
//				$point_back_url = ereg_replace("##ACTION_DATE##", date("YmdHis", $action_log->getActionCompleteDate()), $point_back_url);
//				$point_back_url = ereg_replace("##AUID##", $action_log->getAuid(), $point_back_url);
//
//				$url_array = parse_url($point_back_url);
//
//				if(isset($url_array['query'])){
//
//					$query = "?" . $url_array['query'];
//				}
//
//				$host = $url_array['host'];
//				$path = $url_array['path'] . $query;
//				if(isset($_GET['auid']) && $_GET['auid'] != ''){
//					$path = $url_array['path'] . $query . "&auid=" . $auid;
//				}
//
//				if($path === ""){
//					$path = "/";
//				}
//
//				$port = 80;             // HTTP なので80
//				$timeout = 30;             // 接続に失敗した場合の待ち時間
//
//				$pb_status = 1;
//				$sock = fsockopen($host, $port, $errno, $errstr, $timeout);  // サーバに接続する
//				if($sock === FALSE){    // 接続に失敗したらメッセージを表示し、終了させる
//					echo "SOCK OPEN ERROR<br>";
//					$pb_status = 2;
//				} else {
//					// HTTP ヘッダ部分の送信になる。
//					fwrite($sock, "GET http://" . $host . $path . " HTTP/1.0\r\n");
//					// ヘッダの終了を通知
//					fwrite($sock, "\r\n\r\n");
//
//				}
//				fclose($sock);
//
//				//echo $host.$path;
//
//				//ポイントバック通知結果をデータベースに登録
//				$point_back_log_dao = new PointBackLogDao();
//				$point_back_log = new PointBackLog();
//
//				$point_back_log_dao->transaction_start();
//
//				$point_back_log->setSessionId($session_id);
//				$point_back_log->setMediaId($media_id);
//				$point_back_log->setAdvertId($advert_id);
//
//				$point_back_log->setPointBackParameter($uid);
//				$point_back_log->setPointBackUrl($point_back_url);
//				$point_back_log->setStatus($pb_status);
//
//				//INSERTを実行
//				$db_result = $point_back_log_dao->insertPointBackLog($point_back_log, $result_message);
//
//				if($db_result) {
//
//					$point_back_log_dao->transaction_end();
//					exit();
//				} else {
//
//					$point_back_log_dao->transaction_rollback();
//					exit();
//				}
//			}
//		} else {
//
//			$action_log_dao->transaction_rollback();
//			exit();
//
//		}
//
	}else{

	}

}else{
	exit();
}

function compareDate($year1, $month1, $day1, $year2, $month2, $day2) {
    $dt1 = mktime(0, 0, 0, $month1, $day1, $year1);
    $dt2 = mktime(0, 0, 0, $month2, $day2, $year2);
    $diff = $dt1 - $dt2;
    $diffDay = $diff / 86400;//1日は86400秒
    return $diffDay;
}
?>