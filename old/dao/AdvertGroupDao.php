<?php
class AdvertGroupDao extends CommonDao{

	//コンストラクタ
	function __construct(){
		parent::__construct();
	}

	//デストラクタ
	function __destruct(){
		parent::__destruct();
	}

	//全データの取得
	public function getAllAdvertGroup(){
		is_null($this->mysqli) and $this->connect();
		$result = $this->mysqli->query(" SELECT * FROM advert_groups WHERE deleted_at is NULL ");

		$record_array = array();

		while($row = $result->fetch_array(MYSQLI_ASSOC)){
			$record = new AdvertGroup();
			$record->setId($row["id"]);
			$record->setName($row["name"]);
			$record->setCreatedAt($row["created_at"]);
			$record->setUpdatedAt($row["updated_at"]);
			$record_array[] = $record;
		}
		$result->close();
		return $record_array;
	}

	//指定されたデータの取得
	private function getAdvertGroup($sql){
		is_null($this->mysqli) and $this->connect();
		$result = $this->mysqli->query($sql);

		$record = null;

		if($result->num_rows != 0){
			$row = $result->fetch_array(MYSQLI_ASSOC);
			$record = new AdvertGroup();
			$record->setId($row["id"]);
			$record->setName($row["name"]);
			$record->setCreatedAt($row["created_at"]);
			$record->setUpdatedAt($row["updated_at"]);
		}
		$result->close();
		return $record;
	}

	//idでデータを取得
	public function getAdvertGroupById($id){
		$id = $this->mysqli->real_escape_string($id);
		$sql = " SELECT * FROM advert_groups WHERE id = '$id' AND deleted_at is NULL ";
		return $this->getAdvertGroup($sql);
	}

	//nameでデータを取得
	public function getAdvertGroupByName($name){
		$name = $this->mysqli->real_escape_string($name);
		$sql = " SELECT * FROM advert_groups WHERE name = '$name' AND deleted_at is NULL ";
		return $this->getAdvertGroup($sql);
	}

	//データの更新
	public function updateAdvertGroup($record, &$result_message = ""){
		$record = $this->escapeStringAdvertGroup($record);
		is_null($this->mysqli) and $this->connect();
		$sql = " UPDATE advert_groups SET "
			. " name = '" . $record->getName() . "' "
			. " WHERE id = '" . $record->getId() . "' AND deleted_at is NULL ";
		if(!$this->mysqli->query($sql)) {
			$result_message = "更新に失敗しました" . $this->mysqli->error . "\n";
			return false;
		}
		$result_message = "更新しました";
		return true;
	}

	//新規データの登録
	public function insertAdvertGroup($record, &$result_message = ""){
		$record = $this->escapeStringAdvertGroup($record);
		is_null($this->mysqli) and $this->connect();
		//idカラムに空を挿入することで，auto_incrementによるカウントアップ値
		//を代入できる
		$sql = " INSERT INTO advert_groups values ('', "
			. " '" . $record->getName() . "', "
			. " Now(), Now(), NULL) ";
		if(!$this->mysqli->query($sql)){
			$result_message = "登録に失敗しました" . $this->mysqli->error . "\n";
			return false;
		}
		$result_message = "一件追加しました";
		return true;
	}

	//データの削除
	public function deleteAdvertGroup($id, &$result_message = ""){
		$id = $this->mysqli->real_escape_string($id);
		is_null($this->mysqli) and $this->connect();

		$sql = "UPDATE advert_groups SET deleted_at = Now() "
			. " WHERE id = '$id' and deleted_at is NULL ";
		if(!$this->mysqli->query($sql)){
			$result_message = "削除に失敗しました" . $this->mysqli->error . "\n";
			return false;
		}
		$result_message = "削除しました";
		return true;
	}

	//LoginUserクラスの各プロパティから特殊文字をエスケープ
	private function escapeStringAdvertGroup($record){
		$record->setId($this->mysqli->real_escape_string($record->getId()));
		$record->setName($this->mysqli->real_escape_string($record->getName()));
		$record->setCreatedAt($this->mysqli->real_escape_string($record->getCreatedAt()));
		$record->setUpdatedAt($this->mysqli->real_escape_string($record->getUpdatedAt()));
		$record->setDeletedAt($this->mysqli->real_escape_string($record->getDeletedAt()));
		return $record;
	}
}
?>