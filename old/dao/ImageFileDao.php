<?php
class ImageFileDao extends CommonDao{

	//コンストラクタ
	function __construct(){
		parent::__construct();
	}

	//デストラクタ
	function __destruct(){
		parent::__destruct();
	}

	//全データの取得
	public function getAllImageFile(){
		is_null($this->mysqli) and $this->connect();
		$result = $this->mysqli->query(" SELECT * FROM image_files WHERE deleted_at is NULL ");

		$record_array = array();

		while($row = $result->fetch_array(MYSQLI_ASSOC)){
			$record = new ImageFile();
			$record->setId($row["id"]);
			$record->setOriginalName($row["original_name"]);
			$record->setFileName($row["file_name"]);
			$record->setFileType($row["file_type"]);
			$record->setImageTag($row["image_tag"]);
			$record->setCreatedAt($row["created_at"]);
			$record_array[] = $record;
		}
		$result->close();
		return $record_array;
	}

	//指定されたデータの取得
	private function getImageFile($sql){
		is_null($this->mysqli) and $this->connect();
		$result = $this->mysqli->query($sql);

		$record = null;

		if($result->num_rows != 0){
			$row = $result->fetch_array(MYSQLI_ASSOC);
			$record = new ImageFile();
			$record->setId($row["id"]);
			$record->setOriginalName($row["original_name"]);
			$record->setFileName($row["file_name"]);
			$record->setFileType($row["file_type"]);
			$record->setImageTag($row["image_tag"]);
			$record->setCreatedAt($row["created_at"]);
		}
		$result->close();
		return $record;
	}

	//idでデータを取得
	public function getImageFileById($id){
		$id = $this->mysqli->real_escape_string($id);
		$sql = " SELECT * FROM image_files WHERE id = '$id' AND deleted_at is NULL ";
		return $this->getImageFile($sql);
	}

	//file_nameでデータを取得
	public function getImageFileByFileName($file_name){
		$file_name = $this->mysqli->real_escape_string($file_name);
		$sql = " SELECT * FROM image_files WHERE file_name = '$file_name' AND deleted_at is NULL ";
		return $this->getImageFile($sql);
	}

	//データの更新
	public function updateImageFile($record, &$result_message = ""){
		$record = $this->escapeStringImageFile($record);
		is_null($this->mysqli) and $this->connect();
		$sql = " UPDATE image_files SET "
			. " original_name = '" . $record->getOriginalName() . "', "
			. " file_name = '" . $record->getFileName() . "', "
			. " file_type = '" . $record->getFileType() . "', "
			. " image_tag = '" . $record->getImageTag() . "' "
			. " WHERE id = '" . $record->getId() . "' AND deleted_at is NULL ";
		if(!$this->mysqli->query($sql)) {
			$result_message = "更新に失敗しました" . $this->mysqli->error . "\n";
			return false;
		}
		$result_message = "更新しました";
		return true;
	}

	//新規データの登録
	public function insertImageFile($record, &$result_message = ""){
		$record = $this->escapeStringImageFile($record);
		is_null($this->mysqli) and $this->connect();
		//idカラムに空を挿入することで，auto_incrementによるカウントアップ値
		//を代入できる
		$sql = " INSERT INTO image_files values ('', "
			. " '" . $record->getOriginalName() . "', "
			. " '" . $record->getFileName() . "', "
			. " '" . $record->getFileType() . "', "
			. " '" . $record->getImageTag() . "', "
			. " Now(), NULL) ";
		if(!$this->mysqli->query($sql)){
			$result_message = "登録に失敗しました" . $this->mysqli->error . "\n";
			return false;
		}
		$result_message = "一件追加しました";
		return true;
	}

	//データの削除
	public function deleteImageFile($id, &$result_message = ""){
		$id = $this->mysqli->real_escape_string($id);
		is_null($this->mysqli) and $this->connect();

		$sql = "UPDATE image_files SET deleted_at = Now() "
			. " WHERE id = '$id' and deleted_at is NULL ";
		if(!$this->mysqli->query($sql)){
			$result_message = "削除に失敗しました" . $this->mysqli->error . "\n";
			return false;
		}
		$result_message = "削除しました";
		return true;
	}

	//LoginUserクラスの各プロパティから特殊文字をエスケープ
	private function escapeStringImageFile($record){
		$record->setId($this->mysqli->real_escape_string($record->getId()));
		$record->setOriginalName($this->mysqli->real_escape_string($record->getOriginalName()));
		$record->setFileName($this->mysqli->real_escape_string($record->getFileName()));
		$record->setFileType($this->mysqli->real_escape_string($record->getFileType()));
		$record->setImageTag($this->mysqli->real_escape_string($record->getImageTag()));
		$record->setCreatedAt($this->mysqli->real_escape_string($record->getCreatedAt()));
		$record->setDeletedAt($this->mysqli->real_escape_string($record->getDeletedAt()));
		return $record;
	}
}
?>