<?php
class LoginUserDao extends CommonDao{

	//コンストラクタ
	function __construct(){
		parent::__construct();
	}

	//デストラクタ
	function __destruct(){
		parent::__destruct();
	}

	//全データの取得
	public function getAllLoginUser(){
		is_null($this->mysqli) and $this->connect();
		$result = $this->mysqli->query(" SELECT * FROM login_users WHERE deleted_at is NULL ");

		$record_array = array();

		while($row = $result->fetch_array(MYSQLI_ASSOC)){
			$record = new LoginUser();
			$record->setId($row["id"]);
			$record->setUserName($row["user_name"]);
			$record->setLoginId($row["login_id"]);
			$record->setLoginPass($row["login_pass"]);
			$record->setLastLoginAt($row["last_login_at"]);
			$record->setCreatedAt($row["created_at"]);
			$record->setUpdatedAt($row["updated_at"]);
			$record_array[] = $record;
		}
		$result->close();
		return $record_array;
	}

	//指定されたデータの取得
	private function getLoginUser($sql){
		is_null($this->mysqli) and $this->connect();
		$result = $this->mysqli->query($sql);

		$record = null;

		if($result->num_rows != 0){
			$row = $result->fetch_array(MYSQLI_ASSOC);
			$record = new LoginUser();
			$record->setId($row["id"]);
			$record->setUserName($row["user_name"]);
			$record->setLoginId($row["login_id"]);
			$record->setLoginPass($row["login_pass"]);
			$record->setLastLoginAt($row["last_login_at"]);
			$record->setCreatedAt($row["created_at"]);
			$record->setUpdatedAt($row["updated_at"]);
		}
		$result->close();
		return $record;
	}

	//idでデータを取得
	public function getLoginUserById($id){
		$id = $this->mysqli->real_escape_string($id);
		$sql = " SELECT * FROM login_users WHERE id = '$id' and deleted_at is NULL ";
		return $this->getLoginUser($sql);
	}

	//user_nameでデータを取得
	public function getLoginUserByUserName($name){
		$name = $this->mysqli->real_escape_string($name);
		$sql = " SELECT * FROM login_users WHERE user_name = '$name' and deleted_at is NULL ";
		return $this->getLoginUser($sql);
	}

	//login_idでデータを取得
	public function getLoginUserByLoginId($login_id){
		$login_id = $this->mysqli->real_escape_string($login_id);
		$sql = " SELECT * FROM login_users WHERE login_id = '$login_id' and deleted_at is NULL ";
		return $this->getLoginUser($sql);
	}

	//ログインid/pass情報取得
	public function getLoginUserByIdPass($login_id, $login_pass){
		$login_id = $this->mysqli->real_escape_string($login_id);
		$login_pass = $this->mysqli->real_escape_string($login_pass);
		$sql = " SELECT * FROM login_users WHERE login_id = '$login_id' and login_pass = '$login_pass' and deleted_at is NULL ";
		return $this->getLoginUser($sql);
	}

	//データの更新
	public function updateLoginUser($record, &$result_message = ""){
		$record = $this->escapeStringLoginUser($record);
		is_null($this->mysqli) and $this->connect();
		$sql = "UPDATE login_users SET "
			. " user_name = '" . $record->getUserName() . "', "
			. " login_id = '" . $record->getLoginId() . "', "
			. " login_pass = '" . $record->getLoginPass() . "', "
			. " last_login_at = '" . $record->getLastLoginAt() . "' "
			. " WHERE id = '" . $record->getId() . "' and deleted_at is NULL";
		if(!$this->mysqli->query($sql)){
			$result_message = "更新に失敗しました" . $this->mysqli->error . "\n";
			return false;
		}
		$result_message = "更新しました";
		return true;
	}

	//新規データの登録
	public function insertLoginUser($record, &$result_message = ""){
		$record = $this->escapeStringLoginUser($record);
		is_null($this->mysqli) and $this->connect();
		//idカラムに空を挿入することで，auto_incrementによるカウントアップ値
		//を代入できる
		$sql = " INSERT INTO login_users values ('', "
			. " '" . $record->getUserName() . "', "
			. " '" . $record->getLoginId() . "', "
			. " '" . $record->getLoginPass() . "', "
			. " '" . $record->getLastLoginAt() . "', "
			. " Now(), Now(), NULL) ";
		if(!$this->mysqli->query($sql)){
			$result_message = "登録に失敗しました" . $this->mysqli->error . "\n";
			return false;
		}
		$result_message = "一件追加しました";
		return true;
	}

	//データの削除
	public function deleteLoginUser($id, &$result_message = ""){
		$id = $this->mysqli->real_escape_string($id);
		is_null($this->mysqli) and $this->connect();

		$sql = " UPDATE login_users SET deleted_at = Now() "
			. " WHERE id = '$id' and deleted_at is NULL ";
		if(!$this->mysqli->query($sql)){
			$result_message = "削除に失敗しました" . $this->mysqli->error . "\n";
			return false;
		}
		$result_message = "削除しました";
		return true;
	}

	//LoginUserクラスの各プロパティから特殊文字をエスケープ
	private function escapeStringLoginUser($record){
		$record->setId($this->mysqli->real_escape_string($record->getId()));
		$record->setUserName($this->mysqli->real_escape_string($record->getUserName()));
		$record->setLoginId($this->mysqli->real_escape_string($record->getLoginId()));
		$record->setLoginPass($this->mysqli->real_escape_string($record->getLoginPass()));
		$record->setLastLoginAt($this->mysqli->real_escape_string($record->getLastLoginAt()));
		$record->setCreatedAt($this->mysqli->real_escape_string($record->getCreatedAt()));
		$record->setUpdatedAt($this->mysqli->real_escape_string($record->getUpdatedAt()));
		$record->setDeletedAt($this->mysqli->real_escape_string($record->getDeletedAt()));
		return $record;
	}
}
?>