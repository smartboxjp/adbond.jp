{include file='./header.tpl' page_title='全体集計'}

<!-- Menu -->
{include file='./menu.tpl'}

<div id="my_contents">

<h2>全体集計</h2>

<div id=message>
{if $error_message != '' }
<div id="error_message">
	<h3>ERROR:{$error_message}</h3>
</div>
{/if}
{if $info_message != '' }
<div id="info_message">
	<h3>INFO:{$info_message}</h3>
</div>
{/if}
</div><!-- message -->

<form method="POST" action="{$smarty.server.PHP_SELF}">
</form>

<table cellpadding="0" cellspacing="0">
	<tr>
		<th>年月</th>
		<th>売上額</th>
		<th>支払い額</th>
		<th>報酬手数料</th>
		<th>クリック数</th>
		<th>アクション数</th>
	</tr>
{foreach from=$list key="key" item="data" name="list"}
	<tr>
		<td><a href="./summary_all_day.php?month={$data.month}">{$data.summary_date}</a></td>
		<td>&yen;{$data.sales|number_format}</td>
		<td>&yen;{$data.amounts|number_format}</td>
		<td>&yen;{$data.fees|number_format}</td>
		<td>{$data.click_count|number_format}</td>
		<td>{$data.action_count|number_format}</td>
	</tr>
{/foreach}
</table>

</div><!-- contents -->

{include file='./hooter.tpl'}