{include file='./header.tpl' page_title='媒体別集計'}

<!-- Menu -->
{include file='./menu.tpl'}

<div id="my_contents">

<h2>媒体別集計</h2>

<div id=message>
{if $error_message != '' }
<div id="error_message">
	<h3>ERROR:{$error_message}</h3>
</div>
{/if}
{if $info_message != '' }
<div id="info_message">
	<h3>INFO:{$info_message}</h3>
</div>
{/if}
</div><!-- message -->

<form method="POST" action="{$smarty.server.PHP_SELF}">
</form>

<table cellpadding="0" cellspacing="0">
	<tr>
		<th colspan="5">【該当{$list_count}件】</th>
	</tr>
	<tr>
		<th>媒体名</th>
		<th>媒体発行者名</th>
		<th>クリック数</th>
		<th>アクション数</th>
		<th>金額合計</th>
	</tr>
{foreach from=$list key="key" item="data" name="list"}
	<tr>
		<td><a href="./media.php?mode=edit&id={$data.media_id}">{$data.media_name|htmlspecialchars:$smarty.const.ENT_QUOTES}</a></td>
		<td><a href="./media_publisher.php?mode=edit&id={$data.media_publisher_id}">{$data.publisher_name|htmlspecialchars:$smarty.const.ENT_QUOTES}</a></td>
		<td><a href="./summary_click_detail.php?m_id={$data.media_id}&mp_id={$data.media_publisher_id}">{$data.click_count|number_format}</a></td>
		<td><a href="./summary_action_detail.php?m_id={$data.media_id}&mp_id={$data.media_publisher_id}">{$data.action_count|number_format}</a></td>
		<td>\{$data.total_price|number_format}</td>
	</tr>
{/foreach}
</table>

</div><!-- contents -->

{include file='./hooter.tpl'}