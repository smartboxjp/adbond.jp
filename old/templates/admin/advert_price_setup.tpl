{include file='./header.tpl' page_title='媒体別単価設定'}

<!-- Menu -->
{include file='./menu.tpl'}

<div id="my_contents">

<h2>媒体別単価設定</h2>

<div id=message>
{if $error_message != ''}
<div id="error_message">
<h3>ERROR:{$error_message}</h3>
</div>
{/if}
{if $info_message != ''}
<div id="info_message">
<h3>INFO:{$info_message}</h3>
</div>
{/if}
</div><!-- message -->

<form method="POST" action="{$smarty.server.PHP_SELF}">
<table cellpadding="0" cellspacing="0">
{foreach from=$media_category_array item="data" name="media_category_list"}
	<tr>
		<td>
			<a href="?advert_id={$advert_id}&category_id={$data.id}">{$data.name|htmlspecialchars:$smarty.const.ENT_QUOTES}</a>
		</td>
	</tr>
{/foreach}
</table>
{if $category_id != ""}
<table cellpadding="0" cellspacing="0">
	<tr>
		<td>{$media_category_array[$category_id].name|htmlspecialchars:$smarty.const.ENT_QUOTES}</td>
	</tr>
{foreach from=$media_array item="data" name="media_publisher_list"}
	<tr>
		<td>
			<a href="?advert_id={$advert_id}&category_id={$category_id}&publisher_id={$data.publisher_id}">{$data.publisher_name|htmlspecialchars:$smarty.const.ENT_QUOTES}</a>
		</td>
	</tr>
{/foreach}
</table>
{if $publisher_id != ""}
<table cellpadding="0" cellspacing="0">
	<tr>
		<td>{$media_array[$publisher_id].publisher_name|htmlspecialchars:$smarty.const.ENT_QUOTES}</td>
	</tr>
{foreach from=$media_array[$publisher_id].media item="data" name="media_list"}
	<tr>
		<td>
			<a href="?advert_id={$advert_id}&category_id={$category_id}&publisher_id={$publisher_id}&media_id={$data.media_id}">{$data.media_name|htmlspecialchars:$smarty.const.ENT_QUOTES}</a>
		</td>
	</tr>
{/foreach}
</table>
{if $media_id != ""}
<table cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3">{$media_array[$publisher_id].media[$media_id].media_name|htmlspecialchars:$smarty.const.ENT_QUOTES}</td>
	</tr>
	<tr>
		<th colspan="2">クリック単価(クライアント)</th>
		<td>
			<input type="text" size="10" name="click_price_client" value="{$form_data.click_price_client|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
		</td>
	</tr>
	<tr>
		<th colspan="2">クリック単価(メディア)</th>
		<td>
			<input type="text" size="10" name="click_price_media" value="{$form_data.click_price_media|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
		</td>
	</tr>
	<tr>
		<th colspan="2">アクション単価(金額)(クライアント)</th>
		<td>
			[成果1]
			docomo:<input type="text" size="10" name="action_price_client_docomo_1" value="{$form_data.action_price_client_docomo_1|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
			softbank:<input type="text" size="10" name="action_price_client_softbank_1" value="{$form_data.action_price_client_softbank_1|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
			au:<input type="text" size="10" name="action_price_client_au_1" value="{$form_data.action_price_client_au_1|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
			pc:<input type="text" size="10" name="action_price_client_pc_1" value="{$form_data.action_price_client_pc_1|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円<br />
			[成果2]
			docomo:<input type="text" size="10" name="action_price_client_docomo_2" value="{$form_data.action_price_client_docomo_2|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
			softbank:<input type="text" size="10" name="action_price_client_softbank_2" value="{$form_data.action_price_client_softbank_2|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
			au:<input type="text" size="10" name="action_price_client_au_2" value="{$form_data.action_price_client_au_2|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
			pc:<input type="text" size="10" name="action_price_client_pc_2" value="{$form_data.action_price_client_pc_2|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円<br />
			[成果3]
			docomo:<input type="text" size="10" name="action_price_client_docomo_3" value="{$form_data.action_price_client_docomo_3|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
			softbank:<input type="text" size="10" name="action_price_client_softbank_3" value="{$form_data.action_price_client_softbank_3|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
			au:<input type="text" size="10" name="action_price_client_au_3" value="{$form_data.action_price_client_au_3|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
			pc:<input type="text" size="10" name="action_price_client_pc_3" value="{$form_data.action_price_client_pc_3|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円<br />
			[成果4]
			docomo:<input type="text" size="10" name="action_price_client_docomo_4" value="{$form_data.action_price_client_docomo_4|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
			softbank:<input type="text" size="10" name="action_price_client_softbank_4" value="{$form_data.action_price_client_softbank_4|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
			au:<input type="text" size="10" name="action_price_client_au_4" value="{$form_data.action_price_client_au_4|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
			pc:<input type="text" size="10" name="action_price_client_pc_4" value="{$form_data.action_price_client_pc_4|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円<br />
			[成果5]
			docomo:<input type="text" size="10" name="action_price_client_docomo_5" value="{$form_data.action_price_client_docomo_5|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
			softbank:<input type="text" size="10" name="action_price_client_softbank_5" value="{$form_data.action_price_client_softbank_5|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
			au:<input type="text" size="10" name="action_price_client_au_5" value="{$form_data.action_price_client_au_5|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
			pc:<input type="text" size="10" name="action_price_client_pc_5" value="{$form_data.action_price_client_pc_5|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円<br />
		</td>
	</tr>
	<tr>
		<th colspan="2">アクション単価(金額)(メディア)</th>
		<td>
			[成果1]
			docomo:<input type="text" size="10" name="action_price_media_docomo_1" value="{$form_data.action_price_media_docomo_1|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
			softbank:<input type="text" size="10" name="action_price_media_softbank_1" value="{$form_data.action_price_media_softbank_1|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
			au:<input type="text" size="10" name="action_price_media_au_1" value="{$form_data.action_price_media_au_1|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
			pc:<input type="text" size="10" name="action_price_media_pc_1" value="{$form_data.action_price_media_pc_1|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円<br />
			[成果2]
			docomo:<input type="text" size="10" name="action_price_media_docomo_2" value="{$form_data.action_price_media_docomo_2|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
			softbank:<input type="text" size="10" name="action_price_media_softbank_2" value="{$form_data.action_price_media_softbank_2|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
			au:<input type="text" size="10" name="action_price_media_au_2" value="{$form_data.action_price_media_au_2|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
			pc:<input type="text" size="10" name="action_price_media_pc_2" value="{$form_data.action_price_media_pc_2|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円<br />
			[成果3]
			docomo:<input type="text" size="10" name="action_price_media_docomo_3" value="{$form_data.action_price_media_docomo_3|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
			softbank:<input type="text" size="10" name="action_price_media_softbank_3" value="{$form_data.action_price_media_softbank_3|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
			au:<input type="text" size="10" name="action_price_media_au_3" value="{$form_data.action_price_media_au_3|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
			pc:<input type="text" size="10" name="action_price_media_pc_3" value="{$form_data.action_price_media_pc_3|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円<br />
			[成果4]
			docomo:<input type="text" size="10" name="action_price_media_docomo_4" value="{$form_data.action_price_media_docomo_4|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
			softbank:<input type="text" size="10" name="action_price_media_softbank_4" value="{$form_data.action_price_media_softbank_4|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
			au:<input type="text" size="10" name="action_price_media_au_4" value="{$form_data.action_price_media_au_4|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
			pc:<input type="text" size="10" name="action_price_media_pc_4" value="{$form_data.action_price_media_pc_4|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円<br />
			[成果5]
			docomo:<input type="text" size="10" name="action_price_media_docomo_5" value="{$form_data.action_price_media_docomo_5|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
			softbank:<input type="text" size="10" name="action_price_media_softbank_5" value="{$form_data.action_price_media_softbank_5|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
			au:<input type="text" size="10" name="action_price_media_au_5" value="{$form_data.action_price_media_au_5|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
			pc:<input type="text" size="10" name="action_price_media_pc_5" value="{$form_data.action_price_media_pc_5|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円<br />
		</td>
	</tr>
</table>
<table cellpadding="0" cellspacing="0">
	<tr>
		<td>
			<input type="submit" value="設定" />
			<input type="hidden" name="mode" value="setup" />
			<input type="hidden" name="advert_id" value="{$advert_id}" />
			<input type="hidden" name="category_id" value="{$category_id}" />
			<input type="hidden" name="publisher_id" value="{$publisher_id}" />
			<input type="hidden" name="media_id" value="{$media_id}" />
			<input type="hidden" name="id" value="{$form_data.id}" />
		</td>
	</tr>
</table>
{/if}
{/if}
{/if}
</form>
</div><!-- contents -->

{include file='./hooter.tpl'}