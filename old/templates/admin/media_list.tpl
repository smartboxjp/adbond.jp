{include file='./header.tpl' page_title='媒体一覧'}

<!-- Menu -->
{include file='./menu.tpl'}

<div id="my_contents">

<h2>媒体一覧</h2>

<div id=message>
{if $error_message != '' }
<div id="error_message">
	<h3>ERROR:{$error_message}</h3>
</div>
{/if}
{if $info_message != '' }
<div id="info_message">
	<h3>INFO:{$info_message}</h3>
</div>
{/if}
</div><!-- message -->

<form method="POST" action="{$smarty.server.PHP_SELF}">
<table cellpadding="0" cellspacing="0">
	<tr>
		<td>検索方法</td>
		<td>
			<input type="radio" name="method" value="1"{if $search.method == 1 || $search.method == ""} checked="checked"{/if} /><label>完全一致</label>
			<input type="radio" name="method" value="2"{if $search.method == 2} checked="checked"{/if} /><label>前方一致</label>
			<input type="radio" name="method" value="3"{if $search.method == 3} checked="checked"{/if} /><label>後方一致</label>
			<input type="radio" name="method" value="4"{if $search.method == 4} checked="checked"{/if} /><label>あいまい</label>
			<input type="checkbox" name="reverse" value="1"{if $search.reverse == 1} checked="checked"{/if} /><label>条件反転</label>
		</td>
	</tr>
	<tr>
		<td>ソート</td>
		<td>
			<select name="sort">
				<option value="id"{if $search.sort == "id"} selected="selected"{/if}>媒体ID</option>
				<option value="media_name"{if $search.sort == "media_name"} selected="selected"{/if}>媒体サイト名</option>
				<option value="created_at"{if $search.sort == "created_at"} selected="selected"{/if}>登録日時</option>
			</select>
			<input type="radio" name="order" value="ASC"{if $search.order == "ASC" || $search.method == ""} checked="checked"{/if} /><label>昇順</label>
			<input type="radio" name="order" value="DESC"{if $search.order == "DESC"} checked="checked"{/if} /><label>降順</label>
		</td>
	</tr>
	<tr>
		<td>媒体ID</td>
		<td><input type="text" size="50" name="search_id" value="{$search.id}" /></td>
	</tr>
	<tr>
		<td>媒体サイト名</td>
		<td><input type="text" size="50" name="search_name" value="{$search.name|htmlspecialchars:$smarty.const.ENT_QUOTES}" /></td>
	</tr>
	<tr>
		<td>登録日時</td>
		<td>
			<input type="checkbox" name="created_at_flag" value="1"{if $search.created_at_flag == 1} checked="checked"{/if} />
			<select name="s_year">
{section name=cnt start=2009 loop=2021}
				<option value="{$smarty.section.cnt.index}"{if $set_s_year == $smarty.section.cnt.index} selected="selected"{/if}>{$smarty.section.cnt.index}年</option>
{/section}
			</select>
			<select name="s_month">
{section name=cnt start=1 loop=13}
				<option value="{$smarty.section.cnt.index}"{if $set_s_month == $smarty.section.cnt.index} selected="selected"{/if}>{$smarty.section.cnt.index}月</option>
{/section}
			</select>
			<select name="s_day">
{section name=cnt start=1 loop=32}
				<option value="{$smarty.section.cnt.index}"{if $set_s_day == $smarty.section.cnt.index} selected="selected"{/if}>{$smarty.section.cnt.index}日</option>
{/section}
			</select> ～
			<select name="e_year">
{section name=cnt start=2009 loop=2021}
				<option value="{$smarty.section.cnt.index}"{if $set_e_year == $smarty.section.cnt.index} selected="selected"{/if}>{$smarty.section.cnt.index}年</option>
{/section}
			</select>
			<select name="e_month">
{section name=cnt start=1 loop=13}
				<option value="{$smarty.section.cnt.index}"{if $set_e_month == $smarty.section.cnt.index} selected="selected"{/if}>{$smarty.section.cnt.index}月</option>
{/section}
			</select>
			<select name="e_day">
{section name=cnt start=1 loop=32}
				<option value="{$smarty.section.cnt.index}"{if $set_e_day == $smarty.section.cnt.index} selected="selected"{/if}>{$smarty.section.cnt.index}日</option>
{/section}
			</select>
		</td>
	</tr>
	<tr>
		<td colspan="3">
			<input type="submit" value="検索" />
			<input type="hidden" name="mode" value="search" />
		</td>
	</tr>
</table>
</form>

<table cellpadding="0" cellspacing="0">
	<tr>
		<th colspan="6">【該当{$list_count}件】</th>
	</tr>
	<tr>
		<th>ID</th>
		<th>媒体発行者名</th>
		<th>サイト名</th>
		<th>ステータス</th>
		<th>登録日時</th>
		<th>
			<form method="POST" action="{$smarty.server.PHP_SELF}">
				<input type="submit" value="新規登録" />
				<input type="hidden" name="mode" value="new_regist" />
			</form>
		</th>
	</tr>
{foreach from=$list item="data" name="list"}
	<tr>
		<td>{$data.id}</td>
		<td>{$data.publisher_name|htmlspecialchars:$smarty.const.ENT_QUOTES}</td>
		<td>{$data.media_name|htmlspecialchars:$smarty.const.ENT_QUOTES}</td>
		<td>
{if $data.status == 1}
			仮登録
{elseif $data.status == 2}
			正規
{elseif $data.status == 3}
			退会
{/if}
		</td>
		<td>{$data.created_at}</td>
		<td>
			<form method="POST" action="{$smarty.server.PHP_SELF}">
				<input type="submit" value="詳細" />
				<input type="hidden" name="mode" value="edit" />
				<input type="hidden" name="id" value="{$data.id}" />
			</form>
			<form method="POST" action="{$smarty.server.PHP_SELF}">
				<input type="submit" value="削除" onclick="return confirm('削除してよろしいですか？');" />
				<input type="hidden" name="mode" value="delete" />
				<input type="hidden" name="id" value="{$data.id}" />
			</form>
		</td>
	</tr>
{/foreach}
</table>

</div><!-- contents -->

{include file='./hooter.tpl'}