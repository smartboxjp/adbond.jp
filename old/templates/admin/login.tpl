{include file='./header.tpl' page_title='Login'}

<div id=message>
{if $error_message != ''}
<div id="error_message">
<h3>ERROR:{$error_message}</h3>
</div>
{/if}
{if $info_message != ''}
<div id="info_message">
<h3>INFO:{$info_message}</h3>
</div>
{/if}
</div><!-- message -->

	<div id="login">
		<form action="#" method="POST">
			<input type="hidden" name="logon" value="yy" />
			<table cellspacing="0">
				<tr>
					<th>login:</th>
					<td><input type="text" name="login_id" value="" /></td>
				</tr>
				<tr>
					<th>password:</th>
					<td><input type="password" name="login_pass" value="" /></td>
				</tr>
				<tr>
					<td colspan="2" id="login-btn">
						<input type="submit" value="login" />
						<input type="reset" />
					</td>
				</tr>
			</table>
		</form>
	</div>

{include file='./hooter.tpl'}