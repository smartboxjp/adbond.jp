{include file='./header.tpl' page_title='広告別集計'}

<!-- Menu -->
{include file='./menu.tpl'}

<div id="my_contents">

<h2>広告別集計</h2>

<div id=message>
{if $error_message != '' }
<div id="error_message">
	<h3>ERROR:{$error_message}</h3>
</div>
{/if}
{if $info_message != '' }
<div id="info_message">
	<h3>INFO:{$info_message}</h3>
</div>
{/if}
</div><!-- message -->

<form method="POST" action="{$smarty.server.PHP_SELF}">
</form>

<table cellpadding="0" cellspacing="0">
	<tr>
		<th colspan="5">【該当{$list_count}件】</th>
	</tr>
	<tr>
		<th>広告名</th>
		<th>広告主名</th>
		<th>クリック数</th>
		<th>アクション数</th>
		<th>金額合計</th>
	</tr>
{foreach from=$list key="key" item="data" name="list"}
	<tr>
		<td><a href="./advert.php?mode=edit&id={$data.advert_id}">{$data.advert_name|htmlspecialchars:$smarty.const.ENT_QUOTES}</a></td>
		<td><a href="./advert_client.php?mode=edit&id={$data.advert_client_id}">{$data.client_name|htmlspecialchars:$smarty.const.ENT_QUOTES}</a></td>
		<td><a href="./summary_click_detail.php?a_id={$data.advert_id}&ac_id={$data.advert_client_id}">{$data.click_count|number_format}</a></td>
		<td><a href="./summary_action_detail.php?a_id={$data.advert_id}&ac_id={$data.advert_client_id}">{$data.action_count|number_format}</a></td>
		<td>\{$data.total_price|number_format}</td>
	</tr>
{/foreach}
</table>

</div><!-- contents -->

{include file='./hooter.tpl'}