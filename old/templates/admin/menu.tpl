<div id="my_navigation">

	<p><a href="./login.php?logout=y">ログアウト</a></p>

	<h4 alt="1">■媒体管理</h4>
	<ul>
		<li><a href="./media_publisher.php">媒体発行者管理</a></li>
		<li><a href="./media_group.php">媒体発行者グループ管理</a></li>
		<li><a href="./media.php">媒体管理</a></li>
		<li><a href="./media_category.php">媒体カテゴリー管理</a></li>
	</ul>

	<h4 alt="2">■広告管理</h4>
	<ul>
		<li><a href="./advert_client.php">広告主管理</a></li>
		<li><a href="./advert_group.php">広告主グループ管理</a></li>
		<li><a href="./advert.php">広告管理</a></li>
		<li><a href="./advert_category.php">広告カテゴリー管理</a></li>
	</ul>
<!--
	<h4 alt="3">■ASP管理</h4>
	<ul>
		<li><a href="./asp.php">ASP管理</a></li>
	</ul>
-->
	<h4 alt="4">■集計</h4>
	<ul>
		<li><a href="./summary_all.php">全体集計</a></li>
		<li><a href="./summary_media_publisher.php">媒体発行者別集計</a></li>
		<li><a href="./summary_media.php">媒体別集計</a></li>
		<li><a href="./summary_advert_client.php">広告主別集計</a></li>
		<li><a href="./summary_advert.php">広告別集計</a></li>
	</ul>

	<h4 alt="5">■ログ</h4>
	<ul>
		<li><a href="./result_log.php">成果ログ</a></li>
	</ul>

</div>
