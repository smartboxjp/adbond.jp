{include file='./header.tpl' page_title='成果ログ'}

<!-- Menu -->
{include file='./menu.tpl'}

<div id="my_contents">

<h2>成果ログ</h2>

<div id=message>
{if $error_message != '' }
<div id="error_message">
	<h3>ERROR:{$error_message}</h3>
</div>
{/if}
{if $info_message != '' }
<div id="info_message">
	<h3>INFO:{$info_message}</h3>
</div>
{/if}
</div><!-- message -->

<form method="POST" action="{$smarty.server.PHP_SELF}">
<table cellpadding="0" cellspacing="0">
	<tr>
		<td>集計日時</td>
		<td>
			<select name="s_year">
{section name=cnt start=2009 loop=2021}
				<option value="{$smarty.section.cnt.index}"{if $set_s_year == $smarty.section.cnt.index} selected="selected"{/if}>{$smarty.section.cnt.index}年</option>
{/section}
			</select>
			<select name="s_month">
{section name=cnt start=1 loop=13}
				<option value="{$smarty.section.cnt.index}"{if $set_s_month == $smarty.section.cnt.index} selected="selected"{/if}>{$smarty.section.cnt.index}月</option>
{/section}
			</select>
		</td>
	</tr>
	<tr>
		<td>ログ種類</td>
		<td>
			<input type="radio" name="log_type" value="1"{if $log_type == 1 || $log_type == ""} checked="checked"{/if} /><label>クリックログ</label>
			<input type="radio" name="log_type" value="2"{if $log_type == 2} checked="checked"{/if} /><label>アクションログ</label>
			<input type="radio" name="log_type" value="3"{if $log_type == 3} checked="checked"{/if} /><label>ポイント通知ログ</label>
		</td>
	</tr>
	<tr>
		<td colspan="3">
			<input type="submit" value="検索" />
			<input type="hidden" name="mode" value="search" />
		</td>
	</tr>
</table>
</form>

{if !is_null($list) }
<table cellpadding="0" cellspacing="0">
	<tr>
		<th colspan="3">
			<input type="button" value="{$set_s_year}年{$set_s_month}月分CSVダウンロード" onclick="window.open('./result_log_download.php?date={$search_date}&type={$log_type}&monthly=1')" />
		</th>
	</tr>
	<tr>
		<th>日付</th>
		<th>件数</th>
		<th></th>
	</tr>
{foreach from=$list key="key" item="data" name="list"}
	<tr>
		<td>{$data.log_date}</td>
		<td>{$data.log_count}</td>
		<td>
			<form method="POST" action="{$smarty.server.PHP_SELF}">
				<input type="submit" value="詳細" />
				<input type="hidden" name="mode" value="detail" />
				<input type="hidden" name="download_date" value="{$data.download_date}" />
				<input type="hidden" name="log_type" value="{$log_type}" />
			</form>
			<input type="button" value="CSVダウンロード" onclick="window.open('./result_log_download.php?date={$data.download_date}&type={$log_type}')" />
		</td>
	</tr>
{/foreach}
</table>
{/if}

</div><!-- contents -->

{include file='./hooter.tpl'}