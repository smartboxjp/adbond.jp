{include file='./header.tpl' page_title='クリックログ詳細'}

<!-- Menu -->
{include file='./menu.tpl'}

<div id="my_contents">

<h2>クリックログ詳細</h2>

<div id=message>
{if $error_message != '' }
<div id="error_message">
	<h3>ERROR:{$error_message}</h3>
</div>
{/if}
{if $info_message != '' }
<div id="info_message">
	<h3>INFO:{$info_message}</h3>
</div>
{/if}
</div><!-- message -->

<form method="POST" action="{$smarty.server.PHP_SELF}">
</form>

<table cellpadding="0" cellspacing="0">
	<tr>
		<th colspan="10">【該当{$list_count}件】</th>
	</tr>
	<tr>
		<th>クリック日時</th>
		<th>媒体名</th>
		<th>広告名</th>
		<th>キャリア</th>
		<th>ユーザーエージェント</th>
		<th>個体識別番号</th>
		<th>IPアドレス</th>
		<th>ホスト名</th>
		<th>クリック単価</th>
		<th>ステータス</th>
	</tr>
{foreach from=$list key="key" item="data" name="list"}
	<tr>
		<td>{$data.created_at}</td>
		<td><a href="./media.php?mode=edit&id={$data.media_id}">{$data.media_name|htmlspecialchars:$smarty.const.ENT_QUOTES}</a></td>
		<td><a href="./advert.php?mode=edit&id={$data.advert_id}">{$data.advert_name|htmlspecialchars:$smarty.const.ENT_QUOTES}</a></td>
		<td>{$data.carrier}</td>
		<td>{$data.user_agent|htmlspecialchars:$smarty.const.ENT_QUOTES}</td>
		<td>{$data.uid|htmlspecialchars:$smarty.const.ENT_QUOTES}</td>
		<td>{$data.ip_address}</td>
		<td>{$data.host_name}</td>
		<td>&yen;{$data.click_price_client|number_format}</td>
		<td>{$data.status_show}</td>
	</tr>
{/foreach}
</table>

</div><!-- contents -->

{include file='./hooter.tpl'}