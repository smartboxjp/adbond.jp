{include file='./header.tpl' page_title='媒体カテゴリー('|cat:$sub_title|cat:')'}

<!-- Menu -->
{include file='./menu.tpl'}

<div id="my_contents">

<h2>媒体カテゴリー({$sub_title})</h2>

<div id=message>
{if $error_message != ''}
<div id="error_message">
<h3>ERROR:{$error_message}</h3>
</div>
{/if}
{if $info_message != ''}
<div id="info_message">
<h3>INFO:{$info_message}</h3>
</div>
{/if}
</div><!-- message -->

<form method="POST" action="{$smarty.server.PHP_SELF}">
<table cellpadding="0" cellspacing="0">
	<tr>
		<th colspan="2">媒体カテゴリー名</th>
		<td>
			<input type="text" size="50" name="name" value="{$form_data.name|htmlspecialchars:$smarty.const.ENT_QUOTES}" />
		</td>
	</tr>
	<tr>
		<td colspan="3">
			<input type="submit" value="登録" />
			<input type="hidden" name="mode" value="{$mode}" />
			<input type="hidden" name="id" value="{$form_data.id}" />
		</td>
	</tr>
</table>
</form>

</div><!-- contents -->

{include file='./hooter.tpl'}