{include file='./header.tpl' page_title='メディア管理'}

<!-- Menu -->
{include file='./media_menu.tpl'}

<div id="my_contents">

<h2>登録情報</h2>

<div id=message>
{if $error_message != '' }
<div id="error_message">
	<h3>ERROR:{$error_message}</h3>
</div>
{/if}
{if $info_message != '' }
<div id="info_message">
	<h3>INFO:{$info_message}</h3>
</div>
{/if}
</div><!-- message -->

<table cellpadding="0" cellspacing="0">
	<tr>
		<th colspan="2">登録者情報</th>
	</tr>
	<tr>
		<th>ログインID</th>
		<td>{$data.login_id|htmlspecialchars:$smarty.const.ENT_QUOTES}</td>
	</tr>
	<tr>
		<th>パスワード</th>
		<td>{$data.login_pass|htmlspecialchars:$smarty.const.ENT_QUOTES}</td>
	</tr>
	<tr>
		<th>企業名/個人名</th>
		<td>{$data.publisher_name|htmlspecialchars:$smarty.const.ENT_QUOTES}</td>
	</tr>
	<tr>
		<th>担当者名 ※法人の場合のみ</th>
		<td>{$data.contact_person|htmlspecialchars:$smarty.const.ENT_QUOTES}</td>
	</tr>
	<tr>
		<th>TEL</th>
		<td>{$data.tel|htmlspecialchars:$smarty.const.ENT_QUOTES}</td>
	</tr>
	<tr>
		<th>FAX</th>
		<td>{$data.fax|htmlspecialchars:$smarty.const.ENT_QUOTES}</td>
	</tr>
	<tr>
		<th>メールアドレス</th>
		<td>{$data.email|htmlspecialchars:$smarty.const.ENT_QUOTES}</td>
	</tr>
	<tr>
		<td colspan="2">
			<form method="POST" action="{$smarty.server.PHP_SELF}">
				<input type="submit" value="編集" />
				<input type="hidden" name="mode" value="user_edit" />
			</form>
		</td>
	</tr>
</table>

<table cellpadding="0" cellspacing="0">
	<tr>
		<th colspan="2">口座情報</th>
	</tr>
	<tr>
		<th>振込先銀行名</th>
		<td>{$data.bank_name|htmlspecialchars:$smarty.const.ENT_QUOTES}</td>
	</tr>
	<tr>
		<th>支店名</th>
		<td>{$data.branch_name|htmlspecialchars:$smarty.const.ENT_QUOTES}</td>
	</tr>
	<tr>
		<th>口座種別</th>
		<td>
{if $data.account_type == 1}
			普通
{elseif $data.account_type == 2}
			当座
{/if}
		</td>
	</tr>
	<tr>
		<th>口座名義</th>
		<td>{$data.account_holder|htmlspecialchars:$smarty.const.ENT_QUOTES}</td>
	</tr>
	<tr>
		<th>口座番号</th>
		<td>{$data.account_number|htmlspecialchars:$smarty.const.ENT_QUOTES}</td>
	</tr>
	<tr>
		<td colspan="2">
			<form method="POST" action="{$smarty.server.PHP_SELF}">
				<input type="submit" value="編集" />
				<input type="hidden" name="mode" value="account_edit" />
			</form>
		</td>
	</tr>
</table>

<table cellpadding="0" cellspacing="0">
	<tr>
		<th colspan="2">登録メディア一覧</th>
	</tr>
{foreach from=$media_array item="data" name="media_list"}
	<tr>
		<th>{$data.media_name|htmlspecialchars:$smarty.const.ENT_QUOTES}</th>
		<td>
			<form method="POST" action="./info_media.php">
				<input type="submit" value="編集" />
				<input type="hidden" name="mode" value="edit" />
				<input type="hidden" name="id" value="{$data.id}" />
			</form>
		</td>
	</tr>
{/foreach}
	<tr>
		<td colspan="2">
			<form method="POST" action="./info_media.php">
				<input type="submit" value="追加" />
				<input type="hidden" name="mode" value="new_regist" />
			</form>
		</td>
	</tr>
</table>
</div><!-- contents -->

<!-- フッター -->
{include file='./hooter.tpl'}