{include file='./header.tpl' page_title='メディア管理'}

<!-- Menu -->
{include file='./media_menu.tpl'}

<div id="my_contents">

<h2>登録情報</h2>

<div id=message>
{if $error_message != '' }
<div id="error_message">
	<h3>ERROR:{$error_message}</h3>
</div>
{/if}
{if $info_message != '' }
<div id="info_message">
	<h3>INFO:{$info_message}</h3>
</div>
{/if}
</div><!-- message -->

<form method="POST" action="{$smarty.server.PHP_SELF}">
<table cellpadding="0" cellspacing="0">
	<tr>
		<th colspan="2">口座情報</th>
	</tr>
	<tr>
		<th>振込先銀行名</th>
		<td><input type="text" size="50" name="bank_name" value="{$data.bank_name|htmlspecialchars:$smarty.const.ENT_QUOTES}" /></td>
	</tr>
	<tr>
		<th>支店名</th>
		<td><input type="text" size="50" name="branch_name" value="{$data.branch_name|htmlspecialchars:$smarty.const.ENT_QUOTES}" /></td>
	</tr>
	<tr>
		<th>口座種別</th>
		<td>
			<input type="radio" name="account_type" value="1"{if $data.account_type == 1} checked="checked"{/if} /><label>普通</label>
			<input type="radio" name="account_type" value="2"{if $data.account_type == 2} checked="checked"{/if} /><label>当座</label>
		</td>
	</tr>
	<tr>
		<th>口座名義</th>
		<td><input type="text" size="50" name="account_holder" value="{$data.account_holder|htmlspecialchars:$smarty.const.ENT_QUOTES}" /></td>
	</tr>
	<tr>
		<th>口座番号</th>
		<td><input type="text" size="50" name="account_number" value="{$data.account_number|htmlspecialchars:$smarty.const.ENT_QUOTES}" /></td>
	</tr>
	<tr>
		<td colspan="2">
			<input type="submit" value="登録" />
			<input type="hidden" name="mode" value="account_update" />
		</td>
	</tr>
</table>
</form>
</div><!-- contents -->

<!-- フッター -->
{include file='./hooter.tpl'}