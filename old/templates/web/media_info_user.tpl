{include file='./header.tpl' page_title='メディア管理'}

<!-- Menu -->
{include file='./media_menu.tpl'}

<div id="my_contents">

<h2>登録情報</h2>

<div id=message>
{if $error_message != '' }
<div id="error_message">
	<h3>ERROR:{$error_message}</h3>
</div>
{/if}
{if $info_message != '' }
<div id="info_message">
	<h3>INFO:{$info_message}</h3>
</div>
{/if}
</div><!-- message -->

<form method="POST" action="{$smarty.server.PHP_SELF}">
<table cellpadding="0" cellspacing="0">
	<tr>
		<th colspan="2">登録者情報</th>
	</tr>
	<tr>
		<th>ログインID</th>
		<td><input type="text" size="20" name="login_id" value="{$data.login_id|htmlspecialchars:$smarty.const.ENT_QUOTES}" /></td>
	</tr>
	<tr>
		<th>パスワード</th>
		<td><input type="text" size="20" name="login_pass" value="{$data.login_pass|htmlspecialchars:$smarty.const.ENT_QUOTES}" /></td>
	</tr>
	<tr>
		<th>企業名/個人名</th>
		<td><input type="text" size="50" name="publisher_name" value="{$data.publisher_name|htmlspecialchars:$smarty.const.ENT_QUOTES}" /></td>
	</tr>
	<tr>
		<th>担当者名 ※法人の場合のみ</th>
		<td><input type="text" size="50" name="contact_person" value="{$data.contact_person|htmlspecialchars:$smarty.const.ENT_QUOTES}" /></td>
	</tr>
	<tr>
		<th>TEL</th>
		<td><input type="text" size="30" name="tel" value="{$data.tel|htmlspecialchars:$smarty.const.ENT_QUOTES}" /></td>
	</tr>
	<tr>
		<th>FAX</th>
		<td><input type="text" size="30" name="fax" value="{$data.fax|htmlspecialchars:$smarty.const.ENT_QUOTES}" /></td>
	</tr>
	<tr>
		<th>メールアドレス</th>
		<td><input type="text" size="30" name="email" value="{$data.email|htmlspecialchars:$smarty.const.ENT_QUOTES}" /></td>
	</tr>
	<tr>
		<td colspan="2">
			<input type="submit" value="登録" />
			<input type="hidden" name="mode" value="user_update" />
		</td>
	</tr>
</table>
</form>
</div><!-- contents -->

<!-- フッター -->
{include file='./hooter.tpl'}