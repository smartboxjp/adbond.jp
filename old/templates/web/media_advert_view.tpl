{include file='./header.tpl' page_title='メディア管理'}

<!-- Menu -->
{include file='./media_menu.tpl' user_name=$user_name}

<div id="my_contents">

<h2>広告検索</h2>

<div id=message>
{if $error_message != '' }
<div id="error_message">
	<h3>ERROR:{$error_message}</h3>
</div>
{/if}
{if $info_message != '' }
<div id="info_message">
	<h3>INFO:{$info_message}</h3>
</div>
{/if}
</div><!-- message -->

<table cellpadding="0" cellspacing="0">
	<tr>
		<th colspan="3">広告情報</th>
	</tr>
	<tr>
		<th colspan="2">広告主</th>
		<td>{$data.advert_client_name|htmlspecialchars:$smarty.const.ENT_QUOTES}</td>
	</tr>
	<tr>
		<th colspan="2">広告カテゴリー</th>
		<td>{$data.advert_category_name|htmlspecialchars:$smarty.const.ENT_QUOTES}</td>
	</tr>
	<tr>
		<th colspan="2">広告名</th>
		<td>{$data.advert_name|htmlspecialchars:$smarty.const.ENT_QUOTES}</td>
	</tr>
	<tr>
		<th colspan="2">コンテンツ種別</th>
		<td>
{if $data.content_type == 1}
			一般
{elseif $data.content_type == 2}
			公式
{/if}
		</td>
	</tr>
	<tr>
		<th colspan="2">対応キャリア</th>
		<td>
			{if $data.support_docomo == 1} docomo{/if}
			{if $data.support_softbank == 1} softbank{/if}
			{if $data.support_au == 1} au{/if}
			{if $data.support_pc == 1} pc{/if}
		</td>
	</tr>
	<tr>
		<th colspan="2">広告概要</th>
		<td>{$data.site_outline|htmlspecialchars:$smarty.const.ENT_QUOTES}</td>
	</tr>
	<tr>
		<th colspan="2">クリック単価</th>
		<td>&yen;{$data.click_price_media|number_format}</td>
	</tr>
	<tr>
		<th colspan="2">アクション単価</th>
		<td>
			docomo:&yen;{$data.action_price_media_docomo_1|number_format}
			softbank:&yen;{$data.action_price_media_softbank_1|number_format}
			au:&yen;{$data.action_price_media_au_1|number_format}
			pc:&yen;{$data.action_price_media_pc_1|number_format}
		</td>
	</tr>
	<tr>
		<th colspan="2">ポイントバック</th>
		<td>
			{if $data.point_back_flag == 1}不可{/if}
			{if $data.point_back_flag == 2}可{/if}
		</td>
	</tr>
	<tr>
		<th colspan="2">アダルト</th>
		<td>
			{if $data.adult_flag == 1}不可{/if}
			{if $data.adult_flag == 2}可{/if}
		</td>
	</tr>
	<tr>
		<th colspan="2">出会い</th>
		<td>
			{if $data.dating_flag == 1}不可{/if}
			{if $data.dating_flag == 2}可{/if}
		</td>
	</tr>
	<tr>
		<th colspan="2">出稿開始日</th>
		<td>{$data.advert_start_date}</td>
	</tr>
	<tr>
		<th colspan="2">出稿終了日</th>
		<td>
{if $data.unrestraint_flag == 1}
		無期限
{else}
		{$data.advert_end_date}
{/if}
		</td>
	</tr>
	<tr>
		<th colspan="2">提携状態</th>
		<td>
			{if $connect.status == 1 || $connect.status == ""}未提携{/if}
			{if $connect.status == 2}提携済み{/if}
		</td>
	</tr>
	<tr>
		<td colspan="3">
			<form method="POST" action="{$smarty.server.PHP_SELF}">
				<input type="submit" value="提携申請" />
				<input type="hidden" name="mode" value="connect" />
				<input type="hidden" name="media_id" value="{$media_id}" />
				<input type="hidden" name="advert_id" value="{$advert_id}" />
			</form>
		</td>
	</tr>
</table>
<table cellpadding="0" cellspacing="0">
	<tr>
		<th colspan="3">広告原稿</th>
	</tr>
{if $data.ms_text_1 != ""}
	<tr>
		<th colspan="2">テキスト1</th>
		<td>{$data.ms_text_1|htmlspecialchars:$smarty.const.ENT_QUOTES}</td>
	</tr>
{/if}
{if $data.ms_text_2 != ""}
	<tr>
		<th colspan="2">テキスト2</th>
		<td>{$data.ms_text_2|htmlspecialchars:$smarty.const.ENT_QUOTES}</td>
	</tr>
{/if}
{if $data.ms_text_3 != ""}
	<tr>
		<th colspan="2">テキスト3</th>
		<td>{$data.ms_text_3|htmlspecialchars:$smarty.const.ENT_QUOTES}</td>
	</tr>
{/if}
{if $data.ms_text_4 != ""}
	<tr>
		<th colspan="2">テキスト4</th>
		<td>{$data.ms_text_4|htmlspecialchars:$smarty.const.ENT_QUOTES}</td>
	</tr>
{/if}
{if $data.ms_text_5 != ""}
	<tr>
		<th colspan="2">テキスト5</th>
		<td>{$data.ms_text_5|htmlspecialchars:$smarty.const.ENT_QUOTES}</td>
	</tr>
{/if}
{if $data.ms_email_1 != ""}
	<tr>
		<th colspan="2">メール1</th>
		<td>{$data.ms_email_1|htmlspecialchars:$smarty.const.ENT_QUOTES}</td>
	</tr>
{/if}
{if $data.ms_email_2 != ""}
	<tr>
		<th colspan="2">メール2</th>
		<td>{$data.ms_email_2|htmlspecialchars:$smarty.const.ENT_QUOTES}</td>
	</tr>
{/if}
{if $data.ms_email_3 != ""}
	<tr>
		<th colspan="2">メール3</th>
		<td>{$data.ms_email_3|htmlspecialchars:$smarty.const.ENT_QUOTES}</td>
	</tr>
{/if}
{if $data.ms_email_4 != ""}
	<tr>
		<th colspan="2">メール4</th>
		<td>{$data.ms_email_4|htmlspecialchars:$smarty.const.ENT_QUOTES}</td>
	</tr>
{/if}
{if $data.ms_email_5 != ""}
	<tr>
		<th colspan="2">メール5</th>
		<td>{$data.ms_email_5|htmlspecialchars:$smarty.const.ENT_QUOTES}</td>
	</tr>
{/if}
{if $ms_image_path_1 != ""}
	<tr>
		<th colspan="2">イメージ1</th>
		<td><img src="{$ms_image_path_1}" alt="イメージ1" /></td>
	</tr>
{/if}
{if $ms_image_path_2 != ""}
	<tr>
		<th colspan="2">イメージ2</th>
		<td><img src="{$ms_image_path_2}" alt="イメージ2" /></td>
	</tr>
{/if}
{if $ms_image_path_3 != ""}
	<tr>
		<th colspan="2">イメージ3</th>
		<td><img src="{$ms_image_path_3}" alt="イメージ3" /></td>
	</tr>
{/if}
{if $ms_image_path_4 != ""}
	<tr>
		<th colspan="2">イメージ4</th>
		<td><img src="{$ms_image_path_4}" alt="イメージ4" /></td>
	</tr>
{/if}
{if $ms_image_path_5 != ""}
	<tr>
		<th colspan="2">イメージ5</th>
		<td><img src="{$ms_image_path_5}" alt="イメージ5" /></td>
	</tr>
{/if}
</table>
</div><!-- contents -->

<!-- フッター -->
{include file='./hooter.tpl'}