{include file='./header.tpl' page_title='メディア管理'}

<!-- Menu -->
{include file='./media_menu.tpl'}

<div id="my_contents">

<h2>レポート</h2>

<div id=message>
{if $error_message != '' }
<div id="error_message">
	<h3>ERROR:{$error_message}</h3>
</div>
{/if}
{if $info_message != '' }
<div id="info_message">
	<h3>INFO:{$info_message}</h3>
</div>
{/if}
</div><!-- message -->

<form method="POST" action="{$smarty.server.PHP_SELF}">
<table cellpadding="0" cellspacing="0">
	<tr>
		<td>
			<a href="./report.php">時間別</a>
			広告別
			<a href="./report_media.php">メディア別</a>
		</td>
	</tr>
	<tr>
		<td>
			<input type="radio" name="date_type" value="1"{if $date_type == 1} checked="checked"{/if} /><label>月別</label>
			<input type="radio" name="date_type" value="2"{if $date_type == 2} checked="checked"{/if} /><label>日別</label>
		</td>
	</tr>
	<tr>
		<td>
			<input type="radio" name="carrier" value="1"{if $carrier == 1} checked="checked"{/if} /><label>キャリア合計</label>
			<input type="radio" name="carrier" value="2"{if $carrier == 2} checked="checked"{/if} /><label>キャリア別</label>
		</td>
	</tr>
	<tr>
		<td>
			<select name="s_year">
{section name=cnt start=2009 loop=2021}
				<option value="{$smarty.section.cnt.index}"{if $set_s_year == $smarty.section.cnt.index} selected="selected"{/if}>{$smarty.section.cnt.index}年</option>
{/section}
			</select>
			<select name="s_month">
{section name=cnt start=1 loop=13}
				<option value="{$smarty.section.cnt.index}"{if $set_s_month == $smarty.section.cnt.index} selected="selected"{/if}>{$smarty.section.cnt.index}月</option>
{/section}
			</select>
			<select name="s_day">
{section name=cnt start=1 loop=32}
				<option value="{$smarty.section.cnt.index}"{if $set_s_day == $smarty.section.cnt.index} selected="selected"{/if}>{$smarty.section.cnt.index}日</option>
{/section}
			</select>
			<input type="submit" value="表示" />
			<input type="hidden" name="mode" value="search" />
		</td>
	</tr>
</table>
</form>

{if $carrier == 1}
<table cellpadding="0" cellspacing="0">
	<tr>
		<th rowspan="2">広告名</th>
		<th colspan="2">アフィリエイト報酬</th>
		<th colspan="2">クリック報酬</th>
		<th rowspan="2">合計</th>
	</tr>
	<tr>
		<th>クリック数</th>
		<th>報酬金額</th>
		<th>アクション数</th>
		<th>報酬金額</th>
	</tr>
{foreach from=$summary key="key" item="data" name="summary"}
	<tr>
		<td>{$data.advert_name}</td>
		<td>{$data.action_count|number_format}</td>
		<td>&yen;{$data.action_price|number_format}</td>
		<td>{$data.click_count|number_format}</td>
		<td>&yen;{$data.click_price|number_format}</td>
		<td>&yen;{$data.total_price|number_format}</td>
	</tr>
{/foreach}
	<tr>
		<td>合計</td>
		<td>{$all.action_count|number_format}</td>
		<td>&yen;{$all.action_price|number_format}</td>
		<td>{$all.click_count|number_format}</td>
		<td>&yen;{$all.click_price|number_format}</td>
		<td>&yen;{$all.total_price|number_format}</td>
	</tr>
</table>
{elseif $carrier == 2}
<table cellpadding="0" cellspacing="0">
	<tr>
		<th colspan="2"></th>
		<th colspan="2">アフィリエイト報酬</th>
		<th colspan="2">クリック報酬</th>
		<th rowspan="2">合計</th>
	</tr>
	<tr>
		<th>年月</th>
		<th>キャリア</th>
		<th>クリック数</th>
		<th>報酬金額</th>
		<th>アクション数</th>
		<th>報酬金額</th>
	</tr>
{foreach from=$summary key="key" item="data" name="summary"}
	<tr>
		<td rowspan="4">{$data.advert_name}</td>
		<td>docomo</td>
		<td>{$data.docomo.action_count|number_format}</td>
		<td>&yen;{$data.docomo.action_price|number_format}</td>
		<td>{$data.docomo.click_count|number_format}</td>
		<td>&yen;{$data.docomo.click_price|number_format}</td>
		<td>&yen;{$data.docomo.total_price|number_format}</td>
	</tr>
	<tr>
		<td>softbank</td>
		<td>{$data.softbank.action_count|number_format}</td>
		<td>&yen;{$data.softbank.action_price|number_format}</td>
		<td>{$data.softbank.click_count|number_format}</td>
		<td>&yen;{$data.softbank.click_price|number_format}</td>
		<td>&yen;{$data.softbank.total_price|number_format}</td>
	</tr>
	<tr>
		<td>au</td>
		<td>{$data.au.action_count|number_format}</td>
		<td>&yen;{$data.au.action_price|number_format}</td>
		<td>{$data.au.click_count|number_format}</td>
		<td>&yen;{$data.au.click_price|number_format}</td>
		<td>&yen;{$data.au.total_price|number_format}</td>
	</tr>
	<tr>
		<td>pc</td>
		<td>{$data.pc.action_count|number_format}</td>
		<td>&yen;{$data.pc.action_price|number_format}</td>
		<td>{$data.pc.click_count|number_format}</td>
		<td>&yen;{$data.pc.click_price|number_format}</td>
		<td>&yen;{$data.pc.total_price|number_format}</td>
	</tr>
{/foreach}
	<tr>
		<td colspan="2">合計</td>
		<td>{$all.action_count|number_format}</td>
		<td>&yen;{$all.action_price|number_format}</td>
		<td>{$all.click_count|number_format}</td>
		<td>&yen;{$all.click_price|number_format}</td>
		<td>&yen;{$all.total_price|number_format}</td>
	</tr>
</table>
{/if}
</div><!-- contents -->

<!-- フッター -->
{include file='./hooter.tpl'}