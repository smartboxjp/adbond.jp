{include file='./header_index.tpl' page_title='TOP'}

<div id=message>
{if $error_message != ''}
<div id="error_message">
<h3>ERROR:{$error_message}</h3>
</div>
{/if}
{if $info_message != ''}
<div id="info_message">
<h3>INFO:{$info_message}</h3>
</div>
{/if}
</div><!-- message -->

	<div id="login">
		<div id="media_login">
			<form action="{$smarty.server.PHP_SELF}" method="POST">
				<input type="hidden" name="media_logon" value="yy" />
				<table cellpadding="0" cellspacing="0">
					<tr>
						<td colspan="2">メディア様ログイン</td>
					</tr>
					<tr>
						<th>login:</th>
						<td><input type="text" name="login_id" value="" /></td>
					</tr>
					<tr>
						<th>password:</th>
						<td><input type="password" name="login_pass" value="" /></td>
					</tr>
					<tr>
						<td colspan="2" id="login-btn">
							<input type="submit" value="login" />
							<input type="reset" />
						</td>
					</tr>
				</table>
			</form>
		</div>
		<div id="advert_login">
			<form action="{$smarty.server.PHP_SELF}" method="POST">
				<input type="hidden" name="advert_logon" value="yy" />
				<table cellpadding="0" cellspacing="0">
					<tr>
						<td colspan="2">クライアント様ログイン</td>
					</tr>
					<tr>
						<th>login:</th>
						<td><input type="text" name="login_id" value="" /></td>
					</tr>
					<tr>
						<th>password:</th>
						<td><input type="password" name="login_pass" value="" /></td>
					</tr>
					<tr>
						<td colspan="2" id="login-btn">
							<input type="submit" value="login" />
							<input type="reset" />
						</td>
					</tr>
				</table>
			</form>
		</div>
	</div>

{include file='./hooter.tpl'}