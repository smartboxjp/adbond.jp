<?php
class Advert {

	// プロパティ
	private $id = "";
	private $advert_client_id = "";
	private $advert_category_id = "";
	private $advert_name = "";
	private $content_type = "";
	private $support_docomo = "";
	private $support_softbank = "";
	private $support_au = "";
	private $support_pc = "";
	private $site_url_docomo = "";
	private $site_url_softbank = "";
	private $site_url_au = "";
	private $site_url_pc = "";
	private $site_outline = "";
	private $click_price_client = "";
	private $click_price_media = "";
	private $action_price_client_docomo_1 = "";
	private $action_price_client_softbank_1 = "";
	private $action_price_client_au_1 = "";
	private $action_price_client_pc_1 = "";
	private $action_price_client_docomo_2 = "";
	private $action_price_client_softbank_2 = "";
	private $action_price_client_au_2 = "";
	private $action_price_client_pc_2 = "";
	private $action_price_client_docomo_3 = "";
	private $action_price_client_softbank_3 = "";
	private $action_price_client_au_3 = "";
	private $action_price_client_pc_3 = "";
	private $action_price_client_docomo_4 = "";
	private $action_price_client_softbank_4 = "";
	private $action_price_client_au_4 = "";
	private $action_price_client_pc_4 = "";
	private $action_price_client_docomo_5 = "";
	private $action_price_client_softbank_5 = "";
	private $action_price_client_au_5 = "";
	private $action_price_client_pc_5 = "";
	private $action_price_media_docomo_1 = "";
	private $action_price_media_softbank_1 = "";
	private $action_price_media_au_1 = "";
	private $action_price_media_pc_1 = "";
	private $action_price_media_docomo_2 = "";
	private $action_price_media_softbank_2 = "";
	private $action_price_media_au_2 = "";
	private $action_price_media_pc_2 = "";
	private $action_price_media_docomo_3 = "";
	private $action_price_media_softbank_3 = "";
	private $action_price_media_au_3 = "";
	private $action_price_media_pc_3 = "";
	private $action_price_media_docomo_4 = "";
	private $action_price_media_softbank_4 = "";
	private $action_price_media_au_4 = "";
	private $action_price_media_pc_4 = "";
	private $action_price_media_docomo_5 = "";
	private $action_price_media_softbank_5 = "";
	private $action_price_media_au_5 = "";
	private $action_price_media_pc_5 = "";
	private $ms_text_1 = "";
	private $ms_email_1 = "";
	private $ms_image_type_1 = "";
	private $ms_image_url_1 = "";
	private $ms_text_2 = "";
	private $ms_email_2 = "";
	private $ms_image_type_2 = "";
	private $ms_image_url_2 = "";
	private $ms_text_3 = "";
	private $ms_email_3 = "";
	private $ms_image_type_3 = "";
	private $ms_image_url_3 = "";
	private $ms_text_4 = "";
	private $ms_email_4 = "";
	private $ms_image_type_4 = "";
	private $ms_image_url_4 = "";
	private $ms_text_5 = "";
	private $ms_email_5 = "";
	private $ms_image_type_5 = "";
	private $ms_image_url_5 = "";
	private $unique_click_type = "";
	private $point_back_flag = "";
	private $adult_flag = "";
	private $dating_flag = "";
	private $advert_start_date = "";
	private $advert_end_date = "";
	private $unrestraint_flag = "";
	private $test_flag = "";
	private $status = "";
	private $created_at = "";
	private $updated_at = "";
	private $deleted_at = "";

	// _toString()
	public function _toString(){
		return (string)($this->id . ","
						. $this->advert_client_id . ","
						. $this->advert_category_id . ","
						. $this->advert_name . ","
						. $this->content_type . ","
						. $this->support_docomo . ","
						. $this->support_softbank . ","
						. $this->support_au . ","
						. $this->support_pc . ","
						. $this->site_url_docomo . ","
						. $this->site_url_softbank . ","
						. $this->site_url_au . ","
						. $this->site_url_pc . ","
						. $this->site_outline . ","
						. $this->click_price_client . ","
						. $this->click_price_media . ","
						. $this->action_price_client_docomo_1 . ","
						. $this->action_price_client_softbank_1 . ","
						. $this->action_price_client_au_1 . ","
						. $this->action_price_client_pc_1 . ","
						. $this->action_price_client_docomo_2 . ","
						. $this->action_price_client_softbank_2 . ","
						. $this->action_price_client_au_2 . ","
						. $this->action_price_client_pc_2 . ","
						. $this->action_price_client_docomo_3 . ","
						. $this->action_price_client_softbank_3 . ","
						. $this->action_price_client_au_3 . ","
						. $this->action_price_client_pc_3 . ","
						. $this->action_price_client_docomo_4 . ","
						. $this->action_price_client_softbank_4 . ","
						. $this->action_price_client_au_4 . ","
						. $this->action_price_client_pc_4 . ","
						. $this->action_price_client_docomo_5 . ","
						. $this->action_price_client_softbank_5 . ","
						. $this->action_price_client_au_5 . ","
						. $this->action_price_client_pc_5 . ","
						. $this->action_price_media_docomo_1 . ","
						. $this->action_price_media_softbank_1 . ","
						. $this->action_price_media_au_1 . ","
						. $this->action_price_media_pc_1 . ","
						. $this->action_price_media_docomo_2 . ","
						. $this->action_price_media_softbank_2 . ","
						. $this->action_price_media_au_2 . ","
						. $this->action_price_media_pc_2 . ","
						. $this->action_price_media_docomo_3 . ","
						. $this->action_price_media_softbank_3 . ","
						. $this->action_price_media_au_3 . ","
						. $this->action_price_media_pc_3 . ","
						. $this->action_price_media_docomo_4 . ","
						. $this->action_price_media_softbank_4 . ","
						. $this->action_price_media_au_4 . ","
						. $this->action_price_media_pc_4 . ","
						. $this->action_price_media_docomo_5 . ","
						. $this->action_price_media_softbank_5 . ","
						. $this->action_price_media_au_5 . ","
						. $this->action_price_media_pc_5 . ","
						. $this->ms_text_1 . ","
						. $this->ms_email_1 . ","
						. $this->ms_image_type_1 . ","
						. $this->ms_image_url_1 . ","
						. $this->ms_text_2 . ","
						. $this->ms_email_2 . ","
						. $this->ms_image_type_2 . ","
						. $this->ms_image_url_2 . ","
						. $this->ms_text_3 . ","
						. $this->ms_email_3 . ","
						. $this->ms_image_type_3 . ","
						. $this->ms_image_url_3 . ","
						. $this->ms_text_4 . ","
						. $this->ms_email_4 . ","
						. $this->ms_image_type_4 . ","
						. $this->ms_image_url_4 . ","
						. $this->ms_text_5 . ","
						. $this->ms_email_5 . ","
						. $this->ms_image_type_5 . ","
						. $this->ms_image_url_5 . ","
						. $this->unique_click_type . ","
						. $this->point_back_flag . ","
						. $this->adult_flag . ","
						. $this->dating_flag . ","
						. $this->advert_start_date . ","
						. $this->advert_end_date . ","
						. $this->unrestraint_flag . ","
						. $this->test_flag . ","
						. $this->status . ","
						. $this->created_at . ","
						. $this->updated_at . ","
						. $this->deleted_at);
	}

	// id
	public function getId(){
		return $this->id;
	}

	public function setId($val){
		$this->id = $val;
	}

	// advert_client_id
	public function getAdvertClientId(){
		return $this->advert_client_id;
	}

	public function setAdvertClientId($val){
		$this->advert_client_id = $val;
	}

	// advert_category_id
	public function getAdvertCategoryId(){
		return $this->advert_category_id;
	}

	public function setAdvertCategoryId($val){
		$this->advert_category_id = $val;
	}

	// advert_name
	public function getAdvertName(){
		return $this->advert_name;
	}

	public function setAdvertName($val){
		$this->advert_name = $val;
	}

	// content_type
	public function getContentType(){
		return $this->content_type;
	}

	public function setContentType($val){
		$this->content_type = $val;
	}

	// support_docomo
	public function getSupportDocomo(){
		return $this->support_docomo;
	}

	public function setSupportDocomo($val){
		$this->support_docomo = $val;
	}

	// support_softbank
	public function getSupportSoftbank(){
		return $this->support_softbank;
	}

	public function setSupportSoftbank($val){
		$this->support_softbank = $val;
	}

	// support_au
	public function getSupportAu(){
		return $this->support_au;
	}

	public function setSupportAu($val){
		$this->support_au = $val;
	}

	// support_pc
	public function getSupportPc(){
		return $this->support_pc;
	}

	public function setSupportPc($val){
		$this->support_pc = $val;
	}

	// site_url_docomo
	public function getSiteUrlDocomo(){
		return $this->site_url_docomo;
	}

	public function setSiteUrlDocomo($val){
		$this->site_url_docomo = $val;
	}

	// site_url_softbank
	public function getSiteUrlSoftbank(){
		return $this->site_url_softbank;
	}

	public function setSiteUrlSoftbank($val){
		$this->site_url_softbank = $val;
	}

	// site_url_au
	public function getSiteUrlAu(){
		return $this->site_url_au;
	}

	public function setSiteUrlAu($val){
		$this->site_url_au = $val;
	}

	// site_url_pc
	public function getSiteUrlPc(){
		return $this->site_url_pc;
	}

	public function setSiteUrlPc($val){
		$this->site_url_pc = $val;
	}

	// site_outline
	public function getSiteOutline(){
		return $this->site_outline;
	}

	public function setSiteOutline($val){
		$this->site_outline = $val;
	}

	// click_price_client
	public function getClickPriceClient(){
		return $this->click_price_client;
	}

	public function setClickPriceClient($val){
		$this->click_price_client = $val;
	}

	// click_price_media
	public function getClickPriceMedia(){
		return $this->click_price_media;
	}

	public function setClickPriceMedia($val){
		$this->click_price_media = $val;
	}

	// action_price_client_docomo_1
	public function getActionPriceClientDocomo1(){
		return $this->action_price_client_docomo_1;
	}

	public function setActionPriceClientDocomo1($val){
		$this->action_price_client_docomo_1 = $val;
	}

	// action_price_client_softbank_1
	public function getActionPriceClientSoftbank1(){
		return $this->action_price_client_softbank_1;
	}

	public function setActionPriceClientSoftbank1($val){
		$this->action_price_client_softbank_1 = $val;
	}

	// action_price_client_au_1
	public function getActionPriceClientAu1(){
		return $this->action_price_client_au_1;
	}

	public function setActionPriceClientAu1($val){
		$this->action_price_client_au_1 = $val;
	}

	// action_price_client_pc_1
	public function getActionPriceClientPc1(){
		return $this->action_price_client_pc_1;
	}

	public function setActionPriceClientPc1($val){
		$this->action_price_client_pc_1 = $val;
	}

	// action_price_client_docomo_2
	public function getActionPriceClientDocomo2(){
		return $this->action_price_client_docomo_2;
	}

	public function setActionPriceClientDocomo2($val){
		$this->action_price_client_docomo_2 = $val;
	}

	// action_price_client_softbank_2
	public function getActionPriceClientSoftbank2(){
		return $this->action_price_client_softbank_2;
	}

	public function setActionPriceClientSoftbank2($val){
		$this->action_price_client_softbank_2 = $val;
	}

	// action_price_client_au_2
	public function getActionPriceClientAu2(){
		return $this->action_price_client_au_2;
	}

	public function setActionPriceClientAu2($val){
		$this->action_price_client_au_2 = $val;
	}

	// action_price_client_pc_2
	public function getActionPriceClientPc2(){
		return $this->action_price_client_pc_2;
	}

	public function setActionPriceClientPc2($val){
		$this->action_price_client_pc_2 = $val;
	}

	// action_price_client_docomo_3
	public function getActionPriceClientDocomo3(){
		return $this->action_price_client_docomo_3;
	}

	public function setActionPriceClientDocomo3($val){
		$this->action_price_client_docomo_3 = $val;
	}

	// action_price_client_softbank_3
	public function getActionPriceClientSoftbank3(){
		return $this->action_price_client_softbank_3;
	}

	public function setActionPriceClientSoftbank3($val){
		$this->action_price_client_softbank_3 = $val;
	}

	// action_price_client_au_3
	public function getActionPriceClientAu3(){
		return $this->action_price_client_au_3;
	}

	public function setActionPriceClientAu3($val){
		$this->action_price_client_au_3 = $val;
	}

	// action_price_client_pc_3
	public function getActionPriceClientPc3(){
		return $this->action_price_client_pc_3;
	}

	public function setActionPriceClientPc3($val){
		$this->action_price_client_pc_3 = $val;
	}

	// action_price_client_docomo_4
	public function getActionPriceClientDocomo4(){
		return $this->action_price_client_docomo_4;
	}

	public function setActionPriceClientDocomo4($val){
		$this->action_price_client_docomo_4 = $val;
	}

	// action_price_client_softbank_4
	public function getActionPriceClientSoftbank4(){
		return $this->action_price_client_softbank_4;
	}

	public function setActionPriceClientSoftbank4($val){
		$this->action_price_client_softbank_4 = $val;
	}

	// action_price_client_au_4
	public function getActionPriceClientAu4(){
		return $this->action_price_client_au_4;
	}

	public function setActionPriceClientAu4($val){
		$this->action_price_client_au_4 = $val;
	}

	// action_price_client_pc_4
	public function getActionPriceClientPc4(){
		return $this->action_price_client_pc_4;
	}

	public function setActionPriceClientPc4($val){
		$this->action_price_client_pc_4 = $val;
	}

	// action_price_client_docomo_5
	public function getActionPriceClientDocomo5(){
		return $this->action_price_client_docomo_5;
	}

	public function setActionPriceClientDocomo5($val){
		$this->action_price_client_docomo_5 = $val;
	}

	// action_price_client_softbank_5
	public function getActionPriceClientSoftbank5(){
		return $this->action_price_client_softbank_5;
	}

	public function setActionPriceClientSoftbank5($val){
		$this->action_price_client_softbank_5 = $val;
	}

	// action_price_client_au_5
	public function getActionPriceClientAu5(){
		return $this->action_price_client_au_5;
	}

	public function setActionPriceClientAu5($val){
		$this->action_price_client_au_5 = $val;
	}

	// action_price_client_pc_5
	public function getActionPriceClientPc5(){
		return $this->action_price_client_pc_5;
	}

	public function setActionPriceClientPc5($val){
		$this->action_price_client_pc_5 = $val;
	}

	// action_price_media_docomo_1
	public function getActionPriceMediaDocomo1(){
		return $this->action_price_media_docomo_1;
	}

	public function setActionPriceMediaDocomo1($val){
		$this->action_price_media_docomo_1 = $val;
	}

	// action_price_media_softbank_1
	public function getActionPriceMediaSoftbank1(){
		return $this->action_price_media_softbank_1;
	}

	public function setActionPriceMediaSoftbank1($val){
		$this->action_price_media_softbank_1 = $val;
	}

	// action_price_media_au_1
	public function getActionPriceMediaAu1(){
		return $this->action_price_media_au_1;
	}

	public function setActionPriceMediaAu1($val){
		$this->action_price_media_au_1 = $val;
	}

	// action_price_media_pc_1
	public function getActionPriceMediaPc1(){
		return $this->action_price_media_pc_1;
	}

	public function setActionPriceMediaPc1($val){
		$this->action_price_media_pc_1 = $val;
	}

	// action_price_media_docomo_2
	public function getActionPriceMediaDocomo2(){
		return $this->action_price_media_docomo_2;
	}

	public function setActionPriceMediaDocomo2($val){
		$this->action_price_media_docomo_2 = $val;
	}

	// action_price_media_softbank_2
	public function getActionPriceMediaSoftbank2(){
		return $this->action_price_media_softbank_2;
	}

	public function setActionPriceMediaSoftbank2($val){
		$this->action_price_media_softbank_2 = $val;
	}

	// action_price_media_au_2
	public function getActionPriceMediaAu2(){
		return $this->action_price_media_au_2;
	}

	public function setActionPriceMediaAu2($val){
		$this->action_price_media_au_2 = $val;
	}

	// action_price_media_pc_2
	public function getActionPriceMediaPc2(){
		return $this->action_price_media_pc_2;
	}

	public function setActionPriceMediaPc2($val){
		$this->action_price_media_pc_2 = $val;
	}

	// action_price_media_docomo_3
	public function getActionPriceMediaDocomo3(){
		return $this->action_price_media_docomo_3;
	}

	public function setActionPriceMediaDocomo3($val){
		$this->action_price_media_docomo_3 = $val;
	}

	// action_price_media_softbank_3
	public function getActionPriceMediaSoftbank3(){
		return $this->action_price_media_softbank_3;
	}

	public function setActionPriceMediaSoftbank3($val){
		$this->action_price_media_softbank_3 = $val;
	}

	// action_price_media_au_3
	public function getActionPriceMediaAu3(){
		return $this->action_price_media_au_3;
	}

	public function setActionPriceMediaAu3($val){
		$this->action_price_media_au_3 = $val;
	}

	// action_price_media_pc_3
	public function getActionPriceMediaPc3(){
		return $this->action_price_media_pc_3;
	}

	public function setActionPriceMediaPc3($val){
		$this->action_price_media_pc_3 = $val;
	}

	// action_price_media_docomo_4
	public function getActionPriceMediaDocomo4(){
		return $this->action_price_media_docomo_4;
	}

	public function setActionPriceMediaDocomo4($val){
		$this->action_price_media_docomo_4 = $val;
	}

	// action_price_media_softbank_4
	public function getActionPriceMediaSoftbank4(){
		return $this->action_price_media_softbank_4;
	}

	public function setActionPriceMediaSoftbank4($val){
		$this->action_price_media_softbank_4 = $val;
	}

	// action_price_media_au_4
	public function getActionPriceMediaAu4(){
		return $this->action_price_media_au_4;
	}

	public function setActionPriceMediaAu4($val){
		$this->action_price_media_au_4 = $val;
	}

	// action_price_media_pc_4
	public function getActionPriceMediaPc4(){
		return $this->action_price_media_pc_4;
	}

	public function setActionPriceMediaPc4($val){
		$this->action_price_media_pc_4 = $val;
	}

	// action_price_media_docomo_5
	public function getActionPriceMediaDocomo5(){
		return $this->action_price_media_docomo_5;
	}

	public function setActionPriceMediaDocomo5($val){
		$this->action_price_media_docomo_5 = $val;
	}

	// action_price_media_softbank_5
	public function getActionPriceMediaSoftbank5(){
		return $this->action_price_media_softbank_5;
	}

	public function setActionPriceMediaSoftbank5($val){
		$this->action_price_media_softbank_5 = $val;
	}

	// action_price_media_au_5
	public function getActionPriceMediaAu5(){
		return $this->action_price_media_au_5;
	}

	public function setActionPriceMediaAu5($val){
		$this->action_price_media_au_5 = $val;
	}

	// action_price_media_pc_5
	public function getActionPriceMediaPc5(){
		return $this->action_price_media_pc_5;
	}

	public function setActionPriceMediaPc5($val){
		$this->action_price_media_pc_5 = $val;
	}

	// ms_text_1
	public function getMsText1(){
		return $this->ms_text_1;
	}

	public function setMsText1($val){
		$this->ms_text_1 = $val;
	}

	// ms_email_1
	public function getMsEmail1(){
		return $this->ms_email_1;
	}

	public function setMsEmail1($val){
		$this->ms_email_1 = $val;
	}

	// ms_image_type_1
	public function getMsImageType1(){
		return $this->ms_image_type_1;
	}

	public function setMsImageType1($val){
		$this->ms_image_type_1 = $val;
	}

	// ms_image_url_1
	public function getMsImageUrl1(){
		return $this->ms_image_url_1;
	}

	public function setMsImageUrl1($val){
		$this->ms_image_url_1 = $val;
	}

	// ms_text_2
	public function getMsText2(){
		return $this->ms_text_2;
	}

	public function setMsText2($val){
		$this->ms_text_2 = $val;
	}

	// ms_email_2
	public function getMsEmail2(){
		return $this->ms_email_2;
	}

	public function setMsEmail2($val){
		$this->ms_email_2 = $val;
	}

	// ms_image_type_2
	public function getMsImageType2(){
		return $this->ms_image_type_2;
	}

	public function setMsImageType2($val){
		$this->ms_image_type_2 = $val;
	}

	// ms_image_url_2
	public function getMsImageUrl2(){
		return $this->ms_image_url_2;
	}

	public function setMsImageUrl2($val){
		$this->ms_image_url_2 = $val;
	}

	// ms_text_3
	public function getMsText3(){
		return $this->ms_text_3;
	}

	public function setMsText3($val){
		$this->ms_text_3 = $val;
	}

	// ms_email_3
	public function getMsEmail3(){
		return $this->ms_email_3;
	}

	public function setMsEmail3($val){
		$this->ms_email_3 = $val;
	}

	// ms_image_type_3
	public function getMsImageType3(){
		return $this->ms_image_type_3;
	}

	public function setMsImageType3($val){
		$this->ms_image_type_3 = $val;
	}

	// ms_image_url_3
	public function getMsImageUrl3(){
		return $this->ms_image_url_3;
	}

	public function setMsImageUrl3($val){
		$this->ms_image_url_3 = $val;
	}

	// ms_text_4
	public function getMsText4(){
		return $this->ms_text_4;
	}

	public function setMsText4($val){
		$this->ms_text_4 = $val;
	}

	// ms_email_4
	public function getMsEmail4(){
		return $this->ms_email_4;
	}

	public function setMsEmail4($val){
		$this->ms_email_4 = $val;
	}

	// ms_image_type_4
	public function getMsImageType4(){
		return $this->ms_image_type_4;
	}

	public function setMsImageType4($val){
		$this->ms_image_type_4 = $val;
	}

	// ms_image_url_4
	public function getMsImageUrl4(){
		return $this->ms_image_url_4;
	}

	public function setMsImageUrl4($val){
		$this->ms_image_url_4 = $val;
	}

	// ms_text_5
	public function getMsText5(){
		return $this->ms_text_5;
	}

	public function setMsText5($val){
		$this->ms_text_5 = $val;
	}

	// ms_email_5
	public function getMsEmail5(){
		return $this->ms_email_5;
	}

	public function setMsEmail5($val){
		$this->ms_email_5 = $val;
	}

	// ms_image_type_5
	public function getMsImageType5(){
		return $this->ms_image_type_5;
	}

	public function setMsImageType5($val){
		$this->ms_image_type_5 = $val;
	}

	// ms_image_url_5
	public function getMsImageUrl5(){
		return $this->ms_image_url_5;
	}

	public function setMsImageUrl5($val){
		$this->ms_image_url_5 = $val;
	}

	// unique_click_type
	public function getUniqueClickType(){
		return $this->unique_click_type;
	}

	public function setUniqueClickType($val){
		$this->unique_click_type = $val;
	}

	// point_back_flag
	public function getPointBackFlag(){
		return $this->point_back_flag;
	}

	public function setPointBackFlag($val){
		$this->point_back_flag = $val;
	}

	// adult_flag
	public function getAdultFlag(){
		return $this->adult_flag;
	}

	public function setAdultFlag($val){
		$this->adult_flag = $val;
	}

	// dating_flag
	public function getDatingFlag(){
		return $this->dating_flag;
	}

	public function setDatingFlag($val){
		$this->dating_flag = $val;
	}

	// advert_start_date
	public function getAdvertStartDate(){
		return $this->advert_start_date;
	}

	public function setAdvertStartDate($val){
		$this->advert_start_date = $val;
	}

	// advert_end_date
	public function getAdvertEndDate(){
		return $this->advert_end_date;
	}

	public function setAdvertEndDate($val){
		$this->advert_end_date = $val;
	}

	// unrestraint_flag
	public function getUnrestraintFlag(){
		return $this->unrestraint_flag;
	}

	public function setUnrestraintFlag($val){
		$this->unrestraint_flag = $val;
	}

	// test_flag
	public function getTestFlag(){
		return $this->test_flag;
	}

	public function setTestFlag($val){
		$this->test_flag = $val;
	}

	// status
	public function getStatus(){
		return $this->status;
	}

	public function setStatus($val){
		$this->status = $val;
	}

	// created_at
	public function getCreatedAt(){
		return $this->created_at;
	}

	public function setCreatedAt($val){
		$this->created_at = $val;
	}

	// updated_at
	public function getUpdatedAt(){
		return $this->updated_at;
	}

	public function setUpdatedAt($val){
		$this->updated_at = $val;
	}

	// deleted_at
	public function getDeletedAt(){
		return $this->deleted_at;
	}

	public function setDeletedAt($val){
		$this->deleted_at = $val;
	}

}