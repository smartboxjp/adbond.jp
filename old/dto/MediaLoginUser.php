<?php
class MediaLoginUser {

	// プロパティ
	private $id = "";
	private $user_name = "";
	private $login_id = "";
	private $login_pass = "";
	private $last_login_at = "";
	private $created_at = "";
	private $updated_at = "";
	private $deleted_at = "";

	// _toString()
	public function _toString(){
		return (string)($this->id . ","
						. $this->user_name . ","
						. $this->login_id . ","
						. $this->login_pass . ","
						. $this->last_login_at . ","
						. $this->created_at . ","
						. $this->updated_at . ","
						. $this->deleted_at);
	}

	// id
	public function getId(){
		return $this->id;
	}

	public function setId($val){
		$this->id = $val;
	}

	// user_name
	public function getUserName(){
		return $this->user_name;
	}

	public function setUserName($val){
		$this->user_name = $val;
	}

	// login_id
	public function getLoginId(){
		return $this->login_id;
	}

	public function setLoginId($val){
		$this->login_id = $val;
	}

	// login_pass
	public function getLoginPass(){
		return $this->login_pass;
	}

	public function setLoginPass($val){
		$this->login_pass = $val;
	}

	// last_login_at
	public function getLastLoginAt(){
		return $this->last_login_at;
	}

	public function setLastLoginAt($val){
		$this->last_login_at = $val;
	}

	// created_at
	public function getCreatedAt(){
		return $this->created_at;
	}

	public function setCreatedAt($val){
		$this->created_at = $val;
	}

	// updated_at
	public function getUpdatedAt(){
		return $this->updated_at;
	}

	public function setUpdatedAt($val){
		$this->updated_at = $val;
	}

	// deleted_at
	public function getDeletedAt(){
		return $this->deleted_at;
	}

	public function setDeletedAt($val){
		$this->deleted_at = $val;
	}

}