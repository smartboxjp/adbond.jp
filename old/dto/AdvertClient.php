<?php
class AdvertClient {

	// プロパティ
	private $id = "";
	private $login_user_id = "";
	private $advert_group_id = "";
	private $client_name = "";
	private $contact_person = "";
	private $tel = "";
	private $fax = "";
	private $email = "";
	private $zipcode1 = "";
	private $zipcode2 = "";
	private $pref = "";
	private $address1 = "";
	private $address2 = "";
	private $status = "";
	private $created_at = "";
	private $updated_at = "";
	private $deleted_at = "";

	// _toString()
	public function _toString(){
		return (string)($this->id . ","
						 . $this->login_user_id . ","
						 . $this->advert_group_id . ","
						 . $this->client_name . ","
						 . $this->contact_person . ","
						 . $this->tel . ","
						 . $this->fax . ","
						 . $this->email . ","
						 . $this->zipcode1 . ","
						 . $this->zipcode2 . ","
						 . $this->pref . ","
						 . $this->address1 . ","
						 . $this->address2 . ","
						 . $this->status . ","
						 . $this->created_at . ","
						 . $this->updated_at . ","
						 . $this->deleted_at);
	}

	// id
	public function getId(){
		return $this->id;
	}

	public function setId($val){
		$this->id = $val;
	}

	// login_user_id
	public function getLoginUserId(){
		return $this->login_user_id;
	}

	public function setLoginUserId($val){
		$this->login_user_id = $val;
	}

	// advert_group_id
	public function getAdvertGroupId(){
		return $this->advert_group_id;
	}

	public function setAdvertGroupId($val){
		$this->advert_group_id = $val;
	}

	// client_name
	public function getClientName(){
		return $this->client_name;
	}

	public function setClientName($val){
		$this->client_name = $val;
	}

	// contact_person
	public function getContactPerson(){
		return $this->contact_person;
	}

	public function setContactPerson($val){
		$this->contact_person = $val;
	}

	// tel
	public function getTel(){
		return $this->tel;
	}

	public function setTel($val){
		$this->tel = $val;
	}

	// fax
	public function getFax(){
		return $this->fax;
	}

	public function setFax($val){
		$this->fax = $val;
	}

	// email
	public function getEmail(){
		return $this->email;
	}

	public function setEmail($val){
		$this->email = $val;
	}

	// zipcode1
	public function getZipcode1(){
		return $this->zipcode1;
	}

	public function setZipcode1($val){
		$this->zipcode1 = $val;
	}

	// zipcode2
	public function getZipcode2(){
		return $this->zipcode2;
	}

	public function setZipcode2($val){
		$this->zipcode2 = $val;
	}

	// pref
	public function getPref(){
		return $this->pref;
	}

	public function setPref($val){
		$this->pref = $val;
	}

	// address1
	public function getAddress1(){
		return $this->address1;
	}

	public function setAddress1($val){
		$this->address1 = $val;
	}

	// address2
	public function getAddress2(){
		return $this->address2;
	}

	public function setAddress2($val){
		$this->address2 = $val;
	}

	// status
	public function getStatus(){
		return $this->status;
	}

	public function setStatus($val){
		$this->status = $val;
	}

	// created_at
	public function getCreatedAt(){
		return $this->created_at;
	}

	public function setCreatedAt($val){
		$this->created_at = $val;
	}

	// updated_at
	public function getUpdatedAt(){
		return $this->updated_at;
	}

	public function setUpdatedAt($val){
		$this->updated_at = $val;
	}

	// deleted_at
	public function getDeletedAt(){
		return $this->deleted_at;
	}

	public function setDeletedAt($val){
		$this->deleted_at = $val;
	}

}