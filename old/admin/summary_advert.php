<?php
// 共通設定
require_once( '../common/CommonAdminBase.php' );
require_once( '../common/CommonDao.php' );
require_once( '../dto/LoginUser.php' );

session_start();

if(isset($_SESSION['logon_token']) && $_SESSION['logon_token'] != ''){
	$login_user = new LoginUser();
	$login_user = $_SESSION['login_user'];

	// Smartyオブジェクト取得
	$smarty =& getSmartyObj();

	$smarty->assign("title", "Test Top Page");
	$smarty->assign("login_user", $login_user );

	$common_dao = new CommonDao();

	$list_sql = " SELECT al.*, a.advert_name, ac.client_name "
				. " FROM action_logs as al "
				. " LEFT JOIN advert as a on al.advert_id = a.id "
				. " LEFT JOIN advert_clients as ac on al.advert_client_id = ac.id "
				. " WHERE al.deleted_at is NULL "
				. " AND (al.status = 1 OR al.status = 2) "
				. " ORDER BY al.advert_client_id ASC, al.advert_id ASC ";

	$list_count = 0;

	$db_result = $common_dao->db_query($list_sql);
	if($db_result){

		foreach($db_result as $row) {
			$a_id = $row['advert_id'];

			$summary[$a_id]['advert_id'] = $row['advert_id'];
			$summary[$a_id]['advert_name'] = $row['advert_name'];
			$summary[$a_id]['advert_client_id'] = $row['advert_client_id'];
			$summary[$a_id]['client_name'] = $row['client_name'];

			$summary[$a_id]['click_count'] += 1;
			$summary[$a_id]['total_price'] += $row['click_price_client'];

			if($row['status'] == 2) {
				$summary[$a_id]['action_count'] += 1;
				$summary[$a_id]['total_price'] += $row['action_price_client'];
			}
		}

		$smarty->assign("list", $summary);
		$list_count = count($summary);
	}else{
		$error_message .= "ＤＢからのデータの取得に失敗しました。(su0000)";
	}
	$smarty->assign("list_count", $list_count);
	$smarty->assign("error_message", $error_message);

	// ページを表示
	$smarty->display("./summary_advert.tpl");
	exit();
}else{
	header('Location: ./login.php?error=1');
	exit();
}
?>