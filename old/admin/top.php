<?php
// 共通設定
require_once( '../common/CommonAdminBase.php' );
require_once( '../dto/LoginUser.php' );

session_start();

if(isset($_SESSION['logon_token']) && $_SESSION['logon_token'] != ''){
	$login_user = new LoginUser();
	$login_user = $_SESSION['login_user'];

	// Smartyオブジェクト取得
	$smarty =& getSmartyObj();

	$smarty->assign("title", "Test Top Page");
	$smarty->assign("login_user", $login_user );

	// ページを表示
	$smarty->display("./top.tpl");
}else{
	header('Location: ./login.php?error=1');
	exit();
}
?>