<?php
// 共通設定
require_once( '../common/CommonAdminBase.php' );
require_once( '../common/CommonDao.php' );
require_once( '../dto/LoginUser.php' );

session_start();

$common_dao = new CommonDao();

if(isset($_GET['date']) && $_GET['date'] != '') {
	if(isset($_GET['type']) && $_GET['type'] != '') {
		$download_date = $_GET['date'];
		$log_type = $_GET['type'];

		if($log_type == 1) {

			$download_sql = " SELECT * "
							. " FROM action_logs "
							. " WHERE deleted_at is NULL ";

			if(isset($_GET['monthly']) && $_GET['monthly'] == 1) {
				$download_sql .= " AND DATE_FORMAT(created_at,'%Y%m') = '$download_date' ";
			} else {
				$download_sql .= " AND DATE_FORMAT(created_at,'%Y%m%d') = '$download_date' ";
			}

			$download_sql .= " AND (status = 1 OR status = 2) "
							. " ORDER BY created_at ASC ";

		} elseif($log_type == 2) {

			$download_sql = " SELECT * "
							. " FROM action_logs "
							. " WHERE deleted_at is NULL ";

			if(isset($_GET['monthly']) && $_GET['monthly'] == 1) {
				$download_sql .= " AND DATE_FORMAT(action_complete_date,'%Y%m') = '$download_date' ";
			} else {
				$download_sql .= " AND DATE_FORMAT(action_complete_date,'%Y%m%d') = '$download_date' ";
			}

			$download_sql .= " AND (status = 1 OR status = 2) "
							. " ORDER BY action_complete_date ASC ";

		} elseif($log_type == 3) {

			$download_sql = " SELECT * "
							. " FROM point_back_logs "
							. " WHERE deleted_at is NULL ";

			if(isset($_GET['monthly']) && $_GET['monthly'] == 1) {
				$download_sql .= " AND DATE_FORMAT(created_at,'%Y%m') = '$download_date' ";
			} else {
				$download_sql .= " AND DATE_FORMAT(created_at,'%Y%m%d') = '$download_date' ";
			}

			$download_sql .= " ORDER BY created_at ASC ";

		}

		$db_result2 = $common_dao->db_query($download_sql);
		if($db_result2){
			$outputFile = "../logs/output.cgi";
			touch($outputFile);

			$fp = fopen($outputFile, "w");

			mb_internal_encoding("UTF-8");
			mb_detect_order("ASCII, JIS, UTF-8, eucjp-win, sjis-win, EUC-JP, SJIS");

			if($log_type == 1) {

				$file_name = "click_$download_date.csv";

				$csv_array[] = array('クリック日時', '媒体ID', '広告ID', 'キャリア', 'ユーザーエージェント', '個体識別番号', 'IPアドレス', 'ホスト名', 'リンク先URL', 'ステータス');

				foreach($db_result2 as $row) {
					$data1 = $row['created_at'];
					$data2 = $row['media_id'];
					$data3 = $row['advert_id'];
					$data4 = $row['carrier_id'];
					$data5 = $row['user_agent'];
					$data6 = $row['uid'];
					$data7 = $row['ip_address'];
					$data8 = $row['host_name'];
					$data9 = $row['link_url'];
					$data10 = $row['status'];

					$csv_array[] = array($data1, $data2, $data3, $data4, $data5, $data6, $data7, $data8, $data9, $data10);
				}

			} elseif($log_type == 2) {

				$file_name = "action_$download_date.csv";

				$csv_array[] = array('アクション完了日時', '媒体ID', '広告ID', 'キャリア', 'ユーザーエージェント', '個体識別番号', 'IPアドレス', 'ホスト名', 'ステータス');

				foreach($db_result2 as $row) {
					$data1 = $row['action_complete_date'];
					$data2 = $row['media_id'];
					$data3 = $row['advert_id'];
					$data4 = $row['carrier_id'];
					$data5 = $row['user_agent'];
					$data6 = $row['uid'];
					$data7 = $row['ip_address'];
					$data8 = $row['host_name'];
					$data9 = $row['status'];

					$csv_array[] = array($data1, $data2, $data3, $data4, $data5, $data6, $data7, $data8, $data9);
				}

			} elseif($log_type == 3) {

				$file_name = "point_back_$download_date.csv";

				$csv_array[] = array('ポイントバック通知日時', '媒体ID', '広告ID', 'ユーザ識別ID', 'ポイントバック通知URL', 'ステータス');

				foreach($db_result2 as $row) {
					$data1 = $row['created_at'];
					$data2 = $row['media_id'];
					$data3 = $row['advert_id'];
					$data4 = $row['point_back_parameter'];
					$data5 = $row['point_back_url'];
					$data6 = $row['status'];

					$csv_array[] = array($data1, $data2, $data3, $data4, $data5, $data6);
				}

			}

			mb_convert_variables("SJIS-win", "UTF-8", $csv_array);
			foreach($csv_array as $line) {
				fputcsv($fp, $line);
			}

			fclose($fp);

			header("Content-Type: application/csv");
			header("Content-Disposition: attachment; filename=".$file_name);
			header("Content-Length:".filesize($outputFile));
			readfile($outputFile);
			exit();
		} else {
			exit();
		}
	} else {
		exit();
	}
} else {
	exit();
}

function do_escape_quotes($str){
	//magic_quotesが有効ならクウォート部分を除去
	if(get_magic_quotes_gpc()){
		$str = stripslashes($str);
	}
	return $str;
}
?>