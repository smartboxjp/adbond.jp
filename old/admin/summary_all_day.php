<?php
// 共通設定
require_once( '../common/CommonAdminBase.php' );
require_once( '../common/CommonDao.php' );
require_once( '../dto/LoginUser.php' );

session_start();

if(isset($_SESSION['logon_token']) && $_SESSION['logon_token'] != ''){
	$login_user = new LoginUser();
	$login_user = $_SESSION['login_user'];

	// Smartyオブジェクト取得
	$smarty =& getSmartyObj();

	$smarty->assign("title", "Test Top Page");
	$smarty->assign("login_user", $login_user );

	$common_dao = new CommonDao();

	if(isset($_GET['month'])) {
		$month = $_GET['month'];
	}

	$list_sql = " SELECT al.* "
				. " FROM action_logs as al "
				. " WHERE al.deleted_at is NULL "
				. " AND (al.status = 1 OR al.status = 2) "
				. " AND DATE_FORMAT(created_at,'%Y%m') = '$month' "
				. " ORDER BY created_at DESC ";

	$list_count = 0;

	$db_result = $common_dao->db_query($list_sql);
	if($db_result){

		foreach($db_result as $row) {
			$date = date("Y年m月d日", strtotime($row['created_at']));
			$summary[$date]['summary_date'] = $date;

			$summary[$date]['sales'] += $row['click_price_client'];
			$summary[$date]['sales'] += $row['action_price_client'];

			$summary[$date]['amounts'] += $row['click_price_media'];
			$summary[$date]['amounts'] += $row['action_price_media'];

			$summary[$date]['fees'] += ($row['click_price_client'] - $row['click_price_media']);
			$summary[$date]['fees'] += ($row['action_price_client'] - $row['action_price_media']);

			$summary[$date]['click_count'] += 1;
			if($row['status'] == 2) {
				$summary[$date]['action_count'] += 1;
			}
		}

		$smarty->assign("list", $summary);
		$list_count = count($summary);
	}else{
		$error_message .= "ＤＢからのデータの取得に失敗しました。(su0000)";
	}
	$smarty->assign("list_count", $list_count);
	$smarty->assign("error_message", $error_message);

	// ページを表示
	$smarty->display("./summary_all_day.tpl");
	exit();
}else{
	header('Location: ./login.php?error=1');
	exit();
}
?>