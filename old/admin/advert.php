<?php
// 共通設定
require_once( '../common/CommonAdminBase.php' );
require_once( '../common/CommonDao.php' );
require_once( '../dto/LoginUser.php' );
require_once( '../dao/AdvertDao.php' );
require_once( '../dto/Advert.php' );
require_once( '../dao/AdvertClientDao.php' );
require_once( '../dto/AdvertClient.php' );
require_once( '../dao/AdvertCategoryDao.php' );
require_once( '../dto/AdvertCategory.php' );
require_once( '../dao/ImageFileDao.php' );
require_once( '../dto/ImageFile.php' );

session_start();

if(isset($_SESSION['logon_token']) && $_SESSION['logon_token'] != ''){
	$login_user = new LoginUser();
	$login_user = $_SESSION['login_user'];

	// Smartyオブジェクト取得
	$smarty =& getSmartyObj();

	$smarty->assign("title", "Test Top Page");
	$smarty->assign("login_user", $login_user );

	$common_dao = new CommonDao();
	$advert_dao = new AdvertDao();

	$image_directory = "../advert_image/";

	//広告主
	$advert_client_dao = new AdvertClientDao();
	$advert_client_array = array();
	foreach($advert_client_dao->getAllAdvertClient() as $val){
		$row_array = array('id' => $val->getId(), 'name' => $val->getClientName());
		$advert_client_array[$val->getId()] = $row_array;
	}
	$smarty->assign("advert_client_array", $advert_client_array);

	//広告カテゴリー
	$advert_category_dao = new AdvertCategoryDao();
	$advert_category_array = array();
	foreach($advert_category_dao->getAllAdvertCategory() as $val){
		$row_array = array('id' => $val->getId(), 'name' => $val->getName());
		$advert_category_array[$val->getId()] = $row_array;
	}
	$smarty->assign("advert_category_array", $advert_category_array);

	if(isset($_GET['mode'])) {
		$mode = $_GET['mode'];
	} elseif(isset($_POST['mode'])) {
		$mode = $_POST['mode'];
	}

	if(isset($_GET['id'])) {
		$id = $_GET['id'];
	} else {
		$id = do_escape_quotes($_POST['id']);
	}

	$advert_client_id = do_escape_quotes($_POST['advert_client_id']);
	$advert_category_id = do_escape_quotes($_POST['advert_category_id']);
	$advert_name = do_escape_quotes($_POST['advert_name']);
	$content_type = do_escape_quotes($_POST['content_type']);
	$support_docomo = do_escape_quotes($_POST['support_docomo']);
	$support_softbank = do_escape_quotes($_POST['support_softbank']);
	$support_au = do_escape_quotes($_POST['support_au']);
	$support_pc = do_escape_quotes($_POST['support_pc']);
	$site_url_docomo = do_escape_quotes($_POST['site_url_docomo']);
	$site_url_softbank = do_escape_quotes($_POST['site_url_softbank']);
	$site_url_au = do_escape_quotes($_POST['site_url_au']);
	$site_url_pc = do_escape_quotes($_POST['site_url_pc']);
	$site_outline = do_escape_quotes($_POST['site_outline']);
	$click_price_client = do_escape_quotes($_POST['click_price_client']);
	$click_price_media = do_escape_quotes($_POST['click_price_media']);
	$action_price_client_docomo_1 = do_escape_quotes($_POST['action_price_client_docomo_1']);
	$action_price_client_softbank_1 = do_escape_quotes($_POST['action_price_client_softbank_1']);
	$action_price_client_au_1 = do_escape_quotes($_POST['action_price_client_au_1']);
	$action_price_client_pc_1 = do_escape_quotes($_POST['action_price_client_pc_1']);
	$action_price_client_docomo_2 = do_escape_quotes($_POST['action_price_client_docomo_2']);
	$action_price_client_softbank_2 = do_escape_quotes($_POST['action_price_client_softbank_2']);
	$action_price_client_au_2 = do_escape_quotes($_POST['action_price_client_au_2']);
	$action_price_client_pc_2 = do_escape_quotes($_POST['action_price_client_pc_2']);
	$action_price_client_docomo_3 = do_escape_quotes($_POST['action_price_client_docomo_3']);
	$action_price_client_softbank_3 = do_escape_quotes($_POST['action_price_client_softbank_3']);
	$action_price_client_au_3 = do_escape_quotes($_POST['action_price_client_au_3']);
	$action_price_client_pc_3 = do_escape_quotes($_POST['action_price_client_pc_3']);
	$action_price_client_docomo_4 = do_escape_quotes($_POST['action_price_client_docomo_4']);
	$action_price_client_softbank_4 = do_escape_quotes($_POST['action_price_client_softbank_4']);
	$action_price_client_au_4 = do_escape_quotes($_POST['action_price_client_au_4']);
	$action_price_client_pc_4 = do_escape_quotes($_POST['action_price_client_pc_4']);
	$action_price_client_docomo_5 = do_escape_quotes($_POST['action_price_client_docomo_5']);
	$action_price_client_softbank_5 = do_escape_quotes($_POST['action_price_client_softbank_5']);
	$action_price_client_au_5 = do_escape_quotes($_POST['action_price_client_au_5']);
	$action_price_client_pc_5 = do_escape_quotes($_POST['action_price_client_pc_5']);
	$action_price_media_docomo_1 = do_escape_quotes($_POST['action_price_media_docomo_1']);
	$action_price_media_softbank_1 = do_escape_quotes($_POST['action_price_media_softbank_1']);
	$action_price_media_au_1 = do_escape_quotes($_POST['action_price_media_au_1']);
	$action_price_media_pc_1 = do_escape_quotes($_POST['action_price_media_pc_1']);
	$action_price_media_docomo_2 = do_escape_quotes($_POST['action_price_media_docomo_2']);
	$action_price_media_softbank_2 = do_escape_quotes($_POST['action_price_media_softbank_2']);
	$action_price_media_au_2 = do_escape_quotes($_POST['action_price_media_au_2']);
	$action_price_media_pc_2 = do_escape_quotes($_POST['action_price_media_pc_2']);
	$action_price_media_docomo_3 = do_escape_quotes($_POST['action_price_media_docomo_3']);
	$action_price_media_softbank_3 = do_escape_quotes($_POST['action_price_media_softbank_3']);
	$action_price_media_au_3 = do_escape_quotes($_POST['action_price_media_au_3']);
	$action_price_media_pc_3 = do_escape_quotes($_POST['action_price_media_pc_3']);
	$action_price_media_docomo_4 = do_escape_quotes($_POST['action_price_media_docomo_4']);
	$action_price_media_softbank_4 = do_escape_quotes($_POST['action_price_media_softbank_4']);
	$action_price_media_au_4 = do_escape_quotes($_POST['action_price_media_au_4']);
	$action_price_media_pc_4 = do_escape_quotes($_POST['action_price_media_pc_4']);
	$action_price_media_docomo_5 = do_escape_quotes($_POST['action_price_media_docomo_5']);
	$action_price_media_softbank_5 = do_escape_quotes($_POST['action_price_media_softbank_5']);
	$action_price_media_au_5 = do_escape_quotes($_POST['action_price_media_au_5']);
	$action_price_media_pc_5 = do_escape_quotes($_POST['action_price_media_pc_5']);
	$ms_text_1 = do_escape_quotes($_POST['ms_text_1']);
	$ms_email_1 = do_escape_quotes($_POST['ms_email_1']);
	$ms_image_type_1 = do_escape_quotes($_POST['ms_image_type_1']);
	$ms_image_url_1 = do_escape_quotes($_POST['ms_image_url_1']);
	$ms_text_2 = do_escape_quotes($_POST['ms_text_2']);
	$ms_email_2 = do_escape_quotes($_POST['ms_email_2']);
	$ms_image_type_2 = do_escape_quotes($_POST['ms_image_type_2']);
	$ms_image_url_2 = do_escape_quotes($_POST['ms_image_url_2']);
	$ms_text_3 = do_escape_quotes($_POST['ms_text_3']);
	$ms_email_3 = do_escape_quotes($_POST['ms_email_3']);
	$ms_image_type_3 = do_escape_quotes($_POST['ms_image_type_3']);
	$ms_image_url_3 = do_escape_quotes($_POST['ms_image_url_3']);
	$ms_text_4 = do_escape_quotes($_POST['ms_text_4']);
	$ms_email_4 = do_escape_quotes($_POST['ms_email_4']);
	$ms_image_type_4 = do_escape_quotes($_POST['ms_image_type_4']);
	$ms_image_url_4 = do_escape_quotes($_POST['ms_image_url_4']);
	$ms_text_5 = do_escape_quotes($_POST['ms_text_5']);
	$ms_email_5 = do_escape_quotes($_POST['ms_email_5']);
	$ms_image_type_5 = do_escape_quotes($_POST['ms_image_type_5']);
	$ms_image_url_5 = do_escape_quotes($_POST['ms_image_url_5']);
	$unique_click_type = do_escape_quotes($_POST['unique_click_type']);
	$point_back_flag = do_escape_quotes($_POST['point_back_flag']);
	$adult_flag = do_escape_quotes($_POST['adult_flag']);
	$dating_flag = do_escape_quotes($_POST['dating_flag']);
	$as_year = do_escape_quotes($_POST['as_year']);
	$as_month = do_escape_quotes($_POST['as_month']);
	$as_day = do_escape_quotes($_POST['as_day']);
	$ae_year = do_escape_quotes($_POST['ae_year']);
	$ae_month = do_escape_quotes($_POST['ae_month']);
	$ae_day = do_escape_quotes($_POST['ae_day']);
	$unrestraint_flag = do_escape_quotes($_POST['unrestraint_flag']);
	$test_flag = do_escape_quotes($_POST['test_flag']);
	$status = do_escape_quotes($_POST['status']);

	$method = do_escape_quotes($_POST['method']);
	$reverse = do_escape_quotes($_POST['reverse']);
	$sort = do_escape_quotes($_POST['sort']);
	$order = do_escape_quotes($_POST['order']);
	$search_id = do_escape_quotes($_POST['search_id']);
	$search_name = do_escape_quotes($_POST['search_name']);
	$created_at_flag = do_escape_quotes($_POST['created_at_flag']);

	$s_year = do_escape_quotes($_POST['s_year']);
	$s_month = do_escape_quotes($_POST['s_month']);
	$s_day = do_escape_quotes($_POST['s_day']);
	$e_year = do_escape_quotes($_POST['e_year']);
	$e_month = do_escape_quotes($_POST['e_month']);
	$e_day = do_escape_quotes($_POST['e_day']);

	$advert_start_date = "$as_year-$as_month-$as_day 00:00:00";
	$advert_end_date = "$ae_year-$ae_month-$ae_day 23:59:59";

	$three_last_month = getdate(strtotime("-3 month"));
	$now_date = getdate();

	$smarty->assign("set_as_year", $now_date['year']);
	$smarty->assign("set_as_month", $now_date['mon']);
	$smarty->assign("set_as_day", $now_date['mday']);
	$smarty->assign("set_ae_year", $now_date['year']);
	$smarty->assign("set_ae_month", $now_date['mon']);
	$smarty->assign("set_ae_day", $now_date['mday']);

	$smarty->assign("set_s_year", $three_last_month['year']);
	$smarty->assign("set_s_month", $three_last_month['mon']);
	$smarty->assign("set_s_day", $three_last_month['mday']);
	$smarty->assign("set_e_year", $now_date['year']);
	$smarty->assign("set_e_month", $now_date['mon']);
	$smarty->assign("set_e_day", $now_date['mday']);

	$list_sql = " SELECT a.*, ac.client_name as advert_client_name "
				. " FROM advert as a "
				. " left join advert_clients as ac on a.advert_client_id = ac.id "
				. " WHERE a.deleted_at is NULL "
				. " ORDER BY a.id ASC ";

	if($mode != ''){
		if($mode == 'new_regist'){
			$smarty->assign("mode", 'insert_commit');
			$smarty->assign("sub_title", '新規追加');

			// ページを表示
			$smarty->display("./advert_input.tpl");
			exit();
		}elseif($mode == 'insert_commit'){

			$error_flag = 0;

			if($advert_name == "") {
				$error_message = "広告名を入力してください。";
				$error_flag = 1;
			}

			if($ms_image_type_1 == 1) {
				if($_FILES['ms_image_file_1']['name'] != "") {
					$image_name = uniqid("image_");
					$result = insert_image_file($_FILES['ms_image_file_1'], $image_directory, $image_name, $result_message);

					if($result) {
						$ms_image_url_1 = $result_message;
					} else {
						$error_mesage = $result_message;
						$error_flag = 1;
					}
				}
			}

			if($ms_image_type_2 == 1) {
				if($_FILES['ms_image_file_2']['name'] != "") {
					$image_name = uniqid("image_");
					$result = insert_image_file($_FILES['ms_image_file_2'], $image_directory, $image_name, $result_message);

					if($result) {
						$ms_image_url_2 = $result_message;
					} else {
						$error_mesage = $result_message;
						$error_flag = 1;
					}
				}
			}

			if($ms_image_type_3 == 1) {
				if($_FILES['ms_image_file_3']['name'] != "") {
					$image_name = uniqid("image_");
					$result = insert_image_file($_FILES['ms_image_file_3'], $image_directory, $image_name, $result_message);

					if($result) {
						$ms_image_url_3 = $result_message;
					} else {
						$error_mesage = $result_message;
						$error_flag = 1;
					}
				}
			}

			if($ms_image_type_4 == 1) {
				if($_FILES['ms_image_file_4']['name'] != "") {
					$image_name = uniqid("image_");
					$result = insert_image_file($_FILES['ms_image_file_4'], $image_directory, $image_name, $result_message);

					if($result) {
						$ms_image_url_4 = $result_message;
					} else {
						$error_mesage = $result_message;
						$error_flag = 1;
					}
				}
			}

			if($ms_image_type_5 == 1) {
				if($_FILES['ms_image_file_5']['name'] != "") {
					$image_name = uniqid("image_");
					$result = insert_image_file($_FILES['ms_image_file_5'], $image_directory, $image_name, $result_message);

					if($result) {
						$ms_image_url_5 = $result_message;
					} else {
						$error_mesage = $result_message;
						$error_flag = 1;
					}
				}
			}

			if($error_flag == 0) {
				$advert_dao->transaction_start();

				$advert = new Advert();
				$advert->setAdvertClientId($advert_client_id);
				$advert->setAdvertCategoryId($advert_category_id);
				$advert->setAdvertName($advert_name);
				$advert->setContentType($content_type);
				$advert->setSupportDocomo($support_docomo);
				$advert->setSupportSoftbank($support_softbank);
				$advert->setSupportAu($support_au);
				$advert->setSupportPc($support_pc);
				$advert->setSiteUrlDocomo($site_url_docomo);
				$advert->setSiteUrlSoftbank($site_url_softbank);
				$advert->setSiteUrlAu($site_url_au);
				$advert->setSiteUrlPc($site_url_pc);
				$advert->setSiteOutline($site_outline);
				$advert->setClickPriceClient($click_price_client);
				$advert->setClickPriceMedia($click_price_media);
				$advert->setActionPriceClientDocomo1($action_price_client_docomo_1);
				$advert->setActionPriceClientSoftbank1($action_price_client_softbank_1);
				$advert->setActionPriceClientAu1($action_price_client_au_1);
				$advert->setActionPriceClientPc1($action_price_client_pc_1);
				$advert->setActionPriceClientDocomo2($action_price_client_docomo_2);
				$advert->setActionPriceClientSoftbank2($action_price_client_softbank_2);
				$advert->setActionPriceClientAu2($action_price_client_au_2);
				$advert->setActionPriceClientPc2($action_price_client_pc_2);
				$advert->setActionPriceClientDocomo3($action_price_client_docomo_3);
				$advert->setActionPriceClientSoftbank3($action_price_client_softbank_3);
				$advert->setActionPriceClientAu3($action_price_client_au_3);
				$advert->setActionPriceClientPc3($action_price_client_pc_3);
				$advert->setActionPriceClientDocomo4($action_price_client_docomo_4);
				$advert->setActionPriceClientSoftbank4($action_price_client_softbank_4);
				$advert->setActionPriceClientAu4($action_price_client_au_4);
				$advert->setActionPriceClientPc4($action_price_client_pc_4);
				$advert->setActionPriceClientDocomo5($action_price_client_docomo_5);
				$advert->setActionPriceClientSoftbank5($action_price_client_softbank_5);
				$advert->setActionPriceClientAu5($action_price_client_au_5);
				$advert->setActionPriceClientPc5($action_price_client_pc_5);
				$advert->setActionPriceMediaDocomo1($action_price_media_docomo_1);
				$advert->setActionPriceMediaSoftbank1($action_price_media_softbank_1);
				$advert->setActionPriceMediaAu1($action_price_media_au_1);
				$advert->setActionPriceMediaPc1($action_price_media_pc_1);
				$advert->setActionPriceMediaDocomo2($action_price_media_docomo_2);
				$advert->setActionPriceMediaSoftbank2($action_price_media_softbank_2);
				$advert->setActionPriceMediaAu2($action_price_media_au_2);
				$advert->setActionPriceMediaPc2($action_price_media_pc_2);
				$advert->setActionPriceMediaDocomo3($action_price_media_docomo_3);
				$advert->setActionPriceMediaSoftbank3($action_price_media_softbank_3);
				$advert->setActionPriceMediaAu3($action_price_media_au_3);
				$advert->setActionPriceMediaPc3($action_price_media_pc_3);
				$advert->setActionPriceMediaDocomo4($action_price_media_docomo_4);
				$advert->setActionPriceMediaSoftbank4($action_price_media_softbank_4);
				$advert->setActionPriceMediaAu4($action_price_media_au_4);
				$advert->setActionPriceMediaPc4($action_price_media_pc_4);
				$advert->setActionPriceMediaDocomo5($action_price_media_docomo_5);
				$advert->setActionPriceMediaSoftbank5($action_price_media_softbank_5);
				$advert->setActionPriceMediaAu5($action_price_media_au_5);
				$advert->setActionPriceMediaPc5($action_price_media_pc_5);
				$advert->setMsText1($ms_text_1);
				$advert->setMsEmail1($ms_email_1);
				$advert->setMsImageType1($ms_image_type_1);
				$advert->setMsImageUrl1($ms_image_url_1);
				$advert->setMsText2($ms_text_2);
				$advert->setMsEmail2($ms_email_2);
				$advert->setMsImageType2($ms_image_type_2);
				$advert->setMsImageUrl2($ms_image_url_2);
				$advert->setMsText3($ms_text_3);
				$advert->setMsEmail3($ms_email_3);
				$advert->setMsImageType3($ms_image_type_3);
				$advert->setMsImageUrl3($ms_image_url_3);
				$advert->setMsText4($ms_text_4);
				$advert->setMsEmail4($ms_email_4);
				$advert->setMsImageType4($ms_image_type_4);
				$advert->setMsImageUrl4($ms_image_url_4);
				$advert->setMsText5($ms_text_5);
				$advert->setMsEmail5($ms_email_5);
				$advert->setMsImageType5($ms_image_type_5);
				$advert->setMsImageUrl5($ms_image_url_5);
				$advert->setUniqueClickType($unique_click_type);
				$advert->setPointBackFlag($point_back_flag);
				$advert->setAdultFlag($adult_flag);
				$advert->setDatingFlag($dating_flag);
				$advert->setAdvertStartDate($advert_start_date);
				$advert->setAdvertEndDate($advert_end_date);
				$advert->setUnrestraintFlag($unrestraint_flag);
				$advert->setTestFlag($test_flag);
				$advert->setStatus($status);

				//INSERTを実行
				$db_result = $advert_dao->insertAdvert($advert, $result_message);
				if($db_result) {
					$advert_dao->transaction_end();

					$smarty->assign("info_message", $result_message);

					view_list();
				} else {
					$advert_dao->transaction_rollback();

					$smarty->assign("error_message", $result_message);

					$form_data = array('id' => $id,
										'advert_client_id' => $advert_client_id,
										'advert_category_id' => $advert_category_id,
										'advert_name' => $advert_name,
										'content_type' => $content_type,
										'support_docomo' => $support_docomo,
										'support_softbank' => $support_softbank,
										'support_au' => $support_au,
										'support_pc' => $support_pc,
										'site_url_docomo' => $site_url_docomo,
										'site_url_softbank' => $site_url_softbank,
										'site_url_au' => $site_url_au,
										'site_url_pc' => $site_url_pc,
										'site_outline' => $site_outline,
										'click_price_client' => $click_price_client,
										'click_price_media' => $click_price_media,
										'action_price_client_docomo_1' => $action_price_client_docomo_1,
										'action_price_client_softbank_1' => $action_price_client_softbank_1,
										'action_price_client_au_1' => $action_price_client_au_1,
										'action_price_client_pc_1' => $action_price_client_pc_1,
										'action_price_client_docomo_2' => $action_price_client_docomo_2,
										'action_price_client_softbank_2' => $action_price_client_softbank_2,
										'action_price_client_au_2' => $action_price_client_au_2,
										'action_price_client_pc_2' => $action_price_client_pc_2,
										'action_price_client_docomo_3' => $action_price_client_docomo_3,
										'action_price_client_softbank_3' => $action_price_client_softbank_3,
										'action_price_client_au_3' => $action_price_client_au_3,
										'action_price_client_pc_3' => $action_price_client_pc_3,
										'action_price_client_docomo_4' => $action_price_client_docomo_4,
										'action_price_client_softbank_4' => $action_price_client_softbank_4,
										'action_price_client_au_4' => $action_price_client_au_4,
										'action_price_client_pc_4' => $action_price_client_pc_4,
										'action_price_client_docomo_5' => $action_price_client_docomo_5,
										'action_price_client_softbank_5' => $action_price_client_softbank_5,
										'action_price_client_au_5' => $action_price_client_au_5,
										'action_price_client_pc_5' => $action_price_client_pc_5,
										'action_price_media_docomo_1' => $action_price_media_docomo_1,
										'action_price_media_softbank_1' => $action_price_media_softbank_1,
										'action_price_media_au_1' => $action_price_media_au_1,
										'action_price_media_pc_1' => $action_price_media_pc_1,
										'action_price_media_docomo_2' => $action_price_media_docomo_2,
										'action_price_media_softbank_2' => $action_price_media_softbank_2,
										'action_price_media_au_2' => $action_price_media_au_2,
										'action_price_media_pc_2' => $action_price_media_pc_2,
										'action_price_media_docomo_3' => $action_price_media_docomo_3,
										'action_price_media_softbank_3' => $action_price_media_softbank_3,
										'action_price_media_au_3' => $action_price_media_au_3,
										'action_price_media_pc_3' => $action_price_media_pc_3,
										'action_price_media_docomo_4' => $action_price_media_docomo_4,
										'action_price_media_softbank_4' => $action_price_media_softbank_4,
										'action_price_media_au_4' => $action_price_media_au_4,
										'action_price_media_pc_4' => $action_price_media_pc_4,
										'action_price_media_docomo_5' => $action_price_media_docomo_5,
										'action_price_media_softbank_5' => $action_price_media_softbank_5,
										'action_price_media_au_5' => $action_price_media_au_5,
										'action_price_media_pc_5' => $action_price_media_pc_5,
										'ms_text_1' => $ms_text_1,
										'ms_email_1' => $ms_email_1,
										'ms_image_type_1' => $ms_image_type_1,
										'ms_image_url_1' => $ms_image_url_1,
										'ms_text_2' => $ms_text_2,
										'ms_email_2' => $ms_email_2,
										'ms_image_type_2' => $ms_image_type_2,
										'ms_image_url_2' => $ms_image_url_2,
										'ms_text_3' => $ms_text_3,
										'ms_email_3' => $ms_email_3,
										'ms_image_type_3' => $ms_image_type_3,
										'ms_image_url_3' => $ms_image_url_3,
										'ms_text_4' => $ms_text_4,
										'ms_email_4' => $ms_email_4,
										'ms_image_type_4' => $ms_image_type_4,
										'ms_image_url_4' => $ms_image_url_4,
										'ms_text_5' => $ms_text_5,
										'ms_email_5' => $ms_email_5,
										'ms_image_type_5' => $ms_image_type_5,
										'ms_image_url_5' => $ms_image_url_5,
										'unique_click_type' => $unique_click_type,
										'point_back_flag' => $point_back_flag,
										'adult_flag' => $adult_flag,
										'dating_flag' => $dating_flag,
										'advert_start_date' => $advert_start_date,
										'advert_end_date' => $advert_end_date,
										'unrestraint_flag' => $unrestraint_flag,
										'test_flag' => $test_flag,
										'status' => $status);

					$smarty->assign("form_data", $form_data);

					$smarty->assign("set_as_year", $as_year);
					$smarty->assign("set_as_month", $as_month);
					$smarty->assign("set_as_day", $as_day);
					$smarty->assign("set_ae_year", $ae_year);
					$smarty->assign("set_ae_month", $ae_month);
					$smarty->assign("set_ae_day", $ae_day);

					$smarty->assign("mode", 'insert_commit');
					$smarty->assign("sub_title", '新規追加');

					// ページを表示
					$smarty->display("./advert_input.tpl");
					exit();
				}
			} else {
				$smarty->assign("error_message", $error_message);

				$form_data = array('id' => $id,
									'advert_client_id' => $advert_client_id,
									'advert_category_id' => $advert_category_id,
									'advert_name' => $advert_name,
									'content_type' => $content_type,
									'support_docomo' => $support_docomo,
									'support_softbank' => $support_softbank,
									'support_au' => $support_au,
									'support_pc' => $support_pc,
									'site_url_docomo' => $site_url_docomo,
									'site_url_softbank' => $site_url_softbank,
									'site_url_au' => $site_url_au,
									'site_url_pc' => $site_url_pc,
									'site_outline' => $site_outline,
									'click_price_client' => $click_price_client,
									'click_price_media' => $click_price_media,
									'action_price_client_docomo_1' => $action_price_client_docomo_1,
									'action_price_client_softbank_1' => $action_price_client_softbank_1,
									'action_price_client_au_1' => $action_price_client_au_1,
									'action_price_client_pc_1' => $action_price_client_pc_1,
									'action_price_client_docomo_2' => $action_price_client_docomo_2,
									'action_price_client_softbank_2' => $action_price_client_softbank_2,
									'action_price_client_au_2' => $action_price_client_au_2,
									'action_price_client_pc_2' => $action_price_client_pc_2,
									'action_price_client_docomo_3' => $action_price_client_docomo_3,
									'action_price_client_softbank_3' => $action_price_client_softbank_3,
									'action_price_client_au_3' => $action_price_client_au_3,
									'action_price_client_pc_3' => $action_price_client_pc_3,
									'action_price_client_docomo_4' => $action_price_client_docomo_4,
									'action_price_client_softbank_4' => $action_price_client_softbank_4,
									'action_price_client_au_4' => $action_price_client_au_4,
									'action_price_client_pc_4' => $action_price_client_pc_4,
									'action_price_client_docomo_5' => $action_price_client_docomo_5,
									'action_price_client_softbank_5' => $action_price_client_softbank_5,
									'action_price_client_au_5' => $action_price_client_au_5,
									'action_price_client_pc_5' => $action_price_client_pc_5,
									'action_price_media_docomo_1' => $action_price_media_docomo_1,
									'action_price_media_softbank_1' => $action_price_media_softbank_1,
									'action_price_media_au_1' => $action_price_media_au_1,
									'action_price_media_pc_1' => $action_price_media_pc_1,
									'action_price_media_docomo_2' => $action_price_media_docomo_2,
									'action_price_media_softbank_2' => $action_price_media_softbank_2,
									'action_price_media_au_2' => $action_price_media_au_2,
									'action_price_media_pc_2' => $action_price_media_pc_2,
									'action_price_media_docomo_3' => $action_price_media_docomo_3,
									'action_price_media_softbank_3' => $action_price_media_softbank_3,
									'action_price_media_au_3' => $action_price_media_au_3,
									'action_price_media_pc_3' => $action_price_media_pc_3,
									'action_price_media_docomo_4' => $action_price_media_docomo_4,
									'action_price_media_softbank_4' => $action_price_media_softbank_4,
									'action_price_media_au_4' => $action_price_media_au_4,
									'action_price_media_pc_4' => $action_price_media_pc_4,
									'action_price_media_docomo_5' => $action_price_media_docomo_5,
									'action_price_media_softbank_5' => $action_price_media_softbank_5,
									'action_price_media_au_5' => $action_price_media_au_5,
									'action_price_media_pc_5' => $action_price_media_pc_5,
									'ms_text_1' => $ms_text_1,
									'ms_email_1' => $ms_email_1,
									'ms_image_type_1' => $ms_image_type_1,
									'ms_image_url_1' => $ms_image_url_1,
									'ms_text_2' => $ms_text_2,
									'ms_email_2' => $ms_email_2,
									'ms_image_type_2' => $ms_image_type_2,
									'ms_image_url_2' => $ms_image_url_2,
									'ms_text_3' => $ms_text_3,
									'ms_email_3' => $ms_email_3,
									'ms_image_type_3' => $ms_image_type_3,
									'ms_image_url_3' => $ms_image_url_3,
									'ms_text_4' => $ms_text_4,
									'ms_email_4' => $ms_email_4,
									'ms_image_type_4' => $ms_image_type_4,
									'ms_image_url_4' => $ms_image_url_4,
									'ms_text_5' => $ms_text_5,
									'ms_email_5' => $ms_email_5,
									'ms_image_type_5' => $ms_image_type_5,
									'ms_image_url_5' => $ms_image_url_5,
									'unique_click_type' => $unique_click_type,
									'point_back_flag' => $point_back_flag,
									'adult_flag' => $adult_flag,
									'dating_flag' => $dating_flag,
									'advert_start_date' => $advert_start_date,
									'advert_end_date' => $advert_end_date,
									'unrestraint_flag' => $unrestraint_flag,
									'test_flag' => $test_flag,
									'status' => $status);

				$smarty->assign("form_data", $form_data);

				$smarty->assign("set_as_year", $as_year);
				$smarty->assign("set_as_month", $as_month);
				$smarty->assign("set_as_day", $as_day);
				$smarty->assign("set_ae_year", $ae_year);
				$smarty->assign("set_ae_month", $ae_month);
				$smarty->assign("set_ae_day", $ae_day);

				$smarty->assign("mode", 'insert_commit');
				$smarty->assign("sub_title", '新規追加');

				// ページを表示
				$smarty->display("./advert_input.tpl");
				exit();
			}
		}elseif($mode == 'edit'){
			$advert = new Advert;
			$advert = $advert_dao->getAdvertById($id);

			if(!is_null($advert)) {
				$form_data = array('id' => $advert->getId(),
									'advert_client_id' => $advert->getAdvertClientId(),
									'advert_category_id' => $advert->getAdvertCategoryId(),
									'advert_name' => $advert->getAdvertName(),
									'content_type' => $advert->getContentType(),
									'support_docomo' => $advert->getSupportDocomo(),
									'support_softbank' => $advert->getSupportSoftbank(),
									'support_au' => $advert->getSupportAu(),
									'support_pc' => $advert->getSupportPc(),
									'site_url_docomo' => $advert->getSiteUrlDocomo(),
									'site_url_softbank' => $advert->getSiteUrlSoftbank(),
									'site_url_au' => $advert->getSiteUrlAu(),
									'site_url_pc' => $advert->getSiteUrlPc(),
									'site_outline' => $advert->getSiteOutline(),
									'click_price_client' => $advert->getClickPriceClient(),
									'click_price_media' => $advert->getClickPriceMedia(),
									'action_price_client_docomo_1' => $advert->getActionPriceClientDocomo1(),
									'action_price_client_softbank_1' => $advert->getActionPriceClientSoftbank1(),
									'action_price_client_au_1' => $advert->getActionPriceClientAu1(),
									'action_price_client_pc_1' => $advert->getActionPriceClientPc1(),
									'action_price_client_docomo_2' => $advert->getActionPriceClientDocomo2(),
									'action_price_client_softbank_2' => $advert->getActionPriceClientSoftbank2(),
									'action_price_client_au_2' => $advert->getActionPriceClientAu2(),
									'action_price_client_pc_2' => $advert->getActionPriceClientPc2(),
									'action_price_client_docomo_3' => $advert->getActionPriceClientDocomo3(),
									'action_price_client_softbank_3' => $advert->getActionPriceClientSoftbank3(),
									'action_price_client_au_3' => $advert->getActionPriceClientAu3(),
									'action_price_client_pc_3' => $advert->getActionPriceClientPc3(),
									'action_price_client_docomo_4' => $advert->getActionPriceClientDocomo4(),
									'action_price_client_softbank_4' => $advert->getActionPriceClientSoftbank4(),
									'action_price_client_au_4' => $advert->getActionPriceClientAu4(),
									'action_price_client_pc_4' => $advert->getActionPriceClientPc4(),
									'action_price_client_docomo_5' => $advert->getActionPriceClientDocomo5(),
									'action_price_client_softbank_5' => $advert->getActionPriceClientSoftbank5(),
									'action_price_client_au_5' => $advert->getActionPriceClientAu5(),
									'action_price_client_pc_5' => $advert->getActionPriceClientPc5(),
									'action_price_media_docomo_1' => $advert->getActionPriceMediaDocomo1(),
									'action_price_media_softbank_1' => $advert->getActionPriceMediaSoftbank1(),
									'action_price_media_au_1' => $advert->getActionPriceMediaAu1(),
									'action_price_media_pc_1' => $advert->getActionPriceMediaPc1(),
									'action_price_media_docomo_2' => $advert->getActionPriceMediaDocomo2(),
									'action_price_media_softbank_2' => $advert->getActionPriceMediaSoftbank2(),
									'action_price_media_au_2' => $advert->getActionPriceMediaAu2(),
									'action_price_media_pc_2' => $advert->getActionPriceMediaPc2(),
									'action_price_media_docomo_3' => $advert->getActionPriceMediaDocomo3(),
									'action_price_media_softbank_3' => $advert->getActionPriceMediaSoftbank3(),
									'action_price_media_au_3' => $advert->getActionPriceMediaAu3(),
									'action_price_media_pc_3' => $advert->getActionPriceMediaPc3(),
									'action_price_media_docomo_4' => $advert->getActionPriceMediaDocomo4(),
									'action_price_media_softbank_4' => $advert->getActionPriceMediaSoftbank4(),
									'action_price_media_au_4' => $advert->getActionPriceMediaAu4(),
									'action_price_media_pc_4' => $advert->getActionPriceMediaPc4(),
									'action_price_media_docomo_5' => $advert->getActionPriceMediaDocomo5(),
									'action_price_media_softbank_5' => $advert->getActionPriceMediaSoftbank5(),
									'action_price_media_au_5' => $advert->getActionPriceMediaAu5(),
									'action_price_media_pc_5' => $advert->getActionPriceMediaPc5(),
									'ms_text_1' => $advert->getMsText1(),
									'ms_email_1' => $advert->getMsEmail1(),
									'ms_image_type_1' => $advert->getMsImageType1(),
									'ms_image_url_1' => $advert->getMsImageUrl1(),
									'ms_text_2' => $advert->getMsText2(),
									'ms_email_2' => $advert->getMsEmail2(),
									'ms_image_type_2' => $advert->getMsImageType2(),
									'ms_image_url_2' => $advert->getMsImageUrl2(),
									'ms_text_3' => $advert->getMsText3(),
									'ms_email_3' => $advert->getMsEmail3(),
									'ms_image_type_3' => $advert->getMsImageType3(),
									'ms_image_url_3' => $advert->getMsImageUrl3(),
									'ms_text_4' => $advert->getMsText4(),
									'ms_email_4' => $advert->getMsEmail4(),
									'ms_image_type_4' => $advert->getMsImageType4(),
									'ms_image_url_4' => $advert->getMsImageUrl4(),
									'ms_text_5' => $advert->getMsText5(),
									'ms_email_5' => $advert->getMsEmail5(),
									'ms_image_type_5' => $advert->getMsImageType5(),
									'ms_image_url_5' => $advert->getMsImageUrl5(),
									'unique_click_type' => $advert->getUniqueClickType(),
									'point_back_flag' => $advert->getPointBackFlag(),
									'adult_flag' => $advert->getAdultFlag(),
									'dating_flag' => $advert->getDatingFlag(),
									'advert_start_date' => $advert->getAdvertStartDate(),
									'advert_end_date' => $advert->getAdvertEndDate(),
									'unrestraint_flag' => $advert->getUnrestraintFlag(),
									'test_flag' => $advert->getTestFlag(),
									'status' => $advert->getStatus());

				$smarty->assign("form_data", $form_data);

				if($advert->getMsImageType1() == 1) {
					$ms_image_path_1 = $image_directory . $advert->getMsImageUrl1();
				} else {
					$ms_image_path_1 = $advert->getMsImageUrl1();
				}
				$smarty->assign("ms_image_path_1", $ms_image_path_1);

				if($advert->getMsImageType2() == 1) {
					$ms_image_path_2 = $image_directory . $advert->getMsImageUrl2();
				} else {
					$ms_image_path_2 = $advert->getMsImageUrl2();
				}
				$smarty->assign("ms_image_path_2", $ms_image_path_2);

				if($advert->getMsImageType3() == 1) {
					$ms_image_path_3 = $image_directory . $advert->getMsImageUrl3();
				} else {
					$ms_image_path_3 = $advert->getMsImageUrl3();
				}
				$smarty->assign("ms_image_path_3", $ms_image_path_3);

				if($advert->getMsImageType4() == 1) {
					$ms_image_path_4 = $image_directory . $advert->getMsImageUrl4();
				} else {
					$ms_image_path_4 = $advert->getMsImageUrl4();
				}
				$smarty->assign("ms_image_path_4", $ms_image_path_4);

				if($advert->getMsImageType5() == 1) {
					$ms_image_path_5 = $image_directory . $advert->getMsImageUrl5();
				} else {
					$ms_image_path_5 = $advert->getMsImageUrl5();
				}
				$smarty->assign("ms_image_path_5", $ms_image_path_5);

				$as = explode("-", $advert->getAdvertStartDate());
				$ae = explode("-", $advert->getAdvertEndDate());

				$smarty->assign("set_as_year", $as[0]);
				$smarty->assign("set_as_month", $as[1]);
				$smarty->assign("set_as_day", $as[2]);
				$smarty->assign("set_ae_year", $ae[0]);
				$smarty->assign("set_ae_month", $ae[1]);
				$smarty->assign("set_ae_day", $ae[2]);

				$smarty->assign("mode", 'update_commit');
				$smarty->assign("sub_title", '編集');

				// ページを表示
				$smarty->display("./advert_input.tpl");
				exit();
			} else {
				$error_message .= "該当するデータはありません。";
				$smarty->assign("error_message", $error_message);

				$smarty->assign("mode", 'insert_commit');
				$smarty->assign("sub_title", '新規追加');

				// ページを表示
				$smarty->display("./advert_input.tpl");
				exit();
			}
		}elseif($mode == 'update_commit'){

			$error_flag = 0;

			if($advert_name == "") {
				$error_message = "広告名を入力してください。";
				$error_flag = 1;
			}

			if($ms_image_type_1 == 1) {
				if($_FILES['ms_image_file_1']['name'] != "") {
					$image_name = uniqid("image_");
					$result = insert_image_file($_FILES['ms_image_file_1'], $image_directory, $image_name, $result_message);

					if($result) {
						$ms_image_url_1 = $result_message;
					} else {
						$error_mesage = $result_message;
						$error_flag = 1;
					}
				}
			}

			if($ms_image_type_2 == 1) {
				if($_FILES['ms_image_file_2']['name'] != "") {
					$image_name = uniqid("image_");
					$result = insert_image_file($_FILES['ms_image_file_2'], $image_directory, $image_name, $result_message);

					if($result) {
						$ms_image_url_2 = $result_message;
					} else {
						$error_mesage = $result_message;
						$error_flag = 1;
					}
				}
			}

			if($ms_image_type_3 == 1) {
				if($_FILES['ms_image_file_3']['name'] != "") {
					$image_name = uniqid("image_");
					$result = insert_image_file($_FILES['ms_image_file_3'], $image_directory, $image_name, $result_message);

					if($result) {
						$ms_image_url_3 = $result_message;
					} else {
						$error_mesage = $result_message;
						$error_flag = 1;
					}
				}
			}

			if($ms_image_type_4 == 1) {
				if($_FILES['ms_image_file_4']['name'] != "") {
					$image_name = uniqid("image_");
					$result = insert_image_file($_FILES['ms_image_file_4'], $image_directory, $image_name, $result_message);

					if($result) {
						$ms_image_url_4 = $result_message;
					} else {
						$error_mesage = $result_message;
						$error_flag = 1;
					}
				}
			}

			if($ms_image_type_5 == 1) {
				if($_FILES['ms_image_file_5']['name'] != "") {
					$image_name = uniqid("image_");
					$result = insert_image_file($_FILES['ms_image_file_5'], $image_directory, $image_name, $result_message);

					if($result) {
						$ms_image_url_5 = $result_message;
					} else {
						$error_mesage = $result_message;
						$error_flag = 1;
					}
				}
			}

			if($error_flag == 0) {
				$advert_dao->transaction_start();

				$advert = new Advert();
				$advert->setId($id);
				$advert->setAdvertClientId($advert_client_id);
				$advert->setAdvertCategoryId($advert_category_id);
				$advert->setAdvertName($advert_name);
				$advert->setContentType($content_type);
				$advert->setSupportDocomo($support_docomo);
				$advert->setSupportSoftbank($support_softbank);
				$advert->setSupportAu($support_au);
				$advert->setSupportPc($support_pc);
				$advert->setSiteUrlDocomo($site_url_docomo);
				$advert->setSiteUrlSoftbank($site_url_softbank);
				$advert->setSiteUrlAu($site_url_au);
				$advert->setSiteUrlPc($site_url_pc);
				$advert->setSiteOutline($site_outline);
				$advert->setClickPriceClient($click_price_client);
				$advert->setClickPriceMedia($click_price_media);
				$advert->setActionPriceClientDocomo1($action_price_client_docomo_1);
				$advert->setActionPriceClientSoftbank1($action_price_client_softbank_1);
				$advert->setActionPriceClientAu1($action_price_client_au_1);
				$advert->setActionPriceClientPc1($action_price_client_pc_1);
				$advert->setActionPriceClientDocomo2($action_price_client_docomo_2);
				$advert->setActionPriceClientSoftbank2($action_price_client_softbank_2);
				$advert->setActionPriceClientAu2($action_price_client_au_2);
				$advert->setActionPriceClientPc2($action_price_client_pc_2);
				$advert->setActionPriceClientDocomo3($action_price_client_docomo_3);
				$advert->setActionPriceClientSoftbank3($action_price_client_softbank_3);
				$advert->setActionPriceClientAu3($action_price_client_au_3);
				$advert->setActionPriceClientPc3($action_price_client_pc_3);
				$advert->setActionPriceClientDocomo4($action_price_client_docomo_4);
				$advert->setActionPriceClientSoftbank4($action_price_client_softbank_4);
				$advert->setActionPriceClientAu4($action_price_client_au_4);
				$advert->setActionPriceClientPc4($action_price_client_pc_4);
				$advert->setActionPriceClientDocomo5($action_price_client_docomo_5);
				$advert->setActionPriceClientSoftbank5($action_price_client_softbank_5);
				$advert->setActionPriceClientAu5($action_price_client_au_5);
				$advert->setActionPriceClientPc5($action_price_client_pc_5);
				$advert->setActionPriceMediaDocomo1($action_price_media_docomo_1);
				$advert->setActionPriceMediaSoftbank1($action_price_media_softbank_1);
				$advert->setActionPriceMediaAu1($action_price_media_au_1);
				$advert->setActionPriceMediaPc1($action_price_media_pc_1);
				$advert->setActionPriceMediaDocomo2($action_price_media_docomo_2);
				$advert->setActionPriceMediaSoftbank2($action_price_media_softbank_2);
				$advert->setActionPriceMediaAu2($action_price_media_au_2);
				$advert->setActionPriceMediaPc2($action_price_media_pc_2);
				$advert->setActionPriceMediaDocomo3($action_price_media_docomo_3);
				$advert->setActionPriceMediaSoftbank3($action_price_media_softbank_3);
				$advert->setActionPriceMediaAu3($action_price_media_au_3);
				$advert->setActionPriceMediaPc3($action_price_media_pc_3);
				$advert->setActionPriceMediaDocomo4($action_price_media_docomo_4);
				$advert->setActionPriceMediaSoftbank4($action_price_media_softbank_4);
				$advert->setActionPriceMediaAu4($action_price_media_au_4);
				$advert->setActionPriceMediaPc4($action_price_media_pc_4);
				$advert->setActionPriceMediaDocomo5($action_price_media_docomo_5);
				$advert->setActionPriceMediaSoftbank5($action_price_media_softbank_5);
				$advert->setActionPriceMediaAu5($action_price_media_au_5);
				$advert->setActionPriceMediaPc5($action_price_media_pc_5);
				$advert->setMsText1($ms_text_1);
				$advert->setMsEmail1($ms_email_1);
				$advert->setMsImageType1($ms_image_type_1);
				$advert->setMsImageUrl1($ms_image_url_1);
				$advert->setMsText2($ms_text_2);
				$advert->setMsEmail2($ms_email_2);
				$advert->setMsImageType2($ms_image_type_2);
				$advert->setMsImageUrl2($ms_image_url_2);
				$advert->setMsText3($ms_text_3);
				$advert->setMsEmail3($ms_email_3);
				$advert->setMsImageType3($ms_image_type_3);
				$advert->setMsImageUrl3($ms_image_url_3);
				$advert->setMsText4($ms_text_4);
				$advert->setMsEmail4($ms_email_4);
				$advert->setMsImageType4($ms_image_type_4);
				$advert->setMsImageUrl4($ms_image_url_4);
				$advert->setMsText5($ms_text_5);
				$advert->setMsEmail5($ms_email_5);
				$advert->setMsImageType5($ms_image_type_5);
				$advert->setMsImageUrl5($ms_image_url_5);
				$advert->setUniqueClickType($unique_click_type);
				$advert->setPointBackFlag($point_back_flag);
				$advert->setAdultFlag($adult_flag);
				$advert->setDatingFlag($dating_flag);
				$advert->setAdvertStartDate($advert_start_date);
				$advert->setAdvertEndDate($advert_end_date);
				$advert->setUnrestraintFlag($unrestraint_flag);
				$advert->setTestFlag($test_flag);
				$advert->setStatus($status);

				//UPDATEを実行
				$db_result = $advert_dao->updateAdvert($advert, $result_message);
				if($db_result) {
					$advert_dao->transaction_end();

					$smarty->assign("info_message", $result_message);

					view_list();
				} else {
					$advert_dao->transaction_rollback();

					$smarty->assign("error_message", $result_message);

					$form_data = array('id' => $id,
										'advert_client_id' => $advert_client_id,
										'advert_category_id' => $advert_category_id,
										'advert_name' => $advert_name,
										'content_type' => $content_type,
										'support_docomo' => $support_docomo,
										'support_softbank' => $support_softbank,
										'support_au' => $support_au,
										'support_pc' => $support_pc,
										'site_url_docomo' => $site_url_docomo,
										'site_url_softbank' => $site_url_softbank,
										'site_url_au' => $site_url_au,
										'site_url_pc' => $site_url_pc,
										'site_outline' => $site_outline,
										'click_price_client' => $click_price_client,
										'click_price_media' => $click_price_media,
										'action_price_client_docomo_1' => $action_price_client_docomo_1,
										'action_price_client_softbank_1' => $action_price_client_softbank_1,
										'action_price_client_au_1' => $action_price_client_au_1,
										'action_price_client_pc_1' => $action_price_client_pc_1,
										'action_price_client_docomo_2' => $action_price_client_docomo_2,
										'action_price_client_softbank_2' => $action_price_client_softbank_2,
										'action_price_client_au_2' => $action_price_client_au_2,
										'action_price_client_pc_2' => $action_price_client_pc_2,
										'action_price_client_docomo_3' => $action_price_client_docomo_3,
										'action_price_client_softbank_3' => $action_price_client_softbank_3,
										'action_price_client_au_3' => $action_price_client_au_3,
										'action_price_client_pc_3' => $action_price_client_pc_3,
										'action_price_client_docomo_4' => $action_price_client_docomo_4,
										'action_price_client_softbank_4' => $action_price_client_softbank_4,
										'action_price_client_au_4' => $action_price_client_au_4,
										'action_price_client_pc_4' => $action_price_client_pc_4,
										'action_price_client_docomo_5' => $action_price_client_docomo_5,
										'action_price_client_softbank_5' => $action_price_client_softbank_5,
										'action_price_client_au_5' => $action_price_client_au_5,
										'action_price_client_pc_5' => $action_price_client_pc_5,
										'action_price_media_docomo_1' => $action_price_media_docomo_1,
										'action_price_media_softbank_1' => $action_price_media_softbank_1,
										'action_price_media_au_1' => $action_price_media_au_1,
										'action_price_media_pc_1' => $action_price_media_pc_1,
										'action_price_media_docomo_2' => $action_price_media_docomo_2,
										'action_price_media_softbank_2' => $action_price_media_softbank_2,
										'action_price_media_au_2' => $action_price_media_au_2,
										'action_price_media_pc_2' => $action_price_media_pc_2,
										'action_price_media_docomo_3' => $action_price_media_docomo_3,
										'action_price_media_softbank_3' => $action_price_media_softbank_3,
										'action_price_media_au_3' => $action_price_media_au_3,
										'action_price_media_pc_3' => $action_price_media_pc_3,
										'action_price_media_docomo_4' => $action_price_media_docomo_4,
										'action_price_media_softbank_4' => $action_price_media_softbank_4,
										'action_price_media_au_4' => $action_price_media_au_4,
										'action_price_media_pc_4' => $action_price_media_pc_4,
										'action_price_media_docomo_5' => $action_price_media_docomo_5,
										'action_price_media_softbank_5' => $action_price_media_softbank_5,
										'action_price_media_au_5' => $action_price_media_au_5,
										'action_price_media_pc_5' => $action_price_media_pc_5,
										'ms_text_1' => $ms_text_1,
										'ms_email_1' => $ms_email_1,
										'ms_image_type_1' => $ms_image_type_1,
										'ms_image_url_1' => $ms_image_url_1,
										'ms_text_2' => $ms_text_2,
										'ms_email_2' => $ms_email_2,
										'ms_image_type_2' => $ms_image_type_2,
										'ms_image_url_2' => $ms_image_url_2,
										'ms_text_3' => $ms_text_3,
										'ms_email_3' => $ms_email_3,
										'ms_image_type_3' => $ms_image_type_3,
										'ms_image_url_3' => $ms_image_url_3,
										'ms_text_4' => $ms_text_4,
										'ms_email_4' => $ms_email_4,
										'ms_image_type_4' => $ms_image_type_4,
										'ms_image_url_4' => $ms_image_url_4,
										'ms_text_5' => $ms_text_5,
										'ms_email_5' => $ms_email_5,
										'ms_image_type_5' => $ms_image_type_5,
										'ms_image_url_5' => $ms_image_url_5,
										'unique_click_type' => $unique_click_type,
										'point_back_flag' => $point_back_flag,
										'adult_flag' => $adult_flag,
										'dating_flag' => $dating_flag,
										'advert_start_date' => $advert_start_date,
										'advert_end_date' => $advert_end_date,
										'unrestraint_flag' => $unrestraint_flag,
										'test_flag' => $test_flag,
										'status' => $status);

					$smarty->assign("form_data", $form_data);

					$smarty->assign("set_as_year", $as_year);
					$smarty->assign("set_as_month", $as_month);
					$smarty->assign("set_as_day", $as_day);
					$smarty->assign("set_ae_year", $ae_year);
					$smarty->assign("set_ae_month", $ae_month);
					$smarty->assign("set_ae_day", $ae_day);

					$smarty->assign("mode", 'update_commit');
					$smarty->assign("sub_title", '編集');

					// ページを表示
					$smarty->display("./advert_input.tpl");
					exit();
				}
			} else {
				$smarty->assign("error_message", $error_message);

				$form_data = array('id' => $id,
									'advert_client_id' => $advert_client_id,
									'advert_category_id' => $advert_category_id,
									'advert_name' => $advert_name,
									'content_type' => $content_type,
									'support_docomo' => $support_docomo,
									'support_softbank' => $support_softbank,
									'support_au' => $support_au,
									'support_pc' => $support_pc,
									'site_url_docomo' => $site_url_docomo,
									'site_url_softbank' => $site_url_softbank,
									'site_url_au' => $site_url_au,
									'site_url_pc' => $site_url_pc,
									'site_outline' => $site_outline,
									'click_price_client' => $click_price_client,
									'click_price_media' => $click_price_media,
									'action_price_client_docomo_1' => $action_price_client_docomo_1,
									'action_price_client_softbank_1' => $action_price_client_softbank_1,
									'action_price_client_au_1' => $action_price_client_au_1,
									'action_price_client_pc_1' => $action_price_client_pc_1,
									'action_price_client_docomo_2' => $action_price_client_docomo_2,
									'action_price_client_softbank_2' => $action_price_client_softbank_2,
									'action_price_client_au_2' => $action_price_client_au_2,
									'action_price_client_pc_2' => $action_price_client_pc_2,
									'action_price_client_docomo_3' => $action_price_client_docomo_3,
									'action_price_client_softbank_3' => $action_price_client_softbank_3,
									'action_price_client_au_3' => $action_price_client_au_3,
									'action_price_client_pc_3' => $action_price_client_pc_3,
									'action_price_client_docomo_4' => $action_price_client_docomo_4,
									'action_price_client_softbank_4' => $action_price_client_softbank_4,
									'action_price_client_au_4' => $action_price_client_au_4,
									'action_price_client_pc_4' => $action_price_client_pc_4,
									'action_price_client_docomo_5' => $action_price_client_docomo_5,
									'action_price_client_softbank_5' => $action_price_client_softbank_5,
									'action_price_client_au_5' => $action_price_client_au_5,
									'action_price_client_pc_5' => $action_price_client_pc_5,
									'action_price_media_docomo_1' => $action_price_media_docomo_1,
									'action_price_media_softbank_1' => $action_price_media_softbank_1,
									'action_price_media_au_1' => $action_price_media_au_1,
									'action_price_media_pc_1' => $action_price_media_pc_1,
									'action_price_media_docomo_2' => $action_price_media_docomo_2,
									'action_price_media_softbank_2' => $action_price_media_softbank_2,
									'action_price_media_au_2' => $action_price_media_au_2,
									'action_price_media_pc_2' => $action_price_media_pc_2,
									'action_price_media_docomo_3' => $action_price_media_docomo_3,
									'action_price_media_softbank_3' => $action_price_media_softbank_3,
									'action_price_media_au_3' => $action_price_media_au_3,
									'action_price_media_pc_3' => $action_price_media_pc_3,
									'action_price_media_docomo_4' => $action_price_media_docomo_4,
									'action_price_media_softbank_4' => $action_price_media_softbank_4,
									'action_price_media_au_4' => $action_price_media_au_4,
									'action_price_media_pc_4' => $action_price_media_pc_4,
									'action_price_media_docomo_5' => $action_price_media_docomo_5,
									'action_price_media_softbank_5' => $action_price_media_softbank_5,
									'action_price_media_au_5' => $action_price_media_au_5,
									'action_price_media_pc_5' => $action_price_media_pc_5,
									'ms_text_1' => $ms_text_1,
									'ms_email_1' => $ms_email_1,
									'ms_image_type_1' => $ms_image_type_1,
									'ms_image_url_1' => $ms_image_url_1,
									'ms_text_2' => $ms_text_2,
									'ms_email_2' => $ms_email_2,
									'ms_image_type_2' => $ms_image_type_2,
									'ms_image_url_2' => $ms_image_url_2,
									'ms_text_3' => $ms_text_3,
									'ms_email_3' => $ms_email_3,
									'ms_image_type_3' => $ms_image_type_3,
									'ms_image_url_3' => $ms_image_url_3,
									'ms_text_4' => $ms_text_4,
									'ms_email_4' => $ms_email_4,
									'ms_image_type_4' => $ms_image_type_4,
									'ms_image_url_4' => $ms_image_url_4,
									'ms_text_5' => $ms_text_5,
									'ms_email_5' => $ms_email_5,
									'ms_image_type_5' => $ms_image_type_5,
									'ms_image_url_5' => $ms_image_url_5,
									'unique_click_type' => $unique_click_type,
									'point_back_flag' => $point_back_flag,
									'adult_flag' => $adult_flag,
									'dating_flag' => $dating_flag,
									'advert_start_date' => $advert_start_date,
									'advert_end_date' => $advert_end_date,
									'unrestraint_flag' => $unrestraint_flag,
									'test_flag' => $test_flag,
									'status' => $status);

				$smarty->assign("form_data", $form_data);

				$smarty->assign("set_as_year", $as_year);
				$smarty->assign("set_as_month", $as_month);
				$smarty->assign("set_as_day", $as_day);
				$smarty->assign("set_ae_year", $ae_year);
				$smarty->assign("set_ae_month", $ae_month);
				$smarty->assign("set_ae_day", $ae_day);

				$smarty->assign("mode", 'update_commit');
				$smarty->assign("sub_title", '編集');

				// ページを表示
				$smarty->display("./advert_input.tpl");
				exit();
			}
		}elseif($mode == 'search'){

			$search_list_sql = " SELECT a.*, ac.client_name as advert_client_name "
								. " FROM advert as a "
								. " left join advert_clients as ac on a.advert_client_id = ac.id "
								. " WHERE a.deleted_at is NULL ";

			//絞込みWHERE文格納配列
			$add_where = array();

			switch($method) {
				case 1:	//「完全一致」
					if($search_id != "") {
						$add_where[] = "a.id = '$search_id' ";
					}
					if($search_name != "") {
						$add_where[] = "a.advert_name = '$search_name' ";
					}
					break;
				case 2:	//「前方一致」
					if($search_id != "") {
						$add_where[] = "a.id LIKE '$search_id%' ";
					}
					if($search_name != "") {
						$add_where[] = "a.advert_name LIKE '$search_name%' ";
					}
					break;
				case 3:	//「後方一致」
					if($search_id != "") {
						$add_where[] = "a.id LIKE '%$search_id' ";
					}
					if($search_name != "") {
						$add_where[] = "a.advert_name LIKE '%$search_name' ";
					}
					break;
				case 4:	//「あいまい」
					if($search_id != "") {
						$add_where[] = "a.id LIKE '%$search_id%' ";
					}
					if($search_name != "") {
						$add_where[] = "a.advert_name LIKE '%$search_name%' ";
					}
					break;
			}

			if(count($add_where) > 0) {
				if($reverse == 1) {
					$search_list_sql .= "AND NOT(".implode("OR ", $add_where).") ";
				} else {
					$search_list_sql .= "AND ".implode("AND ", $add_where);
				}
			}

			if($created_at_flag == 1) {
				if($reverse == 1) {
					$search_list_sql .= "AND NOT(a.created_at BETWEEN '$s_year-$s_month-$s_day 00:00:00' AND '$e_year-$e_month-$e_day 23:59:59') ";
				} else {
					$search_list_sql .= "AND a.created_at BETWEEN '$s_year-$s_month-$s_day 00:00:00' AND '$e_year-$e_month-$e_day 23:59:59' ";
				}

				$smarty->assign("set_s_year", $s_year);
				$smarty->assign("set_s_month", $s_month);
				$smarty->assign("set_s_day", $s_day);
				$smarty->assign("set_e_year", $e_year);
				$smarty->assign("set_e_month", $e_month);
				$smarty->assign("set_e_day", $e_day);
			}

			$search_list_sql .= "ORDER BY a.$sort $order ";

			$db_result = $common_dao->db_query($search_list_sql);
			if($db_result){
				$smarty->assign("list", $db_result);
				$list_count = count($db_result);
			}else{
				$error_message .= "該当するデータはありません。";
			}
			$smarty->assign("list_count", $list_count);
			$smarty->assign("error_message", $error_message);

			$search['method'] = $method;
			$search['reverse'] = $reverse;
			$search['sort'] = $sort;
			$search['order'] = $order;
			$search['id'] = $search_id;
			$search['name'] = $search_name;
			$search['created_at_flag'] = $created_at_flag;

			$smarty->assign("search", $search);

			$smarty->assign("mode", 'search');
			$smarty->assign("sub_title", '検索結果');

			// ページを表示
			$smarty->display("./advert_list.tpl");
			exit();
		}elseif($_POST['mode'] == 'delete'){
			$advert_dao->transaction_start();

			if(!is_null($advert_dao->getAdvertById($id))) {
				$db_result = $advert_dao->deleteAdvert($id, $result_message);
				if($db_result){
					$advert_dao->transaction_end();

					$smarty->assign("info_message", $result_message);

					view_list();
				}else{
					$advert_dao->transaction_rollback();

					$smarty->assign("error_message", $result_message);

					view_list();
				}
			}else{
				$advert_dao->transaction_rollback();

				$error_message = "ＤＢの更新に失敗しました。";
				$smarty->assign("error_message", $error_message);

				view_list();
			}
		}else{
			view_list();
		}
	}else{
		view_list();
	}
}else{
	header('Location: ./login.php?error=1');
	exit();
}

function view_list(){
	global $common_dao, $smarty, $list_sql, $error_message;

	$list_count = 0;

	$db_result = $common_dao->db_query($list_sql);
	if($db_result){
		$smarty->assign("list", $db_result);
		$list_count = count($db_result);
	}else{
		$error_message .= "ＤＢからのデータの取得に失敗しました。(su0000)";
	}
	$smarty->assign("list_count", $list_count);
	$smarty->assign("error_message", $error_message);

	// ページを表示
	$smarty->display("./advert_list.tpl");
	exit();
}

function do_escape_quotes($str){
	//magic_quotesが有効ならクウォート部分を除去
	if(get_magic_quotes_gpc()){
		$str = stripslashes($str);
	}
	return $str;
}

function insert_image_file($up_file, $up_directory, $up_name, &$result_message = "") {
	$image_size = $up_file['size'];
	$image_tmp = $up_file['tmp_name'];

	if($image_size <= 4000000) {
		$image_info = getimagesize($image_tmp);
		if($image_info[2] == 1) {
			$file_type = "gif";
		} elseif($image_info[2] == 2) {
			$file_type = "jpg";
		} elseif($image_info[2] == 3) {
			$file_type = "png";
		} else {
			$error_message = "対応画像ファイルではありません。";
			return false;
		}

		$up_file_path = $up_directory . $up_name . "." . $file_type;

		if(move_uploaded_file($image_tmp, $up_file_path)) {
			chmod($up_file_path,0666);
			$result_message = $up_name . "." . $file_type;
			return true;
		} else {
			$result_message = "画像ファイルの保存に失敗しました。";
			return false;
		}
	} else {
		$result_message = "画像ファイルのサイズが大きすぎます。(4000KBまで）";
		return false;
	}
}
?>