<?php
// 共通設定
require_once( '../common/CommonAdminBase.php' );
require_once( '../common/CommonDao.php' );
require_once( '../dto/LoginUser.php' );
require_once( '../dao/AdvertCategoryDao.php' );
require_once( '../dto/AdvertCategory.php' );

session_start();

if(isset($_SESSION['logon_token']) && $_SESSION['logon_token'] != ''){
	$login_user = new LoginUser();
	$login_user = $_SESSION['login_user'];

	// Smartyオブジェクト取得
	$smarty =& getSmartyObj();

	$smarty->assign("title", "Test Top Page");
	$smarty->assign("login_user", $login_user );

	$common_dao = new CommonDao();
	$advert_category_dao = new AdvertCategoryDao();

	$id = do_escape_quotes($_POST['id']);
	$name = do_escape_quotes($_POST['name']);

	$method = do_escape_quotes($_POST['method']);
	$reverse = do_escape_quotes($_POST['reverse']);
	$sort = do_escape_quotes($_POST['sort']);
	$order = do_escape_quotes($_POST['order']);
	$category_id = do_escape_quotes($_POST['category_id']);
	$category_name = do_escape_quotes($_POST['category_name']);
	$created_at_flag = do_escape_quotes($_POST['created_at_flag']);
	$s_year = do_escape_quotes($_POST['s_year']);
	$s_month = do_escape_quotes($_POST['s_month']);
	$s_day = do_escape_quotes($_POST['s_day']);
	$e_year = do_escape_quotes($_POST['e_year']);
	$e_month = do_escape_quotes($_POST['e_month']);
	$e_day = do_escape_quotes($_POST['e_day']);

	$three_last_month = getdate(strtotime("-3 month"));
	$now_date = getdate();

	$smarty->assign("set_s_year", $three_last_month['year']);
	$smarty->assign("set_s_month", $three_last_month['mon']);
	$smarty->assign("set_s_day", $three_last_month['mday']);
	$smarty->assign("set_e_year", $now_date['year']);
	$smarty->assign("set_e_month", $now_date['mon']);
	$smarty->assign("set_e_day", $now_date['mday']);

	$list_sql = " SELECT * FROM advert_categories WHERE deleted_at is NULL ORDER BY id ASC ";

	if(isset($_POST['mode']) && $_POST['mode'] != ''){
		if($_POST['mode'] == 'new_regist'){
			$smarty->assign("mode", 'insert_commit');
			$smarty->assign("sub_title", '新規追加');

			// ページを表示
			$smarty->display("./advert_category_input.tpl");
			exit();
		}elseif($_POST['mode'] == 'insert_commit'){

			$error_flag = 0;

			if($name == "") {
				$error_message = "カテゴリー名を入力してください。";
				$error_flag = 1;
			}

			if($error_flag == 0) {
				$advert_category_dao->transaction_start();

				$advert_category = new AdvertCategory();
				$advert_category->setName($name);

				//既に登録されたカテゴリー名か確認
				if(is_null($advert_category_dao->getAdvertCategoryByName($name))) {
					//INSERTを実行
					$db_result = $advert_category_dao->insertAdvertCategory($advert_category, $result_message);
					if($db_result) {
						$advert_category_dao->transaction_end();

						$smarty->assign("info_message", $result_message);

						view_list();
					} else {
						$advert_category_dao->transaction_rollback();

						$smarty->assign("error_message", $result_message);

						$form_data = array('id' => $id,
											'name' => $name);

						$smarty->assign("form_data", $form_data);

						$smarty->assign("mode", 'insert_commit');
						$smarty->assign("sub_title", '新規追加');

						// ページを表示
						$smarty->display("./advert_category_input.tpl");
						exit();
					}
				} else {
					$advert_category_dao->transaction_rollback();

					$error_message = "入力されたカテゴリー名は登録されてます。";
					$smarty->assign("error_message", $error_message);

					$form_data = array('id' => $id,
										'name' => $name);

					$smarty->assign("form_data", $form_data);

					$smarty->assign("mode", 'insert_commit');
					$smarty->assign("sub_title", '新規追加');

					// ページを表示
					$smarty->display("./advert_category_input.tpl");
					exit();
				}
			} else {
				$smarty->assign("error_message", $error_message);

				$form_data = array('id' => $id,
									'name' => $name);

				$smarty->assign("form_data", $form_data);

				$smarty->assign("mode", 'insert_commit');
				$smarty->assign("sub_title", '新規追加');

				// ページを表示
				$smarty->display("./advert_category_input.tpl");
				exit();
			}
		}elseif($_POST['mode'] == 'edit'){
			$advert_category = new AdvertCategory;
			$advert_category = $advert_category_dao->getAdvertCategoryById($id);

			if(!is_null($advert_category)) {
				$form_data = array('id' => $advert_category->getId(),
									'name' => $advert_category->getName());

				$smarty->assign("form_data", $form_data);

				$smarty->assign("mode", 'update_commit');
				$smarty->assign("sub_title", '編集');

				// ページを表示
				$smarty->display("./advert_category_input.tpl");
				exit();
			} else {
				$error_message .= "該当するデータはありません。";
				$smarty->assign("error_message", $error_message);

				$smarty->assign("mode", 'insert_commit');
				$smarty->assign("sub_title", '新規追加');

				// ページを表示
				$smarty->display("./advert_category_input.tpl");
				exit();
			}
		}elseif($_POST['mode'] == 'update_commit'){

			$error_flag = 0;

			if($name == "") {
				$error_message = "カテゴリー名を入力してください。";
				$error_flag = 1;
			}

			if($error_flag == 0) {
				$advert_category_dao->transaction_start();

				$advert_category = new AdvertCategory();
				$advert_category->setId($id);
				$advert_category->setName($name);

				//既に登録されたカテゴリー名か確認
				$result_data = $advert_category_dao->getAdvertCategoryByName($name);

				if(is_null($result_data) || $id == $result_data->getId()) {
					//UPDATEを実行
					$db_result = $advert_category_dao->updateAdvertCategory($advert_category, $result_message);
					if($db_result) {
						$advert_category_dao->transaction_end();

						$smarty->assign("info_message", $result_message);

						view_list();
					} else {
						$advert_category_dao->transaction_rollback();

						$smarty->assign("error_message", $result_message);

						$form_data = array('id' => $id,
											'name' => $name);

						$smarty->assign("form_data", $form_data);

						$smarty->assign("mode", 'update_commit');
						$smarty->assign("sub_title", '編集');

						// ページを表示
						$smarty->display("./advert_category_input.tpl");
						exit();
					}
				} else {
					$advert_category_dao->transaction_rollback();

					$error_message = "入力されたカテゴリー名は登録されてます";
					$smarty->assign("error_message", $error_message);

					$form_data = array('id' => $id,
										'name' => $name);

					$smarty->assign("form_data", $form_data);

					$smarty->assign("mode", 'update_commit');
					$smarty->assign("sub_title", '編集');

					// ページを表示
					$smarty->display("./advert_category_input.tpl");
					exit();
				}
			} else {
				$smarty->assign("error_message", $error_message);

				$form_data = array('id' => $id,
									'name' => $name);

				$smarty->assign("form_data", $form_data);

				$smarty->assign("mode", 'update_commit');
				$smarty->assign("sub_title", '編集');

				// ページを表示
				$smarty->display("./advert_category_input.tpl");
				exit();
			}
		}elseif($_POST['mode'] == 'search'){

			$search_list_sql = " SELECT * FROM advert_categories "
							. " WHERE deleted_at is NULL ";

			//絞込みWHERE文格納配列
			$add_where = array();

			//「媒体カテゴリーID」「媒体カテゴリー名」絞込み
			switch($method) {
				case 1:	//「完全一致」
					if($category_id != "") {
						$add_where[] = "id = '$category_id' ";
					}
					if($category_name != "") {
						$add_where[] = "name = '$category_name' ";
					}
					break;
				case 2:	//「前方一致」
					if($category_id != "") {
						$add_where[] = "id LIKE '$category_id%' ";
					}
					if($category_name != "") {
						$add_where[] = "name LIKE '$category_name%' ";
					}
					break;
				case 3:	//「後方一致」
					if($category_id != "") {
						$add_where[] = "id LIKE '%$category_id' ";
					}
					if($category_name != "") {
						$add_where[] = "name LIKE '%$category_name' ";
					}
					break;
				case 4:	//「あいまい」
					if($category_id != "") {
						$add_where[] = "id LIKE '%$category_id%' ";
					}
					if($category_name != "") {
						$add_where[] = "name LIKE '%$category_name%' ";
					}
					break;
			}

			if(count($add_where) > 0) {
				if($reverse == 1) {
					$search_list_sql .= "AND NOT(".implode("OR ", $add_where).") ";
				} else {
					$search_list_sql .= "AND ".implode("AND ", $add_where);
				}
			}

			if($created_at_flag == 1) {
				if($reverse == 1) {
					$search_list_sql .= "AND NOT(created_at BETWEEN '$s_year-$s_month-$s_day 00:00:00' AND '$e_year-$e_month-$e_day 23:59:59') ";
				} else {
					$search_list_sql .= "AND created_at BETWEEN '$s_year-$s_month-$s_day 00:00:00' AND '$e_year-$e_month-$e_day 23:59:59' ";
				}

				$smarty->assign("set_s_year", $s_year);
				$smarty->assign("set_s_month", $s_month);
				$smarty->assign("set_s_day", $s_day);
				$smarty->assign("set_e_year", $e_year);
				$smarty->assign("set_e_month", $e_month);
				$smarty->assign("set_e_day", $e_day);
			}

			$search_list_sql .= "ORDER BY $sort $order ";

			$db_result = $common_dao->db_query($search_list_sql);
			if($db_result){
				$smarty->assign("list", $db_result);
				$list_count = count($db_result);
			}else{
				$error_message .= "該当するデータはありません。";
			}
			$smarty->assign("list_count", $list_count);
			$smarty->assign("error_message", $error_message);

			$search['method'] = $method;
			$search['reverse'] = $reverse;
			$search['sort'] = $sort;
			$search['order'] = $order;
			$search['id'] = $category_id;
			$search['name'] = $category_name;
			$search['created_at_flag'] = $created_at_flag;

			$smarty->assign("search", $search);

			$smarty->assign("mode", 'search');
			$smarty->assign("sub_title", '検索結果');

			// ページを表示
			$smarty->display("./advert_category_list.tpl");
			exit();
		}elseif($_POST['mode'] == 'delete'){
			$advert_category_dao->transaction_start();

			if(!is_null($advert_category_dao->getAdvertCategoryById($id))) {
				$db_result = $advert_category_dao->deleteAdvertCategory($id, $result_message);
				if($db_result){
					$advert_category_dao->transaction_end();

					$smarty->assign("info_message", $result_message);

					view_list();
				}else{
					$advert_category_dao->transaction_rollback();

					$smarty->assign("error_message", $result_message);

					view_list();
				}
			}else{
				$advert_category_dao->transaction_rollback();

				$error_message = "ＤＢの更新に失敗しました。";
				$smarty->assign("error_message", $error_message);

				view_list();
			}
		}else{
			view_list();
		}
	}else{
		view_list();
	}
}else{
	header('Location: ./login.php?error=1');
	exit();
}

function view_list(){
	global $common_dao, $smarty, $list_sql, $error_message;

	$list_count = 0;

	$db_result = $common_dao->db_query($list_sql);
	if($db_result){
		$smarty->assign("list", $db_result);
		$list_count = count($db_result);
	}else{
		$error_message .= "ＤＢからのデータの取得に失敗しました。(su0000)";
	}
	$smarty->assign("list_count", $list_count);
	$smarty->assign("error_message", $error_message);

	// ページを表示
	$smarty->display("./advert_category_list.tpl");
	exit();
}

function do_escape_quotes($str){
	//magic_quotesが有効ならクウォート部分を除去
	if(get_magic_quotes_gpc()){
		$str = stripslashes($str);
	}
	return $str;
}
?>