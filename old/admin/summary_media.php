<?php
// 共通設定
require_once( '../common/CommonAdminBase.php' );
require_once( '../common/CommonDao.php' );
require_once( '../dto/LoginUser.php' );

session_start();

if(isset($_SESSION['logon_token']) && $_SESSION['logon_token'] != ''){
	$login_user = new LoginUser();
	$login_user = $_SESSION['login_user'];

	// Smartyオブジェクト取得
	$smarty =& getSmartyObj();

	$smarty->assign("title", "Test Top Page");
	$smarty->assign("login_user", $login_user );

	$common_dao = new CommonDao();

	$list_sql = " SELECT al.*, m.media_name, mp.publisher_name "
				. " FROM action_logs as al "
				. " LEFT JOIN media as m on al.media_id = m.id "
				. " LEFT JOIN media_publishers as mp on al.media_publisher_id = mp.id "
				. " WHERE al.deleted_at is NULL "
				. " AND (al.status = 1 OR al.status = 2) "
				. " ORDER BY al.media_publisher_id ASC, al.media_id ASC ";

	$list_count = 0;

	$db_result = $common_dao->db_query($list_sql);
	if($db_result){

		foreach($db_result as $row) {
			$m_id = $row['media_id'];

			$summary[$m_id]['media_id'] = $row['media_id'];
			$summary[$m_id]['media_name'] = $row['media_name'];
			$summary[$m_id]['media_publisher_id'] = $row['media_publisher_id'];
			$summary[$m_id]['publisher_name'] = $row['publisher_name'];

			$summary[$m_id]['click_count'] += 1;
			$summary[$m_id]['total_price'] += $row['click_price_client'];

			if($row['status'] == 2) {
				$summary[$m_id]['action_count'] += 1;
				$summary[$m_id]['total_price'] += $row['action_price_client'];
			}
		}

		$smarty->assign("list", $summary);
		$list_count = count($summary);
	}else{
		$error_message .= "ＤＢからのデータの取得に失敗しました。(su0000)";
	}
	$smarty->assign("list_count", $list_count);
	$smarty->assign("error_message", $error_message);

	// ページを表示
	$smarty->display("./summary_media.tpl");
	exit();
}else{
	header('Location: ./login.php?error=1');
	exit();
}
?>