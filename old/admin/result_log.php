<?php
// 共通設定
require_once( '../common/CommonAdminBase.php' );
require_once( '../common/CommonDao.php' );
require_once( '../dto/LoginUser.php' );

session_start();

if(isset($_SESSION['logon_token']) && $_SESSION['logon_token'] != ''){
	$login_user = new LoginUser();
	$login_user = $_SESSION['login_user'];

	// Smartyオブジェクト取得
	$smarty =& getSmartyObj();

	$smarty->assign("title", "Test Top Page");
	$smarty->assign("login_user", $login_user );

	$common_dao = new CommonDao();

	$s_year = do_escape_quotes($_POST['s_year']);
	$s_month = do_escape_quotes($_POST['s_month']);
	$log_type = do_escape_quotes($_POST['log_type']);

	$download_date = do_escape_quotes($_POST['download_date']);

	$now_date = getdate();
	$smarty->assign("set_s_year", $now_date['year']);
	$smarty->assign("set_s_month", $now_date['mon']);

	if(isset($_POST['mode']) && $_POST['mode'] != '') {
		if($_POST['mode'] == 'search') {
			$search_date = $s_year.sprintf("%02d",$s_month);

			if($log_type == 1) {

				$list_sql = " SELECT DATE_FORMAT(created_at,'%Y%m%d') as download_date, "
							. " DATE_FORMAT(created_at,'%Y年%m月%d日') as log_date, "
							. " count(DATE_FORMAT(created_at,'%Y%m%d')) as log_count "
							. " FROM action_logs "
							. " WHERE deleted_at is NULL "
							. " AND DATE_FORMAT(created_at,'%Y%m') = '$search_date' "
							. " AND (status = 1 OR status = 2) "
							. " GROUP BY DATE_FORMAT(created_at,'%Y%m%d') "
							. " ORDER BY created_at ASC ";

			} elseif($log_type == 2) {

				$list_sql = " SELECT DATE_FORMAT(action_complete_date,'%Y%m%d') as download_date, "
							. " DATE_FORMAT(action_complete_date,'%Y年%m月%d日') as log_date, "
							. " count(DATE_FORMAT(action_complete_date,'%Y%m%d')) as log_count "
							. " FROM action_logs "
							. " WHERE deleted_at is NULL "
							. " AND DATE_FORMAT(action_complete_date,'%Y%m') = '$search_date' "
							. " AND (status = 1 OR status = 2) "
							. " GROUP BY DATE_FORMAT(action_complete_date,'%Y%m%d') "
							. " ORDER BY action_complete_date ASC ";

			} elseif($log_type == 3) {

				$list_sql = " SELECT DATE_FORMAT(created_at,'%Y%m%d') as download_date, "
							. " DATE_FORMAT(created_at,'%Y年%m月%d日') as log_date, "
							. " count(DATE_FORMAT(created_at,'%Y%m%d')) as log_count "
							. " FROM point_back_logs "
							. " WHERE deleted_at is NULL "
							. " AND DATE_FORMAT(created_at,'%Y%m') = '$search_date' "
							. " GROUP BY DATE_FORMAT(created_at,'%Y%m%d') "
							. " ORDER BY created_at ASC";

			}

			$db_result = $common_dao->db_query($list_sql);
			if($db_result){
				$smarty->assign("list", $db_result);
				$list_count = count($db_result);
			}else{
				$error_message .= "該当するデータはありません。";
			}

			$smarty->assign("error_message", $error_message);

			$smarty->assign("set_s_year", $s_year);
			$smarty->assign("set_s_month", $s_month);

			$smarty->assign("search_date", $search_date);
			$smarty->assign("log_type", $log_type);

			// ページを表示
			$smarty->display("./result_log.tpl");
			exit();
		} elseif($_POST['mode'] == 'detail') {
			if($log_type == 1) {

				$download_sql = " SELECT * "
								. " FROM action_logs "
								. " WHERE deleted_at is NULL "
								. " AND DATE_FORMAT(created_at,'%Y%m%d') = '$download_date' "
								. " AND (status = 1 OR status = 2) "
								. " ORDER BY created_at ASC ";

			} elseif($log_type == 2) {

				$download_sql = " SELECT * "
								. " FROM action_logs "
								. " WHERE deleted_at is NULL "
								. " AND DATE_FORMAT(action_complete_date,'%Y%m%d') = '$download_date' "
								. " AND (status = 1 OR status = 2) "
								. " ORDER BY action_complete_date ASC ";

			} elseif($log_type == 3) {

				$download_sql = " SELECT * "
								. " FROM point_back_logs "
								. " WHERE deleted_at is NULL "
								. " AND DATE_FORMAT(created_at,'%Y%m%d') = '$download_date' "
								. " ORDER BY created_at ASC ";

			}

			$db_result = $common_dao->db_query($download_sql);
			if($db_result){
				$smarty->assign("list", $db_result);
				$list_count = count($db_result);
			}else{
				$error_message .= "ＤＢからのデータの取得に失敗しました。(su0001)";
			}

			$smarty->assign("error_message", $error_message);

			$smarty->assign("download_date", $download_date);
			$smarty->assign("log_type", $log_type);

			if($log_type == 1) {
				// ページを表示
				$smarty->display("./result_log_click.tpl");
				exit();
			} elseif($log_type == 2) {
				// ページを表示
				$smarty->display("./result_log_action.tpl");
				exit();
			} elseif($log_type == 3) {
				// ページを表示
				$smarty->display("./result_log_point_back.tpl");
				exit();
			}
		}
	} else {
		// ページを表示
		$smarty->display("./result_log.tpl");
		exit();
	}
}else{
	header('Location: ./login.php?error=1');
	exit();
}

function do_escape_quotes($str){
	//magic_quotesが有効ならクウォート部分を除去
	if(get_magic_quotes_gpc()){
		$str = stripslashes($str);
	}
	return $str;
}
?>