<?php
// 共通設定
require_once( '../common/CommonAdminBase.php' );
require_once( '../common/CommonDao.php' );
require_once( '../dto/LoginUser.php' );
require_once( '../dao/MediaDao.php' );
require_once( '../dto/Media.php' );
require_once( '../dao/MediaPublisherDao.php' );
require_once( '../dto/MediaPublisher.php' );
require_once( '../dao/MediaGroupDao.php' );
require_once( '../dto/MediaGroup.php' );
require_once( '../dao/MediaCategoryDao.php' );
require_once( '../dto/MediaCategory.php' );

session_start();

if(isset($_SESSION['logon_token']) && $_SESSION['logon_token'] != ''){
	$login_user = new LoginUser();
	$login_user = $_SESSION['login_user'];

	// Smartyオブジェクト取得
	$smarty =& getSmartyObj();

	$smarty->assign("title", "Test Top Page");
	$smarty->assign("login_user", $login_user );

	$common_dao = new CommonDao();
	$media_dao = new MediaDao();

	//媒体発行者
	$media_publisher_dao = new MediaPublisherDao();
	$media_publisher_array = array();
	foreach($media_publisher_dao->getAllMediaPublisher() as $val){
		$row_array = array('id' => $val->getId(), 'name' => $val->getPublisherName());
		$media_publisher_array[$val->getId()] = $row_array;
	}
	$smarty->assign("media_publisher_array", $media_publisher_array);

	//媒体カテゴリー
	$media_category_dao = new MediaCategoryDao();
	$media_category_array = array();
	foreach($media_category_dao->getAllMediaCategory() as $val){
		$row_array = array('id' => $val->getId(), 'name' => $val->getName());
		$media_category_array[$val->getId()] = $row_array;
	}
	$smarty->assign("media_category_array", $media_category_array);

	if(isset($_GET['mode'])) {
		$mode = $_GET['mode'];
	} elseif(isset($_POST['mode'])) {
		$mode = $_POST['mode'];
	}

	if(isset($_GET['id'])) {
		$id = $_GET['id'];
	} else {
		$id = do_escape_quotes($_POST['id']);
	}

	$media_publisher_id = do_escape_quotes($_POST['media_publisher_id']);
	$media_category_id = do_escape_quotes($_POST['media_category_id']);
	$media_name = do_escape_quotes($_POST['media_name']);
	$support_docomo = do_escape_quotes($_POST['support_docomo']);
	$support_softbank = do_escape_quotes($_POST['support_softbank']);
	$support_au = do_escape_quotes($_POST['support_au']);
	$support_pc = do_escape_quotes($_POST['support_pc']);
	$site_url_docomo = do_escape_quotes($_POST['site_url_docomo']);
	$site_url_softbank = do_escape_quotes($_POST['site_url_softbank']);
	$site_url_au = do_escape_quotes($_POST['site_url_au']);
	$site_url_pc = do_escape_quotes($_POST['site_url_pc']);
	$media_type = do_escape_quotes($_POST['media_type']);
	$page_view_day = do_escape_quotes($_POST['page_view_day']);
	$site_outline = do_escape_quotes($_POST['site_outline']);
	$point_back_url = do_escape_quotes($_POST['point_back_url']);
	$point_test_url = do_escape_quotes($_POST['point_test_url']);
	$test_flag = do_escape_quotes($_POST['test_flag']);
	$status = do_escape_quotes($_POST['status']);

	$method = do_escape_quotes($_POST['method']);
	$reverse = do_escape_quotes($_POST['reverse']);
	$sort = do_escape_quotes($_POST['sort']);
	$order = do_escape_quotes($_POST['order']);
	$search_id = do_escape_quotes($_POST['search_id']);
	$search_name = do_escape_quotes($_POST['search_name']);
	$created_at_flag = do_escape_quotes($_POST['created_at_flag']);

	$s_year = do_escape_quotes($_POST['s_year']);
	$s_month = do_escape_quotes($_POST['s_month']);
	$s_day = do_escape_quotes($_POST['s_day']);
	$e_year = do_escape_quotes($_POST['e_year']);
	$e_month = do_escape_quotes($_POST['e_month']);
	$e_day = do_escape_quotes($_POST['e_day']);

	$three_last_month = getdate(strtotime("-3 month"));
	$now_date = getdate();

	$smarty->assign("set_s_year", $three_last_month['year']);
	$smarty->assign("set_s_month", $three_last_month['mon']);
	$smarty->assign("set_s_day", $three_last_month['mday']);
	$smarty->assign("set_e_year", $now_date['year']);
	$smarty->assign("set_e_month", $now_date['mon']);
	$smarty->assign("set_e_day", $now_date['mday']);

	$list_sql = " SELECT m.*, mp.publisher_name as publisher_name "
				. " FROM media as m "
				. " left join media_publishers as mp on m.media_publisher_id = mp.id "
				. " WHERE m.deleted_at is NULL "
				. " ORDER BY m.id ASC ";

	if($mode != ''){
		if($mode == 'new_regist'){
			$smarty->assign("mode", 'insert_commit');
			$smarty->assign("sub_title", '新規追加');

			// ページを表示
			$smarty->display("./media_input.tpl");
			exit();
		}elseif($mode == 'insert_commit'){

			$error_flag = 0;

			if($media_name == "") {
				$error_message = "サイト名を入力してください。";
				$error_flag = 1;
			}

			if($error_flag == 0) {
				$media_dao->transaction_start();

				$media = new Media();
				$media->setMediaPublisherId($media_publisher_id);
				$media->setMediaCategoryId($media_category_id);
				$media->setMediaName($media_name);
				$media->setSupportDocomo($support_docomo);
				$media->setSupportSoftbank($support_softbank);
				$media->setSupportAu($support_au);
				$media->setSupportPc($support_pc);
				$media->setSiteUrlDocomo($site_url_docomo);
				$media->setSiteUrlSoftbank($site_url_softbank);
				$media->setSiteUrlAu($site_url_au);
				$media->setSiteUrlPc($site_url_pc);
				$media->setMediaType($media_type);
				$media->setPageViewDay($page_view_day);
				$media->setSiteOutline($site_outline);
				$media->setPointBackUrl($point_back_url);
				$media->setPointTestUrl($point_test_url);
				$media->setTestFlag($test_flag);
				$media->setStatus($status);

				//INSERTを実行
				$db_result = $media_dao->insertMedia($media, $result_message);
				if($db_result) {
					$media_dao->transaction_end();

					$smarty->assign("info_message", $result_message);

					view_list();
				} else {
					$media_dao->transaction_rollback();

					$smarty->assign("error_message", $result_message);

					$form_data = array('id' => $id,
										'media_publisher_id' => $media_publisher_id,
										'media_category_id' => $media_category_id,
										'media_name' => $media_name,
										'support_docomo' => $support_docomo,
										'support_softbank' => $support_softbank,
										'support_au' => $support_au,
										'support_pc' => $support_pc,
										'site_url_docomo' => $site_url_docomo,
										'site_url_softbank' => $site_url_softbank,
										'site_url_au' => $site_url_au,
										'site_url_pc' => $site_url_pc,
										'media_type' => $media_type,
										'page_view_day' => $page_view_day,
										'site_outline' => $site_outline,
										'point_back_url' => $point_back_url,
										'point_test_url' => $point_test_url,
										'test_flag' => $test_flag,
										'status' => $status);

					$smarty->assign("form_data", $form_data);

					$smarty->assign("mode", 'insert_commit');
					$smarty->assign("sub_title", '新規追加');

					// ページを表示
					$smarty->display("./media_input.tpl");
					exit();
				}
			} else {
				$smarty->assign("error_message", $error_message);

				$form_data = array('id' => $id,
									'media_publisher_id' => $media_publisher_id,
									'media_category_id' => $media_category_id,
									'media_name' => $media_name,
									'support_docomo' => $support_docomo,
									'support_softbank' => $support_softbank,
									'support_au' => $support_au,
									'support_pc' => $support_pc,
									'site_url_docomo' => $site_url_docomo,
									'site_url_softbank' => $site_url_softbank,
									'site_url_au' => $site_url_au,
									'site_url_pc' => $site_url_pc,
									'media_type' => $media_type,
									'page_view_day' => $page_view_day,
									'site_outline' => $site_outline,
									'point_back_url' => $point_back_url,
									'point_test_url' => $point_test_url,
									'test_flag' => $test_flag,
									'status' => $status);

				$smarty->assign("form_data", $form_data);

				$smarty->assign("mode", 'insert_commit');
				$smarty->assign("sub_title", '新規追加');

				// ページを表示
				$smarty->display("./media_input.tpl");
				exit();
			}
		}elseif($mode == 'edit'){
			$media = new Media;
			$media = $media_dao->getMediaById($id);

			if(!is_null($media)) {
				$form_data = array('id' => $media->getId(),
									'media_publisher_id' => $media->getMediaPublisherId(),
									'media_category_id' => $media->getMediaCategoryId(),
									'media_name' => $media->getMediaName(),
									'support_docomo' => $media->getSupportDocomo(),
									'support_softbank' => $media->getSupportSoftbank(),
									'support_au' => $media->getSupportAu(),
									'support_pc' => $media->getSupportPc(),
									'site_url_docomo' => $media->getSiteUrlDocomo(),
									'site_url_softbank' => $media->getSiteUrlSoftbank(),
									'site_url_au' => $media->getSiteUrlAu(),
									'site_url_pc' => $media->getSiteUrlPc(),
									'media_type' => $media->getMediaType(),
									'page_view_day' => $media->getPageViewDay(),
									'site_outline' => $media->getSiteOutline(),
									'point_back_url' => $media->getPointBackUrl(),
									'point_test_url' => $media->getPointTestUrl(),
									'test_flag' => $media->getTestFlag(),
									'status' => $media->getStatus());

				$smarty->assign("form_data", $form_data);

				$smarty->assign("mode", 'update_commit');
				$smarty->assign("sub_title", '編集');

				// ページを表示
				$smarty->display("./media_input.tpl");
				exit();
			} else {
				$error_message .= "該当するデータはありません。";
				$smarty->assign("error_message", $error_message);

				$smarty->assign("mode", 'insert_commit');
				$smarty->assign("sub_title", '新規追加');

				// ページを表示
				$smarty->display("./media_input.tpl");
				exit();
			}
		}elseif($mode == 'update_commit'){

			$error_flag = 0;

			if($media_name == "") {
				$error_message = "サイト名を入力してください。";
				$error_flag = 1;
			}

			if($error_flag == 0) {
				$media_dao->transaction_start();

				$media = new Media();
				$media->setId($id);
				$media->setMediaPublisherId($media_publisher_id);
				$media->setMediaCategoryId($media_category_id);
				$media->setMediaName($media_name);
				$media->setSupportDocomo($support_docomo);
				$media->setSupportSoftbank($support_softbank);
				$media->setSupportAu($support_au);
				$media->setSupportPc($support_pc);
				$media->setSiteUrlDocomo($site_url_docomo);
				$media->setSiteUrlSoftbank($site_url_softbank);
				$media->setSiteUrlAu($site_url_au);
				$media->setSiteUrlPc($site_url_pc);
				$media->setMediaType($media_type);
				$media->setPageViewDay($page_view_day);
				$media->setSiteOutline($site_outline);
				$media->setPointBackUrl($point_back_url);
				$media->setPointTestUrl($point_test_url);
				$media->setTestFlag($test_flag);
				$media->setStatus($status);

				//UPDATEを実行
				$db_result = $media_dao->updateMedia($media, $result_message);
				if($db_result) {
					$media_dao->transaction_end();

					$smarty->assign("info_message", $result_message);

					view_list();
				} else {
					$media_dao->transaction_rollback();

					$smarty->assign("error_message", $result_message);

					$form_data = array('id' => $id,
										'media_publisher_id' => $media_publisher_id,
										'media_category_id' => $media_category_id,
										'media_name' => $media_name,
										'support_docomo' => $support_docomo,
										'support_softbank' => $support_softbank,
										'support_au' => $support_au,
										'support_pc' => $support_pc,
										'site_url_docomo' => $site_url_docomo,
										'site_url_softbank' => $site_url_softbank,
										'site_url_au' => $site_url_au,
										'site_url_pc' => $site_url_pc,
										'media_type' => $media_type,
										'page_view_day' => $page_view_day,
										'site_outline' => $site_outline,
										'point_back_url' => $point_back_url,
										'point_test_url' => $point_test_url,
										'test_flag' => $test_flag,
										'status' => $status);

					$smarty->assign("form_data", $form_data);

					$smarty->assign("mode", 'update_commit');
					$smarty->assign("sub_title", '編集');

					// ページを表示
					$smarty->display("./media_input.tpl");
					exit();
				}
			} else {
				$smarty->assign("error_message", $error_message);

				$form_data = array('id' => $id,
									'media_publisher_id' => $media_publisher_id,
									'media_category_id' => $media_category_id,
									'media_name' => $media_name,
									'support_docomo' => $support_docomo,
									'support_softbank' => $support_softbank,
									'support_au' => $support_au,
									'support_pc' => $support_pc,
									'site_url_docomo' => $site_url_docomo,
									'site_url_softbank' => $site_url_softbank,
									'site_url_au' => $site_url_au,
									'site_url_pc' => $site_url_pc,
									'media_type' => $media_type,
									'page_view_day' => $page_view_day,
									'site_outline' => $site_outline,
									'point_back_url' => $point_back_url,
									'point_test_url' => $point_test_url,
									'test_flag' => $test_flag,
									'status' => $status);

				$smarty->assign("form_data", $form_data);

				$smarty->assign("mode", 'update_commit');
				$smarty->assign("sub_title", '編集');

				// ページを表示
				$smarty->display("./media_input.tpl");
				exit();
			}
		}elseif($mode == 'search'){

			$search_list_sql = " SELECT m.*, mp.publisher_name as publisher_name "
								. " FROM media as m "
								. " left join media_publishers as mp on m.media_publisher_id = mp.id "
								. " WHERE m.deleted_at is NULL ";

			//絞込みWHERE文格納配列
			$add_where = array();

			switch($method) {
				case 1:	//「完全一致」
					if($search_id != "") {
						$add_where[] = "m.id = '$search_id' ";
					}
					if($search_name != "") {
						$add_where[] = "m.media_name = '$search_name' ";
					}
					break;
				case 2:	//「前方一致」
					if($search_id != "") {
						$add_where[] = "m.id LIKE '$search_id%' ";
					}
					if($search_name != "") {
						$add_where[] = "m.media_name LIKE '$search_name%' ";
					}
					break;
				case 3:	//「後方一致」
					if($search_id != "") {
						$add_where[] = "m.id LIKE '%$search_id' ";
					}
					if($search_name != "") {
						$add_where[] = "m.media_name LIKE '%$search_name' ";
					}
					break;
				case 4:	//「あいまい」
					if($search_id != "") {
						$add_where[] = "m.id LIKE '%$search_id%' ";
					}
					if($search_name != "") {
						$add_where[] = "m.media_name LIKE '%$search_name%' ";
					}
					break;
			}

			if(count($add_where) > 0) {
				if($reverse == 1) {
					$search_list_sql .= "AND NOT(".implode("OR ", $add_where).") ";
				} else {
					$search_list_sql .= "AND ".implode("AND ", $add_where);
				}
			}

			if($created_at_flag == 1) {
				if($reverse == 1) {
					$search_list_sql .= "AND NOT(m.created_at BETWEEN '$s_year-$s_month-$s_day 00:00:00' AND '$e_year-$e_month-$e_day 23:59:59') ";
				} else {
					$search_list_sql .= "AND m.created_at BETWEEN '$s_year-$s_month-$s_day 00:00:00' AND '$e_year-$e_month-$e_day 23:59:59' ";
				}

				$smarty->assign("set_s_year", $s_year);
				$smarty->assign("set_s_month", $s_month);
				$smarty->assign("set_s_day", $s_day);
				$smarty->assign("set_e_year", $e_year);
				$smarty->assign("set_e_month", $e_month);
				$smarty->assign("set_e_day", $e_day);
			}

			$search_list_sql .= "ORDER BY m.$sort $order ";

			$db_result = $common_dao->db_query($search_list_sql);
			if($db_result){
				$smarty->assign("list", $db_result);
				$list_count = count($db_result);
			}else{
				$error_message .= "該当するデータはありません。";
			}
			$smarty->assign("list_count", $list_count);
			$smarty->assign("error_message", $error_message);

			$search['method'] = $method;
			$search['reverse'] = $reverse;
			$search['sort'] = $sort;
			$search['order'] = $order;
			$search['id'] = $search_id;
			$search['name'] = $search_name;
			$search['created_at_flag'] = $created_at_flag;

			$smarty->assign("search", $search);

			$smarty->assign("mode", 'search');
			$smarty->assign("sub_title", '検索結果');

			// ページを表示
			$smarty->display("./media_list.tpl");
			exit();
		}elseif($mode == 'delete'){
			$media_dao->transaction_start();

			if(!is_null($media_dao->getMediaById($id))) {
				$db_result = $media_dao->deleteMedia($id, $result_message);
				if($db_result){
					$media_dao->transaction_end();

					$smarty->assign("info_message", $result_message);

					view_list();
				}else{
					$media_dao->transaction_rollback();

					$smarty->assign("error_message", $result_message);

					view_list();
				}
			}else{
				$media_dao->transaction_rollback();

				$error_message = "ＤＢの更新に失敗しました。";
				$smarty->assign("error_message", $error_message);

				view_list();
			}
		}else{
			view_list();
		}
	}else{
		view_list();
	}
}else{
	header('Location: ./login.php?error=1');
	exit();
}

function view_list(){
	global $common_dao, $smarty, $list_sql, $error_message;

	$list_count = 0;

	$db_result = $common_dao->db_query($list_sql);
	if($db_result){
		$smarty->assign("list", $db_result);
		$list_count = count($db_result);
	}else{
		$error_message .= "ＤＢからのデータの取得に失敗しました。(su0000)";
	}
	$smarty->assign("list_count", $list_count);
	$smarty->assign("error_message", $error_message);

	// ページを表示
	$smarty->display("./media_list.tpl");
	exit();
}

function do_escape_quotes($str){
	//magic_quotesが有効ならクウォート部分を除去
	if(get_magic_quotes_gpc()){
		$str = stripslashes($str);
	}
	return $str;
}
?>