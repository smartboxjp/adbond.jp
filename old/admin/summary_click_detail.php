<?php
// 共通設定
require_once( '../common/CommonAdminBase.php' );
require_once( '../common/CommonDao.php' );
require_once( '../dto/LoginUser.php' );

session_start();

if(isset($_SESSION['logon_token']) && $_SESSION['logon_token'] != ''){
	$login_user = new LoginUser();
	$login_user = $_SESSION['login_user'];

	// Smartyオブジェクト取得
	$smarty =& getSmartyObj();

	$smarty->assign("title", "Test Top Page");
	$smarty->assign("login_user", $login_user );

	$common_dao = new CommonDao();

	$list_sql = " SELECT al.*, m.media_name, a.advert_name "
				. " FROM action_logs as al "
				. " LEFT JOIN media as m on al.media_id = m.id "
				. " LEFT JOIN advert as a on al.advert_id = a.id "
				. " WHERE al.deleted_at is NULL "
				. " AND (al.status = 1 OR al.status = 2) ";

	if(isset($_GET['m_id']) && $_GET['m_id'] != "") {
		$m_id = $_GET['m_id'];
		$list_sql .= "AND al.media_id = $m_id ";
	}

	if(isset($_GET['mp_id']) && $_GET['mp_id'] != "") {
		$mp_id = $_GET['mp_id'];
		$list_sql .= "AND al.media_publisher_id = $mp_id ";
	}

	if(isset($_GET['a_id']) && $_GET['a_id'] != "") {
		$a_id = $_GET['a_id'];
		$list_sql .= "AND al.advert_id = $a_id ";
	}

	if(isset($_GET['ac_id']) && $_GET['ac_id'] != "") {
		$ac_id = $_GET['ac_id'];
		$list_sql .= "AND al.advert_client_id = $ac_id ";
	}

	$list_sql .= " ORDER BY al.created_at ASC ";

	$list_count = 0;

	$db_result = $common_dao->db_query($list_sql);
	if($db_result){

		foreach($db_result as $key => $row) {
			if($row['carrier_id'] == 1) {
				$db_result[$key]['carrier'] = "docomo";
			} elseif($row['carrier_id'] == 2) {
				$db_result[$key]['carrier'] = "softbank";
			} elseif($row['carrier_id'] == 3) {
				$db_result[$key]['carrier'] = "au";
			} else {
				$db_result[$key]['carrier'] = "pc";
			}

			if($row['status'] == 1) {
				$db_result[$key]['status_show'] = "クリック";
			} elseif($row['status'] == 2) {
				$db_result[$key]['status_show'] = "登録";
			}
		}

		$smarty->assign("list", $db_result);
		$list_count = count($db_result);
	}else{
		$error_message .= "ＤＢからのデータの取得に失敗しました。(su0000)";
	}
	$smarty->assign("list_count", $list_count);
	$smarty->assign("error_message", $error_message);

	// ページを表示
	$smarty->display("./summary_click_detail.tpl");
	exit();
}else{
	header('Location: ./login.php?error=1');
	exit();
}
?>