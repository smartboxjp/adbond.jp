<?php

if(isset($_GET['id'])) {
	$id = $_GET['id'];
} elseif(isset($_POST['id'])) {
	$id = $_POST['id'];
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>ポイントバックテスト</title>
</head>
<body>

<div id="container">
<p><?= $id ?></p>
</div>

</body>
</html>