<?php
//
//   共通画像関連処理 CommonImageFile.php
//   DATA:2010/02/13 Y.Sakamaki
//
//-------------------------------------------

// 共通設定
//require_once( '../common/CommonDao.php' );

function add_image($image_file, $image_path, $name, $asp_id, $site_id, $common_dao){

	$img_name = $image_file["name"];
	$img_size = $image_file["size"];
	$img_type = $image_file["type"];
	$img_tmp = $image_file["tmp_name"];

	//ファイルサイズチェック
	if($up_file_size <= 4000000){
		//$info_message = "名前は： $img_name <br>サイズは： $img_size <br>"
		//			. " MIMEタイプは： $img_type <br>一時的に保存されているパスは： $img_tmp <br>";
	}else{
		$error_message = "ファイルサイズが大きすぎます。(4000KBまで）";
	}

	$image_size = getimagesize($img_tmp);

	$image_tag = "<img src=\"$image_path$img_name\" />";
	$image_tag = $common_dao->db_string_escape($image_tag);

	switch($image_size[2]){
		case 1:
			$file_type = "gif";
			break;
		case 2:
			$file_type = "jpg";
			break;
		case 3:
			$file_type = "png";
			break;
		default:
			unlink($up_file);
			return "対応画像ファイルではありません。";
			break;
	}

	$send_path = $image_path . $img_name;
	if(move_uploaded_file($img_tmp, $send_path)){
		chmod($send_path,0666);

		$insert_sql = "insert into image_files values( "
					. " '', '$asp_id', '$site_id', "
					. " '$name', '$img_name', '$file_type', "
					. " '$image_tag', Now(), NULL )";

		$db_insert_result = $common_dao->db_update($insert_sql);
		if($db_insert_result){
			return 1;

			$smarty->assign("info_message", $info_message);
		}else{
			unlink($send_path);

			return "画像ファイル情報のＤＢ保存に失敗しました。";
		}
	}else{
		return "画像ファイルの保存に失敗しました。";
	}
}

function add_image_ts($image_file, $image_path, $name, $asp_id, $site_id, $common_dao){

	$img_name = $image_file["name"];
	$img_size = $image_file["size"];
	$img_type = $image_file["type"];
	$img_tmp = $image_file["tmp_name"];

	//ファイルサイズチェック
	if($up_file_size <= 4000000){
		//$info_message = "名前は： $img_name <br>サイズは： $img_size <br>"
		//			. " MIMEタイプは： $img_type <br>一時的に保存されているパスは： $img_tmp <br>";
	}else{
		$error_message = "ファイルサイズが大きすぎます。(4000KBまで）";
	}

	$image_size = getimagesize($img_tmp);

	$image_tag = "<img src=\"$image_path$img_name\" />";
	$image_tag = $common_dao->db_string_escape($image_tag);

	switch($image_size[2]){
		case 1:
			$file_type = "gif";
			break;
		case 2:
			$file_type = "jpg";
			break;
		case 3:
			$file_type = "png";
			break;
		default:
			unlink($up_file);
			return "対応画像ファイルではありません。";
			break;
	}

	$send_path = $image_path . $img_name;
	if(move_uploaded_file($img_tmp, $send_path)){
		chmod($send_path,0666);

		$insert_sql = "insert into image_files values( "
					. " '', '$asp_id', '$site_id', "
					. " '$name', '$img_name', '$file_type', "
					. " '$image_tag', Now(), NULL )";

		$db_insert_result = $common_dao->db_update_ts($insert_sql);
		if($db_insert_result){
			return 1;

			$smarty->assign("info_message", $info_message);
		}else{
			unlink($send_path);

			return "画像ファイル情報のＤＢ保存に失敗しました。";
		}
	}else{
		return "画像ファイルの保存に失敗しました。";
	}
}

function del_image($image_id, $image_path, $common_dao){
	$select_sql = " select * from image_files "
		. " where id = '$image_id' and deleted_at is NULL ";

	$file_name = '';

	$db_select_result = $common_dao->db_query($select_sql);
	if($db_select_result){
		$row_data = array();
		$row_data = $db_select_result[0];
		$file_name = $row_data['file_name'];
	}else{
		return "ＤＢからのデータの取得に失敗しました。";
	}

	if(file_exists($image_path . $file_name)){
		if(unlink($image_path . $file_name)){

			$update_sql = "update image_files set deleted_at = Now() "
						. " where id = '$image_id' and deleted_at is NULL";

			$db_update_result = $common_dao->db_update($update_sql);
			if($db_update_result){
				return 1;
			}else{
				return "画像ファイル情報のＤＢ更新に失敗しました。";
			}
		}else{
			return "画像ファイル削除に失敗しました。";
		}
	}else{
		return "画像ファイルが存在しません。";
	}
}

function del_image_ts($image_id, $image_path, $common_dao){
	$select_sql = " select * from image_files "
		. " where id = '$image_id' and deleted_at is NULL ";

	$file_name = '';

	$db_select_result = $common_dao->db_query($select_sql);
	if($db_select_result){
		$row_data = array();
		$row_data = $db_select_result[0];
		$file_name = $row_data['file_name'];
	}else{
		return "ＤＢからのデータの取得に失敗しました。";
	}

	if(file_exists($image_path . $file_name)){
		if(unlink($image_path . $file_name)){

			$update_sql = "update image_files set deleted_at = Now() "
						. " where id = '$image_id' and deleted_at is NULL";

			$db_update_result = $common_dao->db_update_ts($update_sql);
			if($db_update_result){
				return 1;
			}else{
				return "画像ファイル情報のＤＢ更新に失敗しました。";
			}
		}else{
			return "画像ファイル削除に失敗しました。";
		}
	}else{
		return "画像ファイルが存在しません。";
	}
}

function get_image_file_name($image_id, $common_dao){
	$select_sql = "select * from image_files where id = '$image_id' and deleted_at is NULL";

	$db_select_result = $common_dao->db_query($select_sql);
	if($db_select_result){
		$row_data = array();
		$row_data = $db_select_result[0];
		$file_name = $row_data['file_name'];

		return $file_name;
	}else{
		return null;
	}
}

function view_image($image_id, $common_dao){
	$select_sql = "select * from image_files where id = '$image_id' and deleted_at is NULL";

	$db_select_result = $common_dao->db_query($select_sql);
	if($db_select_result){
		$row_data = array();
		$row_data = $db_select_result[0];
		$file_name = $row_data['file_name'];

		return $image_tag;
	}else{
		return 画像なし;
	}
}

?>