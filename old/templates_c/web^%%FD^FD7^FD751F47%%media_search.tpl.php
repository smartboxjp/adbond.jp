<?php /* Smarty version 2.6.25, created on 2010-05-10 21:15:38
         compiled from ./media_search.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'htmlspecialchars', './media_search.tpl', 31, false),array('modifier', 'number_format', './media_search.tpl', 165, false),array('modifier', 'date_format', './media_search.tpl', 224, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => './header.tpl', 'smarty_include_vars' => array('page_title' => 'メディア管理')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<!-- Menu -->
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => './media_menu.tpl', 'smarty_include_vars' => array('user_name' => $this->_tpl_vars['user_name'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<div id="my_contents">

<h2>広告検索</h2>

<div id=message>
<?php if ($this->_tpl_vars['error_message'] != ''): ?>
<div id="error_message">
	<h3>ERROR:<?php echo $this->_tpl_vars['error_message']; ?>
</h3>
</div>
<?php endif; ?>
<?php if ($this->_tpl_vars['info_message'] != ''): ?>
<div id="info_message">
	<h3>INFO:<?php echo $this->_tpl_vars['info_message']; ?>
</h3>
</div>
<?php endif; ?>
</div><!-- message -->

<form method="POST" action="<?php echo $this->_supers['server']['PHP_SELF']; ?>
">
<input type="hidden" name="media_category_id" value="<?php echo $this->_tpl_vars['media_category_id']; ?>
" />
<table cellpadding="0" cellspacing="0">
	<tr>
		<th colspan="2">メディア選択</th>
		<td>
			<select name="media_id">
			<?php $_from = $this->_tpl_vars['media_array']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['media_list'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['media_list']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['data']):
        $this->_foreach['media_list']['iteration']++;
?>
				<option value="<?php echo $this->_tpl_vars['data']['id']; ?>
"<?php if ($this->_tpl_vars['media_id'] == $this->_tpl_vars['data']['id']): ?> selected="selected"<?php endif; ?>><?php echo ((is_array($_tmp=$this->_tpl_vars['data']['media_name'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
</option>
			<?php endforeach; endif; unset($_from); ?>
			</select>
			<input type="submit" name="submit" value="選択" />
		</td>
	</tr>
</table>
<?php if ($this->_tpl_vars['media_id'] != ""): ?>
<table cellpadding="0" cellspacing="0">
	<tr>
		<td>キーワード</td>
		<td><input type="text" size="50" name="keyword" value="<?php echo $this->_tpl_vars['search']['keyword']; ?>
" /></td>
	</tr>
	<tr>
		<td>カテゴリー</td>
		<td>
<?php $_from = $this->_tpl_vars['advert_category_array']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['advert_category_list'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['advert_category_list']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['data']):
        $this->_foreach['advert_category_list']['iteration']++;
?>
			<input type="checkbox" name="category[<?php echo $this->_tpl_vars['data']['id']; ?>
]" value="1"<?php if ($this->_tpl_vars['search']['category'][$this->_tpl_vars['data']['id']] == 1): ?> checked="checked"<?php endif; ?> /><label><?php echo $this->_tpl_vars['data']['name']; ?>
</label>
<?php endforeach; endif; unset($_from); ?>
		</td>
	</tr>
	<tr>
		<td>対応キャリア</td>
		<td>
			<input type="checkbox" name="support_docomo" value="1"<?php if ($this->_tpl_vars['search']['support_docomo'] == 1): ?> checked="checked"<?php endif; ?> /><label>docomo</label>
			<input type="checkbox" name="support_softbank" value="1"<?php if ($this->_tpl_vars['search']['support_softbank'] == 1): ?> checked="checked"<?php endif; ?> /><label>softbank</label>
			<input type="checkbox" name="support_au" value="1"<?php if ($this->_tpl_vars['search']['support_au'] == 1): ?> checked="checked"<?php endif; ?> /><label>au</label>
			<input type="checkbox" name="support_pc" value="1"<?php if ($this->_tpl_vars['search']['support_pc'] == 1): ?> checked="checked"<?php endif; ?> /><label>pc</label>
		</td>
	</tr>
	<tr>
		<td>単価</td>
		<td>
			<input type="text" size="10" name="min_price" value="<?php echo $this->_tpl_vars['search']['min_price']; ?>
" />円 ～ <input type="text" size="10" name="max_price" value="<?php echo $this->_tpl_vars['search']['max_price']; ?>
" />円
		</td>
	</tr>
	<tr>
		<td>登録日時</td>
		<td>
			<input type="checkbox" name="advert_date_flag" value="1"<?php if ($this->_tpl_vars['search']['advert_date_flag'] == 1): ?> checked="checked"<?php endif; ?> />
			<select name="s_year">
<?php unset($this->_sections['cnt']);
$this->_sections['cnt']['name'] = 'cnt';
$this->_sections['cnt']['start'] = (int)2009;
$this->_sections['cnt']['loop'] = is_array($_loop=2021) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['cnt']['show'] = true;
$this->_sections['cnt']['max'] = $this->_sections['cnt']['loop'];
$this->_sections['cnt']['step'] = 1;
if ($this->_sections['cnt']['start'] < 0)
    $this->_sections['cnt']['start'] = max($this->_sections['cnt']['step'] > 0 ? 0 : -1, $this->_sections['cnt']['loop'] + $this->_sections['cnt']['start']);
else
    $this->_sections['cnt']['start'] = min($this->_sections['cnt']['start'], $this->_sections['cnt']['step'] > 0 ? $this->_sections['cnt']['loop'] : $this->_sections['cnt']['loop']-1);
if ($this->_sections['cnt']['show']) {
    $this->_sections['cnt']['total'] = min(ceil(($this->_sections['cnt']['step'] > 0 ? $this->_sections['cnt']['loop'] - $this->_sections['cnt']['start'] : $this->_sections['cnt']['start']+1)/abs($this->_sections['cnt']['step'])), $this->_sections['cnt']['max']);
    if ($this->_sections['cnt']['total'] == 0)
        $this->_sections['cnt']['show'] = false;
} else
    $this->_sections['cnt']['total'] = 0;
if ($this->_sections['cnt']['show']):

            for ($this->_sections['cnt']['index'] = $this->_sections['cnt']['start'], $this->_sections['cnt']['iteration'] = 1;
                 $this->_sections['cnt']['iteration'] <= $this->_sections['cnt']['total'];
                 $this->_sections['cnt']['index'] += $this->_sections['cnt']['step'], $this->_sections['cnt']['iteration']++):
$this->_sections['cnt']['rownum'] = $this->_sections['cnt']['iteration'];
$this->_sections['cnt']['index_prev'] = $this->_sections['cnt']['index'] - $this->_sections['cnt']['step'];
$this->_sections['cnt']['index_next'] = $this->_sections['cnt']['index'] + $this->_sections['cnt']['step'];
$this->_sections['cnt']['first']      = ($this->_sections['cnt']['iteration'] == 1);
$this->_sections['cnt']['last']       = ($this->_sections['cnt']['iteration'] == $this->_sections['cnt']['total']);
?>
				<option value="<?php echo $this->_sections['cnt']['index']; ?>
"<?php if ($this->_tpl_vars['set_s_year'] == $this->_sections['cnt']['index']): ?> selected="selected"<?php endif; ?>><?php echo $this->_sections['cnt']['index']; ?>
年</option>
<?php endfor; endif; ?>
			</select>
			<select name="s_month">
<?php unset($this->_sections['cnt']);
$this->_sections['cnt']['name'] = 'cnt';
$this->_sections['cnt']['start'] = (int)1;
$this->_sections['cnt']['loop'] = is_array($_loop=13) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['cnt']['show'] = true;
$this->_sections['cnt']['max'] = $this->_sections['cnt']['loop'];
$this->_sections['cnt']['step'] = 1;
if ($this->_sections['cnt']['start'] < 0)
    $this->_sections['cnt']['start'] = max($this->_sections['cnt']['step'] > 0 ? 0 : -1, $this->_sections['cnt']['loop'] + $this->_sections['cnt']['start']);
else
    $this->_sections['cnt']['start'] = min($this->_sections['cnt']['start'], $this->_sections['cnt']['step'] > 0 ? $this->_sections['cnt']['loop'] : $this->_sections['cnt']['loop']-1);
if ($this->_sections['cnt']['show']) {
    $this->_sections['cnt']['total'] = min(ceil(($this->_sections['cnt']['step'] > 0 ? $this->_sections['cnt']['loop'] - $this->_sections['cnt']['start'] : $this->_sections['cnt']['start']+1)/abs($this->_sections['cnt']['step'])), $this->_sections['cnt']['max']);
    if ($this->_sections['cnt']['total'] == 0)
        $this->_sections['cnt']['show'] = false;
} else
    $this->_sections['cnt']['total'] = 0;
if ($this->_sections['cnt']['show']):

            for ($this->_sections['cnt']['index'] = $this->_sections['cnt']['start'], $this->_sections['cnt']['iteration'] = 1;
                 $this->_sections['cnt']['iteration'] <= $this->_sections['cnt']['total'];
                 $this->_sections['cnt']['index'] += $this->_sections['cnt']['step'], $this->_sections['cnt']['iteration']++):
$this->_sections['cnt']['rownum'] = $this->_sections['cnt']['iteration'];
$this->_sections['cnt']['index_prev'] = $this->_sections['cnt']['index'] - $this->_sections['cnt']['step'];
$this->_sections['cnt']['index_next'] = $this->_sections['cnt']['index'] + $this->_sections['cnt']['step'];
$this->_sections['cnt']['first']      = ($this->_sections['cnt']['iteration'] == 1);
$this->_sections['cnt']['last']       = ($this->_sections['cnt']['iteration'] == $this->_sections['cnt']['total']);
?>
				<option value="<?php echo $this->_sections['cnt']['index']; ?>
"<?php if ($this->_tpl_vars['set_s_month'] == $this->_sections['cnt']['index']): ?> selected="selected"<?php endif; ?>><?php echo $this->_sections['cnt']['index']; ?>
月</option>
<?php endfor; endif; ?>
			</select>
			<select name="s_day">
<?php unset($this->_sections['cnt']);
$this->_sections['cnt']['name'] = 'cnt';
$this->_sections['cnt']['start'] = (int)1;
$this->_sections['cnt']['loop'] = is_array($_loop=32) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['cnt']['show'] = true;
$this->_sections['cnt']['max'] = $this->_sections['cnt']['loop'];
$this->_sections['cnt']['step'] = 1;
if ($this->_sections['cnt']['start'] < 0)
    $this->_sections['cnt']['start'] = max($this->_sections['cnt']['step'] > 0 ? 0 : -1, $this->_sections['cnt']['loop'] + $this->_sections['cnt']['start']);
else
    $this->_sections['cnt']['start'] = min($this->_sections['cnt']['start'], $this->_sections['cnt']['step'] > 0 ? $this->_sections['cnt']['loop'] : $this->_sections['cnt']['loop']-1);
if ($this->_sections['cnt']['show']) {
    $this->_sections['cnt']['total'] = min(ceil(($this->_sections['cnt']['step'] > 0 ? $this->_sections['cnt']['loop'] - $this->_sections['cnt']['start'] : $this->_sections['cnt']['start']+1)/abs($this->_sections['cnt']['step'])), $this->_sections['cnt']['max']);
    if ($this->_sections['cnt']['total'] == 0)
        $this->_sections['cnt']['show'] = false;
} else
    $this->_sections['cnt']['total'] = 0;
if ($this->_sections['cnt']['show']):

            for ($this->_sections['cnt']['index'] = $this->_sections['cnt']['start'], $this->_sections['cnt']['iteration'] = 1;
                 $this->_sections['cnt']['iteration'] <= $this->_sections['cnt']['total'];
                 $this->_sections['cnt']['index'] += $this->_sections['cnt']['step'], $this->_sections['cnt']['iteration']++):
$this->_sections['cnt']['rownum'] = $this->_sections['cnt']['iteration'];
$this->_sections['cnt']['index_prev'] = $this->_sections['cnt']['index'] - $this->_sections['cnt']['step'];
$this->_sections['cnt']['index_next'] = $this->_sections['cnt']['index'] + $this->_sections['cnt']['step'];
$this->_sections['cnt']['first']      = ($this->_sections['cnt']['iteration'] == 1);
$this->_sections['cnt']['last']       = ($this->_sections['cnt']['iteration'] == $this->_sections['cnt']['total']);
?>
				<option value="<?php echo $this->_sections['cnt']['index']; ?>
"<?php if ($this->_tpl_vars['set_s_day'] == $this->_sections['cnt']['index']): ?> selected="selected"<?php endif; ?>><?php echo $this->_sections['cnt']['index']; ?>
日</option>
<?php endfor; endif; ?>
			</select> ～
			<select name="e_year">
<?php unset($this->_sections['cnt']);
$this->_sections['cnt']['name'] = 'cnt';
$this->_sections['cnt']['start'] = (int)2009;
$this->_sections['cnt']['loop'] = is_array($_loop=2021) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['cnt']['show'] = true;
$this->_sections['cnt']['max'] = $this->_sections['cnt']['loop'];
$this->_sections['cnt']['step'] = 1;
if ($this->_sections['cnt']['start'] < 0)
    $this->_sections['cnt']['start'] = max($this->_sections['cnt']['step'] > 0 ? 0 : -1, $this->_sections['cnt']['loop'] + $this->_sections['cnt']['start']);
else
    $this->_sections['cnt']['start'] = min($this->_sections['cnt']['start'], $this->_sections['cnt']['step'] > 0 ? $this->_sections['cnt']['loop'] : $this->_sections['cnt']['loop']-1);
if ($this->_sections['cnt']['show']) {
    $this->_sections['cnt']['total'] = min(ceil(($this->_sections['cnt']['step'] > 0 ? $this->_sections['cnt']['loop'] - $this->_sections['cnt']['start'] : $this->_sections['cnt']['start']+1)/abs($this->_sections['cnt']['step'])), $this->_sections['cnt']['max']);
    if ($this->_sections['cnt']['total'] == 0)
        $this->_sections['cnt']['show'] = false;
} else
    $this->_sections['cnt']['total'] = 0;
if ($this->_sections['cnt']['show']):

            for ($this->_sections['cnt']['index'] = $this->_sections['cnt']['start'], $this->_sections['cnt']['iteration'] = 1;
                 $this->_sections['cnt']['iteration'] <= $this->_sections['cnt']['total'];
                 $this->_sections['cnt']['index'] += $this->_sections['cnt']['step'], $this->_sections['cnt']['iteration']++):
$this->_sections['cnt']['rownum'] = $this->_sections['cnt']['iteration'];
$this->_sections['cnt']['index_prev'] = $this->_sections['cnt']['index'] - $this->_sections['cnt']['step'];
$this->_sections['cnt']['index_next'] = $this->_sections['cnt']['index'] + $this->_sections['cnt']['step'];
$this->_sections['cnt']['first']      = ($this->_sections['cnt']['iteration'] == 1);
$this->_sections['cnt']['last']       = ($this->_sections['cnt']['iteration'] == $this->_sections['cnt']['total']);
?>
				<option value="<?php echo $this->_sections['cnt']['index']; ?>
"<?php if ($this->_tpl_vars['set_e_year'] == $this->_sections['cnt']['index']): ?> selected="selected"<?php endif; ?>><?php echo $this->_sections['cnt']['index']; ?>
年</option>
<?php endfor; endif; ?>
			</select>
			<select name="e_month">
<?php unset($this->_sections['cnt']);
$this->_sections['cnt']['name'] = 'cnt';
$this->_sections['cnt']['start'] = (int)1;
$this->_sections['cnt']['loop'] = is_array($_loop=13) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['cnt']['show'] = true;
$this->_sections['cnt']['max'] = $this->_sections['cnt']['loop'];
$this->_sections['cnt']['step'] = 1;
if ($this->_sections['cnt']['start'] < 0)
    $this->_sections['cnt']['start'] = max($this->_sections['cnt']['step'] > 0 ? 0 : -1, $this->_sections['cnt']['loop'] + $this->_sections['cnt']['start']);
else
    $this->_sections['cnt']['start'] = min($this->_sections['cnt']['start'], $this->_sections['cnt']['step'] > 0 ? $this->_sections['cnt']['loop'] : $this->_sections['cnt']['loop']-1);
if ($this->_sections['cnt']['show']) {
    $this->_sections['cnt']['total'] = min(ceil(($this->_sections['cnt']['step'] > 0 ? $this->_sections['cnt']['loop'] - $this->_sections['cnt']['start'] : $this->_sections['cnt']['start']+1)/abs($this->_sections['cnt']['step'])), $this->_sections['cnt']['max']);
    if ($this->_sections['cnt']['total'] == 0)
        $this->_sections['cnt']['show'] = false;
} else
    $this->_sections['cnt']['total'] = 0;
if ($this->_sections['cnt']['show']):

            for ($this->_sections['cnt']['index'] = $this->_sections['cnt']['start'], $this->_sections['cnt']['iteration'] = 1;
                 $this->_sections['cnt']['iteration'] <= $this->_sections['cnt']['total'];
                 $this->_sections['cnt']['index'] += $this->_sections['cnt']['step'], $this->_sections['cnt']['iteration']++):
$this->_sections['cnt']['rownum'] = $this->_sections['cnt']['iteration'];
$this->_sections['cnt']['index_prev'] = $this->_sections['cnt']['index'] - $this->_sections['cnt']['step'];
$this->_sections['cnt']['index_next'] = $this->_sections['cnt']['index'] + $this->_sections['cnt']['step'];
$this->_sections['cnt']['first']      = ($this->_sections['cnt']['iteration'] == 1);
$this->_sections['cnt']['last']       = ($this->_sections['cnt']['iteration'] == $this->_sections['cnt']['total']);
?>
				<option value="<?php echo $this->_sections['cnt']['index']; ?>
"<?php if ($this->_tpl_vars['set_e_month'] == $this->_sections['cnt']['index']): ?> selected="selected"<?php endif; ?>><?php echo $this->_sections['cnt']['index']; ?>
月</option>
<?php endfor; endif; ?>
			</select>
			<select name="e_day">
<?php unset($this->_sections['cnt']);
$this->_sections['cnt']['name'] = 'cnt';
$this->_sections['cnt']['start'] = (int)1;
$this->_sections['cnt']['loop'] = is_array($_loop=32) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['cnt']['show'] = true;
$this->_sections['cnt']['max'] = $this->_sections['cnt']['loop'];
$this->_sections['cnt']['step'] = 1;
if ($this->_sections['cnt']['start'] < 0)
    $this->_sections['cnt']['start'] = max($this->_sections['cnt']['step'] > 0 ? 0 : -1, $this->_sections['cnt']['loop'] + $this->_sections['cnt']['start']);
else
    $this->_sections['cnt']['start'] = min($this->_sections['cnt']['start'], $this->_sections['cnt']['step'] > 0 ? $this->_sections['cnt']['loop'] : $this->_sections['cnt']['loop']-1);
if ($this->_sections['cnt']['show']) {
    $this->_sections['cnt']['total'] = min(ceil(($this->_sections['cnt']['step'] > 0 ? $this->_sections['cnt']['loop'] - $this->_sections['cnt']['start'] : $this->_sections['cnt']['start']+1)/abs($this->_sections['cnt']['step'])), $this->_sections['cnt']['max']);
    if ($this->_sections['cnt']['total'] == 0)
        $this->_sections['cnt']['show'] = false;
} else
    $this->_sections['cnt']['total'] = 0;
if ($this->_sections['cnt']['show']):

            for ($this->_sections['cnt']['index'] = $this->_sections['cnt']['start'], $this->_sections['cnt']['iteration'] = 1;
                 $this->_sections['cnt']['iteration'] <= $this->_sections['cnt']['total'];
                 $this->_sections['cnt']['index'] += $this->_sections['cnt']['step'], $this->_sections['cnt']['iteration']++):
$this->_sections['cnt']['rownum'] = $this->_sections['cnt']['iteration'];
$this->_sections['cnt']['index_prev'] = $this->_sections['cnt']['index'] - $this->_sections['cnt']['step'];
$this->_sections['cnt']['index_next'] = $this->_sections['cnt']['index'] + $this->_sections['cnt']['step'];
$this->_sections['cnt']['first']      = ($this->_sections['cnt']['iteration'] == 1);
$this->_sections['cnt']['last']       = ($this->_sections['cnt']['iteration'] == $this->_sections['cnt']['total']);
?>
				<option value="<?php echo $this->_sections['cnt']['index']; ?>
"<?php if ($this->_tpl_vars['set_e_day'] == $this->_sections['cnt']['index']): ?> selected="selected"<?php endif; ?>><?php echo $this->_sections['cnt']['index']; ?>
日</option>
<?php endfor; endif; ?>
			</select>
		</td>
	</tr>
	<tr>
		<td>提携状態</td>
		<td>
			<input type="radio" name="connect_status" value="1"<?php if ($this->_tpl_vars['search']['connect_status'] == 1 || $this->_tpl_vars['search']['connect_status'] == ""): ?> checked="checked"<?php endif; ?> /><label>指定なし</label>
			<input type="radio" name="connect_status" value="2"<?php if ($this->_tpl_vars['search']['connect_status'] == 2): ?> checked="checked"<?php endif; ?> /><label>未提携</label>
			<input type="radio" name="connect_status" value="3"<?php if ($this->_tpl_vars['search']['connect_status'] == 3): ?> checked="checked"<?php endif; ?> /><label>提携済み</label>
		</td>
	</tr>
	<tr>
		<td>ポイントバック対応</td>
		<td>
			<input type="radio" name="point_back_flag" value="1"<?php if ($this->_tpl_vars['search']['point_back_flag'] == 1 || $this->_tpl_vars['search']['point_back_flag'] == ""): ?> checked="checked"<?php endif; ?> /><label>指定なし</label>
			<input type="radio" name="point_back_flag" value="2"<?php if ($this->_tpl_vars['search']['point_back_flag'] == 2): ?> checked="checked"<?php endif; ?> /><label>不可</label>
			<input type="radio" name="point_back_flag" value="3"<?php if ($this->_tpl_vars['search']['point_back_flag'] == 3): ?> checked="checked"<?php endif; ?> /><label>可</label>
		</td>
	</tr>
	<tr>
		<td colspan="3">
			<input type="submit" name="submit" value="検索" />
		</td>
	</tr>
</table>
<table cellpadding="0" cellspacing="0">
	<tr>
		<th colspan="16">
			<input type="submit" name="submit" value="CSVダウンロード" />
		</th>
	</tr>
</table>
</form>
<table cellpadding="0" cellspacing="0">
	<tr>
		<th colspan="16">
			【該当<?php echo $this->_tpl_vars['list_count']; ?>
件】
		</th>
	</tr>
	<tr>
		<th rowspan="2">広告名</th>
		<th rowspan="2">クリック単価</th>
		<th colspan="4">アクション単価</th>
		<th colspan="4">対応キャリア</th>
		<th colspan="2">種別</th>
		<th rowspan="2">カテゴリー</th>
		<th rowspan="2">ポイントバック</th>
		<th rowspan="2">出稿終了日</th>
		<th rowspan="2">&nbsp;</th>
	</tr>
	<tr>
		<th>docomo</th>
		<th>softbank</th>
		<th>au</th>
		<th>pc</th>
		<th>docomo</th>
		<th>softbank</th>
		<th>au</th>
		<th>pc</th>
		<th>一般</th>
		<th>公式</th>
	</tr>
<?php $_from = $this->_tpl_vars['list']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['list'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['list']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['data']):
        $this->_foreach['list']['iteration']++;
?>
	<tr>
		<td><?php echo ((is_array($_tmp=$this->_tpl_vars['data']['advert_name'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
</td>
		<td>&yen;<?php echo ((is_array($_tmp=$this->_tpl_vars['data']['click_price_media'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
		<td>&yen;<?php echo ((is_array($_tmp=$this->_tpl_vars['data']['action_price_media_docomo_1'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
		<td>&yen;<?php echo ((is_array($_tmp=$this->_tpl_vars['data']['action_price_media_softbank_1'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
		<td>&yen;<?php echo ((is_array($_tmp=$this->_tpl_vars['data']['action_price_media_au_1'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
		<td>&yen;<?php echo ((is_array($_tmp=$this->_tpl_vars['data']['action_price_media_pc_1'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
		<td>
<?php if ($this->_tpl_vars['data']['support_docomo'] == 1): ?>
			○
<?php else: ?>
			-
<?php endif; ?>
		</td>
		<td>
<?php if ($this->_tpl_vars['data']['support_softbank'] == 1): ?>
			○
<?php else: ?>
			-
<?php endif; ?>
		</td>
		<td>
<?php if ($this->_tpl_vars['data']['support_au'] == 1): ?>
			○
<?php else: ?>
			-
<?php endif; ?>
		</td>
		<td>
<?php if ($this->_tpl_vars['data']['support_pc'] == 1): ?>
			○
<?php else: ?>
			-
<?php endif; ?>
		</td>
		<td>
<?php if ($this->_tpl_vars['data']['content_type'] == 1): ?>
			○
<?php else: ?>
			-
<?php endif; ?>
		</td>
		<td>
<?php if ($this->_tpl_vars['data']['content_type'] == 2): ?>
			○
<?php else: ?>
			-
<?php endif; ?>
		</td>
		<td><?php echo ((is_array($_tmp=$this->_tpl_vars['data']['advert_category_name'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
</td>
		<td>
<?php if ($this->_tpl_vars['data']['point_back_flag'] == 2): ?>
			○
<?php else: ?>
			-
<?php endif; ?>
		</td>
		<td>
<?php if ($this->_tpl_vars['data']['unrestraint_flag'] == 1): ?>
			無制限
<?php else: ?>
			<?php echo ((is_array($_tmp=$this->_tpl_vars['data']['advert_end_date'])) ? $this->_run_mod_handler('date_format', true, $_tmp, "%Y-%m-%d") : smarty_modifier_date_format($_tmp, "%Y-%m-%d")); ?>

<?php endif; ?>
		</td>
		<td>
			<form method="POST" action="./search_view.php">
				<input type="submit" value="広告表示" />
				<input type="hidden" name="mode" value="advert_view" />
				<input type="hidden" name="advert_id" value="<?php echo $this->_tpl_vars['data']['id']; ?>
" />
			</form>
		</td>
	</tr>
<?php endforeach; endif; unset($_from); ?>
</table>
<?php endif; ?>
</div><!-- contents -->

<!-- フッター -->
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => './hooter.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>