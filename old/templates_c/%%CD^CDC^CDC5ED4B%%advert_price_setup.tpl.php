<?php /* Smarty version 2.6.25, created on 2010-05-12 20:53:36
         compiled from ./advert_price_setup.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'htmlspecialchars', './advert_price_setup.tpl', 28, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => './header.tpl', 'smarty_include_vars' => array('page_title' => '媒体別単価設定')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<!-- Menu -->
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => './menu.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<div id="my_contents">

<h2>媒体別単価設定</h2>

<div id=message>
<?php if ($this->_tpl_vars['error_message'] != ''): ?>
<div id="error_message">
<h3>ERROR:<?php echo $this->_tpl_vars['error_message']; ?>
</h3>
</div>
<?php endif; ?>
<?php if ($this->_tpl_vars['info_message'] != ''): ?>
<div id="info_message">
<h3>INFO:<?php echo $this->_tpl_vars['info_message']; ?>
</h3>
</div>
<?php endif; ?>
</div><!-- message -->

<form method="POST" action="<?php echo $this->_supers['server']['PHP_SELF']; ?>
">
<table cellpadding="0" cellspacing="0">
<?php $_from = $this->_tpl_vars['media_category_array']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['media_category_list'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['media_category_list']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['data']):
        $this->_foreach['media_category_list']['iteration']++;
?>
	<tr>
		<td>
			<a href="?advert_id=<?php echo $this->_tpl_vars['advert_id']; ?>
&category_id=<?php echo $this->_tpl_vars['data']['id']; ?>
"><?php echo ((is_array($_tmp=$this->_tpl_vars['data']['name'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
</a>
		</td>
	</tr>
<?php endforeach; endif; unset($_from); ?>
</table>
<?php if ($this->_tpl_vars['category_id'] != ""): ?>
<table cellpadding="0" cellspacing="0">
	<tr>
		<td><?php echo ((is_array($_tmp=$this->_tpl_vars['media_category_array'][$this->_tpl_vars['category_id']]['name'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
</td>
	</tr>
<?php $_from = $this->_tpl_vars['media_array']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['media_publisher_list'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['media_publisher_list']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['data']):
        $this->_foreach['media_publisher_list']['iteration']++;
?>
	<tr>
		<td>
			<a href="?advert_id=<?php echo $this->_tpl_vars['advert_id']; ?>
&category_id=<?php echo $this->_tpl_vars['category_id']; ?>
&publisher_id=<?php echo $this->_tpl_vars['data']['publisher_id']; ?>
"><?php echo ((is_array($_tmp=$this->_tpl_vars['data']['publisher_name'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
</a>
		</td>
	</tr>
<?php endforeach; endif; unset($_from); ?>
</table>
<?php if ($this->_tpl_vars['publisher_id'] != ""): ?>
<table cellpadding="0" cellspacing="0">
	<tr>
		<td><?php echo ((is_array($_tmp=$this->_tpl_vars['media_array'][$this->_tpl_vars['publisher_id']]['publisher_name'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
</td>
	</tr>
<?php $_from = $this->_tpl_vars['media_array'][$this->_tpl_vars['publisher_id']]['media']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['media_list'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['media_list']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['data']):
        $this->_foreach['media_list']['iteration']++;
?>
	<tr>
		<td>
			<a href="?advert_id=<?php echo $this->_tpl_vars['advert_id']; ?>
&category_id=<?php echo $this->_tpl_vars['category_id']; ?>
&publisher_id=<?php echo $this->_tpl_vars['publisher_id']; ?>
&media_id=<?php echo $this->_tpl_vars['data']['media_id']; ?>
"><?php echo ((is_array($_tmp=$this->_tpl_vars['data']['media_name'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
</a>
		</td>
	</tr>
<?php endforeach; endif; unset($_from); ?>
</table>
<?php if ($this->_tpl_vars['media_id'] != ""): ?>
<table cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="3"><?php echo ((is_array($_tmp=$this->_tpl_vars['media_array'][$this->_tpl_vars['publisher_id']]['media'][$this->_tpl_vars['media_id']]['media_name'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
</td>
	</tr>
	<tr>
		<th colspan="2">クリック単価(クライアント)</th>
		<td>
			<input type="text" size="10" name="click_price_client" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['click_price_client'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />円
		</td>
	</tr>
	<tr>
		<th colspan="2">クリック単価(メディア)</th>
		<td>
			<input type="text" size="10" name="click_price_media" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['click_price_media'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />円
		</td>
	</tr>
	<tr>
		<th colspan="2">アクション単価(金額)(クライアント)</th>
		<td>
			[成果1]
			docomo:<input type="text" size="10" name="action_price_client_docomo_1" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['action_price_client_docomo_1'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />円
			softbank:<input type="text" size="10" name="action_price_client_softbank_1" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['action_price_client_softbank_1'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />円
			au:<input type="text" size="10" name="action_price_client_au_1" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['action_price_client_au_1'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />円
			pc:<input type="text" size="10" name="action_price_client_pc_1" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['action_price_client_pc_1'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />円<br />
			[成果2]
			docomo:<input type="text" size="10" name="action_price_client_docomo_2" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['action_price_client_docomo_2'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />円
			softbank:<input type="text" size="10" name="action_price_client_softbank_2" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['action_price_client_softbank_2'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />円
			au:<input type="text" size="10" name="action_price_client_au_2" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['action_price_client_au_2'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />円
			pc:<input type="text" size="10" name="action_price_client_pc_2" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['action_price_client_pc_2'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />円<br />
			[成果3]
			docomo:<input type="text" size="10" name="action_price_client_docomo_3" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['action_price_client_docomo_3'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />円
			softbank:<input type="text" size="10" name="action_price_client_softbank_3" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['action_price_client_softbank_3'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />円
			au:<input type="text" size="10" name="action_price_client_au_3" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['action_price_client_au_3'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />円
			pc:<input type="text" size="10" name="action_price_client_pc_3" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['action_price_client_pc_3'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />円<br />
			[成果4]
			docomo:<input type="text" size="10" name="action_price_client_docomo_4" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['action_price_client_docomo_4'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />円
			softbank:<input type="text" size="10" name="action_price_client_softbank_4" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['action_price_client_softbank_4'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />円
			au:<input type="text" size="10" name="action_price_client_au_4" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['action_price_client_au_4'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />円
			pc:<input type="text" size="10" name="action_price_client_pc_4" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['action_price_client_pc_4'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />円<br />
			[成果5]
			docomo:<input type="text" size="10" name="action_price_client_docomo_5" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['action_price_client_docomo_5'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />円
			softbank:<input type="text" size="10" name="action_price_client_softbank_5" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['action_price_client_softbank_5'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />円
			au:<input type="text" size="10" name="action_price_client_au_5" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['action_price_client_au_5'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />円
			pc:<input type="text" size="10" name="action_price_client_pc_5" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['action_price_client_pc_5'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />円<br />
		</td>
	</tr>
	<tr>
		<th colspan="2">アクション単価(金額)(メディア)</th>
		<td>
			[成果1]
			docomo:<input type="text" size="10" name="action_price_media_docomo_1" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['action_price_media_docomo_1'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />円
			softbank:<input type="text" size="10" name="action_price_media_softbank_1" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['action_price_media_softbank_1'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />円
			au:<input type="text" size="10" name="action_price_media_au_1" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['action_price_media_au_1'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />円
			pc:<input type="text" size="10" name="action_price_media_pc_1" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['action_price_media_pc_1'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />円<br />
			[成果2]
			docomo:<input type="text" size="10" name="action_price_media_docomo_2" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['action_price_media_docomo_2'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />円
			softbank:<input type="text" size="10" name="action_price_media_softbank_2" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['action_price_media_softbank_2'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />円
			au:<input type="text" size="10" name="action_price_media_au_2" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['action_price_media_au_2'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />円
			pc:<input type="text" size="10" name="action_price_media_pc_2" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['action_price_media_pc_2'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />円<br />
			[成果3]
			docomo:<input type="text" size="10" name="action_price_media_docomo_3" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['action_price_media_docomo_3'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />円
			softbank:<input type="text" size="10" name="action_price_media_softbank_3" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['action_price_media_softbank_3'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />円
			au:<input type="text" size="10" name="action_price_media_au_3" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['action_price_media_au_3'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />円
			pc:<input type="text" size="10" name="action_price_media_pc_3" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['action_price_media_pc_3'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />円<br />
			[成果4]
			docomo:<input type="text" size="10" name="action_price_media_docomo_4" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['action_price_media_docomo_4'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />円
			softbank:<input type="text" size="10" name="action_price_media_softbank_4" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['action_price_media_softbank_4'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />円
			au:<input type="text" size="10" name="action_price_media_au_4" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['action_price_media_au_4'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />円
			pc:<input type="text" size="10" name="action_price_media_pc_4" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['action_price_media_pc_4'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />円<br />
			[成果5]
			docomo:<input type="text" size="10" name="action_price_media_docomo_5" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['action_price_media_docomo_5'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />円
			softbank:<input type="text" size="10" name="action_price_media_softbank_5" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['action_price_media_softbank_5'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />円
			au:<input type="text" size="10" name="action_price_media_au_5" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['action_price_media_au_5'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />円
			pc:<input type="text" size="10" name="action_price_media_pc_5" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['action_price_media_pc_5'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />円<br />
		</td>
	</tr>
</table>
<table cellpadding="0" cellspacing="0">
	<tr>
		<td>
			<input type="submit" value="設定" />
			<input type="hidden" name="mode" value="setup" />
			<input type="hidden" name="advert_id" value="<?php echo $this->_tpl_vars['advert_id']; ?>
" />
			<input type="hidden" name="category_id" value="<?php echo $this->_tpl_vars['category_id']; ?>
" />
			<input type="hidden" name="publisher_id" value="<?php echo $this->_tpl_vars['publisher_id']; ?>
" />
			<input type="hidden" name="media_id" value="<?php echo $this->_tpl_vars['media_id']; ?>
" />
			<input type="hidden" name="id" value="<?php echo $this->_tpl_vars['form_data']['id']; ?>
" />
		</td>
	</tr>
</table>
<?php endif; ?>
<?php endif; ?>
<?php endif; ?>
</form>
</div><!-- contents -->

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => './hooter.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>