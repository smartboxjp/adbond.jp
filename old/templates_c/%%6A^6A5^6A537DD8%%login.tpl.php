<?php /* Smarty version 2.6.25, created on 2010-05-10 21:44:27
         compiled from login.tpl */ ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => './header.tpl', 'smarty_include_vars' => array('page_title' => 'Login')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<div id=message>
<?php if ($this->_tpl_vars['error_message'] != ''): ?>
<div id="error_message">
<h3>ERROR:<?php echo $this->_tpl_vars['error_message']; ?>
</h3>
</div>
<?php endif; ?>
<?php if ($this->_tpl_vars['info_message'] != ''): ?>
<div id="info_message">
<h3>INFO:<?php echo $this->_tpl_vars['info_message']; ?>
</h3>
</div>
<?php endif; ?>
</div><!-- message -->

	<div id="login">
		<form action="#" method="POST">
			<input type="hidden" name="logon" value="yy" />
			<table cellspacing="0">
				<tr>
					<th>login:</th>
					<td><input type="text" name="login_id" value="" /></td>
				</tr>
				<tr>
					<th>password:</th>
					<td><input type="password" name="login_pass" value="" /></td>
				</tr>
				<tr>
					<td colspan="2" id="login-btn">
						<input type="submit" value="login" />
						<input type="reset" />
					</td>
				</tr>
			</table>
		</form>
	</div>

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => './hooter.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>