<?php /* Smarty version 2.6.25, created on 2010-05-17 03:16:47
         compiled from ./media_report_advert.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'number_format', './media_report_advert.tpl', 85, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => './header.tpl', 'smarty_include_vars' => array('page_title' => 'メディア管理')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<!-- Menu -->
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => './media_menu.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<div id="my_contents">

<h2>レポート</h2>

<div id=message>
<?php if ($this->_tpl_vars['error_message'] != ''): ?>
<div id="error_message">
	<h3>ERROR:<?php echo $this->_tpl_vars['error_message']; ?>
</h3>
</div>
<?php endif; ?>
<?php if ($this->_tpl_vars['info_message'] != ''): ?>
<div id="info_message">
	<h3>INFO:<?php echo $this->_tpl_vars['info_message']; ?>
</h3>
</div>
<?php endif; ?>
</div><!-- message -->

<form method="POST" action="<?php echo $this->_supers['server']['PHP_SELF']; ?>
">
<table cellpadding="0" cellspacing="0">
	<tr>
		<td>
			<a href="./report.php">時間別</a>
			広告別
			<a href="./report_media.php">メディア別</a>
		</td>
	</tr>
	<tr>
		<td>
			<input type="radio" name="date_type" value="1"<?php if ($this->_tpl_vars['date_type'] == 1): ?> checked="checked"<?php endif; ?> /><label>月別</label>
			<input type="radio" name="date_type" value="2"<?php if ($this->_tpl_vars['date_type'] == 2): ?> checked="checked"<?php endif; ?> /><label>日別</label>
		</td>
	</tr>
	<tr>
		<td>
			<input type="radio" name="carrier" value="1"<?php if ($this->_tpl_vars['carrier'] == 1): ?> checked="checked"<?php endif; ?> /><label>キャリア合計</label>
			<input type="radio" name="carrier" value="2"<?php if ($this->_tpl_vars['carrier'] == 2): ?> checked="checked"<?php endif; ?> /><label>キャリア別</label>
		</td>
	</tr>
	<tr>
		<td>
			<select name="s_year">
<?php unset($this->_sections['cnt']);
$this->_sections['cnt']['name'] = 'cnt';
$this->_sections['cnt']['start'] = (int)2009;
$this->_sections['cnt']['loop'] = is_array($_loop=2021) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['cnt']['show'] = true;
$this->_sections['cnt']['max'] = $this->_sections['cnt']['loop'];
$this->_sections['cnt']['step'] = 1;
if ($this->_sections['cnt']['start'] < 0)
    $this->_sections['cnt']['start'] = max($this->_sections['cnt']['step'] > 0 ? 0 : -1, $this->_sections['cnt']['loop'] + $this->_sections['cnt']['start']);
else
    $this->_sections['cnt']['start'] = min($this->_sections['cnt']['start'], $this->_sections['cnt']['step'] > 0 ? $this->_sections['cnt']['loop'] : $this->_sections['cnt']['loop']-1);
if ($this->_sections['cnt']['show']) {
    $this->_sections['cnt']['total'] = min(ceil(($this->_sections['cnt']['step'] > 0 ? $this->_sections['cnt']['loop'] - $this->_sections['cnt']['start'] : $this->_sections['cnt']['start']+1)/abs($this->_sections['cnt']['step'])), $this->_sections['cnt']['max']);
    if ($this->_sections['cnt']['total'] == 0)
        $this->_sections['cnt']['show'] = false;
} else
    $this->_sections['cnt']['total'] = 0;
if ($this->_sections['cnt']['show']):

            for ($this->_sections['cnt']['index'] = $this->_sections['cnt']['start'], $this->_sections['cnt']['iteration'] = 1;
                 $this->_sections['cnt']['iteration'] <= $this->_sections['cnt']['total'];
                 $this->_sections['cnt']['index'] += $this->_sections['cnt']['step'], $this->_sections['cnt']['iteration']++):
$this->_sections['cnt']['rownum'] = $this->_sections['cnt']['iteration'];
$this->_sections['cnt']['index_prev'] = $this->_sections['cnt']['index'] - $this->_sections['cnt']['step'];
$this->_sections['cnt']['index_next'] = $this->_sections['cnt']['index'] + $this->_sections['cnt']['step'];
$this->_sections['cnt']['first']      = ($this->_sections['cnt']['iteration'] == 1);
$this->_sections['cnt']['last']       = ($this->_sections['cnt']['iteration'] == $this->_sections['cnt']['total']);
?>
				<option value="<?php echo $this->_sections['cnt']['index']; ?>
"<?php if ($this->_tpl_vars['set_s_year'] == $this->_sections['cnt']['index']): ?> selected="selected"<?php endif; ?>><?php echo $this->_sections['cnt']['index']; ?>
年</option>
<?php endfor; endif; ?>
			</select>
			<select name="s_month">
<?php unset($this->_sections['cnt']);
$this->_sections['cnt']['name'] = 'cnt';
$this->_sections['cnt']['start'] = (int)1;
$this->_sections['cnt']['loop'] = is_array($_loop=13) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['cnt']['show'] = true;
$this->_sections['cnt']['max'] = $this->_sections['cnt']['loop'];
$this->_sections['cnt']['step'] = 1;
if ($this->_sections['cnt']['start'] < 0)
    $this->_sections['cnt']['start'] = max($this->_sections['cnt']['step'] > 0 ? 0 : -1, $this->_sections['cnt']['loop'] + $this->_sections['cnt']['start']);
else
    $this->_sections['cnt']['start'] = min($this->_sections['cnt']['start'], $this->_sections['cnt']['step'] > 0 ? $this->_sections['cnt']['loop'] : $this->_sections['cnt']['loop']-1);
if ($this->_sections['cnt']['show']) {
    $this->_sections['cnt']['total'] = min(ceil(($this->_sections['cnt']['step'] > 0 ? $this->_sections['cnt']['loop'] - $this->_sections['cnt']['start'] : $this->_sections['cnt']['start']+1)/abs($this->_sections['cnt']['step'])), $this->_sections['cnt']['max']);
    if ($this->_sections['cnt']['total'] == 0)
        $this->_sections['cnt']['show'] = false;
} else
    $this->_sections['cnt']['total'] = 0;
if ($this->_sections['cnt']['show']):

            for ($this->_sections['cnt']['index'] = $this->_sections['cnt']['start'], $this->_sections['cnt']['iteration'] = 1;
                 $this->_sections['cnt']['iteration'] <= $this->_sections['cnt']['total'];
                 $this->_sections['cnt']['index'] += $this->_sections['cnt']['step'], $this->_sections['cnt']['iteration']++):
$this->_sections['cnt']['rownum'] = $this->_sections['cnt']['iteration'];
$this->_sections['cnt']['index_prev'] = $this->_sections['cnt']['index'] - $this->_sections['cnt']['step'];
$this->_sections['cnt']['index_next'] = $this->_sections['cnt']['index'] + $this->_sections['cnt']['step'];
$this->_sections['cnt']['first']      = ($this->_sections['cnt']['iteration'] == 1);
$this->_sections['cnt']['last']       = ($this->_sections['cnt']['iteration'] == $this->_sections['cnt']['total']);
?>
				<option value="<?php echo $this->_sections['cnt']['index']; ?>
"<?php if ($this->_tpl_vars['set_s_month'] == $this->_sections['cnt']['index']): ?> selected="selected"<?php endif; ?>><?php echo $this->_sections['cnt']['index']; ?>
月</option>
<?php endfor; endif; ?>
			</select>
			<select name="s_day">
<?php unset($this->_sections['cnt']);
$this->_sections['cnt']['name'] = 'cnt';
$this->_sections['cnt']['start'] = (int)1;
$this->_sections['cnt']['loop'] = is_array($_loop=32) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['cnt']['show'] = true;
$this->_sections['cnt']['max'] = $this->_sections['cnt']['loop'];
$this->_sections['cnt']['step'] = 1;
if ($this->_sections['cnt']['start'] < 0)
    $this->_sections['cnt']['start'] = max($this->_sections['cnt']['step'] > 0 ? 0 : -1, $this->_sections['cnt']['loop'] + $this->_sections['cnt']['start']);
else
    $this->_sections['cnt']['start'] = min($this->_sections['cnt']['start'], $this->_sections['cnt']['step'] > 0 ? $this->_sections['cnt']['loop'] : $this->_sections['cnt']['loop']-1);
if ($this->_sections['cnt']['show']) {
    $this->_sections['cnt']['total'] = min(ceil(($this->_sections['cnt']['step'] > 0 ? $this->_sections['cnt']['loop'] - $this->_sections['cnt']['start'] : $this->_sections['cnt']['start']+1)/abs($this->_sections['cnt']['step'])), $this->_sections['cnt']['max']);
    if ($this->_sections['cnt']['total'] == 0)
        $this->_sections['cnt']['show'] = false;
} else
    $this->_sections['cnt']['total'] = 0;
if ($this->_sections['cnt']['show']):

            for ($this->_sections['cnt']['index'] = $this->_sections['cnt']['start'], $this->_sections['cnt']['iteration'] = 1;
                 $this->_sections['cnt']['iteration'] <= $this->_sections['cnt']['total'];
                 $this->_sections['cnt']['index'] += $this->_sections['cnt']['step'], $this->_sections['cnt']['iteration']++):
$this->_sections['cnt']['rownum'] = $this->_sections['cnt']['iteration'];
$this->_sections['cnt']['index_prev'] = $this->_sections['cnt']['index'] - $this->_sections['cnt']['step'];
$this->_sections['cnt']['index_next'] = $this->_sections['cnt']['index'] + $this->_sections['cnt']['step'];
$this->_sections['cnt']['first']      = ($this->_sections['cnt']['iteration'] == 1);
$this->_sections['cnt']['last']       = ($this->_sections['cnt']['iteration'] == $this->_sections['cnt']['total']);
?>
				<option value="<?php echo $this->_sections['cnt']['index']; ?>
"<?php if ($this->_tpl_vars['set_s_day'] == $this->_sections['cnt']['index']): ?> selected="selected"<?php endif; ?>><?php echo $this->_sections['cnt']['index']; ?>
日</option>
<?php endfor; endif; ?>
			</select>
			<input type="submit" value="表示" />
			<input type="hidden" name="mode" value="search" />
		</td>
	</tr>
</table>
</form>

<?php if ($this->_tpl_vars['carrier'] == 1): ?>
<table cellpadding="0" cellspacing="0">
	<tr>
		<th rowspan="2">広告名</th>
		<th colspan="2">アフィリエイト報酬</th>
		<th colspan="2">クリック報酬</th>
		<th rowspan="2">合計</th>
	</tr>
	<tr>
		<th>クリック数</th>
		<th>報酬金額</th>
		<th>アクション数</th>
		<th>報酬金額</th>
	</tr>
<?php $_from = $this->_tpl_vars['summary']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['summary'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['summary']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['data']):
        $this->_foreach['summary']['iteration']++;
?>
	<tr>
		<td><?php echo $this->_tpl_vars['data']['advert_name']; ?>
</td>
		<td><?php echo ((is_array($_tmp=$this->_tpl_vars['data']['action_count'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
		<td>&yen;<?php echo ((is_array($_tmp=$this->_tpl_vars['data']['action_price'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
		<td><?php echo ((is_array($_tmp=$this->_tpl_vars['data']['click_count'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
		<td>&yen;<?php echo ((is_array($_tmp=$this->_tpl_vars['data']['click_price'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
		<td>&yen;<?php echo ((is_array($_tmp=$this->_tpl_vars['data']['total_price'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
	</tr>
<?php endforeach; endif; unset($_from); ?>
	<tr>
		<td>合計</td>
		<td><?php echo ((is_array($_tmp=$this->_tpl_vars['all']['action_count'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
		<td>&yen;<?php echo ((is_array($_tmp=$this->_tpl_vars['all']['action_price'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
		<td><?php echo ((is_array($_tmp=$this->_tpl_vars['all']['click_count'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
		<td>&yen;<?php echo ((is_array($_tmp=$this->_tpl_vars['all']['click_price'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
		<td>&yen;<?php echo ((is_array($_tmp=$this->_tpl_vars['all']['total_price'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
	</tr>
</table>
<?php elseif ($this->_tpl_vars['carrier'] == 2): ?>
<table cellpadding="0" cellspacing="0">
	<tr>
		<th colspan="2"></th>
		<th colspan="2">アフィリエイト報酬</th>
		<th colspan="2">クリック報酬</th>
		<th rowspan="2">合計</th>
	</tr>
	<tr>
		<th>年月</th>
		<th>キャリア</th>
		<th>クリック数</th>
		<th>報酬金額</th>
		<th>アクション数</th>
		<th>報酬金額</th>
	</tr>
<?php $_from = $this->_tpl_vars['summary']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['summary'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['summary']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['data']):
        $this->_foreach['summary']['iteration']++;
?>
	<tr>
		<td rowspan="4"><?php echo $this->_tpl_vars['data']['advert_name']; ?>
</td>
		<td>docomo</td>
		<td><?php echo ((is_array($_tmp=$this->_tpl_vars['data']['docomo']['action_count'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
		<td>&yen;<?php echo ((is_array($_tmp=$this->_tpl_vars['data']['docomo']['action_price'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
		<td><?php echo ((is_array($_tmp=$this->_tpl_vars['data']['docomo']['click_count'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
		<td>&yen;<?php echo ((is_array($_tmp=$this->_tpl_vars['data']['docomo']['click_price'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
		<td>&yen;<?php echo ((is_array($_tmp=$this->_tpl_vars['data']['docomo']['total_price'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
	</tr>
	<tr>
		<td>softbank</td>
		<td><?php echo ((is_array($_tmp=$this->_tpl_vars['data']['softbank']['action_count'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
		<td>&yen;<?php echo ((is_array($_tmp=$this->_tpl_vars['data']['softbank']['action_price'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
		<td><?php echo ((is_array($_tmp=$this->_tpl_vars['data']['softbank']['click_count'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
		<td>&yen;<?php echo ((is_array($_tmp=$this->_tpl_vars['data']['softbank']['click_price'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
		<td>&yen;<?php echo ((is_array($_tmp=$this->_tpl_vars['data']['softbank']['total_price'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
	</tr>
	<tr>
		<td>au</td>
		<td><?php echo ((is_array($_tmp=$this->_tpl_vars['data']['au']['action_count'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
		<td>&yen;<?php echo ((is_array($_tmp=$this->_tpl_vars['data']['au']['action_price'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
		<td><?php echo ((is_array($_tmp=$this->_tpl_vars['data']['au']['click_count'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
		<td>&yen;<?php echo ((is_array($_tmp=$this->_tpl_vars['data']['au']['click_price'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
		<td>&yen;<?php echo ((is_array($_tmp=$this->_tpl_vars['data']['au']['total_price'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
	</tr>
	<tr>
		<td>pc</td>
		<td><?php echo ((is_array($_tmp=$this->_tpl_vars['data']['pc']['action_count'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
		<td>&yen;<?php echo ((is_array($_tmp=$this->_tpl_vars['data']['pc']['action_price'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
		<td><?php echo ((is_array($_tmp=$this->_tpl_vars['data']['pc']['click_count'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
		<td>&yen;<?php echo ((is_array($_tmp=$this->_tpl_vars['data']['pc']['click_price'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
		<td>&yen;<?php echo ((is_array($_tmp=$this->_tpl_vars['data']['pc']['total_price'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
	</tr>
<?php endforeach; endif; unset($_from); ?>
	<tr>
		<td colspan="2">合計</td>
		<td><?php echo ((is_array($_tmp=$this->_tpl_vars['all']['action_count'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
		<td>&yen;<?php echo ((is_array($_tmp=$this->_tpl_vars['all']['action_price'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
		<td><?php echo ((is_array($_tmp=$this->_tpl_vars['all']['click_count'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
		<td>&yen;<?php echo ((is_array($_tmp=$this->_tpl_vars['all']['click_price'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
		<td>&yen;<?php echo ((is_array($_tmp=$this->_tpl_vars['all']['total_price'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
	</tr>
</table>
<?php endif; ?>
</div><!-- contents -->

<!-- フッター -->
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => './hooter.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>