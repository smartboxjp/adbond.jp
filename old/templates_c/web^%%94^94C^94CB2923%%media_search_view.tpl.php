<?php /* Smarty version 2.6.25, created on 2010-05-10 21:44:43
         compiled from ./media_search_view.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'htmlspecialchars', './media_search_view.tpl', 29, false),array('modifier', 'number_format', './media_search_view.tpl', 64, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => './header.tpl', 'smarty_include_vars' => array('page_title' => 'メディア管理')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<!-- Menu -->
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => './media_menu.tpl', 'smarty_include_vars' => array('user_name' => $this->_tpl_vars['user_name'])));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<div id="my_contents">

<h2>広告検索</h2>

<div id=message>
<?php if ($this->_tpl_vars['error_message'] != ''): ?>
<div id="error_message">
	<h3>ERROR:<?php echo $this->_tpl_vars['error_message']; ?>
</h3>
</div>
<?php endif; ?>
<?php if ($this->_tpl_vars['info_message'] != ''): ?>
<div id="info_message">
	<h3>INFO:<?php echo $this->_tpl_vars['info_message']; ?>
</h3>
</div>
<?php endif; ?>
</div><!-- message -->

<table cellpadding="0" cellspacing="0">
	<tr>
		<th colspan="3">広告情報</th>
	</tr>
	<tr>
		<th colspan="2">広告主</th>
		<td><?php echo ((is_array($_tmp=$this->_tpl_vars['data']['advert_client_name'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
</td>
	</tr>
	<tr>
		<th colspan="2">広告カテゴリー</th>
		<td><?php echo ((is_array($_tmp=$this->_tpl_vars['data']['advert_category_name'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
</td>
	</tr>
	<tr>
		<th colspan="2">広告名</th>
		<td><?php echo ((is_array($_tmp=$this->_tpl_vars['data']['advert_name'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
</td>
	</tr>
	<tr>
		<th colspan="2">コンテンツ種別</th>
		<td>
<?php if ($this->_tpl_vars['data']['content_type'] == 1): ?>
			一般
<?php elseif ($this->_tpl_vars['data']['content_type'] == 2): ?>
			公式
<?php endif; ?>
		</td>
	</tr>
	<tr>
		<th colspan="2">対応キャリア</th>
		<td>
			<?php if ($this->_tpl_vars['data']['support_docomo'] == 1): ?> docomo<?php endif; ?>
			<?php if ($this->_tpl_vars['data']['support_softbank'] == 1): ?> softbank<?php endif; ?>
			<?php if ($this->_tpl_vars['data']['support_au'] == 1): ?> au<?php endif; ?>
			<?php if ($this->_tpl_vars['data']['support_pc'] == 1): ?> pc<?php endif; ?>
		</td>
	</tr>
	<tr>
		<th colspan="2">広告概要</th>
		<td><?php echo ((is_array($_tmp=$this->_tpl_vars['data']['site_outline'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
</td>
	</tr>
	<tr>
		<th colspan="2">クリック単価</th>
		<td>&yen;<?php echo ((is_array($_tmp=$this->_tpl_vars['data']['click_price_media'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
	</tr>
	<tr>
		<th colspan="2">アクション単価</th>
		<td>
			docomo:&yen;<?php echo ((is_array($_tmp=$this->_tpl_vars['data']['action_price_media_docomo_1'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>

			softbank:&yen;<?php echo ((is_array($_tmp=$this->_tpl_vars['data']['action_price_media_softbank_1'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>

			au:&yen;<?php echo ((is_array($_tmp=$this->_tpl_vars['data']['action_price_media_au_1'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>

			pc:&yen;<?php echo ((is_array($_tmp=$this->_tpl_vars['data']['action_price_media_pc_1'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>

		</td>
	</tr>
	<tr>
		<th colspan="2">ポイントバック</th>
		<td>
			<?php if ($this->_tpl_vars['data']['point_back_flag'] == 1): ?>不可<?php endif; ?>
			<?php if ($this->_tpl_vars['data']['point_back_flag'] == 2): ?>可<?php endif; ?>
		</td>
	</tr>
	<tr>
		<th colspan="2">アダルト</th>
		<td>
			<?php if ($this->_tpl_vars['data']['adult_flag'] == 1): ?>不可<?php endif; ?>
			<?php if ($this->_tpl_vars['data']['adult_flag'] == 2): ?>可<?php endif; ?>
		</td>
	</tr>
	<tr>
		<th colspan="2">出会い</th>
		<td>
			<?php if ($this->_tpl_vars['data']['dating_flag'] == 1): ?>不可<?php endif; ?>
			<?php if ($this->_tpl_vars['data']['dating_flag'] == 2): ?>可<?php endif; ?>
		</td>
	</tr>
	<tr>
		<th colspan="2">出稿開始日</th>
		<td><?php echo $this->_tpl_vars['data']['advert_start_date']; ?>
</td>
	</tr>
	<tr>
		<th colspan="2">出稿終了日</th>
		<td>
<?php if ($this->_tpl_vars['data']['unrestraint_flag'] == 1): ?>
		無期限
<?php else: ?>
		<?php echo $this->_tpl_vars['data']['advert_end_date']; ?>

<?php endif; ?>
		</td>
	</tr>
	<tr>
		<th colspan="2">提携状態</th>
		<td>
			<?php if ($this->_tpl_vars['connect']['status'] == 1 || $this->_tpl_vars['connect']['status'] == ""): ?>未提携<?php endif; ?>
			<?php if ($this->_tpl_vars['connect']['status'] == 2): ?>提携済み<?php endif; ?>
		</td>
	</tr>
</table>
<table cellpadding="0" cellspacing="0">
	<tr>
		<th colspan="3">広告原稿</th>
	</tr>
<?php if ($this->_tpl_vars['data']['ms_text_1'] != ""): ?>
	<tr>
		<th colspan="2">テキスト1</th>
		<td><?php echo ((is_array($_tmp=$this->_tpl_vars['data']['ms_text_1'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
</td>
	</tr>
<?php endif; ?>
<?php if ($this->_tpl_vars['data']['ms_text_2'] != ""): ?>
	<tr>
		<th colspan="2">テキスト2</th>
		<td><?php echo ((is_array($_tmp=$this->_tpl_vars['data']['ms_text_2'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
</td>
	</tr>
<?php endif; ?>
<?php if ($this->_tpl_vars['data']['ms_text_3'] != ""): ?>
	<tr>
		<th colspan="2">テキスト3</th>
		<td><?php echo ((is_array($_tmp=$this->_tpl_vars['data']['ms_text_3'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
</td>
	</tr>
<?php endif; ?>
<?php if ($this->_tpl_vars['data']['ms_text_4'] != ""): ?>
	<tr>
		<th colspan="2">テキスト4</th>
		<td><?php echo ((is_array($_tmp=$this->_tpl_vars['data']['ms_text_4'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
</td>
	</tr>
<?php endif; ?>
<?php if ($this->_tpl_vars['data']['ms_text_5'] != ""): ?>
	<tr>
		<th colspan="2">テキスト5</th>
		<td><?php echo ((is_array($_tmp=$this->_tpl_vars['data']['ms_text_5'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
</td>
	</tr>
<?php endif; ?>
<?php if ($this->_tpl_vars['data']['ms_email_1'] != ""): ?>
	<tr>
		<th colspan="2">メール1</th>
		<td><?php echo ((is_array($_tmp=$this->_tpl_vars['data']['ms_email_1'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
</td>
	</tr>
<?php endif; ?>
<?php if ($this->_tpl_vars['data']['ms_email_2'] != ""): ?>
	<tr>
		<th colspan="2">メール2</th>
		<td><?php echo ((is_array($_tmp=$this->_tpl_vars['data']['ms_email_2'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
</td>
	</tr>
<?php endif; ?>
<?php if ($this->_tpl_vars['data']['ms_email_3'] != ""): ?>
	<tr>
		<th colspan="2">メール3</th>
		<td><?php echo ((is_array($_tmp=$this->_tpl_vars['data']['ms_email_3'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
</td>
	</tr>
<?php endif; ?>
<?php if ($this->_tpl_vars['data']['ms_email_4'] != ""): ?>
	<tr>
		<th colspan="2">メール4</th>
		<td><?php echo ((is_array($_tmp=$this->_tpl_vars['data']['ms_email_4'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
</td>
	</tr>
<?php endif; ?>
<?php if ($this->_tpl_vars['data']['ms_email_5'] != ""): ?>
	<tr>
		<th colspan="2">メール5</th>
		<td><?php echo ((is_array($_tmp=$this->_tpl_vars['data']['ms_email_5'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
</td>
	</tr>
<?php endif; ?>
<?php if ($this->_tpl_vars['ms_image_path_1'] != ""): ?>
	<tr>
		<th colspan="2">イメージ1</th>
		<td><img src="<?php echo $this->_tpl_vars['ms_image_path_1']; ?>
" alt="イメージ1" /></td>
	</tr>
<?php endif; ?>
<?php if ($this->_tpl_vars['ms_image_path_2'] != ""): ?>
	<tr>
		<th colspan="2">イメージ2</th>
		<td><img src="<?php echo $this->_tpl_vars['ms_image_path_2']; ?>
" alt="イメージ2" /></td>
	</tr>
<?php endif; ?>
<?php if ($this->_tpl_vars['ms_image_path_3'] != ""): ?>
	<tr>
		<th colspan="2">イメージ3</th>
		<td><img src="<?php echo $this->_tpl_vars['ms_image_path_3']; ?>
" alt="イメージ3" /></td>
	</tr>
<?php endif; ?>
<?php if ($this->_tpl_vars['ms_image_path_4'] != ""): ?>
	<tr>
		<th colspan="2">イメージ4</th>
		<td><img src="<?php echo $this->_tpl_vars['ms_image_path_4']; ?>
" alt="イメージ4" /></td>
	</tr>
<?php endif; ?>
<?php if ($this->_tpl_vars['ms_image_path_5'] != ""): ?>
	<tr>
		<th colspan="2">イメージ5</th>
		<td><img src="<?php echo $this->_tpl_vars['ms_image_path_5']; ?>
" alt="イメージ5" /></td>
	</tr>
<?php endif; ?>
</table>
</div><!-- contents -->

<!-- フッター -->
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => './hooter.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>