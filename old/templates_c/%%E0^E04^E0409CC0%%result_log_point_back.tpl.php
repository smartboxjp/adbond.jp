<?php /* Smarty version 2.6.25, created on 2010-05-05 18:35:36
         compiled from ./result_log_point_back.tpl */ ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => './header.tpl', 'smarty_include_vars' => array('page_title' => '成果ログ')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<!-- Menu -->
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => './menu.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<div id="my_contents">

<h2>成果ログ</h2>

<div id=message>
<?php if ($this->_tpl_vars['error_message'] != ''): ?>
<div id="error_message">
	<h3>ERROR:<?php echo $this->_tpl_vars['error_message']; ?>
</h3>
</div>
<?php endif; ?>
<?php if ($this->_tpl_vars['info_message'] != ''): ?>
<div id="info_message">
	<h3>INFO:<?php echo $this->_tpl_vars['info_message']; ?>
</h3>
</div>
<?php endif; ?>
</div><!-- message -->

<table cellpadding="0" cellspacing="0">
	<tr>
		<th colspan="10">
			<input type="button" value="CSVダウンロード" onclick="window.open('./result_log_download.php?date=<?php echo $this->_tpl_vars['download_date']; ?>
&type=<?php echo $this->_tpl_vars['log_type']; ?>
')" />
		</th>
	</tr>
	<tr>
		<th>ポイントバック通知日時</th>
		<th>媒体ID</th>
		<th>広告ID</th>
		<th>ユーザ識別ID</th>
		<th>ポイントバック通知URL</th>
		<th>ステータス</th>
	</tr>
<?php $_from = $this->_tpl_vars['list']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['list'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['list']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['data']):
        $this->_foreach['list']['iteration']++;
?>
	<tr>
		<td><?php echo $this->_tpl_vars['data']['created_at']; ?>
</td>
		<td><?php echo $this->_tpl_vars['data']['media_id']; ?>
</td>
		<td><?php echo $this->_tpl_vars['data']['advert_id']; ?>
</td>
		<td><?php echo $this->_tpl_vars['data']['point_back_parameter']; ?>
</td>
		<td><?php echo $this->_tpl_vars['data']['point_back_url']; ?>
</td>
		<td>
<?php if ($this->_tpl_vars['data']['status'] == 1): ?>
			成功
<?php elseif ($this->_tpl_vars['data']['status'] == 2): ?>
			失敗
<?php endif; ?>
		</td>
	</tr>
<?php endforeach; endif; unset($_from); ?>
</table>

</div><!-- contents -->

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => './hooter.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>