<?php /* Smarty version 2.6.25, created on 2010-05-10 03:35:20
         compiled from ./media_info_account.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'htmlspecialchars', './media_info_account.tpl', 30, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => './header.tpl', 'smarty_include_vars' => array('page_title' => 'メディア管理')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<!-- Menu -->
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => './media_menu.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<div id="my_contents">

<h2>登録情報</h2>

<div id=message>
<?php if ($this->_tpl_vars['error_message'] != ''): ?>
<div id="error_message">
	<h3>ERROR:<?php echo $this->_tpl_vars['error_message']; ?>
</h3>
</div>
<?php endif; ?>
<?php if ($this->_tpl_vars['info_message'] != ''): ?>
<div id="info_message">
	<h3>INFO:<?php echo $this->_tpl_vars['info_message']; ?>
</h3>
</div>
<?php endif; ?>
</div><!-- message -->

<form method="POST" action="<?php echo $this->_supers['server']['PHP_SELF']; ?>
">
<table cellpadding="0" cellspacing="0">
	<tr>
		<th colspan="2">口座情報</th>
	</tr>
	<tr>
		<th>振込先銀行名</th>
		<td><input type="text" size="50" name="bank_name" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['data']['bank_name'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" /></td>
	</tr>
	<tr>
		<th>支店名</th>
		<td><input type="text" size="50" name="branch_name" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['data']['branch_name'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" /></td>
	</tr>
	<tr>
		<th>口座種別</th>
		<td>
			<input type="radio" name="account_type" value="1"<?php if ($this->_tpl_vars['data']['account_type'] == 1): ?> checked="checked"<?php endif; ?> /><label>普通</label>
			<input type="radio" name="account_type" value="2"<?php if ($this->_tpl_vars['data']['account_type'] == 2): ?> checked="checked"<?php endif; ?> /><label>当座</label>
		</td>
	</tr>
	<tr>
		<th>口座名義</th>
		<td><input type="text" size="50" name="account_holder" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['data']['account_holder'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" /></td>
	</tr>
	<tr>
		<th>口座番号</th>
		<td><input type="text" size="50" name="account_number" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['data']['account_number'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" /></td>
	</tr>
	<tr>
		<td colspan="2">
			<input type="submit" value="登録" />
			<input type="hidden" name="mode" value="account_update" />
		</td>
	</tr>
</table>
</form>
</div><!-- contents -->

<!-- フッター -->
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => './hooter.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>