<?php /* Smarty version 2.6.25, created on 2010-05-09 06:53:50
         compiled from ./summary_advert_client.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'htmlspecialchars', './summary_advert_client.tpl', 39, false),array('modifier', 'number_format', './summary_advert_client.tpl', 41, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => './header.tpl', 'smarty_include_vars' => array('page_title' => '広告主別集計')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<!-- Menu -->
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => './menu.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<div id="my_contents">

<h2>広告主別集計</h2>

<div id=message>
<?php if ($this->_tpl_vars['error_message'] != ''): ?>
<div id="error_message">
	<h3>ERROR:<?php echo $this->_tpl_vars['error_message']; ?>
</h3>
</div>
<?php endif; ?>
<?php if ($this->_tpl_vars['info_message'] != ''): ?>
<div id="info_message">
	<h3>INFO:<?php echo $this->_tpl_vars['info_message']; ?>
</h3>
</div>
<?php endif; ?>
</div><!-- message -->

<form method="POST" action="<?php echo $this->_supers['server']['PHP_SELF']; ?>
">
</form>

<table cellpadding="0" cellspacing="0">
	<tr>
		<th colspan="5">【該当<?php echo $this->_tpl_vars['list_count']; ?>
件】</th>
	</tr>
	<tr>
		<th>広告主名</th>
		<th>広告数</th>
		<th>クリック数</th>
		<th>アクション数</th>
		<th>金額合計</th>
	</tr>
<?php $_from = $this->_tpl_vars['list']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['list'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['list']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['data']):
        $this->_foreach['list']['iteration']++;
?>
	<tr>
		<td><a href="./advert_client.php?mode=edit&id=<?php echo $this->_tpl_vars['data']['advert_client_id']; ?>
"><?php echo ((is_array($_tmp=$this->_tpl_vars['data']['client_name'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
</a></td>
		<td><?php echo ((is_array($_tmp=$this->_tpl_vars['data']['advert_count'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
</td>
		<td><a href="./summary_click_detail.php?a_id=<?php echo $this->_tpl_vars['data']['advert_id']; ?>
&ac_id=<?php echo $this->_tpl_vars['data']['advert_client_id']; ?>
"><?php echo ((is_array($_tmp=$this->_tpl_vars['data']['click_count'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</a></td>
		<td><a href="./summary_action_detail.php?a_id=<?php echo $this->_tpl_vars['data']['advert_id']; ?>
&ac_id=<?php echo $this->_tpl_vars['data']['advert_client_id']; ?>
"><?php echo ((is_array($_tmp=$this->_tpl_vars['data']['action_count'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</a></td>
		<td>\<?php echo ((is_array($_tmp=$this->_tpl_vars['data']['total_price'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
	</tr>
<?php endforeach; endif; unset($_from); ?>
</table>

</div><!-- contents -->

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => './hooter.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>