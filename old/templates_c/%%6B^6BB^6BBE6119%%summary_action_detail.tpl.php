<?php /* Smarty version 2.6.25, created on 2010-05-17 02:42:09
         compiled from ./summary_action_detail.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'htmlspecialchars', './summary_action_detail.tpl', 47, false),array('modifier', 'number_format', './summary_action_detail.tpl', 54, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => './header.tpl', 'smarty_include_vars' => array('page_title' => '成果通知ログ詳細')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<!-- Menu -->
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => './menu.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<div id="my_contents">

<h2>成果通知ログ詳細</h2>

<div id=message>
<?php if ($this->_tpl_vars['error_message'] != ''): ?>
<div id="error_message">
	<h3>ERROR:<?php echo $this->_tpl_vars['error_message']; ?>
</h3>
</div>
<?php endif; ?>
<?php if ($this->_tpl_vars['info_message'] != ''): ?>
<div id="info_message">
	<h3>INFO:<?php echo $this->_tpl_vars['info_message']; ?>
</h3>
</div>
<?php endif; ?>
</div><!-- message -->

<form method="POST" action="<?php echo $this->_supers['server']['PHP_SELF']; ?>
">
</form>

<table cellpadding="0" cellspacing="0">
	<tr>
		<th colspan="12">【該当<?php echo $this->_tpl_vars['list_count']; ?>
件】</th>
	</tr>
	<tr>
		<th>アクション完了日時</th>
		<th>クリック日時</th>
		<th>媒体名</th>
		<th>広告名</th>
		<th>キャリア</th>
		<th>ユーザーエージェント</th>
		<th>個体識別番号</th>
		<th>IPアドレス</th>
		<th>ホスト名</th>
		<th>クリック単価</th>
		<th>アクション単価</th>
	</tr>
<?php $_from = $this->_tpl_vars['list']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['list'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['list']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['data']):
        $this->_foreach['list']['iteration']++;
?>
	<tr>
		<td><?php echo $this->_tpl_vars['data']['action_complete_date']; ?>
</td>
		<td><?php echo $this->_tpl_vars['data']['created_at']; ?>
</td>
		<td><a href="./media.php?mode=edit&id=<?php echo $this->_tpl_vars['data']['media_id']; ?>
"><?php echo ((is_array($_tmp=$this->_tpl_vars['data']['media_name'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
</a></td>
		<td><a href="./advert.php?mode=edit&id=<?php echo $this->_tpl_vars['data']['advert_id']; ?>
"><?php echo ((is_array($_tmp=$this->_tpl_vars['data']['advert_name'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
</a></td>
		<td><?php echo $this->_tpl_vars['data']['carrier']; ?>
</td>
		<td><?php echo ((is_array($_tmp=$this->_tpl_vars['data']['user_agent'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
</td>
		<td><?php echo ((is_array($_tmp=$this->_tpl_vars['data']['uid'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
</td>
		<td><?php echo $this->_tpl_vars['data']['ip_address']; ?>
</td>
		<td><?php echo $this->_tpl_vars['data']['host_name']; ?>
</td>
		<td>&yen;<?php echo ((is_array($_tmp=$this->_tpl_vars['data']['click_price_client'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
		<td>&yen;<?php echo ((is_array($_tmp=$this->_tpl_vars['data']['action_price_client'])) ? $this->_run_mod_handler('number_format', true, $_tmp) : number_format($_tmp)); ?>
</td>
	</tr>
<?php endforeach; endif; unset($_from); ?>
</table>

</div><!-- contents -->

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => './hooter.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>