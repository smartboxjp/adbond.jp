<?php /* Smarty version 2.6.25, created on 2010-05-05 18:34:47
         compiled from ./result_log_action.tpl */ ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => './header.tpl', 'smarty_include_vars' => array('page_title' => '成果ログ')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<!-- Menu -->
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => './menu.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<div id="my_contents">

<h2>成果ログ</h2>

<div id=message>
<?php if ($this->_tpl_vars['error_message'] != ''): ?>
<div id="error_message">
	<h3>ERROR:<?php echo $this->_tpl_vars['error_message']; ?>
</h3>
</div>
<?php endif; ?>
<?php if ($this->_tpl_vars['info_message'] != ''): ?>
<div id="info_message">
	<h3>INFO:<?php echo $this->_tpl_vars['info_message']; ?>
</h3>
</div>
<?php endif; ?>
</div><!-- message -->

<table cellpadding="0" cellspacing="0">
	<tr>
		<th colspan="10">
			<input type="button" value="CSVダウンロード" onclick="window.open('./result_log_download.php?date=<?php echo $this->_tpl_vars['download_date']; ?>
&type=<?php echo $this->_tpl_vars['log_type']; ?>
')" />
		</th>
	</tr>
	<tr>
		<th>アクション完了日時</th>
		<th>媒体ID</th>
		<th>広告ID</th>
		<th>キャリア</th>
		<th>ユーザーエージェント</th>
		<th>個体識別番号</th>
		<th>IPアドレス</th>
		<th>ホスト名</th>
		<th>ステータス</th>
	</tr>
<?php $_from = $this->_tpl_vars['list']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['list'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['list']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['data']):
        $this->_foreach['list']['iteration']++;
?>
	<tr>
		<td><?php echo $this->_tpl_vars['data']['action_complete_date']; ?>
</td>
		<td><?php echo $this->_tpl_vars['data']['media_id']; ?>
</td>
		<td><?php echo $this->_tpl_vars['data']['advert_id']; ?>
</td>
		<td>
<?php if ($this->_tpl_vars['data']['carrier_id'] == 1): ?>
			docomo
<?php elseif ($this->_tpl_vars['data']['carrier_id'] == 2): ?>
			softbank
<?php elseif ($this->_tpl_vars['data']['carrier_id'] == 3): ?>
			au
<?php elseif ($this->_tpl_vars['data']['carrier_id'] == 4): ?>
			pc
<?php endif; ?>
		</td>
		<td><?php echo $this->_tpl_vars['data']['user_agent']; ?>
</td>
		<td><?php echo $this->_tpl_vars['data']['uid']; ?>
</td>
		<td><?php echo $this->_tpl_vars['data']['ip_address']; ?>
</td>
		<td><?php echo $this->_tpl_vars['data']['host_name']; ?>
</td>
		<td>
<?php if ($this->_tpl_vars['data']['status'] == 1): ?>
			クリック
<?php elseif ($this->_tpl_vars['data']['status'] == 2): ?>
			登録
<?php endif; ?>
		</td>
	</tr>
<?php endforeach; endif; unset($_from); ?>
</table>

</div><!-- contents -->

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => './hooter.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>