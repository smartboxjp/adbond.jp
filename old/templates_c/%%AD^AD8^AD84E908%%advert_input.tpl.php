<?php /* Smarty version 2.6.25, created on 2010-05-11 05:00:30
         compiled from ./advert_input.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'cat', './advert_input.tpl', 1, false),array('modifier', 'htmlspecialchars', './advert_input.tpl', 48, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => './header.tpl', 'smarty_include_vars' => array('page_title' => ((is_array($_tmp=((is_array($_tmp='広告(')) ? $this->_run_mod_handler('cat', true, $_tmp, $this->_tpl_vars['sub_title']) : smarty_modifier_cat($_tmp, $this->_tpl_vars['sub_title'])))) ? $this->_run_mod_handler('cat', true, $_tmp, ')') : smarty_modifier_cat($_tmp, ')')))));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<!-- Menu -->
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => './menu.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<div id="my_contents">

<h2>広告(<?php echo $this->_tpl_vars['sub_title']; ?>
)</h2>

<div id=message>
<?php if ($this->_tpl_vars['error_message'] != ''): ?>
<div id="error_message">
<h3>ERROR:<?php echo $this->_tpl_vars['error_message']; ?>
</h3>
</div>
<?php endif; ?>
<?php if ($this->_tpl_vars['info_message'] != ''): ?>
<div id="info_message">
<h3>INFO:<?php echo $this->_tpl_vars['info_message']; ?>
</h3>
</div>
<?php endif; ?>
</div><!-- message -->

<?php if ($this->_tpl_vars['form_data']['id'] != ""): ?>
<table cellpadding="0" cellspacing="0">
	<tr>
		<td>
			<form method="POST" action="./advert_view_setup.php">
				<input type="submit" value="表示広告設定" />
				<input type="hidden" name="advert_id" value="<?php echo $this->_tpl_vars['form_data']['id']; ?>
" />
			</form>
		</td>
		<td>
			<form method="POST" action="./advert_price_setup.php">
				<input type="submit" value="媒体別単価設定" />
				<input type="hidden" name="advert_id" value="<?php echo $this->_tpl_vars['form_data']['id']; ?>
" />
			</form>
		</td>
	</tr>
</table>
<?php endif; ?>
<form method="POST" action="<?php echo $this->_supers['server']['PHP_SELF']; ?>
" enctype="multipart/form-data">
<table cellpadding="0" cellspacing="0">
	<tr>
		<th colspan="2">広告主</th>
		<td>
			<select name="advert_client_id">
			<?php $_from = $this->_tpl_vars['advert_client_array']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['advert_client_list'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['advert_client_list']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['data']):
        $this->_foreach['advert_client_list']['iteration']++;
?>
				<option value="<?php echo $this->_tpl_vars['data']['id']; ?>
"<?php if ($this->_tpl_vars['form_data']['advert_client_id'] == $this->_tpl_vars['data']['id']): ?> selected="selected"<?php endif; ?>><?php echo ((is_array($_tmp=$this->_tpl_vars['data']['name'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
</option>
			<?php endforeach; endif; unset($_from); ?>
			</select>
		</td>
	</tr>
	<tr>
		<th colspan="2">広告カテゴリー</th>
		<td>
			<select name="advert_category_id">
			<?php $_from = $this->_tpl_vars['advert_category_array']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['advert_category_list'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['advert_category_list']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['data']):
        $this->_foreach['advert_category_list']['iteration']++;
?>
				<option value="<?php echo $this->_tpl_vars['data']['id']; ?>
"<?php if ($this->_tpl_vars['form_data']['advert_category_id'] == $this->_tpl_vars['data']['id']): ?> selected="selected"<?php endif; ?>><?php echo ((is_array($_tmp=$this->_tpl_vars['data']['name'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
</option>
			<?php endforeach; endif; unset($_from); ?>
			</select>
		</td>
	</tr>
	<tr>
		<th colspan="2">広告名</th>
		<td>
			<input type="text" size="50" name="advert_name" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['advert_name'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />
		</td>
	</tr>
	<tr>
		<th colspan="2">コンテンツ種別</th>
		<td>
			<input type="radio" name="content_type" value="1"<?php if ($this->_tpl_vars['form_data']['content_type'] == 1 || $this->_tpl_vars['form_data']['content_type'] == ""): ?> checked="checked"<?php endif; ?> /><label>一般</label>
			<input type="radio" name="content_type" value="2"<?php if ($this->_tpl_vars['form_data']['content_type'] == 2): ?> checked="checked"<?php endif; ?> /><label>公式</label>
		</td>
	</tr>
	<tr>
		<th colspan="2">対応キャリア</th>
		<td>
			<input type="checkbox" name="support_docomo" value="1"<?php if ($this->_tpl_vars['form_data']['support_docomo'] == 1): ?> checked="checked"<?php endif; ?> /><label>docomo</label>
			<input type="checkbox" name="support_softbank" value="1"<?php if ($this->_tpl_vars['form_data']['support_softbank'] == 1): ?> checked="checked"<?php endif; ?> /><label>softbank</label>
			<input type="checkbox" name="support_au" value="1"<?php if ($this->_tpl_vars['form_data']['support_au'] == 1): ?> checked="checked"<?php endif; ?> /><label>au</label>
			<input type="checkbox" name="support_pc" value="1"<?php if ($this->_tpl_vars['form_data']['support_pc'] == 1): ?> checked="checked"<?php endif; ?> /><label>pc</label>
		</td>
	</tr>
	<tr>
		<th colspan="2">サイトURL(docomo)</th>
		<td>
			<input type="text" size="100" name="site_url_docomo" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['site_url_docomo'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />
		</td>
	</tr>
	<tr>
		<th colspan="2">サイトURL(softbank)</th>
		<td>
			<input type="text" size="100" name="site_url_softbank" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['site_url_softbank'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />
		</td>
	</tr>
	<tr>
		<th colspan="2">サイトURL(au)</th>
		<td>
			<input type="text" size="100" name="site_url_au" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['site_url_au'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />
		</td>
	</tr>
	<tr>
		<th colspan="2">サイトURL(pc)</th>
		<td>
			<input type="text" size="100" name="site_url_pc" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['site_url_pc'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />
		</td>
	</tr>
	<tr>
		<th colspan="2">サイトURLについて</th>
		<td>
			＜セッションIDの付加について＞<br />
			サイトURLに?が含まれない場合「?bid=」で挿入される<br />
			http://example.jp/→http://example.jp/?bid=***<br />
			サイトURLに?が含まれる場合「&bid=」で挿入される<br />
			http://example.jp/?a=b→http://example.jp/?a=b&bid=***<br />
		</td>
	</tr>
	<tr>
		<th colspan="2">広告概要</th>
		<td>
			<textarea cols="70" rows="5" name="site_outline"><?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['site_outline'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
</textarea>
		</td>
	</tr>
	<tr>
		<th colspan="2">クリック単価(クライアント)</th>
		<td>
			<input type="text" size="10" name="click_price_client" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['click_price_client'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />円
		</td>
	</tr>
	<tr>
		<th colspan="2">クリック単価(メディア)</th>
		<td>
			<input type="text" size="10" name="click_price_media" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['click_price_media'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />円
		</td>
	</tr>
	<tr>
		<th colspan="2">アクション単価(金額)(クライアント)</th>
		<td>
			[成果1]
			docomo:<input type="text" size="10" name="action_price_client_docomo_1" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['action_price_client_docomo_1'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />円
			softbank:<input type="text" size="10" name="action_price_client_softbank_1" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['action_price_client_softbank_1'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />円
			au:<input type="text" size="10" name="action_price_client_au_1" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['action_price_client_au_1'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />円
			pc:<input type="text" size="10" name="action_price_client_pc_1" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['action_price_client_pc_1'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />円<br />
			[成果2]
			docomo:<input type="text" size="10" name="action_price_client_docomo_2" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['action_price_client_docomo_2'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />円
			softbank:<input type="text" size="10" name="action_price_client_softbank_2" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['action_price_client_softbank_2'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />円
			au:<input type="text" size="10" name="action_price_client_au_2" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['action_price_client_au_2'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />円
			pc:<input type="text" size="10" name="action_price_client_pc_2" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['action_price_client_pc_2'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />円<br />
			[成果3]
			docomo:<input type="text" size="10" name="action_price_client_docomo_3" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['action_price_client_docomo_3'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />円
			softbank:<input type="text" size="10" name="action_price_client_softbank_3" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['action_price_client_softbank_3'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />円
			au:<input type="text" size="10" name="action_price_client_au_3" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['action_price_client_au_3'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />円
			pc:<input type="text" size="10" name="action_price_client_pc_3" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['action_price_client_pc_3'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />円<br />
			[成果4]
			docomo:<input type="text" size="10" name="action_price_client_docomo_4" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['action_price_client_docomo_4'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />円
			softbank:<input type="text" size="10" name="action_price_client_softbank_4" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['action_price_client_softbank_4'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />円
			au:<input type="text" size="10" name="action_price_client_au_4" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['action_price_client_au_4'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />円
			pc:<input type="text" size="10" name="action_price_client_pc_4" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['action_price_client_pc_4'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />円<br />
			[成果5]
			docomo:<input type="text" size="10" name="action_price_client_docomo_5" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['action_price_client_docomo_5'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />円
			softbank:<input type="text" size="10" name="action_price_client_softbank_5" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['action_price_client_softbank_5'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />円
			au:<input type="text" size="10" name="action_price_client_au_5" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['action_price_client_au_5'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />円
			pc:<input type="text" size="10" name="action_price_client_pc_5" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['action_price_client_pc_5'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />円<br />
		</td>
	</tr>
	<tr>
		<th colspan="2">アクション単価(金額)(メディア)</th>
		<td>
			[成果1]
			docomo:<input type="text" size="10" name="action_price_media_docomo_1" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['action_price_media_docomo_1'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />円
			softbank:<input type="text" size="10" name="action_price_media_softbank_1" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['action_price_media_softbank_1'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />円
			au:<input type="text" size="10" name="action_price_media_au_1" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['action_price_media_au_1'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />円
			pc:<input type="text" size="10" name="action_price_media_pc_1" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['action_price_media_pc_1'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />円<br />
			[成果2]
			docomo:<input type="text" size="10" name="action_price_media_docomo_2" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['action_price_media_docomo_2'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />円
			softbank:<input type="text" size="10" name="action_price_media_softbank_2" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['action_price_media_softbank_2'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />円
			au:<input type="text" size="10" name="action_price_media_au_2" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['action_price_media_au_2'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />円
			pc:<input type="text" size="10" name="action_price_media_pc_2" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['action_price_media_pc_2'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />円<br />
			[成果3]
			docomo:<input type="text" size="10" name="action_price_media_docomo_3" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['action_price_media_docomo_3'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />円
			softbank:<input type="text" size="10" name="action_price_media_softbank_3" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['action_price_media_softbank_3'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />円
			au:<input type="text" size="10" name="action_price_media_au_3" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['action_price_media_au_3'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />円
			pc:<input type="text" size="10" name="action_price_media_pc_3" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['action_price_media_pc_3'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />円<br />
			[成果4]
			docomo:<input type="text" size="10" name="action_price_media_docomo_4" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['action_price_media_docomo_4'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />円
			softbank:<input type="text" size="10" name="action_price_media_softbank_4" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['action_price_media_softbank_4'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />円
			au:<input type="text" size="10" name="action_price_media_au_4" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['action_price_media_au_4'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />円
			pc:<input type="text" size="10" name="action_price_media_pc_4" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['action_price_media_pc_4'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />円<br />
			[成果5]
			docomo:<input type="text" size="10" name="action_price_media_docomo_5" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['action_price_media_docomo_5'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />円
			softbank:<input type="text" size="10" name="action_price_media_softbank_5" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['action_price_media_softbank_5'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />円
			au:<input type="text" size="10" name="action_price_media_au_5" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['action_price_media_au_5'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />円
			pc:<input type="text" size="10" name="action_price_media_pc_5" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['action_price_media_pc_5'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />円<br />
		</td>
	</tr>
	<tr>
		<th colspan="2">広告原稿(テキスト1)</th>
		<td>
			<textarea cols="70" rows="2" name="ms_text_1"><?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['ms_text_1'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
</textarea>
		</td>
	</tr>
	<tr>
		<th colspan="2">広告原稿(メール1)</th>
		<td>
			<textarea cols="70" rows="2" name="ms_email_1"><?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['ms_email_1'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
</textarea>
		</td>
	</tr>
	<tr>
		<th colspan="2">広告原稿(イメージ1)</th>
		<td>
<?php if ($this->_tpl_vars['ms_image_path_1'] != ""): ?>
			<img src="<?php echo $this->_tpl_vars['ms_image_path_1']; ?>
" alt="イメージ1" /><br />
<?php endif; ?>
			<input type="radio" name="ms_image_type_1" value="1"<?php if ($this->_tpl_vars['form_data']['ms_image_type_1'] == 1 || $this->_tpl_vars['form_data']['ms_image_type_1'] == ""): ?> checked="checked"<?php endif; ?> /><label>アップロード</label><input type="file" size="30" name="ms_image_file_1" /><br/>
			<input type="radio" name="ms_image_type_1" value="2"<?php if ($this->_tpl_vars['form_data']['ms_image_type_1'] == 2): ?> checked="checked"<?php endif; ?> /><label>URL指定</label><input type="text" size="30" name="ms_image_url_1" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['ms_image_url_1'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />
		</td>
	</tr>
	<tr>
		<th colspan="2">広告原稿(テキスト2)</th>
		<td>
			<textarea cols="70" rows="2" name="ms_text_2"><?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['ms_text_2'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
</textarea>
		</td>
	</tr>
	<tr>
		<th colspan="2">広告原稿(メール2)</th>
		<td>
			<textarea cols="70" rows="2" name="ms_email_2"><?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['ms_email_2'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
</textarea>
		</td>
	</tr>
	<tr>
		<th colspan="2">広告原稿(イメージ2)</th>
		<td>
<?php if ($this->_tpl_vars['ms_image_path_2'] != ""): ?>
			<img src="<?php echo $this->_tpl_vars['ms_image_path_2']; ?>
" alt="イメージ2" /><br />
<?php endif; ?>
			<input type="radio" name="ms_image_type_2" value="1"<?php if ($this->_tpl_vars['form_data']['ms_image_type_2'] == 1 || $this->_tpl_vars['form_data']['ms_image_type_2'] == ""): ?> checked="checked"<?php endif; ?> /><label>アップロード</label><input type="file" size="30" name="ms_image_file_2" /><br/>
			<input type="radio" name="ms_image_type_2" value="2"<?php if ($this->_tpl_vars['form_data']['ms_image_type_2'] == 2): ?> checked="checked"<?php endif; ?> /><label>URL指定</label><input type="text" size="30" name="ms_image_url_2" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['ms_image_url_2'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />
		</td>
	</tr>
	<tr>
		<th colspan="2">広告原稿(テキスト3)</th>
		<td>
			<textarea cols="70" rows="2" name="ms_text_3"><?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['ms_text_3'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
</textarea>
		</td>
	</tr>
	<tr>
		<th colspan="2">広告原稿(メール3)</th>
		<td>
			<textarea cols="70" rows="2" name="ms_email_3"><?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['ms_email_3'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
</textarea>
		</td>
	</tr>
	<tr>
		<th colspan="2">広告原稿(イメージ3)</th>
		<td>
<?php if ($this->_tpl_vars['ms_image_path_3'] != ""): ?>
			<img src="<?php echo $this->_tpl_vars['ms_image_path_3']; ?>
" alt="イメージ3" /><br />
<?php endif; ?>
			<input type="radio" name="ms_image_type_3" value="1"<?php if ($this->_tpl_vars['form_data']['ms_image_type_3'] == 1 || $this->_tpl_vars['form_data']['ms_image_type_3'] == ""): ?> checked="checked"<?php endif; ?> /><label>アップロード</label><input type="file" size="30" name="ms_image_file_3" /><br/>
			<input type="radio" name="ms_image_type_3" value="2"<?php if ($this->_tpl_vars['form_data']['ms_image_type_3'] == 2): ?> checked="checked"<?php endif; ?> /><label>URL指定</label><input type="text" size="30" name="ms_image_url_3" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['ms_image_url_3'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />
		</td>
	</tr>
	<tr>
		<th colspan="2">広告原稿(テキスト4)</th>
		<td>
			<textarea cols="70" rows="2" name="ms_text_4"><?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['ms_text_4'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
</textarea>
		</td>
	</tr>
	<tr>
		<th colspan="2">広告原稿(メール4)</th>
		<td>
			<textarea cols="70" rows="2" name="ms_email_4"><?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['ms_email_4'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
</textarea>
		</td>
	</tr>
	<tr>
		<th colspan="2">広告原稿(イメージ4)</th>
		<td>
<?php if ($this->_tpl_vars['ms_image_path_4'] != ""): ?>
			<img src="<?php echo $this->_tpl_vars['ms_image_path_4']; ?>
" alt="イメージ4" /><br />
<?php endif; ?>
			<input type="radio" name="ms_image_type_4" value="1"<?php if ($this->_tpl_vars['form_data']['ms_image_type_4'] == 1 || $this->_tpl_vars['form_data']['ms_image_type_4'] == ""): ?> checked="checked"<?php endif; ?> /><label>アップロード</label><input type="file" size="30" name="ms_image_file_4" /><br/>
			<input type="radio" name="ms_image_type_4" value="2"<?php if ($this->_tpl_vars['form_data']['ms_image_type_4'] == 2): ?> checked="checked"<?php endif; ?> /><label>URL指定</label><input type="text" size="30" name="ms_image_url_4" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['ms_image_url_4'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />
		</td>
	</tr>
	<tr>
		<th colspan="2">広告原稿(テキスト5)</th>
		<td>
			<textarea cols="70" rows="2" name="ms_text_5"><?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['ms_text_5'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
</textarea>
		</td>
	</tr>
	<tr>
		<th colspan="2">広告原稿(メール5)</th>
		<td>
			<textarea cols="70" rows="2" name="ms_email_5"><?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['ms_email_5'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
</textarea>
		</td>
	</tr>
	<tr>
		<th colspan="2">広告原稿(イメージ5)</th>
		<td>
<?php if ($this->_tpl_vars['ms_image_path_5'] != ""): ?>
			<img src="<?php echo $this->_tpl_vars['ms_image_path_5']; ?>
" alt="イメージ5" /><br />
<?php endif; ?>
			<input type="radio" name="ms_image_type_5" value="1"<?php if ($this->_tpl_vars['form_data']['ms_image_type_5'] == 1 || $this->_tpl_vars['form_data']['ms_image_type_5'] == ""): ?> checked="checked"<?php endif; ?> /><label>アップロード</label><input type="file" size="30" name="ms_image_file_5" /><br/>
			<input type="radio" name="ms_image_type_5" value="2"<?php if ($this->_tpl_vars['form_data']['ms_image_type_5'] == 2): ?> checked="checked"<?php endif; ?> /><label>URL指定</label><input type="text" size="30" name="ms_image_url_5" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['ms_image_url_5'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />
		</td>
	</tr>
	<tr>
		<th colspan="2">ユニーククリック種別</th>
		<td>
			<input type="radio" name="unique_click_type" value="1"<?php if ($this->_tpl_vars['form_data']['unique_click_type'] == 1 || $this->_tpl_vars['form_data']['unique_click_type'] == ""): ?> checked="checked"<?php endif; ?> /><label>マンスリー</label>
			<input type="radio" name="unique_click_type" value="2"<?php if ($this->_tpl_vars['form_data']['unique_click_type'] == 2): ?> checked="checked"<?php endif; ?> /><label>ウィークリー</label>
			<input type="radio" name="unique_click_type" value="3"<?php if ($this->_tpl_vars['form_data']['unique_click_type'] == 3): ?> checked="checked"<?php endif; ?> /><label>デイリー</label>
			<input type="radio" name="unique_click_type" value="4"<?php if ($this->_tpl_vars['form_data']['unique_click_type'] == 4): ?> checked="checked"<?php endif; ?> /><label>全てユニーク</label>
		</td>
	</tr>
	<tr>
		<th colspan="2">ポイントバック</th>
		<td>
			<input type="radio" name="point_back_flag" value="1"<?php if ($this->_tpl_vars['form_data']['point_back_flag'] == 1 || $this->_tpl_vars['form_data']['point_back_flag'] == ""): ?> checked="checked"<?php endif; ?> /><label>不可</label>
			<input type="radio" name="point_back_flag" value="2"<?php if ($this->_tpl_vars['form_data']['point_back_flag'] == 2): ?> checked="checked"<?php endif; ?> /><label>可</label>
		</td>
	</tr>
	<tr>
		<th colspan="2">アダルト</th>
		<td>
			<input type="radio" name="adult_flag" value="1"<?php if ($this->_tpl_vars['form_data']['adult_flag'] == 1 || $this->_tpl_vars['form_data']['adult_flag'] == ""): ?> checked="checked"<?php endif; ?> /><label>不可</label>
			<input type="radio" name="adult_flag" value="2"<?php if ($this->_tpl_vars['form_data']['adult_flag'] == 2): ?> checked="checked"<?php endif; ?> /><label>可</label>
		</td>
	</tr>
	<tr>
		<th colspan="2">出会い</th>
		<td>
			<input type="radio" name="dating_flag" value="1"<?php if ($this->_tpl_vars['form_data']['dating_flag'] == 1 || $this->_tpl_vars['form_data']['dating_flag'] == ""): ?> checked="checked"<?php endif; ?> /><label>不可</label>
			<input type="radio" name="dating_flag" value="2"<?php if ($this->_tpl_vars['form_data']['dating_flag'] == 2): ?> checked="checked"<?php endif; ?> /><label>可</label>
		</td>
	</tr>
	<tr>
		<th colspan="2">出稿開始日</th>
		<td>
			<select name="as_year">
<?php unset($this->_sections['cnt']);
$this->_sections['cnt']['name'] = 'cnt';
$this->_sections['cnt']['start'] = (int)2009;
$this->_sections['cnt']['loop'] = is_array($_loop=2021) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['cnt']['show'] = true;
$this->_sections['cnt']['max'] = $this->_sections['cnt']['loop'];
$this->_sections['cnt']['step'] = 1;
if ($this->_sections['cnt']['start'] < 0)
    $this->_sections['cnt']['start'] = max($this->_sections['cnt']['step'] > 0 ? 0 : -1, $this->_sections['cnt']['loop'] + $this->_sections['cnt']['start']);
else
    $this->_sections['cnt']['start'] = min($this->_sections['cnt']['start'], $this->_sections['cnt']['step'] > 0 ? $this->_sections['cnt']['loop'] : $this->_sections['cnt']['loop']-1);
if ($this->_sections['cnt']['show']) {
    $this->_sections['cnt']['total'] = min(ceil(($this->_sections['cnt']['step'] > 0 ? $this->_sections['cnt']['loop'] - $this->_sections['cnt']['start'] : $this->_sections['cnt']['start']+1)/abs($this->_sections['cnt']['step'])), $this->_sections['cnt']['max']);
    if ($this->_sections['cnt']['total'] == 0)
        $this->_sections['cnt']['show'] = false;
} else
    $this->_sections['cnt']['total'] = 0;
if ($this->_sections['cnt']['show']):

            for ($this->_sections['cnt']['index'] = $this->_sections['cnt']['start'], $this->_sections['cnt']['iteration'] = 1;
                 $this->_sections['cnt']['iteration'] <= $this->_sections['cnt']['total'];
                 $this->_sections['cnt']['index'] += $this->_sections['cnt']['step'], $this->_sections['cnt']['iteration']++):
$this->_sections['cnt']['rownum'] = $this->_sections['cnt']['iteration'];
$this->_sections['cnt']['index_prev'] = $this->_sections['cnt']['index'] - $this->_sections['cnt']['step'];
$this->_sections['cnt']['index_next'] = $this->_sections['cnt']['index'] + $this->_sections['cnt']['step'];
$this->_sections['cnt']['first']      = ($this->_sections['cnt']['iteration'] == 1);
$this->_sections['cnt']['last']       = ($this->_sections['cnt']['iteration'] == $this->_sections['cnt']['total']);
?>
				<option value="<?php echo $this->_sections['cnt']['index']; ?>
"<?php if ($this->_tpl_vars['set_as_year'] == $this->_sections['cnt']['index']): ?> selected="selected"<?php endif; ?>><?php echo $this->_sections['cnt']['index']; ?>
年</option>
<?php endfor; endif; ?>
			</select>
			<select name="as_month">
<?php unset($this->_sections['cnt']);
$this->_sections['cnt']['name'] = 'cnt';
$this->_sections['cnt']['start'] = (int)1;
$this->_sections['cnt']['loop'] = is_array($_loop=13) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['cnt']['show'] = true;
$this->_sections['cnt']['max'] = $this->_sections['cnt']['loop'];
$this->_sections['cnt']['step'] = 1;
if ($this->_sections['cnt']['start'] < 0)
    $this->_sections['cnt']['start'] = max($this->_sections['cnt']['step'] > 0 ? 0 : -1, $this->_sections['cnt']['loop'] + $this->_sections['cnt']['start']);
else
    $this->_sections['cnt']['start'] = min($this->_sections['cnt']['start'], $this->_sections['cnt']['step'] > 0 ? $this->_sections['cnt']['loop'] : $this->_sections['cnt']['loop']-1);
if ($this->_sections['cnt']['show']) {
    $this->_sections['cnt']['total'] = min(ceil(($this->_sections['cnt']['step'] > 0 ? $this->_sections['cnt']['loop'] - $this->_sections['cnt']['start'] : $this->_sections['cnt']['start']+1)/abs($this->_sections['cnt']['step'])), $this->_sections['cnt']['max']);
    if ($this->_sections['cnt']['total'] == 0)
        $this->_sections['cnt']['show'] = false;
} else
    $this->_sections['cnt']['total'] = 0;
if ($this->_sections['cnt']['show']):

            for ($this->_sections['cnt']['index'] = $this->_sections['cnt']['start'], $this->_sections['cnt']['iteration'] = 1;
                 $this->_sections['cnt']['iteration'] <= $this->_sections['cnt']['total'];
                 $this->_sections['cnt']['index'] += $this->_sections['cnt']['step'], $this->_sections['cnt']['iteration']++):
$this->_sections['cnt']['rownum'] = $this->_sections['cnt']['iteration'];
$this->_sections['cnt']['index_prev'] = $this->_sections['cnt']['index'] - $this->_sections['cnt']['step'];
$this->_sections['cnt']['index_next'] = $this->_sections['cnt']['index'] + $this->_sections['cnt']['step'];
$this->_sections['cnt']['first']      = ($this->_sections['cnt']['iteration'] == 1);
$this->_sections['cnt']['last']       = ($this->_sections['cnt']['iteration'] == $this->_sections['cnt']['total']);
?>
				<option value="<?php echo $this->_sections['cnt']['index']; ?>
"<?php if ($this->_tpl_vars['set_as_month'] == $this->_sections['cnt']['index']): ?> selected="selected"<?php endif; ?>><?php echo $this->_sections['cnt']['index']; ?>
月</option>
<?php endfor; endif; ?>
			</select>
			<select name="as_day">
<?php unset($this->_sections['cnt']);
$this->_sections['cnt']['name'] = 'cnt';
$this->_sections['cnt']['start'] = (int)1;
$this->_sections['cnt']['loop'] = is_array($_loop=32) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['cnt']['show'] = true;
$this->_sections['cnt']['max'] = $this->_sections['cnt']['loop'];
$this->_sections['cnt']['step'] = 1;
if ($this->_sections['cnt']['start'] < 0)
    $this->_sections['cnt']['start'] = max($this->_sections['cnt']['step'] > 0 ? 0 : -1, $this->_sections['cnt']['loop'] + $this->_sections['cnt']['start']);
else
    $this->_sections['cnt']['start'] = min($this->_sections['cnt']['start'], $this->_sections['cnt']['step'] > 0 ? $this->_sections['cnt']['loop'] : $this->_sections['cnt']['loop']-1);
if ($this->_sections['cnt']['show']) {
    $this->_sections['cnt']['total'] = min(ceil(($this->_sections['cnt']['step'] > 0 ? $this->_sections['cnt']['loop'] - $this->_sections['cnt']['start'] : $this->_sections['cnt']['start']+1)/abs($this->_sections['cnt']['step'])), $this->_sections['cnt']['max']);
    if ($this->_sections['cnt']['total'] == 0)
        $this->_sections['cnt']['show'] = false;
} else
    $this->_sections['cnt']['total'] = 0;
if ($this->_sections['cnt']['show']):

            for ($this->_sections['cnt']['index'] = $this->_sections['cnt']['start'], $this->_sections['cnt']['iteration'] = 1;
                 $this->_sections['cnt']['iteration'] <= $this->_sections['cnt']['total'];
                 $this->_sections['cnt']['index'] += $this->_sections['cnt']['step'], $this->_sections['cnt']['iteration']++):
$this->_sections['cnt']['rownum'] = $this->_sections['cnt']['iteration'];
$this->_sections['cnt']['index_prev'] = $this->_sections['cnt']['index'] - $this->_sections['cnt']['step'];
$this->_sections['cnt']['index_next'] = $this->_sections['cnt']['index'] + $this->_sections['cnt']['step'];
$this->_sections['cnt']['first']      = ($this->_sections['cnt']['iteration'] == 1);
$this->_sections['cnt']['last']       = ($this->_sections['cnt']['iteration'] == $this->_sections['cnt']['total']);
?>
				<option value="<?php echo $this->_sections['cnt']['index']; ?>
"<?php if ($this->_tpl_vars['set_as_day'] == $this->_sections['cnt']['index']): ?> selected="selected"<?php endif; ?>><?php echo $this->_sections['cnt']['index']; ?>
日</option>
<?php endfor; endif; ?>
			</select>
		</td>
	</tr>
	<tr>
		<th colspan="2">出稿終了日</th>
		<td>
			<select name="ae_year">
<?php unset($this->_sections['cnt']);
$this->_sections['cnt']['name'] = 'cnt';
$this->_sections['cnt']['start'] = (int)2009;
$this->_sections['cnt']['loop'] = is_array($_loop=2021) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['cnt']['show'] = true;
$this->_sections['cnt']['max'] = $this->_sections['cnt']['loop'];
$this->_sections['cnt']['step'] = 1;
if ($this->_sections['cnt']['start'] < 0)
    $this->_sections['cnt']['start'] = max($this->_sections['cnt']['step'] > 0 ? 0 : -1, $this->_sections['cnt']['loop'] + $this->_sections['cnt']['start']);
else
    $this->_sections['cnt']['start'] = min($this->_sections['cnt']['start'], $this->_sections['cnt']['step'] > 0 ? $this->_sections['cnt']['loop'] : $this->_sections['cnt']['loop']-1);
if ($this->_sections['cnt']['show']) {
    $this->_sections['cnt']['total'] = min(ceil(($this->_sections['cnt']['step'] > 0 ? $this->_sections['cnt']['loop'] - $this->_sections['cnt']['start'] : $this->_sections['cnt']['start']+1)/abs($this->_sections['cnt']['step'])), $this->_sections['cnt']['max']);
    if ($this->_sections['cnt']['total'] == 0)
        $this->_sections['cnt']['show'] = false;
} else
    $this->_sections['cnt']['total'] = 0;
if ($this->_sections['cnt']['show']):

            for ($this->_sections['cnt']['index'] = $this->_sections['cnt']['start'], $this->_sections['cnt']['iteration'] = 1;
                 $this->_sections['cnt']['iteration'] <= $this->_sections['cnt']['total'];
                 $this->_sections['cnt']['index'] += $this->_sections['cnt']['step'], $this->_sections['cnt']['iteration']++):
$this->_sections['cnt']['rownum'] = $this->_sections['cnt']['iteration'];
$this->_sections['cnt']['index_prev'] = $this->_sections['cnt']['index'] - $this->_sections['cnt']['step'];
$this->_sections['cnt']['index_next'] = $this->_sections['cnt']['index'] + $this->_sections['cnt']['step'];
$this->_sections['cnt']['first']      = ($this->_sections['cnt']['iteration'] == 1);
$this->_sections['cnt']['last']       = ($this->_sections['cnt']['iteration'] == $this->_sections['cnt']['total']);
?>
				<option value="<?php echo $this->_sections['cnt']['index']; ?>
"<?php if ($this->_tpl_vars['set_ae_year'] == $this->_sections['cnt']['index']): ?> selected="selected"<?php endif; ?>><?php echo $this->_sections['cnt']['index']; ?>
年</option>
<?php endfor; endif; ?>
			</select>
			<select name="ae_month">
<?php unset($this->_sections['cnt']);
$this->_sections['cnt']['name'] = 'cnt';
$this->_sections['cnt']['start'] = (int)1;
$this->_sections['cnt']['loop'] = is_array($_loop=13) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['cnt']['show'] = true;
$this->_sections['cnt']['max'] = $this->_sections['cnt']['loop'];
$this->_sections['cnt']['step'] = 1;
if ($this->_sections['cnt']['start'] < 0)
    $this->_sections['cnt']['start'] = max($this->_sections['cnt']['step'] > 0 ? 0 : -1, $this->_sections['cnt']['loop'] + $this->_sections['cnt']['start']);
else
    $this->_sections['cnt']['start'] = min($this->_sections['cnt']['start'], $this->_sections['cnt']['step'] > 0 ? $this->_sections['cnt']['loop'] : $this->_sections['cnt']['loop']-1);
if ($this->_sections['cnt']['show']) {
    $this->_sections['cnt']['total'] = min(ceil(($this->_sections['cnt']['step'] > 0 ? $this->_sections['cnt']['loop'] - $this->_sections['cnt']['start'] : $this->_sections['cnt']['start']+1)/abs($this->_sections['cnt']['step'])), $this->_sections['cnt']['max']);
    if ($this->_sections['cnt']['total'] == 0)
        $this->_sections['cnt']['show'] = false;
} else
    $this->_sections['cnt']['total'] = 0;
if ($this->_sections['cnt']['show']):

            for ($this->_sections['cnt']['index'] = $this->_sections['cnt']['start'], $this->_sections['cnt']['iteration'] = 1;
                 $this->_sections['cnt']['iteration'] <= $this->_sections['cnt']['total'];
                 $this->_sections['cnt']['index'] += $this->_sections['cnt']['step'], $this->_sections['cnt']['iteration']++):
$this->_sections['cnt']['rownum'] = $this->_sections['cnt']['iteration'];
$this->_sections['cnt']['index_prev'] = $this->_sections['cnt']['index'] - $this->_sections['cnt']['step'];
$this->_sections['cnt']['index_next'] = $this->_sections['cnt']['index'] + $this->_sections['cnt']['step'];
$this->_sections['cnt']['first']      = ($this->_sections['cnt']['iteration'] == 1);
$this->_sections['cnt']['last']       = ($this->_sections['cnt']['iteration'] == $this->_sections['cnt']['total']);
?>
				<option value="<?php echo $this->_sections['cnt']['index']; ?>
"<?php if ($this->_tpl_vars['set_ae_month'] == $this->_sections['cnt']['index']): ?> selected="selected"<?php endif; ?>><?php echo $this->_sections['cnt']['index']; ?>
月</option>
<?php endfor; endif; ?>
			</select>
			<select name="ae_day">
<?php unset($this->_sections['cnt']);
$this->_sections['cnt']['name'] = 'cnt';
$this->_sections['cnt']['start'] = (int)1;
$this->_sections['cnt']['loop'] = is_array($_loop=32) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['cnt']['show'] = true;
$this->_sections['cnt']['max'] = $this->_sections['cnt']['loop'];
$this->_sections['cnt']['step'] = 1;
if ($this->_sections['cnt']['start'] < 0)
    $this->_sections['cnt']['start'] = max($this->_sections['cnt']['step'] > 0 ? 0 : -1, $this->_sections['cnt']['loop'] + $this->_sections['cnt']['start']);
else
    $this->_sections['cnt']['start'] = min($this->_sections['cnt']['start'], $this->_sections['cnt']['step'] > 0 ? $this->_sections['cnt']['loop'] : $this->_sections['cnt']['loop']-1);
if ($this->_sections['cnt']['show']) {
    $this->_sections['cnt']['total'] = min(ceil(($this->_sections['cnt']['step'] > 0 ? $this->_sections['cnt']['loop'] - $this->_sections['cnt']['start'] : $this->_sections['cnt']['start']+1)/abs($this->_sections['cnt']['step'])), $this->_sections['cnt']['max']);
    if ($this->_sections['cnt']['total'] == 0)
        $this->_sections['cnt']['show'] = false;
} else
    $this->_sections['cnt']['total'] = 0;
if ($this->_sections['cnt']['show']):

            for ($this->_sections['cnt']['index'] = $this->_sections['cnt']['start'], $this->_sections['cnt']['iteration'] = 1;
                 $this->_sections['cnt']['iteration'] <= $this->_sections['cnt']['total'];
                 $this->_sections['cnt']['index'] += $this->_sections['cnt']['step'], $this->_sections['cnt']['iteration']++):
$this->_sections['cnt']['rownum'] = $this->_sections['cnt']['iteration'];
$this->_sections['cnt']['index_prev'] = $this->_sections['cnt']['index'] - $this->_sections['cnt']['step'];
$this->_sections['cnt']['index_next'] = $this->_sections['cnt']['index'] + $this->_sections['cnt']['step'];
$this->_sections['cnt']['first']      = ($this->_sections['cnt']['iteration'] == 1);
$this->_sections['cnt']['last']       = ($this->_sections['cnt']['iteration'] == $this->_sections['cnt']['total']);
?>
				<option value="<?php echo $this->_sections['cnt']['index']; ?>
"<?php if ($this->_tpl_vars['set_ae_day'] == $this->_sections['cnt']['index']): ?> selected="selected"<?php endif; ?>><?php echo $this->_sections['cnt']['index']; ?>
日</option>
<?php endfor; endif; ?>
			</select>
			<input type="checkbox" name="unrestraint_flag" value="1"<?php if ($this->_tpl_vars['form_data']['unrestraint_flag'] == 1): ?> checked="checked"<?php endif; ?> /><label>無制限</label>
		</td>
	</tr>
	<tr>
		<th colspan="2">テストフラグ</th>
		<td>
			<input type="checkbox" name="test_flag" value="1"<?php if ($this->_tpl_vars['form_data']['test_flag'] == 1): ?> checked="checked"<?php endif; ?> />テスト用
		</td>
	</tr>
	<tr>
		<th colspan="2">ステータス</th>
		<td>
			<input type="radio" name="status" value="1"<?php if ($this->_tpl_vars['form_data']['status'] == 1 || $this->_tpl_vars['form_data']['status'] == ""): ?> checked="checked"<?php endif; ?> /><label>予約</label>
			<input type="radio" name="status" value="2"<?php if ($this->_tpl_vars['form_data']['status'] == 2): ?> checked="checked"<?php endif; ?> /><label>出稿中</label>
			<input type="radio" name="status" value="3"<?php if ($this->_tpl_vars['form_data']['status'] == 3): ?> checked="checked"<?php endif; ?> /><label>終了</label>
		</td>
	</tr>
	<tr>
		<td colspan="3">
			<input type="submit" value="登録" />
			<input type="hidden" name="mode" value="<?php echo $this->_tpl_vars['mode']; ?>
" />
			<input type="hidden" name="id" value="<?php echo $this->_tpl_vars['form_data']['id']; ?>
" />
		</td>
	</tr>
</table>
</form>
</div><!-- contents -->

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => './hooter.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>