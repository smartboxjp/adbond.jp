<?php /* Smarty version 2.6.25, created on 2010-05-10 23:46:37
         compiled from ./result_log.tpl */ ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => './header.tpl', 'smarty_include_vars' => array('page_title' => '成果ログ')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<!-- Menu -->
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => './menu.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<div id="my_contents">

<h2>成果ログ</h2>

<div id=message>
<?php if ($this->_tpl_vars['error_message'] != ''): ?>
<div id="error_message">
	<h3>ERROR:<?php echo $this->_tpl_vars['error_message']; ?>
</h3>
</div>
<?php endif; ?>
<?php if ($this->_tpl_vars['info_message'] != ''): ?>
<div id="info_message">
	<h3>INFO:<?php echo $this->_tpl_vars['info_message']; ?>
</h3>
</div>
<?php endif; ?>
</div><!-- message -->

<form method="POST" action="<?php echo $this->_supers['server']['PHP_SELF']; ?>
">
<table cellpadding="0" cellspacing="0">
	<tr>
		<td>集計日時</td>
		<td>
			<select name="s_year">
<?php unset($this->_sections['cnt']);
$this->_sections['cnt']['name'] = 'cnt';
$this->_sections['cnt']['start'] = (int)2009;
$this->_sections['cnt']['loop'] = is_array($_loop=2021) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['cnt']['show'] = true;
$this->_sections['cnt']['max'] = $this->_sections['cnt']['loop'];
$this->_sections['cnt']['step'] = 1;
if ($this->_sections['cnt']['start'] < 0)
    $this->_sections['cnt']['start'] = max($this->_sections['cnt']['step'] > 0 ? 0 : -1, $this->_sections['cnt']['loop'] + $this->_sections['cnt']['start']);
else
    $this->_sections['cnt']['start'] = min($this->_sections['cnt']['start'], $this->_sections['cnt']['step'] > 0 ? $this->_sections['cnt']['loop'] : $this->_sections['cnt']['loop']-1);
if ($this->_sections['cnt']['show']) {
    $this->_sections['cnt']['total'] = min(ceil(($this->_sections['cnt']['step'] > 0 ? $this->_sections['cnt']['loop'] - $this->_sections['cnt']['start'] : $this->_sections['cnt']['start']+1)/abs($this->_sections['cnt']['step'])), $this->_sections['cnt']['max']);
    if ($this->_sections['cnt']['total'] == 0)
        $this->_sections['cnt']['show'] = false;
} else
    $this->_sections['cnt']['total'] = 0;
if ($this->_sections['cnt']['show']):

            for ($this->_sections['cnt']['index'] = $this->_sections['cnt']['start'], $this->_sections['cnt']['iteration'] = 1;
                 $this->_sections['cnt']['iteration'] <= $this->_sections['cnt']['total'];
                 $this->_sections['cnt']['index'] += $this->_sections['cnt']['step'], $this->_sections['cnt']['iteration']++):
$this->_sections['cnt']['rownum'] = $this->_sections['cnt']['iteration'];
$this->_sections['cnt']['index_prev'] = $this->_sections['cnt']['index'] - $this->_sections['cnt']['step'];
$this->_sections['cnt']['index_next'] = $this->_sections['cnt']['index'] + $this->_sections['cnt']['step'];
$this->_sections['cnt']['first']      = ($this->_sections['cnt']['iteration'] == 1);
$this->_sections['cnt']['last']       = ($this->_sections['cnt']['iteration'] == $this->_sections['cnt']['total']);
?>
				<option value="<?php echo $this->_sections['cnt']['index']; ?>
"<?php if ($this->_tpl_vars['set_s_year'] == $this->_sections['cnt']['index']): ?> selected="selected"<?php endif; ?>><?php echo $this->_sections['cnt']['index']; ?>
年</option>
<?php endfor; endif; ?>
			</select>
			<select name="s_month">
<?php unset($this->_sections['cnt']);
$this->_sections['cnt']['name'] = 'cnt';
$this->_sections['cnt']['start'] = (int)1;
$this->_sections['cnt']['loop'] = is_array($_loop=13) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['cnt']['show'] = true;
$this->_sections['cnt']['max'] = $this->_sections['cnt']['loop'];
$this->_sections['cnt']['step'] = 1;
if ($this->_sections['cnt']['start'] < 0)
    $this->_sections['cnt']['start'] = max($this->_sections['cnt']['step'] > 0 ? 0 : -1, $this->_sections['cnt']['loop'] + $this->_sections['cnt']['start']);
else
    $this->_sections['cnt']['start'] = min($this->_sections['cnt']['start'], $this->_sections['cnt']['step'] > 0 ? $this->_sections['cnt']['loop'] : $this->_sections['cnt']['loop']-1);
if ($this->_sections['cnt']['show']) {
    $this->_sections['cnt']['total'] = min(ceil(($this->_sections['cnt']['step'] > 0 ? $this->_sections['cnt']['loop'] - $this->_sections['cnt']['start'] : $this->_sections['cnt']['start']+1)/abs($this->_sections['cnt']['step'])), $this->_sections['cnt']['max']);
    if ($this->_sections['cnt']['total'] == 0)
        $this->_sections['cnt']['show'] = false;
} else
    $this->_sections['cnt']['total'] = 0;
if ($this->_sections['cnt']['show']):

            for ($this->_sections['cnt']['index'] = $this->_sections['cnt']['start'], $this->_sections['cnt']['iteration'] = 1;
                 $this->_sections['cnt']['iteration'] <= $this->_sections['cnt']['total'];
                 $this->_sections['cnt']['index'] += $this->_sections['cnt']['step'], $this->_sections['cnt']['iteration']++):
$this->_sections['cnt']['rownum'] = $this->_sections['cnt']['iteration'];
$this->_sections['cnt']['index_prev'] = $this->_sections['cnt']['index'] - $this->_sections['cnt']['step'];
$this->_sections['cnt']['index_next'] = $this->_sections['cnt']['index'] + $this->_sections['cnt']['step'];
$this->_sections['cnt']['first']      = ($this->_sections['cnt']['iteration'] == 1);
$this->_sections['cnt']['last']       = ($this->_sections['cnt']['iteration'] == $this->_sections['cnt']['total']);
?>
				<option value="<?php echo $this->_sections['cnt']['index']; ?>
"<?php if ($this->_tpl_vars['set_s_month'] == $this->_sections['cnt']['index']): ?> selected="selected"<?php endif; ?>><?php echo $this->_sections['cnt']['index']; ?>
月</option>
<?php endfor; endif; ?>
			</select>
		</td>
	</tr>
	<tr>
		<td>ログ種類</td>
		<td>
			<input type="radio" name="log_type" value="1"<?php if ($this->_tpl_vars['log_type'] == 1 || $this->_tpl_vars['log_type'] == ""): ?> checked="checked"<?php endif; ?> /><label>クリックログ</label>
			<input type="radio" name="log_type" value="2"<?php if ($this->_tpl_vars['log_type'] == 2): ?> checked="checked"<?php endif; ?> /><label>アクションログ</label>
			<input type="radio" name="log_type" value="3"<?php if ($this->_tpl_vars['log_type'] == 3): ?> checked="checked"<?php endif; ?> /><label>ポイント通知ログ</label>
		</td>
	</tr>
	<tr>
		<td colspan="3">
			<input type="submit" value="検索" />
			<input type="hidden" name="mode" value="search" />
		</td>
	</tr>
</table>
</form>

<?php if (! is_null ( $this->_tpl_vars['list'] )): ?>
<table cellpadding="0" cellspacing="0">
	<tr>
		<th colspan="3">
			<input type="button" value="<?php echo $this->_tpl_vars['set_s_year']; ?>
年<?php echo $this->_tpl_vars['set_s_month']; ?>
月分CSVダウンロード" onclick="window.open('./result_log_download.php?date=<?php echo $this->_tpl_vars['search_date']; ?>
&type=<?php echo $this->_tpl_vars['log_type']; ?>
&monthly=1')" />
		</th>
	</tr>
	<tr>
		<th>日付</th>
		<th>件数</th>
		<th></th>
	</tr>
<?php $_from = $this->_tpl_vars['list']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['list'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['list']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['data']):
        $this->_foreach['list']['iteration']++;
?>
	<tr>
		<td><?php echo $this->_tpl_vars['data']['log_date']; ?>
</td>
		<td><?php echo $this->_tpl_vars['data']['log_count']; ?>
</td>
		<td>
			<form method="POST" action="<?php echo $this->_supers['server']['PHP_SELF']; ?>
">
				<input type="submit" value="詳細" />
				<input type="hidden" name="mode" value="detail" />
				<input type="hidden" name="download_date" value="<?php echo $this->_tpl_vars['data']['download_date']; ?>
" />
				<input type="hidden" name="log_type" value="<?php echo $this->_tpl_vars['log_type']; ?>
" />
			</form>
			<input type="button" value="CSVダウンロード" onclick="window.open('./result_log_download.php?date=<?php echo $this->_tpl_vars['data']['download_date']; ?>
&type=<?php echo $this->_tpl_vars['log_type']; ?>
')" />
		</td>
	</tr>
<?php endforeach; endif; unset($_from); ?>
</table>
<?php endif; ?>

</div><!-- contents -->

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => './hooter.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>