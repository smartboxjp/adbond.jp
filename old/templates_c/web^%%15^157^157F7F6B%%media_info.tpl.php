<?php /* Smarty version 2.6.25, created on 2010-05-10 01:03:15
         compiled from ./media_info.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'htmlspecialchars', './media_info.tpl', 29, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => './header.tpl', 'smarty_include_vars' => array('page_title' => 'メディア管理')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<!-- Menu -->
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => './media_menu.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<div id="my_contents">

<h2>登録情報</h2>

<div id=message>
<?php if ($this->_tpl_vars['error_message'] != ''): ?>
<div id="error_message">
	<h3>ERROR:<?php echo $this->_tpl_vars['error_message']; ?>
</h3>
</div>
<?php endif; ?>
<?php if ($this->_tpl_vars['info_message'] != ''): ?>
<div id="info_message">
	<h3>INFO:<?php echo $this->_tpl_vars['info_message']; ?>
</h3>
</div>
<?php endif; ?>
</div><!-- message -->

<table cellpadding="0" cellspacing="0">
	<tr>
		<th colspan="2">登録者情報</th>
	</tr>
	<tr>
		<th>ログインID</th>
		<td><?php echo ((is_array($_tmp=$this->_tpl_vars['data']['login_id'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
</td>
	</tr>
	<tr>
		<th>パスワード</th>
		<td><?php echo ((is_array($_tmp=$this->_tpl_vars['data']['login_pass'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
</td>
	</tr>
	<tr>
		<th>企業名/個人名</th>
		<td><?php echo ((is_array($_tmp=$this->_tpl_vars['data']['publisher_name'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
</td>
	</tr>
	<tr>
		<th>担当者名 ※法人の場合のみ</th>
		<td><?php echo ((is_array($_tmp=$this->_tpl_vars['data']['contact_person'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
</td>
	</tr>
	<tr>
		<th>TEL</th>
		<td><?php echo ((is_array($_tmp=$this->_tpl_vars['data']['tel'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
</td>
	</tr>
	<tr>
		<th>FAX</th>
		<td><?php echo ((is_array($_tmp=$this->_tpl_vars['data']['fax'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
</td>
	</tr>
	<tr>
		<th>メールアドレス</th>
		<td><?php echo ((is_array($_tmp=$this->_tpl_vars['data']['email'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
</td>
	</tr>
	<tr>
		<td colspan="2">
			<form method="POST" action="<?php echo $this->_supers['server']['PHP_SELF']; ?>
">
				<input type="submit" value="編集" />
				<input type="hidden" name="mode" value="user_edit" />
			</form>
		</td>
	</tr>
</table>

<table cellpadding="0" cellspacing="0">
	<tr>
		<th colspan="2">口座情報</th>
	</tr>
	<tr>
		<th>振込先銀行名</th>
		<td><?php echo ((is_array($_tmp=$this->_tpl_vars['data']['bank_name'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
</td>
	</tr>
	<tr>
		<th>支店名</th>
		<td><?php echo ((is_array($_tmp=$this->_tpl_vars['data']['branch_name'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
</td>
	</tr>
	<tr>
		<th>口座種別</th>
		<td>
<?php if ($this->_tpl_vars['data']['account_type'] == 1): ?>
			普通
<?php elseif ($this->_tpl_vars['data']['account_type'] == 2): ?>
			当座
<?php endif; ?>
		</td>
	</tr>
	<tr>
		<th>口座名義</th>
		<td><?php echo ((is_array($_tmp=$this->_tpl_vars['data']['account_holder'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
</td>
	</tr>
	<tr>
		<th>口座番号</th>
		<td><?php echo ((is_array($_tmp=$this->_tpl_vars['data']['account_number'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
</td>
	</tr>
	<tr>
		<td colspan="2">
			<form method="POST" action="<?php echo $this->_supers['server']['PHP_SELF']; ?>
">
				<input type="submit" value="編集" />
				<input type="hidden" name="mode" value="account_edit" />
			</form>
		</td>
	</tr>
</table>

<table cellpadding="0" cellspacing="0">
	<tr>
		<th colspan="2">登録メディア一覧</th>
	</tr>
<?php $_from = $this->_tpl_vars['media_array']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['media_list'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['media_list']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['data']):
        $this->_foreach['media_list']['iteration']++;
?>
	<tr>
		<th><?php echo ((is_array($_tmp=$this->_tpl_vars['data']['media_name'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
</th>
		<td>
			<form method="POST" action="./info_media.php">
				<input type="submit" value="編集" />
				<input type="hidden" name="mode" value="edit" />
				<input type="hidden" name="id" value="<?php echo $this->_tpl_vars['data']['id']; ?>
" />
			</form>
		</td>
	</tr>
<?php endforeach; endif; unset($_from); ?>
	<tr>
		<td colspan="2">
			<form method="POST" action="./info_media.php">
				<input type="submit" value="追加" />
				<input type="hidden" name="mode" value="new_regist" />
			</form>
		</td>
	</tr>
</table>
</div><!-- contents -->

<!-- フッター -->
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => './hooter.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>