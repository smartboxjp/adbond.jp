<?php /* Smarty version 2.6.25, created on 2010-05-11 04:56:39
         compiled from ./media_input.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'cat', './media_input.tpl', 1, false),array('modifier', 'htmlspecialchars', './media_input.tpl', 30, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => './header.tpl', 'smarty_include_vars' => array('page_title' => ((is_array($_tmp=((is_array($_tmp='媒体(')) ? $this->_run_mod_handler('cat', true, $_tmp, $this->_tpl_vars['sub_title']) : smarty_modifier_cat($_tmp, $this->_tpl_vars['sub_title'])))) ? $this->_run_mod_handler('cat', true, $_tmp, ')') : smarty_modifier_cat($_tmp, ')')))));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<!-- Menu -->
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => './menu.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<div id="my_contents">

<h2>媒体(<?php echo $this->_tpl_vars['sub_title']; ?>
)</h2>

<div id=message>
<?php if ($this->_tpl_vars['error_message'] != ''): ?>
<div id="error_message">
<h3>ERROR:<?php echo $this->_tpl_vars['error_message']; ?>
</h3>
</div>
<?php endif; ?>
<?php if ($this->_tpl_vars['info_message'] != ''): ?>
<div id="info_message">
<h3>INFO:<?php echo $this->_tpl_vars['info_message']; ?>
</h3>
</div>
<?php endif; ?>
</div><!-- message -->

<form method="POST" action="<?php echo $this->_supers['server']['PHP_SELF']; ?>
" name="form1">
<table cellpadding="0" cellspacing="0">
	<tr>
		<th colspan="2">媒体発行者</th>
		<td>
			<select name="media_publisher_id">
			<?php $_from = $this->_tpl_vars['media_publisher_array']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['media_publisher_list'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['media_publisher_list']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['data']):
        $this->_foreach['media_publisher_list']['iteration']++;
?>
				<option value="<?php echo $this->_tpl_vars['data']['id']; ?>
"<?php if ($this->_tpl_vars['form_data']['media_publisher_id'] == $this->_tpl_vars['data']['id']): ?> selected="selected"<?php endif; ?>><?php echo ((is_array($_tmp=$this->_tpl_vars['data']['name'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
</option>
			<?php endforeach; endif; unset($_from); ?>
			</select>
		</td>
	</tr>
	<tr>
		<th colspan="2">媒体カテゴリー</th>
		<td>
			<select name="media_category_id">
			<?php $_from = $this->_tpl_vars['media_category_array']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['media_category_list'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['media_category_list']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['data']):
        $this->_foreach['media_category_list']['iteration']++;
?>
				<option value="<?php echo $this->_tpl_vars['data']['id']; ?>
"<?php if ($this->_tpl_vars['form_data']['media_category_id'] == $this->_tpl_vars['data']['id']): ?> selected="selected"<?php endif; ?>><?php echo ((is_array($_tmp=$this->_tpl_vars['data']['name'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
</option>
			<?php endforeach; endif; unset($_from); ?>
			</select>
		</td>
	</tr>
	<tr>
		<th colspan="2">サイト名</th>
		<td>
			<input type="text" size="50" name="media_name" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['media_name'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />
		</td>
	</tr>
	<tr>
		<th colspan="2">対応キャリア</th>
		<td>
			<input type="checkbox" name="support_docomo" value="1"<?php if ($this->_tpl_vars['form_data']['support_docomo'] == 1): ?> checked="checked"<?php endif; ?> /><label>docomo</label>
			<input type="checkbox" name="support_softbank" value="1"<?php if ($this->_tpl_vars['form_data']['support_softbank'] == 1): ?> checked="checked"<?php endif; ?> /><label>softbank</label>
			<input type="checkbox" name="support_au" value="1"<?php if ($this->_tpl_vars['form_data']['support_au'] == 1): ?> checked="checked"<?php endif; ?> /><label>au</label>
			<input type="checkbox" name="support_pc" value="1"<?php if ($this->_tpl_vars['form_data']['support_pc'] == 1): ?> checked="checked"<?php endif; ?> /><label>pc</label>
		</td>
	</tr>
	<tr>
		<th colspan="2">サイトURL(docomo)</th>
		<td>
			<input type="text" size="100" name="site_url_docomo" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['site_url_docomo'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />
		</td>
	</tr>
	<tr>
		<th colspan="2">サイトURL(softbank)</th>
		<td>
			<input type="text" size="100" name="site_url_softbank" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['site_url_softbank'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />
		</td>
	</tr>
	<tr>
		<th colspan="2">サイトURL(au)</th>
		<td>
			<input type="text" size="100" name="site_url_au" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['site_url_au'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />
		</td>
	</tr>
	<tr>
		<th colspan="2">サイトURL(pc)</th>
		<td>
			<input type="text" size="100" name="site_url_pc" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['site_url_pc'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />
		</td>
	</tr>
	<tr>
		<th colspan="2">メディア種別</th>
		<td>
			<input type="radio" name="media_type" value="1"<?php if ($this->_tpl_vars['form_data']['media_type'] == 1 || $this->_tpl_vars['form_data']['media_type'] == ""): ?> checked="checked"<?php endif; ?> /><label>サイト</label>
			<input type="radio" name="media_type" value="2"<?php if ($this->_tpl_vars['form_data']['media_type'] == 2): ?> checked="checked"<?php endif; ?> /><label>メール</label>
			<input type="radio" name="media_type" value="3"<?php if ($this->_tpl_vars['form_data']['media_type'] == 3): ?> checked="checked"<?php endif; ?> /><label>その他</label>
		</td>
	</tr>
	<tr>
		<th colspan="2">PV/日(発行部数)</th>
		<td>
			<input type="text" size="30" name="page_view_day" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['page_view_day'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />
		</td>
	</tr>
	<tr>
		<th colspan="2">サイト概要</th>
		<td>
			<textarea cols="70" rows="5" name="site_outline"><?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['site_outline'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
</textarea>
		</td>
	</tr>
	<tr>
		<th colspan="2">ポイントバック通知先URL</th>
		<td>
			<input type="text" size="100" name="point_back_url" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['point_back_url'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" /><br />
			##ID##⇒ユーザー識別ID<br />
			##CID##⇒広告ID<br />
			##CLICK_DATE##⇒クリック日時<br />
			##ACTION_DATE##⇒成果発生日時<br /><br />
			＜ユーザ識別IDの付加について＞<br />
			ユーザ識別IDを付加する位置に「##ID##」を記入する<br />
			http://example.jp/?id=***→http://example.jp/?id=##ID##<br />
			http://example.jp/***→http://example.jp/##ID##<br />
			http://example.jp/?pid=***→http://example.jp/?pid=##ID##<br /><br />
<?php if ($this->_tpl_vars['form_data']['id'] != ""): ?>
			<input type="text" size="100" name="point_test_url" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['point_test_url'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" readonly="readonly" /><br />
			<input type="button" name="test_url_make" value="テストURL発行" onclick="document.form1.point_test_url.value ='http://adbond.jp/action/point_test.php?m=<?php echo $this->_tpl_vars['form_data']['id']; ?>
&a=0';" />
<?php endif; ?>
		</td>
	</tr>
	<tr>
		<th colspan="2">テストフラグ</th>
		<td>
			<input type="checkbox" name="test_flag" value="1"<?php if ($this->_tpl_vars['form_data']['test_flag'] == 1): ?> checked="checked"<?php endif; ?> />テスト用
		</td>
	</tr>
	<tr>
		<th colspan="2">ステータス</th>
		<td>
			<input type="radio" name="status" value="1"<?php if ($this->_tpl_vars['form_data']['status'] == 1 || $this->_tpl_vars['form_data']['status'] == ""): ?> checked="checked"<?php endif; ?> /><label>仮登録</label>
			<input type="radio" name="status" value="2"<?php if ($this->_tpl_vars['form_data']['status'] == 2): ?> checked="checked"<?php endif; ?> /><label>正規</label>
			<input type="radio" name="status" value="3"<?php if ($this->_tpl_vars['form_data']['status'] == 3): ?> checked="checked"<?php endif; ?> /><label>退会</label>
		</td>
	</tr>
	<tr>
		<td colspan="3">
			<input type="submit" value="登録" />
			<input type="hidden" name="mode" value="<?php echo $this->_tpl_vars['mode']; ?>
" />
			<input type="hidden" name="id" value="<?php echo $this->_tpl_vars['form_data']['id']; ?>
" />
		</td>
	</tr>
</table>
</form>

</div><!-- contents -->

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => './hooter.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>