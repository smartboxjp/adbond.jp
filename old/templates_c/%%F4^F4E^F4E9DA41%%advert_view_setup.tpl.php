<?php /* Smarty version 2.6.25, created on 2010-05-11 20:12:10
         compiled from ./advert_view_setup.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'htmlspecialchars', './advert_view_setup.tpl', 29, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => './header.tpl', 'smarty_include_vars' => array('page_title' => '表示広告設定')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<!-- Menu -->
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => './menu.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<div id="my_contents">

<h2>表示広告設定</h2>

<div id=message>
<?php if ($this->_tpl_vars['error_message'] != ''): ?>
<div id="error_message">
<h3>ERROR:<?php echo $this->_tpl_vars['error_message']; ?>
</h3>
</div>
<?php endif; ?>
<?php if ($this->_tpl_vars['info_message'] != ''): ?>
<div id="info_message">
<h3>INFO:<?php echo $this->_tpl_vars['info_message']; ?>
</h3>
</div>
<?php endif; ?>
</div><!-- message -->

<form method="POST" action="<?php echo $this->_supers['server']['PHP_SELF']; ?>
">
<table cellpadding="0" cellspacing="0">
<?php $_from = $this->_tpl_vars['media_category_array']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['media_category_list'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['media_category_list']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['data']):
        $this->_foreach['media_category_list']['iteration']++;
?>
	<tr>
		<td>
			<input type="checkbox" name="check_category[<?php echo $this->_tpl_vars['data']['id']; ?>
]" value="<?php echo $this->_tpl_vars['data']['id']; ?>
"<?php if ($this->_tpl_vars['check_category'][$this->_tpl_vars['data']['id']] == $this->_tpl_vars['data']['id']): ?> checked="checked"<?php endif; ?> />
			<label><a href="?advert_id=<?php echo $this->_tpl_vars['advert_id']; ?>
&category_id=<?php echo $this->_tpl_vars['data']['id']; ?>
"><?php echo ((is_array($_tmp=$this->_tpl_vars['data']['name'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
</a></label>
		</td>
	</tr>
<?php endforeach; endif; unset($_from); ?>
</table>
<?php if ($this->_tpl_vars['category_id'] != ""): ?>
<table cellpadding="0" cellspacing="0">
	<tr>
		<td><?php echo ((is_array($_tmp=$this->_tpl_vars['media_category_array'][$this->_tpl_vars['category_id']]['name'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
</td>
	</tr>
<?php $_from = $this->_tpl_vars['media_array']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['media_publisher_list'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['media_publisher_list']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['data']):
        $this->_foreach['media_publisher_list']['iteration']++;
?>
	<tr>
		<td>
			<input type="checkbox" name="check_publisher[<?php echo $this->_tpl_vars['data']['publisher_id']; ?>
]" value="<?php echo $this->_tpl_vars['data']['publisher_id']; ?>
"<?php if ($this->_tpl_vars['check_publisher'][$this->_tpl_vars['data']['publisher_id']] == $this->_tpl_vars['data']['publisher_id']): ?> checked="checked"<?php endif; ?> />
			<label><a href="?advert_id=<?php echo $this->_tpl_vars['advert_id']; ?>
&category_id=<?php echo $this->_tpl_vars['category_id']; ?>
&publisher_id=<?php echo $this->_tpl_vars['data']['publisher_id']; ?>
"><?php echo ((is_array($_tmp=$this->_tpl_vars['data']['publisher_name'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
</a></label>
		</td>
	</tr>
<?php endforeach; endif; unset($_from); ?>
</table>
<?php if ($this->_tpl_vars['publisher_id'] != ""): ?>
<table cellpadding="0" cellspacing="0">
	<tr>
		<td><?php echo ((is_array($_tmp=$this->_tpl_vars['media_array'][$this->_tpl_vars['publisher_id']]['publisher_name'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
</td>
	</tr>
<?php $_from = $this->_tpl_vars['media_array'][$this->_tpl_vars['publisher_id']]['media']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['media_list'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['media_list']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['data']):
        $this->_foreach['media_list']['iteration']++;
?>
	<tr>
		<td>
			<input type="checkbox" name="check_media[<?php echo $this->_tpl_vars['data']['media_id']; ?>
]" value="<?php echo $this->_tpl_vars['data']['media_id']; ?>
"<?php if ($this->_tpl_vars['check_media'][$this->_tpl_vars['data']['media_id']] == $this->_tpl_vars['data']['media_id']): ?> checked="checked"<?php endif; ?> />
			<label><?php echo ((is_array($_tmp=$this->_tpl_vars['data']['media_name'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
</label>
		</td>
	</tr>
<?php endforeach; endif; unset($_from); ?>
</table>
<?php endif; ?>
<?php endif; ?>
<table cellpadding="0" cellspacing="0">
	<tr>
		<td>
			<input type="submit" value="設定" />
			<input type="hidden" name="mode" value="setup" />
			<input type="hidden" name="advert_id" value="<?php echo $this->_tpl_vars['advert_id']; ?>
" />
			<input type="hidden" name="category_id" value="<?php echo $this->_tpl_vars['category_id']; ?>
" />
			<input type="hidden" name="publisher_id" value="<?php echo $this->_tpl_vars['publisher_id']; ?>
" />
		</td>
	</tr>
</table>
</form>
</div><!-- contents -->

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => './hooter.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>