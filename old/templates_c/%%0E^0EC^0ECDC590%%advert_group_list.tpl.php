<?php /* Smarty version 2.6.25, created on 2010-05-10 03:56:15
         compiled from ./advert_group_list.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'htmlspecialchars', './advert_group_list.tpl', 53, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => './header.tpl', 'smarty_include_vars' => array('page_title' => '広告主グループ一覧')));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<!-- Menu -->
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => './menu.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<div id="my_contents">

<h2>広告主グループ一覧</h2>

<div id=message>
<?php if ($this->_tpl_vars['error_message'] != ''): ?>
<div id="error_message">
	<h3>ERROR:<?php echo $this->_tpl_vars['error_message']; ?>
</h3>
</div>
<?php endif; ?>
<?php if ($this->_tpl_vars['info_message'] != ''): ?>
<div id="info_message">
	<h3>INFO:<?php echo $this->_tpl_vars['info_message']; ?>
</h3>
</div>
<?php endif; ?>
</div><!-- message -->

<form method="POST" action="<?php echo $this->_supers['server']['PHP_SELF']; ?>
">
<table cellpadding="0" cellspacing="0">
	<tr>
		<td>検索方法</td>
		<td>
			<input type="radio" name="method" value="1"<?php if ($this->_tpl_vars['search']['method'] == 1 || $this->_tpl_vars['search']['method'] == ""): ?> checked="checked"<?php endif; ?> /><label>完全一致</label>
			<input type="radio" name="method" value="2"<?php if ($this->_tpl_vars['search']['method'] == 2): ?> checked="checked"<?php endif; ?> /><label>前方一致</label>
			<input type="radio" name="method" value="3"<?php if ($this->_tpl_vars['search']['method'] == 3): ?> checked="checked"<?php endif; ?> /><label>後方一致</label>
			<input type="radio" name="method" value="4"<?php if ($this->_tpl_vars['search']['method'] == 4): ?> checked="checked"<?php endif; ?> /><label>あいまい</label>
			<input type="checkbox" name="reverse" value="1"<?php if ($this->_tpl_vars['search']['reverse'] == 1): ?> checked="checked"<?php endif; ?> /><label>条件反転</label>
		</td>
	</tr>
	<tr>
		<td>ソート</td>
		<td>
			<select name="sort">
				<option value="id"<?php if ($this->_tpl_vars['search']['sort'] == 'id'): ?> selected="selected"<?php endif; ?>>広告主グループID</option>
				<option value="name"<?php if ($this->_tpl_vars['search']['sort'] == 'name'): ?> selected="selected"<?php endif; ?>>広告主グループ名</option>
				<option value="created_at"<?php if ($this->_tpl_vars['search']['sort'] == 'created_at'): ?> selected="selected"<?php endif; ?>>登録日時</option>
			</select>
			<input type="radio" name="order" value="ASC"<?php if ($this->_tpl_vars['search']['order'] == 'ASC' || $this->_tpl_vars['search']['method'] == ""): ?> checked="checked"<?php endif; ?> /><label>昇順</label>
			<input type="radio" name="order" value="DESC"<?php if ($this->_tpl_vars['search']['order'] == 'DESC'): ?> checked="checked"<?php endif; ?> /><label>降順</label>
		</td>
	</tr>
	<tr>
		<td>広告主グループID</td>
		<td><input type="text" size="50" name="group_id" value="<?php echo $this->_tpl_vars['search']['id']; ?>
" /></td>
	</tr>
	<tr>
		<td>広告主グループ名</td>
		<td><input type="text" size="50" name="group_name" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['search']['name'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" /></td>
	</tr>
	<tr>
		<td>登録日時</td>
		<td>
			<input type="checkbox" name="created_at_flag" value="1"<?php if ($this->_tpl_vars['search']['created_at_flag'] == 1): ?> checked="checked"<?php endif; ?> />
			<select name="s_year">
<?php unset($this->_sections['cnt']);
$this->_sections['cnt']['name'] = 'cnt';
$this->_sections['cnt']['start'] = (int)2009;
$this->_sections['cnt']['loop'] = is_array($_loop=2021) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['cnt']['show'] = true;
$this->_sections['cnt']['max'] = $this->_sections['cnt']['loop'];
$this->_sections['cnt']['step'] = 1;
if ($this->_sections['cnt']['start'] < 0)
    $this->_sections['cnt']['start'] = max($this->_sections['cnt']['step'] > 0 ? 0 : -1, $this->_sections['cnt']['loop'] + $this->_sections['cnt']['start']);
else
    $this->_sections['cnt']['start'] = min($this->_sections['cnt']['start'], $this->_sections['cnt']['step'] > 0 ? $this->_sections['cnt']['loop'] : $this->_sections['cnt']['loop']-1);
if ($this->_sections['cnt']['show']) {
    $this->_sections['cnt']['total'] = min(ceil(($this->_sections['cnt']['step'] > 0 ? $this->_sections['cnt']['loop'] - $this->_sections['cnt']['start'] : $this->_sections['cnt']['start']+1)/abs($this->_sections['cnt']['step'])), $this->_sections['cnt']['max']);
    if ($this->_sections['cnt']['total'] == 0)
        $this->_sections['cnt']['show'] = false;
} else
    $this->_sections['cnt']['total'] = 0;
if ($this->_sections['cnt']['show']):

            for ($this->_sections['cnt']['index'] = $this->_sections['cnt']['start'], $this->_sections['cnt']['iteration'] = 1;
                 $this->_sections['cnt']['iteration'] <= $this->_sections['cnt']['total'];
                 $this->_sections['cnt']['index'] += $this->_sections['cnt']['step'], $this->_sections['cnt']['iteration']++):
$this->_sections['cnt']['rownum'] = $this->_sections['cnt']['iteration'];
$this->_sections['cnt']['index_prev'] = $this->_sections['cnt']['index'] - $this->_sections['cnt']['step'];
$this->_sections['cnt']['index_next'] = $this->_sections['cnt']['index'] + $this->_sections['cnt']['step'];
$this->_sections['cnt']['first']      = ($this->_sections['cnt']['iteration'] == 1);
$this->_sections['cnt']['last']       = ($this->_sections['cnt']['iteration'] == $this->_sections['cnt']['total']);
?>
				<option value="<?php echo $this->_sections['cnt']['index']; ?>
"<?php if ($this->_tpl_vars['set_s_year'] == $this->_sections['cnt']['index']): ?> selected="selected"<?php endif; ?>><?php echo $this->_sections['cnt']['index']; ?>
年</option>
<?php endfor; endif; ?>
			</select>
			<select name="s_month">
<?php unset($this->_sections['cnt']);
$this->_sections['cnt']['name'] = 'cnt';
$this->_sections['cnt']['start'] = (int)1;
$this->_sections['cnt']['loop'] = is_array($_loop=13) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['cnt']['show'] = true;
$this->_sections['cnt']['max'] = $this->_sections['cnt']['loop'];
$this->_sections['cnt']['step'] = 1;
if ($this->_sections['cnt']['start'] < 0)
    $this->_sections['cnt']['start'] = max($this->_sections['cnt']['step'] > 0 ? 0 : -1, $this->_sections['cnt']['loop'] + $this->_sections['cnt']['start']);
else
    $this->_sections['cnt']['start'] = min($this->_sections['cnt']['start'], $this->_sections['cnt']['step'] > 0 ? $this->_sections['cnt']['loop'] : $this->_sections['cnt']['loop']-1);
if ($this->_sections['cnt']['show']) {
    $this->_sections['cnt']['total'] = min(ceil(($this->_sections['cnt']['step'] > 0 ? $this->_sections['cnt']['loop'] - $this->_sections['cnt']['start'] : $this->_sections['cnt']['start']+1)/abs($this->_sections['cnt']['step'])), $this->_sections['cnt']['max']);
    if ($this->_sections['cnt']['total'] == 0)
        $this->_sections['cnt']['show'] = false;
} else
    $this->_sections['cnt']['total'] = 0;
if ($this->_sections['cnt']['show']):

            for ($this->_sections['cnt']['index'] = $this->_sections['cnt']['start'], $this->_sections['cnt']['iteration'] = 1;
                 $this->_sections['cnt']['iteration'] <= $this->_sections['cnt']['total'];
                 $this->_sections['cnt']['index'] += $this->_sections['cnt']['step'], $this->_sections['cnt']['iteration']++):
$this->_sections['cnt']['rownum'] = $this->_sections['cnt']['iteration'];
$this->_sections['cnt']['index_prev'] = $this->_sections['cnt']['index'] - $this->_sections['cnt']['step'];
$this->_sections['cnt']['index_next'] = $this->_sections['cnt']['index'] + $this->_sections['cnt']['step'];
$this->_sections['cnt']['first']      = ($this->_sections['cnt']['iteration'] == 1);
$this->_sections['cnt']['last']       = ($this->_sections['cnt']['iteration'] == $this->_sections['cnt']['total']);
?>
				<option value="<?php echo $this->_sections['cnt']['index']; ?>
"<?php if ($this->_tpl_vars['set_s_month'] == $this->_sections['cnt']['index']): ?> selected="selected"<?php endif; ?>><?php echo $this->_sections['cnt']['index']; ?>
月</option>
<?php endfor; endif; ?>
			</select>
			<select name="s_day">
<?php unset($this->_sections['cnt']);
$this->_sections['cnt']['name'] = 'cnt';
$this->_sections['cnt']['start'] = (int)1;
$this->_sections['cnt']['loop'] = is_array($_loop=32) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['cnt']['show'] = true;
$this->_sections['cnt']['max'] = $this->_sections['cnt']['loop'];
$this->_sections['cnt']['step'] = 1;
if ($this->_sections['cnt']['start'] < 0)
    $this->_sections['cnt']['start'] = max($this->_sections['cnt']['step'] > 0 ? 0 : -1, $this->_sections['cnt']['loop'] + $this->_sections['cnt']['start']);
else
    $this->_sections['cnt']['start'] = min($this->_sections['cnt']['start'], $this->_sections['cnt']['step'] > 0 ? $this->_sections['cnt']['loop'] : $this->_sections['cnt']['loop']-1);
if ($this->_sections['cnt']['show']) {
    $this->_sections['cnt']['total'] = min(ceil(($this->_sections['cnt']['step'] > 0 ? $this->_sections['cnt']['loop'] - $this->_sections['cnt']['start'] : $this->_sections['cnt']['start']+1)/abs($this->_sections['cnt']['step'])), $this->_sections['cnt']['max']);
    if ($this->_sections['cnt']['total'] == 0)
        $this->_sections['cnt']['show'] = false;
} else
    $this->_sections['cnt']['total'] = 0;
if ($this->_sections['cnt']['show']):

            for ($this->_sections['cnt']['index'] = $this->_sections['cnt']['start'], $this->_sections['cnt']['iteration'] = 1;
                 $this->_sections['cnt']['iteration'] <= $this->_sections['cnt']['total'];
                 $this->_sections['cnt']['index'] += $this->_sections['cnt']['step'], $this->_sections['cnt']['iteration']++):
$this->_sections['cnt']['rownum'] = $this->_sections['cnt']['iteration'];
$this->_sections['cnt']['index_prev'] = $this->_sections['cnt']['index'] - $this->_sections['cnt']['step'];
$this->_sections['cnt']['index_next'] = $this->_sections['cnt']['index'] + $this->_sections['cnt']['step'];
$this->_sections['cnt']['first']      = ($this->_sections['cnt']['iteration'] == 1);
$this->_sections['cnt']['last']       = ($this->_sections['cnt']['iteration'] == $this->_sections['cnt']['total']);
?>
				<option value="<?php echo $this->_sections['cnt']['index']; ?>
"<?php if ($this->_tpl_vars['set_s_day'] == $this->_sections['cnt']['index']): ?> selected="selected"<?php endif; ?>><?php echo $this->_sections['cnt']['index']; ?>
日</option>
<?php endfor; endif; ?>
			</select> ～
			<select name="e_year">
<?php unset($this->_sections['cnt']);
$this->_sections['cnt']['name'] = 'cnt';
$this->_sections['cnt']['start'] = (int)2009;
$this->_sections['cnt']['loop'] = is_array($_loop=2021) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['cnt']['show'] = true;
$this->_sections['cnt']['max'] = $this->_sections['cnt']['loop'];
$this->_sections['cnt']['step'] = 1;
if ($this->_sections['cnt']['start'] < 0)
    $this->_sections['cnt']['start'] = max($this->_sections['cnt']['step'] > 0 ? 0 : -1, $this->_sections['cnt']['loop'] + $this->_sections['cnt']['start']);
else
    $this->_sections['cnt']['start'] = min($this->_sections['cnt']['start'], $this->_sections['cnt']['step'] > 0 ? $this->_sections['cnt']['loop'] : $this->_sections['cnt']['loop']-1);
if ($this->_sections['cnt']['show']) {
    $this->_sections['cnt']['total'] = min(ceil(($this->_sections['cnt']['step'] > 0 ? $this->_sections['cnt']['loop'] - $this->_sections['cnt']['start'] : $this->_sections['cnt']['start']+1)/abs($this->_sections['cnt']['step'])), $this->_sections['cnt']['max']);
    if ($this->_sections['cnt']['total'] == 0)
        $this->_sections['cnt']['show'] = false;
} else
    $this->_sections['cnt']['total'] = 0;
if ($this->_sections['cnt']['show']):

            for ($this->_sections['cnt']['index'] = $this->_sections['cnt']['start'], $this->_sections['cnt']['iteration'] = 1;
                 $this->_sections['cnt']['iteration'] <= $this->_sections['cnt']['total'];
                 $this->_sections['cnt']['index'] += $this->_sections['cnt']['step'], $this->_sections['cnt']['iteration']++):
$this->_sections['cnt']['rownum'] = $this->_sections['cnt']['iteration'];
$this->_sections['cnt']['index_prev'] = $this->_sections['cnt']['index'] - $this->_sections['cnt']['step'];
$this->_sections['cnt']['index_next'] = $this->_sections['cnt']['index'] + $this->_sections['cnt']['step'];
$this->_sections['cnt']['first']      = ($this->_sections['cnt']['iteration'] == 1);
$this->_sections['cnt']['last']       = ($this->_sections['cnt']['iteration'] == $this->_sections['cnt']['total']);
?>
				<option value="<?php echo $this->_sections['cnt']['index']; ?>
"<?php if ($this->_tpl_vars['set_e_year'] == $this->_sections['cnt']['index']): ?> selected="selected"<?php endif; ?>><?php echo $this->_sections['cnt']['index']; ?>
年</option>
<?php endfor; endif; ?>
			</select>
			<select name="e_month">
<?php unset($this->_sections['cnt']);
$this->_sections['cnt']['name'] = 'cnt';
$this->_sections['cnt']['start'] = (int)1;
$this->_sections['cnt']['loop'] = is_array($_loop=13) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['cnt']['show'] = true;
$this->_sections['cnt']['max'] = $this->_sections['cnt']['loop'];
$this->_sections['cnt']['step'] = 1;
if ($this->_sections['cnt']['start'] < 0)
    $this->_sections['cnt']['start'] = max($this->_sections['cnt']['step'] > 0 ? 0 : -1, $this->_sections['cnt']['loop'] + $this->_sections['cnt']['start']);
else
    $this->_sections['cnt']['start'] = min($this->_sections['cnt']['start'], $this->_sections['cnt']['step'] > 0 ? $this->_sections['cnt']['loop'] : $this->_sections['cnt']['loop']-1);
if ($this->_sections['cnt']['show']) {
    $this->_sections['cnt']['total'] = min(ceil(($this->_sections['cnt']['step'] > 0 ? $this->_sections['cnt']['loop'] - $this->_sections['cnt']['start'] : $this->_sections['cnt']['start']+1)/abs($this->_sections['cnt']['step'])), $this->_sections['cnt']['max']);
    if ($this->_sections['cnt']['total'] == 0)
        $this->_sections['cnt']['show'] = false;
} else
    $this->_sections['cnt']['total'] = 0;
if ($this->_sections['cnt']['show']):

            for ($this->_sections['cnt']['index'] = $this->_sections['cnt']['start'], $this->_sections['cnt']['iteration'] = 1;
                 $this->_sections['cnt']['iteration'] <= $this->_sections['cnt']['total'];
                 $this->_sections['cnt']['index'] += $this->_sections['cnt']['step'], $this->_sections['cnt']['iteration']++):
$this->_sections['cnt']['rownum'] = $this->_sections['cnt']['iteration'];
$this->_sections['cnt']['index_prev'] = $this->_sections['cnt']['index'] - $this->_sections['cnt']['step'];
$this->_sections['cnt']['index_next'] = $this->_sections['cnt']['index'] + $this->_sections['cnt']['step'];
$this->_sections['cnt']['first']      = ($this->_sections['cnt']['iteration'] == 1);
$this->_sections['cnt']['last']       = ($this->_sections['cnt']['iteration'] == $this->_sections['cnt']['total']);
?>
				<option value="<?php echo $this->_sections['cnt']['index']; ?>
"<?php if ($this->_tpl_vars['set_e_month'] == $this->_sections['cnt']['index']): ?> selected="selected"<?php endif; ?>><?php echo $this->_sections['cnt']['index']; ?>
月</option>
<?php endfor; endif; ?>
			</select>
			<select name="e_day">
<?php unset($this->_sections['cnt']);
$this->_sections['cnt']['name'] = 'cnt';
$this->_sections['cnt']['start'] = (int)1;
$this->_sections['cnt']['loop'] = is_array($_loop=32) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$this->_sections['cnt']['show'] = true;
$this->_sections['cnt']['max'] = $this->_sections['cnt']['loop'];
$this->_sections['cnt']['step'] = 1;
if ($this->_sections['cnt']['start'] < 0)
    $this->_sections['cnt']['start'] = max($this->_sections['cnt']['step'] > 0 ? 0 : -1, $this->_sections['cnt']['loop'] + $this->_sections['cnt']['start']);
else
    $this->_sections['cnt']['start'] = min($this->_sections['cnt']['start'], $this->_sections['cnt']['step'] > 0 ? $this->_sections['cnt']['loop'] : $this->_sections['cnt']['loop']-1);
if ($this->_sections['cnt']['show']) {
    $this->_sections['cnt']['total'] = min(ceil(($this->_sections['cnt']['step'] > 0 ? $this->_sections['cnt']['loop'] - $this->_sections['cnt']['start'] : $this->_sections['cnt']['start']+1)/abs($this->_sections['cnt']['step'])), $this->_sections['cnt']['max']);
    if ($this->_sections['cnt']['total'] == 0)
        $this->_sections['cnt']['show'] = false;
} else
    $this->_sections['cnt']['total'] = 0;
if ($this->_sections['cnt']['show']):

            for ($this->_sections['cnt']['index'] = $this->_sections['cnt']['start'], $this->_sections['cnt']['iteration'] = 1;
                 $this->_sections['cnt']['iteration'] <= $this->_sections['cnt']['total'];
                 $this->_sections['cnt']['index'] += $this->_sections['cnt']['step'], $this->_sections['cnt']['iteration']++):
$this->_sections['cnt']['rownum'] = $this->_sections['cnt']['iteration'];
$this->_sections['cnt']['index_prev'] = $this->_sections['cnt']['index'] - $this->_sections['cnt']['step'];
$this->_sections['cnt']['index_next'] = $this->_sections['cnt']['index'] + $this->_sections['cnt']['step'];
$this->_sections['cnt']['first']      = ($this->_sections['cnt']['iteration'] == 1);
$this->_sections['cnt']['last']       = ($this->_sections['cnt']['iteration'] == $this->_sections['cnt']['total']);
?>
				<option value="<?php echo $this->_sections['cnt']['index']; ?>
"<?php if ($this->_tpl_vars['set_e_day'] == $this->_sections['cnt']['index']): ?> selected="selected"<?php endif; ?>><?php echo $this->_sections['cnt']['index']; ?>
日</option>
<?php endfor; endif; ?>
			</select>
		</td>
	</tr>
	<tr>
		<td colspan="3">
			<input type="submit" value="検索" />
			<input type="hidden" name="mode" value="search" />
		</td>
	</tr>
</table>
</form>

<table cellpadding="0" cellspacing="0">
	<tr>
		<th colspan="4">【該当<?php echo $this->_tpl_vars['list_count']; ?>
件】</th>
	</tr>
	<tr>
		<th>ID</th>
		<th>広告主グループ名</th>
		<th>登録日時</th>
		<th>
			<form method="POST" action="<?php echo $this->_supers['server']['PHP_SELF']; ?>
">
				<input type="submit" value="新規登録" />
				<input type="hidden" name="mode" value="new_regist" />
			</form>
		</th>
	</tr>
<?php $_from = $this->_tpl_vars['list']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['list'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['list']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['data']):
        $this->_foreach['list']['iteration']++;
?>
	<tr>
		<td><?php echo $this->_tpl_vars['data']['id']; ?>
</td>
		<td><?php echo ((is_array($_tmp=$this->_tpl_vars['data']['name'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
</td>
		<td><?php echo $this->_tpl_vars['data']['created_at']; ?>
</td>
		<td>
			<form method="POST" action="<?php echo $this->_supers['server']['PHP_SELF']; ?>
">
				<input type="submit" value="編集" />
				<input type="hidden" name="mode" value="edit" />
				<input type="hidden" name="id" value="<?php echo $this->_tpl_vars['data']['id']; ?>
" />
			</form>
			<form method="POST" action="<?php echo $this->_supers['server']['PHP_SELF']; ?>
">
				<input type="submit" value="削除" onclick="return confirm('削除してよろしいですか？');" />
				<input type="hidden" name="mode" value="delete" />
				<input type="hidden" name="id" value="<?php echo $this->_tpl_vars['data']['id']; ?>
" />
			</form>
		</td>
	</tr>
<?php endforeach; endif; unset($_from); ?>
</table>

</div><!-- contents -->

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => './hooter.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>