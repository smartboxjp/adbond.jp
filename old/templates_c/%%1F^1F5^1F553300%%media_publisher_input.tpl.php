<?php /* Smarty version 2.6.25, created on 2010-05-10 22:05:31
         compiled from ./media_publisher_input.tpl */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'cat', './media_publisher_input.tpl', 1, false),array('modifier', 'htmlspecialchars', './media_publisher_input.tpl', 30, false),)), $this); ?>
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => './header.tpl', 'smarty_include_vars' => array('page_title' => ((is_array($_tmp=((is_array($_tmp='媒体発行者(')) ? $this->_run_mod_handler('cat', true, $_tmp, $this->_tpl_vars['sub_title']) : smarty_modifier_cat($_tmp, $this->_tpl_vars['sub_title'])))) ? $this->_run_mod_handler('cat', true, $_tmp, ')') : smarty_modifier_cat($_tmp, ')')))));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<!-- Menu -->
<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => './menu.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>

<div id="my_contents">

<h2>媒体発行者(<?php echo $this->_tpl_vars['sub_title']; ?>
)</h2>

<div id=message>
<?php if ($this->_tpl_vars['error_message'] != ''): ?>
<div id="error_message">
<h3>ERROR:<?php echo $this->_tpl_vars['error_message']; ?>
</h3>
</div>
<?php endif; ?>
<?php if ($this->_tpl_vars['info_message'] != ''): ?>
<div id="info_message">
<h3>INFO:<?php echo $this->_tpl_vars['info_message']; ?>
</h3>
</div>
<?php endif; ?>
</div><!-- message -->

<form method="POST" action="<?php echo $this->_supers['server']['PHP_SELF']; ?>
">
<table cellpadding="0" cellspacing="0">
	<tr>
		<th colspan="2">媒体グループ</th>
		<td>
			<select name="media_group_id">
			<?php $_from = $this->_tpl_vars['media_group_array']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['media_group_list'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['media_group_list']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['data']):
        $this->_foreach['media_group_list']['iteration']++;
?>
				<option value="<?php echo $this->_tpl_vars['data']['id']; ?>
"<?php if ($this->_tpl_vars['form_data']['media_group_id'] == $this->_tpl_vars['data']['id']): ?> selected="selected"<?php endif; ?>><?php echo ((is_array($_tmp=$this->_tpl_vars['data']['name'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
</option>
			<?php endforeach; endif; unset($_from); ?>
			</select>
		</td>
	</tr>
	<tr>
		<th colspan="2">ログインID ※6-16桁</th>
		<td>
			<input type="text" size="20" name="login_id" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['login_id'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />
		</td>
	</tr>
	<tr>
		<th colspan="2">パスワード ※6-16桁</th>
		<td>
			<input type="text" size="20" name="login_pass" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['login_pass'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />
		</td>
	</tr>
	<tr>
		<th colspan="2">企業名/個人名</th>
		<td>
			<input type="text" size="50" name="publisher_name" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['publisher_name'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />
		</td>
	</tr>
	<tr>
		<th colspan="2">担当者名 ※法人の場合のみ</th>
		<td>
			<input type="text" size="50" name="contact_person" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['contact_person'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />
		</td>
	</tr>
	<tr>
		<th colspan="2">TEL</th>
		<td>
			<input type="text" size="30" name="tel" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['tel'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />
		</td>
	</tr>
	<tr>
		<th colspan="2">FAX</th>
		<td>
			<input type="text" size="30" name="fax" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['fax'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />
		</td>
	</tr>
	<tr>
		<th colspan="2">メールアドレス</th>
		<td>
			<input type="text" size="30" name="email" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['email'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />
		</td>
	</tr>
	<tr>
		<th colspan="2">郵便番号</th>
		<td>
			<input type="text" size="10" name="zipcode1" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['zipcode1'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" /> - <input type="text" size="10" name="zipcode2" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['zipcode2'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />
		</td>
	</tr>
	<tr>
		<th colspan="2">都道府県</th>
		<td>
			<select name="pref">
			<?php $_from = $this->_tpl_vars['pref_array']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }$this->_foreach['pref_list'] = array('total' => count($_from), 'iteration' => 0);
if ($this->_foreach['pref_list']['total'] > 0):
    foreach ($_from as $this->_tpl_vars['key'] => $this->_tpl_vars['data']):
        $this->_foreach['pref_list']['iteration']++;
?>
				<option value="<?php echo $this->_tpl_vars['data']['id']; ?>
"<?php if ($this->_tpl_vars['data']['id'] == $this->_tpl_vars['form_data']['pref']): ?> selected="selected"<?php endif; ?>><?php echo ((is_array($_tmp=$this->_tpl_vars['data']['name'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
</option>
			<?php endforeach; endif; unset($_from); ?>
			</select>
		</td>
	</tr>
	<tr>
		<th colspan="2">住所１（市区町村）</th>
		<td>
			<input type="text" size="50" name="address1" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['address1'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />
		</td>
	</tr>
	<tr>
		<th colspan="2">住所２（番地、建物）</th>
		<td>
			<input type="text" size="50" name="address2" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['address2'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />
		</td>
	</tr>
	<tr>
		<th colspan="2">振込先種別</th>
		<td>
			<input type="radio" name="transfer_type" value="1"<?php if ($this->_tpl_vars['form_data']['transfer_type'] == 1 || $this->_tpl_vars['form_data']['transfer_type'] == ""): ?> checked="checked"<?php endif; ?> /><label>自行</label>
			<input type="radio" name="transfer_type" value="2"<?php if ($this->_tpl_vars['form_data']['transfer_type'] == 2): ?> checked="checked"<?php endif; ?> /><label>他行</label>
			<input type="radio" name="transfer_type" value="3"<?php if ($this->_tpl_vars['form_data']['transfer_type'] == 3): ?> checked="checked"<?php endif; ?> /><label>郵便局</label>
		</td>
	</tr>
	<tr>
		<th colspan="2">振込先銀行名</th>
		<td>
			<input type="text" size="50" name="bank_name" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['bank_name'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />
		</td>
	</tr>
	<tr>
		<th colspan="2">支店名</th>
		<td>
			<input type="text" size="50" name="branch_name" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['branch_name'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />
		</td>
	</tr>
	<tr>
		<th colspan="2">口座種別</th>
		<td>
			<input type="radio" name="account_type" value="1"<?php if ($this->_tpl_vars['form_data']['account_type'] == 1 || $this->_tpl_vars['form_data']['account_type'] == ""): ?> checked="checked"<?php endif; ?> /><label>普通</label>
			<input type="radio" name="account_type" value="2"<?php if ($this->_tpl_vars['form_data']['account_type'] == 2): ?> checked="checked"<?php endif; ?> /><label>当座</label>
		</td>
	</tr>
	<tr>
		<th colspan="2">口座名義</th>
		<td>
			<input type="text" size="50" name="account_holder" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['account_holder'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />
		</td>
	</tr>
	<tr>
		<th colspan="2">口座番号</th>
		<td>
			<input type="text" size="50" name="account_number" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['account_number'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />
		</td>
	</tr>
	<tr>
		<th colspan="2">郵便局振込先名義</th>
		<td>
			<input type="text" size="50" name="postal_account_holder" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['postal_account_holder'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />
		</td>
	</tr>
	<tr>
		<th colspan="2">郵便局口座番号</th>
		<td>
			<input type="text" size="50" name="postal_account_number" value="<?php echo ((is_array($_tmp=$this->_tpl_vars['form_data']['postal_account_number'])) ? $this->_run_mod_handler('htmlspecialchars', true, $_tmp, @ENT_QUOTES) : htmlspecialchars($_tmp, @ENT_QUOTES)); ?>
" />
		</td>
	</tr>
	<tr>
		<th colspan="2">ステータス</th>
		<td>
			<input type="radio" name="status" value="1"<?php if ($this->_tpl_vars['form_data']['status'] == 1 || $this->_tpl_vars['form_data']['status'] == ""): ?> checked="checked"<?php endif; ?> /><label>仮登録</label>
			<input type="radio" name="status" value="2"<?php if ($this->_tpl_vars['form_data']['status'] == 2): ?> checked="checked"<?php endif; ?> /><label>正規</label>
			<input type="radio" name="status" value="3"<?php if ($this->_tpl_vars['form_data']['status'] == 3): ?> checked="checked"<?php endif; ?> /><label>退会</label>
		</td>
	</tr>
	<tr>
		<td colspan="3">
			<input type="submit" value="登録" />
			<input type="hidden" name="mode" value="<?php echo $this->_tpl_vars['mode']; ?>
" />
			<input type="hidden" name="id" value="<?php echo $this->_tpl_vars['form_data']['id']; ?>
" />
		</td>
	</tr>
</table>
</form>

</div><!-- contents -->

<?php $_smarty_tpl_vars = $this->_tpl_vars;
$this->_smarty_include(array('smarty_include_tpl_file' => './hooter.tpl', 'smarty_include_vars' => array()));
$this->_tpl_vars = $_smarty_tpl_vars;
unset($_smarty_tpl_vars);
 ?>