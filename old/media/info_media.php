<?php
// 共通設定
require_once( '../common/CommonWebBase.php' );
require_once( '../common/CommonDao.php' );
require_once( '../dao/MediaLoginUserDao.php' );
require_once( '../dto/MediaLoginUser.php' );
require_once( '../dao/MediaPublisherDao.php' );
require_once( '../dto/MediaPublisher.php' );
require_once( '../dao/MediaDao.php' );
require_once( '../dto/Media.php' );
require_once( '../dao/MediaCategoryDao.php' );
require_once( '../dto/MediaCategory.php' );

session_start();

if(isset($_SESSION['media_logon_token']) && $_SESSION['media_logon_token'] != ''){
	$media_login_user_dao = new MediaLoginUserDao();
	$media_login_user = new MediaLoginUser();
	$media_login_user = $_SESSION['media_login_user'];

	$login_user_id = $media_login_user->getid();
	$user_name = $media_login_user->getUserName();
	$login_id = $media_login_user->getLoginId();
	$login_pass = $media_login_user->getLoginPass();

	//登録者情報、口座情報取得
	$media_publisher_dao = new MediaPublisherDao();
	$media_publisher = new MediaPublisher();
	$media_publisher = $media_publisher_dao->getMediaPublisherByLoginUserId($login_user_id);
	$media_publisher_id = $media_publisher->getId();

	// Smartyオブジェクト取得
	$smarty =& getSmartyObj();

	$smarty->assign("user_name", $user_name);

	$common_dao = new CommonDao();
	$media_dao = new MediaDao();

	//媒体カテゴリー
	$media_category_dao = new MediaCategoryDao();
	$media_category_array = array();
	foreach($media_category_dao->getAllMediaCategory() as $val){
		$row_array = array('id' => $val->getId(), 'name' => $val->getName());
		$media_category_array[$val->getId()] = $row_array;
	}
	$smarty->assign("media_category_array", $media_category_array);

	if(isset($_GET['mode'])) {
		$mode = $_GET['mode'];
	} elseif(isset($_POST['mode'])) {
		$mode = $_POST['mode'];
	}

	if(isset($_GET['id'])) {
		$id = $_GET['id'];
	} else {
		$id = do_escape_quotes($_POST['id']);
	}

	$media_category_id = do_escape_quotes($_POST['media_category_id']);
	$media_name = do_escape_quotes($_POST['media_name']);
	$support_docomo = do_escape_quotes($_POST['support_docomo']);
	$support_softbank = do_escape_quotes($_POST['support_softbank']);
	$support_au = do_escape_quotes($_POST['support_au']);
	$support_pc = do_escape_quotes($_POST['support_pc']);
	$site_url_docomo = do_escape_quotes($_POST['site_url_docomo']);
	$site_url_softbank = do_escape_quotes($_POST['site_url_softbank']);
	$site_url_au = do_escape_quotes($_POST['site_url_au']);
	$site_url_pc = do_escape_quotes($_POST['site_url_pc']);
	$media_type = do_escape_quotes($_POST['media_type']);
	$page_view_day = do_escape_quotes($_POST['page_view_day']);
	$site_outline = do_escape_quotes($_POST['site_outline']);

	if($mode != ''){
		if($mode == 'new_regist'){
			$smarty->assign("mode", 'insert_commit');
			$smarty->assign("sub_title", '新規追加');

			// ページを表示
			$smarty->display("./media_info_input.tpl");
			exit();
		}elseif($mode == 'insert_commit'){

			$error_flag = 0;

			if($media_name == "") {
				$error_message = "サイト名を入力してください。";
				$error_flag = 1;
			}

			if($error_flag == 0) {
				$media_dao->transaction_start();

				$media = new Media();
				$media->setMediaPublisherId($media_publisher_id);
				$media->setMediaCategoryId($media_category_id);
				$media->setMediaName($media_name);
				$media->setSupportDocomo($support_docomo);
				$media->setSupportSoftbank($support_softbank);
				$media->setSupportAu($support_au);
				$media->setSupportPc($support_pc);
				$media->setSiteUrlDocomo($site_url_docomo);
				$media->setSiteUrlSoftbank($site_url_softbank);
				$media->setSiteUrlAu($site_url_au);
				$media->setSiteUrlPc($site_url_pc);
				$media->setMediaType($media_type);
				$media->setPageViewDay($page_view_day);
				$media->setSiteOutline($site_outline);

				//INSERTを実行
				$db_result = $media_dao->insertMedia($media, $result_message);
				if($db_result) {
					$media_dao->transaction_end();

					$smarty->assign("info_message", $result_message);

					// ページを表示
					header('Location: ./info.php');
					exit();
				} else {
					$media_dao->transaction_rollback();

					$smarty->assign("error_message", $result_message);

					$form_data = array('id' => $id,
										'media_publisher_id' => $media_publisher_id,
										'media_category_id' => $media_category_id,
										'media_name' => $media_name,
										'support_docomo' => $support_docomo,
										'support_softbank' => $support_softbank,
										'support_au' => $support_au,
										'support_pc' => $support_pc,
										'site_url_docomo' => $site_url_docomo,
										'site_url_softbank' => $site_url_softbank,
										'site_url_au' => $site_url_au,
										'site_url_pc' => $site_url_pc,
										'media_type' => $media_type,
										'page_view_day' => $page_view_day,
										'site_outline' => $site_outline);

					$smarty->assign("form_data", $form_data);

					$smarty->assign("mode", 'insert_commit');
					$smarty->assign("sub_title", '新規追加');

					// ページを表示
					$smarty->display("./media_info_input.tpl");
					exit();
				}
			} else {
				$smarty->assign("error_message", $error_message);

				$form_data = array('id' => $id,
									'media_publisher_id' => $media_publisher_id,
									'media_category_id' => $media_category_id,
									'media_name' => $media_name,
									'support_docomo' => $support_docomo,
									'support_softbank' => $support_softbank,
									'support_au' => $support_au,
									'support_pc' => $support_pc,
									'site_url_docomo' => $site_url_docomo,
									'site_url_softbank' => $site_url_softbank,
									'site_url_au' => $site_url_au,
									'site_url_pc' => $site_url_pc,
									'media_type' => $media_type,
									'page_view_day' => $page_view_day,
									'site_outline' => $site_outline);

				$smarty->assign("form_data", $form_data);

				$smarty->assign("mode", 'insert_commit');
				$smarty->assign("sub_title", '新規追加');

				// ページを表示
				$smarty->display("./media_info_input.tpl");
				exit();
			}
		}elseif($mode == 'edit'){
			$media = new Media;
			$media = $media_dao->getMediaById($id);

			if(!is_null($media)) {
				$form_data = array('id' => $media->getId(),
									'media_publisher_id' => $media->getMediaPublisherId(),
									'media_category_id' => $media->getMediaCategoryId(),
									'media_name' => $media->getMediaName(),
									'support_docomo' => $media->getSupportDocomo(),
									'support_softbank' => $media->getSupportSoftbank(),
									'support_au' => $media->getSupportAu(),
									'support_pc' => $media->getSupportPc(),
									'site_url_docomo' => $media->getSiteUrlDocomo(),
									'site_url_softbank' => $media->getSiteUrlSoftbank(),
									'site_url_au' => $media->getSiteUrlAu(),
									'site_url_pc' => $media->getSiteUrlPc(),
									'media_type' => $media->getMediaType(),
									'page_view_day' => $media->getPageViewDay(),
									'site_outline' => $media->getSiteOutline());

				$smarty->assign("form_data", $form_data);

				$smarty->assign("mode", 'update_commit');
				$smarty->assign("sub_title", '編集');

				// ページを表示
				$smarty->display("./media_info_input.tpl");
				exit();
			} else {
				$error_message .= "該当するデータはありません。";
				$smarty->assign("error_message", $error_message);

				$smarty->assign("mode", 'insert_commit');
				$smarty->assign("sub_title", '新規追加');

				// ページを表示
				$smarty->display("./media_info_input.tpl");
				exit();
			}
		}elseif($mode == 'update_commit'){

			$error_flag = 0;

			if($media_name == "") {
				$error_message = "サイト名を入力してください。";
				$error_flag = 1;
			}

			if($error_flag == 0) {
				$media_dao->transaction_start();

				$media = new Media();
				$media->setId($id);
				$media->setMediaPublisherId($media_publisher_id);
				$media->setMediaCategoryId($media_category_id);
				$media->setMediaName($media_name);
				$media->setSupportDocomo($support_docomo);
				$media->setSupportSoftbank($support_softbank);
				$media->setSupportAu($support_au);
				$media->setSupportPc($support_pc);
				$media->setSiteUrlDocomo($site_url_docomo);
				$media->setSiteUrlSoftbank($site_url_softbank);
				$media->setSiteUrlAu($site_url_au);
				$media->setSiteUrlPc($site_url_pc);
				$media->setMediaType($media_type);
				$media->setPageViewDay($page_view_day);
				$media->setSiteOutline($site_outline);
				$media->setStatus(2);

				//UPDATEを実行
				$db_result = $media_dao->updateMedia($media, $result_message);
				if($db_result) {
					$media_dao->transaction_end();

					$smarty->assign("info_message", $result_message);

					// ページを表示
					header('Location: ./info.php');
					exit();
				} else {
					$media_dao->transaction_rollback();

					$smarty->assign("error_message", $result_message);

					$form_data = array('id' => $id,
										'media_publisher_id' => $media_publisher_id,
										'media_category_id' => $media_category_id,
										'media_name' => $media_name,
										'support_docomo' => $support_docomo,
										'support_softbank' => $support_softbank,
										'support_au' => $support_au,
										'support_pc' => $support_pc,
										'site_url_docomo' => $site_url_docomo,
										'site_url_softbank' => $site_url_softbank,
										'site_url_au' => $site_url_au,
										'site_url_pc' => $site_url_pc,
										'media_type' => $media_type,
										'page_view_day' => $page_view_day,
										'site_outline' => $site_outline);

					$smarty->assign("form_data", $form_data);

					$smarty->assign("mode", 'update_commit');
					$smarty->assign("sub_title", '編集');

					// ページを表示
					$smarty->display("./media_info_input.tpl");
					exit();
				}
			} else {
				$smarty->assign("error_message", $error_message);

				$form_data = array('id' => $id,
									'media_publisher_id' => $media_publisher_id,
									'media_category_id' => $media_category_id,
									'media_name' => $media_name,
									'support_docomo' => $support_docomo,
									'support_softbank' => $support_softbank,
									'support_au' => $support_au,
									'support_pc' => $support_pc,
									'site_url_docomo' => $site_url_docomo,
									'site_url_softbank' => $site_url_softbank,
									'site_url_au' => $site_url_au,
									'site_url_pc' => $site_url_pc,
									'media_type' => $media_type,
									'page_view_day' => $page_view_day,
									'site_outline' => $site_outline);

				$smarty->assign("form_data", $form_data);

				$smarty->assign("mode", 'update_commit');
				$smarty->assign("sub_title", '編集');

				// ページを表示
				$smarty->display("./media_info_input.tpl");
				exit();
			}
		} else {
			header('Location: ../index.php?error=1');
			exit();
		}
	} else {
		header('Location: ../index.php?error=1');
		exit();
	}
}else{
	header('Location: ../index.php?error=1');
	exit();
}

function do_escape_quotes($str){
	//magic_quotesが有効ならクウォート部分を除去
	if(get_magic_quotes_gpc()){
		$str = stripslashes($str);
	}
	return $str;
}
?>