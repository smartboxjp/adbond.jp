<?php
// 共通設定
require_once( '../common/CommonAdminBase.php' );
require_once( '../common/CommonDao.php' );
require_once( '../dto/LoginUser.php' );

session_start();

if(isset($_POST['media_id']) && $_POST['media_id'] != "") {
	$media_id = $_POST['media_id'];
	$advert_id = $_POST['advert_id'];

	$common_dao = new CommonDao();

	$list_sql = " SELECT a.*, ac.name as advert_category_name, "
				. " cl.media_id, cl.status as connect_status "
				. " FROM advert as a "
				. " LEFT JOIN advert_categories as ac on a.advert_category_id = ac.id "
				. " LEFT JOIN connect_logs as cl on a.id = cl.advert_id "
				. " WHERE a.deleted_at is NULL "
				. " ORDER BY a.id ASC ";

	$db_result = $common_dao->db_query($list_sql);
	if($db_result){
//		$outputFile = "../logs/output.cgi";
//		touch($outputFile);
//
//		$fp = fopen($outputFile, "w");
//
//		mb_internal_encoding("UTF-8");
//		mb_detect_order("ASCII, JIS, UTF-8, eucjp-win, sjis-win, EUC-JP, SJIS");
//
//		$download_date = date("Ymd");
//
//		$file_name = "link_$download_date.csv";
//		$csv_array[] = array('広告ID',
//							'広告サイト名',
//							'クリック単価',
//							'アクション単価(docomo)',
//							'アクション単価(softbank)',
//							'アクション単価(au)',
//							'アクション単価(pc)',
//							'キャリア(docomo)',
//							'キャリア(softbank)',
//							'キャリア(au)',
//							'キャリア(pc)',
//							'サイト種別',
//							'カテゴリー',
//							'ポイントバック',
//							'出稿終了日',
//							'広告原稿1(テキスト)',
//							'広告原稿2(テキスト)',
//							'広告原稿3(テキスト)',
//							'広告原稿4(テキスト)',
//							'広告原稿5(テキスト)',
//							'広告原稿1(イメージ)',
//							'広告原稿2(イメージ)',
//							'広告原稿3(イメージ)',
//							'広告原稿4(イメージ)',
//							'広告原稿5(イメージ)',
//							'広告原稿1(メール)',
//							'広告原稿2(メール)',
//							'広告原稿3(メール)',
//							'広告原稿4(メール)',
//							'広告原稿5(メール)',
//							'広告URL');
//
//		foreach($db_result as $row) {
//			$data1 = $row['id'];
//			$data2 = $row['advert_name'];
//			$data3 = $row['click_price_media'];
//			$data4 = $row['action_price_media_docomo_1'];
//			$data5 = $row['action_price_media_softbank_1'];
//			$data6 = $row['action_price_media_au_1'];
//			$data7 = $row['action_price_media_pc_1'];
//			$data8 = ($row['support_docomo'] == 1) ? "○" : "-";
//			$data9 = ($row['support_softbank'] == 1) ? "○" : "-";
//			$data10 = ($row['support_au'] == 1) ? "○" : "-";
//			$data11 = ($row['support_pc'] == 1) ? "○" : "-";
//			$data12 = ($row['content_type'] == 1) ? "一般" : "公式";
//			$data13 = $row['advert_categeory_name'];
//			$data14 = ($row['point_back_flag'] == 1) ? "○" : "-";
//			$data15 = ($row['unrestraint_flag'] == 1) ? "無制限" : $row['advert_end_date'];
//			$data16 = $row['ms_text_1'];
//			$data17 = $row['ms_text_2'];
//			$data18 = $row['ms_text_3'];
//			$data19 = $row['ms_text_4'];
//			$data20 = $row['ms_text_5'];
//			$data21 = $row['ms_image_url_1'];
//			$data22 = $row['ms_image_url_2'];
//			$data23 = $row['ms_image_url_3'];
//			$data24 = $row['ms_image_url_4'];
//			$data25 = $row['ms_image_url_5'];
//			$data26 = $row['ms_email_1'];
//			$data27 = $row['ms_email_2'];
//			$data28 = $row['ms_email_3'];
//			$data29 = $row['ms_email_4'];
//			$data30 = $row['ms_email_5'];
//			$data31 = "http://adbond.jp/action/click.php?m=$media_id&a=".$row['id'];
//
//			$csv_array[] = array($data1, $data2, $data3, $data4, $data5, $data6, $data7, $data8, $data9, $data10,
//								$data11, $data12, $data13, $data14, $data15, $data16, $data17, $data18, $data19, $data20,
//								$data21, $data22, $data23, $data24, $data25, $data26, $data27, $data28, $data29, $data30, $data31);
//		}
//
//		mb_convert_variables("SJIS-win", "UTF-8", $csv_array);
//		foreach($csv_array as $line) {
//			fputcsv($fp, $line);
//		}
//
//		fclose($fp);
//
//		header("Content-Type: application/csv");
//		header("Content-Disposition: attachment; filename=".$file_name);
//		header("Content-Length:".filesize($outputFile));
//		readfile($outputFile);
//		exit();
	} else {
		exit();
	}
} else {
	exit();
}

function do_escape_quotes($str){
	//magic_quotesが有効ならクウォート部分を除去
	if(get_magic_quotes_gpc()){
		$str = stripslashes($str);
	}
	return $str;
}
?>