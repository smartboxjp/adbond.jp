<?php
// 共通設定
require_once( '../common/CommonWebBase.php' );
require_once( '../common/CommonDao.php' );
require_once( '../dao/MediaLoginUserDao.php' );
require_once( '../dto/MediaLoginUser.php' );
require_once( '../dao/MediaPublisherDao.php' );
require_once( '../dto/MediaPublisher.php' );

session_start();

if(isset($_SESSION['media_logon_token']) && $_SESSION['media_logon_token'] != ''){
	$media_login_user_dao = new MediaLoginUserDao();
	$media_login_user = new MediaLoginUser();
	$media_login_user = $_SESSION['media_login_user'];

	$login_user_id = $media_login_user->getid();
	$user_name = $media_login_user->getUserName();
	$login_id = $media_login_user->getLoginId();
	$login_pass = $media_login_user->getLoginPass();

	//登録者情報、口座情報取得
	$media_publisher_dao = new MediaPublisherDao();
	$media_publisher = new MediaPublisher();
	$media_publisher = $media_publisher_dao->getMediaPublisherByLoginUserId($login_user_id);
	$media_publisher_id = $media_publisher->getId();

	// Smartyオブジェクト取得
	$smarty =& getSmartyObj();

	$smarty->assign("user_name", $user_name);

	$date_type = (isset($_POST['date_type'])) ? do_escape_quotes($_POST['date_type']) : 1;
	$carrier = (isset($_POST['carrier'])) ? do_escape_quotes($_POST['carrier']) : 1;

	$now_date = getdate();
	$s_year = (isset($_POST['s_year'])) ? do_escape_quotes($_POST['s_year']) : $now_date['year'];
	$s_month = (isset($_POST['s_month'])) ? do_escape_quotes($_POST['s_month']) : $now_date['mon'];
	$s_day = (isset($_POST['s_day'])) ? do_escape_quotes($_POST['s_day']) : $now_date['mday'];

	$smarty->assign("date_type", $date_type);
	$smarty->assign("carrier", $carrier);

	$smarty->assign("set_s_year", $s_year);
	$smarty->assign("set_s_month", $s_month);
	$smarty->assign("set_s_day", $s_day);

	$common_dao = new CommonDao();

	$sql = " SELECT * "
			. " FROM action_logs "
			. " WHERE deleted_at is NULL "
			. " AND media_publisher_id = $media_publisher_id "
			. " AND (status = 1 OR status = 2) ";

	if($date_type == 2) {
		$sql .= " AND DATE_FORMAT(created_at,'%Y%c') = '$s_year$s_month' ";
	}

	$sql .= " ORDER BY created_at DESC ";

	$db_result = $common_dao->db_query($sql);
	if($db_result){

		foreach($db_result as $row) {
			if($date_type == 1) {
				$date = date("Y年m月", strtotime($row['created_at']));
			} elseif($date_type == 2) {
				$date = date("Y年m月d日", strtotime($row['created_at']));
			}

			if($row['carrier_id'] == 1) {
				$c_id = "docomo";
			} elseif($row['carrier_id'] == 2) {
				$c_id = "softbank";
			} elseif($row['carrier_id'] == 3) {
				$c_id = "au";
			} elseif($row['carrier_id'] == 4) {
				$c_id = "pc";
			}

			if($carrier == 1) {
				$summary[$date]['summary_date'] = $date;

				$summary[$date]['click_price'] += $row['click_price_media'];
				$summary[$date]['action_price'] += $row['action_price_media'];

				$summary[$date]['total_price'] += $row['click_price_media'];
				$summary[$date]['total_price'] += $row['action_price_media'];

				$summary[$date]['click_count'] += 1;
				if($row['status'] == 2) {
					$summary[$date]['action_count'] += 1;
				}
			} elseif($carrier == 2) {
				$summary[$date]['summary_date'] = $date;

				$summary[$date][$c_id]['click_price'] += $row['click_price_media'];
				$summary[$date][$c_id]['action_price'] += $row['action_price_media'];

				$summary[$date][$c_id]['total_price'] += $row['click_price_media'];
				$summary[$date][$c_id]['total_price'] += $row['action_price_media'];

				$summary[$date][$c_id]['click_count'] += 1;
				if($row['status'] == 2) {
					$summary[$date][$c_id]['action_count'] += 1;
				}
			}

			$all['click_price'] += $row['click_price_media'];
			$all['action_price'] += $row['action_price_media'];

			$all['total_price'] += $row['click_price_media'];
			$all['total_price'] += $row['action_price_media'];

			$all['click_count'] += 1;
			if($row['status'] == 2) {
				$all['action_count'] += 1;
			}
		}

		$smarty->assign("summary", $summary);
		$smarty->assign("all", $all);
	}else{
		$error_message .= "ＤＢからのデータの取得に失敗しました。(su0000)";
	}
	$smarty->assign("error_message", $error_message);

	// ページを表示
	$smarty->display("./media_report.tpl");
	exit();
}else{
	header('Location: ../index.php?error=1');
	exit();
}

function do_escape_quotes($str){
	//magic_quotesが有効ならクウォート部分を除去
	if(get_magic_quotes_gpc()){
		$str = stripslashes($str);
	}
	return $str;
}
?>