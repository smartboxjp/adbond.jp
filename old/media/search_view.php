<?php
// 共通設定
require_once( '../common/CommonWebBase.php' );
require_once( '../common/CommonDao.php' );
require_once( '../dao/MediaLoginUserDao.php' );
require_once( '../dto/MediaLoginUser.php' );
require_once( '../dao/MediaPublisherDao.php' );
require_once( '../dto/MediaPublisher.php' );
require_once( '../dao/MediaDao.php' );
require_once( '../dto/Media.php' );
require_once( '../dao/AdvertCategoryDao.php' );
require_once( '../dto/AdvertCategory.php' );
require_once( '../dao/ConnectLogDao.php' );
require_once( '../dto/ConnectLog.php' );

session_start();

if(isset($_SESSION['media_logon_token']) && $_SESSION['media_logon_token'] != ''){
	$media_login_user_dao = new MediaLoginUserDao();
	$media_login_user = new MediaLoginUser();
	$media_login_user = $_SESSION['media_login_user'];

	$login_user_id = $media_login_user->getid();
	$user_name = $media_login_user->getUserName();
	$login_id = $media_login_user->getLoginId();
	$login_pass = $media_login_user->getLoginPass();

	// Smartyオブジェクト取得
	$smarty =& getSmartyObj();

	$smarty->assign("user_name", $user_name);

	$image_directory = "../advert_image/";

	$media_id = do_escape_quotes($_POST['media_id']);
	$smarty->assign("media_id", $media_id);

	$advert_id = do_escape_quotes($_POST['advert_id']);
	$smarty->assign("advert_id", $advert_id);

	$common_dao = new CommonDao();
	$sql = " SELECT a.*, ac.client_name as advert_client_name, aa.name as advert_category_name "
			. " FROM advert as a "
			. " LEFT JOIN advert_clients as ac on a.advert_client_id = ac.id "
			. " LEFT JOIN advert_categories as aa on a.advert_category_id = aa.id "
			. " WHERE a.deleted_at is NULL "
			. " AND a.id = '$advert_id' ";

	$db_result = $common_dao->db_query($sql);
	if($db_result){
		$data = $db_result[0];
		$smarty->assign("data", $data);

		if($data['ms_image_type_1'] == 1) {
			$ms_image_path_1 = $image_directory . $data['ms_image_url_1'];
		} else {
			$ms_image_path_1 = $data['ms_image_url_1'];
		}
		$smarty->assign("ms_image_path_1", $ms_image_path_1);

		if($data['ms_image_type_2'] == 1) {
			$ms_image_path_2 = $image_directory . $data['ms_image_url_2'];
		} else {
			$ms_image_path_2 = $data['ms_image_url_2'];
		}
		$smarty->assign("ms_image_path_2", $ms_image_path_2);

		if($data['ms_image_type_3'] == 1) {
			$ms_image_path_3 = $image_directory . $data['ms_image_url_3'];
		} else {
			$ms_image_path_3 = $data['ms_image_url_3'];
		}
		$smarty->assign("ms_image_path_3", $ms_image_path_3);

		if($data['ms_image_type_4'] == 1) {
			$ms_image_path_4 = $image_directory . $data['ms_image_url_4'];
		} else {
			$ms_image_path_4 = $data['ms_image_url_4'];
		}
		$smarty->assign("ms_image_path_4", $ms_image_path_4);

		if($data['ms_image_type_5'] == 1) {
			$ms_image_path_5 = $image_directory . $data['ms_image_url_5'];
		} else {
			$ms_image_path_5 = $data['ms_image_url_5'];
		}
		$smarty->assign("ms_image_path_5", $ms_image_path_5);

		$sql = " SELECT * FROM connect_logs "
				. " WHERE deleted_at is NULL "
				. " AND media_id = '$media_id' "
				. " AND advert_id = '$advert_id' ";

		$db_result = $common_dao->db_query($sql);
		if($db_result){
			$smarty->assign("connect", $db_result[0]);
		}
	} else {

	}

	// ページを表示
	$smarty->display("./media_search_view.tpl");
	exit();
}else{
	header('Location: ../index.php?error=1');
	exit();
}

function view_list(){
	global $common_dao, $smarty, $list_sql, $error_message;

	$list_count = 0;

	$db_result = $common_dao->db_query($list_sql);
	if($db_result){
		$smarty->assign("list", $db_result);
		$list_count = count($db_result);
	}else{
		$error_message .= "ＤＢからのデータの取得に失敗しました。(su0000)";
	}
	$smarty->assign("list_count", $list_count);
	$smarty->assign("error_message", $error_message);

	// ページを表示
	$smarty->display("./media_search.tpl");
	exit();
}

function do_escape_quotes($str){
	//magic_quotesが有効ならクウォート部分を除去
	if(get_magic_quotes_gpc()){
		$str = stripslashes($str);
	}
	return $str;
}
?>