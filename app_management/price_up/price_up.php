<?
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja">
<head>
<title><?=global_site_name;?>セッション抽出</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />

<link rel="stylesheet" type="text/css" href="../include/admin.css" />
</head>

<body>
<?

	$common_connect -> Fn_admin_check();

	
	foreach($_POST as $key => $value)
	{ 
		$$key = $common_dao->db_string_escape($value);
	}


	
	if($session_id_1!="")
	{
		local_update($advert_id, $media_id, $session_id_1, $price_1, $price_2, $price_3, $price_4);
	}
	
	if($session_id_2!="")
	{
		local_update($advert_id, $media_id, $session_id_2, $price_1, $price_2, $price_3, $price_4);
	}
	
	if($session_id_3!="")
	{
		local_update($advert_id, $media_id, $session_id_3, $price_1, $price_2, $price_3, $price_4);
	}
	
	if($session_id_4!="")
	{
		local_update($advert_id, $media_id, $session_id_4, $price_1, $price_2, $price_3, $price_4);
	}
	
	if($session_id_5!="")
	{
		local_update($advert_id, $media_id, $session_id_5, $price_1, $price_2, $price_3, $price_4);
	}
	
	function local_update($advert_id, $media_id, $session_id_1, $price_1, $price_2, $price_3, $price_4)
	{
		global $common_dao;
		
		if($price_1!="" && $price_2!="")
		{
			$where = "";
			$where .= " and advert_id='".$advert_id."' ";
			$where .= " and media_id='".$media_id."' ";
			$where .= " and action_price_media='".$price_1."' ";
			$sql = "SELECT id FROM action_logs where session_id='".$session_id_1."' ".$where;

			$db_result = $common_dao->db_query($sql);
			if(!$db_result)
			{
				echo "アクション単価(net)：".$session_id_1." のデータがありません。<hr />";
			}
			else
			{
				$db_insert = "update action_logs set action_price_media='".$price_2."' ";
				$db_insert .= "  where  id='".$db_result[0]["id"]."' ";
				
				$db_result = $common_dao->db_update($db_insert);
				echo "アクション単価(net)：".$session_id_1." が更新されました。<hr />";
			}
		}
		
		if($price_3!="" && $price_4!="")
		{
			$where = "";
			$where .= " and advert_id='".$advert_id."' ";
			$where .= " and media_id='".$media_id."' ";
			$where .= " and action_price_client='".$price_3."' ";
			$sql = "SELECT id FROM action_logs where session_id='".$session_id_1."' ".$where;

			$db_result = $common_dao->db_query($sql);
			if(!$db_result)
			{
				echo "アクション単価(Gross)：".$session_id_1." のデータがありません。<hr />";
			}
			else
			{
				$db_insert = "update action_logs set action_price_client='".$price_4."' ";
				$db_insert .= "  where  id='".$db_result[0]["id"]."' ";
				
				$db_result = $common_dao->db_update($db_insert);
				echo "アクション単価(Gross)：".$session_id_1." が更新されました。<hr />";
			}
		}
	}
	
	
?>
	<a href="input.php?advert_id=<? echo $advert_id;?>&media_id=<? echo $media_id;?>&price_1=<? echo $price_1;?>&price_2=<? echo $price_2;?>">前のページの戻る<a>
</body>
</html>
