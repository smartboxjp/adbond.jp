<?php
	require_once $_SERVER['DOCUMENT_ROOT']."/app_include/connect.php";
	$common_connect = new CommonConnect();
	$common_dao = new CommonDao(); //DB関連
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja">
<head>
<title><?=global_site_name;?>広告IP管理</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="Content-Script-Type" content="text/javascript" />
<meta http-equiv="Content-Style-Type" content="text/css" />

</head>

<body>
<?
	//管理者チェック
	$common_connect -> Fn_admin_check();
	
	foreach($_POST as $key => $value)
	{ 
		$$key = $common_dao->db_string_escape($value);
	}
	
	if($ip_address == "")
	{
		$common_connect -> Fn_javascript_back("IPを入力してください。");
	}
	
	$datetime = date("Y/m/d H:i:s");
	
	//array
	$arr_db_field = array("advert_id", "ip_address");

	//基本情報
	if($advert_ip_id=="")
	{
		$db_insert = "insert into advert_ip ( ";
		$db_insert .= " advert_ip_id, ";
		foreach($arr_db_field as $val)
		{
			$db_insert .= $val.", ";
		}
		$db_insert .= " regi_date, up_date ";
		$db_insert .= " ) values ( ";
		$db_insert .= " '', ";
		foreach($arr_db_field as $val)
		{
			$db_insert .= " '".$$val."', ";
		}
		$db_insert .= " '$datetime', '$datetime')";
	}
	else
	{
		$db_insert = "update advert_ip set ";
		foreach($arr_db_field as $val)
		{
			$db_insert .= $val."='".$$val."', ";
		}
		$db_insert .= " up_date='".$datetime."' ";
		$db_insert .= " where advert_ip_id='".$advert_ip_id."'";
	}
	$db_result = $common_dao->db_update($db_insert);

	if ($advert_ip_id == "")
	{
		//自動生成されてるID出力
		$sql = "select last_insert_id() as last_id";  
		$db_result = $common_dao->db_query($sql);
		if($db_result)
		{
			$advert_ip_id = $common_connect -> str_htmlspecialchars($db_result[0]["last_id"]);
		}
	}
	

	

	$common_connect-> Fn_javascript_move("広告IP登録・修正しました", "advert_ip_list.php?advert_id=".$advert_id);
?>
</body>
</html>