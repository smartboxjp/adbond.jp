<!DOCTYPE unspecified PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<meta http-equiv="Content-Style-Type" content="text/css" />
<link rel="stylesheet" href="/kadai2/css_file/style.css" type="text/css" />
<title>問い合わせ画面</title>
</head>
<body>
<h1>お問い合わせ</h1>
<?php
// お問い合わせ画面

// check関数
include_once '/check.php';
// データのset関数
include_once '/set.php';

$dataset = new DataSet();
// 受け取ったデータを配列に格納
$ary = array(0 => $_POST["name"], $_POST["add1"], $_POST["add2"], $_POST["add3"], $_POST["tel1"], $_POST["tel2"], $_POST["tel3"], $_POST["mail"], $_POST["coment"]);

// 入力データをsetメソッドに格納
$dataset->set($ary);

// データ取得
$name = $dataset::$setname;
$add1 = $dataset::$setadd1;
$add2 = $dataset::$setadd2;
$add3 = $dataset::$setadd3;
$tel1 = $dataset::$settel1;
$tel2 = $dataset::$settel2;
$tel3 = $dataset::$settel3;
$mail = $dataset::$setmail;
$coment = $dataset::$setcoment;

$chk = new Chk();
// 入力内容のチェック
$ary = $chk->InputChk();

// 入力内容にエラーがない場合またはキャンセルボタンが押下されていない場合
if($ary[0] != false and isset($_POST["cancel"]) == false){
?>

<form action="end.php" method="post">

<?php
	// 確認画面を表示
	echo'<table>
		<tr>
			<td>名前（必須）</td>
			<td>：'.$name.'</td>
		</tr>
		<tr>
			<td>住所（都道府県）</td>
			<td>：'.$add1.'</td>
		</tr>
		<tr>
			<td>住所（市区町村）</td>
			<td>：'.$add2.'</td>
		</tr>
		<tr>
			<td>住所（番号）</td>
			<td>：'.$add3.'</td>
		</tr>
		<tr>
			<td>電話番号</td>
			<td>：'.$tel1.'-'.$tel2.'-'.$tel3.'</td>
		</tr>
		<tr>
			<td>メール（必須）</td>
			<td>：'.$mail.'</td>
		</tr>
		<tr>
			<td>コメント</td>
			<td>：'.$coment.'</td>
		</tr>
	</table>
	<div>
		<input type="hidden" name="name" value="'.$name.'">
		<input type="hidden" name="add1" value="'.$add1.'">
		<input type="hidden" name="add2" value="'.$add2.'">
		<input type="hidden" name="add3" value="'.$add3.'">
		<input type="hidden" name="tel1" value="'.$tel1.'">
		<input type="hidden" name="tel2" value="'.$tel2.'">
		<input type="hidden" name="tel3" value="'.$tel3.'">
		<input type="hidden" name="mail" value="'.$mail.'">
		<input type="hidden" name="coment" value="'.$coment.'">
	</div>'

?>

	<input type="submit" name="ok" value="OK">
</form>
<form action="contact.php" method="post">
	<input type="submit" name="cancel" value="キャンセル">
<div>

<?php

	echo '
		<input type="hidden" name="name" value="'.$name.'">
		<input type="hidden" name="add1" value="'.$add1.'">
		<input type="hidden" name="add2" value="'.$add2.'">
		<input type="hidden" name="add3" value="'.$add3.'">
		<input type="hidden" name="tel1" value="'.$tel1.'">
		<input type="hidden" name="tel2" value="'.$tel2.'">
		<input type="hidden" name="tel3" value="'.$tel3.'">
		<input type="hidden" name="mail" value="'.$mail.'">
		<input type="hidden" name="coment" value="'.$coment.'">'

?>

</div>
</form>

<?php
// 入力内容にエラーがある場合
} else {
	// 確認ボタンを押下した場合
	if(isset($_POST["check"])){
		// 名前が未入力の場合
		if($ary[1] == false){
?>
			<p class="error">名前が入力されていません。</p>
<?php
		}
		// 電話番号に数字以外が入力されている場合
		if($ary[2] == false){
?>
			<p class="error">電話番号に数字以外が入力されています。</p>
	<?php
		}
		// メールアドレスが未入力の場合
		if($ary[3] == 0){
?>
			<p class="error">メールアドレスが入力されていません。</p>
<?php
		// メールアドレスに入力できない文字が使われている場合
		} else if($ary[3] == -1){
?>
			<p class="error">メールアドレスの入力に誤りがあります。</p>
<?php
		}
	}
?>

<form action="contact.php" method="post">

<?php
// 初期画面
	echo'<table>
		<tr>
			<td>名前（必須）</td>
			<td>：<input name="name" type="text" size=10 maxlength=10 value='.$dataset::$setname.'></td>
		</tr>
		<tr>
			<td>住所（都道府県）</td>
			<td>：<input name="add1" type="text" size=10 maxlength=10 value='.$dataset::$setadd1.'></td>
		</tr>
		<tr>
			<td>住所（市区町村）</td>
			<td>：<input name="add2" type="text" size=50 maxlength=50 value='.$dataset::$setadd2.'></td>
		</tr>
		<tr>
			<td>住所（番号）</td>
			<td>：<input name="add3" type="text" size=20 maxlength=20 value='.$dataset::$setadd3.'></td>
		</tr>
		<tr>
			<td>電話番号</td>
			<td>：<input name="tel1" type="text" size=4 maxlength=4 value='.$dataset::$settel1.'>-
			<input name="tel2" type="text" size=4 maxlength=4 value='.$dataset::$settel2.'>-
			<input name="tel3" type="text" size=4 maxlength=4 value='.$dataset::$settel3.'></td>
		</tr>
		<tr>
			<td>メール（必須）</td>
			<td>：<input name="mail" type="text" size=50 maxlength=50 value='.$dataset::$setmail.'></td>
		</tr>
		<tr>
			<td>コメント：</td>
			<td>　<textarea name="coment" cols="25" rows="10" >'.$dataset::$setcoment.'</textarea></td>
		</tr>
	</table>'
?>

<input type="submit" name="check" value="確認">
</form>


<?php
}
?>

</body>
</html>
