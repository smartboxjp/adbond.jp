<?php
// データの格納

	class DataSet{
		// 名前
		public static $setname;
		// 住所１
		public static $setadd1;
		// 住所２
		public static $setadd2;
		// 住所３
		public static $setadd3;
		// 電話番号１
		public static $settel1;
		// 電話番号２
		public static $settel2;
		// 電話番号３
		public static $settel3;
		// メールアドレス
		public static $setmail;
		// コメント
		public static $setcoment;

		// 入力されたデータを格納
		public function set($ary){
			self::$setname = $ary[0];
			self::$setadd1 = $ary[1];
			self::$setadd2 = $ary[2];
			self::$setadd3 = $ary[3];
			self::$settel1 = $ary[4];
			self::$settel2 = $ary[5];
			self::$settel3 = $ary[6];
			self::$setmail = $ary[7];
			self::$setcoment = $ary[8];
		}
	}
?>