<?php
include_once '/set.php';

// 入力内容チェッククラス
class Chk {
	function InputChk(){
		$setdata = new DataSet();
		// 名前
		$name = $setdata::$setname;
		// 電話番号１
		$tel1 = $setdata::$settel1;
		// 電話番号２
		$tel2 = $setdata::$settel2;
		// 電話番号３
		$tel3 = $setdata::$settel3;
		// メールアドレス
		$mail = $setdata::$setmail;

		// 名前チェックフラグ
		$namechk = true;
		// 電話番号チェックフラグ
		$telchk = true;
		// メールアドレスチェックフラグ
		$mailchk = 0;
		// エラーチェックフラグ
		$errchk = true;


		// 名前のチェック
		// 名前が未入力の場合
		if(empty($name)){
			$namechk = false;
			$errchk = false;
		}

		$telary = array(0 => $tel1, $tel2, $tel3);

		// 電話番号のチェック
		for($count = 0; $count < 3; $count++){
			// 電話番号が数字以外かつ、入力されている場合
			if(!is_numeric($telary[count]) and empty($telary[count]) == false){
				$telchk = false;
				$errchk = false;
				break;
			}
		}

		// メールアドレスのチェック
		// メールアドレスが未入力の場合
		if(empty($mail)){
			$mailchk = 0;
			$errchk = false;
		// メールアドレスが下記の形式ではない場合
		// [小文字英数字または数字または"-","_"]@[小文字英数字または数字].(co.jp/com/ne.jp)
		} else if(!preg_match("/^[a-z0-9\-\_]+@+[a-z0-9]+\.(co.jp|com|ne.jp)/", $mail)){
			$mailchk = -1;
			$errchk = false;
		}

		$ary = array(0 => $errchk, $namechk, $telchk, $mailchk);
		return $ary;
	}
}
?>