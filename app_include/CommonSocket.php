<?php
class CommonSocket{ // extends CommonDao

/*
	//コンストラクタ
	function __construct(){
		parent::__construct();
	}

	//デストラクタ
	function __destruct(){
		parent::__destruct();
	}
*/

	//$server = "domain.net"; //送信先サーバアドレス
	//$port = 80;
	//$timeout = 30;//接続失敗時の待ち時間
	//$str_get = "str_1=value_1&str_2=value_2";
	public function Fn_socket_send_to($server, $port, $timeout, $str_get)
	{	
		$request = "GET ".$str_get." HTTP/1.0\r\n";
		$request .= "HOST: ".$server."\r\n";
		$request .= "User-Agent: ". $_SERVER['HTTP_USER_AGENT']. " PHP/". phpversion(). "\r\n\r\n";

		$send_limit = 3;
		while($send_limit){
			if($send_limit == 3){
				//echo "send_limit equal 3";
			}
			else{
				//echo $send_limit;
				if($this -> Fn_socket_send_request($server, $port, $timeout, $request)) break;
			}
			$send_limit--;
		}
		//echo "end";
	}
	
	
	public function Fn_socket_send_request($server, $port, $timeout, $request){
		$fp = fsockopen($server, $port);
		socket_set_timeout($fp, $timeout);
		fputs($fp, $request);
		$response = "";
		
		while(!feof($fp)){
			$response .= fgets($fp, 4096);
		}
	
		fclose($fp);
		
		$DATA = explode("\r\n\r\n", $response, 2);
		$ResHeaders = explode("\r\n", $DATA[0]);
		
		foreach($ResHeaders as $res){
			if(preg_match('/HTTP\/1\.\d\s*(.*)/', $res, $matches)){
				$ResCode = $matches[1];
			}
		}
	
		if((int)$ResCode == 200){
			return 1;
		}
		return 0;
	
	}
	
	/*
		$server = "domain.net"; //送信先サーバアドレス
		$port = 80;
		$timeout = 30;//接続失敗時の待ち時間
		$errno="";
		$errstr="";
		$url="/action/result.php";
	*/
	public function Fn_socket_simple($server, $port, $errno, $errstr, $timeout, $url) {
		$sock = fsockopen($server, $port, $errno, $errstr, $timeout); // サーバ接続
		
		//HTTP　ヘッダ部分の送信
		fwrite($sock, "GET http://".$server.$url." HTTP/1.0\r\n");
		//ヘッダ終了通知
		fwrite($sock, "\r\n\r\n");
		fclose($sock);
	}
}
?>
