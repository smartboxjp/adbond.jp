<?php

class CommonConnect{ // extends CommonDao

/*
	//コンストラクタ
	function __construct(){
		parent::__construct();
	}

	//デストラクタ
	function __destruct(){
		parent::__destruct();
	}
*/

	public function Fn_admin_check()
	{
		if(isset($_SESSION['logon_token'])!=true || $_SESSION['logon_token'] == '')
		{
			$this -> Fn_javascript_move("管理者専用ページです", "/admin/login.php");
		}
	}

	/* すべて半角に変換 */
	public function Fn_shiftjis($str)
	{
		$str= mb_convert_kana($str,"rnask","UTF-8");
	
		return $str;
		/* すべて全角に変換 */
		//$str = mb_convert_kana($str,"RNASKV","EUC-JP");
	}

	//メールチェック
	public function Fn_valid_email($mail_address)
	{
		if (!preg_match('/^[a-zA-Z0-9_\.\-]+?@[A-Za-z0-9_\.\-]+$/',$mail_address))
			return false;
		else
			return true;
	}
	
	//Javascriptのhistoryback
	public function Fn_javascript_back($message)
	{
		if($this -> Fn_mobile())
		{
			header("Location: /m");
		}
		else
		{
			echo ("
				<SCRIPT LANGUAGE=JavaScript>
				<!--
				alert('$message');
				history.back();
				//-->
				</SCRIPT>
			");
		}
		exit;
	}
	
	//Javascriptのmove
	public function Fn_javascript_move($message1, $message2)
	{
		echo ("
			<SCRIPT LANGUAGE=JavaScript>
			<!--
			alert('$message1');
			document.location.href = '$message2';
			//-->
			</SCRIPT>
		");}

	//モバイルの判断
	public function Fn_mobile()
	{
	
		$fn_env = "";
		if(strstr($_SERVER['HTTP_USER_AGENT'],"DoCoMo")){ $fn_env = 'i'; }
		elseif(strstr($_SERVER['HTTP_USER_AGENT'],"Vodafone")){ $fn_env = 'i'; }
		elseif(strstr($_SERVER['HTTP_USER_AGENT'],"SoftBank")){ $fn_env = 'i'; }
		elseif(strstr($_SERVER['HTTP_USER_AGENT'],"MOT-")){ $fn_env = 'i'; }
		elseif(strstr($_SERVER['HTTP_USER_AGENT'],"J-PHONE")){ $fn_env = 'i'; }
		elseif(strstr($_SERVER['HTTP_USER_AGENT'],"KDDI")){ $fn_env = 'ez'; }
		elseif(strstr($_SERVER['HTTP_USER_AGENT'],"UP.Browser")){ $fn_env = 'i'; }
		elseif(strstr($_SERVER['HTTP_USER_AGENT'],"WILLCOM")){ $fn_env = 'ez'; }
		
		if($fn_env!="")
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	
	
	//スマホの判断
	/*
	//redirect
	$sp_check = $common_connect -> Fn_sp();
	if($sp_check)
	{
		$redirect_url = "/sp".$_SERVER['PHP_SELF'];
		if($_SERVER["QUERY_STRING"]!="") { $redirect_url .= "?".$_SERVER["QUERY_STRING"]; }
		$common_connect -> Fn_redirect($redirect_url );
	}
	*/
	public function Fn_sp()
	{
		$ua = $_SERVER['HTTP_USER_AGENT'];
		
		$fn_env = "";
		if (strpos($ua, 'Android') !== false && strpos($ua, 'Mobile') !== false)
		{
			$fn_env = "sp";
		}
		elseif (strpos($ua, 'iPhone') !== false)
		{
			$fn_env = "sp";
		}
		
		if($fn_env!="")
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	
	//URL移動
	public function Fn_redirect($url)
	{
		echo ("<meta http-equiv='Refresh' content='0; URL=$url'>");
		exit;
	}
	
	//文字化け対策
	//input [real_escape_string] -> output[htmlspecialchars]
  public function str_htmlspecialchars($string){
  	return htmlspecialchars($string);
  }

	//SQL injection対策
	public function Fn_filter($str) 
	{
		$str=htmlspecialchars($str); //特殊文字を HTML エンティティに変換 例）&→&amp;
		$str=strip_tags($str); //html tag delete
		$str=addslashes($str);
		$str=mysql_real_escape_string($str);//文字列の特殊文字をエスケープ
		return $str;
	}

	
	//時間変更
	public function Fn_date($str1)
	{
		return substr($str1,0,4)."年".substr($str1,5,2)."月".substr($str1,8,2)."日(".substr($str1,11,2).":".substr($str1,17,2).")";
	}
	
	//時間変更
	public function Fn_date_day($str1)
	{
		switch(date("w", strtotime($str1))){ 
			case "0": 
			$view_day="日"; 
			break; 
			case "1": 
			$view_day="月"; 
			break; 
			case "2": 
			$view_day="火"; 
			break; 
			case "3": 
			$view_day="水"; 
			break; 
			case "4": 
			$view_day="木"; 
			break; 
			case "5": 
			$view_day="金"; 
			break; 
			case "6": 
			$view_day="土"; 
			break; 
		}
		return $view_day;
	}
	
	//paging
	public function Fn_paging($view_count, $all_count)
	{
      if ($view_count < $all_count)
      {
        foreach($_GET as $key => $value){ 
          if($key!="page")
          {
            $query .= "&".$key."=".urlencode(trim($value));
          }
        }
  
        for ($i=1;$i<ceil(($all_count/$view_count)+1);$i++) 
        {
          if ($i == $page)
          {
            echo "[ ".$i." ]";
          }
          else
          {
            echo "[ <a href=".$_SERVER["PHP_SELF"]."?page=".$i.$query."#form_search>".$i."</a> ]";
          }
        }
      }
			
	}
	
	
	//paging
	public function Fn_paging_10($view_count, $all_count)
	{
		
        foreach($_GET as $key => $value){ 
          if($key!="page")
          {
            $query .= "&".$key."=".urlencode(trim($value));
          }
        }
		$page = $_GET["page"];
		if($page=="")
		{
			$page=1;
		}
		
		$search_page_count = 10;
		
		echo "<div class=\"paging\">";
		echo "<ul>";

		If ($page!="1")
		{
			echo "<li><a href=\"".$_SERVER["PHP_SELF"]."?page=".((int)$page-1).$query."#search_list\">←前</a></li>";
		}
	
		If ($view_count < $all_count)
		{
	
			//表示したいページ件数より少ない場合
			If(ceil(($all_count/$view_count)+1) < $search_page_count)
			{
				For ($i=1;$i<ceil(($all_count/$view_count)+1);$i++) 
				{
					If ($i == $page)
					{
            			echo "<li><a href=\"".$_SERVER["PHP_SELF"]."?page=".$i.$query."#search_list\" class=\"active\">".$i."</a></li>";
					}
					Else
					{
						echo "<li><a href=\"".$_SERVER["PHP_SELF"]."?page=".$i.$query."#search_list\">".$i."</a></li>";
					}
				}
			}
			else
			{
				//現在ページが表示したいページより少ない
				if(($page+($search_page_count/2)-1) < $search_page_count)
				{
					For ($i=1;$i<$search_page_count+1;$i++) 
					{
						If ($i == $page)
						{
							echo "<li><a href=\"".$_SERVER["PHP_SELF"]."?page=".$i.$query."#search_list\" class=\"active\">".$i."</a></li>";
						}
						Else
						{
							echo "<li><a href=\"".$_SERVER["PHP_SELF"]."?page=".$i.$query."#search_list\">".$i."</a></li>";
						}
					}
				}
				//現在ページが表示したいページより多い
				else if((ceil(($all_count/$view_count)+1) > $search_page_count) && ((ceil($all_count/$view_count)+1)-($search_page_count/2) < $page))
				{
					$start = ($page-$search_page_count+(ceil(($all_count/$view_count)+1)-$page));
					For ($i=$start;$i<ceil(($all_count/$view_count)+1);$i++) 
					{
						If ($i == $page)
						{
							echo "<li><a href=\"".$_SERVER["PHP_SELF"]."?page=".$i.$query."#search_list\" class=\"active\">".$i."</a></li>";
						}
						Else
						{
							echo "<li><a href=\"".$_SERVER["PHP_SELF"]."?page=".$i.$query."#search_list\">".$i."</a></li>";
						}
					}
				}
				else
				{
		
					$min_page = ($page-($search_page_count/2));
					if ($min_page<1)
					{
						$min_page = 1;
					}
					$max_page = ceil(($all_count/$view_count)+1);
					
					if ($max_page>($page+($search_page_count/2)-1))
					{
						$max_page = ($page+($search_page_count/2));
					}
					
					For ($i=$min_page;$i<$max_page;$i++) 
					{
						If ($i == $page)
						{
							echo "<li><a href=\"".$_SERVER["PHP_SELF"]."?page=".$i.$query."#search_list\" class=\"active\">".$i."</a></li>";
						}
						Else
						{
							echo "<li><a href=\"".$_SERVER["PHP_SELF"]."?page=".$i.$query."#search_list\">".$i."</a></li>";
						}
					}
				}
			}
		}
	
		if (ceil(($all_count/$view_count)+1)!="1" && $page<ceil(($all_count/$view_count)))
		{
			echo "<li><a href=\"".$_SERVER["PHP_SELF"]."?page=".((int)$page+1).$query."#search_list\">次→</a></li>";
		}
		echo "</ul>";
		echo "</div><!-- /.paging -->";
	}
	

	//フォルダ全て削除
	public function Fn_deldir($dir)
	{
		$handle = opendir($dir);
		while (false!==($FolderOrFile = readdir($handle)))
		{
			if($FolderOrFile != "." && $FolderOrFile != "..") 
			{ 
		
				if(is_dir("$dir/$FolderOrFile")) 
				{ $this -> Fn_deldir("$dir/$FolderOrFile"); } // recursive
					else
				{ unlink("$dir/$FolderOrFile"); }
			} 
		}
		closedir($handle);
		if(rmdir($dir))
		{ $success = true; }
		return $success; 
		
	} 
	
	//フォルダ内古いファイル削除
	//$deadline = 24*60*60;  //削除期限（指定秒数以上経過で削除）
	//$save_dir = $global_path.global_temp_img."/";
	public function Fn_old_delete($path, $deadline)
	{
		
		$count = 0;
		if ($handle = opendir($path)) {
			while (false !== ($file = readdir($handle))) {
				if (is_file($path."/".$file)) {
					if((time() - filemtime($path."/".$file) > $deadline)){
						if(unlink($path."/".$file)){
							//echo("{$file}を削除しました。<br />\n");
							//$count += 1;
						}else{
							//echo("{$file}の削除に失敗しました。<br />\n");
						}
					}else{
						//echo "{$file}は削除しませんでした。<br />\n";
					}
				}else{
					//echo("{$file}はファイルではありません。<br />\n");
				}
			}
			closedir($handle);
		}
		
		//echo("{$count}ファイル削除しました。");
	}
	
	//文字制限
	public function Fn_shot_string($str, $len)
	{
		if(mb_strlen($str, "UTF-8")>=$len)
		{
			$return_str = mb_substr($str, 0, $len, "UTF-8")."...";
		}
		else 
		{
			$return_str = $str;
		}
		
		return $return_str;
	}
	
		
	//directory内ファイルコピー
	public function copyDirectory($imageDir, $destDir)
	{
		$handle=opendir($imageDir);   
		while($filename=readdir($handle))
		{       
			if(strcmp($filename,".")!=0	&& strcmp($filename,"..")!=0)
		 {
			 if(is_dir("$imageDir/$filename"))
			 {
				if(!empty($filename) && !file_exists("$destDir/$filename"))
				mkdir("$destDir/$filename");
				copyDirectory("$imageDir/$filename","$destDir/$filename");
			 }
		 else
			 {
				if(file_exists("$destDir/$filename"))
				unlink("$destDir/$filename");
				copy("$imageDir/$filename","$destDir/$filename");
			 }
		 }
		}     
	}
	
		
	//ランダム生成
	function Fn_random_password($str_len)
	{
		$chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
		for($i = 0; $i < $str_len; $i++){
			$result .= $chars{mt_rand(0, strlen($chars)-1)};
		}
		return $result;
	}
	
	
}


?>
