<?php
define('SMARTY_DIR', 'Smarty/libs/');
require_once(SMARTY_DIR . 'Smarty.class.php');

require_once( './common/CommonDao.php' );
//require_once( './dao/MediaLoginUserDao.php' );
//require_once( './dto/MediaLoginUser.php' );
//require_once( './dao/AdvertLoginUserDao.php' );
//require_once( './dto/AdvertLoginUser.php' );

session_start();
session_regenerate_id(true);

// Smartyオブジェクト取得
$smarty = new Smarty();
$smarty->template_dir = './templates/web/';
$smarty->compile_id   = 'web';
$smarty->compile_dir  = './templates_c/';
$smarty->config_dir   = './config/';
$smarty->cache_dir    = './cache/';


	// ページを表示
	$smarty->display("./media.tpl");
	exit();
?>