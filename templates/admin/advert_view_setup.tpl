{include file='./header.tpl' page_title='表示広告設定'}

<!-- Menu -->
{include file='./menu.tpl'}

<div id="my_contents">

<h2>表示広告設定</h2>

<div id=message>
{if $error_message != ''}
<div id="error_message">
<h3>ERROR:{$error_message}</h3>
</div>
{/if}
{if $info_message != ''}
<div id="info_message">
<h3>INFO:{$info_message}</h3>
</div>
{/if}
</div><!-- message -->

<form method="POST" action="{$smarty.server.PHP_SELF}">
<table cellpadding="0" cellspacing="0">
{foreach from=$media_category_array item="data" name="media_category_list"}
	<tr>
		<td>
			<input type="checkbox" name="check_category[{$data.id}]" value="{$data.id}"{if $check_category[$data.id] == $data.id} checked="checked"{/if} />
			<label><a href="?advert_id={$advert_id}&category_id={$data.id}">{$data.name|htmlspecialchars:$smarty.const.ENT_QUOTES}</a></label>
		</td>
	</tr>
{/foreach}
</table>
{if $category_id != ""}
<table cellpadding="0" cellspacing="0">
	<tr>
		<td id="th_title">{$media_category_array[$category_id].name|htmlspecialchars:$smarty.const.ENT_QUOTES}</td>
	</tr>
{foreach from=$media_array item="data" name="media_publisher_list"}
	<tr>
		<td>
			<input type="checkbox" name="check_publisher[{$data.publisher_id}]" value="{$data.publisher_id}"{if $check_publisher[$data.publisher_id] == $data.publisher_id} checked="checked"{/if} />
			<label><a href="?advert_id={$advert_id}&category_id={$category_id}&publisher_id={$data.publisher_id}">{$data.publisher_name|htmlspecialchars:$smarty.const.ENT_QUOTES}</a></label>
		</td>
	</tr>
{/foreach}
</table>
{if $publisher_id != ""}
<table cellpadding="0" cellspacing="0">
	<tr>
		<td id="th_title">{$media_array[$publisher_id].publisher_name|htmlspecialchars:$smarty.const.ENT_QUOTES}</td>
	</tr>
{foreach from=$media_array[$publisher_id].media item="data" name="media_list"}
	<tr>
		<td>
			<input type="checkbox" name="check_media[{$data.media_id}]" value="{$data.media_id}"{if $check_media[$data.media_id] == $data.media_id} checked="checked"{/if} />
			<label>{$data.media_name|htmlspecialchars:$smarty.const.ENT_QUOTES}</label>
		</td>
	</tr>
{/foreach}
</table>
{/if}
{/if}
<table cellpadding="0" cellspacing="0">
	<tr>
		<td>
			<input type="submit" value="設定" />
			<input type="hidden" name="mode" value="setup" />
			<input type="hidden" name="advert_id" value="{$advert_id}" />
			<input type="hidden" name="category_id" value="{$category_id}" />
			<input type="hidden" name="publisher_id" value="{$publisher_id}" />
		</td>
	</tr>
</table>
</form>
</div><!-- contents -->

{include file='./hooter.tpl'}