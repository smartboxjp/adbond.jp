{include file='./header.tpl' page_title='お問い合わせ一覧'}

<!-- Menu -->
{include file='./menu.tpl'}

<div id="my_contents">

<h2>お問い合わせ一覧</h2>

<div id=message>
{if $error_message != '' }
<div id="error_message">
	<h3>ERROR:{$error_message}</h3>
</div>
{/if}
{if $info_message != '' }
<div id="info_message">
	<h3>INFO:{$info_message}</h3>
</div>
{/if}
</div><!-- message -->

<table cellpadding="0" cellspacing="0">
	<tr>
		<th colspan="4" id="th_title">【該当{$list_count}件】</th>
	</tr>

	<tr>
		<th id="th_title">ID</th>
		<th id="th_title">お問い合わせ主</th>
		<th id="th_title">登録日時</th>
		<th id="th_title"></th>
	</tr>

{foreach from=$list key="key" item="data" name="list"}
	<tr>
		<td>{$data.id}</td>
		<td>{$data.user_name}</td>
		<td>{$data.commit_at}</td>
		<td>
				<form method="POST" action="{$smarty.server.PHP_SELF}">
				<input type="submit" value="詳細" />
				<input type="hidden" name="mode" value="detail" />
				<input type="hidden" name="contact_id" value="{$data.id}" />
			</form>
		</td>
	</tr>
{/foreach}
</table>

</div><!-- contents -->

{include file='./hooter.tpl'}
