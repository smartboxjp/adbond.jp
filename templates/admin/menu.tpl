<div id="my_navigation">

	<p><a href="./login.php?logout=y">ログアウト</a></p>

	<!-- Public 全権限を持つ -->
	<!-- Private ログ、お問い合わせ以外の権限を持つ -->
	<!-- Protect 集計のみ表示 -->
	<!-- Partner その他 使用予定 現在未実装 -->


	<!-- Public Privete -->
	{if $login_user->getLoginClass() == 'Public' || $login_user->getLoginClass() == 'Private'}
	<h4 alt="1">■媒体管理</h4>
	<ul>
		<li><a href="./media_publisher.php">媒体発行者管理</a></li>
		<li><a href="./media_group.php">媒体発行者グループ管理</a></li>
		<li><a href="./media.php">媒体管理</a></li>
		<li><a href="./media_category.php">媒体カテゴリー管理</a></li>
	</ul>
	{/if}


	<!-- Public Privete -->
	{if $login_user->getLoginClass() == 'Public' || $login_user->getLoginClass() == 'Private'}
	<h4 alt="2">■広告管理</h4>
	<ul>
		<li><a href="./advert_client.php">広告主管理</a></li>
		<li><a href="./advert_group.php">広告主グループ管理</a></li>
		<li><a href="./advert.php">広告管理</a></li>
		<li><a href="./advert_category.php">広告カテゴリー管理</a></li>
		<li><a href="./advert_reserve.php">広告予約管理</a></li>
		<li><a href="./advert_attention.php">おすすめ広告管理</a></li>
	</ul>
	{/if}


	<!-- Public Privete -->
<!--	{if $login_user->getLoginClass() == 'Public' || $login_user->getLoginClass() == 'Private'}-->
<!--	<h4 alt="2">■媒体個別認証管理</h4>-->
<!--	<ul>-->
<!--		<li><a href="./connect__view.php">媒体個別認証一覧</a></li>-->
<!--	</ul>-->
<!--	{/if}-->


	<!-- Public Privete Protect -->
	{if $login_user->getLoginClass() == 'Public' || $login_user->getLoginClass() == 'Private' || $login_user->getLoginClass() == 'Protect'}
	<h4 alt="4">■集計</h4>
	<ul>
		<li><a href="./summary_all.php">全体集計</a></li>
		<li><a href="./summary_media_publisher.php">媒体発行者別集計</a></li>
		<li><a href="./summary_media.php">媒体別集計</a></li>
		<li><a href="./summary_media_advert.php">媒体別広告集計</a></li>
		<li><a href="./summary_advert_client.php">広告主別集計</a></li>
		<li><a href="./summary_advert.php">広告別集計</a></li>
		<li><a href="./summary_advert_media.php">広告別媒体集計</a></li>
		<li><a href="/app_management/search/session.php" target="_blank">セッション抽出</a></li>
		<li><a href="/app_management/advert_ip/advert_ip_list.php" target="_blank">広告IP設定</a></li>
		<li><a href="/app_management/price_up/input.php" target="_blank">アクション単価</a></li>
	</ul>
	{/if}


	<!-- Public -->
	{if $login_user->getLoginClass() == 'Public'}
	<h4 alt="5">■ログ</h4>
	<ul>
		<li><a href="./result_log.php">成果ログ</a></li>
		<li><a href="./referer.php">リファラー</a></li>
		<li><a href="./withdrawal_report.php">退会レポート</a></li>
	</ul>
	{/if}


	<!-- Public -->
	{if $login_user->getLoginClass() == 'Public'}
	<h4 alt="6">■お問い合わせ</h4>
	<ul>
		<li><a href="./contact_view.php">一覧表示</a></li>
	</ul>
	{/if}

	<!-- Public -->
<!--	{if $login_user->getLoginClass() == 'Public'}-->
<!--		<h4 alt="6">■admin管理</h4>-->
<!--		<ul>-->
<!--			<li><a href="./admin_manager.php">パスワード管理</a></li>-->
<!--		</ul>-->
<!--	{/if}-->

</div>
