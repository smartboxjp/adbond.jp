{include file='./header.tpl' page_title='媒体発行者('|cat:$sub_title|cat:')'}

<!-- Menu -->
{include file='./menu.tpl'}

<div id="my_contents">

<h2>媒体発行者({$sub_title})</h2>

<div id=message>
{if $error_message != ''}
<div id="error_message">
<h3>ERROR:{$error_message}</h3>
</div>
{/if}
{if $info_message != ''}
<div id="info_message">
<h3>INFO:{$info_message}</h3>
</div>
{/if}
</div><!-- message -->

<form method="POST" action="{$smarty.server.PHP_SELF}">
<table cellpadding="0" cellspacing="0">
	<tr>
		<th colspan="2" id="th_title">媒体グループ</th>
		<td>
			<select name="media_group_id">
			{foreach from=$media_group_array item="data" name="media_group_list"}
				<option value="{$data.id}"{if $form_data.media_group_id == $data.id} selected="selected"{/if}>{$data.name|htmlspecialchars:$smarty.const.ENT_QUOTES}</option>
			{/foreach}
			</select>
		</td>
	</tr>
	<tr>
		<th colspan="2" id="th_title">ログインID ※6-16桁</th>
		<td>
			<input type="text" size="20" name="login_id" value="{$form_data.login_id|htmlspecialchars:$smarty.const.ENT_QUOTES}" />
		</td>
	</tr>
	<tr>
		<th colspan="2" id="th_title">パスワード ※6-16桁</th>
		<td>
			<input type="text" size="20" name="login_pass" value="{$form_data.login_pass|htmlspecialchars:$smarty.const.ENT_QUOTES}" />
		</td>
	</tr>
	<tr>
		<th colspan="2" id="th_title">企業名/個人名</th>
		<td>
			<input type="text" size="50" name="publisher_name" value="{$form_data.publisher_name|htmlspecialchars:$smarty.const.ENT_QUOTES}" />
		</td>
	</tr>
	<tr>
		<th colspan="2" id="th_title">担当者名 ※法人の場合のみ</th>
		<td>
			<input type="text" size="50" name="contact_person" value="{$form_data.contact_person|htmlspecialchars:$smarty.const.ENT_QUOTES}" />
		</td>
	</tr>
	<tr>
		<th colspan="2" id="th_title">TEL</th>
		<td>
			<input type="text" size="30" name="tel" value="{$form_data.tel|htmlspecialchars:$smarty.const.ENT_QUOTES}" />
		</td>
	</tr>
	<tr>
		<th colspan="2" id="th_title">FAX</th>
		<td>
			<input type="text" size="30" name="fax" value="{$form_data.fax|htmlspecialchars:$smarty.const.ENT_QUOTES}" />
		</td>
	</tr>
	<tr>
		<th colspan="2" id="th_title">メールアドレス</th>
		<td>
			<input type="text" size="30" name="email" value="{$form_data.email|htmlspecialchars:$smarty.const.ENT_QUOTES}" />
		</td>
	</tr>
	<tr>
		<th colspan="2" id="th_title">郵便番号</th>
		<td>
			<input type="text" size="10" name="zipcode1" value="{$form_data.zipcode1|htmlspecialchars:$smarty.const.ENT_QUOTES}" /> - <input type="text" size="10" name="zipcode2" value="{$form_data.zipcode2|htmlspecialchars:$smarty.const.ENT_QUOTES}" />
		</td>
	</tr>
	<tr>
		<th colspan="2" id="th_title">都道府県</th>
		<td>
			<select name="pref">
			{foreach from=$pref_array key="key" item="data" name="pref_list"}
				<option value="{$data.id}"{if $data.id == $form_data.pref} selected="selected"{/if}>{$data.name|htmlspecialchars:$smarty.const.ENT_QUOTES}</option>
			{/foreach}
			</select>
		</td>
	</tr>
	<tr>
		<th colspan="2" id="th_title">住所１（市区町村）</th>
		<td>
			<input type="text" size="50" name="address1" value="{$form_data.address1|htmlspecialchars:$smarty.const.ENT_QUOTES}" />
		</td>
	</tr>
	<tr>
		<th colspan="2" id="th_title">住所２（番地、建物）</th>
		<td>
			<input type="text" size="50" name="address2" value="{$form_data.address2|htmlspecialchars:$smarty.const.ENT_QUOTES}" />
		</td>
	</tr>
	<tr>
		<th colspan="2" id="th_title">振込先種別</th>
		<td>
			<input type="radio" name="transfer_type" value="1"{if $form_data.transfer_type == 1 || $form_data.transfer_type == ""} checked="checked"{/if} /><label>自行</label>
			<input type="radio" name="transfer_type" value="2"{if $form_data.transfer_type == 2} checked="checked"{/if} /><label>他行</label>
			<input type="radio" name="transfer_type" value="3"{if $form_data.transfer_type == 3} checked="checked"{/if} /><label>郵便局</label>
		</td>
	</tr>
	<tr>
		<th colspan="2" id="th_title">振込先銀行名</th>
		<td>
			<input type="text" size="50" name="bank_name" value="{$form_data.bank_name|htmlspecialchars:$smarty.const.ENT_QUOTES}" />
		</td>
	</tr>
	<tr>
		<th colspan="2" id="th_title">支店名</th>
		<td>
			<input type="text" size="50" name="branch_name" value="{$form_data.branch_name|htmlspecialchars:$smarty.const.ENT_QUOTES}" />
		</td>
	</tr>
	<tr>
		<th colspan="2" id="th_title">口座種別</th>
		<td>
			<input type="radio" name="account_type" value="1"{if $form_data.account_type == 1 || $form_data.account_type == ""} checked="checked"{/if} /><label>普通</label>
			<input type="radio" name="account_type" value="2"{if $form_data.account_type == 2} checked="checked"{/if} /><label>当座</label>
		</td>
	</tr>
	<tr>
		<th colspan="2" id="th_title">口座名義</th>
		<td>
			<input type="text" size="50" name="account_holder" value="{$form_data.account_holder|htmlspecialchars:$smarty.const.ENT_QUOTES}" />
		</td>
	</tr>
	<tr>
		<th colspan="2" id="th_title">口座番号</th>
		<td>
			<input type="text" size="50" name="account_number" value="{$form_data.account_number|htmlspecialchars:$smarty.const.ENT_QUOTES}" />
		</td>
	</tr>
	<tr>
		<th colspan="2" id="th_title">郵便局振込先名義</th>
		<td>
			<input type="text" size="50" name="postal_account_holder" value="{$form_data.postal_account_holder|htmlspecialchars:$smarty.const.ENT_QUOTES}" />
		</td>
	</tr>
	<tr>
		<th colspan="2" id="th_title">郵便局口座番号</th>
		<td>
			<input type="text" size="50" name="postal_account_number" value="{$form_data.postal_account_number|htmlspecialchars:$smarty.const.ENT_QUOTES}" />
		</td>
	</tr>
	<tr>
		<th colspan="2" id="th_title">ステータス</th>
		<td>
			<input type="radio" name="status" value="1"{if $form_data.status == 1 || $form_data.status == ""} checked="checked"{/if} /><label>仮登録</label>
			<input type="radio" name="status" value="2"{if $form_data.status == 2} checked="checked"{/if} /><label>正規</label>
			<input type="radio" name="status" value="3"{if $form_data.status == 3} checked="checked"{/if} /><label>退会</label>
		</td>
	</tr>
	<tr>
		<td colspan="3">
			<input type="submit" value="登録" />
			<input type="hidden" name="mode" value="{$mode}" />
			<input type="hidden" name="id" value="{$form_data.id}" />
		</td>
	</tr>
</table>
</form>

</div><!-- contents -->

{include file='./hooter.tpl'}