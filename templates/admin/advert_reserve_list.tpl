{include file='./header.tpl' page_title='広告予約一覧'}

<!-- Menu -->
{include file='./menu.tpl'}

<div id="my_contents">

<h2>広告予約一覧</h2>

<div id=message>
{if $error_message != '' }
<div id="error_message">
	<h3>ERROR:{$error_message}</h3>
</div>
{/if}
{if $info_message != '' }
<div id="info_message">
	<h3>INFO:{$info_message}</h3>
</div>
{/if}
</div><!-- message -->

<form method="POST" action="{$smarty.server.PHP_SELF}">
</form>

<table cellpadding="0" cellspacing="0">
	<tr>
		<th colspan="7" id="th_title">【該当{$list_count}件】</th>
	</tr>
	<tr>
		<th id="th_title">ID</th>
		<th id="th_title">広告主</th>
		<th id="th_title">広告名</th>
		<th id="th_title">変更日</th>
		<th id="th_title">ステータス</th>
		<th id="th_title">登録日時</th>
		<th id="th_title">
		</th>
	</tr>
{foreach from=$list item="data" name="list"}
	<tr>
		<td>{$data.id}</td>
		<td>{$data.advert_client_name|htmlspecialchars:$smarty.const.ENT_QUOTES}</td>
		<td>{$data.advert_name|htmlspecialchars:$smarty.const.ENT_QUOTES}</td>
		<td>{$data.reserve_change_date|date_format:"%Y-%m-%d"}</td>
		<td>
{if $data.status == 0}
			変更待ち
{elseif $data.status == 1}
			変更済み
{/if}
		</td>
		<td>{$data.created_at}</td>
		<td>
			<form method="POST" action="{$smarty.server.PHP_SELF}">
				<input type="submit" value="詳細" />
				<input type="hidden" name="mode" value="edit" />
				<input type="hidden" name="id" value="{$data.id}" />
				<input type="hidden" name="advert_id" value="{$data.advert_id}" />
			</form>
			<form method="POST" action="{$smarty.server.PHP_SELF}">
				<input type="submit" value="削除" onclick="return confirm('削除してよろしいですか？');" />
				<input type="hidden" name="mode" value="delete" />
				<input type="hidden" name="id" value="{$data.id}" />
				<input type="hidden" name="advert_id" value="{$data.advert_id}" />
			</form>
		</td>
	</tr>
{/foreach}
</table>

</div><!-- contents -->

{include file='./hooter.tpl'}