{include file='./header.tpl' page_title='成果通知ログ詳細'}

<!-- Menu -->
{include file='./menu.tpl'}

<div id="my_contents">

<h2>成果通知ログ詳細</h2>

<div id=message>
{if $error_message != '' }
<div id="error_message">
	<h3>ERROR:{$error_message}</h3>
</div>
{/if}
{if $info_message != '' }
<div id="info_message">
	<h3>INFO:{$info_message}</h3>
</div>
{/if}
</div><!-- message -->

<form method="POST" action="{$smarty.server.PHP_SELF}">
</form>

<table cellpadding="0" cellspacing="0">
	<tr>
		<th id="page_link" colspan="12" id="th_title">
			【該当{$list_count}件】
{if $list_count > 500}
{if $prev == 0}<span class="prev">PREV</span>{else}<a class="prev" href="?m_id={$m_id}&mp_id={$mp_id}&a_id={$a_id}&ac_id={$ac_id}&type={$type}&date={$date}&start_date={$start_date}&end_date={$end_date}&page={$prev}">PREV</a>{/if}
{section name=cnt start=1 loop=$page_max+1}
{if $smarty.section.cnt.index == $page}<span>[{$smarty.section.cnt.index}]</span>{else}&nbsp;<a href="?m_id={$m_id}&mp_id={$mp_id}&a_id={$a_id}&ac_id={$ac_id}&type={$type}&date={$date}&start_date={$start_date}&end_date={$end_date}&page={$smarty.section.cnt.index}">{$smarty.section.cnt.index}</a>&nbsp;{/if}
{/section}
{if $next == 0}<span class="next">NEXT</span>{else}<a class="next" href="?m_id={$m_id}&mp_id={$mp_id}&a_id={$a_id}&ac_id={$ac_id}&type={$type}&date={$date}&start_date={$start_date}&end_date={$end_date}&page={$next}">NEXT</a>{/if}
{/if}
		</th>
	</tr>
	<tr>
		<th id="th_title">アクション完了日時</th>
		<th id="th_title">クリック日時</th>
		<th id="th_title">媒体名</th>
		<th id="th_title">広告名</th>
		<th id="th_title">キャリア</th>
		<th id="th_title">ユーザーエージェント</th>
		<th id="th_title">個体識別番号</th>
		<th id="th_title">IPアドレス</th>
		<th id="th_title">ホスト名</th>
		<th id="th_title">クリック単価</th>
		<th id="th_title">アクション単価<br />(Gross)</th>
		<th id="th_title">アクション単価<br />(net)</th>
	</tr>
{foreach from=$list key="key" item="data" name="list"}
	<tr>
		<td>{$data.action_complete_date}</td>
		<td>{$data.created_at}</td>
		<td><a href="./media.php?mode=edit&id={$data.media_id}">{$data.media_name|htmlspecialchars:$smarty.const.ENT_QUOTES}</a></td>
		<td><a href="./advert.php?mode=edit&id={$data.advert_id}">{$data.advert_name|htmlspecialchars:$smarty.const.ENT_QUOTES}</a></td>
		<td>{$data.carrier}</td>
		<td>{$data.user_agent|htmlspecialchars:$smarty.const.ENT_QUOTES}</td>
		<td>{$data.uid|htmlspecialchars:$smarty.const.ENT_QUOTES}</td>
		<td>{$data.ip_address}</td>
		<td>{$data.host_name}</td>
		<td>&yen;{$data.click_price_client|number_format}</td>
		<td>&yen;{$data.action_price_client|number_format}</td>
		<td>&yen;{$data.action_price_media|number_format}</td>
	</tr>
{/foreach}
	<tr>
		<td id="th_title" colspan="9" id="th_title">合計</td>
		<td id="th_title" >
			-
		</td>
		<td id="th_title" >
			&yen;{$price_client_total}
		</td>
		<td id="th_title" >
			&yen;{$price_media_total}
		</td>
	</tr>
</table>

</div><!-- contents -->

{include file='./hooter.tpl'}