{include file='./header.tpl' page_title='広告('|cat:$sub_title|cat:')'}

<!-- Menu -->
{include file='./menu.tpl'}

<div id="my_contents">

<h2>広告({$sub_title})</h2>

<div id=message>
{if $error_message != ''}
<div id="error_message">
<h3>ERROR:{$error_message}</h3>
</div>
{/if}
{if $info_message != ''}
<div id="info_message">
<h3>INFO:{$info_message}</h3>
</div>
{/if}
</div><!-- message -->

<form method="POST" action="{$smarty.server.PHP_SELF}" enctype="multipart/form-data">
<table cellpadding="0" cellspacing="0">
	<tr>
		<th id="th_title">広告主</th>
		<td>{$advert_client_array[$form_data.advert_client_id].name|htmlspecialchars:$smarty.const.ENT_QUOTES}</td>
	</tr>
	<tr>
		<th id="th_title">広告カテゴリー</th>
		<td>{$advert_category_array[$form_data.advert_category_id].name|htmlspecialchars:$smarty.const.ENT_QUOTES}</td>
	</tr>
	<tr>
		<th id="th_title">広告名</th>
		<td>{$form_data.advert_name|htmlspecialchars:$smarty.const.ENT_QUOTES}</td>
	</tr>
	<tr>
		<th id="th_title">コンテンツ種別</th>
		<td>{if $form_data.content_type == 1}一般{elseif $form_data.content_type == 2}公式{/if}</td>
	</tr>
	<tr>
		<th id="th_title">対応キャリア</th>
		<td>
			<input type="checkbox" name="support_docomo" value="1"{if $form_data.support_docomo == 1} checked="checked"{/if} /><label>docomo</label>
			<input type="checkbox" name="support_softbank" value="1"{if $form_data.support_softbank == 1} checked="checked"{/if} /><label>softbank</label>
			<input type="checkbox" name="support_au" value="1"{if $form_data.support_au == 1} checked="checked"{/if} /><label>au</label>
			<input type="checkbox" name="support_pc" value="1"{if $form_data.support_pc == 1} checked="checked"{/if} /><label>pc</label>
		</td>
	</tr>
	<tr>
		<th id="th_title">サイトURL(docomo)</th>
		<td>
			<input type="text" size="100" name="site_url_docomo" value="{$form_data.site_url_docomo|htmlspecialchars:$smarty.const.ENT_QUOTES}" />
		</td>
	</tr>
	<tr>
		<th id="th_title">サイトURL(softbank)</th>
		<td>
			<input type="text" size="100" name="site_url_softbank" value="{$form_data.site_url_softbank|htmlspecialchars:$smarty.const.ENT_QUOTES}" />
		</td>
	</tr>
	<tr>
		<th id="th_title">サイトURL(au)</th>
		<td>
			<input type="text" size="100" name="site_url_au" value="{$form_data.site_url_au|htmlspecialchars:$smarty.const.ENT_QUOTES}" />
		</td>
	</tr>
	<tr>
		<th id="th_title">サイトURL(pc)</th>
		<td>
			<input type="text" size="100" name="site_url_pc" value="{$form_data.site_url_pc|htmlspecialchars:$smarty.const.ENT_QUOTES}" />
		</td>
	</tr>
	<tr>
		<th id="th_title">サイトURLについて</th>
		<td>
			＜セッションIDの付加について＞<br />
			サイトURLに?が含まれない場合「?bid=」で挿入される<br />
			http://example.jp/→http://example.jp/?bid=***<br />
			サイトURLに?が含まれる場合「&bid=」で挿入される<br />
			http://example.jp/?a=b→http://example.jp/?a=b&bid=***<br />
		</td>
	</tr>
	<tr>
		<th id="th_title">広告概要</th>
		<td>{$form_data.site_outline|htmlspecialchars:$smarty.const.ENT_QUOTES|nl2br}</td>
	</tr>
	<tr>
		<th id="th_title">クリック単価(クライアント)</th>
		<td>
			<input type="text" size="10" name="click_price_client" value="{$form_data.click_price_client|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
		</td>
	</tr>
	<tr>
		<th id="th_title">クリック単価(メディア)</th>
		<td>
			<input type="text" size="10" name="click_price_media" value="{$form_data.click_price_media|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
		</td>
	</tr>
	<tr>
		<th id="th_title">アクション単価(金額)(クライアント)</th>
		<td>
			[成果1]
			docomo:<input type="text" size="10" name="action_price_client_docomo_1" value="{$form_data.action_price_client_docomo_1|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
			softbank:<input type="text" size="10" name="action_price_client_softbank_1" value="{$form_data.action_price_client_softbank_1|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
			au:<input type="text" size="10" name="action_price_client_au_1" value="{$form_data.action_price_client_au_1|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
			pc:<input type="text" size="10" name="action_price_client_pc_1" value="{$form_data.action_price_client_pc_1|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円<br />
			[成果2]
			docomo:<input type="text" size="10" name="action_price_client_docomo_2" value="{$form_data.action_price_client_docomo_2|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
			softbank:<input type="text" size="10" name="action_price_client_softbank_2" value="{$form_data.action_price_client_softbank_2|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
			au:<input type="text" size="10" name="action_price_client_au_2" value="{$form_data.action_price_client_au_2|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
			pc:<input type="text" size="10" name="action_price_client_pc_2" value="{$form_data.action_price_client_pc_2|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円<br />
			[成果3]
			docomo:<input type="text" size="10" name="action_price_client_docomo_3" value="{$form_data.action_price_client_docomo_3|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
			softbank:<input type="text" size="10" name="action_price_client_softbank_3" value="{$form_data.action_price_client_softbank_3|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
			au:<input type="text" size="10" name="action_price_client_au_3" value="{$form_data.action_price_client_au_3|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
			pc:<input type="text" size="10" name="action_price_client_pc_3" value="{$form_data.action_price_client_pc_3|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円<br />
			[成果4]
			docomo:<input type="text" size="10" name="action_price_client_docomo_4" value="{$form_data.action_price_client_docomo_4|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
			softbank:<input type="text" size="10" name="action_price_client_softbank_4" value="{$form_data.action_price_client_softbank_4|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
			au:<input type="text" size="10" name="action_price_client_au_4" value="{$form_data.action_price_client_au_4|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
			pc:<input type="text" size="10" name="action_price_client_pc_4" value="{$form_data.action_price_client_pc_4|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円<br />
			[成果5]
			docomo:<input type="text" size="10" name="action_price_client_docomo_5" value="{$form_data.action_price_client_docomo_5|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
			softbank:<input type="text" size="10" name="action_price_client_softbank_5" value="{$form_data.action_price_client_softbank_5|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
			au:<input type="text" size="10" name="action_price_client_au_5" value="{$form_data.action_price_client_au_5|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
			pc:<input type="text" size="10" name="action_price_client_pc_5" value="{$form_data.action_price_client_pc_5|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円<br />
		</td>
	</tr>
	<tr>
		<th id="th_title">アクション単価(金額)(メディア)</th>
		<td>
			[成果1]
			docomo:<input type="text" size="10" name="action_price_media_docomo_1" value="{$form_data.action_price_media_docomo_1|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
			softbank:<input type="text" size="10" name="action_price_media_softbank_1" value="{$form_data.action_price_media_softbank_1|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
			au:<input type="text" size="10" name="action_price_media_au_1" value="{$form_data.action_price_media_au_1|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
			pc:<input type="text" size="10" name="action_price_media_pc_1" value="{$form_data.action_price_media_pc_1|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円<br />
			[成果2]
			docomo:<input type="text" size="10" name="action_price_media_docomo_2" value="{$form_data.action_price_media_docomo_2|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
			softbank:<input type="text" size="10" name="action_price_media_softbank_2" value="{$form_data.action_price_media_softbank_2|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
			au:<input type="text" size="10" name="action_price_media_au_2" value="{$form_data.action_price_media_au_2|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
			pc:<input type="text" size="10" name="action_price_media_pc_2" value="{$form_data.action_price_media_pc_2|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円<br />
			[成果3]
			docomo:<input type="text" size="10" name="action_price_media_docomo_3" value="{$form_data.action_price_media_docomo_3|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
			softbank:<input type="text" size="10" name="action_price_media_softbank_3" value="{$form_data.action_price_media_softbank_3|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
			au:<input type="text" size="10" name="action_price_media_au_3" value="{$form_data.action_price_media_au_3|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
			pc:<input type="text" size="10" name="action_price_media_pc_3" value="{$form_data.action_price_media_pc_3|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円<br />
			[成果4]
			docomo:<input type="text" size="10" name="action_price_media_docomo_4" value="{$form_data.action_price_media_docomo_4|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
			softbank:<input type="text" size="10" name="action_price_media_softbank_4" value="{$form_data.action_price_media_softbank_4|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
			au:<input type="text" size="10" name="action_price_media_au_4" value="{$form_data.action_price_media_au_4|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
			pc:<input type="text" size="10" name="action_price_media_pc_4" value="{$form_data.action_price_media_pc_4|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円<br />
			[成果5]
			docomo:<input type="text" size="10" name="action_price_media_docomo_5" value="{$form_data.action_price_media_docomo_5|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
			softbank:<input type="text" size="10" name="action_price_media_softbank_5" value="{$form_data.action_price_media_softbank_5|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
			au:<input type="text" size="10" name="action_price_media_au_5" value="{$form_data.action_price_media_au_5|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円
			pc:<input type="text" size="10" name="action_price_media_pc_5" value="{$form_data.action_price_media_pc_5|htmlspecialchars:$smarty.const.ENT_QUOTES}" />円<br />
		</td>
	</tr>
	<tr>
		<th id="th_title">広告原稿(テキスト1)</th>
		<td>
			<textarea cols="70" rows="2" name="ms_text_1">{$form_data.ms_text_1|htmlspecialchars:$smarty.const.ENT_QUOTES}</textarea>
		</td>
	</tr>
	<tr>
		<th id="th_title">広告原稿(テキスト2)</th>
		<td>
			<textarea cols="70" rows="2" name="ms_text_2">{$form_data.ms_text_2|htmlspecialchars:$smarty.const.ENT_QUOTES}</textarea>
		</td>
	</tr>
	<tr>
		<th id="th_title">広告原稿(テキスト3)</th>
		<td>
			<textarea cols="70" rows="2" name="ms_text_3">{$form_data.ms_text_3|htmlspecialchars:$smarty.const.ENT_QUOTES}</textarea>
		</td>
	</tr>
	<tr>
		<th id="th_title">広告原稿(テキスト4)</th>
		<td>
			<textarea cols="70" rows="2" name="ms_text_4">{$form_data.ms_text_4|htmlspecialchars:$smarty.const.ENT_QUOTES}</textarea>
		</td>
	</tr>
	<tr>
		<th id="th_title">広告原稿(テキスト5)</th>
		<td>
			<textarea cols="70" rows="2" name="ms_text_5">{$form_data.ms_text_5|htmlspecialchars:$smarty.const.ENT_QUOTES}</textarea>
		</td>
	</tr>
	<tr>
		<th id="th_title">広告原稿(イメージ1)</th>
		<td>
{if $form_data.ms_image_path_1 != ""}
			<img src="{$form_data.ms_image_path_1}" alt="イメージ1" /><br />
{/if}
			<input type="radio" name="ms_image_type_1" value="1"{if $form_data.ms_image_type_1 == 1 || $form_data.ms_image_type_1 == ""} checked="checked"{/if} /><label>アップロード</label><input type="file" size="30" name="ms_image_file_1" /><br/>
			<input type="radio" name="ms_image_type_1" value="2"{if $form_data.ms_image_type_1 == 2} checked="checked"{/if} /><label>URL指定</label><input type="text" size="30" name="ms_image_url_1" value="{if $form_data.ms_image_type_1 == 2}{$form_data.ms_image_url_1|htmlspecialchars:$smarty.const.ENT_QUOTES}{/if}" />
			<input type="hidden" name="ms_image_url_1" value="{$form_data.ms_image_url_1}" />
		</td>
	</tr>
	<tr>
		<th id="th_title">広告原稿(イメージ2)</th>
		<td>
{if $form_data.ms_image_path_2 != ""}
			<img src="{$form_data.ms_image_path_2}" alt="イメージ2" /><br />
{/if}
			<input type="radio" name="ms_image_type_2" value="1"{if $form_data.ms_image_type_2 == 1 || $form_data.ms_image_type_2 == ""} checked="checked"{/if} /><label>アップロード</label><input type="file" size="30" name="ms_image_file_2" /><br/>
			<input type="radio" name="ms_image_type_2" value="2"{if $form_data.ms_image_type_2 == 2} checked="checked"{/if} /><label>URL指定</label><input type="text" size="30" name="ms_image_url_2" value="{if $form_data.ms_image_type_1 == 2}{$form_data.ms_image_url_2|htmlspecialchars:$smarty.const.ENT_QUOTES}{/if}" />
			<input type="hidden" name="ms_image_url_2" value="{$form_data.ms_image_url_2}" />
		</td>
	</tr>
	<tr>
		<th id="th_title">広告原稿(イメージ3)</th>
		<td>
{if $form_data.ms_image_path_3 != ""}
			<img src="{$form_data.ms_image_path_3}" alt="イメージ3" /><br />
{/if}
			<input type="radio" name="ms_image_type_3" value="1"{if $form_data.ms_image_type_3 == 1 || $form_data.ms_image_type_3 == ""} checked="checked"{/if} /><label>アップロード</label><input type="file" size="30" name="ms_image_file_3" /><br/>
			<input type="radio" name="ms_image_type_3" value="2"{if $form_data.ms_image_type_3 == 2} checked="checked"{/if} /><label>URL指定</label><input type="text" size="30" name="ms_image_url_3" value="{if $form_data.ms_image_type_1 == 2}{$form_data.ms_image_url_3|htmlspecialchars:$smarty.const.ENT_QUOTES}{/if}" />
			<input type="hidden" name="ms_image_url_3" value="{$form_data.ms_image_url_3}" />
		</td>
	</tr>
	<tr>
		<th id="th_title">広告原稿(イメージ4)</th>
		<td>
{if $form_data.ms_image_path_4 != ""}
			<img src="{$form_data.ms_image_path_4}" alt="イメージ4" /><br />
{/if}
			<input type="radio" name="ms_image_type_4" value="1"{if $form_data.ms_image_type_4 == 1 || $form_data.ms_image_type_4 == ""} checked="checked"{/if} /><label>アップロード</label><input type="file" size="30" name="ms_image_file_4" /><br/>
			<input type="radio" name="ms_image_type_4" value="2"{if $form_data.ms_image_type_4 == 2} checked="checked"{/if} /><label>URL指定</label><input type="text" size="30" name="ms_image_url_4" value="{if $form_data.ms_image_type_1 == 2}{$form_data.ms_image_url_4|htmlspecialchars:$smarty.const.ENT_QUOTES}{/if}" />
			<input type="hidden" name="ms_image_url_4" value="{$form_data.ms_image_url_4}" />
		</td>
	</tr>
	<tr>
		<th id="th_title">広告原稿(イメージ5)</th>
		<td>
{if $form_data.ms_image_path_5 != ""}
			<img src="{$form_data.ms_image_path_5}" alt="イメージ5" /><br />
{/if}
			<input type="radio" name="ms_image_type_5" value="1"{if $form_data.ms_image_type_5 == 1 || $form_data.ms_image_type_5 == ""} checked="checked"{/if} /><label>アップロード</label><input type="file" size="30" name="ms_image_file_5" /><br/>
			<input type="radio" name="ms_image_type_5" value="2"{if $form_data.ms_image_type_5 == 2} checked="checked"{/if} /><label>URL指定</label><input type="text" size="30" name="ms_image_url_5" value="{if $form_data.ms_image_type_1 == 2}{$form_data.ms_image_url_5|htmlspecialchars:$smarty.const.ENT_QUOTES}{/if}" />
			<input type="hidden" name="ms_image_url_5" value="{$form_data.ms_image_url_5}" />
		</td>
	</tr>
	<tr>
		<th id="th_title">広告原稿(メール1)</th>
		<td>
			<textarea cols="70" rows="2" name="ms_email_1">{$form_data.ms_email_1|htmlspecialchars:$smarty.const.ENT_QUOTES}</textarea>
		</td>
	</tr>
	<tr>
		<th id="th_title">広告原稿(メール2)</th>
		<td>
			<textarea cols="70" rows="2" name="ms_email_2">{$form_data.ms_email_2|htmlspecialchars:$smarty.const.ENT_QUOTES}</textarea>
		</td>
	</tr>
	<tr>
		<th id="th_title">広告原稿(メール3)</th>
		<td>
			<textarea cols="70" rows="2" name="ms_email_3">{$form_data.ms_email_3|htmlspecialchars:$smarty.const.ENT_QUOTES}</textarea>
		</td>
	</tr>
	<tr>
		<th id="th_title">広告原稿(メール4)</th>
		<td>
			<textarea cols="70" rows="2" name="ms_email_4">{$form_data.ms_email_4|htmlspecialchars:$smarty.const.ENT_QUOTES}</textarea>
		</td>
	</tr>
	<tr>
		<th id="th_title">広告原稿(メール5)</th>
		<td>
			<textarea cols="70" rows="2" name="ms_email_5">{$form_data.ms_email_5|htmlspecialchars:$smarty.const.ENT_QUOTES}</textarea>
		</td>
	</tr>
	<tr>
		<th id="th_title">ユニーククリック種別</th>
		<td>
			{if $form_data.unique_click_type == 1}
				マンスリー
			{elseif $form_data.unique_click_type == 2}
				ウィークリー
			{elseif $form_data.unique_click_type == 3}
				デイリー
			{elseif $form_data.unique_click_type == 4}
				全てユニーク
			{/if}
		</td>
	</tr>
	<tr>
		<th id="th_title">ポイントバック</th>
		<td>
			{if $form_data.point_back_flag == 1}
				不可
			{elseif $form_data.point_back_flag == 2}
				可
			{/if}
		</td>
	</tr>
	<tr>
		<th id="th_title">アダルト</th>
		<td>
			{if $form_data.adult_flag == 1}
				不可
			{elseif $form_data.adult_flag == 2}
				可
			{/if}
		</td>
	</tr>
	<tr>
		<th id="th_title">出会い</th>
		<td>
			{if $form_data.dating_flag == 1}
				不可
			{elseif $form_data.dating_flag == 2}
				可
			{/if}
		</td>
	</tr>
	<tr>
		<th id="th_title">出稿開始日</th>
		<td>{$advert_start_date_view}</td>
	</tr>
	<tr>
		<th id="th_title">出稿終了日</th>
		<td>
			{if $form_data.unrestraint_flag == 1}
				無制限
			{else}
				{$advert_end_date_view}
			{/if}
		</td>
	</tr>
	<tr>
		<th id="th_title">テストフラグ</th>
		<td>
			{if $form_data.test_flag == 1}テスト用{/if}
		</td>
	</tr>
	<tr>
		<th id="th_title">ステータス</th>
		<td>
			{if $form_data.status == 1}
				予約
			{elseif $form_data.status == 2}
				出稿中
			{elseif $form_data.status == 3}
				終了
			{/if}
		</td>
	</tr>
	<tr>
		<th id="th_title">変更日</th>
		<td>
			<select name="r_year">
{section name=cnt start=2009 loop=2021}
				<option value="{$smarty.section.cnt.index}"{if $set_r_year == $smarty.section.cnt.index} selected="selected"{/if}>{$smarty.section.cnt.index}年</option>
{/section}
			</select>
			<select name="r_month">
{section name=cnt start=1 loop=13}
				<option value="{$smarty.section.cnt.index}"{if $set_r_month == $smarty.section.cnt.index} selected="selected"{/if}>{$smarty.section.cnt.index}月</option>
{/section}
			</select>
			<select name="r_day">
{section name=cnt start=1 loop=32}
				<option value="{$smarty.section.cnt.index}"{if $set_r_day == $smarty.section.cnt.index} selected="selected"{/if}>{$smarty.section.cnt.index}日</option>
{/section}
			</select>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<input type="submit" value="変更予約" />
			<input type="hidden" name="mode" value="{$mode}" />
			<input type="hidden" name="id" value="{$form_data.id}" />
			<input type="hidden" name="advert_id" value="{$form_data.advert_id}" />
		</td>
	</tr>
</table>
</form>
</div><!-- contents -->

{include file='./hooter.tpl'}