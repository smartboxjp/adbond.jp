{include file='./header.tpl' page_title='媒体('|cat:$sub_title|cat:')'}

<!-- Menu -->
{include file='./menu.tpl'}

<div id="my_contents">

<h2>媒体({$sub_title})</h2>

<div id=message>
{if $error_message != ''}
<div id="error_message">
<h3>ERROR:{$error_message}</h3>
</div>
{/if}
{if $info_message != ''}
<div id="info_message">
<h3>INFO:{$info_message}</h3>
</div>
{/if}
</div><!-- message -->

<form method="POST" action="{$smarty.server.PHP_SELF}" name="form1">
<table cellpadding="0" cellspacing="0">
	<tr>
		<th colspan="2" id="th_title">媒体発行者</th>
		<td>
			<select name="media_publisher_id">
			{foreach from=$media_publisher_array item="data" name="media_publisher_list"}
				<option value="{$data.id}"{if $form_data.media_publisher_id == $data.id} selected="selected"{/if}>{$data.name|htmlspecialchars:$smarty.const.ENT_QUOTES}</option>
			{/foreach}
			</select>
		</td>
	</tr>
	<tr>
		<th colspan="2" id="th_title">媒体カテゴリー</th>
		<td>
			<select name="media_category_id">
			{foreach from=$media_category_array item="data" name="media_category_list"}
				<option value="{$data.id}"{if $form_data.media_category_id == $data.id} selected="selected"{/if}>{$data.name|htmlspecialchars:$smarty.const.ENT_QUOTES}</option>
			{/foreach}
			</select>
		</td>
	</tr>
	<tr>
		<th colspan="2" id="th_title">サイト名</th>
		<td>
			<input type="text" size="50" name="media_name" value="{$form_data.media_name|htmlspecialchars:$smarty.const.ENT_QUOTES}" />
		</td>
	</tr>
	<tr>
		<th colspan="2" id="th_title">対応キャリア</th>
		<td>
			<input type="checkbox" name="support_docomo" value="1"{if $form_data.support_docomo == 1} checked="checked"{/if} /><label>docomo</label>
			<input type="checkbox" name="support_softbank" value="1"{if $form_data.support_softbank == 1} checked="checked"{/if} /><label>softbank</label>
			<input type="checkbox" name="support_au" value="1"{if $form_data.support_au == 1} checked="checked"{/if} /><label>au</label>
			<input type="checkbox" name="support_pc" value="1"{if $form_data.support_pc == 1} checked="checked"{/if} /><label>pc</label>
		</td>
	</tr>
	<tr>
		<th colspan="2" id="th_title">サイトURL(docomo)</th>
		<td>
			<input type="text" size="100" name="site_url_docomo" value="{$form_data.site_url_docomo|htmlspecialchars:$smarty.const.ENT_QUOTES}" />
		</td>
	</tr>
	<tr>
		<th colspan="2" id="th_title">サイトURL(softbank)</th>
		<td>
			<input type="text" size="100" name="site_url_softbank" value="{$form_data.site_url_softbank|htmlspecialchars:$smarty.const.ENT_QUOTES}" />
		</td>
	</tr>
	<tr>
		<th colspan="2" id="th_title">サイトURL(au)</th>
		<td>
			<input type="text" size="100" name="site_url_au" value="{$form_data.site_url_au|htmlspecialchars:$smarty.const.ENT_QUOTES}" />
		</td>
	</tr>
	<tr>
		<th colspan="2" id="th_title">サイトURL(pc)</th>
		<td>
			<input type="text" size="100" name="site_url_pc" value="{$form_data.site_url_pc|htmlspecialchars:$smarty.const.ENT_QUOTES}" />
		</td>
	</tr>
	<tr>
		<th colspan="2" id="th_title">メディア種別</th>
		<td>
			<input type="radio" name="media_type" value="1"{if $form_data.media_type == 1 || $form_data.media_type == ""} checked="checked"{/if} /><label>サイト</label>
			<input type="radio" name="media_type" value="2"{if $form_data.media_type == 2} checked="checked"{/if} /><label>メール</label>
			<input type="radio" name="media_type" value="3"{if $form_data.media_type == 3} checked="checked"{/if} /><label>その他</label>
		</td>
	</tr>
	<tr>
		<th colspan="2" id="th_title">PV/日(発行部数)</th>
		<td>
			<input type="text" size="30" name="page_view_day" value="{$form_data.page_view_day|htmlspecialchars:$smarty.const.ENT_QUOTES}" />
		</td>
	</tr>
	<tr>
		<th colspan="2" id="th_title">サイト概要</th>
		<td>
			<textarea cols="70" rows="5" name="site_outline">{$form_data.site_outline|htmlspecialchars:$smarty.const.ENT_QUOTES}</textarea>
		</td>
	</tr>
	<tr>
		<th colspan="2" id="th_title">ID種別</th>
		<td>
			<input type="radio" name="response_type" value="0"{if $form_data.response_type == 0 || $form_data.response_type == ""} checked="checked"{/if} /><label>個体識別ID</label>
			<input type="radio" name="response_type" value="1"{if $form_data.response_type == 1} checked="checked"{/if} /><label>セッションID</label>
			<input type="radio" name="response_type" value="2"{if $form_data.response_type == 2} checked="checked"{/if} /><label>セッションIDと個体識別ID</label>
		</td>
	</tr>
	<tr>
		<th colspan="2" id="th_title">ポイントバック通知先URL</th>
		<td>
			<input type="text" size="100" name="point_back_url" value="{$form_data.point_back_url|htmlspecialchars:$smarty.const.ENT_QUOTES}" /><br />
			＜セッションIDまたは、個体識別両方を返却する時＞<br />
			##ID##⇒「ID種別」で選択したID<br />
			##CID##⇒広告ID<br />
			##CLICK_DATE##⇒クリック日時<br />
			##ACTION_DATE##⇒成果発生日時<br /><br />

			＜セッションIDと個体識別両方を返却する時＞<br />
			##ID##⇒「セッションID」<br />
			##UID##⇒「個体識別ID」<br /><br />

			＜ユーザ識別IDの付加について＞<br />
			ユーザ識別IDを付加する位置に「##ID##」を記入する<br />
			http://example.jp/?id=***→http://example.jp/?id=##ID##<br />
			http://example.jp/***→http://example.jp/##ID##<br />
			http://example.jp/?pid=***→http://example.jp/?pid=##ID##<br /><br />
{if $form_data.id != ""}
			<input type="text" size="100" name="point_test_url" value="{$form_data.point_test_url|htmlspecialchars:$smarty.const.ENT_QUOTES}" readonly="readonly" /><br />
			<input type="button" name="test_url_make" value="テストURL発行" onclick="document.form1.point_test_url.value ='http://adbond.jp/action/point_test.php?guid=ON&m={$form_data.id}&a=0';" />
{/if}
		</td>
	</tr>
	<tr>
		<th colspan="2" id="th_title">テストフラグ</th>
		<td>
			<input type="checkbox" name="test_flag" value="1"{if $form_data.test_flag == 1} checked="checked"{/if} />テスト用
		</td>
	</tr>
	<tr>
		<th colspan="2" id="th_title">ステータス</th>
		<td>
			<input type="radio" name="status" value="1"{if $form_data.status == 1 || $form_data.status == ""} checked="checked"{/if} /><label>仮登録</label>
			<input type="radio" name="status" value="2"{if $form_data.status == 2} checked="checked"{/if} /><label>正規</label>
			<input type="radio" name="status" value="3"{if $form_data.status == 3} checked="checked"{/if} /><label>退会</label>
		</td>
	</tr>
	<tr>
		<td colspan="3">
			<input type="submit" value="登録" />
			<input type="hidden" name="mode" value="{$mode}" />
			<input type="hidden" name="id" value="{$form_data.id}" />
		</td>
	</tr>
</table>
</form>

</div><!-- contents -->

{include file='./hooter.tpl'}