{include file='./header.tpl' page_title='成果ログ'}

<!-- Menu -->
{include file='./menu.tpl'}

<div id="my_contents">

<h2>成果ログ</h2>

<div id=message>
{if $error_message != '' }
<div id="error_message">
	<h3>ERROR:{$error_message}</h3>
</div>
{/if}
{if $info_message != '' }
<div id="info_message">
	<h3>INFO:{$info_message}</h3>
</div>
{/if}
</div><!-- message -->

<form method="GET" action="{$smarty.server.PHP_SELF}">
</form>

<table cellpadding="0" cellspacing="0">
<!--	<tr>-->
<!--		<th colspan="10" id="th_title">-->
<!--			<input type="button" value="CSVダウンロード" onclick="location.href='./result_log_download.php?date={$download_date}&type={$log_type}'" />-->
<!--		</th>-->
<!--	</tr>-->
	<tr id="result_a">
		<th colspan="10" id="th_title">【該当{$list_count}件】
{if $list_count > 1000}
	{if $prev == 0}
			<span class="prev">PREV</span>
	{else}
			<a class="prev" href="?log_type={$log_type}&mode={$mode}&download_date={$download_date}&page={$prev}">PREV</a>
	{/if}
	{section name=cnt start=1 loop=$page_max+1}
		{if $smarty.section.cnt.index == $page}
			<span>[{$smarty.section.cnt.index}]</span>
		{else}
			&nbsp;<a href="?log_type={$log_type}&mode={$mode}&download_date={$download_date}&page={$smarty.section.cnt.index}">{$smarty.section.cnt.index}</a>&nbsp;
		{/if}
	{/section}
	{if $next == 0}
		<span class="next">NEXT</span>
	{else}
		<a class="next" href="?log_type={$log_type}&mode={$mode}&download_date={$download_date}&page={$next}">NEXT</a>
	{/if}
{/if}
		</th>
	</tr>
	<tr>
		<th id="th_title">クリック日時</th>
		<th id="th_title">媒体ID</th>
		<th id="th_title">広告ID</th>
		<th id="th_title">キャリア</th>
		<th id="th_title">ユーザーエージェント</th>
		<th id="th_title">個体識別番号</th>
		<th id="th_title">IPアドレス</th>
		<th id="th_title">ホスト名</th>
		<th id="th_title">リンク先URL</th>
		<th id="th_title">ステータス</th>
	</tr>
{foreach from=$list key="key" item="data" name="list"}
	<tr>
		<td>{$data.created_at}</td>
		<td>{$data.media_id}</td>
		<td>{$data.advert_id}</td>
		<td>
{if $data.carrier_id == 1}
			docomo
{elseif $data.carrier_id == 2}
			softbank
{elseif $data.carrier_id == 3}
			au
{elseif $data.carrier_id == 4}
			pc
{/if}
		</td>
		<td>{$data.user_agent}</td>
		<td>{$data.uid}</td>
		<td>{$data.ip_address}</td>
		<td>{$data.host_name}</td>
		<td>{$data.link_url}</td>
		<td>
{if $data.status == 1}
			クリック
{elseif $data.status == 2}
			登録
{/if}
		</td>
	</tr>
{/foreach}
</table>

</div><!-- contents -->

{include file='./hooter.tpl'}