{include file='./header.tpl' page_title='媒体発行者別集計'}

<!-- Menu -->
{include file='./menu.tpl'}

<div id="my_contents">

<h2>媒体発行者別集計</h2>

<div id=message>
{if $error_message != '' }
<div id="error_message">
	<h3>ERROR:{$error_message}</h3>
</div>
{/if}
{if $info_message != '' }
<div id="info_message">
	<h3>INFO:{$info_message}</h3>
</div>
{/if}
</div><!-- message -->

<form method="POST" action="{$smarty.server.PHP_SELF}">
<table cellpadding="0" cellspacing="0">
	<tr>
		<th id="th_title">媒体発行者</th>
		<td>
			<select name="media_publisher_id">
				<option value="0">指定しない</option>
{foreach from=$media_publisher_array item="data" name="media_publisher_list"}
				<option value="{$data.id}"{if $search.media_publisher_id == $data.id} selected="selected"{/if}>{$data.name|htmlspecialchars:$smarty.const.ENT_QUOTES}</option>
{/foreach}
			</select>
		</td>
	</tr>
	<tr>
		<td rowspan="2" id="th_title">集計日時</td>
		<td>
			<input type="radio" name="select_date_type" value="1"{if $search.select_date_type == 1} checked="checked"{/if} />年月指定
			<select name="monthly_year">
{section name=cnt start=2009 loop=2021}
				<option value="{$smarty.section.cnt.index}"{if $search.monthly_year == $smarty.section.cnt.index} selected="selected"{/if}>{$smarty.section.cnt.index}年</option>
{/section}
			</select>
			<select name="monthly_month">
{section name=cnt start=1 loop=13}
				<option value="{$smarty.section.cnt.index}"{if $search.monthly_month == $smarty.section.cnt.index} selected="selected"{/if}>{$smarty.section.cnt.index}月</option>
{/section}
			</select>
		</td>
	</tr>
	<tr>
		<td>
			<input type="radio" name="select_date_type" value="2"{if $search.select_date_type == 2} checked="checked"{/if} />期間指定
			<select name="between_start_year">
{section name=cnt start=2009 loop=2021}
				<option value="{$smarty.section.cnt.index}"{if $search.between_start_year == $smarty.section.cnt.index} selected="selected"{/if}>{$smarty.section.cnt.index}年</option>
{/section}
			</select>
			<select name="between_start_month">
{section name=cnt start=1 loop=13}
				<option value="{$smarty.section.cnt.index}"{if $search.between_start_month == $smarty.section.cnt.index} selected="selected"{/if}>{$smarty.section.cnt.index}月</option>
{/section}
			</select>
			<select name="between_start_day">
{section name=cnt start=1 loop=32}
				<option value="{$smarty.section.cnt.index}"{if $search.between_start_day == $smarty.section.cnt.index} selected="selected"{/if}>{$smarty.section.cnt.index}日</option>
{/section}
			</select> ～
			<select name="between_end_year">
{section name=cnt start=2009 loop=2021}
				<option value="{$smarty.section.cnt.index}"{if $search.between_end_year == $smarty.section.cnt.index} selected="selected"{/if}>{$smarty.section.cnt.index}年</option>
{/section}
			</select>
			<select name="between_end_month">
{section name=cnt start=1 loop=13}
				<option value="{$smarty.section.cnt.index}"{if $search.between_end_month == $smarty.section.cnt.index} selected="selected"{/if}>{$smarty.section.cnt.index}月</option>
{/section}
			</select>
			<select name="between_end_day">
{section name=cnt start=1 loop=32}
				<option value="{$smarty.section.cnt.index}"{if $search.between_end_day == $smarty.section.cnt.index} selected="selected"{/if}>{$smarty.section.cnt.index}日</option>
{/section}
			</select>
		</td>
	</tr>
	<tr>
		<td colspan="3">
			<input type="submit" value="検索" />
			<input type="hidden" name="mode" value="search" />
		</td>
	</tr>
</table>
</form>

<table cellpadding="0" cellspacing="0">
	<tr>
		<th colspan="5" id="th_title">
{if $search.media_publisher_id != 0}
			{$media_publisher_array[$search.media_publisher_id].name|htmlspecialchars:$smarty.const.ENT_QUOTES}
{/if}
{if $search.select_date_type == 1}
			[{$search.monthly_year}年{$search.monthly_month}月 集計]
{elseif $search.select_date_type == 2}
			[{$search.between_start_year}年{$search.between_start_month}月{$search.between_start_day}日～{$search.between_end_year}年{$search.between_end_month}月{$search.between_end_day}日 集計]
{/if}
		</th>
	</tr>
	<tr>
		<th colspan="5" id="th_title">【該当{$list_count}件】</th>
	</tr>
	<tr>
		<th id="th_title">媒体発行者名</th>
		<th id="th_title">媒体数</th>
		<th id="th_title">クリック数</th>
		<th id="th_title">アクション数</th>
		<th id="th_title">金額合計</th>
	</tr>
{foreach from=$list key="key" item="data" name="list"}
	<tr>
		<td><a href="./media_publisher.php?mode=edit&id={$data.media_publisher_id}">{$data.publisher_name|htmlspecialchars:$smarty.const.ENT_QUOTES}</a></td>
		<td>{$data.media_count|htmlspecialchars:$smarty.const.ENT_QUOTES}</td>
		<td><a href="./summary_click_detail.php?mp_id={$data.media_publisher_id}&type={$type}&date={$date}&start_date={$start_date}&end_date={$end_date}&aggregate_flag=media">{$data.click_count|number_format}</a></td>
		<td><a href="./summary_action_detail.php?mp_id={$data.media_publisher_id}&type={$type}&date={$date}&start_date={$start_date}&end_date={$end_date}&aggregate_flag=media">{$data.action_count|number_format}</a></td>
		<td>\{$data.total_price|number_format}</td>
	</tr>
{/foreach}
</table>

</div><!-- contents -->

{include file='./hooter.tpl'}