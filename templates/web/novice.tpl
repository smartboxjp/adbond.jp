{include file='./header_web.tpl' page_title='TOP'}

{include file='./menu.tpl'}
<br />

<div id="side_contents_2">
	{include file='./side_contents_2.tpl'}
</div><!-- side_contents -->

<div id="main_contents_2">
<div id="content_iti">

	<div class="padding">
<!--          	<h2><img src="images/web/h2_media.gif" alt="メディア" width="141" height="30" class="padding" /></h2>-->
		<div id="side_contents_header">
			<p><b>アフィリエイトとは</b></p>
		</div>
		<br />

			<p>
			「アフィリエイト (affiliate)」とは「提携する」という意味であり、
			アフィリエイトプログラムはインターネット広告において「成果報酬型広告」のことを指します。
			WEBサイトの運営者様（メディアオーナー様）が広告主様と提携をして、
			自身のWEBサイト内に「商品購入」や「資料請求」などを促進する広告を掲載し、
			その広告を通じて実際に商品が購入されたり資料請求が行われた場合に、
			成果に応じてメディアオーナー様に報酬が支払われる仕組みがアフィリエイトプログラムです。
			<br />
			<br />
			国内でのアフィリエイト元年と言われる2001年以降、この「アフィリエイト」という言葉は瞬く間に浸透し、
			現在ではWEBメディアオーナー様の大多数が新たな収入源としてアフィリエイトプログラムを利用しています。
			今や副収入を得るための有力な選択肢のひとつ、それが「アフィリエイト」です。
			</p>

	</div>

	<br />

	<div class="img_center">
		<p><img src="images/web/novice_user.gif" width="100%" height="161" alt="イメージ" /></p>
	</div>

</div>
</div>
<!-- main_contents -->
{include file='./hooter_web.tpl'}

