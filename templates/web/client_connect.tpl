{include file='./header.tpl' page_title='クライアント管理'}

<!-- Menu -->
{include file='./client_menu.tpl'}

<div id="my_contents">

<h2>提携サイト</h2>

<div id=message>
{if $error_message != '' }
<div id="error_message">
	<h3>ERROR:{$error_message}</h3>
</div>
{/if}
{if $info_message != '' }
<div id="info_message">
	<h3>INFO:{$info_message}</h3>
</div>
{/if}
</div><!-- message -->

<form method="POST" action="{$smarty.server.PHP_SELF}">
<table cellpadding="0" cellspacing="0">
	<tr>
		<th colspan="2">広告選択</th>
		<td>
			<select name="advert_id">
			{foreach from=$advert_array item="data" name="advert_list"}
				<option value="{$data.id}"{if $advert_id == $data.id} selected="selected"{/if}>{$data.advert_name|htmlspecialchars:$smarty.const.ENT_QUOTES}</option>
			{/foreach}
			</select>
			<input type="submit" name="submit" value="選択" />
		</td>
	</tr>
</table>
{if $advert_id != ""}
<table cellpadding="0" cellspacing="0">
	<tr>
		<th>サイト名</th>
		<th>メディア種別</th>
		<th>カテゴリ</th>
		<th>概要</th>
		<th>ステータス</th>
	</tr>
{foreach from=$connect_array item="data" name="connect_list"}
	<tr>
		<td>{$data.media_name|htmlspecialchars:$smarty.const.ENT_QUOTES}</td>
		<td>
{if $data.media_type == 1}
			サイト
{elseif $data.media_type == 2}
			メール
{elseif $data.media_type == 3}
			その他
{/if}
		</td>
		<td>{$data.category_name|htmlspecialchars:$smarty.const.ENT_QUOTES}</td>
		<td>{$data.site_outline|htmlspecialchars:$smarty.const.ENT_QUOTES}</td>
		<td>提携済み</td>
	</tr>
{/foreach}
</table>
{/if}
</form>
</div><!-- contents -->

<!-- フッター -->
{include file='./hooter.tpl'}