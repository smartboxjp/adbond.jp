{include file='./header.tpl' page_title='メディア管理'}

<!-- Menu -->
{include file='./media_menu.tpl' user_name=$user_name}

<div id="my_contents">

<h2>広告検索</h2>

<div id=message>
{if $error_message != '' }
<div id="error_message">
	<h3>ERROR:{$error_message}</h3>
</div>
{/if}
{if $info_message != '' }
<div id="info_message">
	<h3>INFO:{$info_message}</h3>
</div>
{/if}
</div><!-- message -->

<table cellpadding="0" cellspacing="0">
	<tr>
		<th colspan="2" id="th_title">広告情報</th>
	</tr>
	<tr>
		<th id="th_title">広告カテゴリー</th>
		<td>{$data.advert_category_name|htmlspecialchars:$smarty.const.ENT_QUOTES}</td>
	</tr>
	<tr>
		<th id="th_title">広告名</th>
		<td>{$data.advert_name|htmlspecialchars:$smarty.const.ENT_QUOTES}</td>
	</tr>
	<tr>
		<th id="th_title">コンテンツ種別</th>
		<td>
{if $data.content_type == 1}
			一般
{elseif $data.content_type == 2}
			公式
{/if}
		</td>
	</tr>
	<tr>
		<th id="th_title">対応キャリア</th>
		<td>
			{if $data.support_docomo == 1} docomo{/if}
			{if $data.support_softbank == 1} softbank{/if}
			{if $data.support_au == 1} au{/if}
			{if $data.support_pc == 1} pc{/if}
		</td>
	</tr>
	<tr>
		<th id="th_title">広告概要</th>
		<td>{$data.site_outline|htmlspecialchars:$smarty.const.ENT_QUOTES|nl2br}</td>
	</tr>
	<tr>
		<th id="th_title">クリック単価</th>
		<td>&yen;{$data.click_price_media|number_format}</td>
	</tr>
	<tr>
		<th id="th_title">アクション単価</th>
		<td>
			docomo:&yen;{$data.action_price_media_docomo_1|number_format}
			softbank:&yen;{$data.action_price_media_softbank_1|number_format}
			au:&yen;{$data.action_price_media_au_1|number_format}
			pc:&yen;{$data.action_price_media_pc_1|number_format}
		</td>
	</tr>
	<tr>
		<th id="th_title">ポイントバック</th>
		<td>
			{if $data.point_back_flag == 1}不可{/if}
			{if $data.point_back_flag == 2}可{/if}
		</td>
	</tr>
	<tr>
		<th id="th_title">アダルト</th>
		<td>
			{if $data.adult_flag == 1}不可{/if}
			{if $data.adult_flag == 2}可{/if}
		</td>
	</tr>
	<tr>
		<th id="th_title">出会い</th>
		<td>
			{if $data.dating_flag == 1}不可{/if}
			{if $data.dating_flag == 2}可{/if}
		</td>
	</tr>
	<tr>
		<th id="th_title">出稿開始日</th>
		<td>{$data.advert_start_date}</td>
	</tr>
	<tr>
		<th id="th_title">出稿終了日</th>
		<td>
{if $data.unrestraint_flag == 1}
		無期限
{else}
		{$data.advert_end_date}
{/if}
		</td>
	</tr>
	<tr>
		<th id="th_title">提携状態</th>
		<td>
			{if $connect_flag == 1}提携済み{else}未提携{/if}
		</td>
	</tr>
{if $connect_flag == 1}
	<tr>
		<th id="th_title">広告URL</th>
		<td><input type="text" size="60" readonly="readonly" value="{$data.advert_url}" /></td>
	</tr>
{else}
	<tr>
		<td colspan="2">
			<form method="POST" action="{$smarty.server.PHP_SELF}">
				<input type="submit" value="提携する" />
				<input type="hidden" name="mode" value="confirm" />
				<input type="hidden" name="advert_id" value="{$data.id}" />
				<input type="hidden" name="media_id" value="{$media_id}" />
			</form>
		</td>
	</tr>
{/if}
</table>
<table cellpadding="0" cellspacing="0">
	<tr>
		<th colspan="2" id="th_title">広告原稿</th>
	</tr>
{if $data.ms_text_1 != ""}
	{if $connect_flag == 1}
	<tr>
		<th rowspan="2" id="th_title">テキスト1</th>
		<td>{$data.ms_text_1|htmlspecialchars:$smarty.const.ENT_QUOTES}</td>
	</tr>
	<tr>
		<td><textarea cols="70" rows="2" readonly="readonly"><a href="{$data.advert_url}">{$data.ms_text_1|htmlspecialchars:$smarty.const.ENT_QUOTES}</a></textarea></td>
	</tr>
	{else}
	<tr>
		<th id="th_title">テキスト1</th>
		<td>{$data.ms_text_1|htmlspecialchars:$smarty.const.ENT_QUOTES}</td>
	</tr>
	{/if}
{/if}
{if $data.ms_text_2 != ""}
	{if $connect_flag == 1}
	<tr>
		<th rowspan="2" id="th_title">テキスト2</th>
		<td>{$data.ms_text_2|htmlspecialchars:$smarty.const.ENT_QUOTES}</td>
	</tr>
	<tr>
		<td><textarea cols="70" rows="2" readonly="readonly"><a href="{$data.advert_url}">{$data.ms_text_2|htmlspecialchars:$smarty.const.ENT_QUOTES}</a></textarea></td>
	</tr>
	{else}
	<tr>
		<th id="th_title">テキスト2</th>
		<td>{$data.ms_text_2|htmlspecialchars:$smarty.const.ENT_QUOTES}</td>
	</tr>
	{/if}
{/if}
{if $data.ms_text_3 != ""}
	{if $connect_flag == 1}
	<tr>
		<th rowspan="2" id="th_title">テキスト3</th>
		<td>{$data.ms_text_3|htmlspecialchars:$smarty.const.ENT_QUOTES}</td>
	</tr>
	<tr>
		<td><textarea cols="70" rows="2" readonly="readonly"><a href="{$data.advert_url}">{$data.ms_text_3|htmlspecialchars:$smarty.const.ENT_QUOTES}</a></textarea></td>
	</tr>
	{else}
	<tr>
		<th id="th_title">テキスト3</th>
		<td>{$data.ms_text_3|htmlspecialchars:$smarty.const.ENT_QUOTES}</td>
	</tr>
	{/if}
{/if}
{if $data.ms_text_4 != ""}
	{if $connect_flag == 1}
	<tr>
		<th rowspan="2" id="th_title">テキスト4</th>
		<td>{$data.ms_text_4|htmlspecialchars:$smarty.const.ENT_QUOTES}</td>
	</tr>
	<tr>
		<td><textarea cols="70" rows="2" readonly="readonly"><a href="{$data.advert_url}">{$data.ms_text_4|htmlspecialchars:$smarty.const.ENT_QUOTES}</a></textarea></td>
	</tr>
	{else}
	<tr>
		<th id="th_title">テキスト4</th>
		<td>{$data.ms_text_4|htmlspecialchars:$smarty.const.ENT_QUOTES}</td>
	</tr>
	{/if}
{/if}
{if $data.ms_text_5 != ""}
	{if $connect_flag == 1}
	<tr>
		<th rowspan="2" id="th_title">テキスト5</th>
		<td>{$data.ms_text_5|htmlspecialchars:$smarty.const.ENT_QUOTES}</td>
	</tr>
	<tr>
		<td><textarea cols="70" rows="2" readonly="readonly"><a href="{$data.advert_url}">{$data.ms_text_5|htmlspecialchars:$smarty.const.ENT_QUOTES}</a></textarea></td>
	</tr>
	{else}
	<tr>
		<th id="th_title">テキスト5</th>
		<td>{$data.ms_text_5|htmlspecialchars:$smarty.const.ENT_QUOTES}</td>
	</tr>
	{/if}
{/if}
{if $data.ms_image_path_1 != ""}
	{if $connect_flag == 1}
	<tr>
		<th rowspan="2" id="th_title">イメージ1</th>
		<td><img src="{$data.ms_image_path_1}" alt="イメージ1" /></td>
	</tr>
	<tr>
		<td><textarea cols="70" rows="2" readonly="readonly"><a href="{$data.advert_url}"><img src="{$data.ms_image_path_1}" /></a></textarea></td>
	</tr>
	{else}
	<tr>
		<th id="th_title">イメージ1</th>
		<td><img src="{$data.ms_image_path_1}" alt="イメージ1" /></td>
	</tr>
	{/if}
{/if}
{if $data.ms_image_path_2 != ""}
	{if $connect_flag == 1}
	<tr>
		<th rowspan="2" id="th_title">イメージ2</th>
		<td><img src="{$data.ms_image_path_2}" alt="イメージ2" /></td>
	</tr>
	<tr>
		<td><textarea cols="70" rows="2" readonly="readonly"><a href="{$data.advert_url}"><img src="{$data.ms_image_path_2}" /></a></textarea></td>
	</tr>
	{else}
	<tr>
		<th id="th_title">イメージ2</th>
		<td><img src="{$data.ms_image_path_1}" alt="イメージ2" /></td>
	</tr>
	{/if}
{/if}
{if $data.ms_image_path_3 != ""}
	{if $connect_flag == 1}
	<tr>
		<th rowspan="2" id="th_title">イメージ3</th>
		<td><img src="{$data.ms_image_path_3}" alt="イメージ3" /></td>
	</tr>
	<tr>
		<td><textarea cols="70" rows="2" readonly="readonly"><a href="{$data.advert_url}"><img src="{$data.ms_image_path_3}" /></a></textarea></td>
	</tr>
	{else}
	<tr>
		<th id="th_title">イメージ3</th>
		<td><img src="{$data.ms_image_path_3}" alt="イメージ3" /></td>
	</tr>
	{/if}
{/if}
{if $data.ms_image_path_4 != ""}
	{if $connect_flag == 1}
	<tr>
		<th rowspan="2" id="th_title">イメージ4</th>
		<td><img src="{$data.ms_image_path_4}" alt="イメージ4" /></td>
	</tr>
	<tr>
		<td><textarea cols="70" rows="2" readonly="readonly"><a href="{$data.advert_url}"><img src="{$data.ms_image_path_4}" /></a></textarea></td>
	</tr>
	{else}
	<tr>
		<th id="th_title">イメージ4</th>
		<td><img src="{$data.ms_image_path_4}" alt="イメージ4" /></td>
	</tr>
	{/if}
{/if}
{if $data.ms_image_path_5 != ""}
	{if $connect_flag == 1}
	<tr>
		<th rowspan="2" id="th_title">イメージ5</th>
		<td><img src="{$data.ms_image_path_5}" alt="イメージ5" /></td>
	</tr>
	<tr>
		<td><textarea cols="70" rows="2" readonly="readonly"><a href="{$data.advert_url}"><img src="{$data.ms_image_path_5}" /></a></textarea></td>
	</tr>
	{else}
	<tr>
		<th id="th_title">イメージ5</th>
		<td><img src="{$data.ms_image_path_5}" alt="イメージ5" /></td>
	</tr>
	{/if}
{/if}
{if $data.ms_email_1 != ""}
	{if $connect_flag == 1}
	<tr>
		<th id="th_title">メール1</th>
		<td><textarea cols="70" rows="2" readonly="readonly">{$data.ms_email_1|htmlspecialchars:$smarty.const.ENT_QUOTES}</textarea></td>
	</tr>
	{else}
	<tr>
		<th id="th_title">メール1</th>
		<td>{$data.ms_email_1|htmlspecialchars:$smarty.const.ENT_QUOTES}</td>
	</tr>
	{/if}
{/if}
{if $data.ms_email_2 != ""}
	<tr>
		<th id="th_title">メール2</th>
		<td>{$data.ms_email_2|htmlspecialchars:$smarty.const.ENT_QUOTES}</td>
	</tr>
{/if}
{if $data.ms_email_3 != ""}
	<tr>
		<th id="th_title">メール3</th>
		<td>{$data.ms_email_3|htmlspecialchars:$smarty.const.ENT_QUOTES}</td>
	</tr>
{/if}
{if $data.ms_email_4 != ""}
	<tr>
		<th id="th_title">メール4</th>
		<td>{$data.ms_email_4|htmlspecialchars:$smarty.const.ENT_QUOTES}</td>
	</tr>
{/if}
{if $data.ms_email_5 != ""}
	<tr>
		<th id="th_title">メール5</th>
		<td>{$data.ms_email_5|htmlspecialchars:$smarty.const.ENT_QUOTES}</td>
	</tr>
{/if}
</table>
</div><!-- contents -->

<!-- フッター -->
{include file='./hooter.tpl'}