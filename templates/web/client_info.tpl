{include file='./header2.tpl' page_title='クライアント管理'}

<!-- Menu -->
{include file='./client_menu.tpl'}

<div id="my_contents">

<h2>登録情報</h2>

<div id=message>
{if $error_message != '' }
<div id="error_message">
	<h3>ERROR:{$error_message}</h3>
</div>
{/if}
{if $info_message != '' }
<div id="info_message">
	<h3>INFO:{$info_message}</h3>
</div>
{/if}
</div><!-- message -->

<table cellpadding="0" cellspacing="0">
	<tr>
		<th colspan="2" id="th_title" >登録情報</th>
	</tr>
	<tr>
		<th id="th_title">ログインID</th>
		<td>{$data.login_id|htmlspecialchars:$smarty.const.ENT_QUOTES}</td>
	</tr>
	<tr>
		<th id="th_title">パスワード</th>
		<td>{$data.login_pass|htmlspecialchars:$smarty.const.ENT_QUOTES}</td>
	</tr>
	<tr>
		<th id="th_title">企業名/個人名</th>
		<td>{$data.client_name|htmlspecialchars:$smarty.const.ENT_QUOTES}</td>
	</tr>
	<tr>
		<th id="th_title">担当者名 ※法人の場合のみ</th>
		<td>{$data.contact_person|htmlspecialchars:$smarty.const.ENT_QUOTES}</td>
	</tr>
	<tr>
		<th id="th_title">TEL</th>
		<td>{$data.tel|htmlspecialchars:$smarty.const.ENT_QUOTES}</td>
	</tr>
	<tr>
		<th id="th_title">FAX</th>
		<td>{$data.fax|htmlspecialchars:$smarty.const.ENT_QUOTES}</td>
	</tr>
	<tr>
		<th id="th_title">メールアドレス</th>
		<td>{$data.email|htmlspecialchars:$smarty.const.ENT_QUOTES}</td>
	</tr>
</table>

<form method="POST" action="{$smarty.server.PHP_SELF}">
<table cellpadding="0" cellspacing="0">
	<tr>
		<th colspan="2" id="th_title">広告選択</th>
		<td>
			<select name="advert_id">
			{foreach from=$advert_array item="data" name="advert_list"}
				<option value="{$data.id}"{if $advert_id == $data.id} selected="selected"{/if}>{$data.advert_name|htmlspecialchars:$smarty.const.ENT_QUOTES}</option>
			{/foreach}
			</select>
			<input type="submit" name="submit" value="選択" />
		</td>
	</tr>
</table>
</form>
{if $advert_id != ""}
<table cellpadding="0" cellspacing="0">
	<tr>
		<th id="th_title">広告カテゴリー</th>
		<td>{$advert_data.category_name|htmlspecialchars:$smarty.const.ENT_QUOTES}</td>
	</tr>
	<tr>
		<th id="th_title">広告名</th>
		<td>{$advert_data.advert_name|htmlspecialchars:$smarty.const.ENT_QUOTES}</td>
	</tr>
	<tr>
		<th id="th_title">コンテンツ種別</th>
		<td>
{if $advert_data.content_type == 1}
			一般
{elseif $advert_data.content_type == 2}
			公式
{/if}
		</td>
	</tr>
	<tr>
		<th id="th_title">対応キャリア</th>
		<td>
			{if $advert_data.support_docomo == 1} docomo{/if}
			{if $advert_data.support_softbank == 1} softbank{/if}
			{if $advert_data.support_au == 1} au{/if}
			{if $advert_data.support_pc == 1} pc{/if}
		</td>
	</tr>
	<tr>
		<th id="th_title">ポイントバック対応</th>
		<td>
{if $advert_data.point_back_flag == 1}
			不可
{elseif $advert_data.point_back_flag == 2}
			可
{/if}
		</td>
	</tr>
</table>
{/if}
</div><!-- contents -->

<!-- フッター -->
{include file='./hooter2.tpl'}