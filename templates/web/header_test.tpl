﻿<?xml version="1.0" encoding="utf-8" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
{config_load file='web_app.conf'}
<title>{#title#}&nbsp;{$page_title}</title>
<link rel="stylesheet" type="text/css" href="../css/reset.css" media="all" />
<link rel="stylesheet" type="text/css" href="../css/web_main_test.css" media="all" />
</head>
<body>
<div id="container">

<!-- ヘッダー -->
<div id="my_header">
	<div id="header_contents_1">
		<p>{#title#}&nbsp;{$page_title} テスト</p>
		<p>{$user_name|htmlspecialchars:$smarty.const.ENT_QUOTES} 様</p>
	</div>
	<div id="header_contents_2">
		<p>　</p>
		<p>トップ　お問い合わせ　<a href="../index.php?media_logout=y">ログアウト</a></p>
	</div>
</div><!-- header -->