﻿<?xml version="1.0" encoding="utf-8" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja">

<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
{config_load file='web_app.conf'}
<title>{#title#}&nbsp;{$page_title}</title>
<link rel="stylesheet" type="text/css" href="./css/reset.css" media="all" />
<link rel="stylesheet" type="text/css" href="./css/web_top_test.css" media="all" />
</head>
<body>
<div id="container">

<!-- ヘッダー -->
<div id="my_center">
<div id="my_header">

<div id="header_navigation">



	<ul>
		<li>
			<img src="./images/web/header_title_banner.jpg" width="153" height="32" alt="{#title#}" align="left" />
		</li>

		<li><span></span></li>
		<li><span></span></li>


		<li id="header_navi_regist">
			<a href="/">メディアオーナー様登録</a>
		</li>

		<li><span></span></li>

		<li id="header_navi_contact">
			<a href="/">お問い合わせ</a>
		</li>
	</ul>
</div>

<!--
	<table id="header_navigation" width="800" >
		<tr>
			<td width="459">
				<img src="./images/web/header_title_banner.jpg" width="153" height="32" alt="{#title#}" align="left" />
			</td>

			<td id="header_navi_regist" width="170" align="left" >
				<a href="/">メディアオーナー様登録</a>

			</td>
			<td id="header_navi_contact" width="170">
				<a href="/">お問い合わせ</a>

			</td>
		</tr>
	</table>
 -->

</div>
<!-- header -->