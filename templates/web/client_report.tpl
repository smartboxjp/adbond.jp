{include file='./header2.tpl' page_title='クライアント管理'}

<!-- Menu -->
{include file='./client_menu.tpl'}

<div id="my_contents">

<h2>レポート</h2>

<div id=message>
{if $error_message != '' }
<div id="error_message">
	<h3>ERROR:{$error_message}</h3>
</div>
{/if}
{if $info_message != '' }
<div id="info_message">
	<h3>INFO:{$info_message}</h3>
</div>
{/if}
</div><!-- message -->

<form method="POST" action="{$smarty.server.PHP_SELF}">
<table cellpadding="0" cellspacing="0">
	<tr>
		<th colspan="2" id="th_title">広告選択</th>
		<td>
			<select name="advert_id">
				<!-- advert_id = 0 のときは全広告検索 -->
				<option value="0"{if $advert_id == '0'} selected="selected"{/if}>全広告レポート</option>
			{foreach from=$advert_array item="data" name="advert_list"}
				<option value="{$data.id}"{if $advert_id == $data.id} selected="selected"{/if}>{$data.advert_name|htmlspecialchars:$smarty.const.ENT_QUOTES}</option>
			{/foreach}
			</select>
			<input type="submit" name="submit" value="選択" />
		</td>
	</tr>
</table>
{if $advert_id != ""}
<table cellpadding="0" cellspacing="0">
	<tr>
		<td colspan="2">
			<input type="radio" name="date_type" value="1"{if $date_type == 1} checked="checked"{/if} /><label>月別</label>
			<input type="radio" name="date_type" value="2"{if $date_type == 2} checked="checked"{/if} /><label>日別</label>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<input type="radio" name="carrier" value="1"{if $carrier == 1} checked="checked"{/if} /><label>キャリア合計</label>
			<input type="radio" name="carrier" value="2"{if $carrier == 2} checked="checked"{/if} /><label>キャリア別</label>
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<select name="s_year">
{section name=cnt start=2009 loop=2021}
				<option value="{$smarty.section.cnt.index}"{if $set_s_year == $smarty.section.cnt.index} selected="selected"{/if}>{$smarty.section.cnt.index}年</option>
{/section}
			</select>
			<select name="s_month">
{section name=cnt start=1 loop=13}
				<option value="{$smarty.section.cnt.index}"{if $set_s_month == $smarty.section.cnt.index} selected="selected"{/if}>{$smarty.section.cnt.index}月</option>
{/section}
			</select>
			<input type="submit" value="表示" />
			<input type="hidden" name="mode" value="search" />
		</td>
	</tr>
</table>
</form>




<table>
	<tr>
		<td valign="top">

{if $carrier == 1}

 	<!-- 検索条件が全広告でない場合表示 -->
 	{if $advert_id != '0'}

 		<h5>単価</h5>
		<!-- アクション単価 -->
		<table cellpadding="0" cellspacing="0">
			<tr>
				<th id="th_title" width="60">単価</th>
				<th id="th_title" width="60">Docomo</th>
				<th id="th_title" width="60">softbank</th>
				<th id="th_title" width="60">au</th>
				<th id="th_title" width="60">pc</th>
			</tr>


			<tr>

					<td>[成果1]</td>
					<td><!-- docomo -->&yen;{$form_data.action_price_client_docomo_1|htmlspecialchars:$smarty.const.ENT_QUOTES}
					<td><!-- softbank -->&yen;{$form_data.action_price_client_softbank_1|htmlspecialchars:$smarty.const.ENT_QUOTES}
					<td><!-- au -->&yen;{$form_data.action_price_client_au_1|htmlspecialchars:$smarty.const.ENT_QUOTES}
					<td><!-- pc -->&yen;{$form_data.action_price_client_pc_1|htmlspecialchars:$smarty.const.ENT_QUOTES}
			</tr>
			<tr>
					<td>[成果2]
					<td><!-- docomo -->&yen;{$form_data.action_price_client_docomo_2|htmlspecialchars:$smarty.const.ENT_QUOTES}
					<td><!-- softbank -->&yen;{$form_data.action_price_client_softbank_2|htmlspecialchars:$smarty.const.ENT_QUOTES}
					<td><!-- au -->&yen;{$form_data.action_price_client_au_2|htmlspecialchars:$smarty.const.ENT_QUOTES}
					<td><!-- pc -->&yen;{$form_data.action_price_client_pc_2|htmlspecialchars:$smarty.const.ENT_QUOTES}
			</tr>
			<tr>
					<td>[成果3]
					<td><!-- docomo -->&yen;{$form_data.action_price_client_docomo_3|htmlspecialchars:$smarty.const.ENT_QUOTES}
					<td><!-- softbank -->&yen;{$form_data.action_price_client_softbank_3|htmlspecialchars:$smarty.const.ENT_QUOTES}
					<td><!-- au -->&yen;{$form_data.action_price_client_au_3|htmlspecialchars:$smarty.const.ENT_QUOTES}
					<td><!-- pc -->&yen;{$form_data.action_price_client_pc_3|htmlspecialchars:$smarty.const.ENT_QUOTES}
			</tr>
			<tr>
					<td>[成果4]
					<td><!-- docomo -->&yen;{$form_data.action_price_client_docomo_4|htmlspecialchars:$smarty.const.ENT_QUOTES}
					<td><!-- softbank -->&yen;{$form_data.action_price_client_softbank_4|htmlspecialchars:$smarty.const.ENT_QUOTES}
					<td><!-- au -->&yen;{$form_data.action_price_client_au_4|htmlspecialchars:$smarty.const.ENT_QUOTES}
					<td><!-- pc -->&yen;{$form_data.action_price_client_pc_4|htmlspecialchars:$smarty.const.ENT_QUOTES}
			</tr>
			<tr>
					<td>[成果5]
					<td><!-- docomo -->&yen;{$form_data.action_price_client_docomo_5|htmlspecialchars:$smarty.const.ENT_QUOTES}
					<td><!-- softbank -->&yen;{$form_data.action_price_client_softbank_5|htmlspecialchars:$smarty.const.ENT_QUOTES}
					<td><!-- au -->&yen;{$form_data.action_price_client_au_5|htmlspecialchars:$smarty.const.ENT_QUOTES}
					<td><!-- pc -->&yen;{$form_data.action_price_client_pc_5|htmlspecialchars:$smarty.const.ENT_QUOTES}

		 	</tr>
		</table>

	{else}
		<h5>全広告のキャリア合計集計です</h5>
	{/if}
<!--
		</td>
		<td>
 -->
 		{if $advert_id != '0'}
 			<h5>集計内容</h5>
 		{/if}
		<table cellpadding="0" cellspacing="0">

			<tr>
				<th rowspan="2" id="th_title">年月</th>
				<th colspan="2" id="th_title">クリック報酬</th>
				<th colspan="2" id="th_title">アフィリエイト報酬</th>
				<th rowspan="2" id="th_title">合計</th>
			</tr>
			<tr>
				<th id="th_title">クリック数</th>
				<th id="th_title">報酬金額</th>
				<th id="th_title">アクション数</th>
				<th id="th_title">報酬金額</th>
			</tr>
		{foreach from=$summary key="key" item="data" name="summary"}
			<tr>
				<td>{$data.summary_date}</td>
				<td>{$data.click_count|number_format}</td>
				<td>&yen;{$data.click_price|number_format}</td>
				<td>{$data.action_count|number_format}</td>
				<td>&yen;{$data.action_price|number_format}</td>
				<td>&yen;{$data.total_price|number_format}</td>
			</tr>
		{/foreach}
			<tr>
				<td>合計</td>
				<td>{$all.click_count|number_format}</td>
				<td>&yen;{$all.click_price|number_format}</td>
				<td>{$all.action_count|number_format}</td>
				<td>&yen;{$all.action_price|number_format}</td>
				<td>&yen;{$all.total_price|number_format}</td>
			</tr>
		</table>

		</td>

{elseif $carrier == 2}

 	<!-- 検索条件が全広告でない場合表示 -->
 	{if $advert_id != '0'}

 		<h5>単価</h5>
		<!-- アクション単価 -->
		<table cellpadding="0" cellspacing="0" align="left">
			<tr>
				<th id="th_title" width="60"></th>
				<th id="th_title" width="60">Docomo</th>
				<th id="th_title" width="60">softbank</th>
				<th id="th_title" width="60">au</th>
				<th id="th_title" width="60">pc</th>
			</tr>

			<tr>

					<td>[成果1]</td>
					<td><!-- docomo -->&yen;{$form_data.action_price_client_docomo_1|htmlspecialchars:$smarty.const.ENT_QUOTES}
					<td><!-- softbank -->&yen;{$form_data.action_price_client_softbank_1|htmlspecialchars:$smarty.const.ENT_QUOTES}
					<td><!-- au -->&yen;{$form_data.action_price_client_au_1|htmlspecialchars:$smarty.const.ENT_QUOTES}
					<td><!-- pc -->&yen;{$form_data.action_price_client_pc_1|htmlspecialchars:$smarty.const.ENT_QUOTES}
			</tr>
			<tr>
					<td>[成果2]
					<td><!-- docomo -->&yen;{$form_data.action_price_client_docomo_2|htmlspecialchars:$smarty.const.ENT_QUOTES}
					<td><!-- softbank -->&yen;{$form_data.action_price_client_softbank_2|htmlspecialchars:$smarty.const.ENT_QUOTES}
					<td><!-- au -->&yen;{$form_data.action_price_client_au_2|htmlspecialchars:$smarty.const.ENT_QUOTES}
					<td><!-- pc -->&yen;{$form_data.action_price_client_pc_2|htmlspecialchars:$smarty.const.ENT_QUOTES}
			</tr>
			<tr>
					<td>[成果3]
					<td><!-- docomo -->&yen;{$form_data.action_price_client_docomo_3|htmlspecialchars:$smarty.const.ENT_QUOTES}
					<td><!-- softbank -->&yen;{$form_data.action_price_client_softbank_3|htmlspecialchars:$smarty.const.ENT_QUOTES}
					<td><!-- au -->&yen;{$form_data.action_price_client_au_3|htmlspecialchars:$smarty.const.ENT_QUOTES}
					<td><!-- pc -->&yen;{$form_data.action_price_client_pc_3|htmlspecialchars:$smarty.const.ENT_QUOTES}
			</tr>
			<tr>
					<td>[成果4]
					<td><!-- docomo -->&yen;{$form_data.action_price_client_docomo_4|htmlspecialchars:$smarty.const.ENT_QUOTES}
					<td><!-- softbank -->&yen;{$form_data.action_price_client_softbank_4|htmlspecialchars:$smarty.const.ENT_QUOTES}
					<td><!-- au -->&yen;{$form_data.action_price_client_au_4|htmlspecialchars:$smarty.const.ENT_QUOTES}
					<td><!-- pc -->&yen;{$form_data.action_price_client_pc_4|htmlspecialchars:$smarty.const.ENT_QUOTES}
			</tr>
			<tr>
					<td>[成果5]
					<td><!-- docomo -->&yen;{$form_data.action_price_client_docomo_5|htmlspecialchars:$smarty.const.ENT_QUOTES}
					<td><!-- softbank -->&yen;{$form_data.action_price_client_softbank_5|htmlspecialchars:$smarty.const.ENT_QUOTES}
					<td><!-- au -->&yen;{$form_data.action_price_client_au_5|htmlspecialchars:$smarty.const.ENT_QUOTES}
					<td><!-- pc -->&yen;{$form_data.action_price_client_pc_5|htmlspecialchars:$smarty.const.ENT_QUOTES}

		 	</tr>
		</table>
	{else}
		<h5>全広告のキャリア別集計です</h5>
	{/if}

		{if $advert_id != '0'}
			<h5>集計内容</h5>
		{/if}
		<table cellpadding="0" cellspacing="0">
			<tr>
				<th colspan="2" id="th_title"></th>
				<th colspan="2" id="th_title">クリック報酬</th>
				<th colspan="2" id="th_title">アフィリエイト報酬</th>
				<th rowspan="2" id="th_title">合計</th>
			</tr>
			<tr>
				<th id="th_title">年月</th>
				<th id="th_title">キャリア</th>
				<th id="th_title">クリック数</th>
				<th id="th_title">報酬金額</th>
				<th id="th_title">アクション数</th>
				<th id="th_title">報酬金額</th>
			</tr>
		{foreach from=$summary key="key" item="data" name="summary"}
			<tr>
				<td rowspan="4">{$data.summary_date}</td>
				<td>docomo</td>
				<td>{$data.docomo.click_count|number_format}</td>
				<td>&yen;{$data.docomo.click_price|number_format}</td>
				<td>{$data.docomo.action_count|number_format}</td>
				<td>&yen;{$data.docomo.action_price|number_format}</td>
				<td>&yen;{$data.docomo.total_price|number_format}</td>
			</tr>
			<tr>
				<td>softbank</td>
				<td>{$data.softbank.click_count|number_format}</td>
				<td>&yen;{$data.softbank.click_price|number_format}</td>
				<td>{$data.softbank.action_count|number_format}</td>
				<td>&yen;{$data.softbank.action_price|number_format}</td>
				<td>&yen;{$data.softbank.total_price|number_format}</td>
			</tr>
			<tr>
				<td>au</td>
				<td>{$data.au.click_count|number_format}</td>
				<td>&yen;{$data.au.click_price|number_format}</td>
				<td>{$data.au.action_count|number_format}</td>
				<td>&yen;{$data.au.action_price|number_format}</td>
				<td>&yen;{$data.au.total_price|number_format}</td>
			</tr>
			<tr>
				<td>pc</td>
				<td>{$data.pc.click_count|number_format}</td>
				<td>&yen;{$data.pc.click_price|number_format}</td>
				<td>{$data.pc.action_count|number_format}</td>
				<td>&yen;{$data.pc.action_price|number_format}</td>
				<td>&yen;{$data.pc.total_price|number_format}</td>
			</tr>
		{/foreach}
			<tr>
				<td colspan="2" id="th_title">合計</td>
				<td>{$all.click_count|number_format}</td>
				<td>&yen;{$all.click_price|number_format}</td>
				<td>{$all.action_count|number_format}</td>
				<td>&yen;{$all.action_price|number_format}</td>
				<td>&yen;{$all.total_price|number_format}</td>
			</tr>
		</table>

		</td>

<!-- carrier == '1' carrier == '2' のendif -->
{/if}

		</td>
	</tr>
</table>


<!-- advert_id == '' のendif -->
{/if}


</div><!-- contents -->

<!-- フッター -->
{include file='./hooter2.tpl'}