{include file='./header2.tpl' page_title='クライアント管理'}

<!-- Menu -->
{include file='./client_menu.tpl'}

<div id="my_contents">

<h2>広告設定</h2>

<div id=message>
{if $error_message != '' }
<div id="error_message">
	<h3>ERROR:{$error_message}</h3>
</div>
{/if}
{if $info_message != '' }
<div id="info_message">
	<h3>INFO:{$info_message}</h3>
</div>
{/if}
</div><!-- message -->

<form method="POST" action="{$smarty.server.PHP_SELF}">
<table cellpadding="0" cellspacing="0">
	<tr>
		<th colspan="2" id="th_title">広告選択</th>
		<td>
			<select name="advert_id">
			{foreach from=$advert_array item="data" name="advert_list"}
				<option value="{$data.id}"{if $advert_id == $data.id} selected="selected"{/if}>{$data.advert_name|htmlspecialchars:$smarty.const.ENT_QUOTES}</option>
			{/foreach}
			</select>
			<input type="submit" name="submit" value="選択" />
		</td>
	</tr>
</table>
</form>
{if $advert_id != ""}
<table cellpadding="0" cellspacing="0">
	<tr>
		<th colspan="2" id="th_title">サイトURL(docomo)</th>
		<td>
			<input type="text" size="100" name="site_url_docomo" value="{$form_data.site_url_docomo|htmlspecialchars:$smarty.const.ENT_QUOTES}" readonly="readonly" />
		</td>
	</tr>
	<tr>
		<th colspan="2" id="th_title">サイトURL(softbank)</th>
		<td>
			<input type="text" size="100" name="site_url_softbank" value="{$form_data.site_url_softbank|htmlspecialchars:$smarty.const.ENT_QUOTES}" readonly="readonly" />
		</td>
	</tr>
	<tr>
		<th colspan="2" id="th_title">サイトURL(au)</th>
		<td>
			<input type="text" size="100" name="site_url_au" value="{$form_data.site_url_au|htmlspecialchars:$smarty.const.ENT_QUOTES}" readonly="readonly" />
		</td>
	</tr>
	<tr>
		<th colspan="2" id="th_title">サイトURL(pc)</th>
		<td>
			<input type="text" size="100" name="site_url_pc" value="{$form_data.site_url_pc|htmlspecialchars:$smarty.const.ENT_QUOTES}" readonly="readonly" />
		</td>
	</tr>
	<tr>
		<th colspan="2" id="th_title">クリック単価</th>
		<td>
			<input type="text" size="10" name="click_price_client" value="{$form_data.click_price_client|htmlspecialchars:$smarty.const.ENT_QUOTES}" readonly="readonly" />円
		</td>
	</tr>
	<tr>
		<th colspan="2" id="th_title">アクション単価(金額)</th>
		<td>
			[成果1]
			docomo:<input type="text" size="10" name="action_price_client_docomo_1" value="{$form_data.action_price_client_docomo_1|htmlspecialchars:$smarty.const.ENT_QUOTES}" readonly="readonly" />円
			softbank:<input type="text" size="10" name="action_price_client_softbank_1" value="{$form_data.action_price_client_softbank_1|htmlspecialchars:$smarty.const.ENT_QUOTES}" readonly="readonly" />円
			au:<input type="text" size="10" name="action_price_client_au_1" value="{$form_data.action_price_client_au_1|htmlspecialchars:$smarty.const.ENT_QUOTES}" readonly="readonly" />円
			pc:<input type="text" size="10" name="action_price_client_pc_1" value="{$form_data.action_price_client_pc_1|htmlspecialchars:$smarty.const.ENT_QUOTES}" readonly="readonly" />円<br />
			[成果2]
			docomo:<input type="text" size="10" name="action_price_client_docomo_2" value="{$form_data.action_price_client_docomo_2|htmlspecialchars:$smarty.const.ENT_QUOTES}" readonly="readonly" />円
			softbank:<input type="text" size="10" name="action_price_client_softbank_2" value="{$form_data.action_price_client_softbank_2|htmlspecialchars:$smarty.const.ENT_QUOTES}" readonly="readonly" />円
			au:<input type="text" size="10" name="action_price_client_au_2" value="{$form_data.action_price_client_au_2|htmlspecialchars:$smarty.const.ENT_QUOTES}" readonly="readonly" />円
			pc:<input type="text" size="10" name="action_price_client_pc_2" value="{$form_data.action_price_client_pc_2|htmlspecialchars:$smarty.const.ENT_QUOTES}" readonly="readonly" />円<br />
			[成果3]
			docomo:<input type="text" size="10" name="action_price_client_docomo_3" value="{$form_data.action_price_client_docomo_3|htmlspecialchars:$smarty.const.ENT_QUOTES}" readonly="readonly" />円
			softbank:<input type="text" size="10" name="action_price_client_softbank_3" value="{$form_data.action_price_client_softbank_3|htmlspecialchars:$smarty.const.ENT_QUOTES}" readonly="readonly" />円
			au:<input type="text" size="10" name="action_price_client_au_3" value="{$form_data.action_price_client_au_3|htmlspecialchars:$smarty.const.ENT_QUOTES}" readonly="readonly" />円
			pc:<input type="text" size="10" name="action_price_client_pc_3" value="{$form_data.action_price_client_pc_3|htmlspecialchars:$smarty.const.ENT_QUOTES}" readonly="readonly" />円<br />
			[成果4]
			docomo:<input type="text" size="10" name="action_price_client_docomo_4" value="{$form_data.action_price_client_docomo_4|htmlspecialchars:$smarty.const.ENT_QUOTES}" readonly="readonly" />円
			softbank:<input type="text" size="10" name="action_price_client_softbank_4" value="{$form_data.action_price_client_softbank_4|htmlspecialchars:$smarty.const.ENT_QUOTES}" readonly="readonly" />円
			au:<input type="text" size="10" name="action_price_client_au_4" value="{$form_data.action_price_client_au_4|htmlspecialchars:$smarty.const.ENT_QUOTES}" readonly="readonly" />円
			pc:<input type="text" size="10" name="action_price_client_pc_4" value="{$form_data.action_price_client_pc_4|htmlspecialchars:$smarty.const.ENT_QUOTES}" readonly="readonly" />円<br />
			[成果5]
			docomo:<input type="text" size="10" name="action_price_client_docomo_5" value="{$form_data.action_price_client_docomo_5|htmlspecialchars:$smarty.const.ENT_QUOTES}" readonly="readonly" />円
			softbank:<input type="text" size="10" name="action_price_client_softbank_5" value="{$form_data.action_price_client_softbank_5|htmlspecialchars:$smarty.const.ENT_QUOTES}" readonly="readonly" />円
			au:<input type="text" size="10" name="action_price_client_au_5" value="{$form_data.action_price_client_au_5|htmlspecialchars:$smarty.const.ENT_QUOTES}" readonly="readonly" />円
			pc:<input type="text" size="10" name="action_price_client_pc_5" value="{$form_data.action_price_client_pc_5|htmlspecialchars:$smarty.const.ENT_QUOTES}" readonly="readonly" />円<br />
		</td>
	</tr>
	<tr>
		<th colspan="2" id="th_title">広告概要</th>
		<td>
			<textarea cols="70" rows="5" name="site_outline" readonly="readonly">{$form_data.site_outline|htmlspecialchars:$smarty.const.ENT_QUOTES}</textarea>
		</td>
	</tr>
</table>

<form method="POST" action="{$smarty.server.PHP_SELF}" enctype="multipart/form-data">
<table cellpadding="0" cellspacing="0">
	<tr>
		<th colspan="2" id="th_title">広告原稿(テキスト1)</th>
		<td>
			<textarea cols="70" rows="2" name="ms_text_1">{$form_data.ms_text_1|htmlspecialchars:$smarty.const.ENT_QUOTES}</textarea>
		</td>
	</tr>
	<tr>
		<th colspan="2" id="th_title">広告原稿(テキスト2)</th>
		<td>
			<textarea cols="70" rows="2" name="ms_text_2">{$form_data.ms_text_2|htmlspecialchars:$smarty.const.ENT_QUOTES}</textarea>
		</td>
	</tr>
	<tr>
		<th colspan="2" id="th_title">広告原稿(テキスト3)</th>
		<td>
			<textarea cols="70" rows="2" name="ms_text_3">{$form_data.ms_text_3|htmlspecialchars:$smarty.const.ENT_QUOTES}</textarea>
		</td>
	</tr>
	<tr>
		<th colspan="2" id="th_title">広告原稿(テキスト4)</th>
		<td>
			<textarea cols="70" rows="2" name="ms_text_4">{$form_data.ms_text_4|htmlspecialchars:$smarty.const.ENT_QUOTES}</textarea>
		</td>
	</tr>
	<tr>
		<th colspan="2" id="th_title">広告原稿(テキスト5)</th>
		<td>
			<textarea cols="70" rows="2" name="ms_text_5">{$form_data.ms_text_5|htmlspecialchars:$smarty.const.ENT_QUOTES}</textarea>
		</td>
	</tr>
	<tr>
		<th colspan="2" id="th_title">広告原稿(イメージ1)</th>
		<td>
{if $ms_image_path_1 != ""}
			<img src="{$ms_image_path_1}" alt="イメージ1" /><br />
{/if}
			<input type="radio" name="ms_image_type_1" value="1"{if $form_data.ms_image_type_1 == 1 || $form_data.ms_image_type_1 == ""} checked="checked"{/if} /><label>アップロード</label><input type="file" size="30" name="ms_image_file_1" /><br/>
			<input type="radio" name="ms_image_type_1" value="2"{if $form_data.ms_image_type_1 == 2} checked="checked"{/if} /><label>URL指定</label><input type="text" size="30" name="ms_image_url_1" value="{$form_data.ms_image_url_1|htmlspecialchars:$smarty.const.ENT_QUOTES}" />
		</td>
	</tr>
	<tr>
		<th colspan="2" id="th_title">広告原稿(イメージ2)</th>
		<td>
{if $ms_image_path_2 != ""}
			<img src="{$ms_image_path_2}" alt="イメージ2" /><br />
{/if}
			<input type="radio" name="ms_image_type_2" value="1"{if $form_data.ms_image_type_2 == 1 || $form_data.ms_image_type_2 == ""} checked="checked"{/if} /><label>アップロード</label><input type="file" size="30" name="ms_image_file_2" /><br/>
			<input type="radio" name="ms_image_type_2" value="2"{if $form_data.ms_image_type_2 == 2} checked="checked"{/if} /><label>URL指定</label><input type="text" size="30" name="ms_image_url_2" value="{$form_data.ms_image_url_2|htmlspecialchars:$smarty.const.ENT_QUOTES}" />
		</td>
	</tr>
	<tr>
		<th colspan="2" id="th_title">広告原稿(イメージ3)</th>
		<td>
{if $ms_image_path_3 != ""}
			<img src="{$ms_image_path_3}" alt="イメージ3" /><br />
{/if}
			<input type="radio" name="ms_image_type_3" value="1"{if $form_data.ms_image_type_3 == 1 || $form_data.ms_image_type_3 == ""} checked="checked"{/if} /><label>アップロード</label><input type="file" size="30" name="ms_image_file_3" /><br/>
			<input type="radio" name="ms_image_type_3" value="2"{if $form_data.ms_image_type_3 == 2} checked="checked"{/if} /><label>URL指定</label><input type="text" size="30" name="ms_image_url_3" value="{$form_data.ms_image_url_3|htmlspecialchars:$smarty.const.ENT_QUOTES}" />
		</td>
	</tr>
	<tr>
		<th colspan="2" id="th_title">広告原稿(イメージ4)</th>
		<td>
{if $ms_image_path_4 != ""}
			<img src="{$ms_image_path_4}" alt="イメージ4" /><br />
{/if}
			<input type="radio" name="ms_image_type_4" value="1"{if $form_data.ms_image_type_4 == 1 || $form_data.ms_image_type_4 == ""} checked="checked"{/if} /><label>アップロード</label><input type="file" size="30" name="ms_image_file_4" /><br/>
			<input type="radio" name="ms_image_type_4" value="2"{if $form_data.ms_image_type_4 == 2} checked="checked"{/if} /><label>URL指定</label><input type="text" size="30" name="ms_image_url_4" value="{$form_data.ms_image_url_4|htmlspecialchars:$smarty.const.ENT_QUOTES}" />
		</td>
	</tr>
	<tr>
		<th colspan="2" id="th_title">広告原稿(イメージ5)</th>
		<td>
{if $ms_image_path_5 != ""}
			<img src="{$ms_image_path_5}" alt="イメージ5" /><br />
{/if}
			<input type="radio" name="ms_image_type_5" value="1"{if $form_data.ms_image_type_5 == 1 || $form_data.ms_image_type_5 == ""} checked="checked"{/if} /><label>アップロード</label><input type="file" size="30" name="ms_image_file_5" /><br/>
			<input type="radio" name="ms_image_type_5" value="2"{if $form_data.ms_image_type_5 == 2} checked="checked"{/if} /><label>URL指定</label><input type="text" size="30" name="ms_image_url_5" value="{$form_data.ms_image_url_5|htmlspecialchars:$smarty.const.ENT_QUOTES}" />
		</td>
	</tr>
	<tr>
		<th colspan="2" id="th_title">広告原稿(メール1)</th>
		<td>
			<textarea cols="70" rows="2" name="ms_email_1">{$form_data.ms_email_1|htmlspecialchars:$smarty.const.ENT_QUOTES}</textarea>
		</td>
	</tr>
	<tr>
		<th colspan="2" id="th_title">広告原稿(メール2)</th>
		<td>
			<textarea cols="70" rows="2" name="ms_email_2">{$form_data.ms_email_2|htmlspecialchars:$smarty.const.ENT_QUOTES}</textarea>
		</td>
	</tr>
	<tr>
		<th colspan="2" id="th_title">広告原稿(メール3)</th>
		<td>
			<textarea cols="70" rows="2" name="ms_email_3">{$form_data.ms_email_3|htmlspecialchars:$smarty.const.ENT_QUOTES}</textarea>
		</td>
	</tr>
	<tr>
		<th colspan="2" id="th_title">広告原稿(メール4)</th>
		<td>
			<textarea cols="70" rows="2" name="ms_email_4">{$form_data.ms_email_4|htmlspecialchars:$smarty.const.ENT_QUOTES}</textarea>
		</td>
	</tr>
	<tr>
		<th colspan="2" id="th_title">広告原稿(メール5)</th>
		<td>
			<textarea cols="70" rows="2" name="ms_email_5">{$form_data.ms_email_5|htmlspecialchars:$smarty.const.ENT_QUOTES}</textarea>
		</td>
	</tr>
	<tr>
		<td colspan="3">
			<input type="submit" name="submit" value="登録" />
			<input type="hidden" name="id" value="{$form_data.id}" />
		</td>
	</tr>
</table>
</form>
{/if}
</div><!-- contents -->

<!-- フッター -->
{include file='./hooter2.tpl'}