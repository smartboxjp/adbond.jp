{include file='./header2.tpl' page_title='クライアント管理'}

<!-- Menu -->
{include file='./client_menu.tpl'}

<div id="my_contents">

<h2>退会レポート</h2>

<!-- ---------------------------------------------------------------------------------- -->
<!-- メッセージ表示 -->

<div id=message>
{if $error_message != '' }
<div id="error_message">
	<h3>ERROR:{$error_message}</h3>
</div>
{/if}
{if $info_message != '' }
<div id="info_message">
	<h3>INFO:{$info_message}</h3>
</div>
{/if}
</div><!-- message -->


<!-- ---------------------------------------------------------------------------------- -->
<!-- 検索フォーム -->

<form method="POST" action="{$smarty.server.PHP_SELF}">
<input type="hidden" name="search_flag" value="1" />

<table cellpadding="0" cellspacing="0">
	<tr>
		<th colspan="1" id="th_title">広告選択</th>
		<td>
			<select name="advert_id">
				<option value="{$data.id}"{if $advert_id == '0'} selected="selected"{/if}>全件表示</option>
				{foreach from=$advert_array item="data" name="advert_array"}
					<option value="{$data.id}"{if $advert_id == $data.id} selected="selected"{/if}>{$data.advert_name|htmlspecialchars:$smarty.const.ENT_QUOTES}</option>
				{/foreach}
			</select>
		</td>
	</tr>
	<tr>
		<td id="th_title">日付選択</td>
		<td>
			<select name="set_year">
				{section name=cnt start=2009 loop=2021}
								<option value="{$smarty.section.cnt.index}"{if $search_year == $smarty.section.cnt.index} selected="selected"{/if}>{$smarty.section.cnt.index}年</option>
				{/section}
			</select>
			<select name="set_month">
				{section name=cnt start=1 loop=13}
								<option value="{$smarty.section.cnt.index}"{if $search_month == $smarty.section.cnt.index} selected="selected"{/if}>{$smarty.section.cnt.index}月</option>
				{/section}
			</select>
			<br />
			<input type="submit" name="submit" value="表示" />
		</td>
	</tr>
</table>
</form>

{literal}
	<SCRIPT language="JavaScript">
	<!--
		// ツリーメニュー
		flag = false;
		function treeMenu(tName) {
		  tMenu = document.all[tName].style;
		  if(tMenu.display == 'none') tMenu.display = "block";
		  else tMenu.display = "none";
		}
	//-->
	</SCRIPT>
{/literal}

<table cellpadding="0" cellspacing="0">
	<thead>
	<tr>
		<th id="th_title"><a href="javaScript:treeMenu('treeMenu1')" style="color:#ffffff;">ヘルプ</a></th>
	</tr>
	</thead>

	<tbody id="treeMenu1" style="display:none">
	<tr>
		<td width="400px">
			<b>◆広告一覧表示</b>
			<br />
			検索フォームの<span style="color:#ff0000;">広告選択で全体表示を選択した場合</span>、日付選択の年月を基に<span style="color:#ff0000;">広告のアクション数・退会数・退会率の一覧が表示</span>されます。
			<br />
			<br />

			<b>◆広告日別表示</b>
			<br />
			検索フォームの<span style="color:#ff0000;">広告選択で個別に広告を選択、または広告一覧表示の広告名をクリック</span>した場合、日付選択の年月を基に<span style="color:#ff0000;">各広告の日別のアクション数・退会数が表示</span>されます。
		</td>
	</tr>
	</tbody>

</table>


<!-- ---------------------------------------------------------------------------------- -->
<!-- 退会表示 広告別 -->

{if  $advert_id == '0'}

<table cellpadding="0" cellspacing="0">
	<tr>
		<th colspan="4" id="th_title">{$search_year}年{$search_month}月 広告一覧表示</th>
	</tr>
	<tr>
		<th rowspan="1" id="th_title">広告名</th>
		<th rowspan="1" id="th_title">アクション数</th>
		<th colspan="1" id="th_title">退会数</th>
		<th colspan="1" id="th_title">退会率</th>
	</tr>

{foreach from=$advert_array item="data" name="advert_array"}

	<tr>
		<td>
			<a href="{$smarty.server.PHP_SELF}?advert_id={$data.id}&set_year={$search_year}&set_month={$search_month}&search_flag=1">
				{$data.advert_name|htmlspecialchars:$smarty.const.ENT_QUOTES}
			</a>
		</td>
		<td width="100px">{$data.action_couunt|number_format}</td>
		<td width="100px">{$data.withdrawal_couunt|number_format}</td>
		<td width="100px">{$data.percent|number_format}%</td>
	</tr>

{/foreach}
</table>


<!-- ---------------------------------------------------------------------------------- -->
<!-- 退会表示 日別 -->

{else}

<p style="font-size:12px;margin-bottom:5px;">
	<a href="{$smarty.server.PHP_SELF}?advert_id=0&set_year={$search_year}&set_month={$search_month}&search_flag=1">
		全件表示に戻る
	</a>
</p>

<table cellpadding="0" cellspacing="0">
	<tr>
		<th colspan="3" id="th_title">{$search_year}年{$search_month}月 日別表示</th>
	</tr>
	<tr>
		<th rowspan="1" id="th_title">日付</th>
		<th rowspan="1" id="th_title">アクション数</th>
		<th colspan="1" id="th_title">退会数</th>
	</tr>

{foreach from=$advert_day_array item="data" name="advert_day_array"}

	<tr>
		<td width="100px">{$data.date|date_format:"%Y年%m月%d日"}</td>
		{if $data.action_id_count == 0}
			<td width="100px">{$data.action_id_count|number_format}</td>
		{else}
			<td width="100px" style="background-color:#ccffff;">{$data.action_id_count|number_format}</td>
		{/if}

		{if $data.withdrawal_id_count == 0}
			<td width="100px">{$data.withdrawal_id_count|number_format}</td>
		{else}
			<td width="100px" style="background-color:#ffccff;">{$data.withdrawal_id_count|number_format}</td>
		{/if}
	</tr>

{/foreach}
</table>

<table cellpadding="0" cellspacing="0">
	<tr>
		<th rowspan="1" id="th_title"></th>
		<th rowspan="1" id="th_title">アクション数</th>
		<th colspan="1" id="th_title">退会数</th>
		<th colspan="1" id="th_title">退会率</th>
	</tr>
	<tr>
		<td id="th_title" width="100px">合計</td>
		<td width="100px">{$advert_day_array.0.action_id_count_total|number_format}</td>
		<td width="100px">{$advert_day_array.0.withdrawal_id_count_total|number_format}</td>
		<td width="100px">{$advert_day_array.0.percent|number_format}%</td>
	</tr>
</table>

{/if}

</div><!-- contents -->

<!-- フッター -->
{include file='./hooter2.tpl'}