{include file='./header_test.tpl' page_title='メディア管理'}

<!-- Menu -->
{include file='./media_menu_test.tpl' user_name=$user_name}

テスト

<div id="my_contents">

<h2>広告検索</h2>

<div id=message>
{if $error_message != '' }
<div id="error_message">
	<h3>ERROR:{$error_message}</h3>
</div>
{/if}
{if $info_message != '' }
<div id="info_message">
	<h3>INFO:{$info_message}</h3>
</div>
{/if}
</div><!-- message -->

<form method="POST" action="{$smarty.server.PHP_SELF}">
<input type="hidden" name="media_category_id" value="{$media_category_id}" />
<table cellpadding="0" cellspacing="0">
	<tr>
		<th colspan="2" id="th_title">メディア選択</th>
		<td>
			<select name="media_id">
			{foreach from=$media_array item="data" name="media_list"}
				<option value="{$data.id}"{if $media_id == $data.id} selected="selected"{/if}>{$data.media_name|htmlspecialchars:$smarty.const.ENT_QUOTES}</option>
			{/foreach}
			</select>
			<input type="submit" name="submit" value="選択" />
		</td>
	</tr>
</table>
{if $media_id != "" && $media_status == '2'}
<table cellpadding="0" cellspacing="0">
	<tr>
		<td id="th_title">キーワード</td>
		<td><input type="text" size="50" name="keyword" value="{$search.keyword}" /></td>
	</tr>
	<tr>
		<td id="th_title">カテゴリー</td>
		<td>
{foreach from=$advert_category_array item="data" name="advert_category_list"}
			<input type="checkbox" name="category[{$data.id}]" value="1"{if $search.category[$data.id] == 1} checked="checked"{/if} /><label>{$data.name}</label>
{/foreach}
		</td>
	</tr>
	<tr>
		<td id="th_title">対応キャリア</td>
		<td>
			<input type="checkbox" name="support_docomo" value="1"{if $search.support_docomo == 1} checked="checked"{/if} /><label>docomo</label>
			<input type="checkbox" name="support_softbank" value="1"{if $search.support_softbank == 1} checked="checked"{/if} /><label>softbank</label>
			<input type="checkbox" name="support_au" value="1"{if $search.support_au == 1} checked="checked"{/if} /><label>au</label>
			<input type="checkbox" name="support_pc" value="1"{if $search.support_pc == 1} checked="checked"{/if} /><label>pc</label>
		</td>
	</tr>
	<tr>
		<td id="th_title">単価</td>
		<td>
			<input type="text" size="10" name="min_price" value="{$search.min_price}" />円 ～ <input type="text" size="10" name="max_price" value="{$search.max_price}" />円
		</td>
	</tr>
	<tr>
		<td id="th_title">登録日時</td>
		<td>
			<input type="checkbox" name="advert_date_flag" value="1"{if $search.advert_date_flag == 1} checked="checked"{/if} />
			<select name="s_year">
{section name=cnt start=2009 loop=2021}
				<option value="{$smarty.section.cnt.index}"{if $set_s_year == $smarty.section.cnt.index} selected="selected"{/if}>{$smarty.section.cnt.index}年</option>
{/section}
			</select>
			<select name="s_month">
{section name=cnt start=1 loop=13}
				<option value="{$smarty.section.cnt.index}"{if $set_s_month == $smarty.section.cnt.index} selected="selected"{/if}>{$smarty.section.cnt.index}月</option>
{/section}
			</select>
			<select name="s_day">
{section name=cnt start=1 loop=32}
				<option value="{$smarty.section.cnt.index}"{if $set_s_day == $smarty.section.cnt.index} selected="selected"{/if}>{$smarty.section.cnt.index}日</option>
{/section}
			</select> ～
			<select name="e_year">
{section name=cnt start=2009 loop=2021}
				<option value="{$smarty.section.cnt.index}"{if $set_e_year == $smarty.section.cnt.index} selected="selected"{/if}>{$smarty.section.cnt.index}年</option>
{/section}
			</select>
			<select name="e_month">
{section name=cnt start=1 loop=13}
				<option value="{$smarty.section.cnt.index}"{if $set_e_month == $smarty.section.cnt.index} selected="selected"{/if}>{$smarty.section.cnt.index}月</option>
{/section}
			</select>
			<select name="e_day">
{section name=cnt start=1 loop=32}
				<option value="{$smarty.section.cnt.index}"{if $set_e_day == $smarty.section.cnt.index} selected="selected"{/if}>{$smarty.section.cnt.index}日</option>
{/section}
			</select>
		</td>
	</tr>
	<tr>
		<td id="th_title">提携状態</td>
		<td>
			<input type="radio" name="connect_status" value="1"{if $search.connect_status == 1 || $search.connect_status == ""} checked="checked"{/if} /><label>指定なし</label>
			<input type="radio" name="connect_status" value="2"{if $search.connect_status == 2} checked="checked"{/if} /><label>未提携</label>
			<input type="radio" name="connect_status" value="3"{if $search.connect_status == 3} checked="checked"{/if} /><label>提携済み</label>
		</td>
	</tr>
	<tr>
		<td id="th_title">ポイントバック対応</td>
		<td>
			<input type="radio" name="point_back_flag" value="1"{if $search.point_back_flag == 1 || $search.point_back_flag == ""} checked="checked"{/if} /><label>指定なし</label>
			<input type="radio" name="point_back_flag" value="2"{if $search.point_back_flag == 2} checked="checked"{/if} /><label>不可</label>
			<input type="radio" name="point_back_flag" value="3"{if $search.point_back_flag == 3} checked="checked"{/if} /><label>可</label>
		</td>
	</tr>
	<tr>
		<td colspan="3">
			<input type="submit" name="submit" value="検索" />
		</td>
	</tr>
</table>
<table cellpadding="0" cellspacing="0">
	<tr>
		<th colspan="16">
			<input type="submit" name="submit" value="CSVダウンロード" />
		</th>
	</tr>
</table>
</form>
<table cellpadding="0" cellspacing="0">
	<tr>
		<th colspan="16" id="th_title">
			【該当{$list_count}件】
		</th>
	</tr>
	<tr>
		<th rowspan="2" id="th_title">広告名</th>
		<th rowspan="2" id="th_title">クリック単価</th>
		<th colspan="4" id="th_title">アクション単価</th>
		<th colspan="4" id="th_title">対応キャリア</th>
		<th colspan="2" id="th_title">種別</th>
		<th rowspan="2" id="th_title">カテゴリー</th>
		<th rowspan="2" id="th_title">ポイントバック</th>
		<th rowspan="2" id="th_title">出稿終了日</th>
		<th rowspan="2" id="th_title">&nbsp;</th>
	</tr>
	<tr>
		<th id="th_title">docomo</th>
		<th id="th_title">softbank</th>
		<th id="th_title">au</th>
		<th id="th_title">pc</th>
		<th id="th_title">docomo</th>
		<th id="th_title">softbank</th>
		<th id="th_title">au</th>
		<th id="th_title">pc</th>
		<th id="th_title">一般</th>
		<th id="th_title">公式</th>
	</tr>
{foreach from=$list item="data" name="list"}
	<tr>
		<td>{$data.advert_name|htmlspecialchars:$smarty.const.ENT_QUOTES}</td>
		<td>&yen;{$data.click_price_media|number_format}</td>
		<td>&yen;{$data.action_price_media_docomo_1|number_format}</td>
		<td>&yen;{$data.action_price_media_softbank_1|number_format}</td>
		<td>&yen;{$data.action_price_media_au_1|number_format}</td>
		<td>&yen;{$data.action_price_media_pc_1|number_format}</td>
		<td>
{if $data.support_docomo == 1}
			○
{else}
			-
{/if}
		</td>
		<td>
{if $data.support_softbank == 1}
			○
{else}
			-
{/if}
		</td>
		<td>
{if $data.support_au == 1}
			○
{else}
			-
{/if}
		</td>
		<td>
{if $data.support_pc == 1}
			○
{else}
			-
{/if}
		</td>
		<td>
{if $data.content_type == 1}
			○
{else}
			-
{/if}
		</td>
		<td>
{if $data.content_type == 2}
			○
{else}
			-
{/if}
		</td>
		<td>{$data.advert_category_name|htmlspecialchars:$smarty.const.ENT_QUOTES}</td>
		<td>
{if $data.point_back_flag == 2}
			○
{else}
			-
{/if}
		</td>
		<td>
{if $data.unrestraint_flag == 1}
			無制限
{else}
			{$data.advert_end_date|date_format:"%Y-%m-%d"}
{/if}
		</td>
		<td>
			<form method="POST" action="./search_view.php">
			{if $data.connect_flag == 1}
				<input type="submit" value="表示" />
			{else}
				<input type="submit" value="申込" />
			{/if}
				<input type="hidden" name="mode" value="advert_view" />
				<input type="hidden" name="advert_id" value="{$data.id}" />
				<input type="hidden" name="media_id" value="{$media_id}" />
				<input type="hidden" name="connect_flag" value="{$data.connect_flag}" />
			</form>
		</td>
	</tr>
{/foreach}
</table>
{/if}
</div><!-- contents -->

<!-- フッター -->
{include file='./hooter_test.tpl'}