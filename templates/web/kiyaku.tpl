{include file='./header_web.tpl' page_title='利用規約'}
{include file='./menu.tpl'}

<div id="side_contents">
{include file='./side_contents.tpl'}
</div><!-- side_contents -->

<div id="main_contents">
<div id="kiyaku">
<h2><img src="./images/web/h2_kiyaku.gif" width="118" height="31" alt="TOS 利用規約" /></h2>

<h3>利用規約</h3>

<p class="article_headline">第1条《総則について》</p>
<p class="article_text">１．サイトオーナー利用規約（以下「本規約」と記載）は、株式会社 laugh&peacce（以下「当社」と記載）の提供するアフィリエイトプログラム「ADbond!!」（以下「本サービス」と記載）に登録し、広告主の広告を掲載するウェブサイトおよびメールマガジン（以下「パートナーサイト」と記載）の管理者（以下「サイトオーナー」と記載）と当社の関係を定めるものとする。<br />
２．本サービスに登録をするサイトオーナーは、本規約の全条項に同意し、遵守するものとする。</p>

<p class="article_headline">第2条《本サービスについて》</p>
<p class="article_text">１．本サービスは、パートナーサイトに広告主のウェブサイト（以下「広告主サイト」と記載）にリンクしたバナー広告等を掲載し、パートナーサイトにアクセスした者（以下「訪問者」と記載）が当該バナー広告等から広告主サイトに移動して登録等をした場合に、次条に定める広告報酬を当社が広告主に代行してサイトオーナーに支払うものとする。<br />
２．サイトオーナーは当社のウェブサイトで提供される当該サイトオーナー専用の管理ページ（以下「管理ページ」と記載）において、広告主が提供する広告報酬の種類、金額等の情報を確認することができるものとする。.</p>

<p class="article_headline">第3条《広告報酬の種類について》</p>
<p class="article_text">１．広告報酬には以下の種類があり、いずれの成功報酬が受けられるかは、広告主が選択するものとする。<br />
ａ．アフィリエイト（成果報酬）型<br />
パートナーサイト上のバナー広告等をクリックして広告主サイトにアクセスした訪問者が、広告サイトにおいて、商品やサービスを購入又は契約した場合、その代金にもしくは商品購入数を成果とみなし成果に応じてサイトオーナーに報酬が支払われるものとする。<br />
ｂ．クリック保証型<br />
パートナーサイト上のバナー広告等が、訪問者にクリックされた回数に応じてサイトオーナーに報酬が支払われるものとする。ただし、訪問者のアクセスが、広告毎の指定された端末以外からのアクセスと判断した場合はその計算に含まないものとする。</p>

<p class="article_headline">第4条《本サービスへの登録について》</p>
<p class="article_text">１．サイトオーナーになろうとする者は、当社ウェブサイトにおいて、当社の定める申込事項をすべて正確に記載し、登録を申し込まなくてはならないものとする。<br />
２．当社は、前項の申込があった場合には、必要な審査をしたうえで登録の承認をするかどうかを決定し、電子メールによりその結果を通知するものとする。サイトオーナーになろうとする者は、当社からの登録の通知を受けることで、正式にサイトオーナーになるものとする。<br />
３．当社は、サイトオーナーになろうとする者が、以下の事由に該当する場合には登録を承認しないものとする。<br />
ａ．過去に本サービスに登録したことがあり、その登録が抹消されたことがある場合<br />
ｂ．申込時に登録申請した事項に偽りがあった場合。<br />
４．当社は、サイトオーナーになろうとする者の管理するウェブサイト及びメールマガジンが、以下の事由に該当する場合には登録を承認しないものとする。<br />
ａ．著作権、その他の知的所有権を侵害するおそれのある表現・内容を含む場合<br />
ｂ．他人の名誉・プライバシー権・肖像権その他の権利を侵害するおそれのある表現・内容を含む場合<br />
ｃ．ねずみ講、マルチ商法等にかかわる又は類似する場合<br />
ｄ．違法または反社会的な表現・内容を含む場合<br />
ｅ．公序良俗に反する表現・内容を含む場合<br />
ｆ．アクセスのためにID・パスワードを必要とする等、一般に公開されていない場合<br />
ｇ．内容が不明ないし乏しい、概観が異様でありウェブサイト及びメールマガジンの場合<br />
ｈ．本条各項に該当するウェブサイトのリンクがある場合<br />
ｉ．本条各号の事由で当てはまる恐れのある場合<br />
ｊ．その他当社が不適当と認める場合</p>

<p class="article_headline">第5条《広告主との報酬支払いの契約の成立について》</p>
<p class="article_text">１．サイトオーナーは、管理画面において、パートナーサイトにバナー広告等を掲載すること（以下「提携」と記載）を希望する広告主を選択するものとする。<br />
２．提携を希望するに当たり、管理ページに記載された広告報酬の種類・金額その他の提携条件を確認してこれを承認した後、提携の申請を行うものとする。<br />
３．サイトオーナーが提携の申請をし、広告主がこれを承諾した場合、サイトオーナーと広告主の間に直接広告報酬支払契約が成立する。</p>

<p class="article_headline">第6条《広告報酬の支払いについて》</p>
<p class="article_text">１．当社は広告主から集金した広告報酬を、広告主に代行してサイトオーナーに支払うものとする。<br />
２．広告報酬の支払いは、原則として45日または60日毎に行う。また、振り込みが土日・祝日の場合は翌営業日の支払いとする。<br />
３．当社は、毎月の広告報酬を翌月末日までにサイトオーナーがあらかじめ届け出た金融機関の口座（国外銀行口座は除く）に対して振込みにより支払うものとする。<br />
４．広告主から当社に対する広告報酬の支払いが遅延した場合には、当社からサイトオーナーに対する支払いを留保することができるものとする。また、広告主が当社に広告報酬を支払わなかった場合、当社はサイトオーナーに対する支払い義務は無いものとする。但し、上記は成果条件に反する成果の場合のみとする。<br />
５．サイトオーナーが本規約に基づき当社に通知すべき事項を通知せず、あるいは第4条-4.に該当する場合や、第10条に該当する場合、その他本規約に違反する行為を行った場合には、当社はサイトオーナーに対し広告報酬を支払う義務を負わず、またサイトオーナーは広告報酬の返還要求に応じる義務があるものとする。<br />
６．サイトオーナーに対する広告報酬の支払い後において、広告単価やクリック・成果報酬の返還を求める事ができるものとし、サイトオーナーはそれに応じなければならないものとする。尚、返還の期限については原則として当社からの返還依頼後1カ月以内とする。</p>

<p class="article_headline">第7条《本サービスの停止・中断について》</p>
<p class="article_text">１．当社は、本サービスの移動するサーバ、ソフトウェア等の保守点検、修理、補修等を定期的に又は緊急に実施する必要があると認められる場合、その他の必要が認められる場合には、本サービスを一時的に停止出来るものとする。<br />
２．当社は、本サービスの提供を継続することが困難とする事情が生じたと判断した場合、本サービスは中断出来るものとする。<br />
３．サイトオーナーは、当社が提供する本サービスが、本条各項の事由により一定期間停止される場合があることをあらかじめ承諾し、広告報酬が減額した場合でも意義を申し立てず、当社は損害賠償義務を負わないこととする。但し、サーバ不具合などによる、緊急停止の場合はそれに限らないものとする。</p>

<p class="article_headline">第8条《本サービス内容の変更について》</p>
<p class="article_text">１．当社は、サイトオーナーに事前通知の上、本サービスの内容を変更出来るものとする。<br />
２．前項の場合によりサイトオーナーに不利益や損害等が発生しても、当社はその責任を負わないものとする。</p>

<p class="article_headline">第9条《本サービスの義務について》</p>
<p class="article_text">１．サイトオーナーは、パートナーサイトの内容の変更を行ったときは、直ちに当社に通知する義務があるものとする。また、パートナーサイトがアクセスできない状態になった時、またはなりうる可能性がある時も同様とする。<br />
２．サイトオーナーは、パートナーサイトの内容を、第4条‐4.各号に揚げる内容に変更してはならないものとする。<br />
３．サイトオーナーは、パートナーサイトの内容に関して、トラブルが発生した場合には、サイトオーナーが全ての自己責任において円滑、迅速に解決を図り、当社及び広告主には一切の損害、負担、迷惑等をかけないものとする。<br />
４．サイトオーナーは、本サービスの提供を受けることに支障、問題が生じた場合や、その他の問題を発見した場合は、直ちに当社に報告するものとする。サイトオーナーが報告を怠ったことにより、トラブルやその他の問題が生じた場合、サイトオーナーが一切の責任を負うものとする。<br />
５．サイトオーナーは、管理ページを利用するために、当社が発行したＩＤ及びパスワード・その他の情報を厳重に管理するものとし、不正使用ならびに、第三者に漏らしたり使用させたりしてはならないものとする。<br />
６．サイトオーナーは、当社を介さずに、広告主との間で、直接に広告報酬契約を締結・交渉してはならないものとする。パートナーサイトの登録抹消後においても同様とする。但し、以下の者については登録抹消後においても、この限りではないものとする。<br />
ａ．サイトオーナーの紹介により本サービスに参加した広告主<br />
ｂ．本サービス参加中の広告主により紹介されたサイトオーナー<br />
ｃ．純広告でのお取引<br />
７．サイトオーナーは、登録申込時に当社に申し出た事項に変更があった場合は、直ちに当社に通知するものとする。サイトオーナーがこの通知を怠ったことにより発生した争いごとについては、サイトオーナーが責任をもって処理し、当社はその責任を負わないものとする。</p>

<p class="article_headline">第10条《禁止事項について》</p>
<p class="article_text">１．禁止表現<br />
サイトオーナーは、パートナーサイトにおいて、広告報酬を目的として広告等を掲載していることを示す表現、広告主の名誉や顧客との信頼関係を損ねかねない表現、本サービス内容を説明する表現、広告報酬の金額等を記載してはならないものとする。但し、宣伝文句・推薦文を記載するなどして訪問者を広告主サイトに誘導することはこの限りではないものとする。<br />
２．詐欺行為の禁止<br />
サイトオーナーは、自ら又は第三者を通じ、広告を何度もクリックしたり、訪問者を装い不正に広告主サイトにアクセスし、広告等をクリックしたり、広告を不正に多く表示させたり、虚偽のクリックを生成したり、虚偽の注文や登録、デジコン媒体と偽ったカマセページでの掲載など、の行為を行ってはならない。この場合は詐欺行為とみなし法的に対処するとともに全額を減算とする。
<br />
３．広告表示コード及び広告文言改変の禁止<br />
サイトオーナーは、配信する広告表示用のHTMLコード、広告に利用する画像および文言などを当社が認める範囲内以上に改変したり、第三者に改変させてはならないものとする。登録抹消後においても同様とする。また、媒体の内外を問わずに当該広告に影響を及ぼす可能性のあるプログラムやスクリプトを記述したり、そのようなプログラムやスクリプトの記述のあるページへ当該広告ソースコードを掲載してはならない。<br />
４．登録サイト以外での広告表示用タグコード使用の禁止<br />
サイトオーナーは、当社が表記・配信する広告表示用のタグコードを、登録を受けたサイト以外で使用してはならない。登録抹消後においても同様とする。<br />
５．クリック誘導の禁止<br />
広告文言をパートナーサイト内のリンクのように見せかけたり、コンテンツ内に広告文面を意図的に紛れ込ませたりするなど、訪問者にクリックを誘導させるような掲載方法は禁止とする。<br />
６．メールマガジンへの広告掲載について<br />
ａ．メールマガジンへの広告掲載についても、原則として、パートナーサイトの禁止事項を適用するものとする。<br />
ｂ．上号に加え、別途、メールマガジンへ広告する場合は、下記の事項を遵守すること。<br />
・クリック保証型広告の場合、メールマガジンへの広告掲載は、１配信につき広告タグ１つとする。<br />
・アフィリエイト型広告の場合、メールマガジンへの広告掲載の制限はないものとする。<br />
７．スパム行為の禁止<br />
電子メールでのスパム行為、掲示板への書き込み等による宣伝行為、またそれ以外の方法・手段による第三者への迷惑行為に該当する宣伝行為をしてはならない。<br />
８．プログラム・機器によりクリック偽造の禁止<br />
サイトオーナーは、パートナーサイト内のHTMLコードを改変する、またはプログラムやスクリプト、コンピューターに接続する電子機器や、それ以外のあらゆる方法によって不正に広告をクリックしてはならない。</p>

<p class="article_headline">第11条《監視業務について》</p>
<p class="article_text">１．当社は、サイトオーナーが本規約に則り本サービスを利用しているか、また、本規約に反する行為や不正がないかを監視する業務を自己の裁量により行う。当社は、疑わしいと考えるサイトオーナーに対して、サーバのログファイルを提出するよう求める権利を有するものとする。<br />
２．当社は、当該監視業務により、不正行為を行っている、もしくは行っている蓋然性が非常に高いと判断したサイトオーナーに対して、支払いの一部もしくは全部を拒否する権利を有するほか、サイトオーナーとしての登録をサイトオーナーへの事前の通知なくして取り消し、または損害賠償を請求する権利ならびに当該不正行為が悪質な場合は刑事告訴等の処置を講ずる権利を有するものとし、サイトオーナーは、これに対して一切の異議を申し立てないものとする。当該監視業務は、当時の独自の裁量により行われるものであり、いかなる意味においても、当社の義務を構成するものと解釈されない。<br />
３．当社は、本規約、配信条件その他規定を遵守した状態で表示された広告に対して、広告を閲覧した訪問者が、広告に対する興味、関心に基づいて、自発的 に、適切な方法で行ったクリックのみをカウントする。適切か否かの判断は、当社が一方的に行う事ができるものとする。当社は、広告主に対してクリックの質 を保証するため、クリックの発信元を分析し、適切なクリックのみをカウントする極めて厳格なクリックカウントシステムを採用しており、短時間に同一人が 行ったクリック、検索エンジン等のロボットが行ったクリック、サイトが誤った設定をしている広告タグからのクリックおよび携帯電話端末以外からのクリックはカウントしない。サイトオーナーは、当社のシステムによるクリックカウント方法について完全に合意し、その結果について一切の異議を述べない。また当社は、セキュリティ確保その他の理由により、クリックカウント方法について詳細を開示する義務を負わない。<br />
４．サイトオーナーは、当社が、ネットワーク巡回システムおよびサイトチェックその他の方法により、サイトオーナーによる不正行為等の監視を行うことにつき、異議なく承諾するものとする。<br />
５．当社は、不正なクリックを発生させている可能性のある疑わしいパートナーサイトを発見した場合には、事前通告無く直ちに、広告配信の停止、支払いの停止、登録の抹消、もしくは法的措置またはこれらに変わる全ての手段に訴えることができるものとする。不正の有無についての判断は、当社が一方的に行うことが出来るものとし、その説明の義務を負わないものとする。</p>

<p class="article_headline">第12条《報酬の計算について》</p>
<p class="article_text">パートナーサイトは、広告報酬を正当に請求する権利を有する。クリック保証型広告による広告報酬は、毎月、当社で認定された適正なクリック数に対してク リック保証型広告単価を適用して算出する。アフィリエイト型広告による広告報酬は、毎月、当社で認定された適正な成果数に対してアフィリエイト型広告単価を適用して算出する。サイトオーナーはこれらの報酬の計算について異議を申し立てることはできない。なお、契約期間中のパートナーサイトの都合による広告の貼付解除は支払い対象としない。また、サイトオーナーが第１０条に規定する禁止事項を行った場合、登録中のサイトすべての報酬の支払いは、行わないもの とする。</p>

<p class="article_headline">第13条《権利の譲渡禁止について》</p>
<p class="article_text">サイトオーナーは、本規約に定める権利の全部、又は一部を第三者に販売・譲渡・担保・第三者に使用等させてはならないものとする。</p>

<p class="article_headline">第14条《情報の管理について》</p>
<p class="article_text">当社は、本サービスの提供により得た情報を外部に漏らしたりせずプライバシーポリシーを遵守するものとする。</p>

<p class="article_headline">第15条《守秘義務について》</p>
<p class="article_text">サイトオーナーは本サービスに関係して知った、当社又は広告主の技術上、業務上、その他の秘密に属すべき一切の事項を第三者に知らせてはならないものとする。サイトオーナーの登録抹消後においても同様とする。</p>

<p class="article_headline">第16条《著作権について》</p>
<p class="article_text">本サービスにおける全ての著作権、その他関連知的財産権は当社に帰属するものとする。</p>

<p class="article_headline">第17条《サイトオーナーの登録抹消について》</p>
<p class="article_text">１．当社は、以下の事項が生じたときは、催告することなく、オーナーサイトに通知することにより、サイトオーナーの登録を抹消できるものとする。サイトオーナーの登録が抹消された場合には、サイトオーナーと広告主間の広告報酬支払契約も終了するものとする。<br />
ａ．戦争・紛争・災害やその他の異常事態により本サービスの提供継続が困難と当社が判断した場合<br />
ｂ．サイトオーナーが登録申込時に申請した事項に、虚偽の事実があったことが判明した場合<br />
ｃ．サイトの内容が第4条-4.各号に掲げるものに該当する場合<br />
ｄ．当社に対して通知すべき事項の通知を怠った場合<br />
ｅ．当社からサイトオーナーに対し連絡が取れなくなった場合<br />
ｆ．サイトオーナーが本規約に違反した場合<br />
ｇ．その他、サイトオーナーに不実又は不振行為があり、契約を継続しがたいと認められる場合<br />
ｈ．極端に広告効果が悪いと当社または広告主が判断した場合<br />
ｉ．本条各項に当てはまる恐れのある場合<br />
２．前項の規定により、登録抹消された場合、第1号に規定する場合を除き、サイトオーナーに対する広告報酬は支払わないものとする。</p>

<p class="article_headline">第18条《広告報酬支払契約の終了について》</p>
<p class="article_text">１．当社と広告主との間の契約が解除された場合は、サイトオーナーと広告主の間の広告報酬支払契約は、同時に終了するものとする。この場合、当社は速やかにサイトオーナーに通知するものとし、契約終了後の広告報酬は支払わないものとする。<br />
２．広告主がサイトオーナーとの提携を解消する旨を申し出た場合、当社が速やかにサイトオーナーに通知するものとし、その通知により広告報酬支払契約が終了するものとする。尚、その場合の理由が、サイトオーナーの本条の義務違反の場合、又はその可能性のある場合は、当社は広告報酬を支払わないものとする。</p>

<p class="article_headline">第19条《サイトオーナーによる退会について》</p>
<p class="article_text">１．サイトオーナーは、いつでも本サービスの登録抹消を申し出ることが出来る。<br />
２．サイトオーナーから登録抹消の申し出があり、当社及び広告掲載中の広告主がこれを承諾し、サイトオーナーにその旨を通知したときに登録は抹消され、広告主との間の広告報酬支払契約も終了するものとする。</p>

<p class="article_headline">第20条《契約の途中解消》</p>
<p class="article_text">パートナーサイトの、あるいは共謀者による不正クリック行為、不正登録行為が判明した場合は、契約途中であっても契約を解消する場合がある。上記の場合、当社は広告報酬を支払わないものとする。</p>

<p class="article_headline">第21条《未払広告報酬の扱いについて》</p>
<p class="article_text">１．第3条の規定より、サイトオーナーと広告主の間の広告報酬支払契約が終了した場合において、未払いの広告報酬の金額が当社の定める最低支払額に満たない場合、サイトオーナーはその請求権を放棄し、その支払いを受けないことに同意するものとする。<br />
２．当社がサイトオーナーの口座に報酬を振り込もうとしたときに、何らかの理由により振込み不可能な場合はその理由を問わず、サイトオーナーが広告報酬支払請求を放棄したものとみなす。</p>

<p class="article_headline">第22条《損害の免責について》</p>
<p class="article_text">当社は、サイトオープンが本サービスに関して被った損害について、その原因・因果関係の如何を問わず、賠償責任を負う義務が無いものとする。</p>

<p class="article_headline">第24条《本契約の効力について》</p>
<p class="article_text">本規約は、サイトオーナーが本規約に同意した登録を申請した日から効力を有するものとする。</p>

<p class="article_headline">第25条《準拠法について》</p>
<p class="article_text">本規約は、日本法を準拠法とし、日本法によって解釈されるものとする。</p>

<p class="article_headline">第26条《合意管轄裁判所について》</p>
<p class="article_text">当社とサイトオーナーの間に本規約に関して争いが生じ訴訟を提起する場合、東京地方裁判所を第一審の合意管轄裁判所とする。</p>

<p class="article_headline">第27条《規約の変更及び条件の改訂について》</p>
<p class="article_text">本規約及び広告主との契約条件は随時変更される事を条件とし、変更内容を通知した後において登録の抹消を申し出なかったサイトオーナーは、変更後の規約を承認したものとする。</p>

<p class="article_headline">第28条《協議事項について》</p>
<p class="article_text">本規約に定めのない事項について疑義が生じる、又は本規約の各条各項の解釈に疑義が生じた場合、当社及びサイトオーナーはお互いに誠意をもって協議し、これを解決するものとする。</p>


<h3>利用規約（広告主用）</h3>

<p class="article_headline">第1条《総則について》</p>
<p class="article_text">広告主利用規約（以下「本規約」と記載）は、株式会社ＴＳＣ（以下「当社」と記載）の提供するアフィリエイトプログラム「ADbond!!」（以下「本サービス」と記載）において、広告配信を依頼する広告主（以下「広告主」と記載）並びに広告主より依頼された広告代理店（以下「代理店」と記載）と当社との関係を定めるものとする。本サービスにおいて当社と契約上にある広告主及び代理店は、本規約の全条項に同意し遵守するものとする。</p>

<p class="article_headline">第2条《本サービスについて》</p>
<p class="article_text">１．本サービスは、広告を掲載するサイト（以下「パートナーサイト」と掲載）に広告主のウェブサイトにリンクしたテキスト・バナー広告等を掲載し、パートナーサイトにアクセスした者が当該広告等をクリックした場合等に、広告報酬を当社が広告主に代行してパートナーサイトの管理者に支払うものとする。<br />
２．広告主及び代理店は、当社のウェブサイトで提供される当該広告主専用の管理ページにおいて広告主が配信依頼した広告のクリック数や成果数等の効果情報を確認することが出来る。</p>

<p class="article_headline">第3条《広告配信の申し込みについて》</p>
<p class="article_text">１．広告配信を希望する広告主及び代理店は、当社担当者と詳細を協議の上、申し込むものとする。<br />
２．当社が広告配信希望者の申し込み内容を承諾した時点から本規約が適用されるものとする。<br />
３．広告主と当社との協議の上、当社が配信を承諾した時点で、契約の成立とする。</p>

<p class="article_headline">第4条《広告の入稿について》</p>
<p class="article_text">１．配信希望の広告原稿を入稿する際は、当社担当者まで電子メールにて送付するものとする。<br />
２．広告の入稿期限は原則として掲載開始希望日の５営業日前までとする。<br />
３．入稿期限を過ぎての解約、広告配信の中止は原則として出来ないものとする。<br />
４．入稿期限を過ぎての解約、広告配信の中止となった際は、通常の利用料金を違約金として当社に対して支払うものとする。</p>

<p class="article_headline">第5条《広告の掲載について》</p>
<p class="article_text">１．広告主は広告の配信を依頼するにあたり、当社が運営するパートナーサイト及び、パートナーサイト上の掲載位置、掲載方法を当社に一任するものとする。<br />
２．広告主はアフィリエイト型広告の配信を依頼するにあたり、パートナーサイトの登録、並びに報酬の支払いに関して承認の可否を決めることができるものとする。<br />
３．上記２項を原則とするが、当社との協議の上、上記以外の掲載条件について当社がこれを承諾したときにはこの限りではないものとする。</p>

<p class="article_headline">第6条《禁止事項について》</p>
<p class="article_text">１．当社と広告主は互いに開示された情報を無断で第三者に対し開示、漏洩、及び使用しないものとする。<br />
２．広告主は当社に対し、虚偽の情報を提供してはならない。<br />
３．本規約の各条項に違反してはならない・</p>

<p class="article_headline">第7条《利用料金の支払いについて》</p>
<p class="article_text">１．広告主は配信終了後、当社より請求する利用料金を支払うものとする。<br />
２．広告主は、30日毎にを支払い行うものとする。<br />
３．広告主から当社に対する広告報酬の支払いが遅延した場合、法的な手段を講じるものとする。<br />
４．広告主が本サービスに基づく利用料金の支払いを怠ったときは、年14パーセントの割合による遅延損害金（年365日の日割計算）を支払うこととする。<br />
５．広告主のウェブサイトに障害が発生したことにより当社で正常な成果情報の取得が不可能となった場合、当社規定の算出方法により算出した想定の成果件数分の料金を広告主もしくは代理店は支払うものとする。<br />
６．上記３項は第4条-４．の違約金の支払い義務にも適用されるものとする。</p>

<p class="article_headline">第8条《システムについて》</p>
<p class="article_text">当社は本サービス並びに本サービス内容をいつでも修正、変更、追加、削除等することができるものとする。サービスに関しては、５営業日前に当社に知らせるものとする。</p>

<p class="article_headline">第9条《準拠法、合意管轄について》</p>
<p class="article_text">１．本規約、並びに契約に関する問題は、日本法を準拠法とする。<br />
２．本規約、並びに契約に関する訴訟は、東京地方裁判所を第一審の専属管轄裁判所とする。</p>

<p class="article_headline">第10条《規約の改訂について》</p>
<p class="article_text">１．本規約は当社の判断により広告主の承諾なく変更、改訂できるものとし、広告主はこれを承諾するものとする。<br />
２．上記改訂後も本規約が当社と広告主とのすべての関係に適用されるものとする。</p>

</div>
</div><!-- main_contents -->

{include file='./hooter_web.tpl'}