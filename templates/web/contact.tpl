{include file='./header_web.tpl' page_title='TOP'}

{include file='./menu.tpl'}

<br />

<div id="side_contents_2">
{include file='./side_contents_2.tpl'}
</div><!-- side_contents -->

<div id="main_contents_2">
<div id="content_iti">

<!--<h2 class="padding"><img src="images/web/h2_contact.gif" alt="お問い合わせ" width="141" height="30" /></h2>-->
<div id="side_contents_header">
	<p><b>お問い合わせ</b></p>
</div>
<br />


<div id=message>
{if $error_message != ''}
<div id="error_message">
<h3>ERROR:{$error_message}</h3>
</div>
{/if}
{if $conf_message != ''}
<div id="info_message">
<h3>INFO:{$conf_message}</h3>
</div>
{/if}
<br />
</div><!-- message -->


{if $conf_message != ''}
<!--<h3>INFO:{$conf_message}</h3>-->
<!--<br />-->

<form method="POST" action="{$smarty.server.PHP_SELF}">

<table cellpadding="0" cellspacing="0" >
	<tr>
		<th colspan="2">貴社名※</th>
		<td>
			{$user_name}
		</td>
	</tr>
		<th colspan="2">都道府県</th>
		<td>
			{$pref}
		</td>
	<tr>
		<th colspan="2">住所１（市区町村）</th>
		<td>
			{$address1}
		</td>
	</tr>
	<tr>
		<th colspan="2">住所２（番地、建物）</th>
		<td>
			{$address2}
		</td>
	</tr>
	<tr>
		<th colspan="2">TEL</th>
		<td>
			{$tel}
		</td>
	</tr>
	<tr>
		<th colspan="2">メールアドレス</th>
		<td>
			{$email}
		</td>
	</tr>
	<tr>
		<th colspan="3">お問合せ内容・ご質問</th>
		<td>
		</td>
	</tr>
	<tr>
		<th colspan="3">{$textarea}</th>
		<td>
		</td>
	</tr>

	<tr>
		<td colspan="3">
			<br />
			<input type="submit" value="登録" style="background-color:#48D1CC; width:80px;" />
			<input type="hidden" name="mode" value="insert_commit_on" />
			<input type="hidden" name="user_name" value="{$user_name}" />
			<input type="hidden" name="pref" value="{$pref}" />
			<input type="hidden" name="address1" value="{$address1}" />
			<input type="hidden" name="address2" value="{$address2}" />
			<input type="hidden" name="tel" value="{$tel}" />
			<input type="hidden" name="email" value="{$email}" />
			<input type="hidden" name="textarea" value="{$textarea}" />
<!--
			<input type="submit" value="戻る" />
			<input type="hidden" name="mode" value="" />
			<input type="hidden" name="user_name" value="{$user_name}" />
			<input type="hidden" name="pref" value="{$pref}" />
			<input type="hidden" name="address1" value="{$address1}" />
			<input type="hidden" name="address2" value="{$address2}" />
			<input type="hidden" name="tel" value="{$tel}" />
			<input type="hidden" name="email" value="{$email}" />
			<input type="hidden" name="textarea" value="{$textarea}" />
 -->
		</td>
	</tr>

</table>
</form>

{else}

<div>
<form method="POST" action="{$smarty.server.PHP_SELF}">
<table cellpadding="0" cellspacing="0" >
	<tr>
		<th colspan="2">貴社名※</th>
		<td>
			<input type="text" size="20" maxlength="16" name="user_name" value="{$form_data.user_name|htmlspecialchars:$smarty.const.ENT_QUOTES}" />
		</td>
	</tr>
	<tr>
		<th colspan="2">都道府県</th>
		<td>
			<select name="pref">
			{foreach from=$pref_array key="key" item="data" name="pref_list"}
				<option value="{$data.id}"{if $data.id == $form_data.pref} selected="selected"{/if}>{$data.name|htmlspecialchars:$smarty.const.ENT_QUOTES}</option>
			{/foreach}
			</select>
		</td>
	</tr>
	<tr>
		<th colspan="2">住所１（市区町村）</th>
		<td>
			<input type="text" size="40" maxlength="40" name="address1" value="{$form_data.address1|htmlspecialchars:$smarty.const.ENT_QUOTES}" />
		</td>
	</tr>
	<tr>
		<th colspan="2">住所２（番地、建物）</th>
		<td>
			<input type="text" size="40" maxlength="40" name="address2" value="{$form_data.address2|htmlspecialchars:$smarty.const.ENT_QUOTES}" />
		</td>
	</tr>
	<tr>
		<th colspan="2">TEL</th>
		<td>
			<input type="text" size="30" maxlength="13"  name="tel" value="{$form_data.tel|htmlspecialchars:$smarty.const.ENT_QUOTES}" />
		</td>
	</tr>
	<tr>
		<th colspan="2">メールアドレス※</th>
		<td>
			<input type="text" size="30" maxlength="30" name="email" value="{$form_data.email|htmlspecialchars:$smarty.const.ENT_QUOTES}" />
		</td>
	</tr>
	<tr>
          <th colspan="3"><br />お問合せ内容・ご質問※(250文字以内)</th>
          <td></td>
    </tr>
	<tr>
          <th colspan="3" ><textarea name="textarea" cols="50"  rows="5" >{$form_data.textarea|htmlspecialchars:$smarty.const.ENT_QUOTES}</textarea>
          </th>
          <td></td>
    </tr>
	<tr>
		<td colspan="3">
			<br />
			<input type="submit" value="登録" style="background-color:#48D1CC; width:80px;" />
			<input type="hidden" name="mode" value="{$mode}" />
			<input type="hidden" name="todofuken" value="{$data.name|htmlspecialchars:$smarty.const.ENT_QUOTES}" />
			<input type="reset" name="reset" id="reset" value="リセット" style="background-color:#48D1CC; width:80px;" />
		</td>
	</tr>
</table>

</form>
</div>
{/if}


</div>
</div>

<!-- contents -->


{include file='./hooter_web.tpl'}
