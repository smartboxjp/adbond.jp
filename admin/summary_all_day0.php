<?php
// 共通設定
require_once( '../common/CommonAdminBase.php' );
require_once( '../common/CommonDao.php' );
require_once( '../dto/LoginUser.php' );

session_start();

if(isset($_SESSION['logon_token']) && $_SESSION['logon_token'] != ''){
	$login_user = new LoginUser();
	$login_user = $_SESSION['login_user'];

	// Smartyオブジェクト取得
	$smarty =& getSmartyObj();

	$smarty->assign("title", "Test Top Page");
	$smarty->assign("login_user", $login_user );

	$common_dao = new CommonDao();

	if(isset($_GET['month'])) {
		$month = $_GET['month'];
	}

	$list_sql = " SELECT IF(status = 2, DATE_FORMAT(action_complete_date,'%Y年%m月%d日'), DATE_FORMAT(created_at,'%Y年%m月%d日')) as date_string, "
				. " IF(status = 2, DATE_FORMAT(action_complete_date,'%Y%m%d'), DATE_FORMAT(created_at,'%Y%m%d')) as summary_month, "
				. " SUM(click_price_client) as click_price_client, "
				. " SUM(click_price_media) as click_price_media, "
				. " SUM(IF(status = 2, action_price_client, NULL)) as action_price_client, "
				. " SUM(IF(status = 2, action_price_media, NULL)) as action_price_media, "
				. " COUNT(status) as click_count, "
				. " COUNT(IF(status = 2, status, NULL)) as action_count "
				. " FROM action_logs "
				. " WHERE deleted_at is NULL "
				. " AND (status = 1 OR status = 2) "
				. " AND ( "
				. " (status = 1 AND DATE_FORMAT(created_at,'%Y%m') = '$month') "
				. " OR "
				. " (status = 2 AND DATE_FORMAT(action_complete_date,'%Y%m') = '$month') "
				. " ) "
				. " GROUP BY summary_month "
				. " ORDER BY created_at DESC ";

	$list_count = 0;
	$summary = array();

	$db_result = $common_dao->db_query($list_sql);
	if($db_result){

		foreach($db_result as $key => $val) {
			$summary[$val['summary_month']]['summary_date'] = $val['date_string'];
			$summary[$val['summary_month']]['month'] = $val['summary_month'];

			$summary[$val['summary_month']]['sales'] = $val['click_price_client'] + $val['action_price_client'];
			$summary[$val['summary_month']]['amounts'] = $val['click_price_media'] + $val['action_price_media'];
			$summary[$val['summary_month']]['fees'] = ($val['click_price_client'] + $val['action_price_client']) - ($val['click_price_media'] + $val['action_price_media']);
			$summary[$val['summary_month']]['click_count'] = $val['click_count'];
			$summary[$val['summary_month']]['action_count'] = $val['action_count'];

			$count[$key] = $val["summary_month"];
		}

		array_multisort($count, SORT_DESC, $summary);

		$smarty->assign("list", $summary);
		$list_count = count($summary);
	}else{
		$error_message .= "ＤＢからのデータの取得に失敗しました。(su0000)";
	}
	$smarty->assign("list_count", $list_count);
	$smarty->assign("error_message", $error_message);

	// ページを表示
	$smarty->display("./summary_all_day.tpl");
	exit();
}else{
	header('Location: ./login.php?error=1');
	exit();
}
?>