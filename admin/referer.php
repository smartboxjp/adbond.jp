<?php
// 共通設定
// ファイルインクルード
require_once( '../common/CommonAdminBase.php' );
require_once( '../common/CommonDao.php' );
require_once( '../dto/LoginUser.php' );
require_once( './class_page_calculate.php' );
require_once( '../dao/AdvertDao.php' );
require_once( '../dto/Advert.php' );

// セッションスタート
session_start();

// ログインチェック
if(isset($_SESSION['logon_token']) && $_SESSION['logon_token'] != ''){

	// ログインユーザークラスを生成
	$login_user = new LoginUser();
	// session変数よりログインクラスオブジェクトを代入
	$login_user = $_SESSION['login_user'];

	// **********************************************************
	// 変数初期化
	// **********************************************************
	// エラーメッセージ
	$error_message = "";
	// カウントページ遷移用path
	$count_link_path = "./referer.php";

	// **********************************************************
	// オブジェクト取得
	// **********************************************************
	// smartyオブジェクト生成
	$smarty =& getSmartyObj();
	// DB接続クラスを生成
	$common_dao = new CommonDao();
	// ページカウントクラス生成
	$c_page_claculate = new C_pageClaculate();

	// **********************************************************
	// smarty変数へデータの代入
	// **********************************************************
	// ページタイトルをsmarty変数へ格納
	$smarty->assign("title", "Test Top Page");
	// ログインユーザーをsmarty変数へ格納
	$smarty->assign("login_user", $login_user );


	// **********************************************************
	// POSTまたはGET送信より受け取ったパラメータを変数へ格納
	// **********************************************************
	// アクションかクリック
	if(isset($_POST['status']) && $_POST['status']) {
		$status = do_escape_quotes($_POST['status']);
	} elseif(isset($_GET['status']) && $_GET['status'] != "") {
		$status = do_escape_quotes($_GET['status']);
	} else {
		// statusがPOSTにセットされていなかった場合
		// statusに1をセット
		$status = 1;
	}

	// レコードカウントスタート値
	if(isset($_POST['limit']) && $_POST['limit'] != ""){
		$limit = do_escape_quotes($_POST['limit']);
	} elseif(isset($_GET['limit']) && $_GET['limit'] != "") {
		$limit = do_escape_quotes($_GET['limit']);
	} else {
		$limit = "0";
	}

	// 昇順か降順か
	if(isset($_POST['order']) && $_POST['order'] != ""){
		$order = do_escape_quotes($_POST['order']);
	} elseif(isset($_GET['order']) && $_GET['order'] != "") {
		$order = do_escape_quotes($_GET['order']);
	} else {
		$order = "DESC";
	}

	// 日付検索のチェック
	if(isset($_POST['date_check']) && $_POST['date_check']) {
		$date_check = do_escape_quotes($_POST['date_check']);
	} elseif(isset($_GET['date_check']) && $_GET['date_check'] != "") {
		$date_check = do_escape_quotes($_GET['date_check']);
	}

	// 広告主検索
	if(isset($_POST['advert_client_id']) && $_POST['advert_client_id'] != ""){
		$advert_client_id = do_escape_quotes($_POST['advert_client_id']);
	}elseif(isset($_GET['advert_client_id']) && $_GET['advert_client_id']){
		$advert_client_id = do_escape_quotes($_GET['advert_client_id']);
	} else {
		$advert_client_id = "0";
	}

	// キャリア選択
	if(isset($_POST['carrier']) && $_POST['carrier'] != ""){
		$carrier = $_POST['carrier'];
	} elseif(isset($_GET['carrier']) && $_GET['carrier'] != "") {
		$carrier = $_GET['carrier'];
	} else {
		$carrier = "0";
	}

	// 日付検索のチェックボックスにチェックがあるか
	// date_checkがセットされていた場合
	// date_checkに1がセットされる
	if($date_check == "1"){
		// -------------------------------------------------------
		// 日付取得
		// -------------------------------------------------------
		// 検索スタート年
		if(isset($_POST['s_year']) && $_POST['s_year'] != ""){
			$s_year = do_escape_quotes($_POST['s_year']);
		} elseif(isset($_GET['s_year']) && $_GET['s_year'] != "") {
			$s_year = do_escape_quotes($_GET['s_year']);
		}
		// 検索スタート月
		if(isset($_POST['s_month']) && $_POST['s_month'] != ""){
			$s_month = do_escape_quotes($_POST['s_month']);
		} elseif(isset($_GET['s_month']) && $_GET['s_month'] != "") {
			$s_month = do_escape_quotes($_GET['s_month']);
		}
		// 検索スタート日
		if(isset($_POST['s_day']) && $_POST['s_day'] != ""){
			$s_day = do_escape_quotes($_POST['s_day']);
		} elseif(isset($_GET['s_day']) && $_GET['s_day'] != "") {
			$s_day = do_escape_quotes($_GET['s_day']);
		}


		// 検索スタート年
		if(isset($_POST['e_year']) && $_POST['e_year'] != ""){
			$e_year = do_escape_quotes($_POST['e_year']);
		} elseif(isset($_GET['e_year']) && $_GET['e_year'] != "") {
			$e_year = do_escape_quotes($_GET['e_year']);
		}
		// 検索スタート年
		if(isset($_POST['e_month']) && $_POST['e_month'] != ""){
			$e_month = do_escape_quotes($_POST['e_month']);
		} elseif(isset($_GET['e_month']) && $_GET['e_month'] != "") {
			$e_month = do_escape_quotes($_GET['e_month']);
		}
		// 検索スタート年
		if(isset($_POST['e_day']) && $_POST['e_day'] != ""){
			$e_day = do_escape_quotes($_POST['e_day']);
		} elseif(isset($_GET['e_day']) && $_GET['e_day'] != "") {
			$e_day = do_escape_quotes($_GET['e_day']);
		}


		// 検索スタート年
		$s_date = $s_year . "-";
		// 検索スタート月
		$s_date .= $s_month . "-";
		// 検索スタート日
		$s_date .= $s_day;

		// 検索エンド年
		$e_date = $e_year . "-";
		// 検索エンド月
		$e_date .= $e_month . "-";
		// 検索エンド日
		$e_date .= $e_day;


	} else {
		// date_checkがセットされていなかった場合
		// date_checkに0をセット
		$date_check = 0;

		// $date_checkがセットされていなかった場合日付を取得
//		$three_last_month = getdate(strtotime("-3 month"));
		$now_date = getdate();

//		// スタート年
//		$s_year = $three_last_month['year'];
//		// スタート月
//		$s_month = $three_last_month['mon'];
//		// スタート日
//		$s_day = $three_last_month['mday'];

		//		// スタート年
		$s_year = $now_date['year'];
		// スタート月
		$s_month = $now_date['mon'];
		// スタート日
		$s_day = $now_date['mday'];

		// 今の年
		$e_year = $now_date['year'];
		// 今の月
		$e_month = $now_date['mon'];
		// 今の日
		$e_day = $now_date['mday'];

	}

	// -----------------------------------------------------------
	// 日付文字列の連結
	// -----------------------------------------------------------
	// 検索スタート年
	$s_date = $s_year . "-";
	// 検索スタート月
	$s_date .= $s_month . "-";
	// 検索スタート日
	$s_date .= $s_day;

	// 検索エンド年
	$e_date = $e_year . "-";
	// 検索エンド月
	$e_date .= $e_month . "-";
	// 検索エンド日
	$e_date .= $e_day;

	// **********************************************************
	// smarty変数へ格納
	// **********************************************************
	// スタート年
	$smarty->assign("set_s_year", $s_year);
	// スタート月
	$smarty->assign("set_s_month", $s_month);
	// スタート日
	$smarty->assign("set_s_day", $s_day);
	// 今の年
	$smarty->assign("set_e_year", $e_year);
	// 今の月
	$smarty->assign("set_e_month", $e_month);
	// 今の日
	$smarty->assign("set_e_day", $e_day);


	// **********************************************************
	// DB接続
	// **********************************************************
	// SELECT文発行 広告主名取得
	$get_select_aql = M_get_referer_advert_clients_select();
	$db_result = $common_dao->db_query($get_select_aql);
	if($db_result){
		// 広告主名をsmarty変数へ格納
		$smarty->assign("advert_clients_name",$db_result);
	}

	// SELECT文発行 status別レコード数カウント取得
	$get_select_aql = M_get_referer_count_select();
	$db_result = $common_dao->db_query($get_select_aql);

//	echo $get_select_aql;

	if($db_result){
		$list_count = $db_result[0]['count'];
		// レコードカウント数をsmarty変数へ格納
		$smarty->assign("list_count",$list_count);
	} else {
		// レコードカウント数をsmarty変数へ格納
		$smarty->assign("list_count", "0");
	}

	// SELECT文を発行 リファラーデータを取得
	$get_select_aql = M_get_referer_select();
	// DB結果を格納
	$db_result = $common_dao->db_query($get_select_aql);
	if($db_result){
		// DB結果をsmarty変数へ格納
		$smarty->assign("referer_list", $db_result);
	} else {
		$error_message = "DBの接続に失敗しました。";
	}

	// -----------------------------------------------------------
	// カウントページ用GETパラメータの連結
	// $status ⇒ アクションかクリック
	// $order ⇒ 昇順か降順
	// $date_check ⇒ 日付にチェックが入っているか
	// $advert_client_id ⇒ 広告主ID
	// $carrier ⇒ キャリア種別
	// $s_year ⇒ スタート年
	// $s_month ⇒ スタート月
	// $s_day ⇒ スタート日
	// $e_year ⇒ エンド年
	// $e_month ⇒ エンド月
	// $e_day ⇒ エンド日
	// -----------------------------------------------------------
	$param = "&status=$status"
	. "&order=$order"
	. "&date_check=$date_check"
	. "&advert_client_id=$advert_client_id"
	. "&carrier=$carrier";
	if($date_check == "1"){
		$param .= "&s_year=$s_year"
		. "&s_month=$s_month"
		. "&s_day=$s_day"
		. "&e_year=$e_year"
		. "&e_month=$e_month"
		. "&e_day=$e_day";
	}

	// **********************************************************
	// smarty変数へ格納
	// **********************************************************
	// ページカウントをsmarty変数へ格納
	$smarty->assign("page_count_link", $c_page_claculate->M_getpageClaculate($list_count, $limit, $count_link_path, $param));
	// $error_messageをsmarty変数へ格納
	$smarty->assign("error_message", $error_message);
	// $statusをsmarty変数へ格納
	$smarty->assign("status", $status);
	// $limitをsmarty変数へ格納
	$smarty->assign("limit", $limit);
	// $date_checkを変数へ格納
	$smarty->assign("date_check", $date_check);
	// $orderを変数へ格納
	$smarty->assign("order", $order);
	// $advert_client_idを変数へ格納
	$smarty->assign("advert_client_id", $advert_client_id);
	// キャリアをsmarty変数へ格納
	$smarty->assign("carrier", $carrier);

	// ページを表示
	$smarty->display("./referer.tpl");
	exit();

}

// *************************************************************
// リファラーテーブルに格納されているレコードを取得
// *************************************************************
function M_get_referer_select(){
	// グローバル変数の宣言
	global $status, $limit, $s_date, $e_date, $order, $advert_client_id, $carrier;

	// スパーグローバル変数
//	$GLOBALS['carrier'];

	// SELEST文の発行
	$sql = " SELECT "
	. " rl.*, "
	. " a.advert_name, "
	. " ac.client_name, "
	. " m.media_name "
	. " FROM ((( "
	. " referer_logs AS rl "
	. " LEFT JOIN advert AS a ON a.id = rl.advert_id ) "
	. " LEFT JOIN media AS m ON m.id = rl.media_id ) "
	. " LEFT JOIN advert_clients AS ac ON ac.id = a.advert_client_id ) "
	. " WHERE "
	. " rl.status = '$status' "
	. " AND "
	. " rl.created_at BETWEEN '$s_date 00:00:00' AND '$e_date 23:59:59' "
	. " AND "
	. " rl.delete_at is NULL ";

	// 広告主が選択されていた場合
	if($advert_client_id != "0"){
		$sql .= " AND ac.id = '$advert_client_id' ";
	}

	// キャリアが選択されていた場合
	if($carrier != "0"){
		$sql .= " AND rl.carrier_id = '$carrier' ";
	}

	$sql .= " ORDER BY rl.created_at $order "
	. " LIMIT $limit, 100 ";

	return $sql;
}

// *************************************************************
// リファラーテーブルに格納されている全てのレコードをカウント
// *************************************************************
function M_get_referer_count_select(){
	// グローバル変数の宣言
	global $status, $date_check,  $s_date, $e_date, $advert_client_id, $carrier;

	// 広告主が選択されていた場合
	if($advert_client_id != "0"){

		// SELECT文の発行
		$sql = " SELECT "
		. " count(rl.id) AS count "
		. " FROM (( "
		. " referer_logs AS rl "
		. " LEFT JOIN advert AS a ON a.id = rl.advert_id ) "
		. " LEFT JOIN advert_clients AS ac ON ac.id = a.advert_client_id ) "
		. " WHERE "
		. " rl.status = '$status' "
		. " AND "
		. " rl.delete_at is NULL "
		. " AND "
		. " a.advert_client_id = '$advert_client_id' ";

		// キャリアが選択されていた場合
		if($carrier != "0"){
			$sql .= " AND carrier_id = '$carrier' ";
		}

//		if ($date_check == "1"){
			$sql .= " AND "
			. " created_at BETWEEN '$s_date 00:00:00' AND '$e_date 23:59:59' ";
//		}

	// 広告主が選択されていなかった場合
	} else {

		// SELECT文の発行
		$sql = " SELECT "
		. " count(id) AS count "
		. " FROM "
		. " referer_logs  "
		. " WHERE "
		. " status = '$status' "
		. " AND "
		. " delete_at is NULL ";

		// キャリアが選択されていた場合
		if($carrier != "0"){
			$sql .= " AND carrier_id = '$carrier' ";
		}

//		if ($date_check == "1"){
			$sql .= " AND "
			. " created_at BETWEEN '$s_date 00:00:00' AND '$e_date 23:59:59' ";
//		}

	}

	return $sql;
}

// *************************************************************
// リファラーテーブルのadvert_idから広告主を取得
// *************************************************************
function M_get_referer_advert_clients_select(){

	// SELECT文の発行
	$sql = " SELECT "
	. " ac.client_name, "
	. " ac.id "
	. " FROM (( "
	. " referer_logs AS rl "
	. " LEFT JOIN advert AS a ON a.id = rl.advert_id ) "
	. " LEFT JOIN advert_clients AS ac ON ac.id = advert_client_id ) "
	. " GROUP BY ac.id ";

	return $sql;
}

// *************************************************************
// クウォート
// *************************************************************
function do_escape_quotes($str){
	//magic_quotesが有効ならクウォート部分を除去
	if(get_magic_quotes_gpc()){
		$str = stripslashes($str);
	}
	return $str;
}
?>