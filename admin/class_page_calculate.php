<?php
// *****************************************************************
// ページ遷移生成クラス
// *****************************************************************
class C_pageClaculate {

	// -------------------------------------------------------------
	// ページの計算とURLの発行
	// $num レコード数÷100 最大ページ数
	// $limit_num 現在の表示ページ
	// $url リンク先URL
	// -------------------------------------------------------------
	public function M_getpageClaculate($num, $limit_num, $url, $param = "") {

		// ページ数の計算
		$page_num = ceil($num / 100);
		$limit_num = ceil($limit_num / 100);

		// ループの中のカウン変数
		$while_count = 0;
		// 文言を格納する配列
		$page_num_str = "";



		// 最初のページリンク設定
		if($limit_num != $while_count){
			// リンクを貼る
			$linkurl = $url . "?limit=" . $while_count . $param;
			$page_num_str .= "<a href='$linkurl'>" . ($while_count + 1) . "</a> ";
		} else {
			// リンクを貼らない
			$page_num_str .= ($while_count + 1) . " ";
		}
		// ループの中のカウン変数に1代入
		$while_count = 1;



		while($page_num > $while_count){
			if($limit_num != $while_count){
				// リンクを貼る
				$linkurl = $url . "?limit=" . $while_count . "00" . $param;
				$page_num_str .= "<a href='$linkurl'>" . ($while_count + 1) . "</a> ";
			} else {
				// リンクを貼らない
				$page_num_str .= ($while_count + 1) . " ";
			}
			$while_count += 1;

		}

		return $page_num_str;

	}
}
?>