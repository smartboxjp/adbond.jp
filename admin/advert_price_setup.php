<?php
// ********************************************************
// 共通設定 ファイルインクルード
// ********************************************************
require_once( '../common/CommonAdminBase.php' );
require_once( '../common/CommonDao.php' );
require_once( '../dto/LoginUser.php' );
require_once( '../dao/AdvertDao.php' );
require_once( '../dto/Advert.php' );
require_once( '../dao/MediaDao.php' );
require_once( '../dto/Media.php' );
require_once( '../dao/MediaPublisherDao.php' );
require_once( '../dto/MediaPublisher.php' );
require_once( '../dao/MediaCategoryDao.php' );
require_once( '../dto/MediaCategory.php' );
require_once( '../dao/AdvertPriceMediaSetDao.php' );
require_once( '../dto/AdvertPriceMediaSet.php' );

// ********************************************************
// sessionスタート
// ********************************************************
session_start();

// ********************************************************
// 変数設定
// ********************************************************
$media_id_array = array();			// メディアIDを格納する配列
$advert_id_array = array();			// 広告(クライアント)IDを格納する配列





if(isset($_SESSION['logon_token']) && $_SESSION['logon_token'] != ''){
	$login_user = new LoginUser();
	$login_user = $_SESSION['login_user'];

	// Smartyオブジェクト取得
	$smarty =& getSmartyObj();

	$smarty->assign("login_user", $login_user );

	$common_dao = new CommonDao();
	$media_category_dao = new MediaCategoryDao();
	$advert_price_media_set_dao = new AdvertPriceMediaSetDao();

	if(isset($_POST['advert_id']) && $_POST['advert_id'] != '') {
		$advert_id = $_POST['advert_id'];
	} elseif(isset($_GET['advert_id']) && $_GET['advert_id'] != '') {
		$advert_id = $_GET['advert_id'];
	} else {
		$davert_id = "";
	}
	$smarty->assign("advert_id", $advert_id);

	if(isset($_POST['category_id']) && $_POST['category_id'] != '') {
		$category_id = $_POST['category_id'];
	} elseif(isset($_GET['category_id']) && $_GET['category_id'] != '') {
		$category_id = $_GET['category_id'];
	} else {
		$category_id = "";
	}
	$smarty->assign("category_id", $category_id);

	if(isset($_POST['publisher_id']) && $_POST['publisher_id'] != '') {
		$publisher_id = $_POST['publisher_id'];
	} elseif(isset($_GET['publisher_id']) && $_GET['publisher_id'] != '') {
		$publisher_id = $_GET['publisher_id'];
	} else {
		$publisher_id = "";
	}
	$smarty->assign("publisher_id", $publisher_id);

	if(isset($_POST['media_id']) && $_POST['media_id'] != '') {
		$media_id = $_POST['media_id'];
	} elseif(isset($_GET['media_id']) && $_GET['media_id'] != '') {
		$media_id = $_GET['media_id'];
	} else {
		$media_id = "";
	}
	$smarty->assign("media_id", $media_id);

	$check_category = array();
	$all_category = array();
	$check_publisher = array();
	$all_publisher = array();

	// セッション変数からmedia_publisher_idを取得
//	$smarty->assign("advert_setting_media_publisher_id", $_SESSION['advert_setting_media_publisher_id']);
//	$advert_setting_media_publisher_id = array();
//	$advert_setting_media_publisher_id = $_SESSION['advert_setting_media_publisher_id'];
//	$_SESSION['advert_setting_media_publisher_id'] = NULL;


	//広告カテゴリー
	$media_category_array = array();
	foreach($media_category_dao->getAllMediaCategory() as $val){
		$row_array = array('id' => $val->getId(), 'name' => $val->getName());
		$media_category_array[$val->getId()] = $row_array;
	}
	$smarty->assign("media_category_array", $media_category_array);

	$sql = " SELECT m.*, mp.publisher_name as publisher_name "
		. " FROM media as m "
		. " LEFT JOIN media_publishers as mp on m.media_publisher_id = mp.id "
		. " WHERE m.deleted_at is NULL ";
		if($category_id != ""){
			$sql .= " AND m.media_category_id = '$category_id' ";
		}
		$sql .= " ORDER BY m.id ASC ";

	$db_result = $common_dao->db_query($sql);
	if($db_result){
		foreach($db_result as $row) {
			$mp_id = $row['media_publisher_id'];
			if($mp_id != "0"){
				$m_id = $row['id'];
				$media_array[$mp_id]['publisher_id'] = $row['media_publisher_id'];

//				if($advert_setting_media_publisher_id){
//					foreach($advert_setting_media_publisher_id as $key => $val) {
//
//						if($val == $row['media_publisher_id']){
//							$media_array[$mp_id]['publisher_name'] = $row['publisher_name']."※";
//						} else {
//							$media_array[$mp_id]['publisher_name'] = $row['publisher_name'];
//						}
//
//					}
//				} else {
//					$media_array[$mp_id]['publisher_name'] = $row['publisher_name'];
//				}
				$media_array[$mp_id]['publisher_name'] = $row['publisher_name'];
				$media_array[$mp_id]['media'][$m_id]['media_id'] = $row['id'];
				$media_array[$mp_id]['media'][$m_id]['media_name'] = $row['media_name'];
			}
		}
		$smarty->assign("media_array", $media_array);

	}else{
		$error_message .= "ＤＢからのデータの取得に失敗しました。(su0000)";
	}

	$id = do_escape_quotes($_POST['id']);
	$click_price_client = do_escape_quotes($_POST['click_price_client']);
	$click_price_media = do_escape_quotes($_POST['click_price_media']);
	$action_price_client_docomo_1 = do_escape_quotes($_POST['action_price_client_docomo_1']);
	$action_price_client_softbank_1 = do_escape_quotes($_POST['action_price_client_softbank_1']);
	$action_price_client_au_1 = do_escape_quotes($_POST['action_price_client_au_1']);
	$action_price_client_pc_1 = do_escape_quotes($_POST['action_price_client_pc_1']);
	$action_price_client_docomo_2 = do_escape_quotes($_POST['action_price_client_docomo_2']);
	$action_price_client_softbank_2 = do_escape_quotes($_POST['action_price_client_softbank_2']);
	$action_price_client_au_2 = do_escape_quotes($_POST['action_price_client_au_2']);
	$action_price_client_pc_2 = do_escape_quotes($_POST['action_price_client_pc_2']);
	$action_price_client_docomo_3 = do_escape_quotes($_POST['action_price_client_docomo_3']);
	$action_price_client_softbank_3 = do_escape_quotes($_POST['action_price_client_softbank_3']);
	$action_price_client_au_3 = do_escape_quotes($_POST['action_price_client_au_3']);
	$action_price_client_pc_3 = do_escape_quotes($_POST['action_price_client_pc_3']);
	$action_price_client_docomo_4 = do_escape_quotes($_POST['action_price_client_docomo_4']);
	$action_price_client_softbank_4 = do_escape_quotes($_POST['action_price_client_softbank_4']);
	$action_price_client_au_4 = do_escape_quotes($_POST['action_price_client_au_4']);
	$action_price_client_pc_4 = do_escape_quotes($_POST['action_price_client_pc_4']);
	$action_price_client_docomo_5 = do_escape_quotes($_POST['action_price_client_docomo_5']);
	$action_price_client_softbank_5 = do_escape_quotes($_POST['action_price_client_softbank_5']);
	$action_price_client_au_5 = do_escape_quotes($_POST['action_price_client_au_5']);
	$action_price_client_pc_5 = do_escape_quotes($_POST['action_price_client_pc_5']);
	$action_price_media_docomo_1 = do_escape_quotes($_POST['action_price_media_docomo_1']);
	$action_price_media_softbank_1 = do_escape_quotes($_POST['action_price_media_softbank_1']);
	$action_price_media_au_1 = do_escape_quotes($_POST['action_price_media_au_1']);
	$action_price_media_pc_1 = do_escape_quotes($_POST['action_price_media_pc_1']);
	$action_price_media_docomo_2 = do_escape_quotes($_POST['action_price_media_docomo_2']);
	$action_price_media_softbank_2 = do_escape_quotes($_POST['action_price_media_softbank_2']);
	$action_price_media_au_2 = do_escape_quotes($_POST['action_price_media_au_2']);
	$action_price_media_pc_2 = do_escape_quotes($_POST['action_price_media_pc_2']);
	$action_price_media_docomo_3 = do_escape_quotes($_POST['action_price_media_docomo_3']);
	$action_price_media_softbank_3 = do_escape_quotes($_POST['action_price_media_softbank_3']);
	$action_price_media_au_3 = do_escape_quotes($_POST['action_price_media_au_3']);
	$action_price_media_pc_3 = do_escape_quotes($_POST['action_price_media_pc_3']);
	$action_price_media_docomo_4 = do_escape_quotes($_POST['action_price_media_docomo_4']);
	$action_price_media_softbank_4 = do_escape_quotes($_POST['action_price_media_softbank_4']);
	$action_price_media_au_4 = do_escape_quotes($_POST['action_price_media_au_4']);
	$action_price_media_pc_4 = do_escape_quotes($_POST['action_price_media_pc_4']);
	$action_price_media_docomo_5 = do_escape_quotes($_POST['action_price_media_docomo_5']);
	$action_price_media_softbank_5 = do_escape_quotes($_POST['action_price_media_softbank_5']);
	$action_price_media_au_5 = do_escape_quotes($_POST['action_price_media_au_5']);
	$action_price_media_pc_5 = do_escape_quotes($_POST['action_price_media_pc_5']);


	if($advert_id != 0 && $media_id != 0) {
		$advert_price_madia_set = new AdvertPriceMediaSet();
		$advert_price_madia_set = $advert_price_media_set_dao->getAdvertPriceMediaSetByMidAid($advert_id, $media_id);

		if(!is_null($advert_price_madia_set)) {
			$form_data = array('id' => $advert_price_madia_set->getId(),
							'click_price_client' => $advert_price_madia_set->getClickPriceClient(),
							'click_price_media' => $advert_price_madia_set->getClickPriceMedia(),
							'action_price_client_docomo_1' => $advert_price_madia_set->getActionPriceClientDocomo1(),
							'action_price_client_softbank_1' => $advert_price_madia_set->getActionPriceClientSoftbank1(),
							'action_price_client_au_1' => $advert_price_madia_set->getActionPriceClientAu1(),
							'action_price_client_pc_1' => $advert_price_madia_set->getActionPriceClientPc1(),
							'action_price_client_docomo_2' => $advert_price_madia_set->getActionPriceClientDocomo2(),
							'action_price_client_softbank_2' => $advert_price_madia_set->getActionPriceClientSoftbank2(),
							'action_price_client_au_2' => $advert_price_madia_set->getActionPriceClientAu2(),
							'action_price_client_pc_2' => $advert_price_madia_set->getActionPriceClientPc2(),
							'action_price_client_docomo_3' => $advert_price_madia_set->getActionPriceClientDocomo3(),
							'action_price_client_softbank_3' => $advert_price_madia_set->getActionPriceClientSoftbank3(),
							'action_price_client_au_3' => $advert_price_madia_set->getActionPriceClientAu3(),
							'action_price_client_pc_3' => $advert_price_madia_set->getActionPriceClientPc3(),
							'action_price_client_docomo_4' => $advert_price_madia_set->getActionPriceClientDocomo4(),
							'action_price_client_softbank_4' => $advert_price_madia_set->getActionPriceClientSoftbank4(),
							'action_price_client_au_4' => $advert_price_madia_set->getActionPriceClientAu4(),
							'action_price_client_pc_4' => $advert_price_madia_set->getActionPriceClientPc4(),
							'action_price_client_docomo_5' => $advert_price_madia_set->getActionPriceClientDocomo5(),
							'action_price_client_softbank_5' => $advert_price_madia_set->getActionPriceClientSoftbank5(),
							'action_price_client_au_5' => $advert_price_madia_set->getActionPriceClientAu5(),
							'action_price_client_pc_5' => $advert_price_madia_set->getActionPriceClientPc5(),
							'action_price_media_docomo_1' => $advert_price_madia_set->getActionPriceMediaDocomo1(),
							'action_price_media_softbank_1' => $advert_price_madia_set->getActionPriceMediaSoftbank1(),
							'action_price_media_au_1' => $advert_price_madia_set->getActionPriceMediaAu1(),
							'action_price_media_pc_1' => $advert_price_madia_set->getActionPriceMediaPc1(),
							'action_price_media_docomo_2' => $advert_price_madia_set->getActionPriceMediaDocomo2(),
							'action_price_media_softbank_2' => $advert_price_madia_set->getActionPriceMediaSoftbank2(),
							'action_price_media_au_2' => $advert_price_madia_set->getActionPriceMediaAu2(),
							'action_price_media_pc_2' => $advert_price_madia_set->getActionPriceMediaPc2(),
							'action_price_media_docomo_3' => $advert_price_madia_set->getActionPriceMediaDocomo3(),
							'action_price_media_softbank_3' => $advert_price_madia_set->getActionPriceMediaSoftbank3(),
							'action_price_media_au_3' => $advert_price_madia_set->getActionPriceMediaAu3(),
							'action_price_media_pc_3' => $advert_price_madia_set->getActionPriceMediaPc3(),
							'action_price_media_docomo_4' => $advert_price_madia_set->getActionPriceMediaDocomo4(),
							'action_price_media_softbank_4' => $advert_price_madia_set->getActionPriceMediaSoftbank4(),
							'action_price_media_au_4' => $advert_price_madia_set->getActionPriceMediaAu4(),
							'action_price_media_pc_4' => $advert_price_madia_set->getActionPriceMediaPc4(),
							'action_price_media_docomo_5' => $advert_price_madia_set->getActionPriceMediaDocomo5(),
							'action_price_media_softbank_5' => $advert_price_madia_set->getActionPriceMediaSoftbank5(),
							'action_price_media_au_5' => $advert_price_madia_set->getActionPriceMediaAu5(),
							'action_price_media_pc_5' => $advert_price_madia_set->getActionPriceMediaPc5());

			$smarty->assign("form_data", $form_data);
		}
	}

	// *********************************************************************************
	// 一括設定
	// *********************************************************************************
	if(isset($_GET['mode']) && $_GET['mode'] == "all_set"){

		// GETで受け取ったパラメータを変数へ格納
		$mode = $_GET['mode'];
		// smarty変数へ格納
		$smarty->assign("mode", $mode);

		if(isset($_SESSION['form_data'])){
			$smarty->assign("form_data", $_SESSION['form_data']);
		}

	}

	// DBへ接続
	if (isset($_POST['mode']) && $_POST['mode'] == "all_set_decision"){

		// GETで受け取ったパラメータを変数へ格納
		$mode = $_POST['mode'];
		// smarty変数へ格納
		$smarty->assign("mode", $mode);

		// 広告データを取得するクラス生成
		$media_dao = new MediaDao();

		// トランザクションスタート
		$media_dao -> transaction_start();

		// advert_idを指定して広告データを取得
		if($category_id == ""){
			$result = $media_dao -> getMediaByPublisherId($publisher_id, $category_id);
		} else {
			$result = $media_dao -> getMediaByPublisherCategoryId($publisher_id, $category_id);
		}

		// DB結果
		if($result){
			// 成功
			echo "成功";
			// 取得レコード分ループ
			foreach($result as $row => $val ) {

				$media_id = $val->getId();

				$advert_price_media_set_dao->transaction_start();
				$result = $advert_price_media_set_dao->getAdvertPriceMediaSetByMidAid($advert_id, $media_id);

//				// テスト出力
//
//				$mode = $_POST['mode'];
//				echo $mode;
//				echo "<br />";
//				echo $advert_id;
//				echo "<br />";
//				echo $media_id;
//				echo "<br />";
//				echo $action_price_client_docomo_1;
//				echo "<br />";
//				if(is_null($result)){
//					echo "insert<br />";
//				}else{
//					echo "update<br />";
//				}
//				echo "<hr />";

// **************************************************************************************
// insert
// **************************************************************************************
						if(is_null($result)) {
							$advert_price_media_set = new AdvertPriceMediaSet();
							$advert_price_media_set->setAdvertId($advert_id);
							$advert_price_media_set->setMediaId($media_id);
							$advert_price_media_set->setClickPriceClient($click_price_client);
							$advert_price_media_set->setClickPriceMedia($click_price_media);
							$advert_price_media_set->setActionPriceClientDocomo1($action_price_client_docomo_1);
							$advert_price_media_set->setActionPriceClientSoftbank1($action_price_client_softbank_1);
							$advert_price_media_set->setActionPriceClientAu1($action_price_client_au_1);
							$advert_price_media_set->setActionPriceClientPc1($action_price_client_pc_1);
							$advert_price_media_set->setActionPriceClientDocomo2($action_price_client_docomo_2);
							$advert_price_media_set->setActionPriceClientSoftbank2($action_price_client_softbank_2);
							$advert_price_media_set->setActionPriceClientAu2($action_price_client_au_2);
							$advert_price_media_set->setActionPriceClientPc2($action_price_client_pc_2);
							$advert_price_media_set->setActionPriceClientDocomo3($action_price_client_docomo_3);
							$advert_price_media_set->setActionPriceClientSoftbank3($action_price_client_softbank_3);
							$advert_price_media_set->setActionPriceClientAu3($action_price_client_au_3);
							$advert_price_media_set->setActionPriceClientPc3($action_price_client_pc_3);
							$advert_price_media_set->setActionPriceClientDocomo4($action_price_client_docomo_4);
							$advert_price_media_set->setActionPriceClientSoftbank4($action_price_client_softbank_4);
							$advert_price_media_set->setActionPriceClientAu4($action_price_client_au_4);
							$advert_price_media_set->setActionPriceClientPc4($action_price_client_pc_4);
							$advert_price_media_set->setActionPriceClientDocomo5($action_price_client_docomo_5);
							$advert_price_media_set->setActionPriceClientSoftbank5($action_price_client_softbank_5);
							$advert_price_media_set->setActionPriceClientAu5($action_price_client_au_5);
							$advert_price_media_set->setActionPriceClientPc5($action_price_client_pc_5);
							$advert_price_media_set->setActionPriceMediaDocomo1($action_price_media_docomo_1);
							$advert_price_media_set->setActionPriceMediaSoftbank1($action_price_media_softbank_1);
							$advert_price_media_set->setActionPriceMediaAu1($action_price_media_au_1);
							$advert_price_media_set->setActionPriceMediaPc1($action_price_media_pc_1);
							$advert_price_media_set->setActionPriceMediaDocomo2($action_price_media_docomo_2);
							$advert_price_media_set->setActionPriceMediaSoftbank2($action_price_media_softbank_2);
							$advert_price_media_set->setActionPriceMediaAu2($action_price_media_au_2);
							$advert_price_media_set->setActionPriceMediaPc2($action_price_media_pc_2);
							$advert_price_media_set->setActionPriceMediaDocomo3($action_price_media_docomo_3);
							$advert_price_media_set->setActionPriceMediaSoftbank3($action_price_media_softbank_3);
							$advert_price_media_set->setActionPriceMediaAu3($action_price_media_au_3);
							$advert_price_media_set->setActionPriceMediaPc3($action_price_media_pc_3);
							$advert_price_media_set->setActionPriceMediaDocomo4($action_price_media_docomo_4);
							$advert_price_media_set->setActionPriceMediaSoftbank4($action_price_media_softbank_4);
							$advert_price_media_set->setActionPriceMediaAu4($action_price_media_au_4);
							$advert_price_media_set->setActionPriceMediaPc4($action_price_media_pc_4);
							$advert_price_media_set->setActionPriceMediaDocomo5($action_price_media_docomo_5);
							$advert_price_media_set->setActionPriceMediaSoftbank5($action_price_media_softbank_5);
							$advert_price_media_set->setActionPriceMediaAu5($action_price_media_au_5);
							$advert_price_media_set->setActionPriceMediaPc5($action_price_media_pc_5);

							//INSERTを実行
							$db_result = $advert_price_media_set_dao->insertAdvertPriceMediaSet($advert_price_media_set, $result_message);
						} else {

// **************************************************************************************
// update
// **************************************************************************************
							$advert_price_media_set = new AdvertPriceMediaSet();
							$advert_price_media_set->setId($id);
							$advert_price_media_set->setAdvertId($advert_id);
							$advert_price_media_set->setMediaId($media_id);
							$advert_price_media_set->setClickPriceClient($click_price_client);
							$advert_price_media_set->setClickPriceMedia($click_price_media);
							$advert_price_media_set->setActionPriceClientDocomo1($action_price_client_docomo_1);
							$advert_price_media_set->setActionPriceClientSoftbank1($action_price_client_softbank_1);
							$advert_price_media_set->setActionPriceClientAu1($action_price_client_au_1);
							$advert_price_media_set->setActionPriceClientPc1($action_price_client_pc_1);
							$advert_price_media_set->setActionPriceClientDocomo2($action_price_client_docomo_2);
							$advert_price_media_set->setActionPriceClientSoftbank2($action_price_client_softbank_2);
							$advert_price_media_set->setActionPriceClientAu2($action_price_client_au_2);
							$advert_price_media_set->setActionPriceClientPc2($action_price_client_pc_2);
							$advert_price_media_set->setActionPriceClientDocomo3($action_price_client_docomo_3);
							$advert_price_media_set->setActionPriceClientSoftbank3($action_price_client_softbank_3);
							$advert_price_media_set->setActionPriceClientAu3($action_price_client_au_3);
							$advert_price_media_set->setActionPriceClientPc3($action_price_client_pc_3);
							$advert_price_media_set->setActionPriceClientDocomo4($action_price_client_docomo_4);
							$advert_price_media_set->setActionPriceClientSoftbank4($action_price_client_softbank_4);
							$advert_price_media_set->setActionPriceClientAu4($action_price_client_au_4);
							$advert_price_media_set->setActionPriceClientPc4($action_price_client_pc_4);
							$advert_price_media_set->setActionPriceClientDocomo5($action_price_client_docomo_5);
							$advert_price_media_set->setActionPriceClientSoftbank5($action_price_client_softbank_5);
							$advert_price_media_set->setActionPriceClientAu5($action_price_client_au_5);
							$advert_price_media_set->setActionPriceClientPc5($action_price_client_pc_5);
							$advert_price_media_set->setActionPriceMediaDocomo1($action_price_media_docomo_1);
							$advert_price_media_set->setActionPriceMediaSoftbank1($action_price_media_softbank_1);
							$advert_price_media_set->setActionPriceMediaAu1($action_price_media_au_1);
							$advert_price_media_set->setActionPriceMediaPc1($action_price_media_pc_1);
							$advert_price_media_set->setActionPriceMediaDocomo2($action_price_media_docomo_2);
							$advert_price_media_set->setActionPriceMediaSoftbank2($action_price_media_softbank_2);
							$advert_price_media_set->setActionPriceMediaAu2($action_price_media_au_2);
							$advert_price_media_set->setActionPriceMediaPc2($action_price_media_pc_2);
							$advert_price_media_set->setActionPriceMediaDocomo3($action_price_media_docomo_3);
							$advert_price_media_set->setActionPriceMediaSoftbank3($action_price_media_softbank_3);
							$advert_price_media_set->setActionPriceMediaAu3($action_price_media_au_3);
							$advert_price_media_set->setActionPriceMediaPc3($action_price_media_pc_3);
							$advert_price_media_set->setActionPriceMediaDocomo4($action_price_media_docomo_4);
							$advert_price_media_set->setActionPriceMediaSoftbank4($action_price_media_softbank_4);
							$advert_price_media_set->setActionPriceMediaAu4($action_price_media_au_4);
							$advert_price_media_set->setActionPriceMediaPc4($action_price_media_pc_4);
							$advert_price_media_set->setActionPriceMediaDocomo5($action_price_media_docomo_5);
							$advert_price_media_set->setActionPriceMediaSoftbank5($action_price_media_softbank_5);
							$advert_price_media_set->setActionPriceMediaAu5($action_price_media_au_5);
							$advert_price_media_set->setActionPriceMediaPc5($action_price_media_pc_5);

							//UPDATEを実行
							$db_result = $advert_price_media_set_dao->updateAdvertPriceMediaAdvertMediaSet($advert_price_media_set, $advert_id, $media_id);
						}
						if($db_result) {
							$advert_price_media_set_dao->transaction_end();
						} else {
							$advert_view_media_set_dao->transaction_rollback();
						}
					}
					$media_dao -> transaction_end();
//					// ページを表示
//					header('Location: ./advert.php');
//					exit();

				//-----------------------------------------------------------------------------------------------
				$form_data = array(
								'action_price_client_docomo_1' => $action_price_client_docomo_1,
								'action_price_client_softbank_1' => $action_price_client_softbank_1,
								'action_price_client_au_1' => $action_price_client_au_1,
								'action_price_client_pc_1' => $action_price_client_pc_1,
								'action_price_client_docomo_2' => $action_price_client_docomo_2,
								'action_price_client_softbank_2' => $action_price_client_softbank_2,
								'action_price_client_au_2' => $action_price_client_au_2,
								'action_price_client_pc_2' => $action_price_client_pc_2,
								'action_price_client_docomo_3' => $action_price_client_docomo_3,
								'action_price_client_softbank_3' => $action_price_client_softbank_3,
								'action_price_client_au_3' => $action_price_client_au_3,
								'action_price_client_pc_3' => $action_price_client_pc_3,
								'action_price_client_docomo_4' => $action_price_client_docomo_4,
								'action_price_client_softbank_4' => $action_price_client_softbank_4,
								'action_price_client_au_4' => $action_price_client_au_4,
								'action_price_client_pc_4' => $action_price_client_pc_4,
								'action_price_client_docomo_5' => $action_price_client_docomo_5,
								'action_price_client_softbank_5' => $action_price_client_softbank_5,
								'action_price_client_au_5' => $action_price_client_au_5,
								'action_price_client_pc_5' => $action_price_client_pc_5,

								'action_price_media_docomo_1' => $action_price_media_docomo_1,
								'action_price_media_softbank_1' => $action_price_media_softbank_1,
								'action_price_media_au_1' => $action_price_media_au_1,
								'action_price_media_pc_1' => $action_price_media_pc_1,
								'action_price_media_docomo_2' => $action_price_media_docomo_2,
								'action_price_media_softbank_2' => $action_price_media_softbank_2,
								'action_price_media_au_2' => $action_price_media_au_2,
								'action_price_media_pc_2' => $action_price_media_pc_2,
								'action_price_media_docomo_3' => $action_price_media_docomo_3,
								'action_price_media_softbank_3' => $action_price_media_softbank_3,
								'action_price_media_au_3' => $action_price_media_au_3,
								'action_price_media_pc_3' => $action_price_media_pc_3,
								'action_price_media_docomo_4' => $action_price_media_docomo_4,
								'action_price_media_softbank_4' => $action_price_media_softbank_4,
								'action_price_media_au_4' => $action_price_media_au_4,
								'action_price_media_pc_4' => $action_price_media_pc_4,
								'action_price_media_docomo_5' => $action_price_media_docomo_5,
								'action_price_media_softbank_5' => $action_price_media_softbank_5,
								'action_price_media_au_5' => $action_price_media_au_5,
								'action_price_media_pc_5' => $action_price_media_pc_5);

				// session変数へ配列を格納
//				$_SESSION['form_data'] = $form_data;

				$smarty->assign("form_data", $form_data);
				//-----------------------------------------------------------------------------------------------


			}else {
			// 失敗
			$error_Message = "DBからデータの取得に失敗しました。";
			// smarty変数へ格納
			$smarty->assign("error_Message", $error_Message);

		}


	// *********************************************************************************
	// 個別設定
	// *********************************************************************************
	} elseif(isset($_POST['mode']) && $_POST['mode'] == "setup") {

		$advert_price_media_set_dao->transaction_start();

		$result = $advert_price_media_set_dao->getAdvertPriceMediaSetByMidAid($advert_id, $media_id);

// **************************************************************************************
// insert
// **************************************************************************************
		if(is_null($result)) {
			$advert_price_media_set = new AdvertPriceMediaSet();
			$advert_price_media_set->setAdvertId($advert_id);
			$advert_price_media_set->setMediaId($media_id);
			$advert_price_media_set->setClickPriceClient($click_price_client);
			$advert_price_media_set->setClickPriceMedia($click_price_media);
			$advert_price_media_set->setActionPriceClientDocomo1($action_price_client_docomo_1);
			$advert_price_media_set->setActionPriceClientSoftbank1($action_price_client_softbank_1);
			$advert_price_media_set->setActionPriceClientAu1($action_price_client_au_1);
			$advert_price_media_set->setActionPriceClientPc1($action_price_client_pc_1);
			$advert_price_media_set->setActionPriceClientDocomo2($action_price_client_docomo_2);
			$advert_price_media_set->setActionPriceClientSoftbank2($action_price_client_softbank_2);
			$advert_price_media_set->setActionPriceClientAu2($action_price_client_au_2);
			$advert_price_media_set->setActionPriceClientPc2($action_price_client_pc_2);
			$advert_price_media_set->setActionPriceClientDocomo3($action_price_client_docomo_3);
			$advert_price_media_set->setActionPriceClientSoftbank3($action_price_client_softbank_3);
			$advert_price_media_set->setActionPriceClientAu3($action_price_client_au_3);
			$advert_price_media_set->setActionPriceClientPc3($action_price_client_pc_3);
			$advert_price_media_set->setActionPriceClientDocomo4($action_price_client_docomo_4);
			$advert_price_media_set->setActionPriceClientSoftbank4($action_price_client_softbank_4);
			$advert_price_media_set->setActionPriceClientAu4($action_price_client_au_4);
			$advert_price_media_set->setActionPriceClientPc4($action_price_client_pc_4);
			$advert_price_media_set->setActionPriceClientDocomo5($action_price_client_docomo_5);
			$advert_price_media_set->setActionPriceClientSoftbank5($action_price_client_softbank_5);
			$advert_price_media_set->setActionPriceClientAu5($action_price_client_au_5);
			$advert_price_media_set->setActionPriceClientPc5($action_price_client_pc_5);
			$advert_price_media_set->setActionPriceMediaDocomo1($action_price_media_docomo_1);
			$advert_price_media_set->setActionPriceMediaSoftbank1($action_price_media_softbank_1);
			$advert_price_media_set->setActionPriceMediaAu1($action_price_media_au_1);
			$advert_price_media_set->setActionPriceMediaPc1($action_price_media_pc_1);
			$advert_price_media_set->setActionPriceMediaDocomo2($action_price_media_docomo_2);
			$advert_price_media_set->setActionPriceMediaSoftbank2($action_price_media_softbank_2);
			$advert_price_media_set->setActionPriceMediaAu2($action_price_media_au_2);
			$advert_price_media_set->setActionPriceMediaPc2($action_price_media_pc_2);
			$advert_price_media_set->setActionPriceMediaDocomo3($action_price_media_docomo_3);
			$advert_price_media_set->setActionPriceMediaSoftbank3($action_price_media_softbank_3);
			$advert_price_media_set->setActionPriceMediaAu3($action_price_media_au_3);
			$advert_price_media_set->setActionPriceMediaPc3($action_price_media_pc_3);
			$advert_price_media_set->setActionPriceMediaDocomo4($action_price_media_docomo_4);
			$advert_price_media_set->setActionPriceMediaSoftbank4($action_price_media_softbank_4);
			$advert_price_media_set->setActionPriceMediaAu4($action_price_media_au_4);
			$advert_price_media_set->setActionPriceMediaPc4($action_price_media_pc_4);
			$advert_price_media_set->setActionPriceMediaDocomo5($action_price_media_docomo_5);
			$advert_price_media_set->setActionPriceMediaSoftbank5($action_price_media_softbank_5);
			$advert_price_media_set->setActionPriceMediaAu5($action_price_media_au_5);
			$advert_price_media_set->setActionPriceMediaPc5($action_price_media_pc_5);

			//INSERTを実行
			$db_result = $advert_price_media_set_dao->insertAdvertPriceMediaSet($advert_price_media_set, $result_message);
		} else {

// **************************************************************************************
// update
// **************************************************************************************
			$advert_price_media_set = new AdvertPriceMediaSet();
			$advert_price_media_set->setId($id);
			$advert_price_media_set->setAdvertId($advert_id);
			$advert_price_media_set->setMediaId($media_id);
			$advert_price_media_set->setClickPriceClient($click_price_client);
			$advert_price_media_set->setClickPriceMedia($click_price_media);
			$advert_price_media_set->setActionPriceClientDocomo1($action_price_client_docomo_1);
			$advert_price_media_set->setActionPriceClientSoftbank1($action_price_client_softbank_1);
			$advert_price_media_set->setActionPriceClientAu1($action_price_client_au_1);
			$advert_price_media_set->setActionPriceClientPc1($action_price_client_pc_1);
			$advert_price_media_set->setActionPriceClientDocomo2($action_price_client_docomo_2);
			$advert_price_media_set->setActionPriceClientSoftbank2($action_price_client_softbank_2);
			$advert_price_media_set->setActionPriceClientAu2($action_price_client_au_2);
			$advert_price_media_set->setActionPriceClientPc2($action_price_client_pc_2);
			$advert_price_media_set->setActionPriceClientDocomo3($action_price_client_docomo_3);
			$advert_price_media_set->setActionPriceClientSoftbank3($action_price_client_softbank_3);
			$advert_price_media_set->setActionPriceClientAu3($action_price_client_au_3);
			$advert_price_media_set->setActionPriceClientPc3($action_price_client_pc_3);
			$advert_price_media_set->setActionPriceClientDocomo4($action_price_client_docomo_4);
			$advert_price_media_set->setActionPriceClientSoftbank4($action_price_client_softbank_4);
			$advert_price_media_set->setActionPriceClientAu4($action_price_client_au_4);
			$advert_price_media_set->setActionPriceClientPc4($action_price_client_pc_4);
			$advert_price_media_set->setActionPriceClientDocomo5($action_price_client_docomo_5);
			$advert_price_media_set->setActionPriceClientSoftbank5($action_price_client_softbank_5);
			$advert_price_media_set->setActionPriceClientAu5($action_price_client_au_5);
			$advert_price_media_set->setActionPriceClientPc5($action_price_client_pc_5);
			$advert_price_media_set->setActionPriceMediaDocomo1($action_price_media_docomo_1);
			$advert_price_media_set->setActionPriceMediaSoftbank1($action_price_media_softbank_1);
			$advert_price_media_set->setActionPriceMediaAu1($action_price_media_au_1);
			$advert_price_media_set->setActionPriceMediaPc1($action_price_media_pc_1);
			$advert_price_media_set->setActionPriceMediaDocomo2($action_price_media_docomo_2);
			$advert_price_media_set->setActionPriceMediaSoftbank2($action_price_media_softbank_2);
			$advert_price_media_set->setActionPriceMediaAu2($action_price_media_au_2);
			$advert_price_media_set->setActionPriceMediaPc2($action_price_media_pc_2);
			$advert_price_media_set->setActionPriceMediaDocomo3($action_price_media_docomo_3);
			$advert_price_media_set->setActionPriceMediaSoftbank3($action_price_media_softbank_3);
			$advert_price_media_set->setActionPriceMediaAu3($action_price_media_au_3);
			$advert_price_media_set->setActionPriceMediaPc3($action_price_media_pc_3);
			$advert_price_media_set->setActionPriceMediaDocomo4($action_price_media_docomo_4);
			$advert_price_media_set->setActionPriceMediaSoftbank4($action_price_media_softbank_4);
			$advert_price_media_set->setActionPriceMediaAu4($action_price_media_au_4);
			$advert_price_media_set->setActionPriceMediaPc4($action_price_media_pc_4);
			$advert_price_media_set->setActionPriceMediaDocomo5($action_price_media_docomo_5);
			$advert_price_media_set->setActionPriceMediaSoftbank5($action_price_media_softbank_5);
			$advert_price_media_set->setActionPriceMediaAu5($action_price_media_au_5);
			$advert_price_media_set->setActionPriceMediaPc5($action_price_media_pc_5);

			//UPDATEを実行
			$db_result = $advert_price_media_set_dao->updateAdvertPriceMediaSet($advert_price_media_set, $result_message);
		}
		if($db_result) {
			$advert_price_media_set_dao->transaction_end();

			// ページを表示
//			header('Location: ./advert.php');
//			exit();
		} else {
			$advert_view_media_set_dao->transaction_rollback();
		}

		// -------------------------------------------------------------------------------------
		// 再度DBへ接続し、変更または、登録したデータを取得
		if($advert_id != 0 && $media_id != 0) {
			$advert_price_madia_set = new AdvertPriceMediaSet();
			$advert_price_madia_set = $advert_price_media_set_dao->getAdvertPriceMediaSetByMidAid($advert_id, $media_id);

			if(!is_null($advert_price_madia_set)) {
				$form_data = array('id' => $advert_price_madia_set->getId(),
								'click_price_client' => $advert_price_madia_set->getClickPriceClient(),
								'click_price_media' => $advert_price_madia_set->getClickPriceMedia(),
								'action_price_client_docomo_1' => $advert_price_madia_set->getActionPriceClientDocomo1(),
								'action_price_client_softbank_1' => $advert_price_madia_set->getActionPriceClientSoftbank1(),
								'action_price_client_au_1' => $advert_price_madia_set->getActionPriceClientAu1(),
								'action_price_client_pc_1' => $advert_price_madia_set->getActionPriceClientPc1(),
								'action_price_client_docomo_2' => $advert_price_madia_set->getActionPriceClientDocomo2(),
								'action_price_client_softbank_2' => $advert_price_madia_set->getActionPriceClientSoftbank2(),
								'action_price_client_au_2' => $advert_price_madia_set->getActionPriceClientAu2(),
								'action_price_client_pc_2' => $advert_price_madia_set->getActionPriceClientPc2(),
								'action_price_client_docomo_3' => $advert_price_madia_set->getActionPriceClientDocomo3(),
								'action_price_client_softbank_3' => $advert_price_madia_set->getActionPriceClientSoftbank3(),
								'action_price_client_au_3' => $advert_price_madia_set->getActionPriceClientAu3(),
								'action_price_client_pc_3' => $advert_price_madia_set->getActionPriceClientPc3(),
								'action_price_client_docomo_4' => $advert_price_madia_set->getActionPriceClientDocomo4(),
								'action_price_client_softbank_4' => $advert_price_madia_set->getActionPriceClientSoftbank4(),
								'action_price_client_au_4' => $advert_price_madia_set->getActionPriceClientAu4(),
								'action_price_client_pc_4' => $advert_price_madia_set->getActionPriceClientPc4(),
								'action_price_client_docomo_5' => $advert_price_madia_set->getActionPriceClientDocomo5(),
								'action_price_client_softbank_5' => $advert_price_madia_set->getActionPriceClientSoftbank5(),
								'action_price_client_au_5' => $advert_price_madia_set->getActionPriceClientAu5(),
								'action_price_client_pc_5' => $advert_price_madia_set->getActionPriceClientPc5(),
								'action_price_media_docomo_1' => $advert_price_madia_set->getActionPriceMediaDocomo1(),
								'action_price_media_softbank_1' => $advert_price_madia_set->getActionPriceMediaSoftbank1(),
								'action_price_media_au_1' => $advert_price_madia_set->getActionPriceMediaAu1(),
								'action_price_media_pc_1' => $advert_price_madia_set->getActionPriceMediaPc1(),
								'action_price_media_docomo_2' => $advert_price_madia_set->getActionPriceMediaDocomo2(),
								'action_price_media_softbank_2' => $advert_price_madia_set->getActionPriceMediaSoftbank2(),
								'action_price_media_au_2' => $advert_price_madia_set->getActionPriceMediaAu2(),
								'action_price_media_pc_2' => $advert_price_madia_set->getActionPriceMediaPc2(),
								'action_price_media_docomo_3' => $advert_price_madia_set->getActionPriceMediaDocomo3(),
								'action_price_media_softbank_3' => $advert_price_madia_set->getActionPriceMediaSoftbank3(),
								'action_price_media_au_3' => $advert_price_madia_set->getActionPriceMediaAu3(),
								'action_price_media_pc_3' => $advert_price_madia_set->getActionPriceMediaPc3(),
								'action_price_media_docomo_4' => $advert_price_madia_set->getActionPriceMediaDocomo4(),
								'action_price_media_softbank_4' => $advert_price_madia_set->getActionPriceMediaSoftbank4(),
								'action_price_media_au_4' => $advert_price_madia_set->getActionPriceMediaAu4(),
								'action_price_media_pc_4' => $advert_price_madia_set->getActionPriceMediaPc4(),
								'action_price_media_docomo_5' => $advert_price_madia_set->getActionPriceMediaDocomo5(),
								'action_price_media_softbank_5' => $advert_price_madia_set->getActionPriceMediaSoftbank5(),
								'action_price_media_au_5' => $advert_price_madia_set->getActionPriceMediaAu5(),
								'action_price_media_pc_5' => $advert_price_madia_set->getActionPriceMediaPc5());

				// smarty変数へ格納
				$smarty->assign("form_data", $form_data);
			}
		}
		// -------------------------------------------------------------------------------------

	} // set_up 終了

	// ページを表示
	$smarty->display("./advert_price_setup.tpl");
	exit();
}else{
	header('Location: ./login.php?error=1');
	exit();
}

function do_escape_quotes($str){
	//magic_quotesが有効ならクウォート部分を除去
	if(get_magic_quotes_gpc()){
		$str = stripslashes($str);
	}
	return $str;
}

?>