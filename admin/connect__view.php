<?php
// 共通設定
require_once( '../common/CommonAdminBase.php' );
require_once( '../common/CommonDao.php' );
require_once( '../dto/LoginUser.php' );
require_once( '../dao/ConnectLogDao.php' );
require_once( '../dto/ConnectLog.php' );
require_once( '../dao/MediaPublisherDao.php' );
require_once( '../dto/MediaPublisher.php' );
require_once( './class_page_calculate.php' );

session_start();

if(isset($_SESSION['logon_token']) && $_SESSION['logon_token'] != ''){
	$login_user = new LoginUser();
	$login_user = $_SESSION['login_user'];

	// Smartyオブジェクト取得
	$smarty =& getSmartyObj();

	$smarty->assign("title", "Test Top Page");
	$smarty->assign("login_user", $login_user );

// **********************************************************************
// オブジェクト取得
// **********************************************************************
	// DB接続クラス
	$common_dao = new CommonDao();
	// コネクトクラス
	$connect_log_dao = new ConnectLogDao();
	// コネクトGetSetクラス
	$connect_log = new ConnectLog();
	// 媒体発行者クラス
	$media_publisher_dao = new MediaPublisherDao();
	// ページカウントクラス生成
	$c_page_claculate = new C_pageClaculate();

// **********************************************************************
// 変数宣言
// **********************************************************************
	// ページモードを格納
	$mode = "";
	// DB結果を格納
	$db_result = "";
	// SELECT文を格納
	$get_select_sql = "";
	// エラーメッセージ
	$error_message = "";
	// コネクトID
	$connect_id = "";
	// 媒体ID
	$media_id = "";
	// 広告ID
	$advert_id = "";
	// 媒体発行者ID
	$media_publisher_id = "0";
	// 昇順降順
	$order = "ASC";
	// 2:正規 1:承認解除
	$method = "0";
	// 検索条件を格納する配列
	$search = array();
	// GETパラメータ
	$param = "";
	// SELECT LIMIT
	$limit = "0";
	// レコード件数
	$list_count = "0";


// **********************************************************************
// GET及びPOSTより送信されたパラメータの変数へ格納
// **********************************************************************
	//modeの取得
	if(isset($_POST['mode']) && $_POST['mode'] != ""){
		$mode = $_POST['mode'];
	} elseif(isset($_GET['mode']) && $_GET['mode'] != "") {
		$mode = $_GET['mode'];
	}

	// コネクトIDの取得
	if(isset($_POST['id']) && $_POST['id'] != ""){
		$connect_id = $_POST['id'];
	} elseif(isset($_GET['id']) && $_GET['id'] != "") {
		$connect_id = $_GET['id'];
	}

	// 媒体IDの取得
	if(isset($_POST['media_id']) && $_POST['media_id'] != ""){
		$media_id = $_POST['media_id'];
	} elseif(isset($_GET['media_id']) && $_GET['media_id'] != "") {
		$media_id = $_GET['media_id'];
	}

	// 広告IDの取得
	if(isset($_POST['advert_id']) && $_POST['advert_id'] != ""){
		$advert_id = $_POST['advert_id'];
	} elseif(isset($_GET['advert_id']) && $_GET['advert_id'] != "") {
		$advert_id = $_GET['advert_id'];
	}

	// 検索条件 媒体発行者IDの取得
	if(isset($_POST['media_publisher_id']) && $_POST['media_publisher_id'] != ""){
		$media_publisher_id = $_POST['media_publisher_id'];
	} elseif(isset($_GET['media_publisher_id']) && $_GET['media_publisher_id'] != "") {
		$media_publisher_id = $_GET['media_publisher_id'];
	}

	// 検索条件 昇順/降順を取得
	if(isset($_POST['order']) && $_POST['order'] != ""){
		$order = $_POST['order'];
	} elseif(isset($_GET['order']) && $_GET['order'] != "") {
		$order = $_GET['order'];
	}

	// 検索条件 正規 承認待ち 承認解除
	if(isset($_POST['method']) && $_POST['method'] != ""){
		$method = $_POST['method'];
	} elseif(isset($_GET['method']) && $_GET['method'] != "") {
		$method = $_GET['method'];
	}

	// レコードカウントスタート値
	if(isset($_POST['limit']) && $_POST['limit'] != ""){
		$limit = do_escape_quotes($_POST['limit']);
	} elseif(isset($_GET['limit']) && $_GET['limit'] != "") {
		$limit = do_escape_quotes($_GET['limit']);
	}

// **********************************************************************
// 媒体発行者取得
// **********************************************************************
	// 配列の初期化
	$media_publisher_array = array();
	// ループにより媒体発行者名を取得
	foreach($media_publisher_dao->getAllMediaPublisher() as $val){
		$row_array = array('id' => $val->getId(), 'name' => $val->getPublisherName());
		$media_publisher_array[$val->getId()] = $row_array;
	}

	//smarty変数へ配列を格納
	$smarty->assign("media_publisher_array", $media_publisher_array);


// **********************************************************************
// 提携解除
// **********************************************************************
	if($mode == "delete") {

		// コネクトIDが空か
		if($connect_id != ""){

			// テスト出力
			echo $connect_id . "<br />";

			// コネクトIDを条件にデータを取得
			$connect_log = $connect_log_dao->getConnectLogById($connect_id);
			// DB接続結果
			if(!is_null($connect_log)){
				// 取得成功
//				$connect_log->setId($val);
//				$connect_log->setMediaId($val);
//				$connect_log->setAdvertId($val);
				// ステータスを変更 Status:1⇒解除
				$connect_log->setStatus(1);

				// コネクトIDを条件にデータ更新
				$db_result = $connect_log_dao->updateConnectLog($connect_log);
				// DB結果
				if($db_result){
					// DB更新成功
					$smarty->assign("info_message", "提携解除しました。" );

				} else {
					// DB更新失敗
					$error_message .= "ＤＢへのデータの更新に失敗しました。(su0000)";
				}

			} else {
				// 取得失敗
				$error_message .= "ＤＢへのデータの更新に失敗しました。(su0000)";
			}


		} else {
			// エラーメッセージを変数へ格納
			$error_message .= "ＤＢへのデータの更新に失敗しました。(su0000)";
		}


// **********************************************************************
// 提携承認
// **********************************************************************
	} elseif($mode == "edit") {

		// コネクトIDが空か
		if($connect_id != ""){

			// テスト出力
//			echo $connect_id . "<br />";

			// コネクトIDを条件にデータを取得
			$connect_log = $connect_log_dao->getConnectLogById($connect_id);
			// DB接続結果
			if(!is_null($connect_log)){
				// 取得成功
//				$connect_log->setId($val);
//				$connect_log->setMediaId($val);
//				$connect_log->setAdvertId($val);
				// ステータスを変更 Status:2⇒承認
				$connect_log->setStatus(2);

				// コネクトIDを条件にデータ更新
				$db_result = $connect_log_dao->updateConnectLog($connect_log);
				// DB結果
				if($db_result){
					// DB更新成功
					$smarty->assign("info_message", "提携承認しました。" );

				} else {
					// DB更新失敗
					$error_message .= "ＤＢへのデータの更新に失敗しました。(su0000)";
				}

			} else {
				// 取得失敗
				$error_message .= "ＤＢへのデータの更新に失敗しました。(su0000)";
			}


		} else {
			// エラーメッセージを変数へ格納
			$error_message .= "ＤＢへのデータの更新に失敗しました。(su0000)";
		}
	}

// **********************************************************************
// 一覧表示
// **********************************************************************

	// SELECT文 作成
	$get_select_sql = " SELECT "
	. " cl.id, "
	. " mp.publisher_name, "
	. " m.media_name, "
	. " a.advert_name, "
	. " cl.status, "
	. " cl.created_at, "
	. " mp.id AS media_publisher_id, "
	. " m.id AS media_id, "
	. " a.id AS advert_id "
	. " FROM ((( "
	. " connect_logs AS cl "
	. " LEFT JOIN advert AS a ON cl.advert_id = a.id ) "
	. " LEFT JOIN media AS m ON cl.media_id = m.id ) "
	. " LEFT JOIN media_publishers AS mp ON m.media_publisher_id = mp.id ) "
	. " WHERE "
	. " cl.deleted_at is NULL ";


// **********************************************************************
// 検索
// **********************************************************************
	if($mode == "search"){

		// 媒体発行者ID
		if($media_publisher_id != "0"){
			$get_select_sql .= " AND mp.id = '$media_publisher_id' ";
		}

		if($method != "0"){
			$get_select_sql .= " AND cl.status = '$method' ";
		}

	}



	// ORDER BY,LIMITを付与する前にレコードをカウント
	$db_result = $common_dao->db_query($get_select_sql);
	// DB結果
	if($db_result){
		// 取得成功
		// レコード数のカウント
		$list_count = count($db_result);
	} else {
		// 取得失敗
		$error_message .= "ＤＢへのデータの更新に失敗しました。(su0000)";
	}

	// テスト出力
//	echo $get_select_sql . "<br />";

	// SELECT文へORDER BY,LIMITを付与
	$get_select_sql .= " ORDER BY cl.created_at $order "
	. " LIMIT $limit, 100 ";

	// 検索条件を配列へ格納
	// 正規 承認待ち 承認解除
	$search['method'] = $method;
	// 昇順 降順
	$search['order'] = $order;
	// 媒体発行者ID
	$search['media_publisher_id'] = $media_publisher_id;

	// GETパラメータを連結
	$param = "&mode=$mode"
	. "&method=$method"
	. "&order=$order"
	. "&media_publisher_id=$media_publisher_id";

	// smarty変数へ検索条件の配列を格納
	$smarty->assign("search", $search);
	// smarty変数へページカウントを格納
	$smarty->assign("page_count_link", $c_page_claculate->M_getpageClaculate($list_count, $limit, "./connect__view.php", $param));
	// smarty変数へレコードカウントを格納
	$smarty->assign("list_count", $list_count);


	// 一覧表示
	view_list();

}

function view_list(){
	global $common_dao, $smarty, $get_select_sql, $error_message;

	$list_count = 0;

	$db_result = $common_dao->db_query($get_select_sql);
	if($db_result){
		$smarty->assign("list", $db_result);
//		$list_count = count($db_result);
	}else{
		$error_message .= "ＤＢからのデータの取得に失敗しました。(su0000)";
	}
//	$smarty->assign("list_count", $list_count);
	$smarty->assign("error_message", $error_message);

	// ページを表示
	$smarty->display("./cnnect_list.tpl");
	exit();
}

function do_escape_quotes($str){
	//magic_quotesが有効ならクウォート部分を除去
	if(get_magic_quotes_gpc()){
		$str = stripslashes($str);
	}
	return $str;
}
?>