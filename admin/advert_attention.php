<?php
// 共通設定
require_once( '../common/CommonAdminBase.php' );
require_once( '../common/CommonDao.php' );
require_once( '../dto/LoginUser.php' );
//require_once( '../dao/AdvertDao.php' );
//require_once( '../dto/Advert.php' );
require_once( '../dao/AdvertClientDao.php' );
require_once( '../dto/AdvertClient.php' );

require_once( '../dao/AdvertAttentionDao.php' );
require_once( '../dto/AdvertAttention.php' );

session_start();

if(isset($_SESSION['logon_token']) && $_SESSION['logon_token'] != ''){
	$login_user = new LoginUser();
	$login_user = $_SESSION['login_user'];

	// Smartyオブジェクト取得
	$smarty =& getSmartyObj();

	$smarty->assign("title", "Test Top Page");
	$smarty->assign("login_user", $login_user );

	$common_dao = new CommonDao();
//	$advert_dao = new AdvertDao();

	$image_directory = "../advert_image/";

	echo $today = date("Ymd");

	//広告主
	$advert_client_dao = new AdvertClientDao();
	$advert_client_array = array();
	foreach($advert_client_dao->getAllAdvertClient() as $val){
		$row_array = array('id' => $val->getId(), 'name' => $val->getClientName());
		$advert_client_array[$val->getId()] = $row_array;
	}
	$smarty->assign("advert_client_array", $advert_client_array);

//	//広告カテゴリー
//	$advert_category_dao = new AdvertCategoryDao();
//	$advert_category_array = array();
//	foreach($advert_category_dao->getAllAdvertCategory() as $val){
//		$row_array = array('id' => $val->getId(), 'name' => $val->getName());
//		$advert_category_array[$val->getId()] = $row_array;
//	}
//	$smarty->assign("advert_category_array", $advert_category_array);

	if(isset($_GET['mode'])) {
		$mode = $_GET['mode'];
	} elseif(isset($_POST['mode'])) {
		$mode = $_POST['mode'];
	} else {
		$mode = "";
	}


	if(isset($_GET['id'])) {
		$id = $_GET['id'];
	} else {
		$id = do_escape_quotes($_POST['id']);
	}


	if(isset($_POST['advert_client_id']) && $_POST['advert_client_id'] != ""){
		$advert_client_id = do_escape_quotes($_POST['advert_client_id']);
	} else {
		$advert_client_id = "0";
	}

	$advert_category_id = do_escape_quotes($_POST['advert_category_id']);
	$advert_name = do_escape_quotes($_POST['advert_name']);

	$ms_text_type = do_escape_quotes($_POST['ms_text_type']);
	$ms_image_type = do_escape_quotes($_POST['ms_image_type']);

	$unique_click_type = do_escape_quotes($_POST['unique_click_type']);
	$point_back_flag = do_escape_quotes($_POST['point_back_flag']);
	$adult_flag = do_escape_quotes($_POST['adult_flag']);
	$dating_flag = do_escape_quotes($_POST['dating_flag']);
	$as_year = do_escape_quotes($_POST['as_year']);
	$as_month = do_escape_quotes($_POST['as_month']);
	$as_day = do_escape_quotes($_POST['as_day']);
	$ae_year = do_escape_quotes($_POST['ae_year']);
	$ae_month = do_escape_quotes($_POST['ae_month']);
	$ae_day = do_escape_quotes($_POST['ae_day']);
	$unrestraint_flag = do_escape_quotes($_POST['unrestraint_flag']);
	$test_flag = do_escape_quotes($_POST['test_flag']);
	$status = do_escape_quotes($_POST['status']);

	$ms_image_file_path_1 = do_escape_quotes($_POST['ms_image_file_path_1']);
	$ms_image_file_path_2 = do_escape_quotes($_POST['ms_image_file_path_2']);
	$ms_image_file_path_3 = do_escape_quotes($_POST['ms_image_file_path_3']);
	$ms_image_file_path_4 = do_escape_quotes($_POST['ms_image_file_path_4']);
	$ms_image_file_path_5 = do_escape_quotes($_POST['ms_image_file_path_5']);


	$sort = do_escape_quotes($_POST['sort']);
	$order = do_escape_quotes($_POST['order']);

	$created_at_flag = do_escape_quotes($_POST['created_at_flag']);

	$s_year = do_escape_quotes($_POST['s_year']);
	$s_month = do_escape_quotes($_POST['s_month']);
	$s_day = do_escape_quotes($_POST['s_day']);
	$e_year = do_escape_quotes($_POST['e_year']);
	$e_month = do_escape_quotes($_POST['e_month']);
	$e_day = do_escape_quotes($_POST['e_day']);

	$advert_start_date = "$as_year-$as_month-$as_day 00:00:00";
	$advert_end_date = "$ae_year-$ae_month-$ae_day 23:59:59";

	$three_last_month = getdate(strtotime("-3 month"));
	$now_date = getdate();

	$smarty->assign("set_as_year", $now_date['year']);
	$smarty->assign("set_as_month", $now_date['mon']);
	$smarty->assign("set_as_day", $now_date['mday']);
	$smarty->assign("set_ae_year", $now_date['year']);
	$smarty->assign("set_ae_month", $now_date['mon']);
	$smarty->assign("set_ae_day", $now_date['mday']);

	$smarty->assign("set_s_year", $three_last_month['year']);
	$smarty->assign("set_s_month", $three_last_month['mon']);
	$smarty->assign("set_s_day", $three_last_month['mday']);
	$smarty->assign("set_e_year", $now_date['year']);
	$smarty->assign("set_e_month", $now_date['mon']);
	$smarty->assign("set_e_day", $now_date['mday']);

// ---------------------------------------------------------------------------------------- 2011/02/01 修正


	if($mode != ''){
// **************************************************************************
// 新規
// **************************************************************************
		if($mode == 'new_regist'){
			$smarty->assign("mode", 'new_regist_client');
			$smarty->assign("sub_title", '新規追加');

			// ページを表示
			$smarty->display("./advert_attention_input.tpl");
			exit();
// **************************************************************************
// 新規 広告名選択
// **************************************************************************
		} elseif($mode == 'new_regist_client') {

			$list_sql = " SELECT a.*, ac.client_name as advert_client_name "
				. " FROM advert as a "
				. " left join advert_clients as ac on a.advert_client_id = ac.id "
				. " WHERE a.deleted_at is NULL "
				. " AND "
				. " ac.id = $advert_client_id "
				. " AND "
				. " a.status = '2' ";

			// テスト出力
//			echo $list_sql . "<hr />";

			$db_result = $common_dao->db_query_ts($list_sql);
			if($db_result) {
				$advert_id_array = $db_result;

				$smarty->assign("advert_client_id", $advert_client_id);
				$smarty->assign("advert_id_array", $advert_id_array);
				$smarty->assign("mode", 'new_regist_advert');
				$smarty->assign("sub_title", '新規追加');
			}else{
				$error_message .= "正規の広告がありません。";
				$smarty->assign("error_message", $error_message);
				$smarty->assign("advert_client_id", $advert_client_id);
				$smarty->assign("mode", 'new_regist_client');
				$smarty->assign("sub_title", '新規追加');
			}
			// ページを表示
			$smarty->display("./advert_attention_input.tpl");
			exit();

// **************************************************************************
// 新規 広告名選択後 詳細表示
// **************************************************************************
		}elseif($mode == 'new_regist_advert'){

			$list_sql = " SELECT a.*, ac.client_name as advert_client_name "
				. " FROM advert as a "
				. " left join advert_clients as ac on a.advert_client_id = ac.id "
				. " WHERE a.deleted_at is NULL "
				. " AND "
				. " ac.id = $advert_client_id "
				. " AND "
				. " a.status = '2' ";

			// テスト出力
//			echo $list_sql . "<hr />";

			$db_result = $common_dao->db_query_ts($list_sql);
			if($db_result) {
				$advert_id_array = $db_result;
				$smarty->assign("advert_id_array", $advert_id_array);
				$smarty->assign("advert_client_id", $advert_client_id);
			} else {
				$error_message .= "ＤＢからのデータの取得に失敗しました。(su0000)";
				$smarty->assign("error_message", $error_message);
				$smarty->assign("sub_title", '新規追加');
			}


			$list_sql = " SELECT a.*, ac.client_name as advert_client_name, aca.name AS advert_categories_name "
				. " FROM advert as a "
				. " left join advert_clients as ac on a.advert_client_id = ac.id "
				. " LEFT JOIN advert_categories AS aca ON a.advert_category_id = aca.id "
				. " WHERE a.deleted_at is NULL "
				. " AND "
				. " ac.id = $advert_client_id "
				. " AND "
				. " a.id= $id ";

			// テスト出力
//			echo $list_sql . "<hr />";

			$db_result = $common_dao->db_query_ts($list_sql);
			if($db_result) {
				$form_data = $db_result[0];

				$smarty->assign("advert_id", $id);
				$smarty->assign("form_data", $form_data);
				$smarty->assign("mode", 'new_regist_data');
				$smarty->assign("sub_title", '新規追加');
			}else{
				$error_message .= "ＤＢからのデータの取得に失敗しました。(su0000)";
				$smarty->assign("error_message", $error_message);
				$smarty->assign("advert_id", $id);
				$smarty->assign("mode", 'new_regist_advert');
				$smarty->assign("sub_title", '新規追加');
			}
			// ページを表示
			$smarty->display("./advert_attention_input.tpl");
			exit();



// **************************************************************************
// 新規 追加処理
// **************************************************************************
		}elseif($mode == 'insert_commit'){

			$advert_attention_dao = new AdvertAttentionDao();
			$advert_attention = new AdvertAttention();

			// データをレコードに設定
			$advert_attention->setAdvertId($id);
			$advert_attention->setMsImageUrlNum($ms_image_type);
			$advert_attention->setMsTextNum($ms_text_type);

			// insert
			$db_result = $advert_attention_dao->insertAdvertAttention($advert_attention);
			if($db_result){
				// insert成功
				$smarty->assign("info_message", "データを追加しました。");
			} else {
				// insert失敗
				$error_message .= "ＤＢからのデータの追加に失敗しました。(su0000)";
				$smarty->assign("error_message", $error_message);
			}

			view_list();

// **************************************************************************
// 編集
// **************************************************************************
		}elseif($mode == 'edit'){

			$advert_attention_dao = new AdvertAttentionDao();
			$advert_attention = new AdvertAttention();

			// IDを条件にデータを取得
			$advert_attention = $advert_attention_dao->getAdvertAttentionById($id);

			if($advert_attention){

				$list_sql = " SELECT a.*, ac.client_name as advert_client_name, aca.name AS advert_categories_name "
				. " FROM advert as a "
				. " left join advert_clients as ac on a.advert_client_id = ac.id "
				. " LEFT JOIN advert_categories AS aca ON a.advert_category_id = aca.id "
				. " WHERE a.deleted_at is NULL "
				. " AND "
				. " a.id= '" . $advert_attention->getAdvertId() . "' ";

				// テスト出力
//				echo $list_sql;


				$db_result = $common_dao->db_query_ts($list_sql);
				if($db_result) {
					$form_data = $db_result[0];
					$form_data['ms_image_url_num'] = $advert_attention->getMsImageUrlNum();
					$form_data['ms_text_num'] = $advert_attention->getMsTextNum();
					$form_data['advert_attention_id'] = $advert_attention->getId();

					$smarty->assign("mode", 'edit');
					$smarty->assign("form_data", $form_data);
				} else {
					$error_message .= "ＤＢからのデータの取得に失敗しました。(su0000)";
					$smarty->assign("error_message", $error_message);
				}


			} else {
				$error_message .= "ＤＢからのデータの取得に失敗しました。(su0000)";
				$smarty->assign("error_message", $error_message);
			}

			$smarty->display("./advert_attention_input.tpl");
			exit();


// **************************************************************************
// 編集 更新処理
// **************************************************************************
		}elseif($mode == 'update_commit'){

			$advert_attention_dao = new AdvertAttentionDao();
			$advert_attention = new AdvertAttention();

			// データをレコードに設定
			$advert_attention->setId($id);
			$advert_attention->setMsImageUrlNum($ms_image_type);
			$advert_attention->setMsTextNum($ms_text_type);

			// update
			$db_result = $advert_attention_dao->updateAdvertAttention($advert_attention);

			if($db_result){
				// insert成功
				$smarty->assign("info_message", "データを更新しました。");
			} else {
				// insert失敗
				$error_message .= "ＤＢからのデータの更新に失敗しました。(su0001)";
				$smarty->assign("error_message", $error_message);
			}

			view_list();

// **************************************************************************
// 検索
// **************************************************************************
		}elseif($mode == 'search'){

			$search_list_sql = "";

			$search_list_sql = " SELECT "
			. " aa.* "
			. " FROM (("
			. " advert_attention AS aa "
			. " LEFT JOIN advert AS a ON aa.advert_id = a.id )"
			. " LEFT JOIN advert_clients AS ac ON a.advert_client_id = ac.id )"
			. " WHERE "
			. " aa.deleted_at is NULL ";
//			. " AND "
//			. " a.status = '2' ";

			// 広告主 検索
			if($advert_client_id != "0"){
				$search_list_sql .= " AND a.advert_client_id = '$advert_client_id' ";
			}
			$smarty->assign("advert_client_id_num", $advert_client_id);

			// 日付 検索
			if($created_at_flag == 1) {

				$search_list_sql .= "AND aa.created_at BETWEEN '$s_year-$s_month-$s_day 00:00:00' AND '$e_year-$e_month-$e_day 23:59:59' ";

				$smarty->assign("set_s_year", $s_year);
				$smarty->assign("set_s_month", $s_month);
				$smarty->assign("set_s_day", $s_day);
				$smarty->assign("set_e_year", $e_year);
				$smarty->assign("set_e_month", $e_month);
				$smarty->assign("set_e_day", $e_day);
			}

			// ソート
			if($sort == 'advert_name'){
				$search_list_sql .= "ORDER BY a.$sort $order ";
			} elseif($sort == 'created_at') {
				$search_list_sql .= "ORDER BY aa.$sort $order ";
			}

			$search['sort'] = $sort;
			$search['order'] = $order;
			$search['created_at_flag'] = $created_at_flag;

			$smarty->assign("search", $search);
			$smarty->assign("mode", 'search');

			$smarty->assign("sub_title", '検索結果');

			// テスト出力
//			echo $search_list_sql;

			view_list();


// **************************************************************************
// 削除
// **************************************************************************
		}elseif($_POST['mode'] == 'delete'){

			$advert_attention_dao = new AdvertAttentionDao();
			$advert_attention = new AdvertAttention();


			if(!is_null($advert_attention_dao->getAdvertAttentionById($id))) {
				// SELECT成功
				$db_result = $advert_attention_dao->deleteAdvertAttention($id);
				if($db_result){
					// 更新成功
					$smarty->assign("info_message", "削除しました。");

				}else{
					// 更新失敗
					$error_message = "ＤＢからのデータの更新に失敗しました。(su0001)";
					$smarty->assign("error_message", $result_message);
				}
			}else{
				// SELECT失敗
				$error_message = "ＤＢからのデータの更新に失敗しました。(su0001)";
				$smarty->assign("error_message", $error_message);

			}
			view_list();

		}else{
// **************************************************************************
// modeが全て該当しない場合一覧を表示
// **************************************************************************
			view_list();
		}
	}else{
// **************************************************************************
// 初期表示 一覧
// **************************************************************************
		view_list();
	}
}else{
	header('Location: ./login.php?error=1');
	exit();
}

function view_list(){
	global $common_dao, $smarty, $mode, $search_list_sql, $error_message;

	$list_count = 0;
	$advert_attention = array();

		// おすすめ広告テーブルのレコードを取得

		// 全件取得
		if($mode == 'search'){
			$advert_attention_sql = $search_list_sql;

		} else {
//			$advert_attention_sql = " SELECT "
//			. " * "
//			. " FROM "
//			. " advert_attention "
//			. " WHERE "
//			. " deleted_at is NULL ";

			$advert_attention_sql = " SELECT "
			. " aa.* "
			. " FROM (("
			. " advert_attention AS aa "
			. " LEFT JOIN advert AS a ON aa.advert_id = a.id )"
			. " LEFT JOIN advert_clients AS ac ON a.advert_client_id = ac.id )"
			. " WHERE "
			. " aa.deleted_at is NULL ";
//			. " AND "
//			. " a.status = '2' ";

		}

		// テスト出力
		//echo $advert_attention_sql . "<br />";

		$rec2 = $common_dao->db_query_ts($advert_attention_sql);

		$list_count = count($rec2);

		foreach( $rec2 as $key => $value ){

			$advert_sql = " SELECT ";

			// イメージタイプ1
			if($value['ms_image_url_num'] == "1"){
				$advert_sql .= " a.ms_image_url_1 AS ms_image_url, ";
			}
			// イメージタイプ1
			if($value['ms_image_url_num'] == "2"){
				$advert_sql .= " a.ms_image_url_2 AS ms_image_url, ";
			}
			// イメージタイプ1
			if($value['ms_image_url_num'] == "3"){
				$advert_sql .= " a.ms_image_url_3 AS ms_image_url, ";
			}
			// イメージタイプ1
			if($value['ms_image_url_num'] == "4"){
				$advert_sql .= " a.ms_image_url_4 AS ms_image_url, ";
			}

			// テキストタイプ1
			if($value['ms_image_url_num'] == "5"){
				$advert_sql .= " a.ms_image_url_5 AS ms_image_url, ";
			}
			// テキストタイプ1
			if($value['ms_text_num'] == "1"){
				$advert_sql .= " a.ms_text_1 AS ms_text, ";
			}
			// テキストタイプ1
			if($value['ms_text_num'] == "2"){
				$advert_sql .= " a.ms_text_2 AS ms_text, ";
			}
			// テキストタイプ1
			if($value['ms_text_num'] == "3"){
				$advert_sql .= " a.ms_text_3 AS ms_text, ";
			}
			// テキストタイプ1
			if($value['ms_text_num'] == "4"){
				$advert_sql .= " a.ms_text_4 AS ms_text, ";
			}
			// テキストタイプ1
			if($value['ms_text_num'] == "5"){
				$advert_sql .= " a.ms_text_5 AS ms_text, ";
			}

			$advert_sql .= " aa.id, "
			. " a.advert_name, "
			. " ac.client_name AS advert_client_name, "
			. " a.status, "
			. " aa.created_at "
			. " FROM (("
			. " advert AS a "
			. " LEFT JOIN advert_clients AS ac ON a.advert_client_id = ac.id )"
			. " LEFT JOIN advert_attention AS aa ON a.id = aa.advert_id ) "
			. " WHERE "
			. " a.id = '" . $value['advert_id'] . "' "
			. " AND "
			. " aa.id = '" . $value['id'] . "' "
			. " AND "
			. " aa.deleted_at is NULL ";

			$rec3 = $common_dao->db_query_ts($advert_sql);

			$advert_attention[] = $rec3[0];

		}

		$smarty->assign("list_count", $list_count);
		$smarty->assign("list", $advert_attention);
		$smarty->assign("images_dir_path", "../advert_image/");

		$smarty->display("./advert_attention_list.tpl");
		exit();

}

function do_escape_quotes($str){
	//magic_quotesが有効ならクウォート部分を除去
	if(get_magic_quotes_gpc()){
		$str = stripslashes($str);
	}
	return $str;
}

?>