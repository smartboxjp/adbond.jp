<?php
//
//   update_view_status.php
//   DATA:2010/04/30 Y.Sakamaki
//
//-------------------------------------------

// 共通設定
require_once( '../common/CommonDao.php'  );

$debug_flag = 1;

$common_dao = new CommonDao();

if($_GET['day'] != '' && $_GET['lday'] != ''){
	$today = $common_dao->db_string_escape($_GET['day']);
	$last_day = $common_dao->db_string_escape($_GET['lday']);
}else{
	$today = date("Ymd");
	$last_day = date("Ymd", strtotime("-1 day"));
}

echo date("Ymd", strtotime("-1 day"));


	// advert
	$off_advert_sql = " update advert set status = 3 "
				   . " where advert_end_date = '$last_day' and unrestraint_flag = 0 and status = 2 and deleted_at is NULL ";
//echo $off_advert_sql;
	$db_off_advert_result = $common_dao->db_update($off_advert_sql);
	if($db_off_advert_result) {

		if($debug_flag){
			$insert_sql = " insert into system_logs values( "
						. " '', 200, 'advert off OK ($db_off_advert_result)', Now() )";
			//echo $insert_sql;
			$db_insert_result = $common_dao->db_update($insert_sql);
			if($db_insert_result) {

			}else{

			}
		}
	}else{

	}

	$on_advert_sql = " update advert set status = 2 "
				  . " where advert_start_date = '$today' and status = 1 and deleted_at is NULL ";
//echo $on_advert_sql;
	$db_on_advert_result = $common_dao->db_update($on_advert_sql);
	if($db_on_advert_result) {

		if($debug_flag){
			$insert_sql = " insert into system_logs values( "
						. " '', 200, 'advert on OK ($db_on_advert_result)', Now() )";
			//echo $insert_sql;
			$db_insert_result = $common_dao->db_update($insert_sql);
			if($db_insert_result) {

			}else{

			}
		}
	}else{

	}
?>
