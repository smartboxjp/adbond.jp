<?php
// 2010/12/16 新規作成
// TSC T.Akagawa
// お問い合わせ一覧


// 共通設定
require_once( '../common/CommonAdminBase.php' );
require_once( '../common/CommonDao.php' );
require_once( '../dto/LoginUser.php' );
require_once( '../dao/UserContactDao.php' );
require_once( '../dto/UserContact.php' );
require_once( '../dao/PrefDao.php' );
require_once( '../dto/Pref.php' );

session_start();

if(isset($_SESSION['logon_token']) && $_SESSION['logon_token'] != ''){
	$login_user = new LoginUser();
	// ログインユーザー
	$login_user = $_SESSION['login_user'];
	// 画面状態
	$mode = $_POST['mode'];
	// id
	$contact_id = $_POST['contact_id'];

	$user_contact_dao = new UserContactDao();

	// SQL文
	$list_sql = " SELECT * FROM user_contact ";

	// Smartyオブジェクト取得
	$smarty =& getSmartyObj();

	$smarty->assign("title", "Test Top Page");
	$smarty->assign("login_user", $login_user );

	$common_dao = new CommonDao();
	$pref_dao = new PrefDao();

	if(isset($mode) && $mode == "detail"){
		$user_contact = new UserContact();
		$user_contact = $user_contact_dao->getUserContactId($contact_id);

		$pref_data = $pref_dao->getPrefById($user_contact->getPref());

		if($user_contact){
			$form_data = array('id' => $user_contact->getId(),
								'user_name' => $user_contact->getUserName(),
								'pref' => $pref_data->getName(),
								'address1' => $user_contact->getAddress1(),
								'address2' => $user_contact->getAddress2(),
								'tel' => $user_contact->getTel(),
								'email' => $user_contact->getEmail(),
								'textarea' => $user_contact->getTextarea(),
								'commit_at' => $user_contact->getCommitAt());

			$smarty->assign('form_data', $form_data);

			$smarty->display("./contact_detail.tpl");
			exit();
		}
	} else {
		// お問い合わせデータの一覧を取得
		view_list();
		// ページを表示
		$smarty->display("./contact_view.tpl");
		exit();
	}
}else{
	header('Location: ./login.php?error=1');
	exit();
}

// 一覧データを取得
function view_list(){
	global $common_dao, $smarty, $list_sql, $error_message;

	$list_count = 0;

	$db_result = $common_dao->db_query($list_sql);
	if($db_result){
		$smarty->assign("list", $db_result);
		$list_count = count($db_result);
	}else{
		$error_message .= "ＤＢからのデータの取得に失敗しました。(su0000)";
	}
	$smarty->assign("list_count", $list_count);
	$smarty->assign("error_message", $error_message);

	// ページを表示
	$smarty->display("./contact_view.tpl");
	exit();
}

function do_escape_quotes($str){
	//magic_quotesが有効ならクウォート部分を除去
	if(get_magic_quotes_gpc()){
		$str = stripslashes($str);
	}
	return $str;
}
?>