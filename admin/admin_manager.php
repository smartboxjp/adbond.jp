<?php
// 2010/12/16 新規作成
// TSC T.Akagawa
// お問い合わせ一覧


// 共通設定
require_once( '../common/CommonAdminBase.php' );
require_once( '../common/CommonDao.php' );
require_once( '../dto/LoginUser.php' );
require_once( '../dao/UserContactDao.php' );
require_once( '../dto/UserContact.php' );
require_once( '../dao/PrefDao.php' );
require_once( '../dto/Pref.php' );

session_start();

// ログインチェック
if(isset($_SESSION['logon_token']) && $_SESSION['logon_token'] != ''){



	// LoginUserクラス生成
	$login_user = new LoginUser();
	// session変数よりログインユーザーを取得
	$login_user = $_SESSION['login_user'];

	// POST送信で受け取ったパラメータ 画面状態を取得
	if(isset($_POST['mode']) && $_POST['mode'] != ""){
		$mode = $_POST['mode'];
	} else {
		$mode = "";
	}

	if(isset($_POST['login_class']) && $_POST['login_class'] != ""){
		$login_class = $_POST['login_class'];
	}

	if(isset($_POST['login_id']) && $_POST['login_id'] != ""){
		$login_id = $_POST['login_id'];
	}

	if(isset($_POST['login_pass']) && $_POST['login_pass'] != ""){
		$login_pass = $_POST['login_pass'];
	}
	if(isset($_POST['id']) && $_POST['id'] != ""){
		$id = $_POST['id'];
	}

	// Smartyオブジェクト取得
	$smarty =& getSmartyObj();
	// DB接続クラスの生成
	$common_dao = new CommonDao();

	if($mode == "detail"){

		// update文発行
		$up_sql = " UPDATE login_users SET "
		. " login_class = '$login_class', "
		. " login_id = '$login_id', "
		. " login_pass = '$login_pass' "
		. " WHERE "
		. " id = $id";

		// テスト出力
		echo $up_sql;
		echo "<br />";

		// DB接続
		$db_result = $common_dao->db_update($up_sql);
		if($db_result){
			// 更新成功
			// テスト出力
			echo "成功";
		} else {
			// 更新失敗
			$error_message .= "DBの更新に失敗しました。";
		}

	}

	// login_usersテーブルのSELECT文を発行
	$list_sql = " SELECT * FROM login_users ";

	view_list();
	exit();

}else{
	header('Location: ./login.php?error=1');
	exit();
}

// 一覧データを取得
function view_list(){
	// グローバル変数を宣言
	global $common_dao, $smarty, $list_sql, $error_message;

	// カウント0初期化
	$list_count = 0;

	// DB接続
	$db_result = $common_dao->db_query($list_sql);
	// DB結果
	if($db_result){
		// 該当レコードが存在する
		// Smarty変数へ
		$smarty->assign("list", $db_result);
		// 取得したレコード数をカウント
		$list_count = count($db_result);
	}else{
		// 該当レコードが存在しない
		// エラーメッセージを発行
		$error_message .= "ＤＢからのデータの取得に失敗しました。(su0000)";
	}

	// レコード数をSmasrty変数へ格納
	$smarty->assign("list_count", $list_count);
	// エラーメッセージをSmarty変数へ格納
	$smarty->assign("error_message", $error_message);

	// ページを表示
	$smarty->display("./admin_manager.tpl");
	exit();
}

function do_escape_quotes($str){
	//magic_quotesが有効ならクウォート部分を除去
	if(get_magic_quotes_gpc()){
		$str = stripslashes($str);
	}
	return $str;
}
?>