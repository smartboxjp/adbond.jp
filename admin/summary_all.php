<?php
// 共通設定
require_once( '../common/CommonAdminBase.php' );
require_once( '../common/CommonDao.php' );
require_once( '../dto/LoginUser.php' );

session_start();

if(isset($_SESSION['logon_token']) && $_SESSION['logon_token'] != ''){
	$login_user = new LoginUser();
	$login_user = $_SESSION['login_user'];

	// Smartyオブジェクト取得
	$smarty =& getSmartyObj();

	$smarty->assign("title", "Test Top Page");
	$smarty->assign("login_user", $login_user );

	$common_dao = new CommonDao();

	$sql = " SELECT IF(status <> 1, DATE_FORMAT(action_complete_date,'%Y年%m月'), DATE_FORMAT(created_at,'%Y年%m月')) as date_string, "
			. " IF(status <> 1, DATE_FORMAT(action_complete_date,'%Y%m'), DATE_FORMAT(created_at,'%Y%m')) as summary_month, "
			. " SUM(click_price_client) as click_price_client, "
			. " SUM(click_price_media) as click_price_media, "
			. " SUM(IF(status = 2 OR status = 4, action_price_client, NULL)) as action_price_client, "
//			. " SUM(IF(status = 2 OR status = 4 OR status = 3, action_price_media, NULL)) as action_price_media, "
			. " SUM(IF(status <> 1, action_price_media, NULL)) as action_price_media, "
			. " COUNT(status) as click_count, "
			. " COUNT(IF(status <> 1, status, NULL)) as action_count "
			. " FROM action_logs "
			. " WHERE deleted_at is NULL "
			// status1=クリック status2=アクション status3=メディアのみ成果反映 status4=クライアントのみ成果反映
			. " AND (status = 1 OR status = 2 OR status = 3 OR status = 4) "
			. " GROUP BY summary_month "
			//. " ORDER BY created_at DESC ";
			. " ORDER BY summary_month DESC ";

//	// テスト出力
//	echo $sql;

	$summary = array();

	$db_result = $common_dao->db_query($sql);
	if($db_result){
		foreach($db_result as $row) {
			$summary[$row['summary_month']]['summary_date'] = $row['date_string'];
			$summary[$row['summary_month']]['month'] = $row['summary_month'];

			$summary[$row['summary_month']]['sales'] = $row['click_price_client'] + $row['action_price_client'];
			$summary[$row['summary_month']]['amounts'] = $row['click_price_media'] + $row['action_price_media'];
			$summary[$row['summary_month']]['fees'] = ($row['click_price_client'] + $row['action_price_client']) - ($row['click_price_media'] + $row['action_price_media']);
			$summary[$row['summary_month']]['click_count'] = $row['click_count'];
			$summary[$row['summary_month']]['action_count'] = $row['action_count'];
		}
	}else{
		$error_message .= "ＤＢからのデータの取得に失敗しました。(su0000)";
	}

	$smarty->assign("list", $summary);
	$list_count = count($summary);
	$smarty->assign("list_count", $list_count);
	$smarty->assign("error_message", $error_message);

	// ページを表示
	$smarty->display("./summary_all.tpl");
	exit();
}else{
	header('Location: ./login.php?error=1');
	exit();
}
?>