<?php
// 共通設定
require_once( '../common/CommonAdminBase.php' );
require_once( '../common/CommonDao.php' );
require_once( '../dto/LoginUser.php' );
require_once( '../dao/AdvertDao.php' );
require_once( '../dto/Advert.php' );
require_once( '../dao/AdvertClientDao.php' );
require_once( '../dto/AdvertClient.php' );
require_once( '../dao/AdvertCategoryDao.php' );
require_once( '../dto/AdvertCategory.php' );
require_once( '../dao/AdvertReserveDao.php' );
require_once( '../dto/AdvertReserve.php' );

session_start();

if(isset($_SESSION['logon_token']) && $_SESSION['logon_token'] != ''){
	$login_user = new LoginUser();
	$login_user = $_SESSION['login_user'];

	// Smartyオブジェクト取得
	$smarty =& getSmartyObj();

	$smarty->assign("title", "Test Top Page");
	$smarty->assign("login_user", $login_user );

	$common_dao = new CommonDao();
	$advert_dao = new AdvertDao();

	$image_directory = "../advert_image/";

	//広告主
	$advert_client_dao = new AdvertClientDao();
	$advert_client_array = array();
	foreach($advert_client_dao->getAllAdvertClient() as $val){
		$row_array = array('id' => $val->getId(), 'name' => $val->getClientName());
		$advert_client_array[$val->getId()] = $row_array;
	}
	$smarty->assign("advert_client_array", $advert_client_array);

	//広告カテゴリー
	$advert_category_dao = new AdvertCategoryDao();
	$advert_category_array = array();
	foreach($advert_category_dao->getAllAdvertCategory() as $val){
		$row_array = array('id' => $val->getId(), 'name' => $val->getName());
		$advert_category_array[$val->getId()] = $row_array;
	}
	$smarty->assign("advert_category_array", $advert_category_array);

	$id = do_escape_quotes($_POST['id']);
	$advert_id = do_escape_quotes($_POST['advert_id']);
	$support_docomo = do_escape_quotes($_POST['support_docomo']);
	$support_softbank = do_escape_quotes($_POST['support_softbank']);
	$support_au = do_escape_quotes($_POST['support_au']);
	$support_pc = do_escape_quotes($_POST['support_pc']);
	$site_url_docomo = do_escape_quotes($_POST['site_url_docomo']);
	$site_url_softbank = do_escape_quotes($_POST['site_url_softbank']);
	$site_url_au = do_escape_quotes($_POST['site_url_au']);
	$site_url_pc = do_escape_quotes($_POST['site_url_pc']);
	$click_price_client = do_escape_quotes($_POST['click_price_client']);
	$click_price_media = do_escape_quotes($_POST['click_price_media']);
	$action_price_client_docomo_1 = do_escape_quotes($_POST['action_price_client_docomo_1']);
	$action_price_client_softbank_1 = do_escape_quotes($_POST['action_price_client_softbank_1']);
	$action_price_client_au_1 = do_escape_quotes($_POST['action_price_client_au_1']);
	$action_price_client_pc_1 = do_escape_quotes($_POST['action_price_client_pc_1']);
	$action_price_client_docomo_2 = do_escape_quotes($_POST['action_price_client_docomo_2']);
	$action_price_client_softbank_2 = do_escape_quotes($_POST['action_price_client_softbank_2']);
	$action_price_client_au_2 = do_escape_quotes($_POST['action_price_client_au_2']);
	$action_price_client_pc_2 = do_escape_quotes($_POST['action_price_client_pc_2']);
	$action_price_client_docomo_3 = do_escape_quotes($_POST['action_price_client_docomo_3']);
	$action_price_client_softbank_3 = do_escape_quotes($_POST['action_price_client_softbank_3']);
	$action_price_client_au_3 = do_escape_quotes($_POST['action_price_client_au_3']);
	$action_price_client_pc_3 = do_escape_quotes($_POST['action_price_client_pc_3']);
	$action_price_client_docomo_4 = do_escape_quotes($_POST['action_price_client_docomo_4']);
	$action_price_client_softbank_4 = do_escape_quotes($_POST['action_price_client_softbank_4']);
	$action_price_client_au_4 = do_escape_quotes($_POST['action_price_client_au_4']);
	$action_price_client_pc_4 = do_escape_quotes($_POST['action_price_client_pc_4']);
	$action_price_client_docomo_5 = do_escape_quotes($_POST['action_price_client_docomo_5']);
	$action_price_client_softbank_5 = do_escape_quotes($_POST['action_price_client_softbank_5']);
	$action_price_client_au_5 = do_escape_quotes($_POST['action_price_client_au_5']);
	$action_price_client_pc_5 = do_escape_quotes($_POST['action_price_client_pc_5']);
	$action_price_media_docomo_1 = do_escape_quotes($_POST['action_price_media_docomo_1']);
	$action_price_media_softbank_1 = do_escape_quotes($_POST['action_price_media_softbank_1']);
	$action_price_media_au_1 = do_escape_quotes($_POST['action_price_media_au_1']);
	$action_price_media_pc_1 = do_escape_quotes($_POST['action_price_media_pc_1']);
	$action_price_media_docomo_2 = do_escape_quotes($_POST['action_price_media_docomo_2']);
	$action_price_media_softbank_2 = do_escape_quotes($_POST['action_price_media_softbank_2']);
	$action_price_media_au_2 = do_escape_quotes($_POST['action_price_media_au_2']);
	$action_price_media_pc_2 = do_escape_quotes($_POST['action_price_media_pc_2']);
	$action_price_media_docomo_3 = do_escape_quotes($_POST['action_price_media_docomo_3']);
	$action_price_media_softbank_3 = do_escape_quotes($_POST['action_price_media_softbank_3']);
	$action_price_media_au_3 = do_escape_quotes($_POST['action_price_media_au_3']);
	$action_price_media_pc_3 = do_escape_quotes($_POST['action_price_media_pc_3']);
	$action_price_media_docomo_4 = do_escape_quotes($_POST['action_price_media_docomo_4']);
	$action_price_media_softbank_4 = do_escape_quotes($_POST['action_price_media_softbank_4']);
	$action_price_media_au_4 = do_escape_quotes($_POST['action_price_media_au_4']);
	$action_price_media_pc_4 = do_escape_quotes($_POST['action_price_media_pc_4']);
	$action_price_media_docomo_5 = do_escape_quotes($_POST['action_price_media_docomo_5']);
	$action_price_media_softbank_5 = do_escape_quotes($_POST['action_price_media_softbank_5']);
	$action_price_media_au_5 = do_escape_quotes($_POST['action_price_media_au_5']);
	$action_price_media_pc_5 = do_escape_quotes($_POST['action_price_media_pc_5']);
	$ms_text_1 = do_escape_quotes($_POST['ms_text_1']);
	$ms_email_1 = do_escape_quotes($_POST['ms_email_1']);
	$ms_image_type_1 = do_escape_quotes($_POST['ms_image_type_1']);
	$ms_image_url_1 = do_escape_quotes($_POST['ms_image_url_1']);
	$ms_text_2 = do_escape_quotes($_POST['ms_text_2']);
	$ms_email_2 = do_escape_quotes($_POST['ms_email_2']);
	$ms_image_type_2 = do_escape_quotes($_POST['ms_image_type_2']);
	$ms_image_url_2 = do_escape_quotes($_POST['ms_image_url_2']);
	$ms_text_3 = do_escape_quotes($_POST['ms_text_3']);
	$ms_email_3 = do_escape_quotes($_POST['ms_email_3']);
	$ms_image_type_3 = do_escape_quotes($_POST['ms_image_type_3']);
	$ms_image_url_3 = do_escape_quotes($_POST['ms_image_url_3']);
	$ms_text_4 = do_escape_quotes($_POST['ms_text_4']);
	$ms_email_4 = do_escape_quotes($_POST['ms_email_4']);
	$ms_image_type_4 = do_escape_quotes($_POST['ms_image_type_4']);
	$ms_image_url_4 = do_escape_quotes($_POST['ms_image_url_4']);
	$ms_text_5 = do_escape_quotes($_POST['ms_text_5']);
	$ms_email_5 = do_escape_quotes($_POST['ms_email_5']);
	$ms_image_type_5 = do_escape_quotes($_POST['ms_image_type_5']);
	$ms_image_url_5 = do_escape_quotes($_POST['ms_image_url_5']);

	$r_year = do_escape_quotes($_POST['r_year']);
	$r_month = do_escape_quotes($_POST['r_month']);
	$r_day = do_escape_quotes($_POST['r_day']);

	$reserve_change_date = "$r_year-$r_month-$r_day";

	$next_day = getdate(strtotime("+1 day"));

	$smarty->assign("set_r_year", $next_day['year']);
	$smarty->assign("set_r_month", $next_day['mon']);
	$smarty->assign("set_r_day", $next_day['mday']);

	$list_sql = " SELECT ar.*, a.advert_name as advert_name, ac.client_name as advert_client_name "
				. " FROM advert_reserve as ar "
				. " LEFT JOIN advert as a on ar.advert_id = a.id "
				. " LEFT JOIN advert_clients as ac on a.advert_client_id = ac.id "
				. " WHERE ar.deleted_at is NULL "
				. " ORDER BY ar.id ASC ";

	if(isset($_POST['mode']) && $_POST['mode'] != ''){
		$advert = new Advert;
		$advert = $advert_dao->getAdvertById($advert_id);

		if(!is_null($advert)) {
			$form_data = array('id' => $advert->getId(),
								'advert_id' => $advert_id,
								'advert_client_id' => $advert->getAdvertClientId(),
								'advert_category_id' => $advert->getAdvertCategoryId(),
								'advert_name' => $advert->getAdvertName(),
								'content_type' => $advert->getContentType(),
								'support_docomo' => $advert->getSupportDocomo(),
								'support_softbank' => $advert->getSupportSoftbank(),
								'support_au' => $advert->getSupportAu(),
								'support_pc' => $advert->getSupportPc(),
								'site_url_docomo' => $advert->getSiteUrlDocomo(),
								'site_url_softbank' => $advert->getSiteUrlSoftbank(),
								'site_url_au' => $advert->getSiteUrlAu(),
								'site_url_pc' => $advert->getSiteUrlPc(),
								'site_outline' => $advert->getSiteOutline(),
								'click_price_client' => $advert->getClickPriceClient(),
								'click_price_media' => $advert->getClickPriceMedia(),
								'action_price_client_docomo_1' => $advert->getActionPriceClientDocomo1(),
								'action_price_client_softbank_1' => $advert->getActionPriceClientSoftbank1(),
								'action_price_client_au_1' => $advert->getActionPriceClientAu1(),
								'action_price_client_pc_1' => $advert->getActionPriceClientPc1(),
								'action_price_client_docomo_2' => $advert->getActionPriceClientDocomo2(),
								'action_price_client_softbank_2' => $advert->getActionPriceClientSoftbank2(),
								'action_price_client_au_2' => $advert->getActionPriceClientAu2(),
								'action_price_client_pc_2' => $advert->getActionPriceClientPc2(),
								'action_price_client_docomo_3' => $advert->getActionPriceClientDocomo3(),
								'action_price_client_softbank_3' => $advert->getActionPriceClientSoftbank3(),
								'action_price_client_au_3' => $advert->getActionPriceClientAu3(),
								'action_price_client_pc_3' => $advert->getActionPriceClientPc3(),
								'action_price_client_docomo_4' => $advert->getActionPriceClientDocomo4(),
								'action_price_client_softbank_4' => $advert->getActionPriceClientSoftbank4(),
								'action_price_client_au_4' => $advert->getActionPriceClientAu4(),
								'action_price_client_pc_4' => $advert->getActionPriceClientPc4(),
								'action_price_client_docomo_5' => $advert->getActionPriceClientDocomo5(),
								'action_price_client_softbank_5' => $advert->getActionPriceClientSoftbank5(),
								'action_price_client_au_5' => $advert->getActionPriceClientAu5(),
								'action_price_client_pc_5' => $advert->getActionPriceClientPc5(),
								'action_price_media_docomo_1' => $advert->getActionPriceMediaDocomo1(),
								'action_price_media_softbank_1' => $advert->getActionPriceMediaSoftbank1(),
								'action_price_media_au_1' => $advert->getActionPriceMediaAu1(),
								'action_price_media_pc_1' => $advert->getActionPriceMediaPc1(),
								'action_price_media_docomo_2' => $advert->getActionPriceMediaDocomo2(),
								'action_price_media_softbank_2' => $advert->getActionPriceMediaSoftbank2(),
								'action_price_media_au_2' => $advert->getActionPriceMediaAu2(),
								'action_price_media_pc_2' => $advert->getActionPriceMediaPc2(),
								'action_price_media_docomo_3' => $advert->getActionPriceMediaDocomo3(),
								'action_price_media_softbank_3' => $advert->getActionPriceMediaSoftbank3(),
								'action_price_media_au_3' => $advert->getActionPriceMediaAu3(),
								'action_price_media_pc_3' => $advert->getActionPriceMediaPc3(),
								'action_price_media_docomo_4' => $advert->getActionPriceMediaDocomo4(),
								'action_price_media_softbank_4' => $advert->getActionPriceMediaSoftbank4(),
								'action_price_media_au_4' => $advert->getActionPriceMediaAu4(),
								'action_price_media_pc_4' => $advert->getActionPriceMediaPc4(),
								'action_price_media_docomo_5' => $advert->getActionPriceMediaDocomo5(),
								'action_price_media_softbank_5' => $advert->getActionPriceMediaSoftbank5(),
								'action_price_media_au_5' => $advert->getActionPriceMediaAu5(),
								'action_price_media_pc_5' => $advert->getActionPriceMediaPc5(),
								'ms_text_1' => $advert->getMsText1(),
								'ms_email_1' => $advert->getMsEmail1(),
								'ms_image_type_1' => $advert->getMsImageType1(),
								'ms_image_url_1' => $advert->getMsImageUrl1(),
								'ms_text_2' => $advert->getMsText2(),
								'ms_email_2' => $advert->getMsEmail2(),
								'ms_image_type_2' => $advert->getMsImageType2(),
								'ms_image_url_2' => $advert->getMsImageUrl2(),
								'ms_text_3' => $advert->getMsText3(),
								'ms_email_3' => $advert->getMsEmail3(),
								'ms_image_type_3' => $advert->getMsImageType3(),
								'ms_image_url_3' => $advert->getMsImageUrl3(),
								'ms_text_4' => $advert->getMsText4(),
								'ms_email_4' => $advert->getMsEmail4(),
								'ms_image_type_4' => $advert->getMsImageType4(),
								'ms_image_url_4' => $advert->getMsImageUrl4(),
								'ms_text_5' => $advert->getMsText5(),
								'ms_email_5' => $advert->getMsEmail5(),
								'ms_image_type_5' => $advert->getMsImageType5(),
								'ms_image_url_5' => $advert->getMsImageUrl5(),
								'unique_click_type' => $advert->getUniqueClickType(),
								'point_back_flag' => $advert->getPointBackFlag(),
								'adult_flag' => $advert->getAdultFlag(),
								'dating_flag' => $advert->getDatingFlag(),
								'advert_start_date' => $advert->getAdvertStartDate(),
								'advert_end_date' => $advert->getAdvertEndDate(),
								'unrestraint_flag' => $advert->getUnrestraintFlag(),
								'test_flag' => $advert->getTestFlag(),
								'status' => $advert->getStatus());

			if($advert->getMsImageType1() == 1 && $advert->getMsImageUrl1() != "") {
				$form_data['ms_image_path_1'] = $image_directory . $advert->getMsImageUrl1();
			} else {
				$form_data['ms_image_path_1'] = $advert->getMsImageUrl1();
			}

			if($advert->getMsImageType2() == 1 && $advert->getMsImageUrl2() != "") {
				$form_data['ms_image_path_2'] = $image_directory . $advert->getMsImageUrl2();
			} else {
				$form_data['ms_image_path_2'] = $advert->getMsImageUrl2();
			}

			if($advert->getMsImageType3() == 1 && $advert->getMsImageUrl3() != "") {
				$form_data['ms_image_path_3'] = $image_directory . $advert->getMsImageUrl3();
			} else {
				$form_data['ms_image_path_3'] = $advert->getMsImageUrl3();
			}

			if($advert->getMsImageType4() == 1 && $advert->getMsImageUrl4() != "") {
				$form_data['ms_image_path_4'] = $image_directory . $advert->getMsImageUrl4();
			} else {
				$form_data['ms_image_path_4'] = $advert->getMsImageUrl4();
			}

			if($advert->getMsImageType5() == 1 && $advert->getMsImageUrl5() != "") {
				$form_data['ms_image_path_5'] = $image_directory . $advert->getMsImageUrl5();
			} else {
				$form_data['ms_image_path_5'] = $advert->getMsImageUrl5();
			}

			$smarty->assign("form_data", $form_data);

			$advert_start_date_view = date("Y年m月d日", strtotime($advert->getAdvertStartDate()));
			$advert_end_date_view = date("Y年m月d日", strtotime($advert->getAdvertEndDate()));
			$smarty->assign("advert_start_date_view", $advert_start_date_view);
			$smarty->assign("advert_end_date_view", $advert_end_date_view);

			if($_POST['mode'] == "reserve_input") {

				$smarty->assign("mode", 'reserve_insert');
				$smarty->assign("sub_title", '予約');

				// ページを表示
				$smarty->display("./advert_reserve_input.tpl");
				exit();

			} elseif($_POST['mode'] == "reserve_insert") {

				$error_flag = 0;

				if($ms_image_type_1 == 1) {
					if($_FILES['ms_image_file_1']['name'] != "") {
						$image_name = uniqid("image_");
						$result = insert_image_file($_FILES['ms_image_file_1'], $image_directory, $image_name, $result_message);

						if($result) {
							$ms_image_url_1 = $result_message;
						} else {
							$error_mesage = $result_message;
							$error_flag = 1;
						}
					}
				}

				if($ms_image_type_2 == 1) {
					if($_FILES['ms_image_file_2']['name'] != "") {
						$image_name = uniqid("image_");
						$result = insert_image_file($_FILES['ms_image_file_2'], $image_directory, $image_name, $result_message);

						if($result) {
							$ms_image_url_2 = $result_message;
						} else {
							$error_mesage = $result_message;
							$error_flag = 1;
						}
					}
				}

				if($ms_image_type_3 == 1) {
					if($_FILES['ms_image_file_3']['name'] != "") {
						$image_name = uniqid("image_");
						$result = insert_image_file($_FILES['ms_image_file_3'], $image_directory, $image_name, $result_message);

						if($result) {
							$ms_image_url_3 = $result_message;
						} else {
							$error_mesage = $result_message;
							$error_flag = 1;
						}
					}
				}

				if($ms_image_type_4 == 1) {
					if($_FILES['ms_image_file_4']['name'] != "") {
						$image_name = uniqid("image_");
						$result = insert_image_file($_FILES['ms_image_file_4'], $image_directory, $image_name, $result_message);

						if($result) {
							$ms_image_url_4 = $result_message;
						} else {
							$error_mesage = $result_message;
							$error_flag = 1;
						}
					}
				}

				if($ms_image_type_5 == 1) {
					if($_FILES['ms_image_file_5']['name'] != "") {
						$image_name = uniqid("image_");
						$result = insert_image_file($_FILES['ms_image_file_5'], $image_directory, $image_name, $result_message);

						if($result) {
							$ms_image_url_5 = $result_message;
						} else {
							$error_mesage = $result_message;
							$error_flag = 1;
						}
					}
				}

				if($error_flag == 0) {

					$advert_reserve_dao = new AdvertReserveDao();
					$advert_reserve_dao->transaction_start();

					$advert_reserve = new AdvertReserve();
					$advert_reserve->setAdvertId($advert_id);
					$advert_reserve->setSupportDocomo($support_docomo);
					$advert_reserve->setSupportSoftbank($support_softbank);
					$advert_reserve->setSupportAu($support_au);
					$advert_reserve->setSupportPc($support_pc);
					$advert_reserve->setSiteUrlDocomo($site_url_docomo);
					$advert_reserve->setSiteUrlSoftbank($site_url_softbank);
					$advert_reserve->setSiteUrlAu($site_url_au);
					$advert_reserve->setSiteUrlPc($site_url_pc);
					$advert_reserve->setClickPriceClient($click_price_client);
					$advert_reserve->setClickPriceMedia($click_price_media);
					$advert_reserve->setActionPriceClientDocomo1($action_price_client_docomo_1);
					$advert_reserve->setActionPriceClientSoftbank1($action_price_client_softbank_1);
					$advert_reserve->setActionPriceClientAu1($action_price_client_au_1);
					$advert_reserve->setActionPriceClientPc1($action_price_client_pc_1);
					$advert_reserve->setActionPriceClientDocomo2($action_price_client_docomo_2);
					$advert_reserve->setActionPriceClientSoftbank2($action_price_client_softbank_2);
					$advert_reserve->setActionPriceClientAu2($action_price_client_au_2);
					$advert_reserve->setActionPriceClientPc2($action_price_client_pc_2);
					$advert_reserve->setActionPriceClientDocomo3($action_price_client_docomo_3);
					$advert_reserve->setActionPriceClientSoftbank3($action_price_client_softbank_3);
					$advert_reserve->setActionPriceClientAu3($action_price_client_au_3);
					$advert_reserve->setActionPriceClientPc3($action_price_client_pc_3);
					$advert_reserve->setActionPriceClientDocomo4($action_price_client_docomo_4);
					$advert_reserve->setActionPriceClientSoftbank4($action_price_client_softbank_4);
					$advert_reserve->setActionPriceClientAu4($action_price_client_au_4);
					$advert_reserve->setActionPriceClientPc4($action_price_client_pc_4);
					$advert_reserve->setActionPriceClientDocomo5($action_price_client_docomo_5);
					$advert_reserve->setActionPriceClientSoftbank5($action_price_client_softbank_5);
					$advert_reserve->setActionPriceClientAu5($action_price_client_au_5);
					$advert_reserve->setActionPriceClientPc5($action_price_client_pc_5);
					$advert_reserve->setActionPriceMediaDocomo1($action_price_media_docomo_1);
					$advert_reserve->setActionPriceMediaSoftbank1($action_price_media_softbank_1);
					$advert_reserve->setActionPriceMediaAu1($action_price_media_au_1);
					$advert_reserve->setActionPriceMediaPc1($action_price_media_pc_1);
					$advert_reserve->setActionPriceMediaDocomo2($action_price_media_docomo_2);
					$advert_reserve->setActionPriceMediaSoftbank2($action_price_media_softbank_2);
					$advert_reserve->setActionPriceMediaAu2($action_price_media_au_2);
					$advert_reserve->setActionPriceMediaPc2($action_price_media_pc_2);
					$advert_reserve->setActionPriceMediaDocomo3($action_price_media_docomo_3);
					$advert_reserve->setActionPriceMediaSoftbank3($action_price_media_softbank_3);
					$advert_reserve->setActionPriceMediaAu3($action_price_media_au_3);
					$advert_reserve->setActionPriceMediaPc3($action_price_media_pc_3);
					$advert_reserve->setActionPriceMediaDocomo4($action_price_media_docomo_4);
					$advert_reserve->setActionPriceMediaSoftbank4($action_price_media_softbank_4);
					$advert_reserve->setActionPriceMediaAu4($action_price_media_au_4);
					$advert_reserve->setActionPriceMediaPc4($action_price_media_pc_4);
					$advert_reserve->setActionPriceMediaDocomo5($action_price_media_docomo_5);
					$advert_reserve->setActionPriceMediaSoftbank5($action_price_media_softbank_5);
					$advert_reserve->setActionPriceMediaAu5($action_price_media_au_5);
					$advert_reserve->setActionPriceMediaPc5($action_price_media_pc_5);
					$advert_reserve->setMsText1($ms_text_1);
					$advert_reserve->setMsEmail1($ms_email_1);
					$advert_reserve->setMsImageType1($ms_image_type_1);
					$advert_reserve->setMsImageUrl1($ms_image_url_1);
					$advert_reserve->setMsText2($ms_text_2);
					$advert_reserve->setMsEmail2($ms_email_2);
					$advert_reserve->setMsImageType2($ms_image_type_2);
					$advert_reserve->setMsImageUrl2($ms_image_url_2);
					$advert_reserve->setMsText3($ms_text_3);
					$advert_reserve->setMsEmail3($ms_email_3);
					$advert_reserve->setMsImageType3($ms_image_type_3);
					$advert_reserve->setMsImageUrl3($ms_image_url_3);
					$advert_reserve->setMsText4($ms_text_4);
					$advert_reserve->setMsEmail4($ms_email_4);
					$advert_reserve->setMsImageType4($ms_image_type_4);
					$advert_reserve->setMsImageUrl4($ms_image_url_4);
					$advert_reserve->setMsText5($ms_text_5);
					$advert_reserve->setMsEmail5($ms_email_5);
					$advert_reserve->setMsImageType5($ms_image_type_5);
					$advert_reserve->setMsImageUrl5($ms_image_url_5);
					$advert_reserve->setReserveChangeDate($reserve_change_date);
					$advert_reserve->setStatus(0);

					//INSERTを実行
					$db_result = $advert_reserve_dao->insertAdvertReserve($advert_reserve, $result_message);
					if($db_result) {

						$advert_reserve_dao->transaction_end();

						view_list();

					} else {

						$advert_reserve_dao->transaction_rollback();

						$smarty->assign("error_message", $error_message);

						$smarty->assign("mode", 'reserve_insert');
						$smarty->assign("sub_title", '予約');

						// ページを表示
						$smarty->display("./advert_reserve_input.tpl");
						exit();

					}

				} else {

					$smarty->assign("error_message", $error_message);

					$smarty->assign("mode", 'reserve_insert');
					$smarty->assign("sub_title", '予約');

					// ページを表示
					$smarty->display("./advert_reserve_input.tpl");
					exit();

				}
			} elseif($_POST['mode'] == "edit") {

				$advert_reserve_dao = new AdvertReserveDao();
				$advert_reserve = new AdvertReserve();
				$advert_reserve = $advert_reserve_dao->getAdvertReserveById($id);

				if(!is_null($advert_reserve)) {

					$form_data['id'] = $id;
					$form_data['advert_id'] = $advert_id;
					$form_data['support_docomo'] = $advert_reserve->getSupportDocomo();
					$form_data['support_softbank'] = $advert_reserve->getSupportSoftbank();
					$form_data['support_au'] = $advert_reserve->getSupportAu();
					$form_data['support_pc'] = $advert_reserve->getSupportPc();
					$form_data['site_url_docomo'] = $advert_reserve->getSiteUrlDocomo();
					$form_data['site_url_softbank'] = $advert_reserve->getSiteUrlSoftbank();
					$form_data['site_url_au'] = $advert_reserve->getSiteUrlAu();
					$form_data['site_url_pc'] = $advert_reserve->getSiteUrlPc();
					$form_data['click_price_client'] = $advert_reserve->getClickPriceClient();
					$form_data['click_price_media'] = $advert_reserve->getClickPriceMedia();
					$form_data['action_price_client_docomo_1'] = $advert_reserve->getActionPriceClientDocomo1();
					$form_data['action_price_client_softbank_1'] = $advert_reserve->getActionPriceClientSoftbank1();
					$form_data['action_price_client_au_1'] = $advert_reserve->getActionPriceClientAu1();
					$form_data['action_price_client_pc_1'] = $advert_reserve->getActionPriceClientPc1();
					$form_data['action_price_client_docomo_2'] = $advert_reserve->getActionPriceClientDocomo2();
					$form_data['action_price_client_softbank_2'] = $advert_reserve->getActionPriceClientSoftbank2();
					$form_data['action_price_client_au_2'] = $advert_reserve->getActionPriceClientAu2();
					$form_data['action_price_client_pc_2'] = $advert_reserve->getActionPriceClientPc2();
					$form_data['action_price_client_docomo_3'] = $advert_reserve->getActionPriceClientDocomo3();
					$form_data['action_price_client_softbank_3'] = $advert_reserve->getActionPriceClientSoftbank3();
					$form_data['action_price_client_au_3'] = $advert_reserve->getActionPriceClientAu3();
					$form_data['action_price_client_pc_3'] = $advert_reserve->getActionPriceClientPc3();
					$form_data['action_price_client_docomo_4'] = $advert_reserve->getActionPriceClientDocomo4();
					$form_data['action_price_client_softbank_4'] = $advert_reserve->getActionPriceClientSoftbank4();
					$form_data['action_price_client_au_4'] = $advert_reserve->getActionPriceClientAu4();
					$form_data['action_price_client_pc_4'] = $advert_reserve->getActionPriceClientPc4();
					$form_data['action_price_client_docomo_5'] = $advert_reserve->getActionPriceClientDocomo5();
					$form_data['action_price_client_softbank_5'] = $advert_reserve->getActionPriceClientSoftbank5();
					$form_data['action_price_client_au_5'] = $advert_reserve->getActionPriceClientAu5();
					$form_data['action_price_client_pc_5'] = $advert_reserve->getActionPriceClientPc5();
					$form_data['action_price_media_docomo_1'] = $advert_reserve->getActionPriceMediaDocomo1();
					$form_data['action_price_media_softbank_1'] = $advert_reserve->getActionPriceMediaSoftbank1();
					$form_data['action_price_media_au_1'] = $advert_reserve->getActionPriceMediaAu1();
					$form_data['action_price_media_pc_1'] = $advert_reserve->getActionPriceMediaPc1();
					$form_data['action_price_media_docomo_2'] = $advert_reserve->getActionPriceMediaDocomo2();
					$form_data['action_price_media_softbank_2'] = $advert_reserve->getActionPriceMediaSoftbank2();
					$form_data['action_price_media_au_2'] = $advert_reserve->getActionPriceMediaAu2();
					$form_data['action_price_media_pc_2'] = $advert_reserve->getActionPriceMediaPc2();
					$form_data['action_price_media_docomo_3'] = $advert_reserve->getActionPriceMediaDocomo3();
					$form_data['action_price_media_softbank_3'] = $advert_reserve->getActionPriceMediaSoftbank3();
					$form_data['action_price_media_au_3'] = $advert_reserve->getActionPriceMediaAu3();
					$form_data['action_price_media_pc_3'] = $advert_reserve->getActionPriceMediaPc3();
					$form_data['action_price_media_docomo_4'] = $advert_reserve->getActionPriceMediaDocomo4();
					$form_data['action_price_media_softbank_4'] = $advert_reserve->getActionPriceMediaSoftbank4();
					$form_data['action_price_media_au_4'] = $advert_reserve->getActionPriceMediaAu4();
					$form_data['action_price_media_pc_4'] = $advert_reserve->getActionPriceMediaPc4();
					$form_data['action_price_media_docomo_5'] = $advert_reserve->getActionPriceMediaDocomo5();
					$form_data['action_price_media_softbank_5'] = $advert_reserve->getActionPriceMediaSoftbank5();
					$form_data['action_price_media_au_5'] = $advert_reserve->getActionPriceMediaAu5();
					$form_data['action_price_media_pc_5'] = $advert_reserve->getActionPriceMediaPc5();
					$form_data['ms_text_1'] = $advert_reserve->getMsText1();
					$form_data['ms_email_1'] = $advert_reserve->getMsEmail1();
					$form_data['ms_image_type_1'] = $advert_reserve->getMsImageType1();
					$form_data['ms_image_url_1'] = $advert_reserve->getMsImageUrl1();
					$form_data['ms_text_2'] = $advert_reserve->getMsText2();
					$form_data['ms_email_2'] = $advert_reserve->getMsEmail2();
					$form_data['ms_image_type_2'] = $advert_reserve->getMsImageType2();
					$form_data['ms_image_url_2'] = $advert_reserve->getMsImageUrl2();
					$form_data['ms_text_3'] = $advert_reserve->getMsText3();
					$form_data['ms_email_3'] = $advert_reserve->getMsEmail3();
					$form_data['ms_image_type_3'] = $advert_reserve->getMsImageType3();
					$form_data['ms_image_url_3'] = $advert_reserve->getMsImageUrl3();
					$form_data['ms_text_4'] = $advert_reserve->getMsText4();
					$form_data['ms_email_4'] = $advert_reserve->getMsEmail4();
					$form_data['ms_image_type_4'] = $advert_reserve->getMsImageType4();
					$form_data['ms_image_url_4'] = $advert_reserve->getMsImageUrl4();
					$form_data['ms_text_5'] = $advert_reserve->getMsText5();
					$form_data['ms_email_5'] = $advert_reserve->getMsEmail5();
					$form_data['ms_image_type_5'] = $advert_reserve->getMsImageType5();
					$form_data['ms_image_url_5'] = $advert_reserve->getMsImageUrl5();

					if($advert_reserve->getMsImageType1() == 1 && $advert_reserve->getMsImageUrl1() != "") {
						$form_data['ms_image_path_1'] = $image_directory . $advert_reserve->getMsImageUrl1();
					} else {
						$form_data['ms_image_path_1'] = $advert_reserve->getMsImageUrl1();
					}

					if($advert_reserve->getMsImageType2() == 1 && $advert_reserve->getMsImageUrl2() != "") {
						$form_data['ms_image_path_2'] = $image_directory . $advert_reserve->getMsImageUrl2();
					} else {
						$form_data['ms_image_path_2'] = $advert_reserve->getMsImageUrl2();
					}

					if($advert_reserve->getMsImageType3() == 1 && $advert_reserve->getMsImageUrl3() != "") {
						$form_data['ms_image_path_3'] = $image_directory . $advert_reserve->getMsImageUrl3();
					} else {
						$form_data['ms_image_path_3'] = $advert_reserve->getMsImageUrl3();
					}

					if($advert_reserve->getMsImageType4() == 1 && $advert_reserve->getMsImageUrl4() != "") {
						$form_data['ms_image_path_4'] = $image_directory . $advert_reserve->getMsImageUrl4();
					} else {
						$form_data['ms_image_path_4'] = $advert_reserve->getMsImageUrl4();
					}

					if($advert_reserve->getMsImageType5() == 1 && $advert_reserve->getMsImageUrl5() != "") {
						$form_data['ms_image_path_5'] = $image_directory . $advert_reserve->getMsImageUrl5();
					} else {
						$form_data['ms_image_path_5'] = $advert_reserve->getMsImageUrl5();
					}

					$smarty->assign("form_data", $form_data);

					$r = explode("-", $advert_reserve->getReserveChangeDate());

					$smarty->assign("set_r_year", $r[0]);
					$smarty->assign("set_r_month", $r[1]);
					$smarty->assign("set_r_day", $r[2]);

					$smarty->assign("mode", 'update_commit');
					$smarty->assign("sub_title", '予約詳細');

					// ページを表示
					$smarty->display("./advert_reserve_input.tpl");
					exit();

				} else {
					$error_message .= "該当するデータはありません。";
					$smarty->assign("error_message", $error_message);

					view_list();
				}
			} elseif($_POST['mode'] == "update_commit") {

				$error_flag = 0;

				if($ms_image_type_1 == 1) {
					if($_FILES['ms_image_file_1']['name'] != "") {
						$image_name = uniqid("image_");
						$result = insert_image_file($_FILES['ms_image_file_1'], $image_directory, $image_name, $result_message);

						if($result) {
							$ms_image_url_1 = $result_message;
						} else {
							$error_mesage = $result_message;
							$error_flag = 1;
						}
					}
				}

				if($ms_image_type_2 == 1) {
					if($_FILES['ms_image_file_2']['name'] != "") {
						$image_name = uniqid("image_");
						$result = insert_image_file($_FILES['ms_image_file_2'], $image_directory, $image_name, $result_message);

						if($result) {
							$ms_image_url_2 = $result_message;
						} else {
							$error_mesage = $result_message;
							$error_flag = 1;
						}
					}
				}

				if($ms_image_type_3 == 1) {
					if($_FILES['ms_image_file_3']['name'] != "") {
						$image_name = uniqid("image_");
						$result = insert_image_file($_FILES['ms_image_file_3'], $image_directory, $image_name, $result_message);

						if($result) {
							$ms_image_url_3 = $result_message;
						} else {
							$error_mesage = $result_message;
							$error_flag = 1;
						}
					}
				}

				if($ms_image_type_4 == 1) {
					if($_FILES['ms_image_file_4']['name'] != "") {
						$image_name = uniqid("image_");
						$result = insert_image_file($_FILES['ms_image_file_4'], $image_directory, $image_name, $result_message);

						if($result) {
							$ms_image_url_4 = $result_message;
						} else {
							$error_mesage = $result_message;
							$error_flag = 1;
						}
					}
				}

				if($ms_image_type_5 == 1) {
					if($_FILES['ms_image_file_5']['name'] != "") {
						$image_name = uniqid("image_");
						$result = insert_image_file($_FILES['ms_image_file_5'], $image_directory, $image_name, $result_message);

						if($result) {
							$ms_image_url_5 = $result_message;
						} else {
							$error_mesage = $result_message;
							$error_flag = 1;
						}
					}
				}

				if($error_flag == 0) {

					$advert_reserve_dao = new AdvertReserveDao();
					$advert_reserve_dao->transaction_start();

					$advert_reserve = new AdvertReserve();

					$advert_reserve->setId($id);
					$advert_reserve->setAdvertId($advert_id);
					$advert_reserve->setSupportDocomo($support_docomo);
					$advert_reserve->setSupportSoftbank($support_softbank);
					$advert_reserve->setSupportAu($support_au);
					$advert_reserve->setSupportPc($support_pc);
					$advert_reserve->setSiteUrlDocomo($site_url_docomo);
					$advert_reserve->setSiteUrlSoftbank($site_url_softbank);
					$advert_reserve->setSiteUrlAu($site_url_au);
					$advert_reserve->setSiteUrlPc($site_url_pc);
					$advert_reserve->setClickPriceClient($click_price_client);
					$advert_reserve->setClickPriceMedia($click_price_media);
					$advert_reserve->setActionPriceClientDocomo1($action_price_client_docomo_1);
					$advert_reserve->setActionPriceClientSoftbank1($action_price_client_softbank_1);
					$advert_reserve->setActionPriceClientAu1($action_price_client_au_1);
					$advert_reserve->setActionPriceClientPc1($action_price_client_pc_1);
					$advert_reserve->setActionPriceClientDocomo2($action_price_client_docomo_2);
					$advert_reserve->setActionPriceClientSoftbank2($action_price_client_softbank_2);
					$advert_reserve->setActionPriceClientAu2($action_price_client_au_2);
					$advert_reserve->setActionPriceClientPc2($action_price_client_pc_2);
					$advert_reserve->setActionPriceClientDocomo3($action_price_client_docomo_3);
					$advert_reserve->setActionPriceClientSoftbank3($action_price_client_softbank_3);
					$advert_reserve->setActionPriceClientAu3($action_price_client_au_3);
					$advert_reserve->setActionPriceClientPc3($action_price_client_pc_3);
					$advert_reserve->setActionPriceClientDocomo4($action_price_client_docomo_4);
					$advert_reserve->setActionPriceClientSoftbank4($action_price_client_softbank_4);
					$advert_reserve->setActionPriceClientAu4($action_price_client_au_4);
					$advert_reserve->setActionPriceClientPc4($action_price_client_pc_4);
					$advert_reserve->setActionPriceClientDocomo5($action_price_client_docomo_5);
					$advert_reserve->setActionPriceClientSoftbank5($action_price_client_softbank_5);
					$advert_reserve->setActionPriceClientAu5($action_price_client_au_5);
					$advert_reserve->setActionPriceClientPc5($action_price_client_pc_5);
					$advert_reserve->setActionPriceMediaDocomo1($action_price_media_docomo_1);
					$advert_reserve->setActionPriceMediaSoftbank1($action_price_media_softbank_1);
					$advert_reserve->setActionPriceMediaAu1($action_price_media_au_1);
					$advert_reserve->setActionPriceMediaPc1($action_price_media_pc_1);
					$advert_reserve->setActionPriceMediaDocomo2($action_price_media_docomo_2);
					$advert_reserve->setActionPriceMediaSoftbank2($action_price_media_softbank_2);
					$advert_reserve->setActionPriceMediaAu2($action_price_media_au_2);
					$advert_reserve->setActionPriceMediaPc2($action_price_media_pc_2);
					$advert_reserve->setActionPriceMediaDocomo3($action_price_media_docomo_3);
					$advert_reserve->setActionPriceMediaSoftbank3($action_price_media_softbank_3);
					$advert_reserve->setActionPriceMediaAu3($action_price_media_au_3);
					$advert_reserve->setActionPriceMediaPc3($action_price_media_pc_3);
					$advert_reserve->setActionPriceMediaDocomo4($action_price_media_docomo_4);
					$advert_reserve->setActionPriceMediaSoftbank4($action_price_media_softbank_4);
					$advert_reserve->setActionPriceMediaAu4($action_price_media_au_4);
					$advert_reserve->setActionPriceMediaPc4($action_price_media_pc_4);
					$advert_reserve->setActionPriceMediaDocomo5($action_price_media_docomo_5);
					$advert_reserve->setActionPriceMediaSoftbank5($action_price_media_softbank_5);
					$advert_reserve->setActionPriceMediaAu5($action_price_media_au_5);
					$advert_reserve->setActionPriceMediaPc5($action_price_media_pc_5);
					$advert_reserve->setMsText1($ms_text_1);
					$advert_reserve->setMsEmail1($ms_email_1);
					$advert_reserve->setMsImageType1($ms_image_type_1);
					$advert_reserve->setMsImageUrl1($ms_image_url_1);
					$advert_reserve->setMsText2($ms_text_2);
					$advert_reserve->setMsEmail2($ms_email_2);
					$advert_reserve->setMsImageType2($ms_image_type_2);
					$advert_reserve->setMsImageUrl2($ms_image_url_2);
					$advert_reserve->setMsText3($ms_text_3);
					$advert_reserve->setMsEmail3($ms_email_3);
					$advert_reserve->setMsImageType3($ms_image_type_3);
					$advert_reserve->setMsImageUrl3($ms_image_url_3);
					$advert_reserve->setMsText4($ms_text_4);
					$advert_reserve->setMsEmail4($ms_email_4);
					$advert_reserve->setMsImageType4($ms_image_type_4);
					$advert_reserve->setMsImageUrl4($ms_image_url_4);
					$advert_reserve->setMsText5($ms_text_5);
					$advert_reserve->setMsEmail5($ms_email_5);
					$advert_reserve->setMsImageType5($ms_image_type_5);
					$advert_reserve->setMsImageUrl5($ms_image_url_5);
					$advert_reserve->setReserveChangeDate($reserve_change_date);
					$advert_reserve->setStatus(0);

					//UPDATEを実行
					$db_result = $advert_reserve_dao->updateAdvertReserve($advert_reserve, $result_message);
					if($db_result) {

						$advert_reserve_dao->transaction_end();

						view_list();

					} else {

						$advert_reserve_dao->transaction_rollback();

						$smarty->assign("error_message", $error_message);

						$smarty->assign("mode", 'update_commit');
						$smarty->assign("sub_title", '予約詳細');

						// ページを表示
						$smarty->display("./advert_reserve_input.tpl");
						exit();

					}

				} else {

					$smarty->assign("error_message", $error_message);

					$smarty->assign("mode", 'update_commit');
					$smarty->assign("sub_title", '予約詳細');

					// ページを表示
					$smarty->display("./advert_reserve_input.tpl");
					exit();

				}
			}elseif($_POST['mode'] == 'delete'){
				$advert_reserve_dao = new AdvertReserveDao();
				$advert_reserve_dao->transaction_start();

				if(!is_null($advert_reserve_dao->getAdvertReserveById($id))) {
					$db_result = $advert_reserve_dao->deleteAdvertReserve($id, $result_message);
					if($db_result){
						$advert_reserve_dao->transaction_end();

						$smarty->assign("info_message", $result_message);

						view_list();
					}else{
						$advert_reserve_dao->transaction_rollback();

						$smarty->assign("error_message", $result_message);

						view_list();
					}
				}else{
					$advert_reserve_dao->transaction_rollback();

					$error_message = "ＤＢの更新に失敗しました。";
					$smarty->assign("error_message", $error_message);

					view_list();
				}
			} else {
				view_list();
			}
		} else {
			view_list();
		}
	} else {
		view_list();
	}
}else{
	header('Location: ./login.php?error=1');
	exit();
}

function view_list(){
	global $common_dao, $smarty, $list_sql, $error_message;

	$list_count = 0;

	$db_result = $common_dao->db_query($list_sql);
	if($db_result){
		$smarty->assign("list", $db_result);
		$list_count = count($db_result);
	}else{
		$error_message .= "ＤＢからのデータの取得に失敗しました。(su0000)";
	}
	$smarty->assign("list_count", $list_count);
	$smarty->assign("error_message", $error_message);

	// ページを表示
	$smarty->display("./advert_reserve_list.tpl");
	exit();
}

function do_escape_quotes($str){
	//magic_quotesが有効ならクウォート部分を除去
	if(get_magic_quotes_gpc()){
		$str = stripslashes($str);
	}
	return $str;
}

function insert_image_file($up_file, $up_directory, $up_name, &$result_message = "") {
	$image_size = $up_file['size'];
	$image_tmp = $up_file['tmp_name'];

	if($image_size <= 4000000) {
		$image_info = getimagesize($image_tmp);
		if($image_info[2] == 1) {
			$file_type = "gif";
		} elseif($image_info[2] == 2) {
			$file_type = "jpg";
		} elseif($image_info[2] == 3) {
			$file_type = "png";
		} else {
			$error_message = "対応画像ファイルではありません。";
			return false;
		}

		$up_file_path = $up_directory . $up_name . "." . $file_type;

		if(move_uploaded_file($image_tmp, $up_file_path)) {
			chmod($up_file_path,0666);
			$result_message = $up_name . "." . $file_type;
			return true;
		} else {
			$result_message = "画像ファイルの保存に失敗しました。";
			return false;
		}
	} else {
		$result_message = "画像ファイルのサイズが大きすぎます。(4000KBまで）";
		return false;
	}
}
?>