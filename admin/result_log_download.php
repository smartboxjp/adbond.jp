<?php
// 共通設定
require_once( '../common/CommonAdminBase.php' );
require_once( '../common/CommonDao.php' );
require_once( '../dto/LoginUser.php' );

session_start();

$common_dao = new CommonDao();

if(isset($_GET['date']) && $_GET['date'] != '') {
	if(isset($_GET['type']) && $_GET['type'] != '') {
		$download_date = $_GET['date'];
		$log_type = $_GET['type'];

		if($log_type == 1) {

// ------------------------------------------------------------ 2010/12/07 修正
//			$download_sql = " SELECT created_at, media_id, advert_id, carrier_id, user_agent, uid, ip_address, host_name, link_url, status "
//							. " FROM action_logs ";
			$download_sql = " SELECT created_at, media_id, advert_id, carrier_id, user_agent, uid, ip_address, host_name, link_url, status ";
			$add_from = " FROM action_logs ";
			if(isset($_GET['monthly']) && $_GET['monthly'] == 1) {
//				$download_sql .= " WHERE DATE_FORMAT(created_at,'%Y%m') = '$download_date' ";
				$add_where = " WHERE DATE_FORMAT(created_at, '%Y%m') = '$download_date' ";
			} else {
//				$download_sql .= " WHERE DATE_FORMAT(created_at,'%Y%m%d') = '$download_date' ";
				$add_where = " WHERE DATE_FORMAT(created_at, '%Y%m%d') = '$download_date' ";
			}

//			$download_sql .= " AND (status = 1 OR status = 2) "
//							. " AND deleted_at is NULL "
//							. " ORDER BY created_at ASC ";

			$add_where .= " AND (status = 1 OR status = 2) "
						." AND deleted_at is NULL "
						." ORDER BY created_at ASC ";

		} elseif($log_type == 2) {

//			$download_sql = " SELECT action_complete_date, media_id, advert_id, carrier_id, user_agent, uid, ip_address, host_name, status "
//							. " FROM action_logs ";
			$download_sql = " SELECT action_complete_date, media_id, advert_id, carrier_id, user_agent, uid, ip_address, host_name, status ";
			$add_from = " FROM action_logs ";
			if(isset($_GET['monthly']) && $_GET['monthly'] == 1) {
//				$download_sql .= " WHERE DATE_FORMAT(action_complete_date,'%Y%m') = '$download_date' ";
				$add_where = " WHERE DATE_FORMAT(action_complete_date,'%Y%m') = '$download_date' ";
			} else {
//				$download_sql .= " WHERE DATE_FORMAT(action_complete_date,'%Y%m%d') = '$download_date' ";
				$add_where = " WHERE DATE_FORMAT(action_complete_date,'%Y%m%d') = '$download_date' ";
			}

			$add_where .= " AND status = 2 "
						. " AND deleted_at is NULL "
						. " ORDER BY action_complete_date ASC ";

		} elseif($log_type == 3) {

//			$download_sql = " SELECT created_at, media_id, advert_id, point_back_parameter, point_back_url, status "
//							. " FROM point_back_logs ";
			$download_sql = " SELECT created_at, media_id, advert_id, point_back_parameter, point_back_url, status ";
			$add_from =  " FROM point_back_logs ";

			if(isset($_GET['monthly']) && $_GET['monthly'] == 1) {
//				$download_sql .= " WHERE DATE_FORMAT(created_at,'%Y%m') = '$download_date' ";
				$add_where = " WHERE DATE_FORMAT(created_at,'%Y%m') = '$download_date' ";
			} else {
//				$download_sql .= " WHERE DATE_FORMAT(created_at,'%Y%m%d') = '$download_date' ";
				$add_where = " WHERE DATE_FORMAT(created_at,'%Y%m%d') = '$download_date' ";
			}

//			$download_sql .= " AND deleted_at is NULL "
//							. " ORDER BY created_at ASC ";
			$add_where .= " AND deleted_at is NULL "
						. " ORDER BY created_at ASC ";
// ----------------------------------------------------------------------------
		}

// ------------------------------------------------------------ 2010/12/07 追加
		$limit_max = 10000;

		$count_sql = " SELECT count(*) as count ";
		$count_sql .= $add_from;
		$count_sql .= $add_where;

		$db_result1 = $common_dao->db_query($count_sql);
		$list_count = $db_result1[0]['count'];

		$count_limit = $list_count/$limit_max;

		$download_sql .= $add_from;
		$download_sql .= $add_where;

		for($count=1; $count<=$count_limit; $count+=1){
			//LIMITの開始 設定
			$limit_start = ($count -1) * $limit_max;

			$add_limit = " LIMIT $limit_start, $limit_max ";
			$sql = $download_sql.$add_limit;
// ----------------------------------------------------------------------------

			$db_result = $common_dao->db_query($sql);
			if($db_result){
		$testfp = fopen("../logs/test.txt", "w");
		fwrite($testfp, "test");
		fclose($testfp);

// ------------------------------------------------------------ 2010/12/07 修正
//				$outputFile = "../logs/output.cgi";
				$outputFile = "../logs/output";
				$outputFile .= $count.".cgi";
// ----------------------------------------------------------------------------

				touch($outputFile);
				chmod($outputFile, 0646);

				$fp = fopen($outputFile, "w");

				mb_internal_encoding("UTF-8");
				mb_detect_order("ASCII, JIS, UTF-8, eucjp-win, sjis-win, EUC-JP, SJIS");

				if($log_type == 1) {

					$file_name = "click_$download_date.csv";

					$csv_array[] = array('クリック日時', '媒体ID', '広告ID', 'キャリア', 'ユーザーエージェント', '個体識別番号', 'IPアドレス', 'ホスト名', 'リンク先URL', 'ステータス');

					foreach($db_result as $row) {
						$data1 = $row['created_at'];
						$data2 = $row['media_id'];
						$data3 = $row['advert_id'];
						$data4 = $row['carrier_id'];
						$data5 = $row['user_agent'];
						$data6 = $row['uid'];
						$data7 = $row['ip_address'];
						$data8 = $row['host_name'];
						$data9 = $row['link_url'];
						$data10 = $row['status'];

						$csv_array[] = array($data1, $data2, $data3, $data4, $data5, $data6, $data7, $data8, $data9, $data10);
					}

				} elseif($log_type == 2) {

					$file_name = "action_$download_date.csv";

					$csv_array[] = array('アクション完了日時', '媒体ID', '広告ID', 'キャリア', 'ユーザーエージェント', '個体識別番号', 'IPアドレス', 'ホスト名', 'ステータス');

					foreach($db_result as $row) {
						$data1 = $row['action_complete_date'];
						$data2 = $row['media_id'];
						$data3 = $row['advert_id'];
						$data4 = $row['carrier_id'];
						$data5 = $row['user_agent'];
						$data6 = $row['uid'];
						$data7 = $row['ip_address'];
						$data8 = $row['host_name'];
						$data9 = $row['status'];

						$csv_array[] = array($data1, $data2, $data3, $data4, $data5, $data6, $data7, $data8, $data9);
					}

				} elseif($log_type == 3) {

					$file_name = "point_back_$download_date.csv";

					$csv_array[] = array('ポイントバック通知日時', '媒体ID', '広告ID', 'ユーザ識別ID', 'ポイントバック通知URL', 'ステータス');

					foreach($db_result as $row) {
						$data1 = $row['created_at'];
						$data2 = $row['media_id'];
						$data3 = $row['advert_id'];
						$data4 = $row['point_back_parameter'];
						$data5 = $row['point_back_url'];
						$data6 = $row['status'];

						$csv_array[] = array($data1, $data2, $data3, $data4, $data5, $data6);
						mb_convert_variables("SJIS-win", "UTF-8", $csv_array[]);
					}

				}

				foreach($csv_array as $line) {
					fputcsv($fp, $line);
				}

				fclose($fp);

				header("Content-Type: application/csv");
				header("Content-Disposition: attachment; filename=".$file_name);
				header("Content-Length:".filesize($outputFile));
				readfile($outputFile);
				exit();
			} else {
				exit();
			}
		}
	} else {
		exit();
	}
} else {
	exit();
}

function do_escape_quotes($str){
	//magic_quotesが有効ならクウォート部分を除去
	if(get_magic_quotes_gpc()){
		$str = stripslashes($str);
	}
	return $str;
}
?>