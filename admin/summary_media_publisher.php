<?php
// 共通設定
require_once( '../common/CommonAdminBase.php' );
require_once( '../common/CommonDao.php' );
require_once( '../dto/LoginUser.php' );
require_once( '../dao/MediaPublisherDao.php' );
require_once( '../dto/MediaPublisher.php' );

session_start();

if(isset($_SESSION['logon_token']) && $_SESSION['logon_token'] != ''){
	$login_user = new LoginUser();
	$login_user = $_SESSION['login_user'];

	// Smartyオブジェクト取得
	$smarty =& getSmartyObj();

	$smarty->assign("title", "Test Top Page");
	$smarty->assign("login_user", $login_user );

	$common_dao = new CommonDao();

	//媒体発行者一覧データ取得
	$media_publisher_dao = new MediaPublisherDao();
	$media_publisher_array = array();
	foreach($media_publisher_dao->getAllMediaPublisher() as $val){
		$row_array = array('id' => $val->getId(), 'name' => $val->getPublisherName());
		$media_publisher_array[$val->getId()] = $row_array;
	}
	$smarty->assign("media_publisher_array", $media_publisher_array);

	//現在日時取得
	$now_date = getdate();
	$now_year = $now_date['year'];
	$now_month = $now_date['mon'];

	$select_date_type = 1;
	$monthly_year = $now_year;
	$monthly_month = $now_month;
	$between_start_year = $now_year;
	$between_start_month = $now_month;
	$between_start_day = 1;
	$between_end_year = $now_year;
	$between_end_month = $now_month;
	$between_end_day = date("d", mktime(0, 0, 0, $now_month + 1, 0, $now_year));

	if(isset($_POST['mode']) && $_POST['mode'] == 'search') {
		$media_publisher_id = do_escape_quotes($_POST['media_publisher_id']);
		$select_date_type = do_escape_quotes($_POST['select_date_type']);
		$monthly_year = $common_dao->db_string_escape(do_escape_quotes($_POST['monthly_year']));
		$monthly_month = $common_dao->db_string_escape(do_escape_quotes($_POST['monthly_month']));
		$between_start_year = $common_dao->db_string_escape(do_escape_quotes($_POST['between_start_year']));
		$between_start_month = $common_dao->db_string_escape(do_escape_quotes($_POST['between_start_month']));
		$between_start_day = $common_dao->db_string_escape(do_escape_quotes($_POST['between_start_day']));
		$between_end_year = $common_dao->db_string_escape(do_escape_quotes($_POST['between_end_year']));
		$between_end_month = $common_dao->db_string_escape(do_escape_quotes($_POST['between_end_month']));
		$between_end_day = $common_dao->db_string_escape(do_escape_quotes($_POST['between_end_day']));
	}

	$view_date = $monthly_year.$monthly_month;
	$view_start_date = "$between_start_year-$between_start_month-$between_start_day";
	$view_end_date = "$between_end_year-$between_end_month-$between_end_day";

	//データ取得用のSQL文作成
	$list_sql = " SELECT al.media_id, al.media_publisher_id, "
				. " m.media_name, mp.publisher_name, "
				. " SUM(al.click_price_client) as click_price_client, "
				. " SUM(al.click_price_media) as click_price_media, "
				. " SUM(IF(al.status <> 1, al.action_price_client, NULL)) as action_price_client, "
				. " SUM(IF(al.status <> 1, al.action_price_media, NULL)) as action_price_media, "
				. " COUNT(al.status) as click_count, "
				. " COUNT(IF(al.status <> 1, al.status, NULL)) as action_count "
				. " FROM action_logs as al "
				. " LEFT JOIN media as m on al.media_id = m.id "
				. " LEFT JOIN media_publishers as mp on al.media_publisher_id = mp.id "
				. " WHERE al.deleted_at is NULL ";

	if($media_publisher_id != 0) {
		$list_sql .= " AND al.media_publisher_id = '$media_publisher_id' ";
	}

	if($select_date_type == 1) {

		//年月指定
		$list_sql .= " AND ( "
					. " (al.status = 1 AND DATE_FORMAT(al.created_at,'%Y%c') = '$view_date') "
					. " OR "
					. " (al.status = 2 AND DATE_FORMAT(al.action_complete_date,'%Y%c') = '$view_date') "
					// ステータス3 特殊なケース 例)ユーザークレーム等で成果を上げる
					. " OR "
					. " (al.status = 3 AND DATE_FORMAT(al.created_at,'%Y%c') = '$view_date') "
					. " ) ";

	} elseif($select_date_type == 2) {

		//期間指定
		$list_sql .= " AND ( "
					. " (al.status = 1 AND al.created_at BETWEEN '$view_start_date 00:00:00' AND '$view_end_date 23:59:59') "
					. " OR "
					. " (al.status = 2 AND al.action_complete_date BETWEEN '$view_start_date 00:00:00' AND '$view_end_date 23:59:59') "
					// ステータス3 特殊なケース 例)ユーザークレーム等で成果を上げる
					. " OR "
					. " (al.status = 3 AND al.created_at BETWEEN '$view_start_date 00:00:00' AND '$view_end_date 23:59:59') "
					. " ) ";

	}

	$list_sql .= " GROUP BY al.media_id "
				. " ORDER BY al.media_publisher_id ASC, al.media_id ASC ";

	$list_count = 0;

	$db_result = $common_dao->db_query($list_sql);
	if($db_result){

		foreach($db_result as $row) {
			$mp_id = $row['media_publisher_id'];

			$summary[$mp_id]['media_id'] = $row['media_id'];
			$summary[$mp_id]['media_name'] = $row['media_name'];
			$summary[$mp_id]['media_publisher_id'] = $row['media_publisher_id'];
			$summary[$mp_id]['publisher_name'] = $row['publisher_name'];

			$m_count[$mp_id][] = $row['media_id'];
			$m_count[$mp_id] = array_unique($m_count[$mp_id]);
			$summary[$mp_id]['media_count'] = count($m_count[$mp_id]);

			$summary[$mp_id]['click_count'] += $row['click_count'];
			$summary[$mp_id]['action_count'] += $row['action_count'];
			$summary[$mp_id]['total_price'] += $row['click_price_media'] + $row['action_price_media'];
		}

		$smarty->assign("list", $summary);
		$list_count = count($summary);
	}else{
		$error_message .= "ＤＢからのデータの取得に失敗しました。(su0000)";
	}
	$smarty->assign("list_count", $list_count);
	$smarty->assign("error_message", $error_message);

	$search['media_publisher_id'] = $media_publisher_id;
	$search['select_date_type'] = $select_date_type;
	$search['monthly_year'] = $monthly_year;
	$search['monthly_month'] = $monthly_month;
	$search['between_start_year'] = $between_start_year;
	$search['between_start_month'] = $between_start_month;
	$search['between_start_day'] = $between_start_day;
	$search['between_end_year'] = $between_end_year;
	$search['between_end_month'] = $between_end_month;
	$search['between_end_day'] = $between_end_day;

	$smarty->assign("search", $search);

	$smarty->assign("type", $select_date_type);
	$smarty->assign("date", $view_date);
	$smarty->assign("start_date", $view_start_date);
	$smarty->assign("end_date", $view_end_date);

		// ページを表示
	$smarty->display("./summary_media_publisher.tpl");
	exit();
}else{
	header('Location: ./login.php?error=1');
	exit();
}

function do_escape_quotes($str){
	//magic_quotesが有効ならクウォート部分を除去
	if(get_magic_quotes_gpc()){
		$str = stripslashes($str);
	}
	return $str;
}
?>