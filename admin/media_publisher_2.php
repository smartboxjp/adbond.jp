<?php
// 共通設定
require_once( '../common/CommonAdminBase.php' );
require_once( '../common/CommonDao.php' );
require_once( '../dto/LoginUser.php' );
require_once( '../dao/MediaLoginUserDao.php' );
require_once( '../dto/MediaLoginUser.php' );
require_once( '../dao/MediaPublisherDao.php' );
require_once( '../dto/MediaPublisher.php' );
require_once( '../dao/MediaGroupDao.php' );
require_once( '../dto/MediaGroup.php' );
require_once( '../dao/PrefDao.php' );
require_once( '../dto/Pref.php' );

session_start();


	if($mode != ''){
		if($mode == 'new_regist'){
			$smarty->assign("mode", 'insert_commit');
			$smarty->assign("sub_title", '新規追加');

			// ページを表示
			$smarty->display("./.tpl");
			exit();
		}elseif($mode == 'insert_commit'){

			$error_flag = 0;

			if($publisher_name == "") {
				$error_message = "企業名/個人名を入力してください。";
				$error_flag = 1;
			} elseif($login_id == "") {
				$error_message = "ログインIDを入力してください。";
				$error_flag = 1;
			} elseif($login_pass == "") {
				$error_message = "パスワードを入力してください。";
				$error_flag = 1;
			}

			//既に登録されたログイン名か確認
			if(is_null($media_login_user_dao->getMediaLoginUserByLoginId($login_id))) {
				$media_login_user_dao->transaction_start();

				$media_login_user = new MediaLoginUser();
				$media_login_user->setUserName($publisher_name);
				$media_login_user->setLoginId($login_id);
				$media_login_user->setLoginPass($login_pass);

				//INSERTを実行
				$db_result = $media_login_user_dao->insertMediaLoginUser($media_login_user, $result_message);
				if($db_result) {
					$media_login_user_dao->transaction_end();

					$insert_record = $media_login_user_dao->getMediaLoginUserByLoginId($login_id);
					if(!is_null($insert_record)) {
						$login_user_id = $insert_record->getId();
					} else {
						$error_message = "ＤＢからのデータの取得に失敗しました。(su0000)";
						$error_flag = 1;
					}
				} else {
					$error_message = $result_message;

					$media_login_user_dao->transaction_rollback();

					$error_flag = 1;
				}
			} else {
				$error_message = "入力されたログインIDは登録されてます。";
				$error_flag = 1;
			}

			if($error_flag == 0) {
				$media_publisher_dao->transaction_start();

				$media_publisher = new MediaPublisher();
				$media_publisher->setLoginUserId($login_user_id);
				$media_publisher->setMediaGroupId($media_group_id);
				$media_publisher->setPublisherName($publisher_name);
				$media_publisher->setContactPerson($contact_person);
				$media_publisher->setTel($tel);
				$media_publisher->setFax($fax);
				$media_publisher->setEmail($email);
				$media_publisher->setZipcode1($zipcode1);
				$media_publisher->setZipcode2($zipcode2);
				$media_publisher->setPref($pref);
				$media_publisher->setAddress1($address1);
				$media_publisher->setAddress2($address2);
				$media_publisher->setTransferType($transfer_type);
				$media_publisher->setBankName($bank_name);
				$media_publisher->setBranchName($branch_name);
				$media_publisher->setAccountType($account_type);
				$media_publisher->setAccountHolder($account_holder);
				$media_publisher->setAccountNumber($account_number);
				$media_publisher->setPostalAccountHolder($postal_account_holder);
				$media_publisher->setPostalAccountNumber($postal_account_number);
				$media_publisher->setStatus($status);

				//INSERTを実行
				$db_result = $media_publisher_dao->insertMediaPublisher($media_publisher, $result_message);
				if($db_result) {
					$media_publisher_dao->transaction_end();

					$smarty->assign("info_message", $result_message);

					view_list();
				} else {
					$media_publisher_dao->transaction_rollback();

					$smarty->assign("error_message", $result_message);

					$form_data = array('id' => $id,
										'login_id' => $login_id,
										'login_pass' => $login_pass,
										'media_group_id' => $media_group_id,
										'publisher_name' => $publisher_name,
										'contact_person' => $contact_person,
										'tel' => $tel,
										'fax' => $fax,
										'email' => $email,
										'zipcode1' => $zipcode1,
										'zipcode2' => $zipcode2,
										'pref' => $pref,
										'address1' => $address1,
										'address2' => $address2,
										'transfer_type' => $transfer_type,
										'bank_name' => $bank_name,
										'branch_name' => $branch_name,
										'account_type' => $account_type,
										'account_holder' => $account_holder,
										'account_number' => $account_number,
										'postal_account_holder' => $postal_account_holder,
										'postal_account_number' => $postal_account_number,
										'status' => $status);

					$smarty->assign("form_data", $form_data);

					$smarty->assign("mode", 'insert_commit');
					$smarty->assign("sub_title", '新規追加');

					// ページを表示
					$smarty->display("./.tpl");
					exit();
				}
			} else {
				$smarty->assign("error_message", $error_message);

				$form_data = array('id' => $id,
									'login_id' => $login_id,
									'login_pass' => $login_pass,
									'media_group_id' => $media_group_id,
									'publisher_name' => $publisher_name,
									'contact_person' => $contact_person,
									'tel' => $tel,
									'fax' => $fax,
									'email' => $email,
									'zipcode1' => $zipcode1,
									'zipcode2' => $zipcode2,
									'pref' => $pref,
									'address1' => $address1,
									'address2' => $address2,
									'transfer_type' => $transfer_type,
									'bank_name' => $bank_name,
									'branch_name' => $branch_name,
									'account_type' => $account_type,
									'account_holder' => $account_holder,
									'account_number' => $account_number,
									'postal_account_holder' => $postal_account_holder,
									'postal_account_number' => $postal_account_number,
									'status' => $status);

				$smarty->assign("form_data", $form_data);

				$smarty->assign("mode", 'insert_commit');
				$smarty->assign("sub_title", '新規追加');

				// ページを表示
				$smarty->display("./media_publisher_input_2.tpl");
				exit();
			}
		}elseif($mode == 'edit'){
			$media_publisher = new MediaPublisher();
			$media_publisher = $media_publisher_dao->getMediaPublisherById($id);
			$login_user_id = $media_publisher->getLoginUserId();

			$media_login_user = new MediaLoginUser();
			$media_login_user = $media_login_user_dao->getMediaLoginUserById($login_user_id);

			if(!is_null($media_publisher) && !is_null($media_login_user)) {
				$form_data = array('id' => $media_publisher->getId(),
									'login_id' => $media_login_user->getLoginId(),
									'login_pass' => $media_login_user->getLoginPass(),
									'media_group_id' => $media_publisher->getMediaGroupId(),
									'publisher_name' => $media_publisher->getPublisherName(),
									'contact_person' => $media_publisher->getContactPerson(),
									'tel' => $media_publisher->getTel(),
									'fax' => $media_publisher->getFax(),
									'email' => $media_publisher->getEmail(),
									'zipcode1' => $media_publisher->getZipcode1(),
									'zipcode2' => $media_publisher->getZipcode2(),
									'pref' => $media_publisher->getPref(),
									'address1' => $media_publisher->getAddress1(),
									'address2' => $media_publisher->getAddress2(),
									'transfer_type' => $media_publisher->getTransferType(),
									'bank_name' => $media_publisher->getBankName(),
									'branch_name' => $media_publisher->getBranchName(),
									'account_type' => $media_publisher->getAccountType(),
									'account_holder' => $media_publisher->getAccountHolder(),
									'account_number' => $media_publisher->getAccountNumber(),
									'postal_account_holder' => $media_publisher->getPostalAccountHolder(),
									'postal_account_number' => $media_publisher->getPostalAccountNumber(),
									'status' => $media_publisher->getStatus());

				$smarty->assign("form_data", $form_data);

				$smarty->assign("mode", 'update_commit');
				$smarty->assign("sub_title", '編集');

				// ページを表示
				$smarty->display("./media_publisher_input_2.tpl");
				exit();
			} else {
				$error_message .= "該当するデータはありません。";
				$smarty->assign("error_message", $error_message);

				$smarty->assign("mode", 'insert_commit');
				$smarty->assign("sub_title", '新規追加');

				// ページを表示
				$smarty->display("./media_publisher_input_2.tpl");
				exit();
			}
		}elseif($mode == 'update_commit'){
		}
	}

//			$error_flag = 0;
//
//			if($publisher_name == "") {
//				$error_message = "企業名/個人名を入力してください。";
//				$error_flag = 1;
//			} elseif($login_id == "") {
//				$error_message = "ログインIDを入力してください。";
//				$error_flag = 1;
//			} elseif($login_pass == "") {
//				$error_message = "パスワードを入力してください。";
//				$error_flag = 1;
//			}
//
//			$media_publisher = new MediaPublisher();
//			$media_publisher = $media_publisher_dao->getMediaPublisherById($id);
//			$login_user_id = $media_publisher->getLoginUserId();
//
//			//既に登録されたログイン名か確認
//			$result_data = $media_login_user_dao->getMediaLoginUserByLoginId($login_id);
//
//			if(is_null($result_data) || $login_user_id == $result_data->getId()) {
//				$media_login_user_dao->transaction_start();
//
//				$media_login_user = new MediaLoginUser();
//				$media_login_user->setId($login_user_id);
//				$media_login_user->setUserName($publisher_name);
//				$media_login_user->setLoginId($login_id);
//				$media_login_user->setLoginPass($login_pass);
//
//				//UPDATEを実行
//				$db_result = $media_login_user_dao->updateMediaLoginUser($media_login_user, $result_message);
//				if($db_result) {
//					$media_login_user_dao->transaction_end();
//
//					$insert_record = $media_login_user_dao->getMediaLoginUserByLoginId($login_id);
//					if(!is_null($insert_record)) {
//						$login_user_id = $insert_record->getId();
//					} else {
//						$error_message = "ＤＢからのデータの取得に失敗しました。(su0000)";
//						$error_flag = 1;
//					}
//				} else {
//					$error_message = $result_message;
//
//					$media_login_user_dao->transaction_rollback();
//
//					$error_flag = 1;
//				}
//			} else {
//				$error_message = "入力されたログインIDは登録されてます。";
//				$error_flag = 1;
//			}
//
//			if($error_flag == 0) {
//				$media_publisher_dao->transaction_start();
//
//				$media_publisher = new MediaPublisher();
//				$media_publisher->setId($id);
//				$media_publisher->setLoginUserId($login_user_id);
//				$media_publisher->setMediaGroupId($media_group_id);
//				$media_publisher->setPublisherName($publisher_name);
//				$media_publisher->setContactPerson($contact_person);
//				$media_publisher->setTel($tel);
//				$media_publisher->setFax($fax);
//				$media_publisher->setEmail($email);
//				$media_publisher->setZipcode1($zipcode1);
//				$media_publisher->setZipcode2($zipcode2);
//				$media_publisher->setPref($pref);
//				$media_publisher->setAddress1($address1);
//				$media_publisher->setAddress2($address2);
//				$media_publisher->setTransferType($transfer_type);
//				$media_publisher->setBankName($bank_name);
//				$media_publisher->setBranchName($branch_name);
//				$media_publisher->setAccountType($account_type);
//				$media_publisher->setAccountHolder($account_holder);
//				$media_publisher->setAccountNumber($account_number);
//				$media_publisher->setPostalAccountHolder($postal_account_holder);
//				$media_publisher->setPostalAccountNumber($postal_account_number);
//				$media_publisher->setStatus($status);
//
//				//UPDATEを実行
//				$db_result = $media_publisher_dao->updateMediaPublisher($media_publisher, $result_message);
//				if($db_result) {
//					$media_publisher_dao->transaction_end();
//
//					$smarty->assign("info_message", $result_message);
//
//					view_list();
//				} else {
//					$media_publisher_dao->transaction_rollback();
//
//					$smarty->assign("error_message", $result_message);
//
//					$form_data = array('id' => $id,
//										'login_id' => $login_id,
//										'login_pass' => $login_pass,
//										'media_group_id' => $media_group_id,
//										'publisher_name' => $publisher_name,
//										'contact_person' => $contact_person,
//										'tel' => $tel,
//										'fax' => $fax,
//										'email' => $email,
//										'zipcode1' => $zipcode1,
//										'zipcode2' => $zipcode2,
//										'pref' => $pref,
//										'address1' => $address1,
//										'address2' => $address2,
//										'transfer_type' => $transfer_type,
//										'bank_name' => $bank_name,
//										'branch_name' => $branch_name,
//										'account_type' => $account_type,
//										'account_holder' => $account_holder,
//										'account_number' => $account_number,
//										'postal_account_holder' => $postal_account_holder,
//										'postal_account_number' => $postal_account_number,
//										'status' => $status);
//
//					$smarty->assign("form_data", $form_data);
//
//					$smarty->assign("mode", 'update_commit');
//					$smarty->assign("sub_title", '編集');
//
//					// ページを表示
//					$smarty->display("./media_publisher_input_2.tpl");
//					exit();
//				}
//			} else {
//				$smarty->assign("error_message", $error_message);
//
//				$form_data = array('id' => $id,
//									'login_id' => $login_id,
//									'login_pass' => $login_pass,
//									'media_group_id' => $media_group_id,
//									'publisher_name' => $publisher_name,
//									'contact_person' => $contact_person,
//									'tel' => $tel,
//									'fax' => $fax,
//									'email' => $email,
//									'zipcode1' => $zipcode1,
//									'zipcode2' => $zipcode2,
//									'pref' => $pref,
//									'address1' => $address1,
//									'address2' => $address2,
//									'transfer_type' => $transfer_type,
//									'bank_name' => $bank_name,
//									'branch_name' => $branch_name,
//									'account_type' => $account_type,
//									'account_holder' => $account_holder,
//									'account_number' => $account_number,
//									'postal_account_holder' => $postal_account_holder,
//									'postal_account_number' => $postal_account_number,
//									'status' => $status);
//
//				$smarty->assign("form_data", $form_data);
//
//				$smarty->assign("mode", 'update_commit');
//				$smarty->assign("sub_title", '編集');
//
//				// ページを表示
//				$smarty->display("./media_publisher_input_2.tpl");
//				exit();
//			}
//		}elseif($mode == 'search'){
//
//			$search_list_sql = " SELECT mp.*, ml.login_id, ml.login_pass "
//							. " FROM media_publishers as mp "
//							. " LEFT JOIN media_login_users as ml on mp.login_user_id = ml.id "
//							. " WHERE mp.deleted_at is NULL ";
//
//			//絞込みWHERE文格納配列
//			$add_where = array();
//
//			switch($method) {
//				case 1:	//「完全一致」
//					if($search_id != "") {
//						$add_where[] = "mp.id = '$search_id' ";
//					}
//					if($search_name != "") {
//						$add_where[] = "mp.publisher_name = '$search_name' ";
//					}
//					break;
//				case 2:	//「前方一致」
//					if($search_id != "") {
//						$add_where[] = "mp.id LIKE '$search_id%' ";
//					}
//					if($search_name != "") {
//						$add_where[] = "mp.publisher_name LIKE '$search_name%' ";
//					}
//					break;
//				case 3:	//「後方一致」
//					if($search_id != "") {
//						$add_where[] = "mp.id LIKE '%$search_id' ";
//					}
//					if($search_name != "") {
//						$add_where[] = "mp.publisher_name LIKE '%$search_name' ";
//					}
//					break;
//				case 4:	//「あいまい」
//					if($search_id != "") {
//						$add_where[] = "mp.id LIKE '%$search_id%' ";
//					}
//					if($search_name != "") {
//						$add_where[] = "mp.publisher_name LIKE '%$search_name%' ";
//					}
//					break;
//			}
//
//			if(count($add_where) > 0) {
//				if($reverse == 1) {
//					$search_list_sql .= "AND NOT(".implode("OR ", $add_where).") ";
//				} else {
//					$search_list_sql .= "AND ".implode("AND ", $add_where);
//				}
//			}
//
//			if($created_at_flag == 1) {
//				if($reverse == 1) {
//					$search_list_sql .= "AND NOT(mp.created_at BETWEEN '$s_year-$s_month-$s_day 00:00:00' AND '$e_year-$e_month-$e_day 23:59:59') ";
//				} else {
//					$search_list_sql .= "AND mp.created_at BETWEEN '$s_year-$s_month-$s_day 00:00:00' AND '$e_year-$e_month-$e_day 23:59:59' ";
//				}
//
//				$smarty->assign("set_s_year", $s_year);
//				$smarty->assign("set_s_month", $s_month);
//				$smarty->assign("set_s_day", $s_day);
//				$smarty->assign("set_e_year", $e_year);
//				$smarty->assign("set_e_month", $e_month);
//				$smarty->assign("set_e_day", $e_day);
//			}
//
//			$search_list_sql .= "ORDER BY mp.$sort $order ";
//
//			$db_result = $common_dao->db_query($search_list_sql);
//			if($db_result){
//				$smarty->assign("list", $db_result);
//				$list_count = count($db_result);
//			}else{
//				$error_message .= "該当するデータはありません。";
//			}
//			$smarty->assign("list_count", $list_count);
//			$smarty->assign("error_message", $error_message);
//
//			$search['method'] = $method;
//			$search['reverse'] = $reverse;
//			$search['sort'] = $sort;
//			$search['order'] = $order;
//			$search['id'] = $search_id;
//			$search['name'] = $search_name;
//			$search['created_at_flag'] = $created_at_flag;
//
//			$smarty->assign("search", $search);
//
//			$smarty->assign("mode", 'search');
//			$smarty->assign("sub_title", '検索結果');
//
//			// ページを表示
//			$smarty->display("./media_publisher_list.tpl");
//			exit();
//		}elseif($mode == 'delete'){
//			$media_publisher_dao->transaction_start();
//
//			$record = $media_publisher_dao->getMediaPublisherById($id);
//
//			if(!is_null($record)) {
//				$login_user_id = $record->getLoginUserId();
//
//				$db_result = $media_publisher_dao->deleteMediaPublisher($id, $result_message);
//				if($db_result){
//					$media_publisher_dao->transaction_end();
//
//					$media_login_user_dao->transaction_start();
//
//					if(!is_null($media_login_user_dao->getMediaLoginUserById($login_user_id))) {
//						$db_result = $media_login_user_dao->deleteMediaLoginUser($login_user_id, $result_message);
//						if($db_result) {
//							$media_login_user_dao->transaction_end();
//
//							$smarty->assign("info_message", $result_message);
//
//							view_list();
//						} else {
//							$media_login_user_dao->transaction_rollback();
//
//							$smarty->assign("error_message", $result_message);
//
//							view_list();
//						}
//					} else {
//						$media_login_user_dao->transaction_rollback();
//
//						$error_message = "ＤＢの更新に失敗しました。";
//						$smarty->assign("error_message", $error_message);
//
//						view_list();
//					}
//				}else{
//					$media_publisher_dao->transaction_rollback();
//
//					$smarty->assign("error_message", $result_message);
//
//					view_list();
//				}
//			}else{
//				$media_publisher_dao->transaction_rollback();
//
//				$error_message = "ＤＢの更新に失敗しました。";
//				$smarty->assign("error_message", $error_message);
//
//				view_list();
//			}
//		}else{
//			view_list();
//		}
//	}else{
//		view_list();
//	}
//}else{
//	header('Location: ./login.php?error=1');
//	exit();
//}
//
//function view_list(){
//	global $common_dao, $smarty, $list_sql, $error_message;
//
//	$list_count = 0;
//
//	$db_result = $common_dao->db_query($list_sql);
//	if($db_result){
//		$smarty->assign("list", $db_result);
//		$list_count = count($db_result);
//	}else{
//		$error_message .= "ＤＢからのデータの取得に失敗しました。(su0000)";
//	}
//	$smarty->assign("list_count", $list_count);
//	$smarty->assign("error_message", $error_message);
//
//	// ページを表示
//	$smarty->display("./media_publisher_list.tpl");
//	exit();
//}
//
//function do_escape_quotes($str){
//	//magic_quotesが有効ならクウォート部分を除去
//	if(get_magic_quotes_gpc()){
//		$str = stripslashes($str);
//	}
//	return $str;
//}
?>