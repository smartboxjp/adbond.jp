<?php
// 共通設定
require_once( '../common/CommonAdminBase.php' );
require_once( '../common/CommonDao.php' );
require_once( '../dto/LoginUser.php' );

session_start();

if(isset($_SESSION['logon_token']) && $_SESSION['logon_token'] != ''){
	$login_user = new LoginUser();
	$login_user = $_SESSION['login_user'];

	// Smartyオブジェクト取得
	$smarty =& getSmartyObj();

	$smarty->assign("title", "Test Top Page");
	$smarty->assign("login_user", $login_user );

	$common_dao = new CommonDao();

	$log_type = do_escape_quotes($_GET['log_type']);

	$download_date = do_escape_quotes($_GET['download_date']);

	$now_date = getdate();
	$smarty->assign("set_s_year", $now_date['year']);
	$smarty->assign("set_s_month", $now_date['mon']);

	//開始ページ数 取得
	if(isset($_GET['page']) && $_GET['page'] != "") {
		$page = $_GET['page'];
	} else {
		$page = 1;
	}
	if(isset($_GET['mode']) && $_GET['mode'] != '') {
		if($_GET['mode'] == 'search') {

			$s_year = do_escape_quotes($_GET['s_year']);
			$s_month = do_escape_quotes($_GET['s_month']);

			$search_date = $s_year.sprintf("%02d",$s_month);

			if($log_type == 1) {

				$list_sql = " SELECT DATE_FORMAT(created_at,'%Y%m%d') as download_date, "
							. " DATE_FORMAT(created_at,'%Y年%m月%d日') as log_date, "
							. " COUNT(*) as log_count "
							. " FROM action_logs "
							. " WHERE DATE_FORMAT(created_at,'%Y%m') = '$search_date' "
							. " AND (status = 1 OR status = 2) "
							. " AND deleted_at is NULL "
							. " GROUP BY log_date "
							. " ORDER BY created_at ASC ";

			} elseif($log_type == 2) {

				$list_sql = " SELECT DATE_FORMAT(action_complete_date,'%Y%m%d') as download_date, "
							. " DATE_FORMAT(action_complete_date,'%Y年%m月%d日') as log_date, "
							. " COUNT(*) as log_count "
							. " FROM action_logs "
							. " WHERE DATE_FORMAT(action_complete_date,'%Y%m') = '$search_date' "
							. " AND status = 2 "
							. " AND deleted_at is NULL "
							. " GROUP BY log_date "
							. " ORDER BY created_at ASC ";

			} elseif($log_type == 3) {

				$list_sql = " SELECT DATE_FORMAT(created_at,'%Y%m%d') as download_date, "
							. " DATE_FORMAT(created_at,'%Y年%m月%d日') as log_date, "
							. " COUNT(*) as log_count "
							. " FROM point_back_logs "
							. " WHERE DATE_FORMAT(created_at,'%Y%m') = '$search_date' "
							. " AND deleted_at is NULL "
							. " GROUP BY log_date "
							. " ORDER BY created_at ASC";

			}

			$db_result = $common_dao->db_query($list_sql);
			if($db_result){
				$smarty->assign("list", $db_result);
				$list_count = count($db_result);
			}else{
				$error_message .= "該当するデータはありません。";
			}

			$smarty->assign("error_message", $error_message);

			$smarty->assign("set_s_year", $s_year);
			$smarty->assign("set_s_month", $s_month);

			$smarty->assign("search_date", $search_date);
			$smarty->assign("log_type", $log_type);

			// ページを表示
			$smarty->display("./result_log.tpl");
			exit();
		} elseif($_GET['mode'] == 'detail') {

			if($log_type == 1) {

				$download_sql = " SELECT created_at, media_id, advert_id, carrier_id, user_agent, uid, ip_address, host_name, link_url, status ";

				$add_from =  " FROM action_logs "
							. " WHERE DATE_FORMAT(created_at,'%Y%m%d') = '$download_date' ";

				$add_where =  " AND (status = 1 OR status = 2) "
							. " AND deleted_at is NULL "
							. " ORDER BY created_at ASC ";

			} elseif($log_type == 2) {

				$download_sql = " SELECT action_complete_date, media_id, advert_id, carrier_id, user_agent, uid, ip_address, host_name, status ";

				$add_from = " FROM action_logs "
							. " WHERE DATE_FORMAT(action_complete_date,'%Y%m%d') = '$download_date' ";

				$add_where =  " AND status = 2 "
							. " AND deleted_at is NULL "
							. " ORDER BY action_complete_date ASC ";

			} elseif($log_type == 3) {

				$download_sql = " SELECT created_at, media_id, advert_id, point_back_parameter, point_back_url ";

				$add_from =  " FROM point_back_logs "
							. " WHERE DATE_FORMAT(created_at,'%Y%m%d') = '$download_date' ";

				$add_where =  " AND deleted_at is NULL "
							. " ORDER BY created_at ASC ";

			}

// ------------------------------------------------------------------- 2010/12/06 追加
			//表示最大件数
			$limit_max = 1000;

			$count_sql = " SELECT count(*) as count ";
			$count_sql .= $add_from;
			$count_sql .= $add_where;

			$db_result1 = $common_dao->db_query($count_sql);
			if($db_result1){
				$list_count = $db_result1[0]['count'];
				$page_max = ceil($list_count / $limit_max);

				$page = ($page < 1) ? 1 : $page;
				$page = ($page > $page_max) ? $page_max : $page;

				//PREV値 設定
				$prev = ($page > 1) ? $page - 1 : 0;

				//NEXT値 設定
				$next = ($page < $page_max) ? $page + 1 : 0;

				//LIMITの開始 設定
				$limit_start = ($page -1) * $limit_max;
			}

			$download_sql .= $add_from;
			$download_sql .= $add_where;
			$download_sql .= " LIMIT $limit_start, $limit_max ";
// -----------------------------------------------------------------------------------


			$db_result = $common_dao->db_query($download_sql);
			if($db_result){
				$smarty->assign("list", $db_result);
				$smarty->assign("list_count", count($db_result));
			}else{
				$error_message .= "ＤＢからのデータの取得に失敗しました。(su0001)";
			}

			$mode = $_GET['mode'];
			$smarty->assign("list_count", $list_count);
			$smarty->assign("page", $page);
			$smarty->assign("page_max", $page_max);
			$smarty->assign("prev", $prev);
			$smarty->assign("next", $next);
			$smarty->assign("mode", $mode);
			$smarty->assign("s_year", $s_year);
			$smarty->assign("s_month", $s_month);

			$smarty->assign("error_message", $error_message);

			$smarty->assign("download_date", $download_date);
			$smarty->assign("log_type", $log_type);

			if($log_type == 1) {
				// ページを表示
				$smarty->display("./result_log_click.tpl");
				exit();
			} elseif($log_type == 2) {
				// ページを表示
				$smarty->display("./result_log_action.tpl");
				exit();
			} elseif($log_type == 3) {
				// ページを表示
				$smarty->display("./result_log_point_back.tpl");
				exit();
			}
		}
	} else {
		// ページを表示
		$smarty->display("./result_log.tpl");
		exit();
	}
}else{
	header('Location: ./login.php?error=1');
	exit();
}

function do_escape_quotes($str){
	//magic_quotesが有効ならクウォート部分を除去
	if(get_magic_quotes_gpc()){
		$str = stripslashes($str);
	}
	return $str;
}
?>