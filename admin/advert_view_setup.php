<?php
// 共通設定
require_once( '../common/CommonAdminBase.php' );
require_once( '../common/CommonDao.php' );
require_once( '../dto/LoginUser.php' );
require_once( '../dao/AdvertDao.php' );
require_once( '../dto/Advert.php' );
require_once( '../dao/MediaDao.php' );
require_once( '../dto/Media.php' );
require_once( '../dao/MediaPublisherDao.php' );
require_once( '../dto/MediaPublisher.php' );
require_once( '../dao/MediaCategoryDao.php' );
require_once( '../dto/MediaCategory.php' );
require_once( '../dao/AdvertViewMediaSetDao.php' );
require_once( '../dto/AdvertViewMediaSet.php' );


session_start();

if(isset($_SESSION['logon_token']) && $_SESSION['logon_token'] != ''){
	$login_user = new LoginUser();
	$login_user = $_SESSION['login_user'];

	// Smartyオブジェクト取得
	$smarty =& getSmartyObj();


	$smarty->assign("login_user", $login_user );

	$common_dao = new CommonDao();
	$media_category_dao = new MediaCategoryDao();
	$advert_view_media_set_dao = new AdvertViewMediaSetDao();

	if(isset($_POST['advert_id']) && $_POST['advert_id'] != '') {
		$advert_id = $_POST['advert_id'];
	} elseif(isset($_GET['advert_id']) && $_GET['advert_id'] != '') {
		$advert_id = $_GET['advert_id'];
	} else {
		$davert_id = "";
	}
	$smarty->assign("advert_id", $advert_id);

	if(isset($_POST['category_id']) && $_POST['category_id'] != '') {
		$category_id = $_POST['category_id'];
	} elseif(isset($_GET['category_id']) && $_GET['category_id'] != '') {
		$category_id = $_GET['category_id'];
	} else {
		$category_id = "";
	}
	$smarty->assign("category_id", $category_id);

	if(isset($_POST['publisher_id']) && $_POST['publisher_id'] != '') {
		$publisher_id = $_POST['publisher_id'];
	} elseif(isset($_GET['publisher_id']) && $_GET['publisher_id'] != '') {
		$publisher_id = $_GET['publisher_id'];
	} else {
		$publisher_id = "";
	}
	$smarty->assign("publisher_id", $publisher_id);

	$check_category = array();
	$all_category = array();
	$check_publisher = array();
	$all_publisher = array();

	//広告カテゴリー
	$media_category_array = array();
	foreach($media_category_dao->getAllMediaCategory() as $val){
		$row_array = array('id' => $val->getId(), 'name' => $val->getName());
		$media_category_array[$val->getId()] = $row_array;
		$all_category[$val->getId()] = $val->getId();
		if(is_null($advert_view_media_set_dao->getAdvertViewMediaSetByMidAid($advert_id, 0, 0, $val->getId()))) {
			$now_category[$val->getId()] = $val->getId();
		}
	}
	$smarty->assign("media_category_array", $media_category_array);
	$smarty->assign("check_category", $now_category);

	$sql = " SELECT m.*, mp.publisher_name as publisher_name "
		. " FROM media as m "
		. " LEFT JOIN media_publishers as mp on m.media_publisher_id = mp.id "
		. " WHERE m.deleted_at is NULL "
		. " AND m.media_category_id = '$category_id' "
		. " ORDER BY m.id ASC ";

	$db_result = $common_dao->db_query($sql);
	if($db_result){
		foreach($db_result as $row) {
			$mp_id = $row['media_publisher_id'];
			$m_id = $row['id'];
			$media_array[$mp_id]['publisher_id'] = $row['media_publisher_id'];
			$media_array[$mp_id]['publisher_name'] = $row['publisher_name'];
			$media_array[$mp_id]['media'][$m_id]['media_id'] = $row['id'];
			$media_array[$mp_id]['media'][$m_id]['media_name'] = $row['media_name'];

			$all_publisher[$mp_id] = $row['media_publisher_id'];

			if(is_null($advert_view_media_set_dao->getAdvertViewMediaSetByMidAid($advert_id, 0, $mp_id, $category_id))) {
				$now_publisher[$mp_id] = $row['media_publisher_id'];
			}

			if($mp_id == $publisher_id) {
				$all_media[$row['id']] = $row['id'];

				if(is_null($advert_view_media_set_dao->getAdvertViewMediaSetByMidAid($advert_id, $row['id'], $mp_id, $category_id))) {
					$now_media[$row['id']] = $row['id'];
				}
			}
		}
		$smarty->assign("media_array", $media_array);
		$smarty->assign("check_publisher", $now_publisher);
		$smarty->assign("check_media", $now_media);

	}else{
		$error_message .= "ＤＢからのデータの取得に失敗しました。(su0000)";
	}

	if(isset($_POST['mode']) && $_POST['mode'] == "setup") {
		//カテゴリー設置
		if(isset($_POST['check_category'])) {
			$check_category = $_POST['check_category'];
			$diff_category = array_diff($all_category, $check_category);
		} else {
			$diff_category = $all_category;
		}

		$smarty->assign("check_category", $check_category);

		$advert_view_media_set_dao = new AdvertViewMediaSetDao();

		foreach($check_category as $val) {
			if(!is_null($result = $advert_view_media_set_dao->getAdvertViewMediaSetByMidAid($advert_id, 0, 0, $val))) {
				$advert_view_media_set_dao->transaction_start();

				$delete_id = $result->getId();

				//DELETEを実行
				$db_result = $advert_view_media_set_dao->deleteAdvertViewMediaSet($delete_id, $result_message);
				if($db_result) {
					$advert_view_media_set_dao->transaction_end();
				} else {
					$advert_view_media_set_dao->transaction_rollback();
				}
			}
		}

		foreach($diff_category as $val) {

			if(is_null($result = $advert_view_media_set_dao->getAdvertViewMediaSetByMidAid($advert_id, 0, 0, $val))) {
				$advert_view_media_set_dao->transaction_start();

				$advert_view_media_set = new AdvertViewMediaSet();
				$advert_view_media_set->setAdvertId($advert_id);
				$advert_view_media_set->setMediaId(0);
				$advert_view_media_set->setMediaPublisherId(0);
				$advert_view_media_set->setMediaCategoryId($val);

				//INSERTを実行
				$db_result = $advert_view_media_set_dao->insertAdvertViewMediaSet($advert_view_media_set, $result_message);
				if($db_result) {
					$advert_view_media_set_dao->transaction_end();
				} else {
					$advert_view_media_set_dao->transaction_rollback();
				}
			}
		}

		//媒体主設定
		if($category_id != 0) {
			if(isset($_POST['check_publisher'])) {
				$check_publisher = $_POST['check_publisher'];
				$diff_publisher = array_diff($all_publisher, $check_publisher);
			} else {
				$diff_publisher = $all_publisher;
			}

			$smarty->assign("check_publisher", $check_publisher);

			$advert_view_media_set_dao = new AdvertViewMediaSetDao();

			if(count($check_publisher) > 0) {
				foreach($check_publisher as $val) {
					if(!is_null($result = $advert_view_media_set_dao->getAdvertViewMediaSetByMidAid($advert_id, 0, $val, $category_id))) {
						$advert_view_media_set_dao->transaction_start();

						$delete_id = $result->getId();

						//DELETEを実行
						$db_result = $advert_view_media_set_dao->deleteAdvertViewMediaSet($delete_id, $result_message);
						if($db_result) {
							$advert_view_media_set_dao->transaction_end();
						} else {
							$advert_view_media_set_dao->transaction_rollback();
						}
					}
				}
			}

			foreach($diff_publisher as $val) {

				if(is_null($result = $advert_view_media_set_dao->getAdvertViewMediaSetByMidAid($advert_id, 0, $val, $category_id))) {
					$advert_view_media_set_dao->transaction_start();

					$advert_view_media_set = new AdvertViewMediaSet();
					$advert_view_media_set->setAdvertId($advert_id);
					$advert_view_media_set->setMediaId(0);
					$advert_view_media_set->setMediaPublisherId($val);
					$advert_view_media_set->setMediaCategoryId($category_id);

					//INSERTを実行
					$db_result = $advert_view_media_set_dao->insertAdvertViewMediaSet($advert_view_media_set, $result_message);
					if($db_result) {
						$advert_view_media_set_dao->transaction_end();
					} else {
						$advert_view_media_set_dao->transaction_rollback();
					}
				}
			}
		}

		//媒体設定
		if($publisher_id != 0) {
			if(isset($_POST['check_media'])) {
				$check_media = $_POST['check_media'];
				$diff_media = array_diff($all_media, $check_media);
			} else {
				$diff_media = $all_media;
			}

			$smarty->assign("check_media", $check_media);

			$advert_view_media_set_dao = new AdvertViewMediaSetDao();

			if(count($check_media) > 0) {
				foreach($check_media as $val) {
					if(!is_null($result = $advert_view_media_set_dao->getAdvertViewMediaSetByMidAid($advert_id, $val, $publisher_id, $category_id))) {
						$advert_view_media_set_dao->transaction_start();

						$delete_id = $result->getId();

						//DELETEを実行
						$db_result = $advert_view_media_set_dao->deleteAdvertViewMediaSet($delete_id, $result_message);
						if($db_result) {
							$advert_view_media_set_dao->transaction_end();
						} else {
							$advert_view_media_set_dao->transaction_rollback();
						}
					}
				}
			}

			foreach($diff_media as $val) {

				if(is_null($result = $advert_view_media_set_dao->getAdvertViewMediaSetByMidAid($advert_id, $val, $publisher_id, $category_id))) {
					$advert_view_media_set_dao->transaction_start();

					$advert_view_media_set = new AdvertViewMediaSet();
					$advert_view_media_set->setAdvertId($advert_id);
					$advert_view_media_set->setMediaId($val);
					$advert_view_media_set->setMediaPublisherId($publisher_id);
					$advert_view_media_set->setMediaCategoryId($category_id);

					//INSERTを実行
					$db_result = $advert_view_media_set_dao->insertAdvertViewMediaSet($advert_view_media_set, $result_message);
					if($db_result) {
						$advert_view_media_set_dao->transaction_end();
					} else {
						$advert_view_media_set_dao->transaction_rollback();
					}
				}
			}
		}
	}

	// ページを表示
	$smarty->display("./advert_view_setup.tpl");
	exit();
}else{
	header('Location: ./login.php?error=1');
	exit();
}

function do_escape_quotes($str){
	//magic_quotesが有効ならクウォート部分を除去
	if(get_magic_quotes_gpc()){
		$str = stripslashes($str);
	}
	return $str;
}

?>