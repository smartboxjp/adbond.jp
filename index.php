<?php
define('SMARTY_DIR', 'Smarty/libs/');
require_once(SMARTY_DIR . 'Smarty.class.php');
require_once( './common/CommonDao.php' );
require_once( './dao/AdvertDao.php' );
require_once( './dto/Advert.php' );

session_start();
session_regenerate_id(true);

// Smartyオブジェクト取得
$smarty = new Smarty();
$smarty->template_dir = './templates/web/';
$smarty->compile_id   = 'web';
$smarty->compile_dir  = './templates_c/';
$smarty->config_dir   = './config/';
$smarty->cache_dir    = './cache/';

$common_dao = new CommonDao();

if(isset($_GET['client_logon_error']) && $_GET['client_logon_error'] != ""){
	$smarty->assign("client_error_message", "※アカウントID、またはパスワードに誤りがあります。");
} else {
	$smarty->assign("client_error_message", "");
}

if(isset($_GET['media_login_error']) && $_GET['media_login_error'] != ""){
	$smarty->assign("media_error_message", "※アカウントID、またはパスワードに誤りがあります。");
} else {
	$smarty->assign("media_error_message", "");
}

// *********************************************************************
// 新着広告 取得
// *********************************************************************
$advert_Max_sql = " SELECT "
			. " id, advert_name, "
			. " DATE_FORMAT(created_at,'%Y年%m月%d日') AS created_date "
			. " FROM "
			. " advert "
			. " WHERE "
			. " status = '2' "
			. " AND "
			. " test_flag = '0' "
			. " AND "
			. " advert_category_id <> '11' "
			. " AND "
			. " advert_name not like '%アダルト%' "
			. " AND "
			. " advert_name not like '%※%' "
			. " AND "
			. " advert_name not like '%同人%' "
			. " AND "
			. " advert_name not like '%テスト%' "
			. " ORDER BY id DESC "
			. " limit 0, 10 ";

$row =  array();
$rec_count = 0;

$rec = $common_dao->db_query_ts($advert_Max_sql);
while($rec_count < count($rec)){
	$coming_soon_date[$rec_count] = ($row = $rec[$rec_count]);
	$smarty->assign("coming_str", $coming_soon_date);
	$rec_count += 1;
}

// *********************************************************************
// おすすめ広告 取得
// *********************************************************************

// おすすめ広告テーブルのレコードを取得

$advert_attention = array();

$advert_attention_sql = " SELECT "
	. " aa.* "
	. " FROM (("
	. " advert_attention AS aa "
	. " LEFT JOIN advert AS a ON aa.advert_id = a.id )"
	. " LEFT JOIN advert_clients AS ac ON a.advert_client_id = ac.id )"
	. " WHERE "
	. " aa.deleted_at is NULL "
	. " AND "
	. " a.status = '2' ";

//echo $advert_attention_sql . "<br />";

$rec2 = $common_dao->db_query_ts($advert_attention_sql);

foreach( $rec2 as $key => $value ){

	$advert_sql = " SELECT ";

	// イメージタイプ1
	if($value['ms_image_url_num'] == "1"){
		$advert_sql .= " ms_image_url_1 AS ms_image_url, ";
	}
	// イメージタイプ1
	if($value['ms_image_url_num'] == "2"){
		$advert_sql .= " ms_image_url_2 AS ms_image_url, ";
	}
	// イメージタイプ1
	if($value['ms_image_url_num'] == "3"){
		$advert_sql .= " ms_image_url_3 AS ms_image_url, ";
	}
	// イメージタイプ1
	if($value['ms_image_url_num'] == "4"){
		$advert_sql .= " ms_image_url_4 AS ms_image_url, ";
	}
	// テキストタイプ1
	if($value['ms_image_url_num'] == "5"){
		$advert_sql .= " ms_image_url_5 AS ms_image_url, ";
	}

	// テキストタイプ1
	if($value['ms_text_num'] == "1"){
		$advert_sql .= " ms_text_1 AS ms_text, ";
	}
	// テキストタイプ1
	if($value['ms_text_num'] == "2"){
		$advert_sql .= " ms_text_2 AS ms_text, ";
	}
	// テキストタイプ1
	if($value['ms_text_num'] == "3"){
		$advert_sql .= " ms_text_3 AS ms_text, ";
	}
	// テキストタイプ1
	if($value['ms_text_num'] == "4"){
		$advert_sql .= " ms_text_4 AS ms_text, ";
	}
	// テキストタイプ1
	if($value['ms_text_num'] == "5"){
		$advert_sql .= " ms_text_5 AS ms_text, ";
	}

	$advert_sql .= " advert_name "
	. " FROM "
	. " advert "
	. " WHERE "
	. " id = '" . $value['advert_id'] . "' "
	. " AND "
	. " deleted_at is NULL ";

//	echo $advert_sql . "<hr />";

	$rec3 = $common_dao->db_query_ts($advert_sql);

	$advert_attention[] = $rec3[0];

}

$smarty->assign("attention_str", $advert_attention);
$smarty->assign("images_dir_path", "./advert_image/");


//$advert_sql = " SELECT "
//			. " aa.*, "
//			. " a.advert_name "
//			. " FROM ( "
//			. " advert_attention AS aa "
//			. " LEFT JOIN advert AS a ON aa.advert_id = a.id ) "
//			. " WHERE "
//			. " a.status = '2' "
//			. " AND "
//			. " a.test_flag = '0' "
//			. " AND "
//			. " a.advert_category_id <> '11' "
//			. " AND "
//			. " a.advert_name not like '%アダルト%' "
//			. " AND "
//			. " a.advert_name not like '%※%' "
//			. " AND "
//			. " a.advert_name not like '%同人%' "
//			. " AND "
//			. " a.advert_name not like '%テスト%' "
//			. " AND "
//			. " a.deleted_at is NULL "
//			. " AND "
//			. " aa.deleted_at is NULL "
//			. " ORDER BY aa.id ASC ";
//
//$rec2 = $common_dao->db_query_ts($advert_sql);
//
//$smarty->assign("attention_str", $rec2);
//$smarty->assign("images_dir_path", "./advert_image/");

	// ページを表示
	$smarty->display("./index.tpl");
	exit();
?>