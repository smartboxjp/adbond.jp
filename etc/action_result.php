<?php

if(isset($_GET['bid'])) {
	$bid = $_GET['bid'];
} elseif(isset($_POST['bid'])) {
	$bid = $_POST['bid'];
}


$check = 1;


// 登録 ----------------------------------------------------------------
if($_POST['action'] == "result") {

$server = "adbond.jp";  // 送信したいサーバのアドレス
$port = 80;             // HTTP なので80
$timeout = 30;             // 接続に失敗した場合の待ち時間

$sock = fsockopen($server, $port, $errno, $errstr, $timeout);  // サーバに接続する
if($sock === FALSE){    // 接続に失敗したらメッセージを表示し、終了させる
	echo "SOCK OPEN ERROR<br>";
	exit(-1);
}

$ac = date('YmdHis');

// HTTP ヘッダ部分の送信になる。
fwrite($sock, "GET http://" . $server . "/action/result.php?bid=$bid&ac=$ac HTTP/1.0\r\n");
// ヘッダの終了を通知
fwrite($sock, "\r\n\r\n");

fclose($sock);

$check = 2;


// 退会 ----------------------------------------------------------------
} elseif($_POST['action'] == "withdrawal") {

sleep(5);

$server = "adbond.jp";  // 送信したいサーバのアドレス
$port = 80;             // HTTP なので80
$timeout = 30;             // 接続に失敗した場合の待ち時間

$sock = fsockopen($server, $port, $errno, $errstr, $timeout);  // サーバに接続する
if($sock === FALSE){    // 接続に失敗したらメッセージを表示し、終了させる
	echo "SOCK OPEN ERROR<br>";
	exit(-1);
}

$ac = date('YmdHis');

// HTTP ヘッダ部分の送信になる。
fwrite($sock, "GET http://" . $server . "/action/withdrawal.php?bid=$bid HTTP/1.0\r\n");
// ヘッダの終了を通知
fwrite($sock, "\r\n\r\n");

fclose($sock);

$check = 3;

}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>テストサイト</title>
</head>
<body>

<div id="container">
	<p>テスト広告サイト</p>
<?php if($check == 1) { ?>
	<form action="<?= $_SERVER['PHP_SELF'] ?>" method="post">
		<input type="hidden" name="action" value="result" />
		<input type="hidden" name="bid" value="<?= $bid ?>" />
		<input type="submit" value="登録完了" />
	</form>

<?php } elseif($check == 2) { ?>
	<p>登録完了しました。</p>

	<form action="<?= $_SERVER['PHP_SELF'] ?>" method="post">
		<input type="hidden" name="action" value="withdrawal" />
		<input type="hidden" name="bid" value="<?= $bid ?>" />
		<input type="submit" value="退会をする" />
	</form>

<!--	<a href="./action_withdrawal.php?bid=<?php echo $bid ?>">退会テストはこちら</a>-->

<?php } elseif($check == 3) { ?>
	<p>退会完了しました。</p>

<?php } ?>
</div>

</body>
</html>