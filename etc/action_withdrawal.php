<?php

if(isset($_GET['bid'])) {
	$bid = $_GET['bid'];
} elseif(isset($_POST['bid'])) {
	$bid = $_POST['bid'];
}

$check = 1;

if($_POST['action'] == "result") {

Sleep(3000);

$server = "adbond.jp";  // 送信したいサーバのアドレス
$port = 80;             // HTTP なので80
$timeout = 30;             // 接続に失敗した場合の待ち時間

$sock = fsockopen($server, $port, $errno, $errstr, $timeout);  // サーバに接続する
if($sock === FALSE){    // 接続に失敗したらメッセージを表示し、終了させる
	echo "SOCK OPEN ERROR<br>";
	exit(-1);
}

$ac = date('YmdHis');

// HTTP ヘッダ部分の送信になる。
fwrite($sock, "GET http://" . $server . "/action/withdrawal.php?bid=$bid HTTP/1.0\r\n");
// ヘッダの終了を通知
fwrite($sock, "\r\n\r\n");

fclose($sock);

$check = 2;
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>テストサイト</title>

</head>
<body>

<div id="container">
	<p>テスト退会ページ</p>
<?php if($check == 1) { ?>
	<div id="inbody">
		セッションIDを入力し、3秒ほど待ってから送信してください。
		<br />

		<form action="<?= $_SERVER['PHP_SELF'] ?>" method="post">
			<input type="hidden" name="action" value="result" />
			<input type="text" name="bid" value="<?php echo $bid ?>" size="16" />
			<input type="submit" value="退会完了" />
		</form>
	</div>

<?php } elseif($check == 2) { ?>
	<p>退会完了しました。</p>

<?php } ?>
</div>

</body>
</html>