<?php
ini_set('display_errors', 1);

//define('SMARTY_DIR', 'C:/pleiades-e3.5-php-jre_20090930/xampp/php/smarty/');
define('SMARTY_DIR', '../Smarty/libs/');
require_once(SMARTY_DIR . 'Smarty.class.php');

function & getSmartyObj() {
	static $smarty = null;

	if( is_null( $smarty ) ){
		$smarty = new Smarty();
		$smarty->template_dir = '../templates/web/';
		$smarty->compile_id   = 'web';
		$smarty->compile_dir  = '../templates_c/';
		$smarty->config_dir   = '../config/';
		$smarty->cache_dir    = '../cache/';
	}

	return $smarty;
}

?>