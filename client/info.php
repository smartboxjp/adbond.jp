<?php
// 共通設定
require_once( '../common/CommonWebBase.php' );
require_once( '../common/CommonDao.php' );
require_once( '../dao/AdvertLoginUserDao.php' );
require_once( '../dto/AdvertLoginUser.php' );
require_once( '../dao/AdvertDao.php' );
require_once( '../dto/Advert.php' );
require_once( '../dao/AdvertClientDao.php' );
require_once( '../dto/AdvertClient.php' );
require_once( '../dao/AdvertCategoryDao.php' );
require_once( '../dto/AdvertCategory.php' );

session_start();

if(isset($_SESSION['advert_logon_token']) && $_SESSION['advert_logon_token'] != ''){
	$advert_login_user_dao = new AdvertLoginUserDao();
	$advert_login_user = new AdvertLoginUser();
	$advert_login_user = $_SESSION['advert_login_user'];

	$login_user_id = $advert_login_user->getid();
	$user_name = $advert_login_user->getUserName();
	$login_id = $advert_login_user->getLoginId();
	$login_pass = $advert_login_user->getLoginPass();

	//登録者情報、口座情報取得
	$advert_client_dao = new AdvertClientDao();
	$advert_client = new AdvertClient();
	$advert_client = $advert_client_dao->getAdvertClientByLoginUserId($login_user_id);
	$advert_client_id = $advert_client->getId();

	// Smartyオブジェクト取得
	$smarty =& getSmartyObj();

	$smarty->assign("user_name", $user_name);
	$smarty->assign("advert_withdrawal_view_user", $_SESSION['advert_withdrawal_view_user'] );

	$data = array('id' => $advert_client->getId(),
						'login_id' => $advert_login_user->getLoginId(),
						'login_pass' => $advert_login_user->getLoginPass(),
						'login_user_id' => $advert_client->getLoginUserId(),
						'client_name' => $advert_client->getClientName(),
						'contact_person' => $advert_client->getContactPerson(),
						'tel' => $advert_client->getTel(),
						'fax' => $advert_client->getFax(),
						'email' => $advert_client->getEmail(),
						'zipcode1' => $advert_client->getZipcode1(),
						'zipcode2' => $advert_client->getZipcode2(),
						'pref' => $advert_client->getPref(),
						'address1' => $advert_client->getAddress1(),
						'address2' => $advert_client->getAddress2());

	$smarty->assign("data", $data);

	if(isset($_POST['submit']) && $_POST['submit'] == '選択'){
		$advert_id = do_escape_quotes($_POST['advert_id']);
		$smarty->assign("advert_id", $advert_id);

		$advert_dao = new AdvertDao();
		$advert = $advert_dao->getAdvertById($advert_id);

		$advert_category_dao = new AdvertCategoryDao();
		$advert_category = $advert_category_dao->getAdvertCategoryById($advert->getAdvertCategoryId());

		$data = array('id' => $advert->getId(),
						'category_name' => $advert_category->getName(),
						'advert_name' => $advert->getAdvertName(),
						'content_type' => $advert->getContentType(),
						'support_docomo' => $advert->getSupportDocomo(),
						'support_softbank' => $advert->getSupportSoftbank(),
						'support_au' => $advert->getSupportAu(),
						'support_pc' => $advert->getSupportPc(),
						'point_back_flag' => $advert->getPointBackFlag());

		$smarty->assign("advert_data", $data);
	}

	//広告一覧取得
	$common_dao = new CommonDao();
	$list_sql = " SELECT * FROM advert "
				. " WHERE deleted_at is NULL "
				. " AND advert_client_id = " . $advert_client->getId()
				. " ORDER BY id ASC ";

	$db_result = $common_dao->db_query($list_sql);
	if($db_result){
		$smarty->assign("advert_array", $db_result);
	}else{
		//$error_message .= "ＤＢからのデータの取得に失敗しました。(su0000)";
	}
	$smarty->assign("error_message", $error_message);

	// ページを表示
	$smarty->display("./client_info.tpl");
	exit();
}else{
	header('Location: ../index.php?error=1');
	exit();
}

function do_escape_quotes($str){
	//magic_quotesが有効ならクウォート部分を除去
	if(get_magic_quotes_gpc()){
		$str = stripslashes($str);
	}
	return $str;
}
?>