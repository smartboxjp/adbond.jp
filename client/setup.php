<?php
// 共通設定
require_once( '../common/CommonWebBase.php' );
require_once( '../common/CommonDao.php' );
require_once( '../dao/AdvertLoginUserDao.php' );
require_once( '../dto/AdvertLoginUser.php' );
require_once( '../dao/AdvertDao.php' );
require_once( '../dto/Advert.php' );
require_once( '../dao/AdvertClientDao.php' );
require_once( '../dto/AdvertClient.php' );
require_once( '../dao/AdvertCategoryDao.php' );
require_once( '../dto/AdvertCategory.php' );

session_start();

if(isset($_SESSION['advert_logon_token']) && $_SESSION['advert_logon_token'] != ''){
	$advert_login_user_dao = new AdvertLoginUserDao();
	$advert_login_user = new AdvertLoginUser();
	$advert_login_user = $_SESSION['advert_login_user'];

	$login_user_id = $advert_login_user->getid();
	$user_name = $advert_login_user->getUserName();
	$login_id = $advert_login_user->getLoginId();
	$login_pass = $advert_login_user->getLoginPass();

	//登録者情報、口座情報取得
	$advert_client_dao = new AdvertClientDao();
	$advert_client = new AdvertClient();
	$advert_client = $advert_client_dao->getAdvertClientByLoginUserId($login_user_id);
	$advert_client_id = $advert_client->getId();

	// Smartyオブジェクト取得
	$smarty =& getSmartyObj();

	$smarty->assign("user_name", $user_name);
	$smarty->assign("advert_withdrawal_view_user", $_SESSION['advert_withdrawal_view_user'] );

	$advert_dao = new AdvertDao();
	$image_directory = "../advert_image/";

	$id = do_escape_quotes($_POST['id']);
	$ms_text_1 = do_escape_quotes($_POST['ms_text_1']);
	$ms_email_1 = do_escape_quotes($_POST['ms_email_1']);
	$ms_image_type_1 = do_escape_quotes($_POST['ms_image_type_1']);
	$ms_image_url_1 = do_escape_quotes($_POST['ms_image_url_1']);
	$ms_text_2 = do_escape_quotes($_POST['ms_text_2']);
	$ms_email_2 = do_escape_quotes($_POST['ms_email_2']);
	$ms_image_type_2 = do_escape_quotes($_POST['ms_image_type_2']);
	$ms_image_url_2 = do_escape_quotes($_POST['ms_image_url_2']);
	$ms_text_3 = do_escape_quotes($_POST['ms_text_3']);
	$ms_email_3 = do_escape_quotes($_POST['ms_email_3']);
	$ms_image_type_3 = do_escape_quotes($_POST['ms_image_type_3']);
	$ms_image_url_3 = do_escape_quotes($_POST['ms_image_url_3']);
	$ms_text_4 = do_escape_quotes($_POST['ms_text_4']);
	$ms_email_4 = do_escape_quotes($_POST['ms_email_4']);
	$ms_image_type_4 = do_escape_quotes($_POST['ms_image_type_4']);
	$ms_image_url_4 = do_escape_quotes($_POST['ms_image_url_4']);
	$ms_text_5 = do_escape_quotes($_POST['ms_text_5']);
	$ms_email_5 = do_escape_quotes($_POST['ms_email_5']);
	$ms_image_type_5 = do_escape_quotes($_POST['ms_image_type_5']);
	$ms_image_url_5 = do_escape_quotes($_POST['ms_image_url_5']);

	if(isset($_POST['submit']) && $_POST['submit'] == '選択'){
		$advert_id = do_escape_quotes($_POST['advert_id']);
		// ***********************************************************
		// 2011/06/06 追加
		if($advert_id == ""){
			$smarty->assign("info_message", "DBからデータを取得できませんでした。");
			// ページを表示
			$smarty->display("./client_setup.tpl");
			exit();
		}
		// ***********************************************************
		$smarty->assign("advert_id", $advert_id);

		$advert = $advert_dao->getAdvertById($advert_id);

		$advert_category_dao = new AdvertCategoryDao();
		$advert_category = $advert_category_dao->getAdvertCategoryById($advert->getAdvertCategoryId());

		$data = array('id' => $advert->getId(),
						'support_docomo' => $advert->getSupportDocomo(),
						'support_softbank' => $advert->getSupportSoftbank(),
						'support_au' => $advert->getSupportAu(),
						'support_pc' => $advert->getSupportPc(),
						'site_url_docomo' => $advert->getSiteUrlDocomo(),
						'site_url_softbank' => $advert->getSiteUrlSoftbank(),
						'site_url_au' => $advert->getSiteUrlAu(),
						'site_url_pc' => $advert->getSiteUrlPc(),
						'site_outline' => $advert->getSiteOutline(),
						'click_price_client' => $advert->getClickPriceClient(),
						'click_price_media' => $advert->getClickPriceMedia(),
						'action_price_client_docomo_1' => $advert->getActionPriceClientDocomo1(),
						'action_price_client_softbank_1' => $advert->getActionPriceClientSoftbank1(),
						'action_price_client_au_1' => $advert->getActionPriceClientAu1(),
						'action_price_client_pc_1' => $advert->getActionPriceClientPc1(),
						'action_price_client_docomo_2' => $advert->getActionPriceClientDocomo2(),
						'action_price_client_softbank_2' => $advert->getActionPriceClientSoftbank2(),
						'action_price_client_au_2' => $advert->getActionPriceClientAu2(),
						'action_price_client_pc_2' => $advert->getActionPriceClientPc2(),
						'action_price_client_docomo_3' => $advert->getActionPriceClientDocomo3(),
						'action_price_client_softbank_3' => $advert->getActionPriceClientSoftbank3(),
						'action_price_client_au_3' => $advert->getActionPriceClientAu3(),
						'action_price_client_pc_3' => $advert->getActionPriceClientPc3(),
						'action_price_client_docomo_4' => $advert->getActionPriceClientDocomo4(),
						'action_price_client_softbank_4' => $advert->getActionPriceClientSoftbank4(),
						'action_price_client_au_4' => $advert->getActionPriceClientAu4(),
						'action_price_client_pc_4' => $advert->getActionPriceClientPc4(),
						'action_price_client_docomo_5' => $advert->getActionPriceClientDocomo5(),
						'action_price_client_softbank_5' => $advert->getActionPriceClientSoftbank5(),
						'action_price_client_au_5' => $advert->getActionPriceClientAu5(),
						'action_price_client_pc_5' => $advert->getActionPriceClientPc5(),
						'action_price_media_docomo_1' => $advert->getActionPriceMediaDocomo1(),
						'action_price_media_softbank_1' => $advert->getActionPriceMediaSoftbank1(),
						'action_price_media_au_1' => $advert->getActionPriceMediaAu1(),
						'action_price_media_pc_1' => $advert->getActionPriceMediaPc1(),
						'action_price_media_docomo_2' => $advert->getActionPriceMediaDocomo2(),
						'action_price_media_softbank_2' => $advert->getActionPriceMediaSoftbank2(),
						'action_price_media_au_2' => $advert->getActionPriceMediaAu2(),
						'action_price_media_pc_2' => $advert->getActionPriceMediaPc2(),
						'action_price_media_docomo_3' => $advert->getActionPriceMediaDocomo3(),
						'action_price_media_softbank_3' => $advert->getActionPriceMediaSoftbank3(),
						'action_price_media_au_3' => $advert->getActionPriceMediaAu3(),
						'action_price_media_pc_3' => $advert->getActionPriceMediaPc3(),
						'action_price_media_docomo_4' => $advert->getActionPriceMediaDocomo4(),
						'action_price_media_softbank_4' => $advert->getActionPriceMediaSoftbank4(),
						'action_price_media_au_4' => $advert->getActionPriceMediaAu4(),
						'action_price_media_pc_4' => $advert->getActionPriceMediaPc4(),
						'action_price_media_docomo_5' => $advert->getActionPriceMediaDocomo5(),
						'action_price_media_softbank_5' => $advert->getActionPriceMediaSoftbank5(),
						'action_price_media_au_5' => $advert->getActionPriceMediaAu5(),
						'action_price_media_pc_5' => $advert->getActionPriceMediaPc5(),
						'ms_text_1' => $advert->getMsText1(),
						'ms_email_1' => $advert->getMsEmail1(),
						'ms_image_type_1' => $advert->getMsImageType1(),
						'ms_image_url_1' => $advert->getMsImageUrl1(),
						'ms_text_2' => $advert->getMsText2(),
						'ms_email_2' => $advert->getMsEmail2(),
						'ms_image_type_2' => $advert->getMsImageType2(),
						'ms_image_url_2' => $advert->getMsImageUrl2(),
						'ms_text_3' => $advert->getMsText3(),
						'ms_email_3' => $advert->getMsEmail3(),
						'ms_image_type_3' => $advert->getMsImageType3(),
						'ms_image_url_3' => $advert->getMsImageUrl3(),
						'ms_text_4' => $advert->getMsText4(),
						'ms_email_4' => $advert->getMsEmail4(),
						'ms_image_type_4' => $advert->getMsImageType4(),
						'ms_image_url_4' => $advert->getMsImageUrl4(),
						'ms_text_5' => $advert->getMsText5(),
						'ms_email_5' => $advert->getMsEmail5(),
						'ms_image_type_5' => $advert->getMsImageType5(),
						'ms_image_url_5' => $advert->getMsImageUrl5());

		$smarty->assign("form_data", $data);

		if($advert->getMsImageType1() == 1) {
			$ms_image_path_1 = $image_directory . $advert->getMsImageUrl1();
		} else {
			$ms_image_path_1 = $advert->getMsImageUrl1();
		}
		$smarty->assign("ms_image_path_1", $ms_image_path_1);

		if($advert->getMsImageType2() == 1) {
			$ms_image_path_2 = $image_directory . $advert->getMsImageUrl2();
		} else {
			$ms_image_path_2 = $advert->getMsImageUrl2();
		}
		$smarty->assign("ms_image_path_2", $ms_image_path_2);

		if($advert->getMsImageType3() == 1) {
			$ms_image_path_3 = $image_directory . $advert->getMsImageUrl3();
		} else {
			$ms_image_path_3 = $advert->getMsImageUrl3();
		}
		$smarty->assign("ms_image_path_3", $ms_image_path_3);

		if($advert->getMsImageType4() == 1) {
			$ms_image_path_4 = $image_directory . $advert->getMsImageUrl4();
		} else {
			$ms_image_path_4 = $advert->getMsImageUrl4();
		}
		$smarty->assign("ms_image_path_4", $ms_image_path_4);

		if($advert->getMsImageType5() == 1) {
			$ms_image_path_5 = $image_directory . $advert->getMsImageUrl5();
		} else {
			$ms_image_path_5 = $advert->getMsImageUrl5();
		}
		$smarty->assign("ms_image_path_5", $ms_image_path_5);

	} elseif(isset($_POST['submit']) && $_POST['submit'] == '登録') {
		$advert_id = do_escape_quotes($_POST['advert_id']);
		$smarty->assign("advert_id", $advert_id);

		if($ms_image_type_1 == 1) {
			if($_FILES['ms_image_file_1']['name'] != "") {
				$image_name = uniqid("image_");
				$result = insert_image_file($_FILES['ms_image_file_1'], $image_directory, $image_name, $result_message);

				if($result) {
					$ms_image_url_1 = $result_message;
				} else {
					$error_mesage = $result_message;
					$error_flag = 1;
				}
			}
		}

		if($ms_image_type_2 == 1) {
			if($_FILES['ms_image_file_2']['name'] != "") {
				$image_name = uniqid("image_");
				$result = insert_image_file($_FILES['ms_image_file_2'], $image_directory, $image_name, $result_message);

				if($result) {
					$ms_image_url_2 = $result_message;
				} else {
					$error_mesage = $result_message;
					$error_flag = 1;
				}
			}
		}

		if($ms_image_type_3 == 1) {
			if($_FILES['ms_image_file_3']['name'] != "") {
				$image_name = uniqid("image_");
				$result = insert_image_file($_FILES['ms_image_file_3'], $image_directory, $image_name, $result_message);

				if($result) {
					$ms_image_url_3 = $result_message;
				} else {
					$error_mesage = $result_message;
					$error_flag = 1;
				}
			}
		}

		if($ms_image_type_4 == 1) {
			if($_FILES['ms_image_file_4']['name'] != "") {
				$image_name = uniqid("image_");
				$result = insert_image_file($_FILES['ms_image_file_4'], $image_directory, $image_name, $result_message);

				if($result) {
					$ms_image_url_4 = $result_message;
				} else {
					$error_mesage = $result_message;
					$error_flag = 1;
				}
			}
		}

		if($ms_image_type_5 == 1) {
			if($_FILES['ms_image_file_5']['name'] != "") {
				$image_name = uniqid("image_");
				$result = insert_image_file($_FILES['ms_image_file_5'], $image_directory, $image_name, $result_message);

				if($result) {
					$ms_image_url_5 = $result_message;
				} else {
					$error_mesage = $result_message;
					$error_flag = 1;
				}
			}
		}

		if($error_flag == 0) {
			$advert_dao->transaction_start();

			$advert = new Advert();
			$advert = $advert_dao->getAdvertById($id);

			if(!is_null($advert)) {
				$advert->setMsText1($ms_text_1);
				$advert->setMsEmail1($ms_email_1);
				$advert->setMsImageType1($ms_image_type_1);
				$advert->setMsImageUrl1($ms_image_url_1);
				$advert->setMsText2($ms_text_2);
				$advert->setMsEmail2($ms_email_2);
				$advert->setMsImageType2($ms_image_type_2);
				$advert->setMsImageUrl2($ms_image_url_2);
				$advert->setMsText3($ms_text_3);
				$advert->setMsEmail3($ms_email_3);
				$advert->setMsImageType3($ms_image_type_3);
				$advert->setMsImageUrl3($ms_image_url_3);
				$advert->setMsText4($ms_text_4);
				$advert->setMsEmail4($ms_email_4);
				$advert->setMsImageType4($ms_image_type_4);
				$advert->setMsImageUrl4($ms_image_url_4);
				$advert->setMsText5($ms_text_5);
				$advert->setMsEmail5($ms_email_5);
				$advert->setMsImageType5($ms_image_type_5);
				$advert->setMsImageUrl5($ms_image_url_5);

				//UPDATEを実行
				$db_result = $advert_dao->updateAdvert($advert, $result_message);
				if($db_result) {
					$advert_dao->transaction_end();

					$smarty->assign("info_message", $result_message);

					$form_data = array('id' => $id,
										'support_docomo' => $support_docomo,
										'support_softbank' => $support_softbank,
										'support_au' => $support_au,
										'support_pc' => $support_pc,
										'site_url_docomo' => $site_url_docomo,
										'site_url_softbank' => $site_url_softbank,
										'site_url_au' => $site_url_au,
										'site_url_pc' => $site_url_pc,
										'site_outline' => $site_outline,
										'click_price_client' => $click_price_client,
										'action_price_client_docomo_1' => $action_price_client_docomo_1,
										'action_price_client_softbank_1' => $action_price_client_softbank_1,
										'action_price_client_au_1' => $action_price_client_au_1,
										'action_price_client_pc_1' => $action_price_client_pc_1,
										'action_price_client_docomo_2' => $action_price_client_docomo_2,
										'action_price_client_softbank_2' => $action_price_client_softbank_2,
										'action_price_client_au_2' => $action_price_client_au_2,
										'action_price_client_pc_2' => $action_price_client_pc_2,
										'action_price_client_docomo_3' => $action_price_client_docomo_3,
										'action_price_client_softbank_3' => $action_price_client_softbank_3,
										'action_price_client_au_3' => $action_price_client_au_3,
										'action_price_client_pc_3' => $action_price_client_pc_3,
										'action_price_client_docomo_4' => $action_price_client_docomo_4,
										'action_price_client_softbank_4' => $action_price_client_softbank_4,
										'action_price_client_au_4' => $action_price_client_au_4,
										'action_price_client_pc_4' => $action_price_client_pc_4,
										'action_price_client_docomo_5' => $action_price_client_docomo_5,
										'action_price_client_softbank_5' => $action_price_client_softbank_5,
										'action_price_client_au_5' => $action_price_client_au_5,
										'action_price_client_pc_5' => $action_price_client_pc_5,
										'ms_text_1' => $ms_text_1,
										'ms_email_1' => $ms_email_1,
										'ms_image_type_1' => $ms_image_type_1,
										'ms_image_url_1' => $ms_image_url_1,
										'ms_text_2' => $ms_text_2,
										'ms_email_2' => $ms_email_2,
										'ms_image_type_2' => $ms_image_type_2,
										'ms_image_url_2' => $ms_image_url_2,
										'ms_text_3' => $ms_text_3,
										'ms_email_3' => $ms_email_3,
										'ms_image_type_3' => $ms_image_type_3,
										'ms_image_url_3' => $ms_image_url_3,
										'ms_text_4' => $ms_text_4,
										'ms_email_4' => $ms_email_4,
										'ms_image_type_4' => $ms_image_type_4,
										'ms_image_url_4' => $ms_image_url_4,
										'ms_text_5' => $ms_text_5,
										'ms_email_5' => $ms_email_5,
										'ms_image_type_5' => $ms_image_type_5,
										'ms_image_url_5' => $ms_image_url_5);

					$smarty->assign("form_data", $form_data);

					if($advert->getMsImageType1() == 1) {
						$ms_image_path_1 = $image_directory . $advert->getMsImageUrl1();
					} else {
						$ms_image_path_1 = $advert->getMsImageUrl1();
					}
					$smarty->assign("ms_image_path_1", $ms_image_path_1);

					if($advert->getMsImageType2() == 1) {
						$ms_image_path_2 = $image_directory . $advert->getMsImageUrl2();
					} else {
						$ms_image_path_2 = $advert->getMsImageUrl2();
					}
					$smarty->assign("ms_image_path_2", $ms_image_path_2);

					if($advert->getMsImageType3() == 1) {
						$ms_image_path_3 = $image_directory . $advert->getMsImageUrl3();
					} else {
						$ms_image_path_3 = $advert->getMsImageUrl3();
					}
					$smarty->assign("ms_image_path_3", $ms_image_path_3);

					if($advert->getMsImageType4() == 1) {
						$ms_image_path_4 = $image_directory . $advert->getMsImageUrl4();
					} else {
						$ms_image_path_4 = $advert->getMsImageUrl4();
					}
					$smarty->assign("ms_image_path_4", $ms_image_path_4);

					if($advert->getMsImageType5() == 1) {
						$ms_image_path_5 = $image_directory . $advert->getMsImageUrl5();
					} else {
						$ms_image_path_5 = $advert->getMsImageUrl5();
					}
					$smarty->assign("ms_image_path_5", $ms_image_path_5);

				} else {
					$advert_dao->transaction_rollback();

					$smarty->assign("error_message", $result_message);

					$form_data = array('id' => $id,
										'support_docomo' => $support_docomo,
										'support_softbank' => $support_softbank,
										'support_au' => $support_au,
										'support_pc' => $support_pc,
										'site_url_docomo' => $site_url_docomo,
										'site_url_softbank' => $site_url_softbank,
										'site_url_au' => $site_url_au,
										'site_url_pc' => $site_url_pc,
										'site_outline' => $site_outline,
										'click_price_client' => $click_price_client,
										'action_price_client_docomo_1' => $action_price_client_docomo_1,
										'action_price_client_softbank_1' => $action_price_client_softbank_1,
										'action_price_client_au_1' => $action_price_client_au_1,
										'action_price_client_pc_1' => $action_price_client_pc_1,
										'action_price_client_docomo_2' => $action_price_client_docomo_2,
										'action_price_client_softbank_2' => $action_price_client_softbank_2,
										'action_price_client_au_2' => $action_price_client_au_2,
										'action_price_client_pc_2' => $action_price_client_pc_2,
										'action_price_client_docomo_3' => $action_price_client_docomo_3,
										'action_price_client_softbank_3' => $action_price_client_softbank_3,
										'action_price_client_au_3' => $action_price_client_au_3,
										'action_price_client_pc_3' => $action_price_client_pc_3,
										'action_price_client_docomo_4' => $action_price_client_docomo_4,
										'action_price_client_softbank_4' => $action_price_client_softbank_4,
										'action_price_client_au_4' => $action_price_client_au_4,
										'action_price_client_pc_4' => $action_price_client_pc_4,
										'action_price_client_docomo_5' => $action_price_client_docomo_5,
										'action_price_client_softbank_5' => $action_price_client_softbank_5,
										'action_price_client_au_5' => $action_price_client_au_5,
										'action_price_client_pc_5' => $action_price_client_pc_5,
										'ms_text_1' => $ms_text_1,
										'ms_email_1' => $ms_email_1,
										'ms_image_type_1' => $ms_image_type_1,
										'ms_image_url_1' => $ms_image_url_1,
										'ms_text_2' => $ms_text_2,
										'ms_email_2' => $ms_email_2,
										'ms_image_type_2' => $ms_image_type_2,
										'ms_image_url_2' => $ms_image_url_2,
										'ms_text_3' => $ms_text_3,
										'ms_email_3' => $ms_email_3,
										'ms_image_type_3' => $ms_image_type_3,
										'ms_image_url_3' => $ms_image_url_3,
										'ms_text_4' => $ms_text_4,
										'ms_email_4' => $ms_email_4,
										'ms_image_type_4' => $ms_image_type_4,
										'ms_image_url_4' => $ms_image_url_4,
										'ms_text_5' => $ms_text_5,
										'ms_email_5' => $ms_email_5,
										'ms_image_type_5' => $ms_image_type_5,
										'ms_image_url_5' => $ms_image_url_5);

					$smarty->assign("form_data", $form_data);

					if($advert->getMsImageType1() == 1) {
						$ms_image_path_1 = $image_directory . $advert->getMsImageUrl1();
					} else {
						$ms_image_path_1 = $advert->getMsImageUrl1();
					}
					$smarty->assign("ms_image_path_1", $ms_image_path_1);

					if($advert->getMsImageType2() == 1) {
						$ms_image_path_2 = $image_directory . $advert->getMsImageUrl2();
					} else {
						$ms_image_path_2 = $advert->getMsImageUrl2();
					}
					$smarty->assign("ms_image_path_2", $ms_image_path_2);

					if($advert->getMsImageType3() == 1) {
						$ms_image_path_3 = $image_directory . $advert->getMsImageUrl3();
					} else {
						$ms_image_path_3 = $advert->getMsImageUrl3();
					}
					$smarty->assign("ms_image_path_3", $ms_image_path_3);

					if($advert->getMsImageType4() == 1) {
						$ms_image_path_4 = $image_directory . $advert->getMsImageUrl4();
					} else {
						$ms_image_path_4 = $advert->getMsImageUrl4();
					}
					$smarty->assign("ms_image_path_4", $ms_image_path_4);

					if($advert->getMsImageType5() == 1) {
						$ms_image_path_5 = $image_directory . $advert->getMsImageUrl5();
					} else {
						$ms_image_path_5 = $advert->getMsImageUrl5();
					}
					$smarty->assign("ms_image_path_5", $ms_image_path_5);

				}
			}
		} else {
			$smarty->assign("error_message", $error_message);

			$form_data = array('id' => $id,
								'support_docomo' => $support_docomo,
								'support_softbank' => $support_softbank,
								'support_au' => $support_au,
								'support_pc' => $support_pc,
								'site_url_docomo' => $site_url_docomo,
								'site_url_softbank' => $site_url_softbank,
								'site_url_au' => $site_url_au,
								'site_url_pc' => $site_url_pc,
								'site_outline' => $site_outline,
								'click_price_client' => $click_price_client,
								'action_price_client_docomo_1' => $action_price_client_docomo_1,
								'action_price_client_softbank_1' => $action_price_client_softbank_1,
								'action_price_client_au_1' => $action_price_client_au_1,
								'action_price_client_pc_1' => $action_price_client_pc_1,
								'action_price_client_docomo_2' => $action_price_client_docomo_2,
								'action_price_client_softbank_2' => $action_price_client_softbank_2,
								'action_price_client_au_2' => $action_price_client_au_2,
								'action_price_client_pc_2' => $action_price_client_pc_2,
								'action_price_client_docomo_3' => $action_price_client_docomo_3,
								'action_price_client_softbank_3' => $action_price_client_softbank_3,
								'action_price_client_au_3' => $action_price_client_au_3,
								'action_price_client_pc_3' => $action_price_client_pc_3,
								'action_price_client_docomo_4' => $action_price_client_docomo_4,
								'action_price_client_softbank_4' => $action_price_client_softbank_4,
								'action_price_client_au_4' => $action_price_client_au_4,
								'action_price_client_pc_4' => $action_price_client_pc_4,
								'action_price_client_docomo_5' => $action_price_client_docomo_5,
								'action_price_client_softbank_5' => $action_price_client_softbank_5,
								'action_price_client_au_5' => $action_price_client_au_5,
								'action_price_client_pc_5' => $action_price_client_pc_5,
								'ms_text_1' => $ms_text_1,
								'ms_email_1' => $ms_email_1,
								'ms_image_type_1' => $ms_image_type_1,
								'ms_image_url_1' => $ms_image_url_1,
								'ms_text_2' => $ms_text_2,
								'ms_email_2' => $ms_email_2,
								'ms_image_type_2' => $ms_image_type_2,
								'ms_image_url_2' => $ms_image_url_2,
								'ms_text_3' => $ms_text_3,
								'ms_email_3' => $ms_email_3,
								'ms_image_type_3' => $ms_image_type_3,
								'ms_image_url_3' => $ms_image_url_3,
								'ms_text_4' => $ms_text_4,
								'ms_email_4' => $ms_email_4,
								'ms_image_type_4' => $ms_image_type_4,
								'ms_image_url_4' => $ms_image_url_4,
								'ms_text_5' => $ms_text_5,
								'ms_email_5' => $ms_email_5,
								'ms_image_type_5' => $ms_image_type_5,
								'ms_image_url_5' => $ms_image_url_5);

			$smarty->assign("form_data", $form_data);

			if($advert->getMsImageType1() == 1) {
				$ms_image_path_1 = $image_directory . $advert->getMsImageUrl1();
			} else {
				$ms_image_path_1 = $advert->getMsImageUrl1();
			}
			$smarty->assign("ms_image_path_1", $ms_image_path_1);

			if($advert->getMsImageType2() == 1) {
				$ms_image_path_2 = $image_directory . $advert->getMsImageUrl2();
			} else {
				$ms_image_path_2 = $advert->getMsImageUrl2();
			}
			$smarty->assign("ms_image_path_2", $ms_image_path_2);

			if($advert->getMsImageType3() == 1) {
				$ms_image_path_3 = $image_directory . $advert->getMsImageUrl3();
			} else {
				$ms_image_path_3 = $advert->getMsImageUrl3();
			}
			$smarty->assign("ms_image_path_3", $ms_image_path_3);

			if($advert->getMsImageType4() == 1) {
				$ms_image_path_4 = $image_directory . $advert->getMsImageUrl4();
			} else {
				$ms_image_path_4 = $advert->getMsImageUrl4();
			}
			$smarty->assign("ms_image_path_4", $ms_image_path_4);

			if($advert->getMsImageType5() == 1) {
				$ms_image_path_5 = $image_directory . $advert->getMsImageUrl5();
			} else {
				$ms_image_path_5 = $advert->getMsImageUrl5();
			}
			$smarty->assign("ms_image_path_5", $ms_image_path_5);

		}
	}

	//広告一覧取得
	$common_dao = new CommonDao();
	$list_sql = " SELECT * FROM advert "
				. " WHERE deleted_at is NULL "
				. " AND advert_client_id = " . $advert_client->getId()
				. " ORDER BY id ASC ";

	$db_result = $common_dao->db_query($list_sql);
	if($db_result){
		$smarty->assign("advert_array", $db_result);
	}else{
		//$error_message .= "ＤＢからのデータの取得に失敗しました。(su0000)";
	}
	$smarty->assign("error_message", $error_message);

	// ページを表示
	$smarty->display("./client_setup.tpl");
	exit();
}else{
	header('Location: ../index.php?error=1');
	exit();
}

function do_escape_quotes($str){
	//magic_quotesが有効ならクウォート部分を除去
	if(get_magic_quotes_gpc()){
		$str = stripslashes($str);
	}
	return $str;
}

function insert_image_file($up_file, $up_directory, $up_name, &$result_message = "") {
	$image_size = $up_file['size'];
	$image_tmp = $up_file['tmp_name'];

	if($image_size <= 4000000) {
		$image_info = getimagesize($image_tmp);
		if($image_info[2] == 1) {
			$file_type = "gif";
		} elseif($image_info[2] == 2) {
			$file_type = "jpg";
		} elseif($image_info[2] == 3) {
			$file_type = "png";
		} else {
			$error_message = "対応画像ファイルではありません。";
			return false;
		}

		$up_file_path = $up_directory . $up_name . "." . $file_type;

		if(move_uploaded_file($image_tmp, $up_file_path)) {
			chmod($up_file_path,0666);
			$result_message = $up_name . "." . $file_type;
			return true;
		} else {
			$result_message = "画像ファイルの保存に失敗しました。";
			return false;
		}
	} else {
		$result_message = "画像ファイルのサイズが大きすぎます。(4000KBまで）";
		return false;
	}
}
?>