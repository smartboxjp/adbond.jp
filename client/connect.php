<?php
// 共通設定
require_once( '../common/CommonWebBase.php' );
require_once( '../common/CommonDao.php' );
require_once( '../dao/AdvertLoginUserDao.php' );
require_once( '../dto/AdvertLoginUser.php' );
require_once( '../dao/AdvertClientDao.php' );
require_once( '../dto/AdvertClient.php' );

session_start();

if(isset($_SESSION['advert_logon_token']) && $_SESSION['advert_logon_token'] != ''){
	$advert_login_user_dao = new AdvertLoginUserDao();
	$advert_login_user = new AdvertLoginUser();
	$advert_login_user = $_SESSION['advert_login_user'];

	$login_user_id = $advert_login_user->getid();
	$user_name = $advert_login_user->getUserName();
	$login_id = $advert_login_user->getLoginId();
	$login_pass = $advert_login_user->getLoginPass();

	//登録者情報、口座情報取得
	$advert_client_dao = new AdvertClientDao();
	$advert_client = new AdvertClient();
	$advert_client = $advert_client_dao->getAdvertClientByLoginUserId($login_user_id);
	$advert_client_id = $advert_client->getId();

	// Smartyオブジェクト取得
	$smarty =& getSmartyObj();

	$smarty->assign("user_name", $user_name);
	$smarty->assign("advert_withdrawal_view_user", $_SESSION['advert_withdrawal_view_user'] );

	//広告一覧取得
	$common_dao = new CommonDao();
	$list_sql = " SELECT * FROM advert "
				. " WHERE deleted_at is NULL "
				. " AND advert_client_id = " . $advert_client->getId()
				. " ORDER BY id ASC ";

	$db_result = $common_dao->db_query($list_sql);
	if($db_result){
		$smarty->assign("advert_array", $db_result);
	}else{
		//$error_message .= "ＤＢからのデータの取得に失敗しました。(su0000)";
	}

	if(isset($_POST['submit']) && $_POST['submit'] == "選択") {
		$advert_id = do_escape_quotes($_POST['advert_id']);
		$smarty->assign("advert_id", $advert_id);

		$sql = " SELECT m.*, mc.name as category_name "
				. " FROM connect_logs as cl "
				. " LEFT JOIN media as m on cl.media_id = m.id "
				. " LEFT JOIN media_categories as mc on m.media_category_id = mc.id "
				. " WHERE cl.deleted_at is NULL "
				. " AND cl.advert_id = '$advert_id' "
				. " ORDER BY cl.id ASC ";

		$db_result = $common_dao->db_query($sql);
		if($db_result){
			$smarty->assign("connect_array", $db_result);
		}else{
			//$error_message .= "ＤＢからのデータの取得に失敗しました。(su0000)";
		}
	}

	// ページを表示
	$smarty->display("./client_connect.tpl");
	exit();
}else{
	header('Location: ../index.php?error=1');
	exit();
}

function do_escape_quotes($str){
	//magic_quotesが有効ならクウォート部分を除去
	if(get_magic_quotes_gpc()){
		$str = stripslashes($str);
	}
	return $str;
}
?>