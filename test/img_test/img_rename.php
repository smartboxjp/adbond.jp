<?php

header("Content-type: text/html; charset=UTF-8");

if(isset($_POST['dir']) && $_POST['dir'] != "") {
	$rename_flag = true;
	// ディレクトリ名取得
	$dir_name = $_POST['dir'];
} else {
	$rename_flag = false;
	$dir_name = "img_1"
}

if($rename_flag){

	// 追加する文字列を取得
	$add_str_arr = explode("_", $dir_name);
	$add_str = $add_str_arr[1];

	//ディレクトリ・ハンドルをオープン
	$res_dir = opendir($dir_name);

	//ディレクトリ内のファイル名を１つずつを取得
	while( $file_name = readdir( $res_dir ) ){

		// _を区切り条件に配列化
		$img_name_arr = explode("_", $file_name);


		if($file_name != "." && $file_name != "..") {
			// 配列の要素3番目が1～4でない
			if($img_name_arr[2] != 1 && $img_name_arr[2] != 2 && $img_name_arr[2] != 3 && $img_name_arr[2] != 4) {

				$loop_count = 0;
				$rename_str = "";
				while (count($img_name_arr) > $loop_count) {
					// 配列の要素が2番目の文字列に数字を連結
					if($loop_count == 1) {
						$img_name_arr[$loop_count] = $img_name_arr[$loop_count]."_".$add_str;
					}
					$loop_count += 1;
				}
				// 配列の中身を_を条件に文字列連結
				$rename_str = implode("_",$img_name_arr);
				// 変更したファイル名をリネームする
				if( rename( "./".$dir_name."/".$file_name, "./".$dir_name."/".$rename_str )) {
					// ※デバッグ表示
//	 				echo "リネーム完了";
				}
			}
		}
	}

	//ディレクトリ・ハンドルをクローズ
	closedir( $res_dir );

	echo "選択したディレクトリは".$dir_name."<br />";
	echo "処理が完了しました。";

} else {

}

?>


<html>
	<head>
		<title>リネームツール</title>
	</head>

	<body>

		<p>フォルダを選択して下さい。</p>
		<form action="/test/img_test/img_rename.php")" method="post">
			リネームするファイルが入ったフォルダ名選択:
			<select name="dir">
				<option value="img_1">img_1</option>
				<option value="img_2">img_2</option>
				<option value="img_3">img_3</option>
				<option value="img_4">img_4</option>
			</select>

			<input type="hidden" name="slot_id" value="1" />
			<input type="submit" value="rename" />
		</form>

	</body>
</html>
