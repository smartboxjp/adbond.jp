<?php
// 共通設定
require_once( '../common/CommonAdminBase.php' );
require_once( '../common/CommonFunc.php' );
require_once( '../common/CommonDao.php' );
require_once( '../dao/ActionLogDao.php' );
require_once( '../dto/ActionLog.php' );
require_once( '../dao/MediaDao.php' );
require_once( '../dto/Media.php' );
require_once( '../dao/AdvertDao.php' );
require_once( '../dto/Advert.php' );
require_once( '../dao/ConnectLogDao.php' );
require_once( '../dto/ConnectLog.php' );
require_once( '../dao/AdvertPriceMediaSetDao.php' );
require_once( '../dto/AdvertPriceMediaSet.php' );

if(isset($_GET['m']) && $_GET['m'] != '' && isset($_GET['a']) && $_GET['a'] != ''){

	if(isset($_GET['sid'])) {
		$sid = $_GET['sid'];
	}

	$common_dao = new CommonDao();
	$action_log_dao = new ActionLogDao();

	$session_id = md5(uniqid(rand(), true));
	$carrier_id = "";
	$user_agent = $_SERVER['HTTP_USER_AGENT'];
	$uid = "";
	$ip_address = $_SERVER['REMOTE_ADDR'];
	$host_name = gethostbyaddr($_SERVER["REMOTE_ADDR"]);
	$media_id = $_GET['m'];
	$media_publisher_id = "";
	$advert_id = $_GET['a'];
	$advert_client_id = "";
	$click_price = 0;
	$action_price = 0;
	$point_back_parameter = "";
	$status = 1;
	//-------------------------------------------
	// 7/21 追加
	$auid = "NULL";
	//-------------------------------------------
	$error_flag = 0;

	$connect_log_dao = new ConnectLogDao();
	$connect_log = new ConnectLog();
	$connect_log = $connect_log_dao->getConnectLogByMidAid($media_id, $advert_id);
	if(is_null($connect_log)) {
		echo "error:1";
		exit();
	}

	//受け取った媒体IDからレコードを取得
	$media_dao = new MediaDao();
	$media = new Media();
	$media = $media_dao->getMediaById($media_id);
	if(!is_null($media)) {	//登録されている媒体か確認
		$media_publisher_id = $media->getMediaPublisherId();
		$media_category_id = $media->getMediaCategoryId();
		$response_type = $media->getResponseType();
		$point_back_url = $media->getPointBackUrl();

		if($response_type == 1) {
			$point_back_parameter = $sid;
		}
	} else {
		echo "error:2";
		exit();
	}

	//受け取った広告IDからレコードを取得
	$advert_dao = new AdvertDao();
	$advert = new Advert();
	$advert = $advert_dao->getAdvertByIdStatus($advert_id, 2);
	if(!is_null($advert)) {	//登録されている広告か確認
		$advert_client_id = $advert->getAdvertClientId();

		$advert_price_media_set_dao = new AdvertPriceMediaSetDao();
		$advert_price_madia_set = new AdvertPriceMediaSet();
		$advert_price_madia_set = $advert_price_media_set_dao->getAdvertPriceMediaSetByMidAid($advert_id, $media_id);

		if(!is_null($advert_price_madia_set)) {
			$click_price_client = $advert_price_madia_set->getClickPriceClient();
			$click_price_media = $advert_price_madia_set->getClickPriceMedia();

			$action_price_client_docomo_1 = $advert_price_madia_set->getActionPriceClientDocomo1();
			$action_price_client_softbank_1 = $advert_price_madia_set->getActionPriceClientSoftbank1();
			$action_price_client_au_1 = $advert_price_madia_set->getActionPriceClientAu1();
			$action_price_client_pc_1 = $advert_price_madia_set->getActionPriceClientPc1();

			$action_price_media_docomo_1 = $advert_price_madia_set->getActionPriceMediaDocomo1();
			$action_price_media_softbank_1 = $advert_price_madia_set->getActionPriceMediaSoftbank1();
			$action_price_media_au_1 = $advert_price_madia_set->getActionPriceMediaAu1();
			$action_price_media_pc_1 = $advert_price_madia_set->getActionPriceMediaPc1();
		} else {
			$click_price_client = $advert->getClickPriceClient();
			$click_price_media = $advert->getClickPriceMedia();

			$action_price_client_docomo_1 = $advert->getActionPriceClientDocomo1();
			$action_price_client_softbank_1 = $advert->getActionPriceClientSoftbank1();
			$action_price_client_au_1 = $advert->getActionPriceClientAu1();
			$action_price_client_pc_1 = $advert->getActionPriceClientPc1();

			$action_price_media_docomo_1 = $advert->getActionPriceMediaDocomo1();
			$action_price_media_softbank_1 = $advert->getActionPriceMediaSoftbank1();
			$action_price_media_au_1 = $advert->getActionPriceMediaAu1();
			$action_price_media_pc_1 = $advert->getActionPriceMediaPc1();
		}

		$support_docomo = $advert->getSupportDocomo();
		$support_softbank = $advert->getSupportSoftbank();
		$support_au = $advert->getSupportAu();
		$support_pc = $advert->getSupportPc();
	} else {
		echo "error:3";
		exit();
	}

	// キャリアチェック
	if(ereg("^DoCoMo", $user_agent)) {
		$carrier_id = 1;

		// docomo 個体識別番号チェック
		if(isset($_SERVER['HTTP_X_DCMGUID'])) {
			$uid = $_SERVER['HTTP_X_DCMGUID'];
		} else {
			preg_match("/^.+ser([0-9a-zA-Z]+).*$/", $user_agent, $match);
			$uid = $match[1];
		}

		$action_price_client = $action_price_client_docomo_1;
		$action_price_media = $action_price_media_docomo_1;

		if($support_docomo == 1) {
			$advert_url = $advert->getSiteUrlDocomo();
		} else {
			$error_flag = 1;
		}
	} else if(ereg("^J-PHONE|^Vodafone|^SoftBank", $user_agent)) {
		$carrier_id = 2;

		// softbank 個体識別番号チェック
		if(isset($_SERVER['HTTP_X_JPHONE_UID'])) {
			$uid = $_SERVER['HTTP_X_JPHONE_UID'];
		}

		$action_price_client = $action_price_client_softbank_1;
		$action_price_media = $action_price_media_softbank_1;

		if($support_softbank == 1) {
			$advert_url = $advert->getSiteUrlSoftbank();
		} else {
			$error_flag = 1;
		}
	} else if(ereg("^UP.Browser|^KDDI", $user_agent)) {
		$carrier_id = 3;

		// au 個体識別番号チェック
		if(isset($_SERVER['HTTP_X_UP_SUBNO'])) {
			$uid = $_SERVER['HTTP_X_UP_SUBNO'];
		}

		$action_price_client = $action_price_client_au_1;
		$action_price_media = $action_price_media_au_1;

		if($support_au == 1) {
			$advert_url = $advert->getSiteUrlAu();
		} else {
			$error_flag = 1;
		}
	} else {
		$carrier_id = 4;

		$action_price_client = $action_price_client_pc_1;
		$action_price_media = $action_price_media_pc_1;

		if($support_pc == 1) {
			$advert_url = $advert->getSiteUrlPc();
		} else {
			$error_flag = 1;
		}
	}

	if($error_flag == 0){
		if(stripos($advert_url, "##ID##")) {
			$advert_url = ereg_replace("##ID##", $session_id, $advert_url);
		} elseif(!stripos($advert_url, "?")) {
			$advert_url .= "?bid=$session_id";
		} else {
			$advert_url .= "&bid=$session_id";
		}
//----------------------------------------------------------------------------------8/16 追加
		if(isset($_GET['uid1'])){
			$uid1= $_GET['uid1'];
			$uid2 = $uid;
			$point_back_parameter = $uid1 . "&uid2=" . $uid2;
			//echo $point_back_parameter;
		}
//----------------------------------------------------------------------------------8/16 追加
		$action_log_dao->transaction_start();

		$action_log = new ActionLog();
		$action_log->setSessionId($session_id);
		$action_log->setCarrierId($carrier_id);
		$action_log->setUserAgent($user_agent);
		$action_log->setUid($uid);
		$action_log->setIpAddress($ip_address);
		$action_log->setHostName($host_name);
		$action_log->setMediaId($media_id);
		$action_log->setMediaPublisherId($media_publisher_id);
		$action_log->setAdvertId($advert_id);
		$action_log->setAdvertClientId($advert_client_id);
		$action_log->setClickPriceClient($click_price_client);
		$action_log->setClickPriceMedia($click_price_media);
		$action_log->setActionPriceClient($action_price_client);
		$action_log->setActionPriceMedia($action_price_media);
		$action_log->setLinkUrl($advert_url);
		$action_log->setPointBackParameter($point_back_parameter);
		$action_log->setPointBackUrl($point_back_url);
		$action_log->setStatus($status);
		//-------------------------------------------
		// 7/21 追加
		$action_log->setAuid($auid);
		//-------------------------------------------
		//INSERTを実行
		$db_result = $action_log_dao->insertActionLog($action_log, $result_message);

		if($db_result) {
			$action_log_dao->transaction_end();
			header('Location: test_media.php');

			exit();
		} else {
			$action_log_dao->transaction_rollback();
			echo $resut_message;
			exit();
		}
	} else {
		exit();
	}
}else{

	exit();
}
?>