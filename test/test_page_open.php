<?php

// 変数初期化
$dir_path = "";
$file_path = "";
$file_name = "";
$html_str = "";
$mode = "";

// 初期ディレクトリ指定
$dir_path = "./10";


if($_GET['mode'] == "kakunin"){
	// ファイル内容の変更 ----------------------------------------------------------------------------
	$file_name = $_GET['file_name'];
	$file_path = $dir_path . "/" . $file_name;
	$mode = $_GET['mode'];

	// 指定されたファイルの取得とファイル変更ページ作成
	$html_str = get_file_open($file_name, $file_path);

// ------------------------------------------------------------------------------------ 2011/02/03追加
} elseif($_GET['mode'] == "delete_conf"){
	// ファイルの削除確認 ----------------------------------------------------------------------------
	$file_name = $_GET['file_name'];
	$file_path = $dir_path . "/" . $file_name;
	$mode = $_GET['mode'];

	// ファイルの削除確認画面
	$html_str = delete_file_conf($file_name, $file_path);
} elseif($_GET['mode'] == "delete") {
	// ファイルの削除 --------------------------------------------------------------------------------
	$file_name = $_GET['file_name'];
	$file_path = $dir_path . "/" . $file_name;
	$mode = $_GET['mode'];

	// ファイルの削除
	$html_str = delete_file($file_name, $file_path);
// ---------------------------------------------------------------------------------------------------

} elseif($_GET['mode'] == "update") {
	// ファイル内容の更新 ----------------------------------------------------------------------------
	$html_str = $_GET['html_str'];
	$file_name = $_GET['file_name'];
	$file_path = $dir_path . "/" . $file_name;
	$mode = $_GET['mode'];

	$html_str = get_file_while_result($file_name, $file_path, $html_str);

} else {
	// 指定ディレクトリ内の一覧ページ ----------------------------------------------------------------
	// ディレクトリ内のファイル一覧取得と一覧ページ作成
	$html_str = get_dir_open($dir_path);

}

// ディレクトリ内のファイル一覧取得と一覧ページ作成 --------------------------------------------------
function get_dir_open($dir_path){
	//ディレクトリのハンドルをオープン
	$res_dir = opendir($dir_path);

	//ディレクトリ内のファイル名を１つずつを取得
	while( $file_name = readdir($res_dir)){

// ------------------------------------------------------------------------------------- 2011/02/03追加
		$file_name_rp = str_replace(".","",$file_name);

		if($file_name_rp != ""){
// ----------------------------------------------------------------------------------------------------

			//取得したファイル名を表示html
			$html_str .= "<form action='./test_page_open.php' method='get' >"
						."$file_name"
						."<br />"
						."<input type='submit'  value='確認' />"
						."<input type='hidden' name='file_name' value='$file_name' />"
						."<input type='hidden' name='mode' value='kakunin' />"
						."</form>"

// ------------------------------------------------------------------------------------- 2011/02/03追加
						."<form action='./test_page_open.php' method='get' >"
						."<input type='submit' value='削除' />"
						."<input type='hidden' name='file_name' value='$file_name' />"
						."<input type='hidden' name='mode' value='delete_conf' />"
						."</form>"
// ----------------------------------------------------------------------------------------------------

						."<hr />"
						."<br />";
		}
	}
	//ディレクトリのハンドルをクローズ
	closedir( $res_dir );

	return $html_str;
}

// 指定されたファイルの取得とファイル変更ページ作成 --------------------------------------------------
function get_file_open($file_name, $file_path){

	//ファイルを開く
	if ( ! ($fp = fopen ($file_path, "r+"))) {
			echo "error:ファイルが開けません。<br />";
	}

	//ファイルの読み込み
	$html_str_fp = fread($fp, 4096) ;
	//ファイルを閉じる
	fclose ($fp) ;

	$html_str_rp = str_replace(">", ">\r\n", $html_str_fp);

	$html_str = "<form action='./test_page_open.php' method='get' >"
				."<h3>$file_name</h3>"
				."<textarea name='html_str' cols='80' rows='40' >$html_str_rp</textarea>"
				."<br />"
				."<input type='submit'  value='変更' />"
				."<input type='hidden' name='file_name' value='$file_name' />"
				."<input type='hidden' name='mode' value='update' />"
				."</form>"
				."<a href='./test_page_open.php'>一覧ページへ戻る</a>"
				."<br />";

	return $html_str;
}
// 指定されたファイルの更新とファイル更新完了ページ作成 ------------------------------------------------
function get_file_while_result($file_name, $file_path, $html_str){

	// 改行の置き換え
	$html_str_rp = str_replace("\r\n", "", $html_str);

	// ファイル書き込み
	$fp = fopen($file_path,"w+");
	fwrite($fp, stripcslashes($html_str_rp));
	fclose($fp);
//	$ref2 = chmod($path_name, 0777);

	$html_str = $file_name . "の更新が完了しました"
				."<br />"
				."<a href='./test_page_open.php'>一覧ページへ戻る</a>"
				."<br /><br />"
				."変更内容"
				."<br />"
				. $html_str_rp;



	return $html_str;
}

// ------------------------------------------------------------------------------------- 2011/02/03追加
// 指定されたファイルの削除確認
function  delete_file_conf($file_name, $file_path){
	// ファイルを開く
	if($fp = fopen($file_path, "r+")){
		// ファイルの読み込み
		$html_str_fp = fread($fp, 4096);
		// ファイルを閉じる
		fclose($fp);

		$html_str_rp = str_replace(">", ">\r\n", $html_str_fp);

		// ファイルの内容を表示
		$html_str = "<form action='./test_page_open.php' method='get'>"
					."<h3>$file_name</h3>"
					."<textarea readonly name='html_str' cols='80' rows=40'>$html_str_rp</textarea>"
					."<br />"
					."<input type='submit' value='削除' />"
					."<input type='hidden' name='file_name' value='$file_name' />"
					."<input type='hidden' name='mode' value='delete' />"
					."</form>";

	// ファイルの読み込みに失敗した場合
	}else{
		$html_str = "ファイルの読み込みに失敗しました。<br />";
	}

	$html_str .= "<a href='./test_page_open.php'>一覧ページへ戻る</a><br />";

	return $html_str;
}

// 指定されたファイルの削除
function delete_file($file_name, $file_path){
	// ファイルを削除
	$del_fp = unlink($file_path);

	if($del_fp){
		$message = $file_name."を削除しました。";
	}else{
		$message = "ファイルの削除に失敗しました。";
	}

	$html_str = $message
				."<br />"
				."<a href='./test_page_open.php'>一覧ページへ戻る</a>"
				."<br />";

	return $html_str;
}
// ----------------------------------------------------------------------------------------------------

?>


<?php if($mode == "kakunin" || $mode == "delete_conf"){ ?>
	<html>
		<head>
			<title>確認</title>
		</head>
		<body>

			<?php echo stripcslashes($html_str) ?>

		</body>
	</html>
<?php } elseif($mode == "update" || $mode == "delete") { ?>
	<html>
		<head>
			<title>完了</title>
		</head>
		<body>

			<?php echo stripcslashes($html_str) ?>

		</body>
	</html>
<?php } else { ?>
	<html>
		<head>
			<title>一覧</title>
		</head>
		<body>

			<?php echo stripcslashes($html_str) ?>

		</body>
	</html>
<?php } ?>