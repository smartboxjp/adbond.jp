<?php
//require_once('Net/UserAgent/Mobile.php');


//###############################################################################
// 個体識別、ユーザーエージェントを取得するためのツール

//###############################################################################

	$errer_flg = true;
	$careers_id = 0;
	$user_agent = "";
	$uid = "";

	echo "<a href='example.html' utn>test</a>";

	//********************************************************************************
	// キャリアチェック 個体識別取得
	//********************************************************************************
	$user_agent = $_SERVER['HTTP_USER_AGENT'];

	// キャリアチェック
	if(ereg("^DoCoMo", $user_agent)) {
		$careers_id = 1;
		// docomo 個体識別番号チェック
		if(isset($_SERVER['HTTP_X_DCMGUID'])) {
			$uid = $_SERVER['HTTP_X_DCMGUID'];
//		} else {
			preg_match("/^.+ser([0-9a-zA-Z]+).*$/", $user_agent, $match);
			$uid = $match[1];
		}

	} else if(ereg("^J-PHONE|^Vodafone|^SoftBank", $user_agent)) {
		$careers_id = 3;
		// softbank 個体識別番号チェック
		if(isset($_SERVER['HTTP_X_JPHONE_UID'])) {
			$uid = $_SERVER['HTTP_X_JPHONE_UID'];
		} else {
			$errer_flg = false;
		}

	} else if(ereg("^UP.Browser|^KDDI", $user_agent)) {
		$careers_id = 2;
		// au 個体識別番号チェック
		if(isset($_SERVER['HTTP_X_UP_SUBNO'])) {
			$uid = $_SERVER['HTTP_X_UP_SUBNO'];
		} else {
			$errer_flg = false;
		}

	} else {
		$careers_id = 4;
		$errer_flg = false;
	}


//	$agent = Net_UserAgent_Mobile::singleton();
//	$mobile_id = $agent->getSerialNumber();
//	$mobile_id = $agent->getCardID();



	//********************************************************************************
	// 表示
	//********************************************************************************
	header('Content-type: application/xhtml+xml;charset=utf-8');

	if($errer_flg){
		echo "キャリア:";
		if($careers_id == 1){
			echo "docomo"
				. "<br />";
		} elseif($careers_id == 2) {
			echo "ezweb"
				. "<br />";
		} elseif($careers_id == 3) {
			echo "softbank"
				. "<br />";
		} else {
			echo "error：携帯で接続して下さい。";
			exit();
		}

		echo "UserAgent:" . $user_agent
			. "<br />"
			. "uid:" . $uid
			. "<br />"
			. $mobile_id;
		exit();

	} else {
		echo "error：携帯で接続していないか、UIDが存在しません。";
		exit();
	}


?>