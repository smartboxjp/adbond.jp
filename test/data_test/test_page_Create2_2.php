<?php
	// セッションスタート
	session_start();
	// 変数の初期化・宣言

	define("my_create_tools_path", "./test_page_Create2_2.php");

	$selected = "new";									// new:新規作成 url_ref:URL参照 upload:アップロードファイル
	$mode = "";

	$html_title = "ページ複製ツール";							// 作成ページタイトル名
	$html_meta = "no_meta";								// 作成ページのメタタグのname
	$html_body = "ここにbody部分を書き込んで下さい。";	// 作成ページのボディ部分のhtml
	$html_views_str = "";								// 作成ページの全html
	$html_create_str = "";
	$page_num = "";										// 作成ページ数
	$create_name = "create";							// ディレクトリ名及び、ファイル名
	$dir_name = "";										// 作成ページを格納するディレクトリ名
	$file_name = "";									// 作成ページのファイル名
	$path_name = "";									// 書き込みするパス
	$file_create_count = 0;								// ファイル作成のカウント
	$file_name_count = 1;								// 重複したファイル名のカウント
	$dir_name_count = 1;								// 重複したディレクトリ名のカウント
	$zokusei_num = 0747;								// FTPにおける属性値の初期設定

// 追加 菊池------------------------------------------------------------------
	$inpot_url = "";
	$save_dir = "anyway_dir";
	$html_msg = "";
// ---------------------------------------------------------------------------
if(isset($_POST['selected']) || $_POST['selected'] != ""){
	$selected = $_POST['selected'];
}

if($selected == "new"){
// 新規作成 ------------------------------------------------------------------------------------------------------
	if($_POST['mode'] == "kakunin"){ // 確認画面 ------------------------------------------------------------

		// GETで受け取ったパラメータを変数へ代入
		$mode = $_POST['mode'];
		$html_title = $_POST['html_title'];
		$html_meta = $_POST['html_meta'];
		$html_body = $_POST['html_body'];
		$page_num = $_POST['page_num'];

		// セッション変数へ編集内容を代入
		$_SESSION["html_body"] = $html_body;

		// 編集内容を表示htmlへ代入
		$html_views_str = $html_body;
		// </body>を""へ置き換え
		$html_views_str = str_replace("</body>", "", $html_views_str);
		// </html>を""へ置き換え
		$html_views_str = str_replace("</html>", "", $html_views_str);

		// 確認ページのbody部分のhtml作成
		$html_views_str = get_kakunin_html($html_body, $html_views_str, $page_num, my_create_tools_path);

	} elseif($_POST['mode'] == "create") { // ページ作成 ----------------------------------------------------

		// GETで受け取ったパラメータを変数へ代入
		$mode = $_POST['mode'];
		$html_body = $_SESSION["html_body"];
		$page_num = $_POST['page_num'];

 		// 作成するページのフッダー部分のhtmlを作成する
 		$html_create_str = $html_body;
		// ファイル作成
 		get_create_file($html_body, $page_num, $zokusei_num, $create_name, $dir_name_count, $file_name_count);

 		$html_msg = "<b>" . $page_num . "ページ作成が完了しました。</b>";
		$html_body = "<html>\n"
					."<head>\n"
					."<meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>\n"
					."<title></title>\n"
					."</head>\n"
					."<body>\n"
					."</body>\n"
					."</html>\n";

		// ツールのページのヘッダー部分のhtmlを作成する 注：直書き
		$html_views_str = get_head_html($html_title, "", $html_msg, "", my_create_tools_path);
		// 初期入力フォームのbody部分のhtml作成
		$html_views_str = get_inpot_start_html($html_body, $html_views_str, my_create_tools_path);

	} else { // 初期画面 -----------------------------------------------------------------------------------

		if($_POST['mode'] == "hensyu") {
			$html_msg = "<b>ページ編集して下さい。</b>";
			//セッション変数の内容を編集部分へ代入
			$html_body = $_SESSION["html_body"];

		} else {
			$html_msg = "<b>ページ作成して下さい。</b>";
			$html_body = "<html>\n"
						."<head>\n"
						."<meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>\n"
						."<title></title>\n"
						."</head>\n"
						."<body>\n"
						."</body>\n"
						."</html>\n";

		}

		// ツールのページのヘッダー部分のhtmlを作成する 注：直書き
		$html_views_str = get_head_html($html_title, "", $html_msg, "", my_create_tools_path);
		// 初期入力フォームのbody部分のhtml作成
		$html_views_str = get_inpot_start_html($html_body, $html_views_str, my_create_tools_path);
	}

} elseif($selected == "url_ref") {
// 外部URL取り込み -----------------------------------------------------------------------------------------------
	if($_POST['mode'] == "hensyu"){ // 編集画面 ------------------------------------------------------------

		// GETで受け取ったパラメータを変数へ代入
		$mode = $_POST['mode'];
		$inpot_url = $_POST['inpot_url'];


	if(isset($_SESSION['html_str']) || $_SESSION['html_str'] != ""){
		$html_body = $_SESSION['html_str'];
	} else {
		// 指定した参照URLにファイルが存在するか
		if(@file($inpot_url)){ // エラーを強制的に非表示とする
			// 存在した場合、入力した参照URLのファイル内容を取得
			$html_body = stripcslashes(mb_convert_encoding(file_get_contents($inpot_url), 'UTF-8', 'auto'));

			// 参照URLのファイルの文字数が4000バイトを超えているか
			if (strlen($html_body) > 4000){
			 	$html_msg = "指定したURLのファイルサイズは" . strlen($html_body) . "バイトです。"
			 				."<br />"
			 				."4000バイト以内のURLを参照して下さい。";
			 	$html_body = "";
			 }

		} else {
			// 存在しない場合、フォーム部分にメッセージ表示
			$html_msg = "指定したURLのファイルを取得できませんですた。";
			$html_body = "";
		}
	}
		// 編集画面のページのヘッダー部分のhtmlを作成する 注：直書き
		$html_views_str = get_head_html("", "", "<b>ページ編集して下さい。</b><br />$html_msg", "", my_create_tools_path);
		// 編集画面の入力フォームのhtml作成
		$html_views_str = get_inpot_url_start_html($html_body, $html_views_str, $inpot_url, my_create_tools_path);

	} elseif($_POST['mode'] == "kakunin") { // 確認画面 ----------------------------------------------------

		// GETで受け取ったパラメータを変数へ代入
		$mode = $_POST['mode'];
		$html_title = $_POST['html_title'];
		$html_meta = $_POST['html_meta'];
		$html_body = $_POST['html_body'];
		$page_num = $_POST['page_num'];





		// 作成するページのヘッダー部分のhtmlを作成する
		//$html_views_str = get_head_html($html_title, $html_meta, $html_body, $mode, my_create_tools_path);

		$html_views_str = $html_body;

		$html_views_str = str_replace("</body>", "", $html_views_str);
		$html_views_str = str_replace("</html>", "", $html_views_str);

		$html_views_str = get_kakunin_url_html($html_title, $html_meta, $html_body, $html_views_str, $page_num, my_create_tools_path);

		$_SESSION["html_str"] = $html_body;

	} else { // 初期画面 -----------------------------------------------------------------------------------

		// 初期画面ページのヘッダー部分のhtmlを作成する 注：直書き
		$html_views_str = get_head_html("", "", "<b>ページ作成して下さい。</b>", "", my_create_tools_path);
		// 初期入力フォームのhtml作成
		$html_views_str = get_inpot_url_start_html("",$html_views_str, $inpot_url, my_create_tools_path);
	}

} elseif($selected == "upload") {
// 外部ファイル取り込み ------------------------------------------------------------------------------------------
	if($_POST['mode'] == "hensyu"){

		// GETで受け取ったパラメータを変数へ代入
		$mode = $_POST['mode'];
		$file_path = $_POST['file_path'];

//		if(isset($_POST['html_body']) || $_POST['html_body'] != ""){
//			$html_body = $_POST['html_body'];
//		}else {

//		}

		if($file_path == ""){
			// ファイルパス
			$file_path = "./".$_FILES["file_path"]["name"];
		}

		// メッセージ
		$html_msg = "<b>ファイルをアップロードしてください。</b><br />";

		// ファイルの存在チェック
		if(!(file_exists($file_path)) && $file_path != "./"){
			// ファイルが選択されているかチェック
			if (is_uploaded_file($_FILES["file_path"]["tmp_name"])) {
				// ファイルの拡張子チェック
				if(strpos($file_path, ".html") === false){
					$html_msg .= "拡張子がhtml以外のものはアップロードできません。";
					$html_body = "ここにbody部分を書き込んで下さい。";
					$mode = "";
				// ファイルを一時保存
				}elseif(move_uploaded_file($_FILES["file_path"]["tmp_name"], $file_path)) {
					chmod($file_path, $zokusei_num);
					$html_body = mb_convert_encoding(file_get_contents($file_path), 'UTF-8', 'auto');

					if($file_path != ""){
						// 一時ファイルを削除
						unlink($file_path);
					}
					// ファイルの中身の文字コードを変換
					$html_msg = "<b>編集してください。</b><br />";
				}else{
					$html_msg .= "ファイルの一時保存に失敗しました。";
					$html_body = "ここにbody部分を書き込んで下さい。";
				}
			} else {
				$html_msg .= "ファイルが選択されていません。";
				$html_body = "ここにbody部分を書き込んで下さい。";
			}
		} else {
			@unlink($file_path);
		}
		if($file_path != "./"){
			$html_body = $_SESSION['html_body'];
		}

		$html_views_str = get_head_html("","", $html_msg, $mode, my_create_tools_path);
		$html_views_str = get_upload_start_html($html_body, $html_views_str, my_create_tools_path, $mode, $file_path);
	}elseif($_POST['mode'] == "kakunin"){


		// GETで受け取ったパラメータを変数へ代入
		$mode = $_POST['mode'];
		$html_body = $_POST['html_body'];
		$page_num = $_POST['page_num'];
		$file_path = $_POST['file_path'];

		// 作成するページのヘッダー部分のhtmlを作成する
//		$html_views_str = get_upload_head_html($html_body);
//		$html_views_str = get_head_html("","",$html_body, $mode, my_create_tools_path);

		$html_views_str = $html_body;
		// $html_bodyの内容をSESSIONに格納
		$_SESSION['html_body'] = $html_views_str;

		if($file_path != "" && $html_body != ""){
			$html_views_str = str_replace("</body>", "", $html_views_str);
			$html_views_str = str_replace("</html>", "", $html_views_str);

			if(!(strpos($html_views_str, "EUC-JP") == false)){
				$html_views_str = str_replace("EUC-JP", "UTF-8", $html_views_str);
			}

			// 確認ページのbody部分のhtml作成
			$html_views_str = get_upload_kakunin_html($html_body, $html_views_str, $page_num, my_create_tools_path, $file_path);
		}else{
			$mode = "";
			$html_views_str = get_head_html("","", "<b>ファイルをアップロードしてください。</b><br />ファイルが選択されていません。", $mode, my_create_tools_path);
			$html_views_str = get_upload_start_html($html_body, $html_views_str, my_create_tools_path, $mode, $file_path);
		}

	} elseif($_POST['mode'] == "create") {

		// GETで受け取ったパラメータを変数へ代入
		$mode = $_POST['mode'];
		$page_num = $_POST['page_num'];
		$file_path = $_POST['file_path'];


		$html_body = $_SESSION['html_body'];

		// ファイル作成
		get_create_file($html_body, $page_num, $zokusei_num, $create_name, $dir_name_count, $file_name_count);

		// 作成結果画面のページのヘッダー部分のhtmlを作成する 注：直書き
		$html_views_str = get_head_html("", "", "<b>" . $page_num . "ページ作成が完了しました。</b>", "", my_create_tools_path);
		$html_views_str = get_upload_start_html("", $html_views_str, my_create_tools_path, $mode, $file_path);

	} else { // 初期画面 -----------------------------------------------------------------------------------------
		$mode = "";

		$html_views_str = get_head_html("","", "<b>ファイルをアップロードしてください。</b>", "", my_create_tools_path);
		$html_views_str = get_upload_start_html($html_body, $html_views_str, my_create_tools_path, $mode, $file_path);
	}
}












// 新規作成メソッド ----------------------------------------------------------------------------------------------
// 作成するページのヘッダー部分のhtmlを作成する
function get_head_html($html_title, $html_meta, $html_body, $mode = "", $my_path = ""){

	// body部分の改行を消す
	$html_body_rp = str_replace("\r\n", "<br />", $html_body);

	$html_str = "<html>"
				."<head>"
				."<meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>"
				."<meta name='$html_meta' />"
				."<title>$html_title</title>";

				if($mode != "kakunin" && $mode != "create"){
					$html_str = get_style_str($html_str);
				}

	$html_str .= "</head>"
				."<body>"
				."$html_body_rp"
				."<br /><br />";

	if($mode != "kakunin" && $mode != "create"){
		$html_str = get_select_btn($html_str, $my_path);
	}

	return $html_str;
}

// 確認ページのbody部分のhtml作成
function get_kakunin_html($html_body, $html_str, $page_num, $my_path){

	$html_str .= "<form action='$my_path' method='post' >"
				."<input type='submit' value='作成' />"
				."<input type='hidden' name='page_num' value='$page_num' />"
				."<input type='hidden' name='mode' value='create' />"
				."<input type='hidden' name='selected' value='new' />"
				."</form>"
				."<form action='$my_path' method='post' >"
				."<input type='submit' value='戻る' />"
				."<input type='hidden' name='mode' value='hensyu' />"
				."<input type='hidden' name='selected' value='new' />"
				."</form>"
				."</body>"
				."</html>";
	return $html_str;
}

// 初期入力フォームのbody部分のhtml作成
function get_inpot_start_html($html_body, $html_str, $my_path){
	$html_str .= "<table align='center'>"
				."<td>"
				."<form action='$my_path' method='post' >"
				."作成数<br />"
				."<select name='page_num'>"
				."<option value='10'>10</option>"
				."<option value='20'>20</option>"
				."<option value='30'>30</option>"
				."<option value='40'>40</option>"
				."<option value='10000'>10000</option>"
				."</select>"
				."<br /><br />"
				."ページ内容 全角2000文字 半角4000文字<br />"
				."<textarea name='html_body' cols='80' rows='40' >$html_body</textarea>"
				."<br /><br />"
				."<input type='submit'  value='確認' />"
				."<input type='hidden' name='mode' value='kakunin' />"
				."<input type='hidden' name='selected' value='new' />"
				."</form>"
				."</td>";
	$html_str = get_help_html($html_str);

	$html_str .= "</body>"
				."</html>";

	return $html_str;
}

// 外部URL取り込みメソッド ---------------------------------------------------------------------------------------
// 外部URL取り込みフォームのbody部分のhtml作成
function get_inpot_url_start_html($html_body, $html_str, $inpot_url, $my_path){
	$html_str .= "<?php session_start(); ?>"
				."<table align='center'>"
				."<td>"
				."<form action='$my_path' method='post' >"
				."参照先URL 200文字<br />"
				."<input type='text' size='80' maxlength='200' name='inpot_url' value='$inpot_url' />"
				."<input type='submit'  value='参照' />"
				."<input type='hidden' name='mode' value='hensyu' />"
				."<input type='hidden' name='selected' value='url_ref' />"
				."</form>"
				."<form action='$my_path' method='post' >"
				."作成数<br />"
				."<select name='page_num'>"
				."<option value='10'>10</option>"
				."<option value='20'>20</option>"
				."<option value='30'>30</option>"
				."<option value='40'>40</option>"
				."<option value='10000'>10000</option>"
				."</select>"
				."<br /><br />"
				."ページ内容 全角2000文字 半角4000文字<br />"
				."<textarea name='html_body' cols='80' rows='40' >$html_body</textarea>"
				."<br /><br />"
				."<input type='submit'  value='確認' />"
				."<input type='hidden' name='mode' value='kakunin' />"
				."<input type='hidden' name='selected' value='url_ref' />"
				."</form>"
				."</td>";

	$html_str = get_help_html($html_str);

	$html_str .= "</body>"
				."</html>";

	return $html_str;
}

// 確認ページのbody部分のhtml作成
function get_kakunin_url_html($html_title, $html_meta, $html_body, $html_str, $page_num, $my_path){

	$html_str .= "<form action='$my_path' method='post' >"
				."<input type='submit' value='作成' />"
//				."<input type='hidden' name='html_title' value='$html_title' />"
//				."<input type='hidden' name='html_meta' value='$html_meta' />"
//				."<input type='hidden' name='html_body' value='$html_body' />"
//				."<input type='hidden' name='page_num' value='$page_num' />"
				."<input type='hidden' name='mode' value='create' />"
				."<input type='hidden' name='selected' value='url_ref' />"
				."</form>"
				."<form action='$my_path' method='post' >"
				."<input type='submit' value='戻る' />"
//				."<input type='hidden' name='html_title' value='$html_title' />"
//				."<input type='hidden' name='html_meta' value='$html_meta' />"
//				."<input type='hidden' name='html_body' value='$html_body' />"
				."<input type='hidden' name='mode' value='hensyu' />"
				."<input type='hidden' name='selected' value='url_ref' />"
				."</form>"
				."</body>"
				."</html>";
	return $html_str;
}

// 外部ファイル取り込みメソッド ----------------------------------------------------------------------------------
// 確認ページのbody部分のhtml作成
function get_upload_kakunin_html($html_body, $html_str, $page_num, $my_path, $file_path){
	$html_str = str_replace("</body>", "", $html_str);
	$html_str = str_replace("</html>", "", $html_str);
//	$html_body = str_replace("'", "\'", $html_body);
	$html_body = mb_convert_encoding($html_body, 'UTF-8', 'auto');
	$html_str = mb_convert_encoding($html_str, 'UTF-8', 'auto');

	$html_str .= "<form action='$my_path' method='post' >"
				."<input type='submit' value='作成' />"
				."<input type='hidden' name='file_path' value='$file_path' />"
//				."<input type='hidden' name='html_body' value='$html_body' />"
				."<input type='hidden' name='page_num' value='$page_num' />"
				."<input type='hidden' name='mode' value='create' />"
				."<input type='hidden' name='selected' value='upload' />"
				."</form>"
				."<form action='$my_path' method='post' >"
				."<input type='submit' value='戻る' />"
//				."<input type='hidden' name='html_body' value='$html_body' />"
				."<input type='hidden' name='mode' value='hensyu' />"
				."<input type='hidden' name='selected' value='upload' />"
				."<input type='hidden' name='file_path' value='$file_path' />"
				."</form>";
//				."</body>"
//				."</html>";

	return $html_str;
}

function get_upload_start_html($html_body, $html_str, $my_path, $mode, $file_path){
	// modeごとにsubmitを変更
	if($mode == "kakunin"){
		$html_input_type = "<input type='submit' value='作成' />"
							."<input type='hidden' name='mode' value='end' />";
	} else {
		$html_input_type = "<input type='submit' value='確認' />"
							."<input type='hidden' name='mode' value='kakunin' />";
	}

	$html_str .= "<table align='center'>"
				."<td>"
				."<form action='$my_path' method='post' enctype='multipart/form-data'>"
				."<input type='file' name='file_path'><br /><br />"
				."<input type='submit' value='ｱｯﾌﾟﾛｰﾄﾞ' /><br />"
				."<input type='hidden' name='selected' value='upload' />"
				."<input type='hidden' name='mode' value='hensyu' />"
				."</form>"
				."<form action='$my_path' method='post' enctype='multipart/form-data'>"
				."作成数<br />"
				."<select name='page_num'>"
				."<option value='10'>10</option>"
				."<option value='20'>20</option>"
				."<option value='30'>30</option>"
				."<option value='40'>40</option>"
				."<option value='10000'>10000</option>"
				."</select>"
				."<br /><br />"
				."<textarea name='html_body' cols='80' rows='40' >$html_body</textarea>"
				."<br /><br />"
				.$html_input_type
				."<input type='hidden' name='selected' value='upload' />"
				."<input type='hidden' name='file_path' value='$file_path' />"
				."</form>"
				."</td>";
	$html_str = get_help_html($html_str);

	return $html_str;
}

// 新規・外部URL取り込み・外部ファイル取り込みを選択するボタン作成
function get_select_btn($html_str, $my_path){
	// 新規作成 選択ボタン
	$html_str .= "<form action='$my_path' method='post' >"
				."<input type='submit'  value='新規' />"
				."<input type='hidden' name='selected' value='new' />"
				."</form>";

	// 外部URL取り込み 選択ボタン
	$html_str .= "<form action='$my_path' method='post' >"
				."<input type='submit'  value='URL参照' />"
				."<input type='hidden' name='selected' value='url_ref' />"
				."</form>";

	// 外部ファイル取り込み 選択ボタン
	$html_str .= "<form action='$my_path' method='post' >"
				."<input type='submit'  value='ファイルアップロード' />"
				."<input type='hidden' name='selected' value='upload' />"
				."</form>";

	return $html_str;
}

// ツール仕様にあたっての説明文
function get_help_html($html_str){

	$html_str .= "<td valign='top'>"
				."<font size='-1'>"
				."<b>■作成数</b>"
				."<br />"
				."ページを作成する数です。"
				."<br />"
				."<hr />"
				."<b>■ページタイトル</b>"
				."<br />"
				."ページタイトルです。"
				."<br />"
				."<hr />"
				."<b>■検索文言</b>"
				."<br />"
				."検索ワードです。"
				."<br />"
				."記入例）テキスト,テキスト,テキスト…"
				."<br />"
				."<hr />"
				."<b>■body部分のタグ例</b>"
				."<br />"
				."イメージタグ"
				."<br />"
				."&ltimg src=&quotURL&quot /&gt"
				."<br />"
				."<br />"
				."リンクタグ"
				."<br />"
				."&lta href=&quotURL&quot&gtテキスト&lt/a&gt"
				."<br />"
				."<br />"
				."太文字タグ"
				."<br />"
				."&ltb&gtテキスト&lt/b&gt"
				."<br />"
				."<br />"
				."見出しタグ"
				."<br />"
				."&lth1&gtテキスト&lt/h1&gt"
				."<br />"
				."<br />"
				."区切り線タグ"
				."<br />"
				."&lthr /&gt"
				."<br />"
				."<hr />"
				."<b>■ツール説明</b>"
				."<br />"
				."ディレクトリの構成"
				."<br />"
				."当ツールを配置した同じ階層に新しいフォルダを作成し、"
				."<br />"
				."作成したフォルダ内へwebページを指定された数だけ作成します。"
				."<br /><br />"
				."作成時の注意"
				."<br />"
				."レンタルサーバーによっては作成されたフォルダ及び、"
				."<br />"
				."ファイルを削除することが出来ない場合があります。"
				."<br />"
				."この場合、オーダー/グループを作成したフォルダ及び、"
				."<br />"
				."ファイルに設定することで削除が可能になります。"
				."<br />"
				."尚、FTPにおける属性値は747となっております。"
				."<br />"
				."</font>"
				."</td>"
				."</table >";
	return $html_str;
}

// 入力フォームのスタイルシート作成
function get_style_str($html_str){
	$html_str .= "<style type='text/css'>"
				."td{"
				."border:1px solid #000000;"
				."padding:10px;"
				."}"
				."</style>";
	return $html_str;
}

// ファイル作成メソッド
function get_create_file($html_body, $page_num, $zokusei_num, $create_name, $dir_name_count, $file_name_count){
	// 作成したページを格納するディレクトリの名前を作成
	// タイトル+今日の日付
	$dir_name = $create_name . date("Y-m-d");
// ----------------------------------------------------------------------- 2011/02/03 修正
	// ディレクトリの確認
	while($dir_name_count > 0){
		if(is_dir("./".$dir_name)){
			// 同じディレクトリ名が存在する
			$dir_name = $create_name . date("Y-m-d") . "_" . $dir_name_count;
		}else{
			break;
		}
		$dir_name_count += 1;
	}
// -------------------------------------------------------------------------------------

// ----------------------------------------------------------------------- 2011/02/03 修正
	// ディレクトリ作成
	// ディレクトリの属性を変更
	//$ref1 = chmod(mkdir("./" . $dir_name), 0777);
	$ref1 = mkdir("./".$dir_name, $zokusei_num);
// ---------------------------------------------------------------------------------------

	// ファイル名作成
	$file_name = $dir_name;

// ----------------------------------------------------------------------- 2011/02/03 修正
	while($file_name_count > 0){
		if(file_exists($file_name)){
			// 同じファイル名が存在
			$file_name = $dir_name . "_" . $file_name_count;
		}else{
			break;
		}
		$file_name_count += 1;
	}
// ---------------------------------------------------------------------------------------

	while($file_create_count < $page_num){
		// パス作成
		// パスの構成 ./ディレクトリ名/ファイル名_数字.html
		$path_name = "./" . $dir_name . "/" . $file_name . "_" . $file_create_count . ".html";

		// ファイル書き込み
		$fp = fopen($path_name,"w+");
		fwrite($fp, stripcslashes($html_body));
		fclose($fp);

		// ファイルの属性を変更
		$ref2 = chmod($path_name, $zokusei_num);

		$file_create_count += 1;
	}
	// 書き込み処理後入力フォーム画面へ遷移 パラメータ：mode=result
//	header('Location: '.my_create_tools_path . '?mode=result&page_num=' . $page_num);
	// セッションの破棄
	session_destroy();
}



?>

<?php echo stripcslashes($html_views_str) ?>
