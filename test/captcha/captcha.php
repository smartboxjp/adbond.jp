<?
	if (!function_exists("imagettftext")) {
		die("FreeTypeが利用できません");
	}

	// 設定項目
	define("LOGIN_ENABLE_SEC", 300);// 表示した画像でログインを許可する期間(秒)
	define("IMAGE_SIZE_X", 150);// 回転前の画像サイズ（横）
	define("IMAGE_SIZE_Y", 80);// 回転前の画像サイズ（縦）
	define("IMAGE_TYPE", "jpeg");// "jpeg" or "png"
	define("FONT_FILENAME", "ipam.ttf");// TrueTypeフォントのファイル名


	function getmicrotime(){
		list($usec, $sec) = explode(" ",microtime());
		return ((float)$sec + (float)$usec);
	}

	// 不正リクエストのチェック
	function error_check($time, $key) {
		if ($time < time() - LOGIN_ENABLE_SEC) {
			// 許可秒数を超過していた場合
			return true;
		} else if ($time > time()+5) {
			// 未来の日付(不正リクエスト)
			return true;
		} else if (make_key($time) != $key) {
		}
		return false;
	}

	// キーを作る
	function make_key($time) {
		return md5($time . "\r\n" . $_SERVER['SCRIPT_FILENAME']);
	}

	// 画像に表示する文字列を作る
	function make_string($key) {
		if (strlen($key)) {
			// srand⇒乱数の初期化(srandをベースにrandが決定する)
			// hexdec⇒16進数を10進数へ変換
			// crc32⇒サムチェック
			srand(hexdec(crc32($key)));

//			$s = "あいうえおかきくけこさしすせそたちつてとなにぬねのはひふへほまみむめもやゆよらりるれろわをんがぎぐげござじずぜぞだぢづでど";

			// 画像認証の文字列の種類
			$s = "1234567890abcdefgijkmnlopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
			// 文字列は5文字
			//forを5回繰り返す
			for($i=0;$i<5;$i++) {
				// 0番目～画像認証の文字数-1で乱数を発生させる
				// ↑を文字抽出の開始位置として使用
				// 画像認証の文字数列から1文字抽出
				$string .= mb_substr($s,rand(0,mb_strlen($s)-1),1);
			}
		}
		return $string;
	}

	// 画像を作って表示する
	function make_image($time, $key) {
		//if (error_check($_GET['time'], $_GET['key'])) die("image error");
		// 画像を生成する範囲 縦×横を指定する
		$im = imagecreate(IMAGE_SIZE_X, IMAGE_SIZE_Y);
		// RGBカラーコード宣言
		imagecolorallocate($im, 255, 255, 255);

		// RGBカラーコード黒
		$black = imagecolorallocate($im, 0, 0, 0);
		// RGBカラーコード緑
		$gray  = imagecolorallocate($im, 196, 196, 196);
		// RGBカラード白
		$white= imagecolorallocate($im, 255, 255, 255);

		// keyを条件に画像に表示する文字列を取得
		$string = make_string($key);
		// 乱数の初期化
		srand();

		// 画像に表示する文字数分forで繰り返し(現在文字数が5なので5回繰り返す)
		for($i=0; $i<strlen($string); $i++) {
			// 可視性を低くさせるためにダミーの直線を入れる
			// imageline⇒画像リソース・始点X座標・始点Y座標・終点X座標・終点Y座標・RGBカラーコード
			// 縦横の始点終点値を0～サイズ-1の範囲で乱数を発生させ直線を生成
			imageline($im, rand(0,IMAGE_SIZE_X - 1),rand(0,IMAGE_SIZE_Y - 1),rand(0,IMAGE_SIZE_X - 1),rand(0,IMAGE_SIZE_Y - 1), $gray);
		}

		// 用途不明
		$font = dirname($_SERVER['SCRIPT_PATH'])."/".FONT_FILENAME;

		// 画像に表示する文字数分forで繰り返し(現在文字数が5なので5回繰り返す)
		for($i=0; $i<strlen($string); $i++) {
			// X座標 24 * カウント数 + 0～9の乱数
			$x = 24*$i + rand(0,9);
			// Y座標 26 + -6～6の乱数
			$y = 26 + rand(-6,6);
			// フォントの角度 -15～15の乱数
			$r = rand(-15,15);
			// 適度にランダムな位置に散らして文字を描く
			// imagettftext⇒TrueTypeフォントを使用してテキストを画像に書き込む
			// 画像リソース・フォントサイズ・フォントの表示角度・X座標・Y座標・RGBカラーコード・TrueTypeフォントのpath・UTF-8でエンコードされた文字列を1文字抽出
			imagettftext($im, 16, $r, $x, $y, $black, FONT_FILENAME, mb_substr(mb_convert_encoding($string, "UTF-8"), $i, 1, "UTF-8"));
		}
		// 可視性を低くさせるために回転させる
		// imagerotate⇒画像リソース・回転角度・回転後のカバーされないRGBカラー
		// 回転角度は-10～10の乱数で生成
		$im = imagerotate($im, rand(-10,10), $white);

		// イメージタイプはjpegか
		if (IMAGE_TYPE == "jpeg") {
			// イメージをjpegでヘッダーへ出力
			header('Content-type: image/jpeg');
			// jpeg形式でデータを出力
			imagejpeg($im);

		// イメージがjpeg以外か
		} else {
			// イメージをpngでヘッダーへ出力
			header('Content-type: image/png');
			// png形式でデータを出力
			imagepng($im);
		}

		// 画像リソースをメモリ上から解放
		imagedestroy($im);
	}
?>