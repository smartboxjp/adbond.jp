<?
	// captcha.phpファイルをインクルード
	include_once("captcha.php");

	// ログインチェック
	function login() {
		// リクエスト変数の文字数を確認
		if (strlen($_REQUEST['image_key'])) {
			// 時間切れチェック
			if (error_check($_REQUEST['time'], $_REQUEST['key'])) {
				// 不正リクエストか時間切れ
				echo "時間切れです<br />";
				// login.phpファイルをインクルード
				include_once("login.php");
				// 処理終了
				exit;
			}
			// リクエスト変数imega_keyを条件に画像の文字列を取得
			// ↑条件で取得した文字列とリクエスト変数keyの文字列が等しいか
			return make_string($_REQUEST['key']) == $_REQUEST['image_key'];
		}
	}

	header("Content-type: text/html; charset=utf-8");

	// loginメソッドがfalseか
	if (!login()) {
		echo "正しい文字列を入れ下さい<br />";
		// login.phpファイルをインクルード
		include_once("login.php");
		// 処理終了
		exit;
	}

	// 以下、ログインに成功したときの処理
	echo "認証成功しました";
	exit;
?>