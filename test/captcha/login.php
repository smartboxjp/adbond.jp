<?
	// captcha.phpファイルのmake_imageメソッドが定義されていなかった場合処理を終了する
	if (!function_exists("make_image")) die;

	// captcha.phpファイルのgetmicrotimeメソッドより画像の有効時間 現在の時間を取得
	// timeを条件にcaptcha.phpファイルのmake_keyメソッドよりキーを作成変数へ格納
	$key = make_key($time = getmicrotime());
?>
<html>
<body>
※文字が読みにくい場合は、何も入力せずに「ログイン」ボタンを押して再表示してください
<form action="?" method="POST">
 <!-- image.phpから画像パスを取得 -->
 <!-- GETパラメータとしてtime、keyを付与 -->
 <img src="image.php?time=<?=$time?>&key=<?=$key?>">
 <br />
 画像の文字列：<input type=text name=image_key>
 <br />
 <br />
 <!-- hiddenでtimeとkeyをPOST送信 -->
 <input type=hidden name=time value="<?=$time?>">
 <input type=hidden name=key value="<?=$key?>">
 <input type=submit value="ログイン">
 <br />

</form>
</body>
</html>