<?php

$test_post = "";
$test_get = "";

// postチェック
if(isset($_POST['test_soshin']) && $_POST['test_soshin'] != ""){
	$test_post = $_POST['test_soshin'];
}

// getチェック
if(isset($_GET['test_soshin']) && $_GET['test_soshin'] != ""){
	$test_get = $_GET['test_soshin'];
}


?>


<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>送信テスト</title>
	</head>
	<body>
		<?php if($test_post == "" && $test_get == ""){ ?>
			<form method="POST" action="" name="search_form">
				<b>POST送信</b>
				<br />
				送信したパラメータを入力して下さい。
				<br />
				<input type="text" name="test_soshin" value="" size="50" />
				<input type="submit" value="送信" />
			</form>
			<br />
			<form method="GET" action="" name="search_form">
				<b>GET送信</b>
				<br />
				送信したパラメータを入力して下さい。
				<br />
				<input type="text" name="test_soshin" value="" size="50" />
				<input type="submit" value="送信" />
			</form>
		<?php } else { ?>
			<form method="GET" action="" name="search_form">

				<?php if($test_post != ""){ ?>
					<b>POST送信結果表示</b>
					<br />
					test_postというパラメータ変数に「
					<?php echo $test_post; ?>
					」というパラメータが入りました。
				<?php } ?>

				<?php if($test_get != ""){ ?>
					<b>GET送信結果表示</b>
					<br />
					test_getというパラメータ変数に「
					<?php echo $test_get; ?>
					」というパラメータが入りました。
				<?php } ?>

				<input type="submit" value="戻る" />
			</form>
		<?php } ?>

	</body>
</html>