<?php
// セリフSQL作成用スクリプト
// @author Ogura
// @date 12/06/17(日) 16:36:51

$file = "word_area.csv";
//$file = "word_slot.csv";

@unlink($file . ".sql");

if($handle = fopen($file, "r")){
	$cnt = 1;
// 	header("Content-Type: text/csv; charset=Shift_JIS ");
	while($data = fgetcsv($handle, 0, ",")){
		// 1行目はタイトルなので
		if($cnt != 1 && !empty($data[0])){
			// sqlの作成
			$sql = createSql($data);
			error_log($sql . "\n\n", 3, $file . ".sql");
		}
		$cnt ++;
	}
}

function createSql($data){

	// 空文字の調整
	$data[33] = empty($data[33]) ? $data[32] : $data[33];
	$data[34] = empty($data[34]) ? $data[32] : $data[34];
	$data[36] = empty($data[36]) ? $data[35] : $data[36];
	$data[37] = empty($data[37]) ? $data[35] : $data[37];
	$data[39] = empty($data[39]) ? $data[38] : $data[39];
	$data[40] = empty($data[40]) ? $data[38] : $data[40];

	$data[44] = empty($data[44]) ? $data[43] : $data[44];
	$data[45] = empty($data[45]) ? $data[43] : $data[45];
	$data[47] = empty($data[47]) ? $data[46] : $data[47];
	$data[48] = empty($data[48]) ? $data[46] : $data[48];
	$data[50] = empty($data[50]) ? $data[49] : $data[50];
	$data[51] = empty($data[51]) ? $data[49] : $data[51];

	$data[55] = empty($data[55]) ? $data[54] : $data[55];
	$data[56] = empty($data[56]) ? $data[54] : $data[56];
	$data[58] = empty($data[58]) ? $data[57] : $data[58];
	$data[59] = empty($data[59]) ? $data[57] : $data[59];
	$data[61] = empty($data[61]) ? $data[60] : $data[61];
	$data[62] = empty($data[62]) ? $data[60] : $data[62];

	$data[66] = empty($data[66]) ? $data[65] : $data[66];
	$data[67] = empty($data[67]) ? $data[65] : $data[67];
	$data[69] = empty($data[69]) ? $data[68] : $data[69];
	$data[70] = empty($data[70]) ? $data[68] : $data[70];
	$data[72] = empty($data[72]) ? $data[71] : $data[72];
	$data[73] = empty($data[73]) ? $data[71] : $data[73];

	$sql =
	"-- start query \n" .
	"update word_data set \n\n" .

	"mypage_morning_1 = '{mb_check_encoding($data[1], "UTF-8")}',\n" .
	"mypage_morning_2 = '{mb_check_encoding($data[2], "UTF-8")}',\n" .
	"mypage_morning_3 = '{mb_check_encoding($data[3], "UTF-8")}',\n" .
	"mypage_morning_4 = '{mb_check_encoding($data[4], "UTF-8")}',\n" .
	"mypage_morning_5 = '{mb_check_encoding($data[5], "UTF-8")}',\n" .

	"mypage_daytime_1 = '{mb_check_encoding($data[6], "UTF-8")}',\n" .
	"mypage_daytime_2 = '{mb_check_encoding($data[7], "UTF-8")}',\n" .
	"mypage_daytime_3 = '{mb_check_encoding($data[8], "UTF-8")}',\n" .
	"mypage_daytime_4 = '{mb_check_encoding($data[9], "UTF-8")}',\n" .
	"mypage_daytime_5 = '{mb_check_encoding($data[10], "UTF-8")}',\n" .

	"mypage_evening_1 = '{mb_check_encoding($data[11], "UTF-8")}',\n" .
	"mypage_evening_2 = '{mb_check_encoding($data[12], "UTF-8")}',\n" .
	"mypage_evening_3 = '{mb_check_encoding($data[13], "UTF-8")}',\n" .
	"mypage_evening_4 = '{mb_check_encoding($data[14], "UTF-8")}',\n" .
	"mypage_evening_5 = '{mb_check_encoding($data[15], "UTF-8")}',\n" .

	"mypage_night_1 = '{mb_check_encoding($data[16], "UTF-8")}',\n" .
	"mypage_night_2 = '{mb_check_encoding($data[17], "UTF-8")}',\n" .
	"mypage_night_3 = '{mb_check_encoding($data[18], "UTF-8")}',\n" .
	"mypage_night_4 = '{mb_check_encoding($data[19], "UTF-8")}',\n" .
	"mypage_night_5 = '{mb_check_encoding($data[20], "UTF-8")}',\n" .

	"mypage_random_1 = '{mb_check_encoding($data[21], "UTF-8")}',\n" .
	"mypage_random_2 = '{mb_check_encoding($data[22], "UTF-8")}',\n" .
	"mypage_random_3 = '{mb_check_encoding($data[23], "UTF-8")}',\n" .
	"mypage_random_4 = '{mb_check_encoding($data[24], "UTF-8")}',\n" .
	"mypage_random_5 = '{mb_check_encoding($data[25], "UTF-8")}',\n" .

	"quest_battle_1 = '{mb_check_encoding($data[26], "UTF-8")}',\n" .
	"quest_battle_2 = '{mb_check_encoding($data[27], "UTF-8")}',\n" .
	"quest_battle_3 = '{mb_check_encoding($data[28], "UTF-8")}',\n" .
	"quest_battle_4 = '{mb_check_encoding($data[29], "UTF-8")}',\n" .
	//"quest_battle_5 = '{mb_check_encoding($data[30], "UTF-8")}'," .
	//"quest_battle_6 = '{mb_check_encoding($data[31], "UTF-8")}'," .
	//"quest_battle_7 = '{mb_check_encoding($data[32], "UTF-8")}'," .
	"quest_battle_5 = '{mb_check_encoding($data[77], "UTF-8")}',\n" .
	"quest_battle_6 = '{mb_check_encoding($data[78], "UTF-8")}',\n" .
	"quest_battle_7 = '{mb_check_encoding($data[79], "UTF-8")}',\n" .

	"quest_talk_1 = '{mb_check_encoding($data[30], "UTF-8")}',\n" .
	"quest_talk_2 = '{mb_check_encoding($data[31], "UTF-8")}',\n" .
	"quest_talk_3 = '{mb_check_encoding($data[32], "UTF-8")}',\n" .

	"quest_talk_3_2 = '{mb_check_encoding($data[33], "UTF-8")}',\n" .
	"quest_talk_3_3 = '{mb_check_encoding($data[34], "UTF-8")}',\n" .

	"quest_talk_4 = '{mb_check_encoding($data[35], "UTF-8")}',\n" .

	"quest_talk_4_2 = '{mb_check_encoding($data[36], "UTF-8")}',\n" .
	"quest_talk_4_3 = '{mb_check_encoding($data[37], "UTF-8")}',\n" .

	"quest_talk_5 = '{mb_check_encoding($data[38], "UTF-8")}',\n" .

	"quest_talk_5_2 = '{mb_check_encoding($data[39], "UTF-8")}',\n" .
	"quest_talk_5_3 = '{mb_check_encoding($data[40], "UTF-8")}',\n" .

	"quest_patrol_1 = '{mb_check_encoding($data[41], "UTF-8")}',\n" .
	"quest_patrol_2 = '{mb_check_encoding($data[42], "UTF-8")}',\n" .
	"quest_patrol_3 = '{mb_check_encoding($data[43], "UTF-8")}',\n" .

	"quest_patrol_3_2 = '{mb_check_encoding($data[44], "UTF-8")}',\n" .
	"quest_patrol_3_3 = '{mb_check_encoding($data[45], "UTF-8")}',\n" .

	"quest_patrol_4 = '{mb_check_encoding($data[46], "UTF-8")}',\n" .

	"quest_patrol_4_2 = '{mb_check_encoding($data[47], "UTF-8")}',\n" .
	"quest_patrol_4_3 = '{mb_check_encoding($data[48], "UTF-8")}',\n" .


	"quest_patrol_5 = '{mb_check_encoding($data[49], "UTF-8")}',\n" .

	"quest_patrol_5_2 = '{mb_check_encoding($data[50], "UTF-8")}',\n" .
	"quest_patrol_5_3 = '{mb_check_encoding($data[51], "UTF-8")}',\n" .

	"quest_cook_1 = '{mb_check_encoding($data[52], "UTF-8")}',\n" .
	"quest_cook_2 = '{mb_check_encoding($data[53], "UTF-8")}',\n" .
	"quest_cook_3 = '{mb_check_encoding($data[54], "UTF-8")}',\n" .

	"quest_cook_3_2 = '{mb_check_encoding($data[55], "UTF-8")}',\n" .
	"quest_cook_3_3 = '{mb_check_encoding($data[56], "UTF-8")}',\n" .

	"quest_cook_4 = '{mb_check_encoding($data[57], "UTF-8")}',\n" .

	"quest_cook_4_2 = '{mb_check_encoding($data[58], "UTF-8")}',\n" .
	"quest_cook_4_3 = '{mb_check_encoding($data[59], "UTF-8")}',\n" .

	"quest_cook_5 = '{mb_check_encoding($data[60], "UTF-8")}',\n" .

	"quest_cook_5_2 = '{mb_check_encoding($data[61], "UTF-8")}',\n" .
	"quest_cook_5_3 = '{mb_check_encoding($data[62], "UTF-8")}',\n" .

	"quest_play_1 = '{mb_check_encoding($data[63], "UTF-8")}',\n" .
	"quest_play_2 = '{mb_check_encoding($data[64], "UTF-8")}',\n" .
	"quest_play_3 = '{mb_check_encoding($data[65], "UTF-8")}',\n" .

	"quest_play_3_2 = '{mb_check_encoding($data[66], "UTF-8")}',\n" .
	"quest_play_3_3 = '{mb_check_encoding($data[67], "UTF-8")}',\n" .

	"quest_play_4 = '{mb_check_encoding($data[68], "UTF-8")}',\n" .

	"quest_play_4_2 = '{mb_check_encoding($data[69], "UTF-8")}',\n" .
	"quest_play_4_3 = '{mb_check_encoding($data[70], "UTF-8")}',\n" .

	"quest_play_5 = '{mb_check_encoding($data[71], "UTF-8")}',\n" .

	"quest_play_5_2 = '{mb_check_encoding($data[72], "UTF-8")}',\n" .
	"quest_play_5_3 = '{mb_check_encoding($data[73], "UTF-8")}',\n" .

	"battle_start = '{mb_check_encoding($data[74], "UTF-8")}',\n" .
	"battle_lose = '{mb_check_encoding($data[75], "UTF-8")}',\n" .
	"battle_win = '{mb_check_encoding($data[76], "UTF-8")}'\n" .

	"\n where id = " . (int)$data[0] . ";";


	return $sql;
}
