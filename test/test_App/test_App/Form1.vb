﻿Public Class Form1

    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        'フォームオブジェクトを変数へ格納
        Dim a As Form2
        '絶対パスを格納する変数
        Dim zettaiPath As String
        '早退パスを格納する変数
        Dim soutaiPath As String = "..\..\save_us\test.html"
        'HTMLを格納する変数
        Dim strHtml As String

        'HTMLの構文を作成
        strHtml = ""
        strHtml = strHtml & "<?xml version='1.0' encoding='utf-8' ?>"
        strHtml = strHtml & "<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>"
        strHtml = strHtml & "<html xmlns='http://www.w3.org/1999/xhtml' xml:lang='ja' lang='ja'>"
        strHtml = strHtml & "<head>"
        'METAタグ
        strHtml = strHtml & "<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />"
        strHtml = strHtml & "<meta name='description' content='" & Text_name_2.Text & "'>"
        strHtml = strHtml & "<meta name='keywords' lang='ja' content='" & Text_name_5.Text & "'>"
        'ページタイトル
        strHtml = strHtml & "<title>"
        strHtml = strHtml & Text_name_1.Text
        strHtml = strHtml & "</title>"
        strHtml = strHtml & "</head>"
        strHtml = strHtml & "<body>"
        strHtml = strHtml & "<a href='http://www.google.co.jp/'>google</a>"
        strHtml = strHtml & "<br />"
        strHtml = strHtml & "<hr />"
        strHtml = strHtml & "</body>"
        strHtml = strHtml & "</html>"

        MsgBox(strHtml)

        '絶対パスから早退パスを取得
        zettaiPath = System.IO.Path.GetFullPath(soutaiPath)

        'ストリームで対象ファイルを開く
        Dim sw As New System.IO.StreamWriter(zettaiPath, _
        False, _
        System.Text.Encoding.GetEncoding("UTF-8"))

        'ファイルに内容を書き込む
        sw.Write(strHtml)
        'ファイルを閉じる
        sw.Close()
        'フォーム2を生成
        a = New Form2
        'フォーム2を開く
        a.Show(Me)

    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click

        Dim ofd As New OpenFileDialog()

        'ファイルの種類を表示
        ofd.Filter = "CSSファイル(*.css)|*.css|すべてのファイル(*.*)|*.*"

        If ofd.ShowDialog() = System.Windows.Forms.DialogResult.OK Then

            'Dim sr As New System.IO.StreamReader(OpenFileDialog1.FileName)
            'ファイルの内容をメッセージボックスに表示
            'MessageBox.Show(sr.ReadToEnd)
            Text_name_8.Text = ofd.FileName
            'sr.Close()
        End If

    End Sub

    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click

        Dim ofd As New OpenFileDialog()

        'ファイルの種類を表示
        ofd.Filter = "JPEGファイル(*.jpeg;*.jpg)|*.jpeg;*.jpg|GIF(*.gif)|*.gif"

        If ofd.ShowDialog() = System.Windows.Forms.DialogResult.OK Then

            'Dim sr As New System.IO.StreamReader(OpenFileDialog1.FileName)
            'ファイルの内容をメッセージボックスに表示
            'MessageBox.Show(sr.ReadToEnd)
            Text_name_9.Text = ofd.FileName
            'sr.Close()
        End If

    End Sub
End Class
