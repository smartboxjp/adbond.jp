﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'フォームがコンポーネントの一覧をクリーンアップするために dispose をオーバーライドします。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Windows フォーム デザイナで必要です。
    Private components As System.ComponentModel.IContainer

    'メモ: 以下のプロシージャは Windows フォーム デザイナで必要です。
    'Windows フォーム デザイナを使用して変更できます。  
    'コード エディタを使って変更しないでください。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Button1 = New System.Windows.Forms.Button
        Me.Button2 = New System.Windows.Forms.Button
        Me.TabControl1 = New System.Windows.Forms.TabControl
        Me.TabPage1 = New System.Windows.Forms.TabPage
        Me.GroupBox2 = New System.Windows.Forms.GroupBox
        Me.RadioButton4 = New System.Windows.Forms.RadioButton
        Me.RadioButton3 = New System.Windows.Forms.RadioButton
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.RadioButton2 = New System.Windows.Forms.RadioButton
        Me.RadioButton1 = New System.Windows.Forms.RadioButton
        Me.Button4 = New System.Windows.Forms.Button
        Me.Text_name_9 = New System.Windows.Forms.TextBox
        Me.Lbl_name_9 = New System.Windows.Forms.Label
        Me.Button3 = New System.Windows.Forms.Button
        Me.Text_name_8 = New System.Windows.Forms.TextBox
        Me.Lbl_name_8 = New System.Windows.Forms.Label
        Me.Text_name_7 = New System.Windows.Forms.TextBox
        Me.Lbl_name_7 = New System.Windows.Forms.Label
        Me.Text_name_6 = New System.Windows.Forms.TextBox
        Me.Lbl_name_6 = New System.Windows.Forms.Label
        Me.Text_name_5 = New System.Windows.Forms.TextBox
        Me.Lbl_name_5 = New System.Windows.Forms.Label
        Me.Text_name_4 = New System.Windows.Forms.TextBox
        Me.Lbl_name_4 = New System.Windows.Forms.Label
        Me.Text_name_3 = New System.Windows.Forms.TextBox
        Me.Lbl_name_3 = New System.Windows.Forms.Label
        Me.Text_name_2 = New System.Windows.Forms.TextBox
        Me.Lbl_name_2 = New System.Windows.Forms.Label
        Me.Text_name_1 = New System.Windows.Forms.TextBox
        Me.Lbl_mane_1 = New System.Windows.Forms.Label
        Me.TabPage2 = New System.Windows.Forms.TabPage
        Me.TabPage3 = New System.Windows.Forms.TabPage
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(124, 563)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(154, 24)
        Me.Button1.TabIndex = 0
        Me.Button1.Text = "プレビュー"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(284, 563)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(154, 24)
        Me.Button2.TabIndex = 1
        Me.Button2.Text = "キャンセル"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabPage3)
        Me.TabControl1.Location = New System.Drawing.Point(12, 12)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(534, 545)
        Me.TabControl1.TabIndex = 2
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.GroupBox2)
        Me.TabPage1.Controls.Add(Me.GroupBox1)
        Me.TabPage1.Controls.Add(Me.Button4)
        Me.TabPage1.Controls.Add(Me.Text_name_9)
        Me.TabPage1.Controls.Add(Me.Lbl_name_9)
        Me.TabPage1.Controls.Add(Me.Button3)
        Me.TabPage1.Controls.Add(Me.Text_name_8)
        Me.TabPage1.Controls.Add(Me.Lbl_name_8)
        Me.TabPage1.Controls.Add(Me.Text_name_7)
        Me.TabPage1.Controls.Add(Me.Lbl_name_7)
        Me.TabPage1.Controls.Add(Me.Text_name_6)
        Me.TabPage1.Controls.Add(Me.Lbl_name_6)
        Me.TabPage1.Controls.Add(Me.Text_name_5)
        Me.TabPage1.Controls.Add(Me.Lbl_name_5)
        Me.TabPage1.Controls.Add(Me.Text_name_4)
        Me.TabPage1.Controls.Add(Me.Lbl_name_4)
        Me.TabPage1.Controls.Add(Me.Text_name_3)
        Me.TabPage1.Controls.Add(Me.Lbl_name_3)
        Me.TabPage1.Controls.Add(Me.Text_name_2)
        Me.TabPage1.Controls.Add(Me.Lbl_name_2)
        Me.TabPage1.Controls.Add(Me.Text_name_1)
        Me.TabPage1.Controls.Add(Me.Lbl_mane_1)
        Me.TabPage1.Location = New System.Drawing.Point(4, 21)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(526, 520)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "TabPage1"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.RadioButton4)
        Me.GroupBox2.Controls.Add(Me.RadioButton3)
        Me.GroupBox2.Location = New System.Drawing.Point(270, 326)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(249, 57)
        Me.GroupBox2.TabIndex = 23
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "サイトタイプ"
        '
        'RadioButton4
        '
        Me.RadioButton4.AutoSize = True
        Me.RadioButton4.Location = New System.Drawing.Point(120, 25)
        Me.RadioButton4.Name = "RadioButton4"
        Me.RadioButton4.Size = New System.Drawing.Size(92, 16)
        Me.RadioButton4.TabIndex = 1
        Me.RadioButton4.TabStop = True
        Me.RadioButton4.Text = "RadioButton4"
        Me.RadioButton4.UseVisualStyleBackColor = True
        '
        'RadioButton3
        '
        Me.RadioButton3.AutoSize = True
        Me.RadioButton3.Checked = True
        Me.RadioButton3.Location = New System.Drawing.Point(6, 25)
        Me.RadioButton3.Name = "RadioButton3"
        Me.RadioButton3.Size = New System.Drawing.Size(92, 16)
        Me.RadioButton3.TabIndex = 0
        Me.RadioButton3.TabStop = True
        Me.RadioButton3.Text = "RadioButton3"
        Me.RadioButton3.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.RadioButton2)
        Me.GroupBox1.Controls.Add(Me.RadioButton1)
        Me.GroupBox1.Location = New System.Drawing.Point(270, 263)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(250, 57)
        Me.GroupBox1.TabIndex = 22
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "出力方法"
        '
        'RadioButton2
        '
        Me.RadioButton2.AutoSize = True
        Me.RadioButton2.Location = New System.Drawing.Point(121, 26)
        Me.RadioButton2.Name = "RadioButton2"
        Me.RadioButton2.Size = New System.Drawing.Size(92, 16)
        Me.RadioButton2.TabIndex = 1
        Me.RadioButton2.TabStop = True
        Me.RadioButton2.Text = "RadioButton2"
        Me.RadioButton2.UseVisualStyleBackColor = True
        '
        'RadioButton1
        '
        Me.RadioButton1.AutoSize = True
        Me.RadioButton1.Checked = True
        Me.RadioButton1.Location = New System.Drawing.Point(6, 26)
        Me.RadioButton1.Name = "RadioButton1"
        Me.RadioButton1.Size = New System.Drawing.Size(92, 16)
        Me.RadioButton1.TabIndex = 0
        Me.RadioButton1.TabStop = True
        Me.RadioButton1.Text = "RadioButton1"
        Me.RadioButton1.UseVisualStyleBackColor = True
        '
        'Button4
        '
        Me.Button4.Location = New System.Drawing.Point(466, 238)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(54, 19)
        Me.Button4.TabIndex = 19
        Me.Button4.Text = "参照"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'Text_name_9
        '
        Me.Text_name_9.Location = New System.Drawing.Point(270, 238)
        Me.Text_name_9.Name = "Text_name_9"
        Me.Text_name_9.Size = New System.Drawing.Size(190, 19)
        Me.Text_name_9.TabIndex = 18
        '
        'Lbl_name_9
        '
        Me.Lbl_name_9.AutoSize = True
        Me.Lbl_name_9.Location = New System.Drawing.Point(268, 223)
        Me.Lbl_name_9.Name = "Lbl_name_9"
        Me.Lbl_name_9.Size = New System.Drawing.Size(65, 12)
        Me.Lbl_name_9.TabIndex = 17
        Me.Lbl_name_9.Text = "ヘッダー画像"
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(466, 201)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(54, 19)
        Me.Button3.TabIndex = 16
        Me.Button3.Text = "参照"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Text_name_8
        '
        Me.Text_name_8.Location = New System.Drawing.Point(270, 201)
        Me.Text_name_8.Name = "Text_name_8"
        Me.Text_name_8.Size = New System.Drawing.Size(190, 19)
        Me.Text_name_8.TabIndex = 15
        '
        'Lbl_name_8
        '
        Me.Lbl_name_8.AutoSize = True
        Me.Lbl_name_8.Location = New System.Drawing.Point(268, 186)
        Me.Lbl_name_8.Name = "Lbl_name_8"
        Me.Lbl_name_8.Size = New System.Drawing.Size(95, 12)
        Me.Lbl_name_8.TabIndex = 14
        Me.Lbl_name_8.Text = "テンプレート（必須）"
        '
        'Text_name_7
        '
        Me.Text_name_7.Location = New System.Drawing.Point(270, 18)
        Me.Text_name_7.Multiline = True
        Me.Text_name_7.Name = "Text_name_7"
        Me.Text_name_7.Size = New System.Drawing.Size(250, 165)
        Me.Text_name_7.TabIndex = 13
        '
        'Lbl_name_7
        '
        Me.Lbl_name_7.AutoSize = True
        Me.Lbl_name_7.Location = New System.Drawing.Point(268, 3)
        Me.Lbl_name_7.Name = "Lbl_name_7"
        Me.Lbl_name_7.Size = New System.Drawing.Size(82, 12)
        Me.Lbl_name_7.TabIndex = 12
        Me.Lbl_name_7.Text = "アクセス解析タグ"
        '
        'Text_name_6
        '
        Me.Text_name_6.Location = New System.Drawing.Point(6, 360)
        Me.Text_name_6.Name = "Text_name_6"
        Me.Text_name_6.Size = New System.Drawing.Size(250, 19)
        Me.Text_name_6.TabIndex = 11
        '
        'Lbl_name_6
        '
        Me.Lbl_name_6.AutoSize = True
        Me.Lbl_name_6.Location = New System.Drawing.Point(6, 345)
        Me.Lbl_name_6.Name = "Lbl_name_6"
        Me.Lbl_name_6.Size = New System.Drawing.Size(114, 12)
        Me.Lbl_name_6.TabIndex = 10
        Me.Lbl_name_6.Text = "サイトURL（半角英数）"
        '
        'Text_name_5
        '
        Me.Text_name_5.Location = New System.Drawing.Point(6, 323)
        Me.Text_name_5.Name = "Text_name_5"
        Me.Text_name_5.Size = New System.Drawing.Size(250, 19)
        Me.Text_name_5.TabIndex = 9
        '
        'Lbl_name_5
        '
        Me.Lbl_name_5.AutoSize = True
        Me.Lbl_name_5.Location = New System.Drawing.Point(6, 308)
        Me.Lbl_name_5.Name = "Lbl_name_5"
        Me.Lbl_name_5.Size = New System.Drawing.Size(179, 12)
        Me.Lbl_name_5.TabIndex = 8
        Me.Lbl_name_5.Text = "METAキーワード（半角カンマ区切り）"
        '
        'Text_name_4
        '
        Me.Text_name_4.Location = New System.Drawing.Point(6, 286)
        Me.Text_name_4.Name = "Text_name_4"
        Me.Text_name_4.Size = New System.Drawing.Size(250, 19)
        Me.Text_name_4.TabIndex = 7
        '
        'Lbl_name_4
        '
        Me.Lbl_name_4.AutoSize = True
        Me.Lbl_name_4.Location = New System.Drawing.Point(6, 271)
        Me.Lbl_name_4.Name = "Lbl_name_4"
        Me.Lbl_name_4.Size = New System.Drawing.Size(77, 12)
        Me.Lbl_name_4.TabIndex = 6
        Me.Lbl_name_4.Text = "ヘッダーテキスト"
        '
        'Text_name_3
        '
        Me.Text_name_3.Location = New System.Drawing.Point(6, 249)
        Me.Text_name_3.Name = "Text_name_3"
        Me.Text_name_3.Size = New System.Drawing.Size(250, 19)
        Me.Text_name_3.TabIndex = 5
        '
        'Lbl_name_3
        '
        Me.Lbl_name_3.AutoSize = True
        Me.Lbl_name_3.Location = New System.Drawing.Point(6, 234)
        Me.Lbl_name_3.Name = "Lbl_name_3"
        Me.Lbl_name_3.Size = New System.Drawing.Size(55, 12)
        Me.Lbl_name_3.TabIndex = 4
        Me.Lbl_name_3.Text = "H1テキスト"
        '
        'Text_name_2
        '
        Me.Text_name_2.Location = New System.Drawing.Point(6, 55)
        Me.Text_name_2.Multiline = True
        Me.Text_name_2.Name = "Text_name_2"
        Me.Text_name_2.Size = New System.Drawing.Size(250, 176)
        Me.Text_name_2.TabIndex = 3
        '
        'Lbl_name_2
        '
        Me.Lbl_name_2.AutoSize = True
        Me.Lbl_name_2.Location = New System.Drawing.Point(6, 40)
        Me.Lbl_name_2.Name = "Lbl_name_2"
        Me.Lbl_name_2.Size = New System.Drawing.Size(56, 12)
        Me.Lbl_name_2.TabIndex = 2
        Me.Lbl_name_2.Text = "サイト概要"
        '
        'Text_name_1
        '
        Me.Text_name_1.Location = New System.Drawing.Point(6, 18)
        Me.Text_name_1.Name = "Text_name_1"
        Me.Text_name_1.Size = New System.Drawing.Size(220, 19)
        Me.Text_name_1.TabIndex = 1
        '
        'Lbl_mane_1
        '
        Me.Lbl_mane_1.AutoSize = True
        Me.Lbl_mane_1.Location = New System.Drawing.Point(6, 3)
        Me.Lbl_mane_1.Name = "Lbl_mane_1"
        Me.Lbl_mane_1.Size = New System.Drawing.Size(44, 12)
        Me.Lbl_mane_1.TabIndex = 0
        Me.Lbl_mane_1.Text = "サイト名"
        '
        'TabPage2
        '
        Me.TabPage2.Location = New System.Drawing.Point(4, 21)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(526, 520)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "TabPage2"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'TabPage3
        '
        Me.TabPage3.Location = New System.Drawing.Point(4, 21)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage3.Size = New System.Drawing.Size(526, 520)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "TabPage3"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 12.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(558, 599)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents Lbl_mane_1 As System.Windows.Forms.Label
    Friend WithEvents Text_name_2 As System.Windows.Forms.TextBox
    Friend WithEvents Lbl_name_2 As System.Windows.Forms.Label
    Friend WithEvents Text_name_1 As System.Windows.Forms.TextBox
    Friend WithEvents Text_name_4 As System.Windows.Forms.TextBox
    Friend WithEvents Lbl_name_4 As System.Windows.Forms.Label
    Friend WithEvents Text_name_3 As System.Windows.Forms.TextBox
    Friend WithEvents Lbl_name_3 As System.Windows.Forms.Label
    Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
    Friend WithEvents Text_name_6 As System.Windows.Forms.TextBox
    Friend WithEvents Lbl_name_6 As System.Windows.Forms.Label
    Friend WithEvents Text_name_5 As System.Windows.Forms.TextBox
    Friend WithEvents Lbl_name_5 As System.Windows.Forms.Label
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Text_name_9 As System.Windows.Forms.TextBox
    Friend WithEvents Lbl_name_9 As System.Windows.Forms.Label
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Text_name_8 As System.Windows.Forms.TextBox
    Friend WithEvents Lbl_name_8 As System.Windows.Forms.Label
    Friend WithEvents Text_name_7 As System.Windows.Forms.TextBox
    Friend WithEvents Lbl_name_7 As System.Windows.Forms.Label
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents RadioButton2 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton1 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton4 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton3 As System.Windows.Forms.RadioButton
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog

End Class
