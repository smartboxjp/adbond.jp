<?php
define('SMARTY_DIR', 'Smarty/libs/');
require_once(SMARTY_DIR . 'Smarty.class.php');
require_once( './common/CommonAdminBase.php' );
require_once( './common/CommonDao.php' );
require_once( './dto/LoginUser.php' );
require_once( './dao/MediaLoginUserDao.php' );
require_once( './dto/MediaLoginUser.php' );
require_once( './dao/MediaPublisherDao.php' );
require_once( './dto/MediaPublisher.php' );
require_once( './dao/MediaGroupDao.php' );
require_once( './dto/MediaGroup.php' );
require_once( './dao/PrefDao.php' );
require_once( './dto/Pref.php' );
require_once( './dao/MediaCategoryDao.php' );
require_once( './dto/MediaCategory.php' );
require_once( './dao/UserContactDao.php' );
require_once( './dto/UserContact.php' );

	session_start();
	session_regenerate_id(true);

	// Smartyオブジェクト取得
	$smarty = new Smarty();
	$smarty->template_dir = './templates/web/';
	$smarty->compile_id   = 'web';
	$smarty->compile_dir  = './templates_c/';
	$smarty->config_dir   = './config/';
	$smarty->cache_dir    = './cache/';

	$common_dao = new CommonDao();
	$user_contact_dao = new UserContactDao();

	//都道府県を取得
	$pref_dao = new PrefDao();
	$pref_array = array();
	foreach($pref_dao->getAllPref() as $val){
		$row_array = array('id' => $val->getId(), 'name' => $val->getName());
		$pref_array[$val->getId()] = $row_array;
	}
	$smarty->assign("pref_array", $pref_array);

	//入力フォームの状態を取得
	$mode = ($_POST['mode']);
//----------------------------------------------------------お問い合わせ入力内容
	//POSTで取得
	$user_name = ($_POST['user_name']);
	$tel = ($_POST['tel']);

	$email = ($_POST['email']);
	$zipcode1 = ($_POST['zipcode1']);
	$zipcode2 = ($_POST['zipcode2']);
	$pref = ($_POST['pref']);
	$address1 = ($_POST['address1']);
	$address2 = ($_POST['address2']);
	$textarea = ($_POST['textarea']);

	//POSTで取得した情報を配列に格納
	$form_data = array('user_name' => $user_name,
		'tel' => $tel,
		'email' => $email,
		'zipcode1' => $zipcode1,
		'zipcode2' => $zipcode2,
		'pref' => $pref,
		'address1' => $address1,
		'address2' => $address2,
		'textarea' => $textarea
	);
	$smarty->assign("form_data", $form_data);

//----------------------------------------------------------
	//変数の初期値設定
	$insert_commit = "insert_commit";		//入力確認まで
	$insert_commit_on = "insert_commit_on";	//登録
	$error_flag = 0;						//エラー判別フラグ
	$flag_t = 0;							//フラグTrue用
	$flag_f = 1;							//フラグFalse用
	$error_message = "";					//エラーメッセージ
	$conf_message = "ご入力内容の確認";		//入力確認メッセージ
	$max_len = 250;							//お問い合わせの最大文字数250
//----------------------------------------------------------


	if($_POST['mode'] == ''){
			$smarty->assign("mode", $insert_commit);
			//$smarty->assign("sub_title", '新規追加');

			// ページを表示
			$smarty->display("./contact.tpl");
			exit();
	}elseif($mode == 'insert_commit'){

//----------------------------------------------------------check 後で関数化

		if($user_name == "") {					//貴社名 空白チェック
			$error_message = "貴社名を入力してください。";
			$smarty->assign("error_message", $error_message);
			$error_flag = $flag_f;
		}elseif ($email == ""){					//メルアド 空白チェック
			$error_message = "メールアドレスを入力してください。";
			$smarty->assign("error_message", $error_message);
			$error_flag = $flag_f;
		}elseif ($textarea == ""){				//お問い合わせ 空白内容チェック
			$error_message = "お問合せ内容を入力してください。";
			$smarty->assign("error_message", $error_message);
			$error_flag = $flag_f;
		}elseif (mb_strlen($textarea) > $max_len){	//お問い合わせ 文字数チェック
			$error_message = "お問合せ内容は".$max_len."文字以内で入力してください。";
			$smarty->assign("error_message", $error_message);
			$error_flag = $flag_f;
		}
//----------------------------------------------------------

		if($error_flag == $flag_t){
			//入力確認メッセージ
			$smarty->assign("conf_message", $conf_message);
			//確認表示
			$smarty->assign("user_name", $user_name);
			$smarty->assign("tel", $tel);
			$smarty->assign("email", $email);
			$smarty->assign("zipcode1", $zipcode1);
			$smarty->assign("zipcode2", $zipcode2);
			$smarty->assign("pref", $pref);
			$smarty->assign("todofuken", $todofuken);
			$smarty->assign("address1", $address1);
			$smarty->assign("address2", $address2);
			$smarty->assign("textarea", $textarea);
			//ページ表示
			$smarty->display("./contact.tpl");
			exit();
		}else{
			//入力エラー ページ表示
			$smarty->assign("mode", $insert_commit);
			$smarty->display("./contact.tpl");
			exit();
		}
	}elseif($mode == $insert_commit_on){
		//DB処理をここに書く
		$user_contact_dao->transaction_start();
		$user_contact = new UserContact();
		$user_contact->setUserName($user_name);
		$user_contact->setPref($pref);
		$user_contact->setAddress1($address1);
		$user_contact->setAddress2($address2);
		$user_contact->setTel($tel);
		$user_contact->setEmail($email);

		$textarea_kai = ereg_replace("\r\n", "<br />", $textarea);
		$user_contact->setTextarea($textarea_kai);

		$db_result = $user_contact_dao->insertUserContact($user_contact, $result_message);
		if($db_result) {
			$user_contact_dao->transaction_end();
		}

		//登録完了ページ
		$smarty->display("./contact_info.tpl");
	}
?>