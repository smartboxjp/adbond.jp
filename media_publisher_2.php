<?php
define('SMARTY_DIR', 'Smarty/libs/');
require_once(SMARTY_DIR . 'Smarty.class.php');

// 共通設定
require_once( './common/CommonAdminBase.php' );
require_once( './common/CommonDao.php' );
require_once( './dto/LoginUser.php' );
require_once( './dao/MediaLoginUserDao.php' );
require_once( './dto/MediaLoginUser.php' );
require_once( './dao/MediaPublisherDao.php' );
require_once( './dto/MediaPublisher.php' );
require_once( './dao/MediaGroupDao.php' );
require_once( './dto/MediaGroup.php' );
require_once( './dao/PrefDao.php' );
require_once( './dto/Pref.php' );
require_once( './dao/MediaCategoryDao.php' );
require_once( './dto/MediaCategory.php' );


session_start();
session_regenerate_id(true);

// Smartyオブジェクト取得
$smarty = new Smarty();
$smarty->template_dir = './templates/web/';
$smarty->compile_id   = 'web';
$smarty->compile_dir  = './templates_c/';
$smarty->config_dir   = './config/';
$smarty->cache_dir    = './cache/';


	$smarty->assign("title", "Test Top Page");
	$smarty->assign("login_user", $login_user );

	$common_dao = new CommonDao();
	$media_login_user_dao = new MediaLoginUserDao();
	$media_publisher_dao = new MediaPublisherDao();

	//媒体発行者グループ
	$media_group_dao = new MediaGroupDao();
	$media_group_array = array();
	foreach($media_group_dao->getAllMediaGroup() as $val){
		$row_array = array('id' => $val->getId(), 'name' => $val->getName());
		$media_group_array[$val->getId()] = $row_array;
	}
	$smarty->assign("media_group_array", $media_group_array);

	//都道府県
	$pref_dao = new PrefDao();
	$pref_array = array();
	foreach($pref_dao->getAllPref() as $val){
		$row_array = array('id' => $val->getId(), 'name' => $val->getName());
		$pref_array[$val->getId()] = $row_array;
	}
	$smarty->assign("pref_array", $pref_array);


	$mode = ($_POST['mode']);
//----------------------------------------------------------媒体管理者
	$media_group_id = ($_POST['media_group_id']);
	$login_id = ($_POST['login_id']);
	$login_pass = ($_POST['login_pass']);
	$publisher_name = ($_POST['publisher_name']);
	$contact_person = ($_POST['contact_person']);
	$tel = ($_POST['tel']);
	$fax = ($_POST['fax']);
	$email = ($_POST['email']);
	$zipcode1 = ($_POST['zipcode1']);
	$zipcode2 = ($_POST['zipcode2']);
	$pref = ($_POST['pref']);
	$address1 = ($_POST['address1']);
	$address2 = ($_POST['address2']);
	$transfer_type = ($_POST['transfer_type']);
	$bank_name = ($_POST['bank_name']);
	$branch_name = ($_POST['branch_name']);
	$account_type = ($_POST['account_type']);
	$account_holder = ($_POST['account_holder']);
	$account_number = ($_POST['account_number']);
	$postal_account_holder = ($_POST['postal_account_holder']);
	$postal_account_number = ($_POST['postal_account_number']);
	$status = ($_POST['status']);


//----------------------------------------------------------媒体管理
	//$media_publisher_id = ($_POST['media_publisher_id']);
//	$media_category = ($_POST['media_category']);
//	$media_name = ($_POST['media_name']);
//	$support_docomo = ($_POST['support_docomo']);
//	$support_softbank = ($_POST['support_softbank']);
//	$support_au = ($_POST['support_au']);
//	$support_pc = ($_POST['support_pc']);
//	$site_url_docomo = ($_POST['site_url_docomo']);
//	$site_url_softbank = ($_POST['site_url_softbank']);
//	$site_url_au = ($_POST['site_url_au']);
//	$site_url_pc = ($_POST['site_url_pc']);
//	$media_type = ($_POST['media_type']);
//	$page_view_day = ($_POST['page_view_day']);
//	$site_outline = ($_POST['site_outline']);
//	$response_type = ($_POST['response_type']);
//	$point_back_url = ($_POST['point_back_url']);
//	$point_test_url = ($_POST['point_test_url']);
//	$test_flag = ($_POST['test_flag']);
//	$status = ($_POST['status']);

	//$support_num = $support_docomo + $support_softbank + $support_au + $support_pc;

//----------------------------------------------------------

	$form_data = array('id' => $id,
		'login_id' => $login_id,
		'login_pass' => $login_pass,
		'media_group_id' => $media_group_id,
		'publisher_name' => $publisher_name,
		'contact_person' => $contact_person,
		'tel' => $tel,
		'fax' => $fax,
		'email' => $email,
		'zipcode1' => $zipcode1,
		'zipcode2' => $zipcode2,
		'pref' => $pref,
		'address1' => $address1,
		'address2' => $address2,
		'transfer_type' => $transfer_type,
		'bank_name' => $bank_name,
		'branch_name' => $branch_name,
		'account_type' => $account_type,
		'account_holder' => $account_holder,
		'account_number' => $account_number,
		'postal_account_holder' => $postal_account_holder,
		'postal_account_number' => $postal_account_number,
//		'status' => $status,
//
//		'media_category' => $media_category,
//		'media_name' => $media_name,
//		'support_docomo' => $support_docomo,
//		'support_softbank' => $support_softbank,
//		'support_au' => $support_au,
//		'support_pc' => $support_pc,
//		'site_url_docomo' => $site_url_docomo,
//		'site_url_softbank' => $site_url_softbank,
//		'site_url_au' => $site_url_au,
//		'site_url_pc' => $site_url_pc,
//		'media_type' => $media_type,
//		'page_view_day' => $page_view_day,
//		'site_outline' => $site_outline,
//		'response_type' => $response_type,
//		'point_back_url' => $point_back_url,
//		'point_test_url' => $point_test_url,
//		'test_flag' => $test_flag,
		'status' => $status);
	$smarty->assign("form_data", $form_data);

//----------------------------------------------------------

	if($_POST['mode'] == ''){
			$smarty->assign("mode", 'insert_commit');
			//$smarty->assign("sub_title", '新規追加');

			// ページを表示
			$smarty->display("./media_publisher_input_2.tpl");
			exit();
	}elseif($mode == 'insert_commit'){

//----------------------------------------------------------check 後で関数化
			$error_flag = 0;

			if($publisher_name == "") {
				$error_message = "企業名/個人名を入力してください。";
				$smarty->assign("error_message", $error_message);
				$error_flag = 1;
			}elseif($login_id == "") {
				$error_message = "ログインIDを入力してください。";
				$smarty->assign("error_message", $error_message);
				$error_flag = 1;
			}elseif(mb_strlen($login_id) < 6) {
				$error_message = "ログインIDは6文字以上です。";
				$smarty->assign("error_message", $error_message);
				$error_flag = 1;
			}elseif($login_pass == "") {
				$error_message = "パスワードを入力してください。";
				$smarty->assign("error_message", $error_message);
				$error_flag = 1;
			}elseif(mb_strlen($login_pass) < 6) {
				$error_message = "パスワードは6文字以上です。";
				$smarty->assign("error_message", $error_message);
				$error_flag = 1;
			}elseif($tel == ""){
				$error_message = "電話番号を入力してください。";
				$smarty->assign("error_message", $error_message);
				$error_flag = 1;
			}elseif($email == ""){
				$error_message = "メールアドレスを入力してください。";
				$smarty->assign("error_message", $error_message);
				$error_flag = 1;
			}elseif($zipcode1 == "" or $zipcode2 == ""){
				$error_message = "郵便番号を入力してください。";
				$smarty->assign("error_message", $error_message);
				$error_flag = 1;
			}elseif($address1 == "" or $address2 == ""){
				$error_message = "住所を入力してください。";
				$smarty->assign("error_message", $error_message);
				$error_flag = 1;
			}elseif($transfer_type != 3 and $bank_name == ""){
				$error_message = "振込先銀行名を入力してください。";
				$smarty->assign("error_message", $error_message);
				$error_flag = 1;
			}elseif($transfer_type != 3 and $branch_name == ""){
				$error_message = "支店名を入力してください。";
				$smarty->assign("error_message", $error_message);
				$error_flag = 1;
			}elseif($account_holder == ""){
				$error_message = "口座名義を入力してください。";
				$smarty->assign("error_message", $error_message);
				$error_flag = 1;
			}elseif($account_number == ""){
				$error_message = "口座番号を入力してください。";
				$smarty->assign("error_message", $error_message);
				$error_flag = 1;
			}elseif($transfer_type == 3 and $postal_account_holder == ""){
				$error_message = "郵便局振込先名義を入力してください。";
				$smarty->assign("error_message", $error_message);
				$error_flag = 1;
			}elseif($transfer_type == 3 and $postal_account_number == ""){
				$error_message = "郵便局口座番号を入力してください。";
				$smarty->assign("error_message", $error_message);
				$error_flag = 1;
//			}elseif($media_category == ""){
//				$error_message = "カテゴリを入力してください。";
//				$smarty->assign("error_message", $error_message);
//				$error_flag = 1;
//			}elseif($page_view_day == ""){
//				$error_message = "PV/日を入力してください。";
//				$smarty->assign("error_message", $error_message);
//				$error_flag = 1;
//			}elseif($point_back_url == ""){
//				$error_message = "ポイントバック通知先URLを入力してください。";
//				$smarty->assign("error_message", $error_message);
//				$error_flag = 1;
//			}elseif($media_name == ""){
//				$error_message = "サイト名を入力してください。";
//				$smarty->assign("error_message", $error_message);
//				$error_flag = 1;
//			}elseif($support_num < 1){
//				$error_message = "対応キャリアを選択してください。";
//				$smarty->assign("error_message", $error_message);
//				$error_flag = 1;
			}

//-------------------------------------------------

			if($error_flag == 0){
				$smarty->assign("mode", 'ok_commit');
				if($mode == 'ok_commit'){



				//既に登録されたログイン名か確認
				if(is_null($media_login_user_dao->getMediaLoginUserByLoginId($login_id))) {

				} else {
					$error_message = "入力されたログインIDは登録されてます。";
					$smarty->assign("error_message", $error_message);
					$error_flag = 1;
				}

				//既に登録されたカテゴリー名か確認
				if(is_null($media_category_dao->getMediaCategoryByName($media_category))) {

				} else {
					$media_category_dao->transaction_rollback();

					$error_message = "入力されたカテゴリー名は登録されてます。";
					$smarty->assign("error_message", $error_message);
					$error_flag = 1;
				}
			}


		if($error_flag == 0){

					//ユーザーID登録
					$media_login_user_dao->transaction_start();
					$media_login_user = new MediaLoginUser();
					$media_login_user->setUserName($publisher_name);
					$media_login_user->setLoginId($login_id);
					$media_login_user->setLoginPass($login_pass);
					$db_result = $media_login_user_dao->insertMediaLoginUser($media_login_user, $result_message);
					if($db_result) {
						$media_login_user_dao->transaction_end();

						$insert_record = $media_login_user_dao->getMediaLoginUserByLoginId($login_id);
						if(!is_null($insert_record)) {
							$login_user_id = $insert_record->getId();
						} else {
							$error_message = "ＤＢからのデータの取得に失敗しました。(su0000)";
							$error_flag = 1;
						}
					} else {
						$error_message = $result_message;
						$media_login_user_dao->transaction_rollback();
						$error_flag = 1;
					}
//					//カテゴリINSERTを実行
//					$media_category_new = new MediaCategory();
//					$media_category2->setName($media_category_new);
//					$db_result = $media_category_dao->insertMediaCategory($media_category2, $result_message);
//					if($db_result) {
//						$media_category_dao->transaction_end();
//
//					} else {
//						$media_category_dao->transaction_rollback();
//						$error_message = $result_message;
//						$error_flag = 1;
//					}
					//媒体管理者insert
					$media_publisher_dao->transaction_start();

					$media_publisher = new MediaPublisher();
					$media_publisher->setLoginUserId($login_user_id);
					$media_publisher->setMediaGroupId($media_group_id);
					$media_publisher->setPublisherName($publisher_name);
					$media_publisher->setContactPerson($contact_person);
					$media_publisher->setTel($tel);
					$media_publisher->setFax($fax);
					$media_publisher->setEmail($email);
					$media_publisher->setZipcode1($zipcode1);
					$media_publisher->setZipcode2($zipcode2);
					$media_publisher->setPref($pref);
					$media_publisher->setAddress1($address1);
					$media_publisher->setAddress2($address2);
					$media_publisher->setTransferType($transfer_type);
					$media_publisher->setBankName($bank_name);
					$media_publisher->setBranchName($branch_name);
					$media_publisher->setAccountType($account_type);
					$media_publisher->setAccountHolder($account_holder);
					$media_publisher->setAccountNumber($account_number);
					$media_publisher->setPostalAccountHolder($postal_account_holder);
					$media_publisher->setPostalAccountNumber($postal_account_number);
					$media_publisher->setStatus(1);

					//INSERTを実行
					$db_result = $media_publisher_dao->insertMediaPublisher($media_publisher, $result_message);
					if($db_result) {
						$media_publisher_dao->transaction_end();
						$smarty->assign("info_message", $result_message);
					}

//					//媒体管理insert
//					$media_dao->transaction_start();
//
//					$media = new Media();
//					$media->setMediaPublisherId($publisher_name);
//					$media->setMediaCategoryId($media_category);
//					$media->setMediaName($media_name);
//					$media->setSupportDocomo($support_docomo);
//					$media->setSupportSoftbank($support_softbank);
//					$media->setSupportAu($support_au);
//					$media->setSupportPc($support_pc);
//					$media->setSiteUrlDocomo($site_url_docomo);
//					$media->setSiteUrlSoftbank($site_url_softbank);
//					$media->setSiteUrlAu($site_url_au);
//					$media->setSiteUrlPc($site_url_pc);
//					$media->setMediaType($media_type);
//					$media->setPageViewDay($page_view_day);
//					$media->setSiteOutline($site_outline);
//					$media->setResponseType($response_type);
//					$media->setPointBackUrl($point_back_url);
//					$media->setPointTestUrl($point_test_url);
//					$media->setTestFlag($test_flag);
//					$media->setStatus($status);
//
//					//INSERTを実行
//					$db_result = $media_dao->insertMedia($media, $result_message);
//					if($db_result) {
//						$media_dao->transaction_end();
//					}

			}


			$smarty->display("./media_regist_info.tpl");
			exit();
		}else{
			$smarty->assign("mode", 'insert_commit');
			$smarty->display("./media_publisher_input_2.tpl");
			exit();
		}
	}
?>