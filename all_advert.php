<?php
define('SMARTY_DIR', 'Smarty/libs/');
require_once(SMARTY_DIR . 'Smarty.class.php');
require_once( './common/CommonDao.php' );
require_once( './dao/AdvertDao.php' );
require_once( './dto/Advert.php' );

session_start();
session_regenerate_id(true);

// Smartyオブジェクト取得
$smarty = new Smarty();
$smarty->template_dir = './templates/web/';
$smarty->compile_id   = 'web';
$smarty->compile_dir  = './templates_c/';
$smarty->config_dir   = './config/';
$smarty->cache_dir    = './cache/';

$common_dao = new CommonDao();

// *********************************************************************
// 新着広告 取得
// *********************************************************************
$advert_Max_sql = " SELECT "
			. " id, advert_name, "
			. " DATE_FORMAT(created_at,'%Y年%m月%d日') AS created_date "
			. " FROM "
			. " advert "
			. " WHERE "
			. " deleted_at is NULL "
			. " AND "
			. " status = '2' "
			. " AND "
			. " test_flag = '0' "
			. " AND "
			. " advert_category_id <> '11' "
			. " AND "
			. " advert_name not like '%アダルト%' "
			. " AND "
			. " advert_name not like '%※%' "
			. " AND "
			. " advert_name not like '%同人%' "
			. " AND "
			. " advert_name not like '%テスト%' "
			. " ORDER BY id DESC "
			. " limit 0, 30 ";

$row =  array();
$rec_count = 0;

$rec = $common_dao->db_query_ts($advert_Max_sql);
while($rec_count < count($rec)){
	$coming_soon_date[$rec_count] = ($row = $rec[$rec_count]);
	$smarty->assign("coming_str", $coming_soon_date);
	$rec_count += 1;
}

$smarty->assign("images_dir_path", "./advert_image/");

$smarty->display("./all_advert.tpl");
exit();

?>