<?php
// 共通設定
require_once( '../common/CommonWebBase.php' );
require_once( '../common/CommonDao.php' );
require_once( '../dao/MediaLoginUserDao.php' );
require_once( '../dto/MediaLoginUser.php' );
require_once( '../dao/MediaPublisherDao.php' );
require_once( '../dto/MediaPublisher.php' );
require_once( '../dao/MediaDao.php' );
require_once( '../dto/Media.php' );

session_start();

if(isset($_SESSION['media_logon_token']) && $_SESSION['media_logon_token'] != ''){
	$media_login_user_dao = new MediaLoginUserDao();
	$media_login_user = new MediaLoginUser();
	$media_login_user = $_SESSION['media_login_user'];

	$login_user_id = $media_login_user->getid();
	$user_name = $media_login_user->getUserName();
	$login_id = $media_login_user->getLoginId();
	$login_pass = $media_login_user->getLoginPass();

	// Smartyオブジェクト取得
	$smarty =& getSmartyObj();

	$smarty->assign("user_name", $user_name);

	//登録者情報、口座情報取得
	$media_publisher_dao = new MediaPublisherDao();
	$media_publisher = new MediaPublisher();
	$media_publisher = $media_publisher_dao->getMediaPublisherByLoginUserId($login_user_id);

	$data = array('id' => $media_publisher->getId(),
						'login_id' => $media_login_user->getLoginId(),
						'login_pass' => $media_login_user->getLoginPass(),
						'login_user_id' => $media_publisher->getLoginUserId(),
						'media_group_id' => $media_publisher->getMediaGroupId(),
						'publisher_name' => $media_publisher->getPublisherName(),
						'contact_person' => $media_publisher->getContactPerson(),
						'tel' => $media_publisher->getTel(),
						'fax' => $media_publisher->getFax(),
						'email' => $media_publisher->getEmail(),
						'zipcode1' => $media_publisher->getZipcode1(),
						'zipcode2' => $media_publisher->getZipcode2(),
						'pref' => $media_publisher->getPref(),
						'address1' => $media_publisher->getAddress1(),
						'address2' => $media_publisher->getAddress2(),
						'transfer_type' => $media_publisher->getTransferType(),
						'bank_name' => $media_publisher->getBankName(),
						'branch_name' => $media_publisher->getBranchName(),
						'account_type' => $media_publisher->getAccountType(),
						'account_holder' => $media_publisher->getAccountHolder(),
						'account_number' => $media_publisher->getAccountNumber(),
						'postal_account_holder' => $media_publisher->getPostalAccountHolder(),
						'postal_account_number' => $media_publisher->getPostalAccountNumber(),
						'status' => $media_publisher->getStatus());

	$smarty->assign("data", $data);

	if(isset($_POST['mode']) && $_POST['mode'] != ''){
		if($_POST['mode'] == 'user_edit'){
			// ページを表示
			$smarty->display("./media_info_user.tpl");
			exit();
		} elseif($_POST['mode'] == 'user_update') {

			$login_id = do_escape_quotes($_POST['login_id']);
			$login_pass = do_escape_quotes($_POST['login_pass']);
			$publisher_name = do_escape_quotes($_POST['publisher_name']);
			$contact_person = do_escape_quotes($_POST['contact_person']);
			$tel = do_escape_quotes($_POST['tel']);
			$fax = do_escape_quotes($_POST['fax']);
			$email = do_escape_quotes($_POST['email']);

			$error_flag = 0;

			if($publisher_name == "") {
				$error_message = "企業名/個人名を入力してください。";
				$error_flag = 1;
			} elseif($login_id == "") {
				$error_message = "ログインIDを入力してください。";
				$error_flag = 1;
			} elseif($login_pass == "") {
				$error_message = "パスワードを入力してください。";
				$error_flag = 1;
			}

			//既に登録されたログイン名か確認
			$result_data = $media_login_user_dao->getMediaLoginUserByLoginId($login_id);

			if(is_null($result_data) || $login_user_id == $result_data->getId()) {
				$media_login_user_dao->transaction_start();

				$media_login_user->setUserName($publisher_name);
				$media_login_user->setLoginId($login_id);
				$media_login_user->setLoginPass($login_pass);

				//UPDATEを実行
				$db_result = $media_login_user_dao->updateMediaLoginUser($media_login_user, $result_message);
				if($db_result) {
					$media_login_user_dao->transaction_end();

					$media_login_user = $media_login_user_dao->getMediaLoginUserByLoginId($login_id);
					if(!is_null($media_login_user)) {
						$login_user_id = $media_login_user->getId();
					} else {
						$error_message = "ＤＢからのデータの取得に失敗しました。(su0000)";
						$error_flag = 1;
					}
				} else {
					$error_message = $result_message;

					$media_login_user_dao->transaction_rollback();

					$error_flag = 1;
				}
			} else {
				$error_message = "入力されたログインIDは登録されてます。";
				$error_flag = 1;
			}

			if($error_flag == 0) {
				$media_publisher_dao->transaction_start();

				$media_publisher->setPublisherName($publisher_name);
				$media_publisher->setContactPerson($contact_person);
				$media_publisher->setTel($tel);
				$media_publisher->setFax($fax);
				$media_publisher->setEmail($email);

				//UPDATEを実行
				$db_result = $media_publisher_dao->updateMediaPublisher($media_publisher, $result_message);
				if($db_result) {
					$media_publisher_dao->transaction_end();

					$smarty->assign("info_message", $result_message);

					$data = array('id' => $media_publisher->getId(),
										'login_id' => $media_login_user->getLoginId(),
										'login_pass' => $media_login_user->getLoginPass(),
										'login_user_id' => $media_publisher->getLoginUserId(),
										'media_group_id' => $media_publisher->getMediaGroupId(),
										'publisher_name' => $media_publisher->getPublisherName(),
										'contact_person' => $media_publisher->getContactPerson(),
										'tel' => $media_publisher->getTel(),
										'fax' => $media_publisher->getFax(),
										'email' => $media_publisher->getEmail(),
										'zipcode1' => $media_publisher->getZipcode1(),
										'zipcode2' => $media_publisher->getZipcode2(),
										'pref' => $media_publisher->getPref(),
										'address1' => $media_publisher->getAddress1(),
										'address2' => $media_publisher->getAddress2(),
										'transfer_type' => $media_publisher->getTransferType(),
										'bank_name' => $media_publisher->getBankName(),
										'branch_name' => $media_publisher->getBranchName(),
										'account_type' => $media_publisher->getAccountType(),
										'account_holder' => $media_publisher->getAccountHolder(),
										'account_number' => $media_publisher->getAccountNumber(),
										'postal_account_holder' => $media_publisher->getPostalAccountHolder(),
										'postal_account_number' => $media_publisher->getPostalAccountNumber(),
										'status' => $media_publisher->getStatus());

					$smarty->assign("data", $data);

				} else {
					$media_publisher_dao->transaction_rollback();

					$smarty->assign("error_message", $result_message);

					// ページを表示
					$smarty->display("./media_info_user.tpl");
					exit();
				}
			} else {
				$smarty->assign("error_message", $error_message);

				// ページを表示
				$smarty->display("./media_info_user.tpl");
				exit();
			}
		} elseif($_POST['mode'] == 'account_edit'){
			// ページを表示
			$smarty->display("./media_info_account.tpl");
			exit();
		} elseif($_POST['mode'] == 'account_update') {

			$bank_name = do_escape_quotes($_POST['bank_name']);
			$branch_name = do_escape_quotes($_POST['branch_name']);
			$account_type = do_escape_quotes($_POST['account_type']);
			$account_holder = do_escape_quotes($_POST['account_holder']);
			$account_number = do_escape_quotes($_POST['account_number']);

			$error_flag = 0;

			if($error_flag == 0) {
				$media_publisher_dao->transaction_start();

				$media_publisher->setBankName($bank_name);
				$media_publisher->setBranchName($branch_name);
				$media_publisher->setAccountType($account_type);
				$media_publisher->setAccountHolder($account_holder);
				$media_publisher->setAccountNumber($account_number);

				//UPDATEを実行
				$db_result = $media_publisher_dao->updateMediaPublisher($media_publisher, $result_message);
				if($db_result) {
					$media_publisher_dao->transaction_end();

					$smarty->assign("info_message", $result_message);

					$data = array('id' => $media_publisher->getId(),
										'login_id' => $media_login_user->getLoginId(),
										'login_pass' => $media_login_user->getLoginPass(),
										'login_user_id' => $media_publisher->getLoginUserId(),
										'media_group_id' => $media_publisher->getMediaGroupId(),
										'publisher_name' => $media_publisher->getPublisherName(),
										'contact_person' => $media_publisher->getContactPerson(),
										'tel' => $media_publisher->getTel(),
										'fax' => $media_publisher->getFax(),
										'email' => $media_publisher->getEmail(),
										'zipcode1' => $media_publisher->getZipcode1(),
										'zipcode2' => $media_publisher->getZipcode2(),
										'pref' => $media_publisher->getPref(),
										'address1' => $media_publisher->getAddress1(),
										'address2' => $media_publisher->getAddress2(),
										'transfer_type' => $media_publisher->getTransferType(),
										'bank_name' => $media_publisher->getBankName(),
										'branch_name' => $media_publisher->getBranchName(),
										'account_type' => $media_publisher->getAccountType(),
										'account_holder' => $media_publisher->getAccountHolder(),
										'account_number' => $media_publisher->getAccountNumber(),
										'postal_account_holder' => $media_publisher->getPostalAccountHolder(),
										'postal_account_number' => $media_publisher->getPostalAccountNumber(),
										'status' => $media_publisher->getStatus());

					$smarty->assign("data", $data);

				} else {
					$media_publisher_dao->transaction_rollback();

					$smarty->assign("error_message", $result_message);

					// ページを表示
					$smarty->display("./media_info_account.tpl");
					exit();
				}
			} else {
				$smarty->assign("error_message", $error_message);

				// ページを表示
				$smarty->display("./media_info_account.tpl");
				exit();
			}
		}
	}

	//メディア一覧取得
	$common_dao = new CommonDao();
	$list_sql = " SELECT * FROM media "
				. " WHERE deleted_at is NULL "
				. " AND media_publisher_id = " . $media_publisher->getId()
				. " ORDER BY id ASC ";

	$db_result = $common_dao->db_query($list_sql);
	if($db_result){
		$smarty->assign("media_array", $db_result);
	}else{
		$error_message .= "ＤＢからのデータの取得に失敗しました。(su0000)";
	}
	$smarty->assign("error_message", $error_message);

	// ページを表示
	$smarty->display("./media_info.tpl");
	exit();
}else{
	header('Location: ../index.php?error=1');
	exit();
}

function do_escape_quotes($str){
	//magic_quotesが有効ならクウォート部分を除去
	if(get_magic_quotes_gpc()){
		$str = stripslashes($str);
	}
	return $str;
}
?>