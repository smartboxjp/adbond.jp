<?php
// 共通設定
require_once( '../common/CommonWebBase.php' );
require_once( '../common/CommonDao.php' );
require_once( '../dao/MediaLoginUserDao.php' );
require_once( '../dto/MediaLoginUser.php' );
require_once( '../dao/MediaPublisherDao.php' );
require_once( '../dto/MediaPublisher.php' );
require_once( '../dao/MediaDao.php' );
require_once( '../dto/Media.php' );
require_once( '../dao/AdvertCategoryDao.php' );
require_once( '../dto/AdvertCategory.php' );
require_once( '../dao/ConnectLogDao.php' );
require_once( '../dto/ConnectLog.php' );
require_once( '../dao/AdvertPriceMediaSetDao.php' );
require_once( '../dto/AdvertPriceMediaSet.php' );

session_start();

if(isset($_SESSION['media_logon_token']) && $_SESSION['media_logon_token'] != ''){
	$media_login_user_dao = new MediaLoginUserDao();
	$media_login_user = new MediaLoginUser();
	$media_login_user = $_SESSION['media_login_user'];

	$login_user_id = $media_login_user->getid();
	$user_name = $media_login_user->getUserName();
	$login_id = $media_login_user->getLoginId();
	$login_pass = $media_login_user->getLoginPass();

	// Smartyオブジェクト取得
	$smarty =& getSmartyObj();

	$smarty->assign("user_name", $user_name);

	$image_directory = "../advert_image/";

	$media_id = do_escape_quotes($_POST['media_id']);
	$smarty->assign("media_id", $media_id);

	$advert_id = do_escape_quotes($_POST['advert_id']);
	$smarty->assign("advert_id", $advert_id);

	$connect_flag = do_escape_quotes($_POST['connect_flag']);
	$smarty->assign("connect_flag", $connect_flag);

	if(isset($_POST['mode']) && $_POST['mode'] == "apply") {
		$connect_log_dao = new ConnectLogDao();

		$connect_log_dao->transaction_start();

		$connect_log = new ConnectLog();
		$connect_log->setMediaId($media_id);
		$connect_log->setAdvertId($advert_id);
		$connect_log->setStatus(2);

		//INSERTを実行
		$db_result = $connect_log_dao->insertConnectLog($connect_log, $result_message);
		if($db_result) {
			$connect_log_dao->transaction_end();

			$connect_flag = 1;
			$smarty->assign("connect_flag", $connect_flag);

		} else {
			$connect_log_dao->transaction_rollback();
		}
	}


	// クライアント情報を取得
	$common_dao = new CommonDao();
	$sql = " SELECT a.*, ac.client_name as advert_client_name, aa.name as advert_category_name "
			. " FROM advert as a "
			. " LEFT JOIN advert_clients as ac on a.advert_client_id = ac.id "
			. " LEFT JOIN advert_categories as aa on a.advert_category_id = aa.id "
			. " WHERE a.deleted_at is NULL "
			. " AND a.id = '$advert_id' ";

	// DB接続
	$db_result = $common_dao->db_query($sql);
	if($db_result){
		$data = $db_result[0];

		// 媒体別単価を検索
		$advert_price_media_set_dao = new AdvertPriceMediaSetDao();
		$advert_price_media_set = $advert_price_media_set_dao->getAdvertPriceMediaSetByMidAid($advert_id, $media_id);

		// 媒体別単価のレコードが存在するか
		if(!is_null($advert_price_media_set)) {
			//媒体別単価のレコードが存在した場合

			// クリック単価
			if($advert_price_media_set->getClickPriceMedia() != 0){
				$data['click_price_media'] = $advert_price_media_set->getClickPriceMedia();
			}
			// アクションdocomo単価
			if($advert_price_media_set->getActionPriceMediaDocomo1() != 0){
				$data['action_price_media_docomo_1'] = $advert_price_media_set->getActionPriceMediaDocomo1();
			}
			// アクションsoftbank単価
			if($advert_price_media_set->getActionPriceMediaSoftbank1() != 0){
				$data['action_price_media_softbank_1'] = $advert_price_media_set->getActionPriceMediaSoftbank1();
			}
			// アクションau単価
			if($advert_price_media_set->getActionPriceMediaAu1() != 0){
				$data['action_price_media_au_1'] = $advert_price_media_set->getActionPriceMediaAu1();
			}
			// アクションpc単価
			if($advert_price_media_set->getActionPriceMediaPc1() != 0){
				$data['action_price_media_pc_1'] = $advert_price_media_set->getActionPriceMediaPc1();
			}
		}

		if($data['ms_image_type_1'] == 1 && $data['ms_image_url_1'] != "") {
			$data['ms_image_path_1'] = $image_directory . $data['ms_image_url_1'];
		} else {
			$data['ms_image_path_1'] = $data['ms_image_url_1'];
		}

		if($data['ms_image_type_2'] == 1 && $data['ms_image_url_2'] != "") {
			$data['ms_image_path_2'] = $image_directory . $data['ms_image_url_2'];
		} else {
			$data['ms_image_path_2'] = $data['ms_image_url_2'];
		}

		if($data['ms_image_type_3'] == 1 && $data['ms_image_url_3'] != "") {
			$data['ms_image_path_3'] = $image_directory . $data['ms_image_url_3'];
		} else {
			$data['ms_image_path_3'] = $data['ms_image_url_3'];
		}

		if($data['ms_image_type_4'] == 1 && $data['ms_image_url_4'] != "") {
			$data['ms_image_path_4'] = $image_directory . $data['ms_image_url_4'];
		} else {
			$data['ms_image_path_4'] = $data['ms_image_url_4'];
		}

		if($data['ms_image_type_5'] == 1 && $data['ms_image_url_5'] != "") {
			$data['ms_image_path_5'] = $image_directory . $data['ms_image_url_5'];
		} else {
			$data['ms_image_path_5'] = $data['ms_image_url_5'];
		}

		if($connect_flag == 1) {
			$data['advert_url'] = "http://adbond.jp/action/click.php?guid=ON&m=$media_id&a=$advert_id";
		}
		// smarty変数へ格納
		$smarty->assign("data", $data);



		// 提携中か
		if($connect_flag == '1'){
			// 提携中

//			// 既に媒体別単価レコードが存在するかselect
//			$result = $advert_price_media_set_dao->getAdvertPriceMediaSetByMidAid($advert_id, $media_id);
//
//			// DB結果
//			if(is_null($result)) {
//
//				// DB接続
//
//
//
//
//				// レコードが存在しない
//				$advert_price_media_set = new AdvertPriceMediaSet();
//				$advert_price_media_set->setAdvertId($advert_id);
//				$advert_price_media_set->setMediaId($media_id);
//				// クリック単価 クライアント
//				$advert_price_media_set->setClickPriceClient($click_price_client);
//				// クリック単価 メディア
//				$advert_price_media_set->setClickPriceMedia($click_price_media);
//				// クライアント アクション単価 ドコモ1
//				$advert_price_media_set->setActionPriceClientDocomo1($action_price_client_docomo_1);
//				// クライアント アクション単価 ソフバン1
//				$advert_price_media_set->setActionPriceClientSoftbank1($action_price_client_softbank_1);
//				// クライアント アクション単価 ＡＵ1
//				$advert_price_media_set->setActionPriceClientAu1($action_price_client_au_1);
//				// クライアント アクション単価 ＰＣ1
//				$advert_price_media_set->setActionPriceClientPc1($action_price_client_pc_1);
//				// クライアント アクション単価 ドコモ2
//				$advert_price_media_set->setActionPriceClientDocomo2($action_price_client_docomo_2);
//				// クライアント アクション単価 ソフバン2
//				$advert_price_media_set->setActionPriceClientSoftbank2($action_price_client_softbank_2);
//				// クライアント アクション単価 ＡＵ2
//				$advert_price_media_set->setActionPriceClientAu2($action_price_client_au_2);
//				// クライアント アクション単価 ＰＣ2
//				$advert_price_media_set->setActionPriceClientPc2($action_price_client_pc_2);
//				// クライアント アクション単価 ドコモ3
//				$advert_price_media_set->setActionPriceClientDocomo3($action_price_client_docomo_3);
//				// クライアント アクション単価 ソフバン3
//				$advert_price_media_set->setActionPriceClientSoftbank3($action_price_client_softbank_3);
//				// クライアント アクション単価 ＡＵ3
//				$advert_price_media_set->setActionPriceClientAu3($action_price_client_au_3);
//				// クライアント アクション単価 ＰＣ3
//				$advert_price_media_set->setActionPriceClientPc3($action_price_client_pc_3);
//				// クライアント アクション単価 ドコモ4
//				$advert_price_media_set->setActionPriceClientDocomo4($action_price_client_docomo_4);
//				// クライアント アクション単価 ソフバン4
//				$advert_price_media_set->setActionPriceClientSoftbank4($action_price_client_softbank_4);
//				// クライアント アクション単価 ＡＵ4
//				$advert_price_media_set->setActionPriceClientAu4($action_price_client_au_4);
//				// クライアント アクション単価 ＰＣ4
//				$advert_price_media_set->setActionPriceClientPc4($action_price_client_pc_4);
//				// クライアント アクション単価 ドコモ5
//				$advert_price_media_set->setActionPriceClientDocomo5($action_price_client_docomo_5);
//				// クライアント アクション単価 ソフバン5
//				$advert_price_media_set->setActionPriceClientSoftbank5($action_price_client_softbank_5);
//				// クライアント アクション単価 ＡＵ5
//				$advert_price_media_set->setActionPriceClientAu5($action_price_client_au_5);
//				// クライアント アクション単価 ＰＣ5
//				$advert_price_media_set->setActionPriceClientPc5($action_price_client_pc_5);
//
//				// メディア アクション単価 ドコモ1
//				$advert_price_media_set->setActionPriceMediaDocomo1($data['action_price_media_docomo_1']);
//				// メディア アクション単価 ソフバン1
//				$advert_price_media_set->setActionPriceMediaSoftbank1($data['action_price_media_softbank_1']);
//				// メディア アクション単価 ＡＵ1
//				$advert_price_media_set->setActionPriceMediaAu1($data['action_price_media_au_1']);
//				// メディア アクション単価 ＰＣ1
//				$advert_price_media_set->setActionPriceMediaPc1($data['action_price_media_pc_1']);
//				// メディア アクション単価 ドコモ2
//				$advert_price_media_set->setActionPriceMediaDocomo2($action_price_media_docomo_2);
//				// メディア アクション単価 ソフバン2
//				$advert_price_media_set->setActionPriceMediaSoftbank2($action_price_media_softbank_2);
//				// メディア アクション単価 ＡＵ2
//				$advert_price_media_set->setActionPriceMediaAu2($action_price_media_au_2);
//				// メディア アクション単価 ＰＣ2
//				$advert_price_media_set->setActionPriceMediaPc2($action_price_media_pc_2);
//				// メディア アクション単価 ドコモ3
//				$advert_price_media_set->setActionPriceMediaDocomo3($action_price_media_docomo_3);
//				// メディア アクション単価 ソフバン3
//				$advert_price_media_set->setActionPriceMediaSoftbank3($action_price_media_softbank_3);
//				// メディア アクション単価 ＡＵ3
//				$advert_price_media_set->setActionPriceMediaAu3($action_price_media_au_3);
//				// メディア アクション単価 ＰＣ3
//				$advert_price_media_set->setActionPriceMediaPc3($action_price_media_pc_3);
//				// メディア アクション単価 ドコモ4
//				$advert_price_media_set->setActionPriceMediaDocomo4($action_price_media_docomo_4);
//				// メディア アクション単価 ソフバン4
//				$advert_price_media_set->setActionPriceMediaSoftbank4($action_price_media_softbank_4);
//				// メディア アクション単価 ＡＵ4
//				$advert_price_media_set->setActionPriceMediaAu4($action_price_media_au_4);
//				// メディア アクション単価 ＰＣ4
//				$advert_price_media_set->setActionPriceMediaPc4($action_price_media_pc_4);
//				// メディア アクション単価 ドコモ5
//				$advert_price_media_set->setActionPriceMediaDocomo5($action_price_media_docomo_5);
//				// メディア アクション単価 ソフバン5
//				$advert_price_media_set->setActionPriceMediaSoftbank5($action_price_media_softbank_5);
//				// メディア アクション単価 ＡＵ5
//				$advert_price_media_set->setActionPriceMediaAu5($action_price_media_au_5);
//				// メディア アクション単価 ＰＣ5
//				$advert_price_media_set->setActionPriceMediaPc5($action_price_media_pc_5);
//
//				//INSERTを実行
//				$db_result = $advert_price_media_set_dao->insertAdvertPriceMediaSet($advert_price_media_set, $result_message);
//
//			}

		}


	} else {

	}

	if(isset($_POST['mode']) && $_POST['mode'] == "confirm") {
		// ページを表示
		$smarty->display("./media_search_confirm.tpl");
		exit();
	}

	// ページを表示
	$smarty->display("./media_search_view.tpl");
	exit();
}else{
	header('Location: ../index.php?error=1');
	exit();
}

//function view_list(){
//	global $common_dao, $smarty, $list_sql, $error_message;
//
//	$list_count = 0;
//
//	$db_result = $common_dao->db_query($list_sql);
//	if($db_result){
//		$smarty->assign("list", $db_result);
//		$list_count = count($db_result);
//	}else{
//		$error_message .= "ＤＢからのデータの取得に失敗しました。(su0000)";
//	}
//	$smarty->assign("list_count", $list_count);
//	$smarty->assign("error_message", $error_message);
//
//	// ページを表示
//	$smarty->display("./media_search.tpl");
//	exit();
//}

function do_escape_quotes($str){
	//magic_quotesが有効ならクウォート部分を除去
	if(get_magic_quotes_gpc()){
		$str = stripslashes($str);
	}
	return $str;
}
?>