<?php
// 共通設定
require_once( '../common/CommonWebBase.php' );
require_once( '../common/CommonDao.php' );
require_once( '../dao/MediaLoginUserDao.php' );
require_once( '../dto/MediaLoginUser.php' );
require_once( '../dao/MediaPublisherDao.php' );
require_once( '../dto/MediaPublisher.php' );
require_once( '../dao/MediaDao.php' );
require_once( '../dto/Media.php' );
require_once( '../dao/AdvertCategoryDao.php' );
require_once( '../dto/AdvertCategory.php' );
require_once( '../dao/ConnectLogDao.php' );
require_once( '../dto/ConnectLog.php' );
require_once( '../dao/AdvertPriceMediaSetDao.php' );
require_once( '../dto/AdvertPriceMediaSet.php' );
require_once( '../dao/AdvertViewMediaSetDao.php' );
require_once( '../dto/AdvertViewMediaSet.php' );


session_start();

if(isset($_SESSION['media_logon_token']) && $_SESSION['media_logon_token'] != ''){
	$media_login_user_dao = new MediaLoginUserDao();
	$media_login_user = new MediaLoginUser();
	$media_login_user = $_SESSION['media_login_user'];

	$login_user_id = $media_login_user->getid();
	$user_name = $media_login_user->getUserName();
	$login_id = $media_login_user->getLoginId();
	$login_pass = $media_login_user->getLoginPass();

	//登録者情報、口座情報取得
	$media_publisher_dao = new MediaPublisherDao();
	$media_publisher = new MediaPublisher();
	$media_publisher = $media_publisher_dao->getMediaPublisherByLoginUserId($login_user_id);
	$media_publisher_id = $media_publisher->getId();

	// Smartyオブジェクト取得
	$smarty =& getSmartyObj();

	$smarty->assign("user_name", $user_name);

	//メディア一覧取得
	$common_dao = new CommonDao();
	$list_sql = " SELECT * FROM media "
				. " WHERE deleted_at is NULL "
				. " AND media_publisher_id = " . $media_publisher->getId()
				. " ORDER BY id ASC ";

	$db_result = $common_dao->db_query($list_sql);
	if($db_result){
		$smarty->assign("media_array", $db_result);
	}else{
		//$error_message .= "ＤＢからのデータの取得に失敗しました。(su0000)";
	}

	//広告カテゴリー
	$advert_category_dao = new AdvertCategoryDao();
	$advert_category_array = array();
	foreach($advert_category_dao->getAllAdvertCategory() as $val){
		$row_array = array('id' => $val->getId(), 'name' => $val->getName());
		$advert_category_array[$val->getId()] = $row_array;
	}
	$smarty->assign("advert_category_array", $advert_category_array);

	$media_id = do_escape_quotes($_POST['media_id']);
	$smarty->assign("media_id", $media_id);

	$media_category_id = do_escape_quotes($_POST['media_category_id']);
	$smarty->assign("media_category_id", $media_category_id);

	$advert_id = do_escape_quotes($_POST['advert_id']);
	$smarty->assign("advert_id", $advert_id);

	$keyword = do_escape_quotes($_POST['keyword']);
	$category = (isset($_POST['category'])) ? $_POST['category'] : array();
	$support_docomo = do_escape_quotes($_POST['support_docomo']);
	$support_softbank = do_escape_quotes($_POST['support_softbank']);
	$support_au = do_escape_quotes($_POST['support_au']);
	$support_pc = do_escape_quotes($_POST['support_pc']);
	$min_price = do_escape_quotes($_POST['min_price']);
	$max_price = do_escape_quotes($_POST['max_price']);
	$advert_date_flag = do_escape_quotes($_POST['advert_date_flag']);
	$connect_status = do_escape_quotes($_POST['connect_status']);
	$point_back_flag = do_escape_quotes($_POST['point_back_flag']);

	$s_year = do_escape_quotes($_POST['s_year']);
	$s_month = do_escape_quotes($_POST['s_month']);
	$s_day = do_escape_quotes($_POST['s_day']);
	$e_year = do_escape_quotes($_POST['e_year']);
	$e_month = do_escape_quotes($_POST['e_month']);
	$e_day = do_escape_quotes($_POST['e_day']);

	$three_last_month = getdate(strtotime("-3 month"));
	$now_date = getdate();

	$smarty->assign("set_s_year", $three_last_month['year']);
	$smarty->assign("set_s_month", $three_last_month['mon']);
	$smarty->assign("set_s_day", $three_last_month['mday']);
	$smarty->assign("set_e_year", $now_date['year']);
	$smarty->assign("set_e_month", $now_date['mon']);
	$smarty->assign("set_e_day", $now_date['mday']);

	if(isset($_POST['submit']) && $_POST['submit'] != '') {
		if($_POST['submit'] == '選択') {

			$media_dao = new MediaDao();
			$media = new Media();
			$media = $media_dao->getMediaBYId($media_id);
			$media_category_id = $media->getMediaCategoryId();

// ************************************************************************************
// 正規に登録された媒体以外は広告を表示させない

			// smarty変数へメッセージを格納
			$smarty->assign("media_status", $media->getStatus());

			// メッセージ作成
			if($media->getStatus() != 2){

				// 仮登録
				if($media->getStatus() == 1 || $media->getStatus() == 0){
					// smarty変数へメッセージを格納
					$smarty->assign("info_message", "現在承認待ちです。");

				// 退会
				} elseif($media->getStatus() == 3) {
					// smarty変数へメッセージを格納
					$smarty->assign("info_message", "この案件は終了しました。");

				}

				// ページを表示
				$smarty->display("./media_search.tpl");
				exit();
			}

// ************************************************************************************





			$smarty->assign("media_category_id", $media_category_id);

//			$list_sql = " SELECT DISTINCT a.*, ac.name as advert_category_name "
//						. " FROM advert as a "
//						. " LEFT JOIN advert_categories as ac on a.advert_category_id = ac.id "
//						. " LEFT JOIN advert_view_media_sets as am on a.id = am.advert_id "
//						. " WHERE a.deleted_at is NULL "
//						. " AND (am.media_id <> '$media_id' OR am.media_id is NULL) "
//						. " AND (am.media_publisher_id <> '$media_publisher_id' OR am.media_publisher_id is NULL) "
//						. " AND (am.media_category_id <> '$media_category_id' OR am.media_category_id is NULL) "
//						. " AND a.status = '2' "
//						. " ORDER BY a.id ASC ";

			$list_sql = " SELECT a.id, a.advert_name, a.click_price_media, "
						. " a.action_price_media_docomo_1, a.action_price_media_softbank_1, "
						. " a.action_price_media_au_1, a.action_price_media_pc_1, "
						. " a.support_docomo, a.support_softbank, a.support_au, a.support_pc, "
						. " a.content_type, a.point_back_flag, a.unrestraint_flag, a.advert_end_date, "
						. " ac.name as advert_category_name "
						. " FROM advert as a "
						. " LEFT JOIN advert_categories as ac on a.advert_category_id = ac.id "
						. " WHERE a.deleted_at is NULL "
						. " AND a.status = '2' ";

// ************************************************************************************
// リンクエッジ用メディアの広告表示
// リンクエッジの広告のみ表示させる
// $login_user_id指定
// ORにて対応
// 71⇒AF-MAX
// 73⇒SAKAZUKI AF
// ************************************************************************************
						if($login_user_id == "62" ||
						 $login_user_id == '71' ||
						 $login_user_id == '73' ){

							$list_sql .= "AND a.advert_client_id = '35' ";
							$list_sql .= "OR a.id = '793'";
							$list_sql .= "OR a.id = '1'";
						}

// ************************************************************************************
// 2012/01/30 Akagawa 追加
// login_id=62のみ表示
// 1243、1244
// ************************************************************************************
						if($login_user_id == "62"){
							// マイデコメーカー(525円コース)
							$list_sql .= "OR a.id = '1243' ";
							// 競馬予想の競馬君(525円コース)
							$list_sql .= "OR a.id = '1244' ";
						}

// ************************************************************************************
// イークスアフィリ用メディアの広告表示
// Docomo対応の広告を非表示
// login_id⇒74
// ************************************************************************************
						/*
						if($login_user_id == "74"){
							// Docomo以外
							$list_sql .= "AND a.support_docomo <> '1' ";
							// 7Rush@スポニチ(315円コース)のみ表示
							$list_sql .= "OR a.id = '445' ";
							// 萌ゲー☆キャラブック(525円コース)
							$list_sql .= "OR a.id = '448' ";
							// ムービーフルPlus(315円コース)※3キャリア
							$list_sql .= "OR a.id = '587' ";
							// ムービーフル(315円コース)※3キャリア
							$list_sql .= "OR a.id = '633' ";
							// ファンタジーサーガ
							$list_sql .= "OR a.id = '673' ";
							// ワニブックス＠モバイル(315円コース)
							$list_sql .= "OR a.id = '447' ";

// ************************************************************************************
// 2011/12/02 Akagawa 追加
// 追加ID 1077
// ************************************************************************************
							// パチパチ動画777(315円コース)※docomo用
							$list_sql .= "OR a.id = '1077' ";
// ************************************************************************************
// 2011/12/07 Akagawa 追加
// 追加ID 1113、1114
// ************************************************************************************
							// ♂から♀へのアドバイス(300円コース)
							$list_sql .= "OR a.id = '1113' ";
							// 他人から見たあなた(300円コース)
							$list_sql .= "OR a.id = '1114' ";
							// どっきりコミック(525円コース)※docomo
							$list_sql .= "OR a.id = '1138' ";

							$list_sql .= "OR a.id = '1264' ";
							$list_sql .= "OR a.id = '1263' ";
							// めちゃコミ(525円コース)※docomo、au
							$list_sql .= "OR a.id = '1258' ";
							$list_sql .= "OR a.id = '1230' ";
							$list_sql .= "OR a.id = '1399' ";
							$list_sql .= "OR a.id = '1421' ";
							$list_sql .= "OR a.id = '1464' ";
							// リモートきせかえ
							$list_sql .= "OR a.id = '1541' ";
							$list_sql .= "OR a.id = '1543' ";
							$list_sql .= "OR a.id = '1544' ";

							$list_sql .= "OR a.id = '1558' ";
							$list_sql .= "OR a.id = '1610' ";
							$list_sql .= "OR a.id = '1611' ";
							$list_sql .= "OR a.id = '1650' ";
							$list_sql .= "OR a.id = '1693' ";
							$list_sql .= "OR a.id = '1748' ";
							$list_sql .= "OR a.id = '1756' ";
							$list_sql .= "OR a.id = '1766' ";
							$list_sql .= "OR a.id = '1765' ";
							$list_sql .= "OR a.id = '1795' ";
							//20130704追加
							$list_sql .= "OR a.id = '1462' ";
							$list_sql .= "OR a.id = '1463' ";
							$list_sql .= "OR a.id = '1788' ";
							//20130710追加
							$list_sql .= "OR a.id = '2030' ";
							//20131003追加
							$list_sql .= "OR a.id = '2115' ";
							//20131008追加
							$list_sql .= "OR a.id = '2120' ";
							//20131023追加
							$list_sql .= "OR a.id = '1877' ";
							$list_sql .= "OR a.id = '2130' ";
							//20131129追加
							$list_sql .= "OR a.id = '2143' ";
							//20131216追加
							$list_sql .= "OR a.id = '2148' ";
							//20140108追加
							$list_sql .= "OR a.id = '2158' ";
							$list_sql .= "OR a.id = '2160' ";
							$list_sql .= "OR a.id = '2161' ";
							//20140123追加
							$list_sql .= "OR a.id = '2185' ";
							//20140226追加
							$list_sql .= "OR a.id = '2196' ";
							//20140306追加
							$list_sql .= "OR a.id = '2201' ";
							//20140318追加
							$list_sql .= "OR a.id = '2214' ";
							//20140325追加
							$list_sql .= "OR a.id = '2216' ";
							//20140326追加
							$list_sql .= "OR a.id = '2217' ";
							$list_sql .= "OR a.id = '2220' ";
							//20140417追加
							$list_sql .= "OR a.id = '2224' ";
							$list_sql .= "OR a.id = '2225' ";
							//20140424追加
							$list_sql .= "OR a.id = '2241' ";
							$list_sql .= "OR a.id = '2242' ";
							//20140520追加
							$list_sql .= "OR a.id = '2258' ";
							$list_sql .= "OR a.id = '2260' ";
							$list_sql .= "OR a.id = '2262' ";
							//20140529追加
							$list_sql .= "OR a.id = '2275' ";
							//20140611追加
							$list_sql .= "OR a.id = '2278' ";
							$list_sql .= "OR a.id = '2280' ";
							//20140623追加
							$list_sql .= "OR a.id = '1721' ";
							//20140624追加
							$list_sql .= "OR a.id = '1192' ";
							$list_sql .= "OR a.id = '1972' ";
							$list_sql .= "OR a.id = '1977' ";
							//20140708追加
							$list_sql .= "OR a.id = '2283' ";
							$list_sql .= "OR a.id = '2284' ";
							//20140712追加
							$list_sql .= "OR a.id = '2285' ";
							$list_sql .= "OR a.id = '2286' ";
							$list_sql .= "OR a.id = '2287' ";
							$list_sql .= "OR a.id = '2288' ";
							$list_sql .= "OR a.id = '2289' ";
							//20140723追加
							$list_sql .= "OR a.id = '2295' ";
							//20140729追加
							$list_sql .= "OR a.id = '2296' ";
							$list_sql .= "OR a.id = '2297' ";
							//20140827追加
							$list_sql .= "OR a.id = '2309' ";
							$list_sql .= "OR a.id = '2310' ";
							$list_sql .= "OR a.id = '2311' ";
							$list_sql .= "OR a.id = '2312' ";
							$list_sql .= "OR a.id = '2313' ";
							$list_sql .= "OR a.id = '2314' ";
							$list_sql .= "OR a.id = '2315' ";
							$list_sql .= "OR a.id = '1920' ";
							//20140925追加
							$list_sql .= "OR a.id = '2345' ";
							//20140926追加
							$list_sql .= "OR a.id = '2344' ";
							//20141027追加
							$list_sql .= "OR a.id = '2359' ";
							$list_sql .= "OR a.id = '2361' ";
							$list_sql .= "OR a.id = '2363' ";
							$list_sql .= "OR a.id = '2364' ";
							$list_sql .= "OR a.id = '2365' ";
						}
						*/

// ************************************************************************************
// 2011/10/11 Akagawa 追加
// 発行者ID 82
// ************************************************************************************
//						if($login_user_id == "82"){
//							$list_sql .= "AND a.id = '860' ";
//						}

// ************************************************************************************
// 有限会社ネットAD用メディアの広告表示
// 広告指定
// login_id⇒76
// ************************************************************************************
//						if($login_user_id == "76"){
//							$list_sql .= "AND a.id = '1' ";
//
//						}

// ************************************************************************************
// メディアサーチを含む新規媒体の広告表示
// 表示せない広告を指定
// $login_user_id指定
// ORにて対応
// BREAK SOUNDフル(315円コース)
// BREAK SOUNDフル(315円コース)※アダルト用
// BREAK SOUNDフル(525円コース)
// BREAK SOUNDフル(1050円コース)
// ************************************************************************************
						if($login_user_id > 69){
//							echo "check1";
							// カテゴリ クッションORアダルト
							if($media_category_id == "11" || $media_category_id == "8"){
//								echo "check2";
								$list_sql .= "  "
								. " AND a.id <> '477' "
								. " AND a.id <> '478' "
								. " AND a.id <> '480' ";

							}

						}
// ************************************************************************************

						$list_sql .= " ORDER BY a.id ASC ";

//echo $list_sql;


			view_list();
		} elseif($_POST['submit'] == '検索') {

// ************************************************************************************
// メディアのステータスを確認

			$media_dao = new MediaDao();
			$media = new Media();
			$media = $media_dao->getMediaBYId($media_id);
			$media_category_id = $media->getMediaCategoryId();

			$smarty->assign("media_status", $media->getStatus());

// ************************************************************************************

			$search = array('keyword' => $keyword,
							'category' => $category,
							'support_docomo' => $support_docomo,
							'support_softbank' => $support_softbank,
							'support_au' => $support_au,
							'support_pc' => $support_pc,
							'min_price' => $min_price,
							'max_price' => $max_price,
							'advert_date_flag' => $advert_date_flag,
							'advert_start_date' => "$s_year-$s_month-$s_day",
							'advert_end_date' => "$e_year-$e_month-$e_day",
							'connect_status' => $connect_status,
							'point_back_flag' => $point_back_flag);

			$smarty->assign("search", $search);

//			$list_sql = "  SELECT DISTINCT a.*, ac.name as advert_category_name "
//						. " FROM advert as a "
//						. " LEFT JOIN advert_categories as ac on a.advert_category_id = ac.id "
//						. " LEFT JOIN advert_view_media_sets as am on a.id = am.advert_id "
//						. " WHERE a.deleted_at is NULL "
//						. " AND (am.media_id <> '$media_id' OR am.media_id is NULL) "
//						. " AND (am.media_publisher_id <> '$media_publisher_id' OR am.media_publisher_id is NULL) "
//						. " AND (am.media_category_id <> '$media_category_id' OR am.media_category_id is NULL) "
//						. " AND a.status = '2' ";

			$list_sql = " SELECT a.id, a.advert_name, a.click_price_media, "
						. " a.action_price_media_docomo_1, a.action_price_media_softbank_1, "
						. " a.action_price_media_au_1, a.action_price_media_pc_1, "
						. " a.support_docomo, a.support_softbank, a.support_au, a.support_pc, "
						. " a.content_type, a.point_back_flag, a.unrestraint_flag, a.advert_end_date, "
						. " ac.name as advert_category_name "
						. " FROM advert as a "
						. " LEFT JOIN advert_categories as ac on a.advert_category_id = ac.id "
						. " WHERE a.deleted_at is NULL "
						. " AND a.status = '2' ";

// ************************************************************************************
// リンクエッジ用メディアの広告表示
// リンクエッジの広告のみ表示させる
// $login_user_id指定
// ORにて対応
// 71⇒AF-MAX
// 73⇒SAKAZUKI AF
// ************************************************************************************
						if($login_user_id == "62" ||
						 $login_user_id == '71' ||
						 $login_user_id == '73' ){

							$list_sql .= "AND a.advert_client_id = '35' ";
							$list_sql .= "OR a.id = '793'";
							$list_sql .= "OR a.id = '1'";
						}

// ************************************************************************************
// 2012/01/30 Akagawa 追加
// login_id=62のみ表示
// 1243、1244
// ************************************************************************************
						if($login_user_id == "62"){
							// マイデコメーカー(525円コース)
							$list_sql .= "OR a.id = '1243' ";
							// 競馬予想の競馬君(525円コース)
							$list_sql .= "OR a.id = '1244' ";
						}

// ************************************************************************************
// イークスアフィリ用メディアの広告表示
// Docomo対応の広告を非表示
// login_id⇒74
// ************************************************************************************
						/*
						if($login_user_id == "74"){
							// Docomo以外
							$list_sql .= "AND ( a.support_docomo <> '1' ";
							// 7Rush@スポニチ(315円コース)のみ表示
							$list_sql .= "OR a.id = '445' ";
							// 萌ゲー☆キャラブック(525円コース)
							$list_sql .= "OR a.id = '448' ";
							// ムービーフルPlus(315円コース)※3キャリア
							$list_sql .= "OR a.id = '587' ";
							// ムービーフル(315円コース)※3キャリア
							$list_sql .= "OR a.id = '633' ";
							// ファンタジーサーガ
							$list_sql .= "OR a.id = '673' ) ";
							// ワニブックス＠モバイル(315円コース)
							$list_sql .= "OR a.id = '447' ";



// ************************************************************************************
// 2011/12/02 Akagawa 追加
// 追加ID 1077
// ************************************************************************************
							// パチパチ動画777(315円コース)※docomo用
							$list_sql .= "OR a.id = '1077' ";
// ************************************************************************************
// 2011/12/07 Akagawa 追加
// 追加ID 1113、1114
// ************************************************************************************
							// ♂から♀へのアドバイス(300円コース)
							$list_sql .= "OR a.id = '1113' ";
							// 他人から見たあなた(300円コース)
							$list_sql .= "OR a.id = '1114' ";
							// どっきりコミック(525円コース)※docomo
							$list_sql .= "OR a.id = '1138' ";

							$list_sql .= "OR a.id = '1399' ";
							$list_sql .= "OR a.id = '1421' ";
							$list_sql .= "OR a.id = '1464' ";
							// リモートきせかえ
							$list_sql .= "OR a.id = '1541' ";
							$list_sql .= "OR a.id = '1543' ";
							$list_sql .= "OR a.id = '1544' ";

							$list_sql .= "OR a.id = '1558' ";
							$list_sql .= "OR a.id = '1610' ";
							$list_sql .= "OR a.id = '1611' ";
							$list_sql .= "OR a.id = '1650' ";
							$list_sql .= "OR a.id = '1693' ";
							$list_sql .= "OR a.id = '1748' ";
							$list_sql .= "OR a.id = '1756' ";
							$list_sql .= "OR a.id = '1766' ";
							$list_sql .= "OR a.id = '1765' ";
							$list_sql .= "OR a.id = '1795' ";
							//20130704追加
							$list_sql .= "OR a.id = '1462' ";
							$list_sql .= "OR a.id = '1463' ";
							$list_sql .= "OR a.id = '1788' ";
							//20130710追加
							$list_sql .= "OR a.id = '2030' ";
							//20131003追加
							$list_sql .= "OR a.id = '2115' ";
							//20131008追加
							$list_sql .= "OR a.id = '2120' ";
							//20131023追加
							$list_sql .= "OR a.id = '1877' ";
							$list_sql .= "OR a.id = '2130' ";
							//20131129追加
							$list_sql .= "OR a.id = '2143' ";
							//20131216追加
							$list_sql .= "OR a.id = '2148' ";
							//20140108追加
							$list_sql .= "OR a.id = '2158' ";
							$list_sql .= "OR a.id = '2160' ";
							$list_sql .= "OR a.id = '2161' ";
							//20140123追加
							$list_sql .= "OR a.id = '2185' ";
							//20140226追加
							$list_sql .= "OR a.id = '2196' ";
							//20140306追加
							$list_sql .= "OR a.id = '2201' ";
							//20140318追加
							$list_sql .= "OR a.id = '2214' ";
							//20140325追加
							$list_sql .= "OR a.id = '2216' ";
							//20140326追加
							$list_sql .= "OR a.id = '2217' ";
							$list_sql .= "OR a.id = '2220' ";
							//20140417追加
							$list_sql .= "OR a.id = '2224' ";
							$list_sql .= "OR a.id = '2225' ";
							//20140424追加
							$list_sql .= "OR a.id = '2241' ";
							$list_sql .= "OR a.id = '2242' ";
							//20140520追加
							$list_sql .= "OR a.id = '2258' ";
							$list_sql .= "OR a.id = '2260' ";
							$list_sql .= "OR a.id = '2262' ";
							//20140529追加
							$list_sql .= "OR a.id = '2275' ";
							//20140611追加
							$list_sql .= "OR a.id = '2278' ";
							$list_sql .= "OR a.id = '2280' ";
							//20140623追加
							$list_sql .= "OR a.id = '1721' ";
							//20140624追加
							$list_sql .= "OR a.id = '1192' ";
							$list_sql .= "OR a.id = '1972' ";
							$list_sql .= "OR a.id = '1977' ";
							//20140708追加
							$list_sql .= "OR a.id = '2283' ";
							$list_sql .= "OR a.id = '2284' ";
							//20140712追加
							$list_sql .= "OR a.id = '2285' ";
							$list_sql .= "OR a.id = '2286' ";
							$list_sql .= "OR a.id = '2287' ";
							$list_sql .= "OR a.id = '2288' ";
							$list_sql .= "OR a.id = '2289' ";
							//20140723追加
							$list_sql .= "OR a.id = '2295' ";
							//20140729追加
							$list_sql .= "OR a.id = '2296' ";
							$list_sql .= "OR a.id = '2297' ";
							//20140827追加
							$list_sql .= "OR a.id = '2309' ";
							$list_sql .= "OR a.id = '2310' ";
							$list_sql .= "OR a.id = '2311' ";
							$list_sql .= "OR a.id = '2312' ";
							$list_sql .= "OR a.id = '2313' ";
							$list_sql .= "OR a.id = '2314' ";
							$list_sql .= "OR a.id = '2315' ";
							$list_sql .= "OR a.id = '1920' ";
							//20140925追加
							$list_sql .= "OR a.id = '2345' ";
							//20140926追加
							$list_sql .= "OR a.id = '2344' ";
							//20141027追加
							$list_sql .= "OR a.id = '2359' ";
							$list_sql .= "OR a.id = '2361' ";
							$list_sql .= "OR a.id = '2363' ";
							$list_sql .= "OR a.id = '2364' ";
							$list_sql .= "OR a.id = '2365' ";
						}
						*/

// ************************************************************************************
// 2011/10/11 Akagawa 追加
// 発行者ID 82
// ************************************************************************************
//						if($login_user_id == "82"){
//							$list_sql .= " AND a.id = '860' ";
//						}

// ************************************************************************************
// 有限会社ネットAD用メディアの広告表示
// 広告指定
// login_id⇒76
// ************************************************************************************
//						if($login_user_id == "76"){
//							$list_sql .= "AND a.id = '1' ";
//
//						}

// ************************************************************************************
// メディアサーチを含む新規媒体の広告表示
// 表示せない広告を指定
// $login_user_id指定
// ORにて対応
// BREAK SOUNDフル(315円コース)
// BREAK SOUNDフル(315円コース)※アダルト用
// BREAK SOUNDフル(525円コース)
// BREAK SOUNDフル(1050円コース)
// ************************************************************************************
						if($login_user_id > 69){
//							echo "check1";
							// カテゴリ クッションORアダルト
							if($media_category_id == "11" || $media_category_id == "8"){
//								echo "check2";
								$list_sql .= "  "
								. " AND a.id <> '477' "
								. " AND a.id <> '478' "
								. " AND a.id <> '480' ";

							}

						}
// ************************************************************************************



			$add_where = array();

			//キーワード
			if($keyword != "") {
				$list_sql .= " AND a.advert_name LIKE '%$keyword%' ";
			}

			//カテゴリー
			foreach($category as $key => $val) {
				if($val == 1) {
					$add_where[] = "a.advert_category_id = '$key'";
				}
			}
			if(count($add_where) > 0) {
				$list_sql .= " AND (".implode("OR ", $add_where).") ";
			}

			//対応キャリア
			if($support_docomo == 1) {
				$list_sql .= " AND a.support_docomo = '1' ";
			}
			if($support_softbank == 1) {
				$list_sql .= " AND a.support_softbank = '1' ";
			}
			if($support_au == 1) {
				$list_sql .= " AND a.support_au = '1' ";
			}
			if($support_pc == 1) {
				$list_sql .= " AND a.support_pc = '1' ";
			}

			//単価
			if($min_price != "") {
				if(is_numeric($min_price)) {
					$list_sql .= " AND (a.click_price_media >= '$min_price' OR a.action_price_media_pc >= '$min_price') ";
				}
			}
			if($max_price != "") {
				if(is_numeric($max_price)) {
					$list_sql .= " AND (a.action_price_media_pc <= '$max_price' OR a.action_price_media_pc <= '$max_price') ";
				}
			}

			//登録日時
			if($advert_date_flag == 1) {
				$list_sql .= " AND a.created_at BETWEEN '$s_year-$s_month-$s_day 00:00:00' AND '$e_year-$e_month-$e_day 23:59:59' ";
			}

//			//提携状態
//			if($connect_status == 2) {
//				$list_sql .= " AND (cl.media_id <> '$media_id' OR cl.media_id is NULL) ";
//			} elseif($connect_status == 3) {
//				$list_sql .= " AND cl.media_id = '$media_id' AND cl.status = '2' ";
//			}

			//ポイントバック対応
			if($point_back_flag == 2) {
				$list_sql .= " AND a.point_back_flag = '1' ";
			} elseif($point_back_flag == 3) {
				$list_sql .= " AND a.point_back_flag = '2' ";
			}

			$list_sql .= " ORDER BY a.id ASC ";

			view_list();

		} elseif($_POST['submit'] == 'CSVダウンロード') {

// ************************************************************************************
// メディアのステータスを確認

			$media_dao = new MediaDao();
			$media = new Media();
			$media = $media_dao->getMediaBYId($media_id);
			$media_category_id = $media->getMediaCategoryId();

			$smarty->assign("media_status", $media->getStatus());

// ************************************************************************************

			$connect_log_dao = new ConnectLogDao();

			$search = array('keyword' => $keyword,
							'category' => $category,
							'support_docomo' => $support_docomo,
							'support_softbank' => $support_softbank,
							'support_au' => $support_au,
							'support_pc' => $support_pc,
							'min_price' => $min_price,
							'max_price' => $max_price,
							'advert_date_flag' => $advert_date_flag,
							'advert_start_date' => "$s_year-$s_month-$s_day",
							'advert_end_date' => "$e_year-$e_month-$e_day",
							'connect_status' => $connect_status,
							'point_back_flag' => $point_back_flag);

			$smarty->assign("search", $search);

//			$list_sql = "  SELECT DISTINCT a.*, ac.name as advert_category_name "
//						. " FROM advert as a "
//						. " LEFT JOIN advert_categories as ac on a.advert_category_id = ac.id "
//						. " LEFT JOIN advert_view_media_sets as am on a.id = am.advert_id "
//						. " WHERE a.deleted_at is NULL "
//						. " AND (am.media_id <> '$media_id' OR am.media_id is NULL) "
//						. " AND (am.media_publisher_id <> '$media_publisher_id' OR am.media_publisher_id is NULL) "
//						. " AND (am.media_category_id <> '$media_category_id' OR am.media_category_id is NULL) "
//						. " AND a.status = '2' ";

			$list_sql = " SELECT a.id, a.advert_name, a.click_price_media, "
						. " a.action_price_media_docomo_1, a.action_price_media_softbank_1, "
						. " a.action_price_media_au_1, a.action_price_media_pc_1, "
						. " a.support_docomo, a.support_softbank, a.support_au, a.support_pc, "
						. " a.content_type, a.point_back_flag, a.unrestraint_flag, a.advert_end_date, "
						. "a.ms_text_1, a.ms_text_2, a.ms_text_3, a.ms_text_4, a.ms_text_5, "
						. " a.ms_image_type_1, a.ms_image_type_2, a.ms_image_type_3, a.ms_image_type_4, a.ms_image_type_5, "
						. " a.ms_image_url_1, a.ms_image_url_2, a.ms_image_url_3, a.ms_image_url_4, a.ms_image_url_5, "
						. " a.ms_email_1, a.ms_email_2, a.ms_email_3, a.ms_email_4, a.ms_email_5, "
						. " ac.name as advert_category_name "
						. " FROM advert as a "
						. " LEFT JOIN advert_categories as ac on a.advert_category_id = ac.id "
						. " WHERE a.deleted_at is NULL "
						. " AND a.status = '2' ";

// ************************************************************************************
// リンクエッジ用メディアの広告表示
// リンクエッジの広告のみ表示させる
// $login_user_id指定
// ORにて対応
// 71⇒AF-MAX
// 73⇒SAKAZUKI AF
// ************************************************************************************
						if($login_user_id == "62" ||
						 $login_user_id == '71' ||
						 $login_user_id == '73' ){

							$list_sql .= "AND a.advert_client_id = '35' ";
							$list_sql .= "OR a.id = '793'";
							$list_sql .= "OR a.id = '1'";
						}

// ************************************************************************************
// 2012/01/30 Akagawa 追加
// login_id=62のみ表示
// 1243、1244
// ************************************************************************************
						if($login_user_id == "62"){
							// マイデコメーカー(525円コース)
							$list_sql .= "OR a.id = '1243' ";
							// 競馬予想の競馬君(525円コース)
							$list_sql .= "OR a.id = '1244' ";
						}

// ************************************************************************************
// イークスアフィリ用メディアの広告表示
// Docomo対応の広告を非表示
// login_id⇒74
// ************************************************************************************
						/*
						if($login_user_id == "74"){
							// Docomo以外
							$list_sql .= "AND ( a.support_docomo <> '1' ";
							// 7Rush@スポニチ(315円コース)のみ表示
							$list_sql .= "OR a.id = '445' ";
							// 萌ゲー☆キャラブック(525円コース)
							$list_sql .= "OR a.id = '448' ";
							// ムービーフルPlus(315円コース)※3キャリア
							$list_sql .= "OR a.id = '587' ";
							// ムービーフル(315円コース)※3キャリア
							$list_sql .= "OR a.id = '633' ";
							// ファンタジーサーガ
							$list_sql .= "OR a.id = '673' )";
							// ワニブックス＠モバイル(315円コース)
							$list_sql .= "OR a.id = '447' ";



// ************************************************************************************
// 2011/12/02 Akagawa 追加
// 追加ID 1077
// ************************************************************************************
							// パチパチ動画777(315円コース)※docomo用
							$list_sql .= "OR a.id = '1077' ";
// ************************************************************************************
// 2011/12/07 Akagawa 追加
// 追加ID 1113、1114
// ************************************************************************************
							// ♂から♀へのアドバイス(300円コース)
							$list_sql .= "OR a.id = '1113' ";
							// 他人から見たあなた(300円コース)
							$list_sql .= "OR a.id = '1114' ";
							// どっきりコミック(525円コース)※docomo
							$list_sql .= "OR a.id = '1138' ";

							$list_sql .= "OR a.id = '1399' ";
							$list_sql .= "OR a.id = '1421' ";
							$list_sql .= "OR a.id = '1464' ";
							// リモートきせかえ
							$list_sql .= "OR a.id = '1541' ";
							$list_sql .= "OR a.id = '1543' ";
							$list_sql .= "OR a.id = '1544' ";

							$list_sql .= "OR a.id = '1558' ";
							$list_sql .= "OR a.id = '1610' ";
							$list_sql .= "OR a.id = '1611' ";
							$list_sql .= "OR a.id = '1650' ";
							$list_sql .= "OR a.id = '1748' ";
							$list_sql .= "OR a.id = '1756' ";
							$list_sql .= "OR a.id = '1766' ";
							$list_sql .= "OR a.id = '1765' ";
							$list_sql .= "OR a.id = '1795' ";
							//20130704追加
							$list_sql .= "OR a.id = '1462' ";
							$list_sql .= "OR a.id = '1463' ";
							$list_sql .= "OR a.id = '1788' ";
							//20130710追加
							$list_sql .= "OR a.id = '2030' ";
							//20131003追加
							$list_sql .= "OR a.id = '2115' ";
							//20131008追加
							$list_sql .= "OR a.id = '2120' ";
							//20131023追加
							$list_sql .= "OR a.id = '1877' ";
							$list_sql .= "OR a.id = '2130' ";
							//20131129追加
							$list_sql .= "OR a.id = '2143' ";
							//20131216追加
							$list_sql .= "OR a.id = '2148' ";
							//20140108追加
							$list_sql .= "OR a.id = '2158' ";
							$list_sql .= "OR a.id = '2160' ";
							$list_sql .= "OR a.id = '2161' ";
							//20140123追加
							$list_sql .= "OR a.id = '2185' ";
							//20140226追加
							$list_sql .= "OR a.id = '2196' ";
							//20140306追加
							$list_sql .= "OR a.id = '2201' ";
							//20140318追加
							$list_sql .= "OR a.id = '2214' ";
							//20140325追加
							$list_sql .= "OR a.id = '2216' ";
							//20140326追加
							$list_sql .= "OR a.id = '2217' ";
							$list_sql .= "OR a.id = '2220' ";
							//20140417追加
							$list_sql .= "OR a.id = '2224' ";
							$list_sql .= "OR a.id = '2225' ";
							//20140424追加
							$list_sql .= "OR a.id = '2241' ";
							$list_sql .= "OR a.id = '2242' ";
							//20140520追加
							$list_sql .= "OR a.id = '2258' ";
							$list_sql .= "OR a.id = '2260' ";
							$list_sql .= "OR a.id = '2262' ";
							//20140529追加
							$list_sql .= "OR a.id = '2275' ";
							//20140611追加
							$list_sql .= "OR a.id = '2278' ";
							$list_sql .= "OR a.id = '2280' ";
							//20140623追加
							$list_sql .= "OR a.id = '1721' ";
							//20140624追加
							$list_sql .= "OR a.id = '1192' ";
							$list_sql .= "OR a.id = '1972' ";
							$list_sql .= "OR a.id = '1977' ";
							//20140708追加
							$list_sql .= "OR a.id = '2283' ";
							$list_sql .= "OR a.id = '2284' ";
							//20140712追加
							$list_sql .= "OR a.id = '2285' ";
							$list_sql .= "OR a.id = '2286' ";
							$list_sql .= "OR a.id = '2287' ";
							$list_sql .= "OR a.id = '2288' ";
							$list_sql .= "OR a.id = '2289' ";
							//20140723追加
							$list_sql .= "OR a.id = '2295' ";
							//20140729追加
							$list_sql .= "OR a.id = '2296' ";
							$list_sql .= "OR a.id = '2297' ";
							//20140827追加
							$list_sql .= "OR a.id = '2309' ";
							$list_sql .= "OR a.id = '2310' ";
							$list_sql .= "OR a.id = '2311' ";
							$list_sql .= "OR a.id = '2312' ";
							$list_sql .= "OR a.id = '2313' ";
							$list_sql .= "OR a.id = '2314' ";
							$list_sql .= "OR a.id = '2315' ";
							$list_sql .= "OR a.id = '1920' ";
							//20140925追加
							$list_sql .= "OR a.id = '2345' ";
							//20140926追加
							$list_sql .= "OR a.id = '2344' ";
							//20141027追加
							$list_sql .= "OR a.id = '2359' ";
							$list_sql .= "OR a.id = '2361' ";
							$list_sql .= "OR a.id = '2363' ";
							$list_sql .= "OR a.id = '2364' ";
							$list_sql .= "OR a.id = '2365' ";
						}
						*/

// ************************************************************************************
// 2011/10/11 Akagawa 追加
// 発行者ID 82
// ************************************************************************************
//						if($login_user_id == "82"){
//							$list_sql .= " AND a.id = '860' ";
//						}

// ************************************************************************************
// 有限会社ネットAD用メディアの広告表示
// 広告指定
// login_id⇒76
// ************************************************************************************
//						if($login_user_id == "76"){
//							$list_sql .= "AND a.id = '1' ";
//
//						}

// ************************************************************************************
// メディアサーチを含む新規媒体の広告表示
// 表示せない広告を指定
// $login_user_id指定
// ORにて対応
// BREAK SOUNDフル(315円コース)
// BREAK SOUNDフル(315円コース)※アダルト用
// BREAK SOUNDフル(525円コース)
// BREAK SOUNDフル(1050円コース)
// ************************************************************************************
						if($login_user_id > 69){
//							echo "check1";
							// カテゴリ クッションORアダルト
							if($media_category_id == "11" || $media_category_id == "8"){
//								echo "check2";
								$list_sql .= "  "
								. " AND a.id <> '477' "
								. " AND a.id <> '478' "
								. " AND a.id <> '480' ";

							}

						}
// ************************************************************************************

			$add_where = array();

			//キーワード
			if($keyword != "") {
				$list_sql .= " AND a.advert_name LIKE '%$keyword%' ";
			}

			//カテゴリー
			foreach($category as $key => $val) {
				if($val == 1) {
					$add_where[] = "a.advert_category_id = '$key'";
				}
			}
			if(count($add_where) > 0) {
				$list_sql .= " AND (".implode("OR ", $add_where).") ";
			}

			//対応キャリア
			if($support_docomo == 1) {
				$list_sql .= " AND a.support_docomo = '1' ";
			}
			if($support_softbank == 1) {
				$list_sql .= " AND a.support_softbank = '1' ";
			}
			if($support_au == 1) {
				$list_sql .= " AND a.support_au = '1' ";
			}
			if($support_pc == 1) {
				$list_sql .= " AND a.support_pc = '1' ";
			}

			//単価
			if($min_price != "") {
				if(is_numeric($min_price)) {
					$list_sql .= " AND (a.click_price_media >= '$min_price' OR a.action_price_media_pc >= '$min_price') ";
				}
			}
			if($max_price != "") {
				if(is_numeric($max_price)) {
					$list_sql .= " AND (a.action_price_media_pc <= '$max_price' OR a.action_price_media_pc <= '$max_price') ";
				}
			}

			//登録日時
			if($advert_date_flag == 1) {
				$list_sql .= " AND a.created_at BETWEEN '$s_year-$s_month-$s_day 00:00:00' AND '$e_year-$e_month-$e_day 23:59:59' ";
			}

//			//提携状態
//			if($connect_status == 2) {
//				$list_sql .= " AND (cl.media_id <> '$media_id' OR cl.media_id is NULL) ";
//			} elseif($connect_status == 3) {
//				$list_sql .= " AND cl.media_id = '$media_id' AND cl.status = '2' ";
//			}

			//ポイントバック対応
			if($point_back_flag == 2) {
				$list_sql .= " AND a.point_back_flag = '1' ";
			} elseif($point_back_flag == 3) {
				$list_sql .= " AND a.point_back_flag = '2' ";
			}





			$list_sql .= " ORDER BY a.id ASC ";



			$db_result = $common_dao->db_query($list_sql);
			if($db_result){
				$outputFile = "../logs/output.cgi";
				touch($outputFile);

				$fp = fopen($outputFile, "w");

				mb_internal_encoding("UTF-8");
				mb_detect_order("ASCII, JIS, UTF-8, eucjp-win, sjis-win, EUC-JP, SJIS");

				$download_date = date("YmdHis");

				$file_name = "link_$download_date.csv";
				$csv_array[] = array('広告ID',
									'広告サイト名',
									'クリック単価',
									'アクション単価(docomo)',
									'アクション単価(softbank)',
									'アクション単価(au)',
									'アクション単価(pc)',
									'キャリア(docomo)',
									'キャリア(softbank)',
									'キャリア(au)',
									'キャリア(pc)',
									'サイト種別',
									'カテゴリー',
									'ポイントバック',
									'出稿終了日',
									'広告原稿1(テキスト)',
									'広告原稿2(テキスト)',
									'広告原稿3(テキスト)',
									'広告原稿4(テキスト)',
									'広告原稿5(テキスト)',
									'広告原稿1(イメージ)',
									'広告原稿2(イメージ)',
									'広告原稿3(イメージ)',
									'広告原稿4(イメージ)',
									'広告原稿5(イメージ)',
									'広告原稿1(メール)',
									'広告原稿2(メール)',
									'広告原稿3(メール)',
									'広告原稿4(メール)',
									'広告原稿5(メール)',
									'広告URL');

				$image_dir = "http://adbond.jp/advert_image/";

				foreach($db_result as $row) {
					$error_flag = 0;

					$advert_view_media_set_dao = new AdvertViewMediaSetDao();
					$result_view = $advert_view_media_set_dao->getAdvertViewMediaSetByMidAid($row['id'], $media_id, $media_publisher_id, $media_category_id);
					if(!is_null($result_view)) {
						$error_flag = 1;
					}

					$result_view = $advert_view_media_set_dao->getAdvertViewMediaSetByMidAid($row['id'], 0, $media_publisher_id, $media_category_id);
					if(!is_null($result_view)) {
						$error_flag = 1;
					}

					$result_view = $advert_view_media_set_dao->getAdvertViewMediaSetByMidAid($row['id'], 0, 0, $media_category_id);
					if(!is_null($result_view)) {
						$error_flag = 1;
					}

					if($connect_status == 2 || $connect_status == 3) {
						$connect_log_dao = new ConnectLogDao();
						$record = $connect_log_dao->getConnectLogByMidAid($media_id, $row['id']);
						if($connect_status == 2) {
							if(!is_null($record)) {
								$error_flag = 1;
							}
						} elseif($connect_status == 3) {
							if(is_null($record)) {
								$error_flag = 1;
							}
						}
					}

					if($error_flag == 0) {
						$data1 = $row['id'];
						$data2 = $row['advert_name'];

						$advert_price_media_set_dao = new AdvertPriceMediaSetDao();
						$advert_price_media_set = $advert_price_media_set_dao->getAdvertPriceMediaSetByMidAid($row['id'], $media_id);

						if(!is_null($advert_price_media_set)) {
							$data3 = $advert_price_media_set->getClickPriceMedia();
							$data4 = $advert_price_media_set->getActionPriceMediaDocomo1();
							$data5 = $advert_price_media_set->getActionPriceMediaSoftbank1();
							$data6 = $advert_price_media_set->getActionPriceMediaAu1();
							$data7 = $advert_price_media_set->getActionPriceMediaPc1();
						} else {
							$data3 = $row['click_price_media'];
							$data4 = $row['action_price_media_docomo_1'];
							$data5 = $row['action_price_media_softbank_1'];
							$data6 = $row['action_price_media_au_1'];
							$data7 = $row['action_price_media_pc_1'];
						}

						$data8 = ($row['support_docomo'] == 1) ? "○" : "-";
						$data9 = ($row['support_softbank'] == 1) ? "○" : "-";
						$data10 = ($row['support_au'] == 1) ? "○" : "-";
						$data11 = ($row['support_pc'] == 1) ? "○" : "-";
						$data12 = ($row['content_type'] == 1) ? "一般" : "公式";
						$data13 = $row['advert_categeory_name'];
						$data14 = ($row['point_back_flag'] == 1) ? "○" : "-";
						$data15 = ($row['unrestraint_flag'] == 1) ? "無制限" : $row['advert_end_date'];
						$data16 = $row['ms_text_1'];
						$data17 = $row['ms_text_2'];
						$data18 = $row['ms_text_3'];
						$data19 = $row['ms_text_4'];
						$data20 = $row['ms_text_5'];
						$data21 = ($row['ms_image_type_1'] == 1 && $row['ms_image_url_1'] != "") ? $image_dir.$row['ms_image_url_1'] : $row['ms_image_url_1'];
						$data22 = ($row['ms_image_type_2'] == 1 && $row['ms_image_url_2'] != "") ? $image_dir.$row['ms_image_url_2'] : $row['ms_image_url_2'];
						$data23 = ($row['ms_image_type_3'] == 1 && $row['ms_image_url_3'] != "") ? $image_dir.$row['ms_image_url_3'] : $row['ms_image_url_3'];
						$data24 = ($row['ms_image_type_4'] == 1 && $row['ms_image_url_4'] != "") ? $image_dir.$row['ms_image_url_4'] : $row['ms_image_url_4'];
						$data25 = ($row['ms_image_type_5'] == 1 && $row['ms_image_url_5'] != "") ? $image_dir.$row['ms_image_url_5'] : $row['ms_image_url_5'];
						$data26 = $row['ms_email_1'];
						$data27 = $row['ms_email_2'];
						$data28 = $row['ms_email_3'];
						$data29 = $row['ms_email_4'];
						$data30 = $row['ms_email_5'];
						$data31 = "http://adbond.jp/action/click.php?guid=ON&m=$media_id&a=".$row['id'];

						$csv_array[] = array($data1, $data2, $data3, $data4, $data5, $data6, $data7, $data8, $data9, $data10,
											$data11, $data12, $data13, $data14, $data15, $data16, $data17, $data18, $data19, $data20,
											$data21, $data22, $data23, $data24, $data25, $data26, $data27, $data28, $data29, $data30, $data31);

						$result = $connect_log_dao->getConnectLogByMidAid($media_id, $row['id']);
						if(is_null($result)) {
							$connect_log_dao->transaction_start();

							$connect_log = new ConnectLog();
							$connect_log->setMediaId($media_id);
							$connect_log->setAdvertId($row['id']);
							$connect_log->setStatus(2);

							//INSERTを実行
							$db_result = $connect_log_dao->insertConnectLog($connect_log, $result_message);
							if($db_result) {
								$connect_log_dao->transaction_end();
							} else {
								$connect_log_dao->transaction_rollback();
							}
						}
					}
				}

				mb_convert_variables("SJIS-win", "UTF-8", $csv_array);
				foreach($csv_array as $line) {
					fputcsv($fp, $line);
				}

				fclose($fp);

				header("Content-Type: application/csv");
				header("Content-Disposition: attachment; filename=".$file_name);
				header("Content-Length:".filesize($outputFile));
				readfile($outputFile);
				exit();
			} else {
				exit();
			}
		}
	}

	// ページを表示
	$smarty->display("./media_search.tpl");
	exit();
}else{
	header('Location: ../index.php?error=1');
	exit();
}

function view_list(){
	global $common_dao, $smarty, $list_sql, $error_message, $advert_id, $media_id, $media_publisher_id, $media_category_id, $connect_status;

	$list_count = 0;

	$db_result = $common_dao->db_query($list_sql);
	if($db_result){
		foreach($db_result as $key => $row) {

			$error_flag = 0;

			$advert_view_media_set_dao = new AdvertViewMediaSetDao();
			$record = $advert_view_media_set_dao->getAdvertViewMediaSetByMidAid($row['id'], $media_id, $media_publisher_id, $media_category_id);
			if(!is_null($record)) {
				$error_flag = 1;
			}

			$record = $advert_view_media_set_dao->getAdvertViewMediaSetByMidAid($row['id'], 0, $media_publisher_id, $media_category_id);
			if(!is_null($record)) {
				$error_flag = 1;
			}

			$record = $advert_view_media_set_dao->getAdvertViewMediaSetByMidAid($row['id'], 0, 0, $media_category_id);
			if(!is_null($record)) {
				$error_flag = 1;
			}

//			if($connect_status == 2 || $connect_status == 3) {
//				$connect_log_dao = new ConnectLogDao();
//				$record = $connect_log_dao->getConnectLogByMidAid($media_id, $row['id']);
//				if($connect_status == 2) {
//					if(!is_null($record)) {
//						$error_flag = 1;
//					}
//				} elseif($connect_status == 3) {
//					if(is_null($record)) {
//						$error_flag = 1;
//					}
//				}
//			}
			$connect_log_dao = new ConnectLogDao();
			$record = $connect_log_dao->getConnectLogByMidAid($media_id, $row['id']);
			if(!is_null($record)) {
				$db_result[$key]['connect_flag'] = 1;
				if($connect_status == 2) {
					$error_flag = 1;
				}
			} else {
				$db_result[$key]['connect_flag'] = 0;
				if($connect_status == 3) {
					$error_flag = 1;
				}
			}

			if($error_flag == 0) {
				$advert_price_media_set_dao = new AdvertPriceMediaSetDao();
				$advert_price_media_set = $advert_price_media_set_dao->getAdvertPriceMediaSetByMidAid($row['id'], $media_id);

				if(!is_null($advert_price_media_set)) {
					// クリック単価
					if($advert_price_media_set->getClickPriceMedia() != 0){
						$db_result[$key]['click_price_media'] = $advert_price_media_set->getClickPriceMedia();
					}
					// アクションdocomo単価
					if($advert_price_media_set->getActionPriceMediaDocomo1() != 0){
						$db_result[$key]['action_price_media_docomo_1'] = $advert_price_media_set->getActionPriceMediaDocomo1();
					}
					// アクションsoftbank単価
					if($advert_price_media_set->getActionPriceMediaSoftbank1() != 0){
						$db_result[$key]['action_price_media_softbank_1'] = $advert_price_media_set->getActionPriceMediaSoftbank1();
					}
					// アクションau単価
					if($advert_price_media_set->getActionPriceMediaAu1() != 0){
						$db_result[$key]['action_price_media_au_1'] = $advert_price_media_set->getActionPriceMediaAu1();
					}
					// アクションpc単価
					if($advert_price_media_set->getActionPriceMediaPc1() != 0){
						$db_result[$key]['action_price_media_pc_1'] = $advert_price_media_set->getActionPriceMediaPc1();
					}
				}
			} else {
				unset($db_result[$key]);
			}
		}
//		echo "check1";

		$smarty->assign("list", $db_result);
		$list_count = count($db_result);
	}else{
		$error_message .= "ＤＢからのデータの取得に失敗しました。(su0000)";
	}
	$smarty->assign("list_count", $list_count);
	$smarty->assign("error_message", $error_message);

	// テスト出力
//	echo $list_sql;

	// ページを表示
	$smarty->display("./media_search.tpl");
	exit();
}

function do_escape_quotes($str){
	//magic_quotesが有効ならクウォート部分を除去
	if(get_magic_quotes_gpc()){
		$str = stripslashes($str);
	}
	return $str;
}
?>